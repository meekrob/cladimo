#include <stdlib.h>
#include <check.h>

Suite *
clm_suite (void)
{
    Suite * s = suite_create("clm");
    return s;
}
Suite *
gff_suite (void)
{
    Suite * s = suite_create("gff");
    return s;
}

int
main (void)
{
    int number_failed;
    Suite * s = clm_suite();
    SRunner *sr = srunner_create (s);
    srunner_run_all (sr, CK_NORMAL);
    number_failed = srunner_ntests_failed (sr);
    srunner_free (sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
