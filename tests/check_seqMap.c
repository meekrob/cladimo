#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include "../src/clm/seqMap.h"

struct seqMap * smap;
char readSeqLine_0_filename[] = "seq_input/seqline_0.txt";
char readSeqLine_0_seq[] = "aaagatgctatgctaatttgtaAAACCCAAAATAAGTTTT"; // tested by test_seqMap_readSeqLine_0_projectSeqIndex
char readSeqLine_0_text[] = "--aaagatgctatgctaattt--gtaAAACCC-AAAATAAGTTTT"; // tested by test_seqMap_readSeqLine_0_projectedSequence

void
tcase_seqMap_readSeqLine_0_setup (void)
{
    /*                            0         1         2         3         4     */
    /*                text index: 012345678901234567890123456789012345678901234 */
    /*             file contains: --aaagatgctatgctaattt--gtaAAACCC-AAAATAAGTTTT */
    smap = newSeqMap();
    FILE * infile = fopen (readSeqLine_0_filename, "r");
    readSeqLine(infile, smap);
    fclose(infile);
}
void
tcase_seqMap_readSeqLine_0_teardown (void)
{
    seqMap_free(&smap);
}

/** test seqMap FILE reading routines **/
START_TEST (test_seqMap_readSeqLine_0_seq)
{
    // inspect the gapfree sequence
    ck_assert_str_eq((char*)smap->seq, readSeqLine_0_seq);
    ck_assert_int_eq(smap->seqlen, 40);
    ck_assert_int_eq(smap->maplen, 45);
}
END_TEST
START_TEST (test_seqMap_readSeqLine_0_col_to_coord)
{
    // inspect the col_to_coord
    /* int col_to_coord ('-' == -1) : --0123456789012345678--901234567-890123456789 */
    /*                            0           1           2          3          */
    ck_assert_int_eq(smap->col_to_coord[0], -1); // leading gap
    ck_assert_int_eq(smap->col_to_coord[1], -1);
    ck_assert_int_eq(smap->col_to_coord[2], 0);
    ck_assert_int_eq(smap->col_to_coord[12], 10);
    ck_assert_int_eq(smap->col_to_coord[21], -1); // two gaps at 21-22
    ck_assert_int_eq(smap->col_to_coord[22], -1);
    ck_assert_int_eq(smap->col_to_coord[23], 19);
    ck_assert_int_eq(smap->col_to_coord[32], -1); // last gap at 32
    ck_assert_int_eq(smap->col_to_coord[33], 28); 
}
END_TEST
START_TEST (test_seqMap_readSeqLine_0_coord_to_col)
{
    // inspect the coord_to_col
    /*                            v */
    /*                      text: --aaagatgctatgctaattt--gtaAAACCC-AAAATAAGTTTT */
    /* only seq-valid indices   :   2345678901234567890  345678901 345678901234 */
    /*                     index: 012345678901234567890123456789012345678901234 */
    /*                            0         1         2         3         4     */

    /*                           0!       1         2!      3 !      4     */
    /* int coord_to_col  has          2345678901234567890345678901345678901234 */ 
    /*                          : 0123456789012345678901234567890123456789 */
    /*                            0         1         2         3          */
    ck_assert_int_eq(smap->coord_to_col[0],  2); // !!! gap x2 !!!
    ck_assert_int_eq(smap->coord_to_col[1],  3); // 
    ck_assert_int_eq(smap->coord_to_col[2],  4); 
    ck_assert_int_eq(smap->coord_to_col[3],  5); 
    ck_assert_int_eq(smap->coord_to_col[4],  6); 

    ck_assert_int_eq(smap->coord_to_col[5],  7); 
    ck_assert_int_eq(smap->coord_to_col[6],  8); 
    ck_assert_int_eq(smap->coord_to_col[7],  9); 
    ck_assert_int_eq(smap->coord_to_col[8],  10); 
    ck_assert_int_eq(smap->coord_to_col[9],  11); 

    ck_assert_int_eq(smap->coord_to_col[10], 12); 
    ck_assert_int_eq(smap->coord_to_col[11], 13); 
    ck_assert_int_eq(smap->coord_to_col[12], 14); 
    ck_assert_int_eq(smap->coord_to_col[13], 15); 
    ck_assert_int_eq(smap->coord_to_col[14], 16); 

    ck_assert_int_eq(smap->coord_to_col[15], 17); 
    ck_assert_int_eq(smap->coord_to_col[16], 18); 
    ck_assert_int_eq(smap->coord_to_col[17], 19); 
    ck_assert_int_eq(smap->coord_to_col[18], 20); 
    ck_assert_int_eq(smap->coord_to_col[19], 23); // !!! gap x2 !!!

    ck_assert_int_eq(smap->coord_to_col[20], 24); 
    ck_assert_int_eq(smap->coord_to_col[21], 25); 
    ck_assert_int_eq(smap->coord_to_col[22], 26); 
    ck_assert_int_eq(smap->coord_to_col[23], 27); 
    ck_assert_int_eq(smap->coord_to_col[24], 28); 

    ck_assert_int_eq(smap->coord_to_col[25], 29); 
    ck_assert_int_eq(smap->coord_to_col[26], 30); 
    ck_assert_int_eq(smap->coord_to_col[27], 31); 
    ck_assert_int_eq(smap->coord_to_col[28], 33); // !!! gap x1 !!!
    ck_assert_int_eq(smap->coord_to_col[29], 34); 

    ck_assert_int_eq(smap->coord_to_col[30], 35); 
    ck_assert_int_eq(smap->coord_to_col[31], 36); 
    ck_assert_int_eq(smap->coord_to_col[32], 37); 
    ck_assert_int_eq(smap->coord_to_col[33], 38); 
    ck_assert_int_eq(smap->coord_to_col[34], 39); 

    ck_assert_int_eq(smap->coord_to_col[35], 40); 
    ck_assert_int_eq(smap->coord_to_col[36], 41); 
    ck_assert_int_eq(smap->coord_to_col[37], 42); 
    ck_assert_int_eq(smap->coord_to_col[38], 43); 
    ck_assert_int_eq(smap->coord_to_col[39], 44); 
}
END_TEST

/**
  Dereference the sequence text out of the alignment text (in readSeqLine_0_text)
  using projectSeqIndex(): 
  f(smap->coord_to_col, _i) => seq_i
  _i = 0..40, the length of the sequence.
**/
START_TEST (test_seqMap_readSeqLine_0_projectSeqIndex)
{
    int map_i = projectSeqIndex(_i, smap);
    unsigned char letter = readSeqLine_0_text[map_i];
    ck_assert(letter == smap->seq[_i]);
}
END_TEST
/**
  Recompose the alignment text using projectedSequence(): 
  f(smap->seq, smap->col_to_coord, _i) => alignmenttext_i
  _i = 0..44, the length of the alignment.
**/
START_TEST (test_seqMap_readSeqLine_0_projectedSequence)
{
    unsigned char letter = projectedSequence(_i, smap);
    ck_assert(letter == readSeqLine_0_text[_i]);
}
END_TEST

START_TEST (test_seqMap_readMapSeq)
{
    FILE * infile = fopen ("../sample_data/ghp010.fa", "r");
    struct seqMap * s = readMapSeq(infile);
    ck_assert_str_eq(s->info, "chr7_66075464_66076163");
    char testseq[] = "TTCCACAAATCCTGAGAGTTGATTTGAAGACCCCCAGAATAAATATTTCGTGCTGCTTTAAGATAGAGCAGGGCTTCAGAACACGCCAGGATGTCTCTTCAGCTTTTGGAAGGCTCCAGTGTCTGTGTTTCTGGAAGGTTCTAATCAGCAGCTCTATTTTTGTGGGTGGCTGGCGCGGCAGAGAGGCTCTGTGGCTGGGTGGCTGATAAGGTTATCTGGGGACTGTTTCTGGCAGTTCAAGTGCCTGTTGTACAACTTGGGGCTGGGGAAGTCGGAGGGCTGGGCAGGGCGGGGACTTCGTCGTCTTAGAGCTTGCCTTTCTGTGAGAGAGGAATAAAACCAAAGGTTCCACTACCCAGAGGCCCCTCATCTCCACCATCCCAAGGCTCCCCGCTGTAGACTGTTGGGTATAGATAGGTAGGAATGGTTGTAGCATCCTATGACATAGCCCAAAGGGTGTACCTCCGTGCAGCCCAGCTGGACCCACCAGGAGCCAGTGGGGTCAGGAGAGTTGGGTCCAGCCTAGACAAGAGACTGGCAGTGGAGTTAGATCACAACCTATAGACGGACAGGGCAGGCACCCCACCCACCATTCAACCACTGTGCACTCCGAACAGACAGGGCTCAGGGGAGGAAGCCTGCTAGCCCTTGGGCCCACACATGGCTTCAGGCAGAGGCAGCCAACGCTGTGGAGGCTGG";
    ck_assert_str_eq((char*)s->seq,testseq);
    seqMap_free(&s);
    fclose(infile);
}
END_TEST

#include "../src/clm/maf.h"

Suite *
seqMap_suite (void)
{
    Suite * s = suite_create("seqMap");

    TCase *tc_seqMap_readSeqLine_0 = tcase_create ("seqMap_readSeqLine_0");
    tcase_add_unchecked_fixture (tc_seqMap_readSeqLine_0, tcase_seqMap_readSeqLine_0_setup, tcase_seqMap_readSeqLine_0_teardown);
    tcase_add_test (tc_seqMap_readSeqLine_0, test_seqMap_readSeqLine_0_seq);
    tcase_add_test (tc_seqMap_readSeqLine_0, test_seqMap_readSeqLine_0_col_to_coord);
    tcase_add_test (tc_seqMap_readSeqLine_0, test_seqMap_readSeqLine_0_coord_to_col);
    tcase_add_loop_test (tc_seqMap_readSeqLine_0, test_seqMap_readSeqLine_0_projectSeqIndex, 0, 40);
    tcase_add_loop_test (tc_seqMap_readSeqLine_0, test_seqMap_readSeqLine_0_projectedSequence, 0, 44);
    suite_add_tcase (s, tc_seqMap_readSeqLine_0);
    return s;
}

int
main (void)
{
    int number_failed;
    Suite * s = seqMap_suite();
    SRunner *sr = srunner_create (s);
    //srunner_set_xml (sr, "check_seqMap_log.xml");
    srunner_run_all (sr, CK_NORMAL);
    number_failed = srunner_ntests_failed (sr);
    srunner_free (sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
