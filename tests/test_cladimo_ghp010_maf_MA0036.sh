# simple cladimo runner
cladimo="../src/cladimo"
sample_data="../sample_data" 
data="$sample_data/ghp010.maf"
query="MA0036"
positive_control="$sample_data/cladimo_ghp010_maf_MA0036.gff"
processed_positive_control=`basename $positive_control`.out
grep -v ^# $positive_control > $processed_positive_control

cmd="$cladimo $data $query"
#echo $cmd

output=`$cmd | grep -v '^#' | diff /dev/stdin $processed_positive_control`
# cleanup
rm $processed_positive_control

test -z "$output" || echo "$output" > /dev/stderr;
test -z "$output" || exit 1;
exit 0;
