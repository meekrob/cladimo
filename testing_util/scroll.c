#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "genomeFeature.h"
#include "append_to_array.h"
#include "attributes.h"
#include "variableType.h"

#ifndef MIN
#define MIN(x,y) ((x) < (y) ? x: y)
#endif
#ifndef MAX
#define MAX(x,y) ((x) > (y) ? x: y)
#endif

struct gfCladisticMotif
{
    struct genomeFeature *cladistic_motif;
    struct genomeFeature **matches;
    int nmatches;
    int min_column;
    int max_width;
};

#ifndef isnul
#define isnul(x) (x==NULL)
#endif
void gfCladisticMotifFree(struct gfCladisticMotif **cptr)
{
    if (isnul(cptr) || isnul(*cptr)) return;
    struct gfCladisticMotif *clm = *cptr;
    genomeFeatureFree(&(clm->cladistic_motif));

    for (int i=0; i < clm->nmatches; i++)
        genomeFeatureFree(&(clm->matches[i]));

    free(clm->matches);
    free(clm);
    *cptr = NULL;
}

struct gfCladisticMotif *new_gfCladisticMotif(struct genomeFeature *clm)
{
    struct gfCladisticMotif *new = malloczero(new);
    new->cladistic_motif = clm;
    return new;
}
struct gfCladisticMotif *read_ClmMatches(struct genomeFeature *clm, FILE *in)
{
    int nmatches = getAttrValInt("number_of_matches", 0, clm->attributes);
    struct gfCladisticMotif *newClm = new_gfCladisticMotif(clm);
    newClm->nmatches = readGFFLines(in, &(newClm->matches), nmatches, NULL);
    return newClm;
}

void set_spans(struct gfCladisticMotif **list, int nclms)
{
    for (int clmi=0; clmi<nclms; clmi++)
    {
        struct gfCladisticMotif *clm = list[clmi];
        // scan column/width for span
        for (int matchi=0; matchi < clm->nmatches; matchi++)
        {
            int column = getAttrValInt("column", 0, clm->matches[matchi]->attributes);
            int width = getAttrValInt("width", 0, clm->matches[matchi]->attributes);
            if (matchi==0)
            {
                clm->min_column = column;
                clm->max_width = width;
            }
            else
            {
                if (column < clm->min_column) clm->min_column = column;
                if (width > clm->max_width) clm->max_width = width;
            }
        }
    }
}
static void freeStrings(char ***str, int n)
{
    for (int i=0; i< n; i++) free((*str)[i]);
    free(*str);
    *str = NULL;
}

int str_compar(const void *a, const void *b)
{
    char **x = (char **)a;
    char **y = (char **)b;
    return strcmp(*x,*y);
}
static int condense_array(void *base, size_t nmemb, size_t size, int(*compar)(const void *, const void *));
static int condense_array(void *base, size_t nmemb, size_t size, int(*compar)(const void *, const void *))
{
    int i;
    size_t nrmemb = 1;
    for (i = nmemb - 2; i >= 0; i--)
    {
        void *a = base + size*i;
        void *b = base + size*(i + 1);
        if (compar(a, b) == 0)
            memmove(a,b, size*nrmemb);
        else
            nrmemb++;
    }
    return nrmemb;
}

char **cpy_unique(char **arr, int nmemb, int *nrlen)
{
    char **str_union = malloc(sizeof(char*)*nmemb);

    memcpy(str_union, arr, sizeof(char*)*nmemb);
    qsort(str_union, nmemb, sizeof(char*), str_compar);
    *nrlen = condense_array(str_union, nmemb, sizeof(char*), str_compar);
    if (*nrlen < nmemb) str_union = realloc(str_union, *nrlen * sizeof(char*));
    return str_union;
}
struct keyedAttribute *attr_str_union(char *newkeyname, struct keyedAttribute *list1, struct keyedAttribute *list2)
{
    int max = list1->nvalues + list2->nvalues;
    char **str_total = calloc(max, sizeof(char*));
    for (int listi1 = 0; listi1 < list1->nvalues; listi1++)
    {
        str_total[listi1] = list1->values[listi1]->sval;
    }
    for (int listi2 = 0; listi2 < list2->nvalues; listi2++)
    {
        str_total[list1->nvalues + listi2] = list2->values[listi2]->sval;
    }

    int nrlen = 0;
    char **str_union = cpy_unique(str_total, max, &nrlen);
    struct keyedAttribute *rval = keyedAttributeFromStrings(newkeyname, str_union, nrlen);
    free(str_union);
    free(str_total);

    return rval;
}
void printKeyedAttribute(FILE *out, struct keyedAttribute *attr);

struct gfCladisticMotif *mergeGFclm(struct gfCladisticMotif * restrict clm1, struct gfCladisticMotif * restrict clm2)
{
    // comprehensive span, reference sequence coordinates
    int start = MIN(clm1->cladistic_motif->start, clm2->cladistic_motif->start);
    int end = MAX(clm1->cladistic_motif->end, clm2->cladistic_motif->end);

    // modify clm1->cladistic_motif in-place to encompass the merge
    struct genomeFeature *clm = clm1->cladistic_motif;
    clm->start = start;
    clm->end = end;

    // take the union of species matched
    struct keyedAttribute *splist1 = popAttr("species_matched", clm1->cladistic_motif->attributes);
    struct keyedAttribute *splist2 = popAttr("species_matched", clm2->cladistic_motif->attributes);
    struct keyedAttribute *species_matched = attr_str_union("species_matched", splist1, splist2);
    freeKeyedAttribute(&splist1);
    freeKeyedAttribute(&splist2);

    addAttr(species_matched, clm->attributes);

    // append clm2 matchlist to clm1
    size_t arr_t = clm1->nmatches;
    clm1->nmatches = append_to_array(clm2->matches, &(clm1->matches), clm1->nmatches, clm2->nmatches, sizeof(struct genomeFeature*), &(arr_t), clm2->nmatches);

    delAttr("number_of_matches", clm->attributes);
    addAttr(keyedAttributeFromArgs("number_of_matches", "d", clm1->nmatches), clm->attributes);
    
    genomeFeatureFree(&(clm2->cladistic_motif));
    free(clm2->matches);
    free(clm2);
    return clm1;
}


struct gfCladisticMotif **merge_across_gaps(struct genomeFeature *blockptr, struct gfCladisticMotif **list, int *nclms, int max_gap_distance)
{
    int ncolumns = -1;
    if (nclms==0) 
        return list;

    if (blockptr) 
        ncolumns = getAttrValInt("ncolumns", 0, blockptr->attributes);

    struct gfCladisticMotif **mergedList = NULL;
    size_t arr_t = 0;
    int nclust = 0;

    set_spans(list, *nclms);
    struct gfCladisticMotif *last = list[0];
    for (int clmi=1; clmi < *nclms; clmi++)
    {
        struct gfCladisticMotif *curr = list[clmi];
        int gapdist = curr->min_column - (last->min_column + last->max_width); 

        if ((max_gap_distance > 0) && (gapdist < max_gap_distance)) // merge
        {
            last = mergeGFclm(last, curr);
        }
        else // don't merge
        {
            // add previously merged to list
            addAttr(keyedAttributeFromArgs("max_gap_distance", "d", max_gap_distance), last->cladistic_motif->attributes);
            nclust = append_to_array(&last, &(mergedList), nclust, 1, sizeof(struct gfCladisticMotif *), &(arr_t), 5);
            last = curr;
        }
    }
    addAttr(keyedAttributeFromArgs("max_gap_distance", "d", max_gap_distance), last->cladistic_motif->attributes);
    nclust = append_to_array(&last, &(mergedList), nclust, 1, sizeof(struct gfCladisticMotif *), &(arr_t), 5);
    *nclms = nclust;
    free(list);
    return mergedList;

}

struct gfCladisticMotif **read_all_in_block(FILE *in, int *ntotal, struct genomeFeature **nextblock)
{
    struct genomeFeature *gff = NULL;
    struct gfCladisticMotif **output = NULL;
    struct gfCladisticMotif **clm_array = NULL;

    int nclms = 0;
    size_t arr_t = 0;

    while((gff = readNextGFFLine(in, NULL)) != NULL)
    {
        if (strcmp(gff->feature, "cladistic_motif")==0)
        {
            struct gfCladisticMotif *clm = read_ClmMatches(gff, in);
            nclms = append_to_array(&clm, &(clm_array), nclms, 1, sizeof(struct gfCladisticMotif *), &(arr_t), 5);
        }
        else if (strcmp(gff->feature, "maf_block")==0)
        {
            *nextblock = gff;
            break;
        }
    }
    if (arr_t > nclms) output = realloc(output, sizeof(struct gfCladisticMotif *) * nclms);
    free(clm_array);
    return output;

}

struct gfCladisticMotif **read_all_merged(FILE *in, int *ntotal, int gapdist)
{
    struct genomeFeature *gff = NULL;
    struct genomeFeature *blockptr = NULL;
    struct gfCladisticMotif **output = NULL;
    *ntotal = 0;
    size_t total_t = 0;
    struct gfCladisticMotif **clm_array = NULL;

    int nclms = 0;
    size_t arr_t = 0;

    while((gff = readNextGFFLine(in, NULL)) != NULL)
    {
        if (strcmp(gff->feature, "maf_block")==0)
        {
            if (nclms > 0)
            {
                clm_array = merge_across_gaps(blockptr, clm_array, &nclms, gapdist);
                *ntotal = append_to_array(clm_array, &output, *ntotal, nclms, sizeof(struct gfCladisticMotif *), &(total_t), 5);
            }
            // start a new one
            genomeFeatureFree(&blockptr);
            blockptr = gff;
            nclms = 0;
            free(clm_array);
            clm_array = NULL;
            arr_t = 0;
        }
        else if (strcmp(gff->feature, "cladistic_motif")==0)
        {
            struct gfCladisticMotif *clm = read_ClmMatches(gff, in);
            nclms = append_to_array(&clm, &(clm_array), nclms, 1, sizeof(struct gfCladisticMotif *), &(arr_t), 5);
        }
    }
    if (nclms > 0)
    {
        clm_array = merge_across_gaps(blockptr, clm_array, &nclms, gapdist);
        *ntotal = append_to_array(clm_array, &output, *ntotal, nclms, sizeof(struct gfCladisticMotif *), &(total_t), 5);
    }
    free(clm_array);
    genomeFeatureFree(&blockptr);
    return output;
}

void printattribute(FILE *out, char *key, struct keyedAttributeList *list)
{
    struct keyedAttribute *attr = getAttr(key, list);
    fprintf(out, "<%s: ", attr->key);
    if (attr == NULL) 
    {
        fprintf(out, "NULL>");
        return;
    }

    for (int i=0; i< attr->nvalues; i++)
        fprintf(out, " %s", attr->values[i]->sval);

    fprintf(out, ">");
}

void writeCLM(FILE *out, struct gfCladisticMotif *clm)
{
    writeGFFgenomeFeature(out, clm->cladistic_motif);
    /*for(int i=0; i< clm->nmatches; i++)
    {
        fprintf(out, "\t");
        printattribute(out, "position", clm->matches[i]->attributes);
        printattribute(out, "column", clm->matches[i]->attributes);
        printattribute(out, "width", clm->matches[i]->attributes);
        fprintf(out, "\n");
        //fprintf(out, "\t");writeGFFgenomeFeature(out, clm->matches[i]);
    }*/
}

void score_bls(struct gfCladisticMotif *clm, struct btree *tree)
{
    struct keyedAttribute *species_matched_attr = getAttr("species_matched", clm->cladistic_motif->attributes);
    if (species_matched_attr != NULL)
    {
        struct keyedAttribute *bls = createBLSattr("species_matched", species_matched_attr, tree);
        addAttr(bls, clm->cladistic_motif->attributes);
        clm->cladistic_motif->score = bls->values[0]->fval;
    }
}

int compare_score(const void *a, const void *b)
{
    float x = (*(struct gfCladisticMotif **)a)->cladistic_motif->score;
    float y = (*(struct gfCladisticMotif **)b)->cladistic_motif->score;
    if (x<y) return 1;
    if (x>y) return -1;
    return 0;
}

int main(int argc, char **argv)
{
    char usage[] = "%s gapdist [tree.nh] < in.gff\n";
    if (argc < 2) 
    {
        fprintf(stderr, usage, argv[0]);
        return 1;
    }
    FILE *in = stdin;
    int gapdist = atoi(argv[1]);
    struct btree *tree = NULL;
    char **species = NULL;
    char *reference = NULL;
    int nspecies = 0;
    if (argc > 2) 
    {
        tree = readTreeFile(argv[2]);
        species = leaf_names(tree, &nspecies);
        reference = species[0];
        for (int i=0; i < nspecies; i++)
        {
            if (i>0) printf(",");
            printf("%s", species[i]);
        }
        printf("\n");
    }

    int nclm = 0;
    struct gfCladisticMotif **clms = read_all_merged(in, &nclm, gapdist);
    for (int i=0; i < nclm; i++)
    {
        score_bls(clms[i], tree);
    }
    qsort(clms, nclm, sizeof(struct gfCladisticMotif *), compare_score);

    for (int i=0; i < nclm; i++)
    {
        //if (i < 5)
        {
            //printf("%d:", i);
            writeCLM(stdout, clms[i]);
        }
        gfCladisticMotifFree(&(clms[i]));
    }
    free(clms);
    fclose(in);
    depth_first_free(&tree);
    freeStrings(&species, nspecies);
    return 0;
}

