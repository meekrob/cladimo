#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "maf.h"
typedef enum { maf, mfasta, fasta, nib } fileformat;
typedef struct 
{
    fileformat format;
    char *filename;
    FILE *fp;
} inputfile;

void parse_infile(inputfile *infile, char *path_arg)
{
    char *path = NULL;
    char *eqptr = NULL;
    if ((eqptr = index(path_arg, '=')) != NULL)
    {
        int eoffset = (eqptr - path_arg);
        path = (eqptr+1);
        if (0 == strncasecmp("mfasta", path_arg, eoffset)) infile->format = mfasta;
        else if (0 == strncasecmp("maf", path_arg, eoffset)) infile->format = maf;
        else if (0 == strncasecmp("nib", path_arg, eoffset)) infile->format = nib;
        else if (0 == strncasecmp("fasta", path_arg, eoffset)) infile->format = fasta;
        else
        {
            fprintf(stderr, "unrecognized format %.*s for input file %s\n", eoffset, (eqptr+1), path);
        }
    }
    else
    {
        path = path_arg;
        infile->format = maf;
    }
    infile->fp = fopen(path, "r");
    infile->filename = path;
}
void print_block(struct seqMap **block, int nseq);

int main(int argc, char **argv)
{
    inputfile infile;
    parse_infile(&infile, argv[1]);
    struct seqMap **alignment;
    struct seqMap ** overhang = NULL;
    int n_seqs = 0;
    while((alignment = read_mafblock_contiguous(infile.fp, &overhang, &n_seqs, 6, NULL)) != NULL)
    {
        //print_block(alignment, n_seqs);
        blockFree(&alignment, n_seqs);
    }
    blockFree(&overhang, n_seqs);
    fclose(infile.fp);
    return 0;
}

void print_block(struct seqMap **block, int nseq)
{
    int i;
    char strands[] = "- +";
    int width = MIN(block[0]->maplen, 75);
    for (i=0; i< nseq; i++)
    {
        fprintf(stdout, "%10.10s ", block[i]->info);
        fprintf(stdout, "% 10d % 10d %c ", block[i]->genome_coordinate.base, block[i]->genome_coordinate.base + block[i]->seqlen, strands[block[i]->genome_coordinate.strand + 1]);
        int a;
        for(a=0; a<width; a++) fputc(projectedSequence(a, block[i]), stdout);
        fputc('\n', stdout);
    }
}
