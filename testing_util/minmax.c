#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "combo_lib.h"
#include "seq.h"
#include "pwm.h"
#include "matrix.h"
void print_count_matrix(float **count_matrix, int pwmlen, int as_ints)
{
    _pwm_loop_
    char nt[] = "ACGT";
    for_each_base
    {
        printf("%4c ", nt[base]);
        for_each_position( pwmlen )
        {
            if (as_ints)
                printf("% 5d ", (int)count_matrix[base][position]);
            else
                printf("%5.2f ", count_matrix[base][position]);
        }
        printf("\n");
    }
}

int main(int argc, char **argv)
{
    _pwm_loop_
    char **arglist = NULL;
    if (argc > 1) arglist = &(argv[1]);
    struct combo_lib *runlib = parse_arg_list(arglist, 1);
    bitsequence minseq,maxseq;

    PWM *pwm = runlib->pwm_library[0];
    float **counts = pwm_counts(pwm);
    minseq = matrix_min_bitsequence(counts, pwm->length);
    maxseq = matrix_max_bitsequence(counts, pwm->length);
    print_numeric_sequence(stdout, minseq, pwm->length); putchar('\n');
    print_numeric_sequence(stdout, maxseq, pwm->length); putchar('\n');
    print_count_matrix(counts, pwm->length, 1);
    free_matrix(&counts);

    minseq = matrix_min_bitsequence(pwm->matrix, pwm->length);
    maxseq = matrix_max_bitsequence(pwm->matrix, pwm->length);
    print_numeric_sequence(stdout, minseq, pwm->length); putchar('\n');
    print_numeric_sequence(stdout, maxseq, pwm->length); putchar('\n');
    print_pwm(stdout, pwm);

    pwm_retransform(pwm, .05, .45, .45, .05);
    minseq = matrix_min_bitsequence(pwm->matrix, pwm->length);
    maxseq = matrix_max_bitsequence(pwm->matrix, pwm->length);
    print_numeric_sequence(stdout, minseq, pwm->length); putchar('\n');
    print_numeric_sequence(stdout, maxseq, pwm->length); putchar('\n');
    print_pwm(stdout, pwm);

    free_combo_lib(&runlib);
    return 0;
}
