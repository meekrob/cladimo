#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pwm.h"
#include "matrix.h"
#include "readers.h"

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "%s filename\n", argv[0]);
        return 1;
    }
    int filevar=1;
    for (; filevar < argc; filevar++)
    {
        int npwm=0;
        PWM **pwmlist = read_pwm_file(argv[filevar], &npwm);
        int i;
        for(i=0; i<npwm; i++)
        {
            PWM *pwm = pwmlist[i];
            fprintf(stdout, "accession %s\n", pwm->accession);
            fprintf(stdout, "name %s\n", pwm->name);
            fprintf(stdout, "label %s\n", pwm->label);
            fprintf(stdout, "consensus motif %s\n", pwm->motif);
            print_pwm(stdout, pwm);

            int j;
            float *col_max = calloc(pwm->length, sizeof(float));
            float *cum_max = calloc(pwm->length, sizeof(float));
            for (j=pwm->length-1; j >=0; j--)
            {
                float column_max = DOUBLE_MIN;
                int k;
                for (k=0; k<4; k++) column_max = MAX(column_max, pwm->matrix[k][j]);
                col_max[j] = column_max;
                if (j < pwm->length-1) cum_max[j] = cum_max[j+1] + col_max[j];
                else cum_max[j] = col_max[j];
            }
            for (j=0; j < pwm->length; j++) printf("%.3f ", col_max[j]);
            putchar('\n');
            for (j=0; j < pwm->length; j++) printf("%.3f ", cum_max[j]);
            putchar('\n');

            // now, for the supplied strings, compute the maximum possible score for each position of the string (should flatline quickly)
            int wordi;
            for (wordi = 2; wordi < argc; wordi++)
            {
                char* word= argv[wordi];
                int p = 0;
                float score = 0;
                for (p=0; p < strlen(word)-1; p++)
                {
                    score += pwm_match(word[p], p, pwm);
                    float max_possible_score = score + cum_max[p+1];
                    float forward_adj = (max_possible_score - pwm->min_score) / (pwm->max_score - pwm->min_score);
                    printf("%.3f ", forward_adj);
                }
                putchar('\n');

            }
            free(cum_max);
            free(col_max);
            free_pwm(&pwm);
        }
        free(pwmlist);
    }
    return 0;
}

