#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "seqMap.h"

int main(int argc, char **argv)
{
    int i;
    FILE *infile = fopen(argv[1], "r");
    struct seqMap *seq = NULL;
    while((seq = readMapSeq(infile)) != NULL)
    {
        fprintf(stderr, "info %s\n", seq->info);
        fprintf(stderr, "base %d, max %d, strand %d\n", seq->genome_coordinate.base,seq->genome_coordinate.max,seq->genome_coordinate.strand);
        fprintf(stderr, "seqlen %d\n", seq->seqlen);
        fprintf(stderr, "maplen %d\n", seq->maplen);
        fprintf(stderr, "seq %.*s\n", seq->seqlen, seq->seq);
        seqMap_free(&seq);
    }
    return 0;
}
