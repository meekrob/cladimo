#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "combo_lib.h"
#include "seq.h"
#include "pwm.h"
#include "matrix.h"
#define letterval(a) dna_encoding[a]

float** _ICfunc_(PWM *pwm, int use_pseudocounts);
void print_count_matrix(float **count_matrix, int pwmlen, int as_ints);
void print_counts(PWM *pwm);
extern const signed char dna_encoding[256];
void seq_freqs(unsigned char *seq, float *arr)
{
    int i;
    bzero(arr, 4*sizeof(float));
    for (i=0; i< strlen((char *)seq); i++)
    {
        int c = seq[i];
        arr[letterval(c)] += 1; 
    }
}

int main(int argc, char **argv)
{
    _pwm_loop_
    float sum = 0;

    char **arglist = NULL;
    if (argc > 1) arglist = &(argv[1]);
    else
    {
        printf("%s accession\n", argv[0]);
    }
    struct combo_lib *runlib = parse_arg_list(arglist, 1);


    PWM *pwm = runlib->pwm_library[0];
    printf("label %s\n", pwm->label);
    printf("ic %.3f\n", pwm->information_content);
    printf("Nsites %d\n", pwm->Nsites);
    printf("base counts %.3f %.3f %.3f %.3f\n", 
            pwm->base_counts[0], 
            pwm->base_counts[1], 
            pwm->base_counts[2], 
            pwm->base_counts[3]);

    printf("base pseudocounts %.3f %.3f %.3f %.3f\n", 
            pwm->base_pseudocounts[0], 
            pwm->base_pseudocounts[1], 
            pwm->base_pseudocounts[2], 
            pwm->base_pseudocounts[3]);

    // normal
    printf("normal:\n");
    float **ICmx = _ICfunc_(pwm, 0);
    printf("IC on counts:\n");
    print_count_matrix(ICmx, pwm->length, 0);

    sum = 0;
    printf("IC   ");
    for_each_position( pwm->length )
    {
        printf("%5.2f ", ICmx[4][position]);
        sum += ICmx[4][position];
    }
    printf("IC total = %.3f\n", sum);
    free_matrix(&ICmx);

    // with pseudocounts
    ICmx = _ICfunc_(pwm, 1);
    printf("IC with pseudocounts:\n");
    print_count_matrix(ICmx, pwm->length, 0);

    sum = 0;
    printf("IC   ");
    for_each_position( pwm->length )
    {
        printf("%5.2f ", ICmx[4][position]);
        sum += ICmx[4][position];
    }
    printf("IC total = %.3f\n", sum);
    free_matrix(&ICmx);

    free_combo_lib(&runlib);
    return 0;
}

void print_count_matrix(float **count_matrix, int pwmlen, int as_ints)
{
    _pwm_loop_
    char nt[] = "ACGT";
    for_each_base
    {
        printf("%4c ", nt[base]);
        for_each_position( pwmlen )
        {
            if (as_ints)
                printf("% 5d ", (int)count_matrix[base][position]);
            else
                printf("%5.2f ", count_matrix[base][position]);
        }
        printf("\n");
    }
}
void print_counts(PWM *pwm)
{
    float **cmx = pwm_counts(pwm);
    print_count_matrix(cmx, pwm->length, 1);
    free_matrix(&cmx);
}
float __Ufunc(float P)
{
    if (P==0) return 0;
    return -P * log2f(P);
}
float** _ICfunc_(PWM *pwm, int use_pseudocounts)
{
    _pwm_loop_
    float H_background = 2;
    float **H_matrix = zeroes(5, pwm->length);
    float **freq_matrix = zeroes(5, pwm->length);
    float **count_matrix = pwm_counts(pwm);
    float Nsites = pwm->Nsites;
    float pseudos[] = {0.01, 0.01, 0.01, 0.01};
    printf("new pseudocounts: %.2f %.2f %.2f %.2f\n", pseudos[0],pseudos[1],pseudos[2],pseudos[3]);
    if (use_pseudocounts)
    {
        //Nsites += array_sum(pwm->base_pseudocounts,4);
        Nsites += array_sum(pseudos,4);
    }

    for_each_position( pwm->length )
    {
        float H = 0.;
        for_each_base
        {
            if (use_pseudocounts) 
            {
                //count_matrix[base][position] += pwm->base_pseudocounts[base];
                count_matrix[base][position] += pseudos[base];
            }
            float P = count_matrix[base][position] / Nsites;
            freq_matrix[base][position] = P;
            H_matrix[base][position] = fabs( (H_background-__Ufunc(P)) * P);
            H += __Ufunc(P);

        }
        H_matrix[4][position] = H_background - H;
    }
    printf("count matrix:\n");
    print_count_matrix(count_matrix, pwm->length, 0);
    printf("frequency matrix:\n");
    print_count_matrix(freq_matrix, pwm->length, 0);
    free_matrix(&count_matrix);
    free_matrix(&freq_matrix);
    return H_matrix;
}

