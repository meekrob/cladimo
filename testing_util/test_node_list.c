#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "common_names.h"
#include "strsep_all.h"
#include "genomeFeature.h"
#include "append_to_array.h" 
/* 



An object of class hclust which describes the tree produced by the clustering process. The object is a list with components:

merge    an n-1 by 2 matrix. Row i of merge describes the merging of clusters at step i of the clustering. If an element j in the row is negative, then observation -j was merged at this stage. If j is positive then the merge was with the cluster formed at the (earlier) stage j of the algorithm. Thus negative entries in merge indicate agglomerations of singletons, and positive entries indicate agglomerations of non-singletons.
height   a set of n-1 non-decreasing real values. The clustering height: that is, the value of the criterion associated with the clustering method for the particular agglomeration.
order    a vector giving the permutation of the original observations suitable for plotting, in the sense that a cluster plot using this ordering and matrix merge will not have crossings of the branches.
labels   labels for each of the objects being clustered.
call     the call which produced the result.
method   the cluster method that has been used.
dist.method  the distance that has been used to create d (only returned if the distance object has a "method" attribute).
 */

int node_count(struct btree *tree)
{
    if (is_leaf(tree)) 
        return 0;
    else
        return 1 + node_count(tree->left) + node_count(tree->right);
}

float set_weights_as_treesum(struct btree *tree)
{
    if (tree == NULL) return 0;
    if (is_leaf(tree))
        return tree->node_weight; // this is zero until set by the parent recursive call

    float current_node_weight = 0;
    // leaf-parent calls: set the leaf weight with the branch length
    if (is_leaf(tree->left)) 
    {
        tree->left->node_weight = tree->left_edge;
        current_node_weight += tree->left_edge;
    }
    else
        current_node_weight += tree->left_edge + set_weights_as_treesum(tree->left);
    if (is_leaf(tree->right)) 
    {
        tree->right->node_weight = tree->right_edge;
        current_node_weight += tree->right_edge;
    }
    else
        current_node_weight += tree->right_edge + set_weights_as_treesum(tree->right);

    tree->node_weight = current_node_weight;
    return tree->node_weight;
}

char **leaf_names(struct btree *tree, int *nnames)
{
    char **leafnames = NULL;
    if (is_leaf(tree))
    {
        *nnames = 1;
        leafnames = malloc(sizeof*leafnames);
        leafnames[0] = strdup(tree->name);
        return leafnames;
    }

    int nleft = 0;
    char **left_list = leaf_names(tree->left, &nleft);
    int nright = 0;
    char **right_list = leaf_names(tree->right, &nright);

    leafnames = malloc(sizeof(char*) * (nleft+nright));
    memcpy(leafnames, left_list, sizeof(char*) * nleft);
    memcpy(&(leafnames[nleft]), right_list, sizeof(char*) * nright);
    *nnames = nleft + nright;
    free(left_list);
    free(right_list);
    return leafnames;
}

struct btree **node_list(struct btree *tree, int *nnodes)
{
    struct btree **list = malloc(sizeof(struct btree*));
    *list = tree;

    if (is_leaf(tree))
    {
        *nnodes = 1;
        return list;
    }

    int nleft = 0;
    struct btree **left_list = node_list(tree->left, &nleft);
    int nright = 0;
    struct btree **right_list = node_list(tree->right, &nright);

    list = realloc(list, sizeof(struct btree*) * (1+nleft+nright));
    memcpy(&(list[1]), left_list, sizeof(struct btree*) * nleft);
    memcpy(&(list[1+nleft]), right_list, sizeof(struct btree*) * nright);
    *nnodes = 1 + nleft + nright;
    free(left_list);
    free(right_list);
    return list;
}

struct nodeval
{
    struct btree *nodeptr;
    float height;
    int rank;
};

int nodevalcmp(const void *a, const void *b)
{
    struct nodeval *x = (struct nodeval *)a;
    struct nodeval *y = (struct nodeval *)b;
    if (x->height == y->height) return 0;
    return x->height < y->height ? -1 : 1;
}

int compar(const void *a, const void *b)
{
    char **x = (char **)a;
    char **y = (char **)b;
    return strcmp(*x,*y);
}

int indexof(char *hunt, char **list, int nitems)
{
    for (int i=0; i< nitems; i++)
        if (0 == strcmp(list[i], hunt)) return i;

    return -1;
}

void printdelim(int i)
{
    if(i > 1) 
        printf(",");
    /*if(i % 8 == 0) 
        printf("\n");
    else */
        printf(" ");
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stdout, "newick2hclust - create an R representation of a newick tree using the \"hclust\" class.\n");
        fprintf(stdout, "Usage: %s treefile.nh > treestructure.R\n", argv[0]);
        return 1;
    }
    struct btree *tree = readTreeFile(argv[1]);

    int nnames = 0;
    char **names = leaf_names(tree, &nnames); 
    char **names2 = leaf_names(tree, &nnames);

    qsort(names, nnames, sizeof(char*), compar);

    set_weights_as_treesum(tree);
    int nnodes = node_count(tree);
    int listn = 0;
    struct btree** nodelist = node_list(tree, &listn);

    struct nodeval * nodevals = calloc(sizeof(struct nodeval), listn);
    for (int i=0; i<listn; i++)
    {
        nodevals[i].nodeptr = nodelist[i];
        nodevals[i].height = nodevals[i].nodeptr->node_weight;
    }

    qsort(nodevals, listn, sizeof(struct nodeval), nodevalcmp);
    int nodenum = 0;
    for (int i=0; i<listn; i++)
    {
        if (!is_leaf(nodevals[i].nodeptr)) 
        {
            nodenum++;
            nodevals[i].rank = nodenum;
            nodevals[i].nodeptr->node_weight = nodenum;
        }
        else
        {
            nodevals[i].nodeptr->node_weight = -(1 + indexof(nodevals[i].nodeptr->name, names, nnames));
        }
    }

    printf("`tree` <-\n");
    /* merge. See the help doc for hclust. */
    printf("structure(list(merge = structure(c(\n");
    for (int i=0; i<listn; i++) // left column
    {
        if (!is_leaf(nodevals[i].nodeptr)) 
        {
            printdelim(nodevals[i].rank);
            printf("% dL", (int)nodevals[i].nodeptr->left->node_weight); 
        }
    }
    printf(", ");
    for (int i=0; i<listn; i++) // right column
    {
        if (!is_leaf(nodevals[i].nodeptr)) 
        {
            printdelim(nodevals[i].rank);
            printf("% dL", (int)nodevals[i].nodeptr->right->node_weight); 
        }
    }
    printf("), .Dim = c(%dL, 2L)), ", nnodes);

    /* height */
    printf("\nheight = c(\n");
    for (int i=0; i<listn; i++)
    {
        if (!is_leaf(nodevals[i].nodeptr)) 
        {
            printdelim(nodevals[i].rank);
            printf("%f", nodevals[i].height);
        }
    }
    printf("),\n");

    /* order */
    printf(" order = c(\n");
    for (int i=0; i < nnames; i++)
    {
        printdelim(i+1);
        int order = indexof(names2[i], names, nnames) + 1;
        printf("%d", order);
    }
    printf("),\n");

    /* labels */
    printf(" labels = c(\n");
    for (int i=0; i < nnames; i++)
    {
        printdelim(i+1);
        printf("\"%s\"", names[i]);
        free(names[i]);
        free(names2[i]);
    }
    printf(")),\n");
    printf(" .Names = c(\"merge\", \"height\", \"order\", \"labels\"), class = \"hclust\")\n");
    free(names);
    free(names2);
    free(nodevals);
    free(nodelist);
    depth_first_free(&tree);
    return 0;
}
