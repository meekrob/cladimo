#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "strsep_all.h"
static int indexof(char *hunt, char **list, int nitems)
{
    for (int i=0; i< nitems; i++)
        if (0 == strcmp(list[i], hunt)) return i;

    return -1;
}

float longest_path(struct btree *tree)
{
    if (tree == NULL) return 0;
    float left = tree->left_edge + longest_path(tree->left);
    float right = tree->right_edge + longest_path(tree->right);
    return left > right ? left : right;
}

float midpoint(struct btree *tree, char **order, int nleafs);
float leftpoint(struct btree *tree, char **order, int nleafs)
{
    if (is_leaf(tree)) abort();
    if (is_leaf(tree->left)) 
        return indexof(tree->left->name, order, nleafs) + 1;
    else
        return midpoint(tree->left, order, nleafs);
}
float rightpoint(struct btree *tree, char **order, int nleafs)
{
    if (is_leaf(tree)) abort();
    if (is_leaf(tree->right)) 
        return indexof(tree->right->name, order, nleafs) + 1;
    else
        return midpoint(tree->right, order, nleafs);
}
float midpoint(struct btree *tree, char **order, int nleafs)
{
    if(is_leaf(tree))
        return indexof(tree->name, order, nleafs) + 1;
    else
        return (leftpoint(tree, order, nleafs) + rightpoint(tree, order, nleafs)) / 2;
}


int leftmost(struct btree *tree, char ** order, int nleafs)
{
    if (is_leaf(tree)) return indexof(tree->name, order, nleafs);
    else
        return leftmost(tree->left, order, nleafs);
}
int rightmost(struct btree *tree, char ** order, int nleafs)
{
    if (is_leaf(tree)) return indexof(tree->name, order, nleafs);
    else
        return rightmost(tree->right, order, nleafs);
}

int breadth(struct btree *tree, char ** order, int nleafs)
{
    return rightmost(tree, order, nleafs) - leftmost(tree, order, nleafs);
}

float midpointf(int x)
{
    float midpoint = 1.5;
    for (int i=2; i <= x; i++)
    {
        midpoint = (midpoint + i)/2;
    }
    return midpoint;
}

float midpointf2(struct btree *tree, char ** order, int nleafs)
{
    if(is_leaf(tree))
    {
        return indexof(tree->name, order, nleafs);
    }
    return (midpointf2(tree->left, order, nleafs) + midpointf2(tree->right, order, nleafs)) / 2;
}

float print_dendrogram(FILE *out, float parent_height, float height, struct btree *tree, char **order, int nleafs)
{ // return the density below

    if(is_leaf(tree))
    {
        int num = indexof(tree->name, order, nleafs) + 1;
        //fprintf(out, "structure(%dL, label = \"%s.%d\", members = 1L, height = %f, leaf = TRUE)\n", num, tree->name, num, height);
        fprintf(out, "structure(%dL, label = \"%s\", members = 1L, height = %f, leaf = TRUE)\n", num, tree->name, height);
        return 1;
    }

    //else, a node
    fprintf(out, "structure(");

    // branches
    fprintf(out, "list(");
    int nleft = print_dendrogram(out, height, height - tree->left_edge, tree->left, order, nleafs); 
    fprintf(out, ",");
    int nright = print_dendrogram(out, height, height - tree->right_edge, tree->right, order, nleafs);
    fprintf(out, "),");

    /* end structure() with node information/fields */
    float midpt = midpoint(tree, order, nleafs);
    float leftpt = leftmost(tree, order, nleafs) + 1;
    //fprintf(out, " edgetext = \"%.2f-%.2f=%.2f\",", midpt, leftpt, midpt - leftpt);
    //fprintf(out, " edgetext = \"%.3f\",", parent_height - height);
    fprintf(out, " members = %dL,", (nleft+nright));
    fprintf(out, " midpoint = %.3f,", midpt - leftpt);
    fprintf(out, " height = %.4f,", height);
    fprintf(out, " class = \"dendrogram\")\n");
    return nleft + nright;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stdout, "%s - create an R dendrogram from a newick tree.\n", argv[0]);
        fprintf(stdout, "Usage: %s tree.nh > tree.R\n", argv[0]);
        return 1;
    }
    struct btree *tree = readTreeFile(argv[1]);

    int nnames = 0;
    char **names = leaf_names(tree, &nnames); 
    float max_height = longest_path(tree);

    fprintf(stdout, "`tree` <- \n");
    print_dendrogram(stdout, max_height, max_height, tree, names, nnames);
    depth_first_free(&tree);
    for (int i=0; i < nnames; i++)
    {
        //fprintf(stderr, "%d: %s\n", i, names[i]);
        free(names[i]);
    }
    free(names);
}
