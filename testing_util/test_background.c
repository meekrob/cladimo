#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "combo_lib.h"
#include "seq.h"
#include "pwm.h"
#include "matrix.h"
#define letterval(a) dna_encoding[a]

float Ufunc(float P);
void print_count_matrix(float **count_matrix, int pwmlen, int as_ints);
void print_counts(PWM *pwm);
extern const signed char dna_encoding[256];
void seq_freqs(unsigned char *seq, float *arr)
{
    int i;
    bzero(arr, 4*sizeof(float));
    for (i=0; i< strlen((char *)seq); i++)
    {
        int c = seq[i];
        arr[letterval(c)] += 1; 
    }
}

void print_matrices(PWM *pwm)
{
    _pwm_loop_
    printf("original:\n");
    print_counts(pwm);

    float **freq = pwm_pmatrix(pwm);
    printf("frequencies:\n");
    print_count_matrix(freq, pwm->length, 0);
    free_matrix(&freq);

    printf("scores:\n");
    print_pwm(stdout, pwm);

    float **ICmx = ICfunc(pwm);
    printf("Information content:\n");
    print_count_matrix(ICmx, pwm->length, 0);

    float sum = 0;
    printf("IC   ");
    for_each_position( pwm->length )
    {
        printf("%5.2f ", ICmx[4][position]);
        sum += ICmx[4][position];
    }
    printf("IC total = %.3f\n", sum);
    free_matrix(&ICmx);
}

int main(int argc, char **argv)
{
    _pwm_loop_

    float counts[4];
    char **arglist = NULL;
    if (argc > 2) arglist = &(argv[2]);
    struct combo_lib *runlib = parse_arg_list(arglist, 1);

    PWM *pwm = runlib->pwm_library[0];
    printf("label %s\n", pwm->label);
    printf("ic %.3f\n", pwm->information_content);
    printf("Nsites %d\n", pwm->Nsites);
    printf("base counts %.3f %.3f %.3f %.3f\n", 
            pwm->base_counts[0], 
            pwm->base_counts[1], 
            pwm->base_counts[2], 
            pwm->base_counts[3]);

    printf("base pseudocounts %.3f %.3f %.3f %.3f\n", 
            pwm->base_pseudocounts[0], 
            pwm->base_pseudocounts[1], 
            pwm->base_pseudocounts[2], 
            pwm->base_pseudocounts[3]);

    // as-is
    print_matrices(pwm);

    // get background from sequence
    SEQ *seqq = seq_open(argv[1]);
    seq_read(seqq);
    uchar* seq= SEQ_CHARS(seqq);
    int seqlen = strlen((char*)seq);

    seq_freqs(seq, counts);
    fprintf(stdout, "%.1f %.1f %.1f %.1f\n", counts[0], counts[1], counts[2], counts[3]);

    // transform to probs
    for_each_base counts[base] = counts[base]/seqlen;
    fprintf(stdout, "%.3f %.3f %.3f %.3f\n", counts[0], counts[1], counts[2], counts[3]);

    pwm_retransform(pwm, counts[0], counts[1], counts[2], counts[3]);
    print_matrices(pwm);

    free_combo_lib(&runlib);
    seq_close(seqq);
    return 0;
}

void print_count_matrix(float **count_matrix, int pwmlen, int as_ints)
{
    _pwm_loop_
    char nt[] = "ACGT";
    for_each_base
    {
        printf("%4c ", nt[base]);
        for_each_position( pwmlen )
        {
            if (as_ints)
                printf("% 5d ", (int)count_matrix[base][position]);
            else
                printf("%5.2f ", count_matrix[base][position]);
        }
        printf("\n");
    }
}
void print_counts(PWM *pwm)
{
    float **cmx = pwm_counts(pwm);
    print_count_matrix(cmx, pwm->length, 1);
    free_matrix(&cmx);
}

