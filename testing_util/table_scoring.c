#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pwm.h"
#include "library.h"
#include "readers.h"

#include "maf.h"
#include "seq.h"
#include "mafchop.h"

#include "cladistic_motif_printing.h"
#include "combo_lib.h"
#define MAXWORDSIZE 32

struct bset
{
    unsigned char *set;
    int len;
    int nbytes;
};
void free_bset(struct bset **bb)
{
    struct bset *b  = *bb;
    free(b->set);
    free(b);
    *bb = NULL;
}
unsigned char getbit(struct bset *b, int val)
{
    int base = val / 8;
    unsigned char offset = val % 8;
    return (b->set[base] & (1<<offset)) != 0;
}
void printbyte(FILE *out, unsigned char byte)
{
    int i;
    for(i=0; i<8; i++) 
    {
        fprintf(out, "%d", byte & 1);
        byte >>= 1;
    }
}
int sumbyte(unsigned char byte)
{
    int i;
    int sum=0;
    for(i=0; i<8; i++) 
    {
        sum += byte & 1;
        byte >>= 1;
    }
    return sum;
}
int sum_bset(struct bset *b)
{
    int i;
    int sum = 0;
    for (i=0; i< b->nbytes; i++)
        sum += sumbyte(b->set[i]);
    return sum;
}
void setbit(struct bset *b, int val)
{
    int base = val / 8;
    unsigned char offset = val % 8;
    b->set[base] |= (1<<offset);
}
struct bset* new_bset(int maxval)
{
    int nbytes = maxval/8 + 1;
    struct bset *b = malloc(sizeof(struct bset));
    b->set = calloc(nbytes, sizeof(unsigned char));
    b->len = maxval;
    b->nbytes = nbytes;
    return b;
}
int main(int argc, char **argv)
{
    int i,j;
    SEQ *seqq = NULL;
    seqq = seq_open(argv[1]);
    seq_read(seqq);
    uchar* seq= SEQ_CHARS(seqq);
    int seqlen = strlen((char*)seq);
    char *contig_name = seqq->fname;
    if (rindex(seqq->fname, '/')!=NULL)
        contig_name = rindex(seqq->fname, '/') +1;

    int contig_trunc_point = dotplace(contig_name);
    if (strcmp(&(contig_name[contig_trunc_point]), ".nib") !=0 )
    {
        contig_trunc_point = strlen(contig_name);
    }
    struct combo_lib *runlib = parse_arg_list(&(argv[2]), argc-2);

    int tablewidth = 6;
    unsigned char *deparse_buf = calloc(tablewidth + 1, sizeof(char));

    int tablelength = 1 << (tablewidth * 2);
    int mask = tablelength - 1;
    int setsize = runlib->total_n;
    struct bset **table = calloc(tablelength, sizeof(struct bset*));
    fprintf(stderr, "tablewidth %d tablelength %d setsize %d\n", tablewidth, tablelength, setsize);
    //initialize table
    for (i=0; i < tablelength; i++)
        table[i] = new_bset(setsize);

    int pwmi;
    for (pwmi=0; pwmi < runlib->total_n; pwmi++)
    //for (pwmi=0; pwmi < 3; pwmi++)
    {
        PWM *pwm = runlib->pwm_library[pwmi];
        // use the table as a score index
        if (pwm->length <= tablewidth)
        {
            for (i=0; i < tablelength; i++) 
            {
                int submask = (1 << (pwm->length * 2)) - 1;
                int subseq = i & submask;
                float match = numeric_wordscore(subseq, pwm);
                if (fabs(match) >= pwm->threshold)
                    setbit(table[i], pwmi);
            }
            continue;
        }

        // flag passable primer-pwm combinations
        int base_position = pwm->length - tablewidth;
        float score_left = max_score_left(pwm, base_position);
        float score_right = max_score_right(pwm, tablewidth-1);

        for (i=0; i < tablelength; i++)
        {
            int word = i;
            float fscore = 0;
            float rscore = 0;
            for (j=0; j<tablewidth; j++)
            {
                fscore += pwm->matrix[word & 3][pwm->length - (j+1)];
                rscore += pwm->matrix[3 - (word&3)][j];
                word >>= 2;
            }
            float max_fscore = ((fscore + score_left) - pwm->min_score) / (pwm->max_score-pwm->min_score);
            float max_rscore = ((rscore + score_right) - pwm->min_score) / (pwm->max_score-pwm->min_score);
            deparse(deparse_buf, i, tablewidth);
            if ((max_fscore >= pwm->threshold) || (max_rscore >= pwm->threshold))
            {
                setbit(table[i], pwmi);
            }

        }
    }

    long nmatches = 0;
    long nonmatches = 0; // to keep track of the number of times a PWM was used to score a primed sequence and did not pass the threshold
    long num_ops = 0;
    long total_ops = 0;
    int dna_word_index = 0;
    for(i=0; i < seqlen; i++)
    {
        if (! validchar(seq[i])) continue;
        dna_word_index = dnashiftone((unsigned char)seq[i], dna_word_index, MAXWORDSIZE);
        if (i <= tablewidth) continue;
        int primer = dna_word_index & mask;

        int ix;
        float match = 0;
        for (ix=0; ix<runlib->total_n; ix++)
        {
            total_ops += 1;

            PWM *pwm = runlib->pwm_library[ix];
            int setposition = i - (pwm->length - 1);
            if (setposition < 0) continue;

            int prematch = getbit(table[primer], ix);
            if (pwm->length <= tablewidth)//use the truth value as the match score
            {
                match = (float)prematch;
                nmatches += prematch;
            }
            else if (prematch)// score the sequence
            {
                num_ops++;
                match = numeric_wordscore(dna_word_index, pwm);
                if (fabs(match) >= pwm->threshold)
                    nmatches++;
                else
                    nonmatches++;
            }
            /*if (fabs(match) >= pwm->threshold)
            {
                printf("%.*s %d %d ", contig_trunc_point, contig_name, setposition, setposition + pwm->length);
                // as in cladistic_motif_printing
                if (strlen(pwm->accession) != 0)
                    printf("%.*s_%s", (int)(index(pwm->name, ' ') - pwm->name), pwm->name, pwm->accession);
                else
                    printf("%s", pwm->motif);

                printf(" %.*s %.3f\n", contig_trunc_point, contig_name, match); // substitutes for species_list
            }*/
            
        }
    }
    seq_close(seqq);
    fprintf(stdout, "nonmatches %ld\n", nonmatches);
    fprintf(stdout, "nmatches %ld\n", nmatches);
    fprintf(stdout, "nonmatches + nmatches %ld\n", nonmatches + nmatches);
    fprintf(stdout, "num_ops %ld\n", num_ops);
    fprintf(stdout, "matches / num_ops %.3lf\n", (double)(nmatches) / num_ops);


    fprintf(stdout, "total_ops %ld\n", total_ops);
    fprintf(stdout, "num_ops / total_ops %.3f\n", (float)num_ops/total_ops);
    for (i=0; i < tablelength; i++) free_bset(&(table[i]));        
    free(table);
    free(deparse_buf);
    free_combo_lib(&runlib);
    return 0;
}
