#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "maf.h"
#include "seq.h"

int is_maf(FILE* in)
{
    int i, is_a_maf = 0;
    char maf_header[] = "##maf version=";
    int bufsiz = strlen(maf_header);
    char *buf = calloc(bufsiz, sizeof(char));
    size_t nread = 0;
    nread = fread(buf,sizeof(char), bufsiz, in);
    if (strcmp(maf_header, buf)==0) is_a_maf = 1;
    for (i=bufsiz-1; i >= 0; i--) ungetc(buf[i], in);
    return is_a_maf;
}

int main(int argc, char **argv)
{
    struct mafFile *mf = NULL;
    SEQ *seqq = NULL;
    FILE* testfile = fopen(argv[1], "r");

    if (is_maf(testfile))
        mf = mafOpenFd(testfile, 0);
    else
        seqq = seq_open_fd(testfile);

    if (seqq!=NULL)
    {
        seq_read(seqq);
        uchar* seq= SEQ_CHARS(seqq);
        int seqlen = strlen((char*)seq);
        printf("read the seq\n");
        if (seqq) seq_close(seqq);
        
    }
    else
    {
        printf("mf=%p\n", mf);
        printf("read the maf\n");
        if (mf) mafFileFree(&mf);
    }
    return 0;
}
