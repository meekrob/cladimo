#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pwm.h"
#include "seq.h"
#define MAXWORDSIZE 32

int main(int argc, char **argv)
{
    PWM *pwm = new_pwm();
    strcpy(pwm->motif, "WGATAR");
    int motiflen = strlen(pwm->motif);
    pwm->length = motiflen;
    pwm->index = build_index(pwm);
    //char *seq = argv[1];
    SEQ *seqq;
    seqq = seq_open(argv[1]);
    seq_read(seqq);
    uchar* seq= SEQ_CHARS(seqq);
    int seqlen = strlen((char*)seq);

    int dna_word_index = 0;
    int i;
    for(i=0; i < seqlen; i++)
    {
        dna_word_index = dnashiftone((unsigned char)seq[i], dna_word_index, MAXWORDSIZE);
        int setposition = i - (motiflen - 1);
        if (setposition >= 0)
        {
            float match = numeric_wordscore(dna_word_index, pwm);
            if (match != 0)
            {
                printf("%.*s %d %d %.2f\n", motiflen, &(seq[setposition]), setposition, i, match);
            }
        }
    }
    free_pwm(&pwm);
    seq_close(seqq);
    return 0;
}
