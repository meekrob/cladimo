#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common_names.h"

int main(int argc, char **argv)
{
    char *info = calloc(infolen+1, sizeof(char));
    strcpy(info, rename_assembly_source(buf));
    init_assembly_names();
    printf("%s->%s\n", argv[1], translate_assembly_name(argv[1], -1));
    return 0;
}
