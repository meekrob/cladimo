#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pwm.h"
#include "matrix.h"
#include "math.h"
#include "readers.h"
#include "linesplit.h"
float *ICfunc_original(PWM *pwm);
float *partials(PWM *pwm);
float *max_score_left(PWM *pwm);
void print_points(FILE* out, PWM *pwm)
{
    int i,j;
    int nrows=4;
    char nt[] = {'A','C','G','T'};
    if (pwm->motif[0] != 0)
    {
        fprintf(out,"%4c ", ' ');
        for (i=0; i<pwm->length; i++) 
            fprintf(out,"%5c ",pwm->motif[i]);
        fputc('\n',out);
    }
    for (i=0; i<nrows; i++) 
    {
        fprintf(out,"%4c ",nt[i]);
        for(j=0; j< pwm->length; j++)
            //fprintf(out, "% 4.2f ", (pwm->matrix[i][j] - pwm->min_score) / (pwm->max_score - pwm->min_score) );
            fprintf(out, "% 4.2f ", pwm->matrix[i][j]);

        fputc('\n',out);
    }
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "%s filename\n", argv[0]);
        return 1;
    }
    int filevar=1;
    for (; filevar < argc; filevar++)
    {
        int npwm=0;
        PWM **pwmlist = read_pwm_file(argv[filevar], &npwm);
        fprintf(stdout, "npwm %d\n", npwm);
        int i;
        for(i=0; i<npwm; i++)
        {
            PWM *pwm = pwmlist[i];
            fprintf(stdout, "accession %s\n", pwm->accession);
            fprintf(stdout, "name %s\n", pwm->name);
            fprintf(stdout, "label %s\n", pwm->label);
            fprintf(stdout, "consensus motif %s\n", pwm->motif);
            fprintf(stdout, "pwm->min_score %.3f\n", pwm->min_score);
            fprintf(stdout, "pwm->max_score %.3f\n", pwm->max_score);
            print_points(stdout, pwm);

            // IC
            fprintf(stdout,"%5s%.*s\n", " ", pwm->length * 6, "_______________________________________________________________________________________");
            fprintf(stdout, "%5s", "part");
            float *plist = max_score_left(pwm);
            int k = 0;
            while (k < pwm->length) fprintf(stdout, "% 4.2f ", plist[k++]);
            fputc('\n', stdout);

            int word = 0;
            int width = 3;
            int nwords = 1 << 2*width;
            for (; word < nwords;word++)
            {
                int j;
                float score = 0;
                int position = word;
                for(j=0;j<width;j++)
                {
                    score += pwm->matrix[position & 3][pwm->length - j - 1];
                    position >>= 2;
                }
                unsigned char buf[4];
                deparse(buf, word, width);
                float mx_pos = (score + plist[pwm->length-width] - pwm->min_score) / (pwm->max_score-pwm->min_score);
                if (mx_pos < pwm->threshold) fprintf(stdout, "%s max possible %.3f\n", buf, mx_pos);
            }
            free_pwm(&pwm);
        }
        free(pwmlist);
    }
    return 0;
}

float Ufunc(float P)
{
    if (P==0) return 0;
    return -P * log2f(P);
}
float uncertainty(float *P, int len)
{
    int i = 0;
    float H = 0;
    while (i<len) H += Ufunc(P[i++]);
    return H;
}
float *ICfunc_original(PWM *pwm)
{
    int i,column;
    //float H_background = uncertainty(pwm->background, 4);
    float H_background = 2;
    float *H_result = calloc(pwm->length, sizeof(float));
    float **count_matrix = pwm_untransform(pwm);
    float correction = 0;
    for (column=0; column < pwm->length; column++)
    {
        float H = 0.;
        for (i=0; i<4; i++)
        {
            float P = count_matrix[i][column] / (pwm->Nsites+correction);
            H += Ufunc(P);
        }
        H_result[column] = H_background - H;
    }
    free_matrix(&count_matrix);
    return H_result;
}
