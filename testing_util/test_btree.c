int main(int argc, char **argv)
{
    float branchlength = 0;
    extern char assembly_tree_44way[];
    if (strlen(assembly_tree_44way) > SENTENCE)
    {
        fprintf(stdout, "%ld is too long\n", strlen(assembly_tree_44way));
        return 0;
    }
    struct btree *tree = parse_nh(assembly_tree_44way);
    extern int nodenum;
    nodenum = 0;
    init_assembly_names();
    print_tree(tree,0, 0);

    struct weight targets[] = {{ "hg18", 0.89 }, {"panTro2", 0.85}, {"mm9", 0.95}, {"rn4", 1.0}};
    int ntargets = sizeof(targets) / sizeof(targets[0]);
    qsort(targets, ntargets, sizeof(struct weight), weightcmp);

    char **subs = calloc(ntargets, sizeof(char*));
    int i;
    for (i=0; i < ntargets; i++) subs[i] = targets[i].name;
    qsort(subs, ntargets, sizeof(char*), cmp);
    struct btree *sub = subtree(tree, subs, ntargets, &branchlength);

    float score = 0;
    branchlength = 0;
    struct btree *score_tree = weighted_branchsum(sub, 0, targets, ntargets, &branchlength, &score);
    print_tree(score_tree, score, 0);

    fprintf(stdout, "(");
    print_nh(stdout, score_tree, score);
    fprintf(stdout, ");");
    printf("\n");
    depth_first_free(&tree);
    depth_first_free(&sub);
    depth_first_free(&score_tree);
    free(subs);
    return 0;
}

