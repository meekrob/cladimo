#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pwm.h"
#define letterval(a) dna_encoding[a]
extern const signed char dna_encoding[256];

long long lldnashiftone(unsigned char letter, long long code)
{
    return (code << 2) | letterval(letter);
}
void ll_deparse(unsigned char *buf, long long index, int windowsize)
{
    unsigned char letters[] = "ACGT";
    int i;
    buf[windowsize] = 0;
    for (i=0; i<windowsize; i++)
    {
        buf[windowsize-(i+1)] = letters[ index & 3 ];
        index >>= 2;
    }
}

int main(int argc, char **argv)
{
    long long word = 0;
    unsigned char buf[32 + 1];
    printf("sizeof(word) = %d\n", sizeof(long long));

    int buflen = sizeof(buf) / sizeof(buf[0]);
    printf("buf[%d]\n", buflen);

    char *sequence = argv[1];
    int seqlen = strlen(sequence);
    printf("sequence %s (%d)\n", sequence, seqlen);

    int i;
    for (i=0; i< seqlen; i++)
    {
        word = lldnashiftone(sequence[i], word);
        printf("word %lld \n", word);
    }
    ll_deparse(buf, word, seqlen);
    printf("buf %.*s\n", buflen-1, buf);
    printf("correct? %d\n", strncmp(sequence,buf, seqlen)==0);
    
    return 0;
}
