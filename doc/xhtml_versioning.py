#!/usr/bin/env python2.5
import sys, time

timestamp = 'Created on ' + time.strftime("%B %d, %G", time.localtime())
release_version = sys.argv[2]

if 0:
    from xml.etree.cElementTree import ElementTree, iterparse, XMLTreeBuilder
    ns = '{http://www.w3.org/1999/xhtml}'
    xhtml = ElementTree()
    xhtml.parse( open( sys.argv[1] ) )
    for tag in xhtml.getiterator():
        if tag.tag == ns + 'span' and 'class' in tag.attrib: 
                if tag.attrib['class'] == 'left_footer':
                    tag.text = tag.text.replace('RELEASE_VERSION', 'version ' + release_version)
                elif tag.attrib['class'] == 'center_footer':
                    #tag.text = tag.text.replace('DATE', 'Created on ' + time.strftime("%B %d, %G", time.localtime()))
                    tag.text = tag.text.replace('DATE', timestamp)

    xhtml.write(sys.stdout)

if 1:
    buf = file(sys.argv[1]).read()
    buf = buf.replace('RELEASE_VERSION', release_version)    
    buf = buf.replace('DATE', timestamp)
    print buf
