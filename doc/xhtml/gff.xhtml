<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>GFF format</title>
<link rel="stylesheet" type="text/css" href="man.css" />
</head>
<body>
<div class="TH">
<span class="name">GFF 1</span>
<span class="center_footer">Created on February 18, 2014</span>
<span class="left_footer">1.0</span>
<span class="center_header">File formats for genomic data</span>
</div>

<div class="SH">
<h2>NAME</h2> GFF - (General Feature Format) Specifications
</div>
<div class="SH">
<h2>SYNOPSIS</h2>
<b class="spaced">chrom</b>t<b class="spaced">pname</b>t<b class="spaced">fname</b>t<b class="spaced">start</b>t<b class="spaced">end</b>t<b class="spaced">score</b>t<b class="spaced">strand</b>t<b class="spaced">.</b>t<b class="spaced">[attribute_list]</b>
<p>
The genome coordinates are 1-based, meaning that the lowest value of <b>start</b> is 1. <br/>
All fields are <b>tab-delimited (t),</b> except for the <b>attribute_list,</b> which is a <b>semicolon (;)-delimited</b> list of field/value pairs. 
</p>
</div>
<div class="SH">
<h2>Introduction</h2>
<p>
Essentially all current approaches to feature finding in higher organisms
use a variety of recognition methods that give scores to likely
signals (starts, splice sites, stops, motifs, etc.) or to extended regions
(exons, introns, protein domains etc.), and then combine these to give complete gene,
RNA transcript or protein structures.  Normally the combination step is done in the 
same program as the feature detection, often using dynamic programming methods.  To enable 
these processes to be decoupled, a format called GFF ('Gene-Finding
Format' or 'General Feature Format') 
was proposed as a protocol for the transfer of feature information.  
It is now possible to take features from an outside source and add them in to 
an existing program, or in the extreme to write a dynamic programming system 
which only took external features.
</p><p>
GFF allows people to develop features
and have them tested without having to maintain a complete
feature-finding system.  Equally, it would help those developing and
applying integrated gene-finding programs to test new feature
detectors developed by others, or even by themselves.
</p><p>
We want the GFF format to be easy to parse and process by a variety of
programs in different languages.  e.g. it would be useful if Unix
tools like grep, sort and simple perl and awk scripts could easily
extract information out of the file.  For these reasons, for the
primary format, we propose a record-based structure, where each
feature is described on a single line, and line order is not relevant.
</p><p>
We do not intend GFF format to be used for complete data management of
the analysis and annotation of genomic sequence.  Systems such as
Acedb, Genotator etc. that have much richer data representation
semantics have been designed for that purpose.  The disadvantages in
using their formats for data exchange (or other richer formats such as
ASN.1) are (1) they require more complexity in parsing/processing, (2)
there is little hope on achieving consensus on how to capture all
information.  GFF is intentionally aiming for a low common
denominator.
</p><p>
With the changes taking place to version 2 of the format, we also 
allow for feature sets to be defined over RNA and Protein sequences,
as well as genomic DNA.  This is used for example by the <a 
HREF="http://emboss.sourceforge.net/">EMBOSS</a> project to
provide standard format output for all features as an option.
In this case the &lt;strand&gt; and &lt;frame&gt; 
fields should be set to '.'.  To assist this transition in specification,
a new <a HREF="#Type_Meta_Comment">#Type Meta-Comment</a> has been added.
</p><p>
Here are some example records:
</p>
<pre>
SEQ1	EMBL	atg	103	105	.	+	0
SEQ1	EMBL	exon	103	172	.	+	0
SEQ1	EMBL	splice5	172	173	.	+	.
SEQ1	netgene	splice5	172	173	0.94	+	.
SEQ1	genie	sp5-20	163	182	2.3	+	.
SEQ1	genie	sp5-10	168	177	2.1	+	.
SEQ2	grail	ATG	17	19	2.1	-	0
</pre>
</div>

<div class="SH">
<h2>DEFINITION</h2>
<p class="IP"><span>Fields are:</span>
&#060;seqname&#062; &#060;source&#062; &#060;feature&#062; &#060;start&#062; &#060;end&#062; &#060;score&#062; &#060;strand&#062; &#060;frame&#062; [attributes] [comments]
</p>
 <dl>
 <dt>&#060;seqname&#062;</dt>
 <dd>The name of the sequence.  Having an explicit sequence name
allows a feature file to be prepared for a data set of multiple
sequences.  Normally the seqname will be the identifier of the
sequence in an accompanying fasta format file.  An alternative is that
&lt;seqname&gt; is the identifier for a sequence in a public database, such
as an EMBL/Genbank/DDBJ accession number.  Which is the case, and
which file or database to use, should be explained in accompanying
information.</dd>
<br/>

 <dt>&#060;source&#062;</dt>
 <dd> The source of this feature.  This field will normally be used to
indicate the program making the prediction, or if it comes from public
database annotation, or is experimentally verified, etc.</dd><br/>

 <dt>&#060;feature&#062;</dt>
 <dd> The feature type name.  We hope to suggest a standard set of
features, to facilitate import/export, comparison etc..  Of course,
people are free to define new ones as needed.  For example, Genie
splice detectors account for a region of DNA, and multiple detectors
may be available for the same site, as shown above.</dd><br/>
<br/> 
We would like to enforce a standard nomenclature for
common GFF features. This does not forbid the use of other features,
rather, just that if the feature is obviously described in the standard
list, that the standard label should be used. For this standard table
we propose to fall back on the international public standards for genomic 
database feature annotation, specifically, the 
<a href="http://www3.ebi.ac.uk/Services/WebFeat/">
DDBJ/EMBL/GenBank feature table documentation</a>).
<br/>
 <dt>&#060;start&#062;, &#060;end&#062; </dt>
 <dd> Integers.  &#060;start&#062; must be less than or equal to
&#060;end&#062;.  Sequence numbering starts at 1, so these numbers
should be between 1 and the length of the relevant sequence,
inclusive. (<b>Version 2 change</b>: version 2 condones values of
&#060;start&#062; and &#060;end&#062; that extend outside the
reference sequence.  This is often more natural when dumping from
acedb, rather than clipping.  It means that some software using the
files may need to clip for itself.)<br/>
</dd>
 <dt>&#060;score&#062;</dt>
 <dd> A floating point value.  When there is no score (i.e. for a
sensor that just records the possible presence of a signal, as for the
EMBL features above) you should use '.'. (<b>Version 2 change</b>: in
version 1 of GFF you had to write 0 in such circumstances.)</dd>

 <dt>&#060;strand&#062; </dt>
 <dd> One of '+', '-' or '.'.  '.' should be used when
strand is not relevant, e.g. for dinucleotide repeats.
<b>Version 2 change</b>: This field is left empty '.' for RNA and protein features.</dd>

 <dt>&#060;frame&#062;</dt>
 <dd> One of '0', '1', '2' or '.'.  '0' indicates that the specified
region is in frame, i.e. that its first base corresponds to the first
base of a codon.  '1' indicates that there is one extra base,
i.e. that the second base of the region corresponds to the first base
of a codon, and '2' means that the third base of the region is the
first base of a codon.  If the strand is '-', then the first base of
the region is value of &#060;end&#062;, because the corresponding
coding region will run from &#060;end&#062; to &#060;start&#062; on
the reverse strand.  As with &#060;strand&#062;, if the frame is not
relevant then set &#060;frame&#062; to '.'.  
It has been pointed out that "phase" might be a better descriptor than
"frame" for this field.
<b>Version 2 change</b>: This field is left empty '.' for RNA and protein features.</dd>

<dt>[attribute]</dt>
<dd> From version 2 onwards, the attribute field 
must have an tag value structure following the syntax used within
objects in a .ace file, flattened onto one line by semicolon
separators.  Tags must be standard identifiers
([A-Za-z][A-Za-z0-9_]*).  Free text values must be quoted with double
quotes. <em>Note: all non-printing characters in such free text value strings
(e.g. newlines, tabs, control characters, etc)
must be explicitly represented by their C (UNIX) style backslash-escaped
representation (e.g. newlines as '\n', tabs as '\t').</em>
As in ACEDB, multiple values can follow a specific tag.  The
aim is to establish consistent use of particular tags, corresponding
to an underlying implied ACEDB model if you want to think that way
(but acedb is not required).  Examples of these would be:
<font size="3"><pre>
seq1     BLASTX  similarity   101  235 87.1 + 0	Target "HBA_HUMAN" 11 55 ; E_value 0.0003
dJ102G20 GD_mRNA coding_exon 7105 7201   .  - 2 Sequence "dJ102G20.C1.1"
</pre></font>
<p>
The semantics of tags in attribute field tag-values pairs has
intentionally not been formalized.  Two useful guidelines are to use 
DDBJ/EMBL/GenBank feature 'qualifiers' (see 
<a href="http://www3.ebi.ac.uk/Services/WebFeat/">DDBJ/EMBL/GenBank
feature table documentation</a>), or the features that ACEDB generates 
when it dumps GFF.
</p>
<p>
<b>Version 1 note</b> In version 1 the attribute field was called the
group field, with the following specification:<br/>
An optional string-valued field that can be used as a name to
group together a set of records.  Typical uses might be to group the
introns and exons in one gene prediction (or experimentally verified
gene structure), or to group multiple regions of match to another
sequence, such as an EST or a protein.
</p>
</dd>
</dl>
</div>

<div class="SH">
<h2>COMMENTS</h2>
Comments are allowed, starting with "#" as in Perl, awk etc.
Everything following # until the end of the line is ignored.
Effectively this can be used in two ways.  Either it must be at the
beginning of the line (after any whitespace), to make the whole line a
comment, or the comment could come after all the required fields on
the line.

<h4> ## comment lines for meta information </h4>

There is a set of standardised (i.e. parsable) ## line types that can
be used optionally at the top of a gff file.  The philosophy is a
little like the special set of %% lines at the top of postscript
files, used for example to give the BoundingBox for EPS files.<br/>

Current proposed ## lines are:
</div>
<div class="SH">
<h2>FILE NAMING</h2>
</div>
<div class="SH">
<h2>SEMANTICS</h2>
</div>
<div class="SH">
<h2>WAYS TO USE GFF</h2>
</div>
<div class="SH">
<h2>COMPLEX EXAMPLES</h2>
<h3>Similarities to Other Sequences</h3>
</div>
<div class="SH">
<h2>CUMULATIVE SCORE ARRAYS</h2>
</div>

</body>
</html>

