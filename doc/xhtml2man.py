#!/usr/bin/env python2.5
from xml.etree.cElementTree import ElementTree, iterparse, XMLTreeBuilder
import sys
ns = '{http://www.w3.org/1999/xhtml}'
marg = '  '

def classify(tag, attrib):
    tag = tag.replace(ns, '')
    if attrib and 'class' in attrib:
        return tag + '.' + attrib['class']
    else:
        return tag

class Parser:
    tagstack = []
    indent = -1 
    printqueue = ''
    in_body = 0
    def push(self, tag, attrib):
        classtag = classify(tag, attrib)
        self.tagstack.append( classtag )
        return classtag
    def pop(self):
        if self.tagstack:
            return self.tagstack.pop()
        else:
            return None
    def current_tag(self):
        if len(self.tagstack) > 0:
            return self.tagstack[-1]
        else:
            return None
    def parent_tag(self):
        if len(self.tagstack) > 1:
            return self.tagstack[-2]
        else:
            return None
    def start(self, tag, attrib):
        if not self.in_body:
            if tag == ns + 'body':
                self.in_body = True
            else:
                return

        start_tag = self.push( tag, attrib )
        if start_tag == 'div.SH':
            self.flush()
            self.printqueue = '.SH'
        elif start_tag == 'div.TH':
            self.flush()
            self.printqueue = '.TH'
        elif start_tag == 'div.TP':
            self.flush()
            self.flush('.TP')
        elif start_tag.startswith('br'):
            self.flush()
            if start_tag == 'br.PP':
                self.flush('.PP')
            else:
                self.flush('.br\n')

        self.indent += 1

    def end(self, end_tag):
        if not self.in_body: return
        current_tag = self.current_tag()
        parent_tag = self.parent_tag()
        if current_tag and current_tag.startswith('p'):
            if not current_tag == 'p.IP':
                self.flush('.P')
        if current_tag == 'div.TH':
            self.flush()
        self.pop()
        self.indent -= 1
    def data(self, data):
        if not self.in_body: return
        if len(data.strip()) == 0: return
        current_tag = self.current_tag()
        parent_tag = self.parent_tag()
        if current_tag.startswith('span'):
            if parent_tag == 'div.TH':
                if current_tag == 'span.name':
                    self.printqueue += ' %s' % data.strip()
                else:
                    self.printqueue += ' "%s"' % data.strip()
            elif parent_tag == 'p.IP':
                self.flush('.IP "%s"' % data.strip())
            elif parent_tag == 'div.TP':
                self.flush(data.strip())
            else:
                self.flush( data.strip() ) 
        elif current_tag.startswith('pre'):
            self.flush()
            self.flush('.nf')
            self.flush(data)
            self.flush('.fi')
        elif current_tag.startswith('div'):
            if current_tag == 'div.TH':
                self.printqueue += '.TH.'
            elif current_tag == 'div.SH':
                self.flush(data.strip())
            else:
                self.flush(data.strip())
        elif current_tag == 'h2':
            if parent_tag == 'div.SH': 
                self.flush(' %s' % data.strip())
        elif current_tag == 'h3' and parent_tag == 'div.SH':
            self.flush('.SS %s' % data.strip())
        elif current_tag.startswith('b'):
            self.flush()
            self.flush('.B %s' % data.strip())
        elif current_tag.startswith('u'):
            self.flush()
            self.flush('.I %s' % data.strip())
        elif current_tag.startswith('p'):
            self.flush( data.strip() )
        else:
            #self.flush( data.strip() )
            self.printqueue += data
                
    def flush(self, text=''):
        self.printqueue += text
        #self.printqueue = self.printqueue.strip()
        if len( self.printqueue ) > 0: print self.printqueue
        self.printqueue = ''

    def close(self):
        pass
        
target = Parser()
parser = XMLTreeBuilder(target=target)

parser.feed( open( sys.argv[1] ).read() )
parser.close()

