#!/usr/bin/env python2.5
import sys


db = int( sys.argv[2] )
link = 'http://jaspar.genereg.net/cgi-bin/jaspar_db.pl?ID=%%s&#38;rm=present&#38;db=%d' % db
count = 0

print '<div class="SH">'
print '<h2>ACCESSIONS</h2>'

for line in file( sys.argv[1] ):
    if line.startswith('>'):
        count += 1
        fields = line.lstrip('>').strip().split('\t')
    else:
        continue
    print '<div class="TP">'
    print '<span>%d</span>' % count,
    acc_link = link % fields[0]
    print '<a target="jaspar_info" href="%s"><b>%s</b></a>' % (acc_link, fields[0]),

    attribs = fields.pop()
    print " ".join(fields[1:]),
    for attrib in attribs.split(';'):
        try:
            name,value = attrib.strip().split()
            print '; <u>%s</u> %s' % (name, value),
        except:
            pass
    print
    print '</div>'


print '</div>'
