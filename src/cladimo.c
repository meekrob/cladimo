#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pwm.h"
#include "library.h"
#include "pwm_readers.h"
#include "maf.h"
#include "seqMap.h"
#include "cladistic_motif.h"
#include "common_names.h"
#include "linesplit.h"

#include "combo_lib.h"
#include "gff.h"

//#define VERSION_STRING_HELPER(X) #X
//#define VERSION_STRING(X) VERSION_STRING_HELPER(X)
#include "config.h"

const char USAGE[] = "cladimo.v.%s\n\
%s [format=]file motif|pwm|accession\n\
";

typedef enum { maf, mfasta, fasta, nib } fileformat;
typedef struct 
{
    fileformat format;
    char *filename;
    FILE *fp;
} inputfile;

#define shortmatch(a, b, c) (0 == strncasecmp(a, b, c))
void parse_infile(inputfile *infile, char *path_arg)
{
    char *path = NULL;
    char *eqptr = NULL;
    if ((eqptr = index(path_arg, '=')) != NULL)
    {
        int eoffset = (eqptr - path_arg);
        path = (eqptr+1);
        if (shortmatch("maf", path_arg, eoffset)) infile->format = maf;
        else if (shortmatch("nib", path_arg, eoffset)) infile->format = nib;
        else if (shortmatch("mfasta", path_arg, eoffset)) infile->format = mfasta;
        else if (shortmatch("fasta", path_arg, eoffset)) infile->format = fasta;
        else
        {
            fprintf(stderr, "unrecognized format %.*s for input file %s\n", eoffset, (eqptr+1), path);
        }
    }
    else
    {
        path = path_arg;
        infile->format = maf;
    }
    infile->fp = fopen(path, "r");
    infile->filename = path;
}
#undef shortmatch

#ifdef SCAN_TIMING
#include <time.h>
#endif
int main(int argc, char **argv)
{
    int i;
    inputfile infile;
    if (argc < 2)
    {
        //printf(USAGE,VERSION_STRING(VERSION), argv[0]);
        printf(USAGE,VERSION, argv[0]);
        return 1;
    }
    parse_infile(&infile, argv[1]);
    if (infile.fp == NULL)
    {
        fprintf(stderr, "bad infile:\n");
        fprintf(stderr, "fp %p\n", infile.fp);
        fprintf(stderr, "filename %s\n", infile.filename);
        return 1;
    }

    // seq, align vars
    char * refstring = NULL;
    struct seqMap **alignment;
    int seqi, n_seqs;
    int nmatches = 0;
    struct scored_seqMap_feature *scores = NULL;
    size_t scores_size_t = 0;

    // pwm, runlib vars
    struct combo_lib *runlib = parse_arg_list(&(argv[2]), argc-2);
    PWM **pwms = runlib->pwm_library;
    int npwms = runlib->total_n;

    // tree vars
    struct btree *scaling_tree = parse_nh(READABLE_44WAY_TREE_STR);
    //struct btree *scaling_tree = parse_nh(READABLE_INSECT_12WAY_NTREE_STR);

    //printf("#cladimo.v.%s\n", VERSION_STRING(VERSION));
    printf("#cladimo.v.%s\n", VERSION);

    if (infile.format == maf)
    {
#ifdef SCAN_TIMING
        double dtime = 0;
        time_t clock = time(NULL);
        int total_ref_scanned = 0;
        int total_all_scanned = 0;
#endif
        int trailing = runlib->max_pwm_length;
        struct seqMap ** overhang = NULL;
        n_seqs = 0;
        while((alignment = read_mafblock_contiguous(infile.fp, &overhang, &n_seqs, trailing, stdout)) != NULL)
        {
            //print_gff_alignment_info(stdout, alignment, n_seqs, 0);
            if (refstring == NULL) refstring = reference_from_maf_seq(alignment[0]);

            nmatches = 0;
            for (seqi=0; seqi < n_seqs; seqi++)
            {
#ifdef SCAN_TIMING
                total_all_scanned += (alignment[seqi]->seqlen / 1e3);
#endif
                nmatches = score_seqMap(alignment[seqi], &scores, nmatches, &scores_size_t, pwms, npwms, trailing);
            }
#ifdef SCAN_TIMING
            total_ref_scanned += (alignment[0]->seqlen / 1e3);
#endif

            if (nmatches > 0)
            {
                int ncms = 0;
                struct cladistic_motif **carray = score_cladistic_motifs(scores, nmatches, alignment, n_seqs, scaling_tree, &ncms);
                print_cladistic_motifs(stdout, refstring, carray, ncms, alignment, n_seqs);
                
                fprintf(stdout, "\n");
                for(i=0; i<ncms; i++) free_cm(&(carray[i]));
                free(carray);
            }
            blockFree(&alignment, n_seqs);
#ifdef SCAN_TIMING
            if ((total_all_scanned > 0) && ((total_all_scanned % 10) == 0))
            {
                dtime = difftime(time(NULL), clock);
                fprintf(stderr, "MAF: scanned %6dKB ref (%6.2fKB/sec) %6dKB total (%6.2fKB/sec)\r", total_ref_scanned, (float)total_ref_scanned/dtime, total_all_scanned, total_all_scanned/dtime);
            }
#endif
        }
#ifdef SCAN_TIMING
        dtime = difftime(time(NULL), clock);
        fprintf(stderr, "MAF: scanned %6dKB ref (%6.2fKB/sec) %6dKB total (%6.2fKB/sec). %f seconds total.\n", total_ref_scanned, (float)total_ref_scanned/dtime, total_all_scanned, total_all_scanned/dtime, dtime);
#endif
        blockFree(&overhang, n_seqs);
    }
    else if (infile.format == mfasta)
    {
        struct seqMap *seq = NULL;
        nmatches = 0;
        size_t arr_t = 0;
        alignment = NULL;
        scores = NULL;
        n_seqs = 0;
        while( (seq = readMapSeq(infile.fp)) != NULL)
        {
            nmatches = score_seqMap(seq, &scores, nmatches, &scores_size_t, pwms, npwms, 0);
            n_seqs = append_to_array(&seq, &alignment, n_seqs, 1, sizeof(struct seqMap*), &arr_t, 8);
        }
        if ((n_seqs > 0) && (nmatches > 0))
        {
            asprintf(&refstring, "%s", alignment[0]->info);
            int ncms = 0;
            struct cladistic_motif **carray = score_cladistic_motifs(scores, nmatches, alignment, n_seqs, scaling_tree, &ncms);
            print_cladistic_motifs(stdout, refstring, carray, ncms, alignment, n_seqs);
            
            fprintf(stdout, "\n");
            for(i=0; i<ncms; i++) free_cm(&(carray[i]));
            free(carray);
            blockFree(&alignment, n_seqs);
        }

    }
    else if (infile.format == fasta)
    {
        struct seqMap *seq = NULL;
        while( (seq = readMapSeq(infile.fp)) != NULL)
        {
            nmatches = 0;
            nmatches = score_seqMap(seq, &scores, nmatches, &scores_size_t, pwms, npwms, 0);
            // report scores
            for(i=0; i<nmatches; i++)
                print_pwm_match(stdout, "pwm_match", &(scores[i]), seq, seq->info);
            seqMap_free(&seq);
        }
    }
    else if (infile.format == nib)
    {
        /* nib routine */
        // report scores
    }
    else {}

    free(scores);
    free(refstring);
    free_combo_lib(&runlib);
    depth_first_free(&scaling_tree);
    fclose(infile.fp);
    return 0;
}
