#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linesplit.h"
#include "seqMap.h"
#define MIN(a,b) a < b ? a : b
int cmp_seqMap_info(const void *a, const void*b)
{
    struct seqMap *x = (struct seqMap *)a;
    struct seqMap *y = (struct seqMap *)b;
    return strcmp(x->info,y->info);
}
int seqMap_genome_start_pnt(struct seqMap *map, int offset)
{
    if (map->genome_coordinate.strand >= 0)
    {
        return map->genome_coordinate.base + offset;
    }
    else
    {
        return ((map->genome_coordinate.max - map->genome_coordinate.base) - map->seqlen) + (map->seqlen-offset);
    }
}

void debug_seqMap(char *name, struct seqMap* sqm, FILE *out)
{
    return;
    //int a;
    int seqlen = sqm->seqlen;
    fprintf(out, "    %s:", name);
    //fprintf(out, " info: %s, %d-%d %d.", sqm->info, sqm->genome_coordinate.base, sqm->genome_coordinate.base + sqm->seqlen, sqm->genome_coordinate.strand);
    fprintf(out, " info: %20s, %c.", sqm->info, (sqm->genome_coordinate.strand ? '+' : '-'));
    fprintf(out, " seqlen: %d.", seqlen);
    fprintf(out, " maplen %d.\n", sqm->maplen);
    fprintf(out, "% *d ", 75, sqm->genome_coordinate.base);
    // instead of the whole fucking sequence, let's do a fixed-width representation showing the ends
    if (seqlen <= 50)
    {
        fprintf(out, "%.*s", seqlen, sqm->seq);
    }
    else
    {  
        int repr_width = 60;
        int seqChunk = (repr_width - 16)/2;
        const char format[] = "%.*s ... % 6d ... %.*s";
        fprintf(out, format, seqChunk, sqm->seq, seqlen - seqChunk*2, seqChunk, &(sqm->seq[seqlen-seqChunk]));
    }
    fprintf(out, " %d\n", sqm->genome_coordinate.base + sqm->seqlen);
    //fprintf(out, "        coord_to_col ");
    //for(a=0; a< sqm->seqlen; a++) fprintf(out, "%d ", sqm->coord_to_col[a]);
    //fputc('\n', out);
    //fprintf(out, "        col_to_coord ");
    //for(a=0; a< sqm->maplen; a++) fprintf(out, "%d ", sqm->col_to_coord[a]);
    //fputc('\n', out);
    //fprintf(out, "        align ");
    //for(a=0; a< sqm->maplen; a++) fputc(projectedSequence(a, sqm), out);
    //fputc('\n', out);
}

void debug_seqMap_block(char *name, struct seqMap **block, int n, FILE *out)
{
    int i;
    char * buf = malloc(sizeof(char)*strlen(name)+50);
    for (i=0; i < n; i++)
    {
        struct seqMap * sqm = block[i];
        sprintf(buf, "%s: %d", name, i);
        debug_seqMap(buf, sqm, out);
    }
    free(buf);
}

void *mallocClear(size_t size)
{
    void *ptr = NULL;
    ptr = malloc(size);
    bzero(ptr, size);
    return ptr;
}

void seqMap_free(struct seqMap **ptr)
{
    if (*ptr == NULL) return;
    free((*ptr)->seq);
    free((*ptr)->col_to_coord);
    free((*ptr)->info);
    free((*ptr)->coord_to_col);
    free(*ptr);
    *ptr = NULL;
}
#if 0
inline int deprojectMapIndex(int mapPos, struct seqMap *map)
// return -1 for gap positions, otherwise, return the corresponding offset in the sequence
{
    return map->col_to_coord[mapPos];
}
#endif
int deprojectMapIndexSafe(int mapPos, struct seqMap *map)
// return the corresponding offset or the nearest preceeding, or 0
{
    if (mapPos==0) return 0;
    mapPos = MIN(mapPos, (map->maplen-1));
    int seqpos = (int)map->col_to_coord[mapPos];
    while (seqpos < 0)
    {
        seqpos = deprojectMapIndex(--mapPos, map);
        if (mapPos==0) return 0;
    }
    return seqpos;
}
int projectSeqIndex(int seqIndex, struct seqMap *map)
// return the offset in the aligned text
{
    if (seqIndex >= map->seqlen) return map->coord_to_col[map->seqlen-1];
    return map->coord_to_col[seqIndex];
}
/** returns alignment text (by recomposition) **/
unsigned char projectedSequence(int mapPos, struct seqMap *map)
{
    if (map->col_to_coord[mapPos] >= 0) return map->seq[map->col_to_coord[mapPos]];
    else
        return '-';
}
#define DEREF_MAPIX_T(map) &(map->array_size_t.mapix)
#define DEREF_SEQIX_T(map) &(map->array_size_t.seqix)
#define DEREF_SEQ_T(map) &(map->array_size_t.seq)
#define CHNK 64
#define append_to_coord_to_col(data, nitems, sqmp) append_to_array(data, &(sqmp->coord_to_col), sqmp->seqlen, nitems, sizeof(int), DEREF_MAPIX_T(sqmp), CHNK)
#define append_to_col_to_coord(data, nitems, sqmp) append_to_array(data, &(sqmp->col_to_coord), sqmp->maplen, nitems, sizeof(int), DEREF_SEQIX_T(sqmp), CHNK)
#define append_to_seq(data, nitems, sqmp) append_to_array(data, &(sqmp->seq), sqmp->seqlen, nitems, sizeof(unsigned char), DEREF_SEQ_T(sqmp), CHNK)

static inline void update_map(int chi, struct seqMap *map)
{
    char ch = (char)chi;
    int seqpos = map->seqlen;
    int mappos = map->maplen;
    int gap = -1;

    if (ch == '-')
        // update the projection with a gap mapping
        map->maplen = append_to_col_to_coord(&gap, 1, map);
    else
    {
        // add to sequence
        append_to_seq(&ch, 1, map);

        // add the map index to the projection
        append_to_coord_to_col(&mappos, 1, map);

        // add the sequence index to the map
        map->maplen = append_to_col_to_coord(&seqpos, 1, map);
        map->seqlen = seqpos+1;
    }
}
struct seqMap *sliceMap(struct seqMap *base, int cut_pnt, int len)
{
    int i;
    int seqLeft = deprojectMapIndexSafe(cut_pnt, base);
    int seqRight = deprojectMapIndexSafe(cut_pnt+len, base);
    int seqlen = (seqRight - seqLeft) + 1;

    struct seqMap *newSeqMap = mallocClear(sizeof(struct seqMap));
    // genome_coordinate
    newSeqMap->genome_coordinate.base = base->genome_coordinate.base;
    newSeqMap->genome_coordinate.max = base->genome_coordinate.max;
    newSeqMap->genome_coordinate.strand = base->genome_coordinate.strand;
    // info
    size_t wordlen = strlen(base->info) + 1;
    newSeqMap->info = calloc(wordlen, sizeof(char));
    strcpy(newSeqMap->info, base->info);
    // arrays
    unsigned char *newSeq = &(base->seq[seqLeft]);
    int *newSeqIndex = &(base->col_to_coord[cut_pnt]);
    int *newMapIndex = &(base->coord_to_col[seqLeft]);
    append_to_seq(newSeq, seqlen, newSeqMap);
    append_to_col_to_coord(newSeqIndex, len, newSeqMap);
    append_to_coord_to_col(newMapIndex, seqlen, newSeqMap);

    // shift coord_to_coles left
    for (i=0; i< seqlen; i++) 
        newSeqMap->coord_to_col[i] -= cut_pnt;
    // shift col_to_coordes left
    for (i=0; i< len; i++)
        newSeqMap->col_to_coord[i] -= seqLeft;

    newSeqMap->maplen = len;
    newSeqMap->seqlen = seqlen;
    newSeqMap->genome_coordinate.base += seqLeft;
    return newSeqMap;
}
void appendMap(struct seqMap *left, struct seqMap *right)
{
    int i;
    // shift the appended non-gap indexes to the right
    append_to_col_to_coord(right->col_to_coord, right->maplen, left);
    for(i = left->maplen; i < (left->maplen + right->maplen); i++)
        if (left->col_to_coord[i] != -1) left->col_to_coord[i] += left->seqlen;

    // shift the appended map coordinates to the right
    append_to_coord_to_col(right->coord_to_col, right->seqlen, left);
    for(i = left->seqlen; i < (left->seqlen+right->seqlen); i++)
        left->coord_to_col[i] += left->maplen;

    append_to_seq(right->seq, right->seqlen, left);

    left->maplen += right->maplen;
    left->seqlen += right->seqlen;
}
struct seqMap *seqMapCpy(struct seqMap *src)
{
    struct seqMap *copy = mallocClear(sizeof(struct seqMap));
    copy->genome_coordinate.max = src->genome_coordinate.max;
    copy->genome_coordinate.base = src->genome_coordinate.base;
    copy->genome_coordinate.strand = src->genome_coordinate.strand;
    copy->maplen = append_to_col_to_coord(src->col_to_coord, src->maplen, copy);
    append_to_coord_to_col(src->coord_to_col, src->seqlen, copy);
    copy->seqlen = append_to_seq(src->seq, src->seqlen, copy); 

    char *info = malloc(sizeof(char) * (strlen(src->info)+1));
    strcpy(info, src->info);
    copy->info = info;
    return copy;
}
struct seqMap *concatMaps(struct seqMap *left, struct seqMap *right)
{
    struct seqMap *base = seqMapCpy(left);
    appendMap(base, right);
    return base;
}
void shiftMapRight(struct seqMap *map, int margin)
{
    if (margin==0) return;
    int* old_col_to_coord = map->col_to_coord;
    int old_maplen = map->maplen;

    // new col_to_coord: (-1 * margin ) + old col_to_coord
    map->col_to_coord = mallocClear(sizeof(int) * (map->maplen + margin));
    map->array_size_t.seqix = map->maplen + margin;
    // fill empty seq coordinates left
    map->maplen = margin;
    for (map->maplen = 0; map->maplen < margin; map->maplen++) 
        map->col_to_coord[map->maplen] = -1;

    map->maplen = append_to_col_to_coord(old_col_to_coord, old_maplen, map);
    free(old_col_to_coord);

    // shift map coordinates right
    int i;
    for (i=0; i< map->seqlen; i++) 
        map->coord_to_col[i] += margin;
}

void readSeqLine(FILE *in, struct seqMap *seq)
{
    int ch;
    while ((ch = fgetc(in)) != -1)
    {
        if (ch == '\n') break;
        else if (isspace(ch)) continue;
        update_map(ch, seq);
    }
}
struct seqMap *newSeqMap()
{
    return mallocClear(sizeof(struct seqMap));
}
struct seqMap *readMapSeq(FILE *in)
{
    if (feof(in)) return NULL;
    struct seqMap * seq = mallocClear(sizeof(struct seqMap));

    int linestart;
    while ((linestart = fgetc(in)) != -1)
    {
        if (isspace(linestart))
            continue;
        else if (seq->info == NULL)
        {
            int nfields=0;
            char **fields = linesplit(in, &nfields);
            if (linestart != '>') // non-FASTA-style header?
            {
                char *header = join_fields(" ", fields, nfields);
                asprintf(&(seq->info), "%c%s", linestart, header);
                free(header);
            }
            else // FASTA-style header '>'
            {
                seq->info = join_fields(" ", fields, nfields);
            }
            char *sptr = NULL;
            // replace all spaces with '_'
            while ((sptr = index(seq->info, ' ')) !=NULL) *sptr = '_';

            fieldsfree(&fields);
        }
        else if (index("-ACGTacgt", linestart) != 0)
        {
            update_map(linestart, seq);
            readSeqLine(in, seq);
        }
        else if (seq->seqlen > 0) 
        {
            break;
        }
    }
    int seqlen = seq->seqlen;
    seq->seq= realloc(seq->seq, sizeof(unsigned char) * (seqlen+1));
    seq->seq[seqlen] = 0;
    seq->seqlen = seqlen;
    return seq;
}
