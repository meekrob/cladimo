#ifndef CLADIMO_GFF_H
#define CLADIMO_GFF_H
#include <stdio.h>
#include "seqMap.h"
#include "cladistic_motif.h"
char *str_chr_nul(char *str, int c);
char *contig_ptr(char *full);
void print_gff_line_starter(FILE *outp, char *contig, char *feature, int feature_start, int feature_end, float score, char feature_strand);
void print_pwm_match(FILE *outp, char *feature_label, struct scored_seqMap_feature *match, struct seqMap *reference, char *contig);
void print_cladistic_motif(FILE *outp, struct cladistic_motif *cm, struct seqMap *reference, struct seqMap **alignment, int nseqs);
void print_cladistic_motifs(FILE *outp, char *refstring, struct cladistic_motif **cms, int ncms, struct seqMap **alignment, int nseqs);
void print_gff_alignment_info(FILE *outp, struct seqMap **alignment, int nseqs, float score);
#endif
