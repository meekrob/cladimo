#ifndef SEQMAP_H
#define SEQMAP_H
/* Specialized objects for dealing with coordinates in aligned sequences */
struct seqMap
{
    struct { int base; int max; int strand; } genome_coordinate;
    char *info;
    // seqlen
    unsigned char* seq;
    /**
     ** coord_tol_col : 0 .. seqlen
                         index i | value
       sequence position (coord) | alignment text offset (col)
     **/
    int *coord_to_col; 
    int seqlen; 
    /**
     ** col_to_coord: 0 .. maplen
                              index i | value
        alignment text position (col) | corresponding sequence position (coord) or -1 
    **/
    int *col_to_coord;  
    int maplen;
    struct { size_t seq; size_t seqix; size_t mapix; } array_size_t;
};
struct seqMap *newSeqMap();
void seqMap_free(struct seqMap **ptr);

// return -1 for gap positions, otherwise, return the corresponding offset in sequence
//inline int deprojectMapIndex(int mapPos, struct seqMap *map);
#define deprojectMapIndex(mapPos, map) map->col_to_coord[mapPos]

// return the corresponding offset or the nearest preceeding, or 0
int deprojectMapIndexSafe(int mapPos, struct seqMap *map);

// return the offset in the aligned text
int projectSeqIndex(int seqIndex, struct seqMap *map);

// return aligned text char
unsigned char projectedSequence(int mapPos, struct seqMap *map);

// seqMap editing functions
static inline void update_map(int ch, struct seqMap *map);
struct seqMap *sliceMap(struct seqMap *base, int cut_pnt, int len);
void appendMap(struct seqMap *left, struct seqMap *right);
void shiftMapRight(struct seqMap *map, int margin);
struct seqMap *concatMaps(struct seqMap *left, struct seqMap *right);
struct seqMap *seqMapCpy(struct seqMap *src);
int seqMap_genome_start_pnt(struct seqMap *map, int offset);
int cmp_seqMap_info(const void *a, const void*b);

/* readers */

// text
void readSeqLine(FILE *in, struct seqMap *seq);
struct seqMap *readMapSeq(FILE *in);

void debug_seqMap(char *name, struct seqMap* sqm, FILE *out);
#endif
