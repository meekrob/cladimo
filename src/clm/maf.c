#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linesplit.h"
#include "seqMap.h"
#include "maf.h"
#include "gff.h"
#include "common_names.h"
#include "compat.h"
/* Routines for handling MAF format alignments with seqMaps
 */
char **maf_sources(struct seqMap **mafblock, int nseq)
{
    int i;
    char **srcs = calloc(nseq, sizeof(char**));
    for (i=0; i<nseq; i++) 
    {
        char *sourcename = mafblock[i]->info;
        srcs[i] = compat_strndup(sourcename, dot_place(sourcename));
    }
    return srcs;
}
char **maf_species(struct seqMap **mafblock, int nseq)
{
    int i;
    char **srcs = calloc(nseq, sizeof(char**));
    for (i=0; i<nseq; i++) 
    {
        char *sourcename = mafblock[i]->info;
        int dp = dot_place(sourcename);
        char *name = translate_assembly_name(sourcename, dp);
        if (name==sourcename)
            srcs[i] = compat_strndup(name, dp);
        else
            srcs[i] = strdup(name);
    }
    return srcs;
}
char *reference_from_maf_seq(struct seqMap *seq)
{
    size_t slen = strlen(seq->info) + 1;
    char *refstring = malloc(sizeof(char) * slen);
    strcpy(refstring, seq->info);
    return refstring;
}

char *rename_assembly_source(char *buf)
{   //modifies in-place

    int dp = dot_place(buf);
    char *name = translate_assembly_name(buf, dp);
    if (name != buf) 
    {
        char *suffix = &(buf[dp]);
        int suflen = strlen(suffix);
        int namelen = strlen(name);
        char *scratch = calloc(namelen+suflen+1, sizeof(char));
        strcpy(scratch, name);
        strcat(scratch, suffix);
        strcpy(buf, scratch);
        return scratch;
    }
    return strdup(buf);
}
long nextlinelen(FILE *fp)
{
    long start = ftell(fp);
    long linelen = 0;
    while(!feof(fp))
    {
        if ('\n' == fgetc(fp)) 
        {
            fseek(fp, start, SEEK_SET);
            return linelen+1;
        }
        linelen++;
    }
    return EOF;
}
char *nextline(FILE *in, char **linebuf, size_t *linebufsize)
{
    long linelen = nextlinelen(in);
    if (feof(in)) return NULL;
    if (linelen > *linebufsize) 
    {
        *linebufsize = linelen;
        *linebuf = realloc(*linebuf, *linebufsize * sizeof(char));
    }
    fread(*linebuf, sizeof(char), linelen, in);
    (*linebuf)[linelen-1] = 0;
    return *linebuf;
}

struct seqMap *read_MAF_source_line(FILE *in)
{
    struct seqMap *sq = newSeqMap();
    const int linebuflen = 512;

    char buf[linebuflen]; bzero(buf, linebuflen*sizeof(char));
    char strand = 0;
    int named_seqlen = 0;
    long linestart = ftell(in);
    int conv = fscanf(in, " %s %d %d %c %d", buf, &(sq->genome_coordinate.base), &named_seqlen, &strand, &(sq->genome_coordinate.max));
    if (conv < 5)
    {
        fprintf(stderr, "bad pie (%d)\nbuf:%s at %ld\n\n\n\n\n",conv, buf, ftell(in));
        fseek(in, linestart, SEEK_SET);
        char *debug = NULL;
        size_t dbt = 0;
        debug = nextline(in, &debug, &dbt);
        fprintf(stderr, "failed on line:\n\n\n%s\n\n\n", debug);
        free(debug);
        return NULL;
    }
    char *info = rename_assembly_source(buf);
    sq->info = info;
    sq->genome_coordinate.strand = (strand == '-' ? -1 : 1);

    readSeqLine(in, sq);
    sq->seq = realloc(sq->seq, sizeof(char)*(sq->seqlen + 1));
    sq->seq[sq->seqlen] = 0;
    return sq;
}
int seek_line_start(FILE *in);
struct seqMap **read_MAF_block(FILE *in, int *nseq, float *blockScore)
{
    init_assembly_names();
    struct seqMap **seqs = NULL;
    *nseq = 0;
    size_t arr_t = 0;
    int ch = seek_line_start(in);
    

    //fprintf(stderr, "<read_MAF_block %c @%ld>\n", ch, ftell(in));
    while (!feof(in))
    {
        // score
        if (ch == 'a')
        {
            int numconv = fscanf(in, " score=%f", blockScore);
            //fprintf(stderr, "<a score=%d,%f @%ld>\n", numconv, *blockScore, ftell(in));
        }
        // source line
        else if (ch == 's')
        {
            struct seqMap *newseq = read_MAF_source_line(in);
            *nseq = append_to_array(&newseq, &seqs, *nseq, 1, sizeof(struct seqMap*), &arr_t, 8);
            ch=fgetc(in);
            continue;
        }
        // inter-block
        else if (isspace(ch))
        {
            //fprintf(stderr, "<inter-block1 %c @%ld>\n", ch, ftell(in));
            if (*nseq > 0) 
            {
                break;
            }
            ch = seek_line_start(in);
            //fprintf(stderr, "<inter-block2 %c @%ld>\n", ch, ftell(in));
        }
        ch = seek_line_start(in);
        //fprintf(stderr, "<outer %c @%ld>\n", ch, ftell(in));
    } 
    if (ungetc('\n', in)==EOF)
    {
        fprintf(stderr, "ungetc didn't work");
    }
    return seqs;
}
struct seqMap **cpy_block(struct seqMap **src, int nseq)
{
    int i;
    struct seqMap **newblock = calloc(nseq, sizeof(struct seqMap *));
    for (i=0; i< nseq; i++)
        newblock[i] = seqMapCpy(src[i]);

    return newblock;
}
struct seqMap **connect_MAF_blocks(struct seqMap **pre, int npre, struct seqMap **post, int npost)
{
    if (pre == NULL) return cpy_block(post, npost);
    if (post == NULL) return NULL;
    int i;
    struct seqMap **newblock = calloc(npost, sizeof(struct seqMap *));
    int maxblocksize = 0;
    for (i=0; i< npost; i++)
    {
        int j;
        for (j=0; j< npre; j++)
        {
            if (strcmp(post[i]->info, pre[j]->info) != 0) continue;
            if (post[i]->genome_coordinate.strand != pre[j]->genome_coordinate.strand) continue;
            if ((post[i]->genome_coordinate.base - (pre[j]->genome_coordinate.base + pre[j]->seqlen)) != 0) continue;
            // found contiguous in pre
            newblock[i] = concatMaps(pre[j],post[i]);
            break;
        }
        if (newblock[i] == NULL)
            // no contiguous alignments in pre
            newblock[i] = seqMapCpy(post[i]);

        // textsize
        if (newblock[i]->maplen > maxblocksize) 
            maxblocksize = newblock[i]->maplen;
    }
    for (i=0; i< npost; i++)
        shiftMapRight(newblock[i], maxblocksize - newblock[i]->maplen);

    return newblock;
}
struct seqMap **slice_MAF_block(struct seqMap **block, int nseq, int at, int len)
{
    int i;
    struct seqMap **newblock = calloc(nseq, sizeof(struct seqMap*));
    for (i=0; i<nseq; i++)
        newblock[i] = sliceMap(block[i], at, len);

    return newblock;
}
void debug_seqMap_block(char *name, struct seqMap **block, int n, FILE *out);

struct seqMap **read_mafblock_contiguous(FILE *in, struct seqMap ***last_block, int *nlast, int taillen, FILE *verbosity)
{
    int i, ncurr = 0;
    struct seqMap **connected_block = NULL;
    float blockscore = 0;
    if (*last_block)
    {
        debug_seqMap_block("last_block", *last_block, *nlast, stderr);
    }
    else
    {
        //fprintf(stderr, "last_block == NULL\n");
    }
    struct seqMap **next_block = read_MAF_block(in, &ncurr, &blockscore);
    if (next_block != NULL)
    {
        debug_seqMap_block("next_block", next_block, ncurr, stderr);
        // output?
        if (verbosity != NULL)
        {
            print_gff_alignment_info(verbosity, next_block, ncurr, blockscore);
        }
        // connect
        connected_block = connect_MAF_blocks(*last_block, *nlast, next_block, ncurr);
        debug_seqMap_block("connected_block", connected_block, ncurr, stderr);
        blockFree(last_block, *nlast);
        blockFree(&next_block, ncurr);

        //find cut point
        int textSize = connected_block[0]->maplen;
        int chop_pnt = textSize;
        for (i=0; i < ncurr; i++)
        {
            int seqlen = connected_block[i]->seqlen;
            int this_chop_pnt = projectSeqIndex(MAX(seqlen-taillen, 0), connected_block[i]);
            chop_pnt = MIN(chop_pnt, this_chop_pnt);
        }

        // copy the end for contiguiity
        int overhang_len = textSize-chop_pnt;
        //fprintf(stderr, "chop_pnt: %d, overhang_len: %d\n", chop_pnt, overhang_len);
        *last_block = slice_MAF_block(connected_block, ncurr, chop_pnt, overhang_len);
        *nlast = ncurr;
    }
    else if (*last_block != NULL)
    {
        blockFree(last_block, *nlast);
    }
    return connected_block;
}
void blockFree(struct seqMap ***blockptr, int nseq)
{
    if (*blockptr == NULL) return;
    int i;
    struct seqMap **block = *blockptr;
    for (i=0;i<nseq;i++)
        seqMap_free(&(block[i]));
    free(block);
    *blockptr = NULL;
}
int seek_line_start(FILE *in)
{
    do
    {
        int ch = fgetc(in);
        if (ch == '\n')
            return fgetc(in);

    } while(!feof(in));
    return EOF;
}
