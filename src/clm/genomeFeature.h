#ifndef GENOME_FEATURE_H
#define GENOME_FEATURE_H
#include <stdio.h>
#include "attributes.h"
#include "btree.h"
#define BLOCK 2048
typedef char str[BLOCK];
typedef enum { oneBased, inclusiveEnd } gFflag;
struct genomeFeature
{
    str seqname;
    str source;
    str feature;
    int start;
    int end;
    float score;
    char strand;
    char frame;
    struct set *flags;
    struct keyedAttributeList *attributes;
    void *data;
};

void writeGFFgenomeFeature(FILE *out, struct genomeFeature *gfeat);
void genomeFeatureFree(struct genomeFeature **gptr);
struct genomeFeature * genomeFeatureFromGFFStr(char *gffline);
struct genomeFeature * readNextGFFLine(FILE *in, FILE *pipe_comments);
struct keyedAttribute *createBLSattr(const char *termkey, struct keyedAttribute *species_matched_attr, struct btree *tree);
int readGFFLines(FILE *gff, struct genomeFeature ***arrayptr, int howmany, FILE *pipe_comments);
#endif
