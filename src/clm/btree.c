#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "common_names.h"
extern int nodenum;
int nodenum = 0;
struct str
{
    char source[SENTENCE+1];
    char *ptr;
};
float parse_name(char *name)
{
    float value = 0;
    char *colindex;
    if ((colindex = index(name, ':')) != NULL)
    {
        if (*(colindex+1) != 0) value = strtod(colindex+1, NULL);
        *colindex = 0;
    }
    return value;
}
void parse_values(struct btree* tree)
{
    if (tree)
    {
        if (tree->left)
        {
            parse_values(tree->left);
            tree->left_edge = parse_name(tree->left->name);
        }
        if (tree->right)
        {
            parse_values(tree->right);
            tree->right_edge = parse_name(tree->right->name);
        }
    }
}
void print_nh_middle(FILE *out, struct btree *node, float val, int prec, int weights_as_branches)
{
    extern int nodenum;
    if (node==NULL) return;
    if ((node->left != NULL) && (node->right != NULL))
    {
        fputc('(', out);
        print_nh_middle(out, node->left, node->left_edge, prec, weights_as_branches);
        fputc(',', out);
        print_nh_middle(out, node->right, node->right_edge, prec, weights_as_branches);
        fputc(')', out);
    }
    if (strlen(node->name)==0) sprintf(node->name, "n%d", ++nodenum);

    if (weights_as_branches)
    {
        fprintf(out, "%s:%.*f", node->name, prec, node->node_weight);
    }
    else
    {
        if (node->node_weight != 0) fprintf(out, "%s_%.*f:%.*f", node->name, prec, node->node_weight, prec, val);
        else fprintf(out, "%s:%.*f", node->name, prec, val);
    }
}
void print_nh_top(FILE *out, struct btree *node, float val, int prec, int weights_as_branches)
{
    extern int nodenum;
    if (node==NULL) return;
    if ((node->left != NULL) && (node->right != NULL))
    {
        fputc('(', out);
        print_nh_middle(out, node->left, node->left_edge, prec, weights_as_branches);
        fputc(',', out);
        print_nh_middle(out, node->right, node->right_edge, prec, weights_as_branches);
        fputc(')', out);
    }
}
void print_nh_weights(FILE *out, struct btree *node, float val, int prec)
{
    extern int nodenum;
    nodenum = 0;
    print_nh_top(out, node, val, prec, 1);
    fprintf(out, ";");
}
void print_nh(FILE *out, struct btree *node, float val, int prec)
{
    extern int nodenum;
    nodenum = 0;
    print_nh_top(out, node, val, prec, 0);
    fprintf(out, ";");
}
void print_tree(struct btree *node, float val, int indent)
{
    extern int nodenum;
    if (node==NULL) 
    {
        return;
    }
    int c = 0;
    while (c++ < indent) putchar('\t');
    if (strlen(node->name) == 0)
    {
        nodenum++;
        printf("n%d %.2f\n", nodenum, val);
    }
    else
    {
        printf("%s %.2f\n", node->name, val);
    }
    print_tree(node->left, node->left_edge, indent+1);
    print_tree(node->right, node->right_edge, indent+1);
}

void depth_first_free(struct btree** rootptr)
{
    if (rootptr == NULL) return;
    if (*rootptr == NULL) return;
    struct btree *root = *rootptr;
    depth_first_free(&(root->left));
    depth_first_free(&(root->right));
    free(root);
    *rootptr = NULL;
    return;
}

struct btree *new_node()
{
    struct btree *tree = malloc(sizeof(struct btree));
    bzero(tree, sizeof(struct btree));
    return tree;
}
struct btree *new_leaf(char *name, int len)
{
    struct btree *tree = new_node();
    strncpy(tree->name, name, len);
    tree->name[len]=0;
    return tree;
}
int strncmp_cmp(const void *a, const void *b)
{
    char *m = *(char**)a;
    char *n = *(char**)b;
    return strncmp(m,n,SENTENCE);
}
void src_list_sort(char **list, int n)
{
    qsort(list, n, sizeof(char*), strncmp_cmp);
}

int is_leaf(struct btree *tree)
{
    return (int)(tree->left == NULL) && (tree->right == NULL);
}
struct btree *subtree(struct btree *tree, char **names, int n, float *branchlength)
{
    if (tree==NULL) return NULL;
    struct btree *left, *right, *newnode;
    float leftlen, rightlen;
    if ((tree->left != NULL)&&(tree->right != NULL))
    {
        rightlen = 0;
        right = subtree(tree->right, names, n, &rightlen);
        leftlen = 0;
        left = subtree(tree->left, names, n, &leftlen);
        if ((right != NULL) && (left != NULL))
        {
            newnode = new_leaf(tree->name, SENTENCE);
            newnode->left = left;
            newnode->right = right;
            newnode->left_edge = tree->left_edge + leftlen;
            newnode->right_edge = tree->right_edge + rightlen;
            *branchlength = 0;
            return newnode;
        }
        else if (right != NULL)
        {
            *branchlength = tree->right_edge + rightlen;
            return right;
        }
        else if (left != NULL)
        {
            *branchlength = tree->left_edge + leftlen;
            return left;
        }
    }
    else // leaf
    {
        char *key = tree->name;
        if (bsearch(&key, names, n, sizeof(char*), strncmp_cmp) != NULL)
        {
            newnode = new_leaf(tree->name, SENTENCE);
            *branchlength = 0;
            return newnode;
        }
    }
    *branchlength = 0;
    return NULL;
}
struct btree *get_subtree(struct btree *tree, char **names, int n, float *branchlength)
{

    src_list_sort(names, n);
    *branchlength = 0;
    struct btree * rval = subtree(tree, names, n, branchlength);
    return rval;
}
struct btree * parse_str(struct str *s)
{
    struct btree * node = NULL;
    struct btree * leaf = NULL;
    if (s->ptr[0] == 0)
    {
        return NULL;
    }
    else if (s->ptr[0] == '(')
    {
        node = new_node();
        s->ptr++;
        node->left = parse_str(s);
        node->right = parse_str(s);
        if (s->ptr[0] == ',')
        {
            s->ptr++;
            return node;
        }
        else if (s->ptr[0] == ';')
        {
            s->ptr[0] = 0;
            return node;
        }
        else
        {
            size_t nodenamesize = strcspn(s->ptr, " ,)");
            strncpy(node->name, s->ptr, nodenamesize);
            node->name[nodenamesize] = 0;
            s->ptr += nodenamesize+1;
            return node;
        }
        
    }
    else
    {
        size_t wordsize = strcspn(s->ptr, " ,)");
        leaf = new_leaf(s->ptr, wordsize);
        s->ptr += wordsize+1;
        return leaf;
    }
}
struct btree *parse_nh(char * nh)
{
    struct str s;
    //bzero(s.source, sizeof(char)*SENTENCE);
    strncpy(s.source, nh, SENTENCE);
    s.ptr = s.source;
    s.ptr[SENTENCE-1] = 0;

    char *semiplace = NULL;
    if ((semiplace = index(s.ptr, ';')) != NULL) *semiplace = 0;
    struct btree *tree = parse_str(&s);
    parse_values(tree);
    return tree;
}
struct btree *copy_btree(struct btree *tree)
{
    if (tree==NULL) return NULL;
    struct btree *node = new_leaf(tree->name, SENTENCE);
    node->node_weight = tree->node_weight;
    node->left_edge = tree->left_edge;
    node->right_edge = tree->right_edge;
    node->left = copy_btree(tree->left);
    node->right = copy_btree(tree->right);
    return node;
}
float treesum(struct btree *tree)
{
    if (tree==NULL) return 0;
    float rval = 0;
    rval = tree->left_edge + treesum(tree->left);
    rval += tree->right_edge + treesum(tree->right);
    return rval;
}
float subset_BLS(struct btree *tree, char **names, int nnames)
{
    float len = 0;
    struct btree* subtree = get_subtree(tree, names, nnames, &len);
    float subsum = treesum(subtree);
    depth_first_free(&subtree);
    return subsum;
}
struct btree *readTreeFile(char *path)
{
    char treestring[0x100000];
    FILE *infile = fopen(path,"r");
    fscanf(infile, "%s", treestring);
    fclose(infile);
    return parse_nh(treestring);
}
int node_count(struct btree *tree)
{
    if (is_leaf(tree)) 
        return 0;
    else
        return 1 + node_count(tree->left) + node_count(tree->right);
}

char **leaf_names(struct btree *tree, int *nnames)
{
    char **leafnames = NULL;
    if (is_leaf(tree))
    {
        *nnames = 1;
        leafnames = malloc(sizeof*leafnames);
        leafnames[0] = strdup(tree->name);
        return leafnames;
    }

    int nleft = 0;
    char **left_list = leaf_names(tree->left, &nleft);
    int nright = 0;
    char **right_list = leaf_names(tree->right, &nright);

    leafnames = malloc(sizeof(char*) * (nleft+nright));
    memcpy(leafnames, left_list, sizeof(char*) * nleft);
    memcpy(&(leafnames[nleft]), right_list, sizeof(char*) * nright);
    *nnames = nleft + nright;
    free(left_list);
    free(right_list);
    return leafnames;
}

struct btree **node_list(struct btree *tree, int *nnodes)
{
    struct btree **list = malloc(sizeof(struct btree*));
    *list = tree;

    if (is_leaf(tree))
    {
        *nnodes = 1;
        return list;
    }

    int nleft = 0;
    struct btree **left_list = node_list(tree->left, &nleft);
    int nright = 0;
    struct btree **right_list = node_list(tree->right, &nright);

    list = realloc(list, sizeof(struct btree*) * (1+nleft+nright));
    memcpy(&(list[1]), left_list, sizeof(struct btree*) * nleft);
    memcpy(&(list[1+nleft]), right_list, sizeof(struct btree*) * nright);
    *nnodes = 1 + nleft + nright;
    free(left_list);
    free(right_list);
    return list;
}

