#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "seqMap.h"
#include "gff.h"
#include "pwm.h"
#include "maf.h"
#include "combo_lib.h"
#include "cladistic_motif.h"
#include "btree.h"
#include "linesplit.h" // for append_to_array
#define _SET_(nv) nv = nv
int cmp_features_by_seqinfo(const void *a, const void *b)
{
    struct scored_seqMap_feature *x = (struct scored_seqMap_feature *)a;
    struct scored_seqMap_feature *y = (struct scored_seqMap_feature *)b;
    return cmp_seqMap_info(x->source,y->source);
}
int cmp_features_in_projection(const void *a, const void *b)
{
    struct scored_seqMap_feature *x = (struct scored_seqMap_feature *)a;
    struct scored_seqMap_feature *y = (struct scored_seqMap_feature *)b;
    int p1 = projectSeqIndex(x->position_in_source, x->source);
    int p2 = projectSeqIndex(y->position_in_source, y->source);
    if (p1 == p2)
    {
        void *ref1 = x->model;
        void *ref2 = y->model;
        return (ref1 != ref2);// just check the pointers to see if they're to the same model
    }
    return p1-p2;
}
struct scored_seqMap_feature * new_scored_feature(int position_in_source, float model_score, struct seqMap *source, PWM *model)
{
    int len = model->length;
    struct scored_seqMap_feature *match = malloc(SCORE_SIZE_T);
    bzero(match, SCORE_SIZE_T);
    match->_SET_(position_in_source);
    match->_SET_(model_score);
    match->_SET_(source);
    match->_SET_(model);
    match->_SET_(len);
    return match;
}
#define append_match(data, array, arrlen, arr_limit) append_to_array(data, &(array), arrlen, 1, SCORE_SIZE_T, arr_limit, 16)
int score_seqMap(struct seqMap *seq, struct scored_seqMap_feature **f_array, int nmatches, size_t *f_size_t, PWM **pwms, int npwms, int offset)
{
    //_pwm_indexing_
    extern const signed char dna_encoding[256];
    bitsequence bitseq = 0;
    int position;
    for (position=0; position < seq->seqlen; position++)
    //_for_each_position_
    {
        unsigned char seqchar = seq->seq[position];
        if (letterval(seqchar) == -1) continue;
        bitseq = dnashiftone((unsigned char)seqchar, bitseq, MAXWORDSIZE);
        if (position < offset) continue;
        int pwmi;
        for (pwmi=0; pwmi< npwms; pwmi++)
        //_for_each_pwm_
        {
            PWM *pwm = pwms[pwmi];
            int setposition = position - (pwm->length - 1);
            if (setposition >= 0)
            {
                float score = numeric_wordscore(bitseq, pwm);
                if (fabs(score) >= pwm->threshold)
                {
                    struct scored_seqMap_feature *newScoredFeature = new_scored_feature(setposition, score, seq, pwm);
                    nmatches = append_match(newScoredFeature, *f_array, nmatches, f_size_t);
                    free(newScoredFeature);
                }
            }
        }
    }
    return nmatches;
}
struct scored_seqMap_feature *score_alignment(struct seqMap **alignment, int nseqs, PWM **pwms, int npwms, int *nmatches, int overlap)
{
    struct scored_seqMap_feature *f_array = NULL;
    size_t f_size_t=0;
    int seqi;
    *nmatches = 0;
    for (seqi=0; seqi < nseqs; seqi++)
        *nmatches = score_seqMap(alignment[seqi], &f_array, *nmatches, &f_size_t, pwms, npwms, overlap);

    return f_array;
}

void debug_block(struct seqMap **block, int nseq)
{
    int i;
    char strands[] = "- +";
    int width = MIN(block[0]->maplen, 75);
    for (i=0; i< nseq; i++)
    {
        fprintf(stderr, "%10.10s ", block[i]->info);
        fprintf(stderr, "% 10d % 10d %c ", block[i]->genome_coordinate.base, block[i]->genome_coordinate.base + block[i]->seqlen, strands[block[i]->genome_coordinate.strand + 1]);
        int a;
        for(a=0; a<width; a++) fputc(projectedSequence(a, block[i]), stderr);
        fputc('\n', stderr);
    }
}
//cladistic scores
struct cladistic_motif **score_cladistic_motifs(struct scored_seqMap_feature *matches, int nmatches, struct seqMap **alignment, int nseqs, struct btree *scaling_tree, int *ncms)
{
    struct cladistic_motif *cm = NULL;
    struct cladistic_motif **cm_arr = NULL;
    size_t cm_arr_size_t = 0;

    /*char **sources = maf_sources(alignment, nseqs);
    struct btree *scaling_subtree = NULL;
    if (scaling_tree != NULL) 
    {
        int i;
        float bl=0;
        scaling_subtree = get_subtree(scaling_tree, sources, nseqs, &bl);
        for(i=0; i<nseqs; i++) free(sources[i]);
        free(sources);
    }*/

    qsort(matches, nmatches, SCORE_SIZE_T, cmp_features_in_projection);
    int last_i = 0;
    int curr_i = 0;
    struct scored_seqMap_feature *last = NULL;
    for (curr_i=0; curr_i<nmatches; curr_i++)
    {
        int ident = 0;
        last = &(matches[last_i]);

        if (curr_i > 0)
            ident = cmp_features_in_projection(last, &(matches[curr_i]));
        if (ident != 0)
        {
            cm = clm(last, curr_i - last_i, scaling_tree, alignment, nseqs);
            *ncms = append_to_array(&cm, &(cm_arr), *ncms, 1, sizeof(struct cladistic_motif*), &cm_arr_size_t, 16);
            last_i = curr_i;
        }
    }
    if (last_i < nmatches)
    {
        cm = clm(last, nmatches - last_i, scaling_tree, alignment, nseqs);
        *ncms = append_to_array(&cm, &(cm_arr), *ncms, 1, sizeof(struct cladistic_motif*), &cm_arr_size_t, 16);
    }
    //depth_first_free(&scaling_subtree);
    return cm_arr;
}
void free_cm(struct cladistic_motif **cmptr)
{
    struct cladistic_motif *cm = *cmptr;
    depth_first_free(&(cm->score_tree));
    free(cm->matches);
    free(cm);
    *cmptr = NULL;
}
float bls(struct btree *scaling_tree, struct scored_seqMap_feature *matches, int nmatches) 
{
    int i;
    typedef char scratch[SENTENCE]; 
    scratch *matchnames = calloc(sizeof(scratch), nmatches);
    char **matchptr = calloc(sizeof(char*), nmatches);
    for(i=0; i< nmatches; i++) 
    {
        bzero(matchnames[i], sizeof(scratch));  
        strncpy(matchnames[i], matches[i].source->info, dot_place(matches[i].source->info));
        matchptr[i] = matchnames[i];
    }
    float bl = 0;
    struct btree *subtree = get_subtree(scaling_tree, matchptr, nmatches, &bl);
    float bls = treesum(subtree) / treesum(scaling_tree);
    depth_first_free(&subtree);
    free(matchnames);
    free(matchptr);
    return bls;
}
struct cladistic_motif *clm(struct scored_seqMap_feature *matches, int nmatches, struct btree *scaling_tree, struct seqMap **alignment, int nseq)
{
    struct cladistic_motif *newcm = malloc(sizeof(struct cladistic_motif));
    bzero(newcm, sizeof(struct cladistic_motif));
    size_t matches_t = 0;
    newcm->nmatches = append_to_array(matches, &(newcm->matches), 0, nmatches, sizeof(struct scored_seqMap_feature), &matches_t, nmatches);
    if (scaling_tree != NULL)
    {
        qsort(newcm->matches, nmatches, sizeof(struct scored_seqMap_feature), cmp_features_by_seqinfo);
        struct scored_seqMap_feature *refmatch = &(newcm->matches[0]); 

        int aligncoord = projectSeqIndex(refmatch->position_in_source, refmatch->source);
        int seqi;
        //struct weight *weights = calloc(nmatches, sizeof(struct weight));
        struct weight *weights = calloc(nseq, sizeof(struct weight));
        //for (matchi=0; matchi < nmatches; matchi++)
        for (seqi=0; seqi < nseq; seqi++)
        {
            // search for a match to the align seq
            struct scored_seqMap_feature *res, key = { 0, alignment[seqi], 0, 0, NULL }; // only the aligment[seqi] is necessary
            res = bsearch(&key, newcm->matches, nmatches, sizeof(struct scored_seqMap_feature), cmp_features_by_seqinfo);
            if (res != NULL)
            {
                weights[seqi].nameptr = res->source->info;
                weights[seqi].plen = dot_place(weights[seqi].nameptr);
                weights[seqi].val = fabs(res->model_score);
            }
            else // get the score for the mismatch
            {
                struct seqMap * alignseq = alignment[seqi];
                PWM *pwm = (PWM *)(refmatch->model);
                weights[seqi].nameptr = alignseq->info;
                weights[seqi].plen = dot_place(weights[seqi].nameptr);
                // rescore the non-match at the orthologous point
                int orthopnt = deprojectMapIndex(aligncoord, alignseq);
                if (orthopnt < 0)
                    weights[seqi].val = 0;
                else if ((orthopnt + pwm->length) < alignseq->seqlen)
                {
                    weights[seqi].val = fabs(pwm_stringscore((char*)&(alignseq->seq[orthopnt]), 0, pwm));
                }
                else
                    weights[seqi].val = 0;
            }
            
        }
        //float branchlength = 0;
        //float score = 0;
        //qsort(weights, nseq, sizeof(struct weight), weightcmp);
        //newcm->score_tree = weighted_branchsum(scaling_tree, 0, weights, nseq, &branchlength, &score);
        //newcm->score_tree = summary_tree(scaling_tree, weights, nseq);
        //newcm->score_tree = copy_btree(scaling_tree);
        //average_over_topology(newcm->score_tree, weights, nseq);
        //newcm->score_tree = get_subtree(scaling_tree, weights, nseq, &score);
        newcm->score_tree = NULL;
        newcm->tree_score = bls(scaling_tree, matches, nmatches);
        free(weights);
    }
    return newcm;
}
