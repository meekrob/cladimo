#ifndef ORTHO_H
#define ORTHO_H
#include <stdio.h>
#include "seqMap.h"
#include "pwm.h"
#include "btree.h"
/* These objects represent the data used and obtained during scoring.
   For a general definition of cladistic motifs and pwm matches, see ...
 */
struct scored_seqMap_feature
{
    int len;
    struct seqMap *source;
    int position_in_source;
    float model_score;
    void *model;
};
struct cladistic_motif
{
    struct scored_seqMap_feature *matches; 
    int nmatches;
    struct btree *score_tree;
    float tree_score;
};
struct cladistic_motif *clm(struct scored_seqMap_feature *matches, int nmatches, struct btree *scaling_tree, struct seqMap **alignment, int nseq);
struct cladistic_motif **score_cladistic_motifs(struct scored_seqMap_feature *matches, int nmatches, struct seqMap **alignment, int nseqs, struct btree *scaling_tree, int *ncms);
void free_cm(struct cladistic_motif **);

#define SCORE_SIZE_T sizeof(struct scored_seqMap_feature)
#define SCORE_PTR_SIZE_T sizeof(struct scored_seqMap_feature *)

struct scored_seqMap_feature * new_scored_feature(int position_in_source, float model_score, struct seqMap *source, PWM *model);
int score_seqMap(struct seqMap *seq, struct scored_seqMap_feature **f_array, int nmatches, size_t *f_size_t, PWM **pwms, int npwms, int offset);
struct scored_seqMap_feature *score_alignment(struct seqMap **alignment, int nseqs, PWM **pwms, int npwms, int *nmatches, int overlap);
void cladistic_motifs(char *refstring, struct scored_seqMap_feature *matches, int nmatch, struct seqMap **alignment, int nseqs);
#endif
