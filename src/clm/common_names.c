#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common_names.h"

#ifndef MIN
#define MIN(a,b) a < b ? a : b
#endif
int assemb_cmp(const void *a, const void *b)
{
    struct assembly_name *x = (struct assembly_name *)a;
    struct assembly_name *y = (struct assembly_name *)b;
    return strncasecmp(x->assembly,y->assembly,MIN(x->plen,y->plen));
}

// actually arthropoda

struct assembly_name insect_common_names[] = {
{ 2, "dm", "d_melanogaster"} ,
{ 6, "drosim", "d_simulans"} ,
{ 6, "drosec", "d_sechellia"} ,
{ 6, "droyak", "d_yakuba"} ,
{ 6, "droere", "d_erecta"} ,
{ 6, "droana", "d_ananassae"} ,
{ 2, "dp", "d_pseudoobscura"} ,
{ 6, "droper", "d_persimilis"} ,
{ 6, "drowil", "d_willistoni"} ,
{ 6, "drovir", "d_virilis"} ,
{ 6, "dromoj", "d_mojavensis"} ,
{ 6, "drogri", "d_grimshawi"} ,
{ 6, "anogam", "a_gambiae"} ,
{ 6, "apimel", "a_mellifera"} ,
{ 6, "tricas", "t_castaneum"}};
const int N_INSECT_NAMES = sizeof(insect_common_names)/sizeof(struct assembly_name);
extern const int N_INSECT_NAMES;
extern struct assembly_name insect_common_names[];

struct assembly_name vertebrate_common_names[] = {
{ 2, "hg", "human"},
{ 6, "pantro", "chimp"},
{ 6, "gorgor", "gorilla"},
{ 6, "ponabe", "orangutan"},
{ 6, "rhemac", "rhesus"},
{ 7, "macaque", "rhesus"},
{ 6, "caljac", "marmoset"},
{ 6, "tarsyr", "tarsier"},
{ 6, "micmur", "mouse_lemur"},
{ 6, "otogar", "bushbaby"},
{ 6, "tupbel", "treeshrew"},
{ 2, "mm", "mouse"},
{ 2, "rn", "rat"},
{ 6, "dipord", "kangaroo_rat"},
{ 6, "cavpor", "guineapig"},
{ 6, "spetri", "squirrel"},
{ 6, "orycun", "rabbit"},
{ 6, "ochpri", "pika"},
{ 6, "vicpac", "alpaca"},
{ 6, "turtru", "dolphin"},
{ 6, "bostau", "cow"},
{ 6, "equcab", "horse"},
{ 6, "felcat", "cat"},
{ 6, "canfam", "dog"},
{ 2, "cf", "dog"},
{ 6, "myoluc", "microbat"},
{ 6, "ptevam", "megabat"},
{ 6, "erieur", "hedgehog"},
{ 6, "sorara", "shrew"},
{ 6, "loxafr", "elephant"},
{ 6, "procap", "rock_hyrax"},
{ 6, "echtel", "tenrec"},
{ 6, "dasnov", "armadillo"},
{ 6, "chohof", "sloth"},
{ 6, "maceug", "wallaby"},
{ 6, "mondom", "opossum"},
{ 6, "ornana", "platypus"},
{ 6, "galgal", "chicken"},
{ 6, "taegut", "zebra_finch"},
{ 6, "anocar", "lizard"},
{ 6, "xentro", "xenopus"},
{ 6, "tetnig", "tetraodon"},
{ 2, "fr", "fugu"},
{ 6, "gasacu", "stickleback"},
{ 6, "orylat", "medaka"},
{ 6, "danrer", "zebrafish"},
{ 6, "petmar", "lamprey"}};
const int N_VERTEBRATE_NAMES = sizeof(vertebrate_common_names)/sizeof(struct assembly_name);
extern const int N_VERTEBRATE_NAMES;
extern struct assembly_name vertebrate_common_names[];

int init_vertebrate_assembly_names()
{
    extern struct assembly_name vertebrate_common_names[];
    extern const int N_VERTEBRATE_NAMES;
    qsort(vertebrate_common_names, N_VERTEBRATE_NAMES, sizeof(struct assembly_name), assemb_cmp);
    return N_VERTEBRATE_NAMES;
}
int init_insect_assembly_names()
{
    extern struct assembly_name insect_common_names[];
    extern const int N_INSECT_NAMES;
    qsort(insect_common_names, N_INSECT_NAMES, sizeof(struct assembly_name), assemb_cmp);
    return N_INSECT_NAMES;
}
int init_assembly_names()
{
    int v = init_vertebrate_assembly_names();
    int i = init_insect_assembly_names();
    return i + v;
}

char *translate_vertebrate_assembly_name(char *name, int plen)
{
    struct assembly_name key, *res;
    extern const int N_VERTEBRATE_NAMES;
    extern struct assembly_name vertebrate_common_names[];
    strcpy(key.assembly, name);
    if (plen > -1) 
        key.plen = plen;
    else 
        key.plen = strlen(name);

    res = bsearch(&key, vertebrate_common_names, N_VERTEBRATE_NAMES, sizeof(struct assembly_name), assemb_cmp);
    if (res == NULL) 
        return name;
    return res->commonname;
}
char *translate_insect_assembly_name(char *name, int plen)
{
    struct assembly_name key, *res;
    extern const int N_INSECT_NAMES;
    extern struct assembly_name insect_common_names[];
    strcpy(key.assembly, name);
    if (plen > -1) 
        key.plen = plen;
    else 
        key.plen = strlen(name);

    res = bsearch(&key, insect_common_names, N_INSECT_NAMES, sizeof(struct assembly_name), assemb_cmp);
    if (res == NULL) 
        return name;
    return res->commonname;
}

char *translate_assembly_name(char *name, int plen)
{
    char *insect_name = translate_insect_assembly_name(name, plen);
    char *vert_name = translate_vertebrate_assembly_name(name, plen);
    // any alternate result is a match
    if (strcmp(insect_name, name) != 0) return insect_name;
    if (strcmp(vert_name, name) != 0) return vert_name;
    return name;
}

int common_names_c_main(int argc, char **argv)
{
    printf("# vertebrate names\n");
    for (int i=0; i < N_VERTEBRATE_NAMES; i++)
    {
         printf("%s %s\n", vertebrate_common_names[i].assembly, vertebrate_common_names[i].commonname);
    }
    printf("# insect names\n");
    for (int i=0; i < N_INSECT_NAMES; i++)
    {
         printf("%s %s\n", insect_common_names[i].assembly, insect_common_names[i].commonname);
    }
    return 0;
}
