#ifndef BTREE_H
#define BTREE_H
#define SENTENCE 2048
#define IS_LEAF_NODE(node) ((node->left == NULL) && (node->right == NULL))
struct btree
{
    char name[SENTENCE+1];
    float node_weight;
    float left_edge;
    struct btree *left;
    float right_edge;
    struct btree *right;
};
struct weight
{
    char *nameptr;
    int plen;
    float val;
};

int weightcmp(const void *a, const void *b);
struct btree *get_subtree(struct btree *tree, char **names, int n, float *branchlength);
struct btree *parse_nh(char * nh);
struct btree *copy_btree(struct btree *tree);
struct btree *new_leaf(char *name, int len);
void print_nh(FILE *out, struct btree *node, float val, int prec);
void print_nh_weights(FILE *out, struct btree *node, float val, int prec);
//void print_nh_middle(FILE *out, struct btree *node, float val, int prec);
void average_over_topology(struct btree *tree, struct weight *weights, int nweights);
struct btree *dfs(struct btree *tree, char *searchname, float *bl);
void depth_first_free(struct btree** rootptr);
void print_tree(struct btree *node, float val, int indent);
struct btree * weighted_branchsum(struct btree *tree, float stem, struct weight *weights, int n, float *branchlength, float *score);
struct btree * summary_tree(struct btree *node, struct weight *weights, int n);
float treesum(struct btree*);
struct btree *readTreeFile(char *path);
int is_leaf(struct btree *tree);
char **leaf_names(struct btree *tree, int *nnames); /* returns a list of copied strings */
int node_count(struct btree *tree);
struct btree **node_list(struct btree *tree, int *nnodes);
float subset_BLS(struct btree *tree, char **names, int nnames);
#endif
#ifndef MIN
#define MIN(a,b) ((a)<=(b)) ? (a) : (b)
#endif
