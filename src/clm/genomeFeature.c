#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "set.h"
#include "btree.h"
#include "append_to_array.h"
#include "strsep_all.h"
#include "genomeFeature.h"

void gffprint(struct genomeFeature *gfeat)
{
    printf("seqname: %s\n", gfeat->seqname);
    printf("source: %s\n", gfeat->source);
    printf("feature: %s\n", gfeat->feature);
    printf("start: %d\n", gfeat->start);
    printf("end: %d\n", gfeat->end);
    printf("score: %f\n", gfeat->score);
    printf("strand: %c\n", gfeat->strand);
    printf("frame: %c\n", gfeat->frame);
    printf("attribute_str <%s>\n", (char*)gfeat->data);
}

void *parsefunc(struct genomeFeature *g, char *attr_str)
{
    fprintf(stderr, "parse func for g\n");
    return strdup(attr_str);
}

void freefunc(void *dataptr)
{
    char **data = (char**)dataptr;
    fprintf(stderr, "free func for g\n");
    free(*data);
}
static void freeStrings(char ***str, int n)
{
    for (int i=0; i< n; i++) free((*str)[i]);
    free(*str);
    *str = NULL;
}

struct keyedAttribute *createBLSattr(const char *termkey, struct keyedAttribute *species_matched_attr, struct btree *tree)
{
    int nspecies = species_matched_attr->nvalues;
    char **species_matched = variableTypes_toStrings(species_matched_attr->values, nspecies);
    float subsum = subset_BLS(tree, species_matched, nspecies); 

    char *blskeyname = NULL;
    asprintf(&blskeyname, "%s_bls", species_matched_attr->key);

    struct keyedAttribute *bls_attr = keyedAttributeFromArgs(blskeyname, "f", subsum);

    free(blskeyname);
    freeStrings(&species_matched, nspecies);
    return bls_attr;
}


void writeGFFgenomeFeature(FILE *out, struct genomeFeature *gfeat)
{
    fprintf(out, "%s\t", gfeat->seqname);
    fprintf(out, "%s\t", gfeat->source);
    fprintf(out, "%s\t", gfeat->feature);
    fprintf(out, "%d\t", gfeat->start);
    fprintf(out, "%d\t", gfeat->end);
    fprintf(out, "%.3f\t", gfeat->score);
    fprintf(out, "%c\t", gfeat->strand);
    fprintf(out, "%c\t", gfeat->frame);
    if (gfeat->attributes)
    {
        for (int attr_i=0; attr_i < gfeat->attributes->nattributes; attr_i++)
        {
            struct keyedAttribute *attr = gfeat->attributes->list[attr_i];
            if (attr_i > 0) fprintf(out, " ; ");
            fprintf(out, "%s ", attr->key);
            for(int val_i = 0; val_i < attr->nvalues; val_i++)
            {
                if (val_i > 0) fprintf(out, " ");
                variableType *val = attr->values[val_i];
                fprintf(out, "%s", val->sval);
            }
        }
    }
    fprintf(out, "\n");
}

#define isnul(x) (x==NULL)
void genomeFeatureFree(struct genomeFeature **gptr)
{
    if (!isnul(gptr) && !isnul(*gptr))
    {
        struct genomeFeature *gfeat = *gptr;
        freeKeyedAttributeList(&(gfeat->attributes));
        freeset(&(gfeat->flags));
        free(gfeat);
        *gptr = NULL;
    }
}
struct genomeFeature * mallocZeroGenomeFeature()
{
    struct genomeFeature *gff = malloczero(gff);
    return gff;
}
struct keyedAttribute *attrFromGffString(char *attr_str)
{
    struct keyedAttribute *attr = NULL;
    int nfields = 0;

    /**comma split... commas will be converted to WS*****/
    char *comma = attr_str;
    while((comma = index(comma, ',')) != NULL) *comma = ' ';
    /***************************************************/

    char **fields = strsep_qsall(attr_str, &nfields);
    if (nfields > 0)
    {
        char *key = fields[0];
        char **values = NULL;
        int nvalues = 0;
        if (nfields > 1)
        {
            values = &(fields[1]);
            nvalues = nfields - 1;
        }
        attr = keyedAttributeFromStrings(key, values, nvalues);
    }
    free(fields);
    return attr;
}
struct keyedAttributeList *attrListFromGffString(char *attr_str)
{
    struct keyedAttributeList *attributes = NULL;
    int nfields = 0;
    char **fields = strsep_all(attr_str, ";", &nfields);
    for (int field_i=0; field_i < nfields; field_i++)
    {
        struct keyedAttribute *attr = attrFromGffString(fields[field_i]);
        if(attr) 
            attributes = addAttr(attr, attributes);
    }
    free(fields);
    return attributes;
}
struct genomeFeature * genomeFeatureFromGFFStr(char *gffline)
{
    if (gffline == NULL) return NULL;
    if (gffline[0] == '\0') return NULL;
    struct genomeFeature *gff = mallocZeroGenomeFeature();
    int pos = 0;
    int conv = sscanf(gffline, "%s %s %s %d %d %f %c %c %n", gff->seqname, gff->source, gff->feature, &(gff->start), &(gff->end), &(gff->score), &(gff->strand), &(gff->frame), &pos);
    if (conv < 8)
    {
        free(gff);
        return NULL;
    }
    gff->flags = makeset(8);
    mark(gff->flags, oneBased | inclusiveEnd);

    char *attr_str = &(gffline[pos]);
    gff->attributes = attrListFromGffString(attr_str);
    return gff;
}
int readGFFLines(FILE *gff, struct genomeFeature ***arrayptr, int howmany, FILE *pipe_comments)
{
    *arrayptr = calloc(howmany, sizeof(struct genomeFeature*));
    int nread = 0;

    char *line = NULL;
    char *linebuf = NULL;
    size_t line_limit = 0;
    while(next_line_buf(gff, &linebuf, &line_limit) >= 0)
    {
        if (errno > 0)
        {
            perror("An error occured reading from stdin\n");
            abort();
        }
        line = linebuf;
        if (strlen(line) < 1) 
            continue;
        struct genomeFeature *gfeat = NULL;
        if ((gfeat = genomeFeatureFromGFFStr(line)) != NULL)
        {
            (*arrayptr)[nread++] = gfeat;
            if (nread == howmany)
                break;
        }
        else if (!isnul(pipe_comments) && (line[0] == '#'))
        {
            fprintf(pipe_comments, "%s\n", line);
        }
    }
    free(linebuf);
    return nread;
}
struct genomeFeature * readNextGFFLine(FILE *in, FILE *pipe_comments)
{
    struct genomeFeature **arr = NULL;
    readGFFLines(in, &arr, 1, pipe_comments);
    struct genomeFeature *rval = arr[0];
    free(arr);
    return rval;
}


/*struct genomeFeature * gFeatFromBEDStr(char *bedline)

    struct genomeFeature *bed = malloc(sizeof *bed);
    int pos = 0;
    int conv = sscanf(bedline, "%s %d %d %n", bed->seqname, &(bed->start), &(bed->end), &pos);
    bed->flags = makeset(8);
    return bed;
}*/


void gff_attr_func(char *attr_str)
{
    int nattribs = 0;
    char **attribs = strsep_all(attr_str, ";", &nattribs);
    for (int i=0; i<nattribs; i++)
    {
        char *attrib = attribs[i];
        int nfields = 0;
        char **fields = strsep_qsall(attrib, &nfields);
        printf("<%d:%s", i, attrib);
        if ((nfields == 2)&&(index(fields[1], ',')!=NULL))
        {
            char *field = fields[1];
            int ncfields = 0;
            char **commafields = strsep_all(field, ",", &ncfields);
            printf("\n");
            for (int j=0; j < ncfields; j++)
                printf("\t<%d:%s>\n", j, commafields[j]);
            free(commafields);
        }
        else
        {
            for (int j=1; j<nfields; j++)
                printf(" %d:%s", j-1, fields[j]);
            printf(">\n");
        }
        free(fields);
    }
    free(attribs);
}

int maintest_gff(int argc, char **argv)
{
    char gffline[] = "chr7	cladimo	maf_block	66075465	66075512	0.000	+	.	aligned_sources mouse.chr7,rat.chr1,human.chr15,chimp.chr15,cow.chr21,dog.chr3,armadillo.scaffold_180734,elephant.scaffold_68633,fugu.chrUn;ncolumns 66; ";

    printf("%s\n", gffline);
    struct genomeFeature *gff = genomeFeatureFromGFFStr(gffline);
    gffprint(gff);
    genomeFeatureFree(&gff);

    return 0;
}
