#ifndef CLADIMO_MAF_H
#define CLADIMO_MAF_H
#include <stdio.h>
#define dot_place(str) strcspn(str, ".")
#include "seqMap.h"
struct seqMap **read_mafblock_contiguous(FILE *in, struct seqMap ***last_block, int *nlast, int taillen, FILE *verbosity);
char *reference_from_maf_seq(struct seqMap *seq);
struct seqMap *read_MAF_source_line(FILE *in);
struct seqMap **read_MAF_block(FILE *in, int *nseq, float *);
struct seqMap **connect_MAF_blocks(struct seqMap **pre, int npre, struct seqMap **post, int npost);
struct seqMap **slice_MAF_block(struct seqMap **block, int nseq, int at, int len);
/* this should be in a more general location */
void blockFree(struct seqMap ***blockptr, int nseq);
char **maf_sources(struct seqMap **mafblock, int nseq);
char **maf_species(struct seqMap **mafblock, int nseq);
#endif
#ifndef MAX
#define MAX(a,b) (a>b ? a : b)
#endif
#ifndef MIN
#define MIN(a,b) (a<b ? a : b)
#endif
