#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "maf.h"
#include "seqMap.h"
#include "cladistic_motif.h"
#include "gff.h"
#include "btree.h"
const char strands[] = "-?+";
#define strand(seqmap) strands[seqmap->genome_coordinate.strand + 1]
void print_pwm_attribs(FILE *outp, PWM *pwm)
{
    if (strlen(pwm->accession)>0) fprintf(outp, "pwm_accession \"%s\" ; ", pwm->accession);
    if (strlen(pwm->motif)>0) fprintf(outp, "pwm_motif \"%s\" ; ", pwm->motif);
    if (strlen(pwm->name)>0) fprintf(outp, "pwm_name \"%s\" ; ", pwm->name);
}
void print_gff_line_starter(FILE *outp, char *contig, char *feature, int feature_start, int feature_end, float score, char feature_strand)
{
    /* seqname */ fprintf(outp, "%s\t", contig);
    /* source  */fprintf(outp, "cladimo\t");
    /* feature */fprintf(outp, "%s\t", feature);
    /* start   */fprintf(outp, "%d\t", feature_start + 1);
    /* end     */fprintf(outp, "%d\t", feature_end);
    /* score   */fprintf(outp, "%.3f\t", score);
    /* strand  */fprintf(outp, "%c\t", feature_strand);
    /* frame   */fprintf(outp, ".\t");
}
char *contig_ptr(char *full)
{
    char *spname = index(full, '.');
    if ((spname == NULL) || *(spname + 1) == 0) 
        return full;
    return spname + 1;
}
void print_gff_alignment_info(FILE *outp, struct seqMap **alignment, int nseqs, float score)
{
    if (nseqs == 0) return;

    struct seqMap *fsrc = alignment[0];
    print_gff_line_starter(outp, contig_ptr(fsrc->info), "maf_block", fsrc->genome_coordinate.base, fsrc->genome_coordinate.base + fsrc->seqlen, score, strand(fsrc));
    int i;
    fprintf(outp, "aligned_sources ");
    for (i=0; i<nseqs; i++)
    {
        fprintf(outp, "%s", alignment[i]->info);
        if (i < (nseqs-1)) fprintf(outp, ",");
    }
    fprintf(outp, ";");
    int textsize = fsrc->maplen;
    fprintf(outp, "ncolumns %d; ", textsize);
    fprintf(outp, "\n");
}
void print_pwm_match(FILE *outp, char *feature_name, struct scored_seqMap_feature *match, struct seqMap *reference, char *contig)
{
    int map_pnt, ref_start, ref_end;
    int column, width;
    /* project matches onto the reference */
    int match_pnt = match->position_in_source;
    map_pnt = projectSeqIndex(match_pnt, match->source);
    column = map_pnt;
    ref_start = deprojectMapIndexSafe(map_pnt, reference) + reference->genome_coordinate.base;

    map_pnt = projectSeqIndex(match_pnt+match->len, match->source);
    width = map_pnt - column;
    ref_end = deprojectMapIndexSafe(map_pnt, reference) + reference->genome_coordinate.base;
    char strand = match->model_score >= 0 ? '+' : '-';

    print_gff_line_starter(outp, contig, feature_name, ref_start, ref_end, fabs(match->model_score), strand);

    /* species-specific coordinates */
    int startpnt = seqMap_genome_start_pnt(match->source, match_pnt);
    int endpnt = seqMap_genome_start_pnt(match->source, match_pnt + match->len);
    if (match->source->info != NULL) 
    {
        if (startpnt < endpnt)
            fprintf(outp, "position %s %d %d ;", match->source->info, startpnt + 1, endpnt);
        else
            fprintf(outp, "position %s %d %d ;", match->source->info, endpnt + 1, startpnt);
    }

    /* PWM detail */
    print_pwm_attribs(outp, (PWM*)match->model);

    /* matched sequence */
    fprintf(outp, " sequence %.*s ;", match->len, &(match->source->seq[match_pnt]));

    /* alignment coordinates */
    fprintf(outp, " column %d ; width %d ;", column, width);
    fprintf(outp, "\n");
}
void print_cm_score_tree(FILE *outp, struct cladistic_motif *cm)
{
    print_nh_weights(outp, cm->score_tree, cm->tree_score, 3);
}
void print_cladistic_motif(FILE *outp, struct cladistic_motif *cm, struct seqMap *reference, struct seqMap **alignment, int nseqs)
{
    int i;
    //int start = seqMap_genome_start_pnt(reference);
    //fprintf(stderr, "print_cladistic_motif .. ref_strand\n");
    char ref_strand = strands[reference->genome_coordinate.strand+1];
    //fprintf(stderr, "print_cladistic_motif .. contig_ptr\n");
    char *contig = contig_ptr(reference->info);

    int map_pnt, ref_start, ref_end;
    /* project matches onto the reference */
    struct scored_seqMap_feature *match = &(cm->matches[0]);
    int match_pnt = match->position_in_source;
    map_pnt = projectSeqIndex(match_pnt, match->source);
    ref_start = deprojectMapIndexSafe(map_pnt, reference) + reference->genome_coordinate.base;
    map_pnt = projectSeqIndex(match_pnt+match->len, match->source);
    ref_end = deprojectMapIndexSafe(map_pnt, reference) + reference->genome_coordinate.base;
    print_gff_line_starter(outp, contig, "cladistic_motif", ref_start, ref_end, cm->tree_score, ref_strand);

    fprintf(outp, "number_of_matches  %d ; ", cm->nmatches);
    fprintf(outp, "species_matched ");
    for (i=0; i< cm->nmatches; i++)
    {
        fprintf(outp, "%.*s", (int)dot_place(cm->matches[i].source->info), cm->matches[i].source->info);
        if (i < (cm->nmatches-1)) fprintf(outp, ",");
    }
    fprintf(outp, " ; ");

    print_pwm_attribs(outp, (PWM*)cm->matches[0].model); fprintf(outp, " ; ");
    //print_cm_score_tree(outp, cm);

    fprintf(outp, "\n");
    // individual matches
    for (i=0; i< cm->nmatches; i++)
        print_pwm_match(outp, "pwm_match", &(cm->matches[i]), reference, contig);
    fprintf(outp, "\n");
}
#if 0
struct genomeFeature *makeCLM(struct cladistic_motif *cm, struct seqMap *reference, struct seqMap **alignment, int nseqs)
{
    struct genomeFeature *clm = initializeGenomeFeature("cladistic_motif");
    struct scored_seqMap_feature *reference_match = &(cm->matches[0]);
    clm->start = genomeStartPoint(reference_match);
    clm->end = genomeEndPoint(reference_match);
    clm->strand = genomeStrand(reference_match);
    createGfAttr(clm, "number_of_matches", "d", cm->nmatches);
}
#endif
void print_cladistic_motifs(FILE *outp, char *refstring, struct cladistic_motif **cms, int ncms, struct seqMap **alignment, int nseqs)
{
    int i;
    struct seqMap *reference = NULL;
    for(i=0; i < nseqs; i++)
    {
        if (strcmp(refstring, alignment[i]->info) == 0) 
        {
            reference = alignment[i];
            break;
        }
    }
    for(i=0; i<ncms; i++)
        print_cladistic_motif(outp, cms[i], reference, alignment, nseqs);
}
