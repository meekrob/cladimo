#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "append_to_array.h"
#include "set.h"
#include "strset.h"

void strsetFree(struct strset **sptr)
{
    struct strset *ptr = *sptr;
    free(ptr->strings);
    freeset(&(ptr->bitset));
    free(ptr);
    *sptr = NULL;
}

struct strset *build_strset_template(char **allstrings, int nstrings)
{
    struct strset *rval = malloczero(rval);
    rval->strings = calloc(nstrings, sizeof(char *));
    for (int stringi=0; stringi < nstrings; stringi++)
        rval->strings[stringi] = allstrings[stringi];

    rval->len = nstrings;
    rval->bitset = makeset(nstrings);
    return rval;
}
struct strset *clone_strset_template(struct strset *setlist)
{
    return build_strset_template(setlist->strings, setlist->len);
}

static int strfind(char *item, char **list, int nitems)
{
    for(int i=0; i<nitems; i++)
    {
        if (0 == strcmp(item,list[i])) return i;
    }
    return -1;
}

int strset_has(char *str, struct strset *setlist)
{
    int position = strfind(str, setlist->strings, setlist->len);
    if (position >=0)
        return readmark(setlist->bitset, position);
    return -1;
}

int set_string_in_list(char *str, struct strset *setlist)
{
    int position = strfind(str, setlist->strings, setlist->len);
    if (position >=0)
        mark(setlist->bitset, position);

    return position;
}
int unset_string_in_list(char *str, struct strset *setlist)
{
    int position = strfind(str, setlist->strings, setlist->len);
    if (position >=0)
        unmark(setlist->bitset, position);

    return position;
}
int is_string_in_strset(char *str, struct strset *setlist)
{
    int position = strfind(str, setlist->strings, setlist->len);
    return ! (position == -1);
}

char **get_sublist(struct strset *setlist)
{
    int size = setsize(setlist->bitset);
    char **strings = calloc(size, sizeof(char*));
    int str_i = 0;
    int pos = 0;
    while( (pos = nextmarked(setlist->bitset, pos)) < setlist->len)
    {
        fprintf(stderr, "<nextmarked: %d>\n", pos);
        strings[str_i] = setlist->strings[pos];
        pos++;
        str_i++;
    }
    return strings;
}
void strset_union(struct strset *a, struct strset *b)
{
    setunion(a->bitset, b->bitset);
}
int strset_count_overlapping(struct strset *a, struct strset *b)
{
    return set_count_overlapping(a->bitset, b->bitset);
}
void strset_xunion(struct strset *a, struct strset *b)
{
    set_xunion(a->bitset, b->bitset);
}
void strset_intersection(struct strset *a, struct strset *b)
{
    setintersection(a->bitset, b->bitset);
}
int strset_c_main(int argc, char **argv)
{
    char *list[] = {"alpha", "beta", "gamma"};
    int list_size = 3;
    struct strset *template = build_strset_template(list, list_size);
    struct strset *list1  = clone_strset_template(template);
    struct strset *list2  = clone_strset_template(template);
    set_string_in_list("beta", list1);
    set_string_in_list("alpha", list2);

    strset_union(list2, list1); // result stored in first argument

    char **subset = get_sublist(list2);
    int n = setsize(list2->bitset);
    for (int i=0; i < n; i++)
    {
        fprintf(stdout, "%d: %s\n", i, subset[i]);
    }
    strsetFree(&list1);
    strsetFree(&list2);
    strsetFree(&template);
    free(subset);
    return 0;
}
