#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include "linesplit.h"
#include "strsep_all.h"
#include "append_to_array.h"
void debugfields(char **fields, int nfields)
{
    int i = 0;

    fprintf(stderr, "<%d: ", nfields);
    while (i<nfields)
    {
        fprintf(stderr, "%d:%s", i,fields[i]);
        if (i < (nfields-1)) fputc(' ',stderr);
        i++;
    }
    fprintf(stderr, ">\n");
}
char ** optionsplit(FILE *fp, int *nfields)
{ /*If the line contains tabs, they will be used as the delimiter instead of whitespace*/
    *nfields = 0;

    char **tabfields = linesplit_tabs(fp, nfields);
    if (*nfields > 1) return tabfields;
    else if (*nfields == 0) return NULL;
    
    // split whitespace on the single field
    char **wsfields = stringsplit(tabfields[0], nfields);
    fieldsfree(&tabfields);
    return wsfields;
}
void fieldsfree(char ***fptr)
{
    if(fptr!=NULL)
    {
        char **fields = *fptr;
        if(fields) free(fields[0]);
        free(fields);
        *fptr = NULL;
    }
}
char ** linesplit_tabs(FILE *fp, int *nfields)
{
    char *line = next_line(fp);
    return strsep_all(line, "\t", nfields);
}
char ** linesplit(FILE *fp, int *nfields)
{
    char *line = next_line(fp);
    return strsep_qsall(line, nfields);
}
char* join_fields(char *delim, char **fields, int nfields)
{
    int delimsize = strlen(delim);
    char *outstring = NULL;
    int len = 0;
    size_t s_t = 0;
    for (int i=0; i<nfields;i++)
    {
        char *field = fields[i];
        if (i > 0)
            len = append_to_array(delim, &outstring, len, delimsize, sizeof(char), &s_t, 24);

        len = append_to_array(field, &outstring, len, strlen(field), sizeof(char), &s_t, 24);
    }
    int lineend = 0;
    append_char(lineend, &outstring, len, &s_t);
    return outstring;
}
char ** stringsplit(char *sentence, int *nfields)
{
    return strsep_qsall(sentence, nfields);
}
