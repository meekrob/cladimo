#ifndef APPEND_TO_ARRAY_H
#define APPEND_TO_ARRAY_H
#include <stdio.h>
#include <stdlib.h>
#define malloczero(ptr) malloc(sizeof*ptr); zero(ptr, sizeof*ptr)
#define APP_TO_ARR_CHNK 64
#define append_char(addition, array, len, arrtptr) append_to_array(&addition, array, len, 1, sizeof(char), arrtptr, APP_TO_ARR_CHNK)
#define append_int(addition, array, len, arrtptr) append_to_array(&addition, array, len, 1, sizeof(int), arrtptr, APP_TO_ARR_CHNK)
#define append_long(addition, array, len, arrtptr) append_to_array(&addition, array, len, 1, sizeof(long), arrtptr, APP_TO_ARR_CHNK)
#define append_float(addition, array, len, arrtptr) append_to_array(&addition, array, len, 1, sizeof(float), arrtptr, APP_TO_ARR_CHNK)
#define append_double(addition, array, len, arrtptr) append_to_array(&addition, array, len, 1, sizeof(double), arrtptr, APP_TO_ARR_CHNK)
#define append_pointer(addition, array, len, arrtptr) append_to_array(&addition, array, len, 1, sizeof(char*), arrtptr, APP_TO_ARR_CHNK)
#define append_vars(addition, array, array_len, nitems, arrtptr) append_to_array(&addition, array, array_len, nitems, sizeof *addition, arrtptr, APP_TO_ARR_CHNK)
inline int append_to_array(void *, void *, int , int , size_t , size_t *, int );
//inline int append_str(char addition, char **str, int str_len, size_t *strsize);
char *next_line(FILE *in);
int next_line_buf(FILE *in, char **buf, size_t *bufsize);
int next_line_buf_chunk(FILE *in, char **buf, size_t *bufsize, int chunk);
void *zero(void *ptr, size_t size);
#endif
