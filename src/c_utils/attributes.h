#include "variableType.h"
#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H
struct keyedAttribute
{
    char *key;
    variableType **values;
    size_t nvalues; /* number of items in the array */
    size_t size; /* array size */
};
struct keyedAttributeList
{
    struct keyedAttribute **list;
    size_t nattributes; /* number of items in the array */
    size_t size; /* array size */
};

void freeKeyedAttribute(struct keyedAttribute **pptr);
void freeKeyedAttributeList(struct keyedAttributeList **pptr);
void printKeyedAttribute(FILE *out, struct keyedAttribute *attr);
int addValues(variableType *val, int nadd, struct keyedAttribute *attr);
struct keyedAttribute *keyedAttributeFromStrings(const char *key, char **values, int nvalues);
struct keyedAttribute *keyedAttributeFromArgs(const char * restrict key, const char * restrict fmt, ...);
//struct keyedAttribute *keyedAttributeFromArgs(char *key, char *fmt, ...);

struct keyedAttributeList *addAttr(struct keyedAttribute *new, struct keyedAttributeList *);
struct keyedAttributeList *addAttrFromStrings(const char *key, char **values, int nvalues, struct keyedAttributeList *attributes);
struct keyedAttribute *getAttr(const char *key, struct keyedAttributeList *);
struct keyedAttribute *popAttr(const char *key, struct keyedAttributeList *);

variableType *getAttrVal(const char *key, int valueIx, struct keyedAttributeList *attributes);
int getAttrValInt(const char *key, int valueIx, struct keyedAttributeList *attributes);
float getAttrValFloat(const char *key, int valueIx, struct keyedAttributeList *attributes);

void delAttr(const char *key, struct keyedAttributeList *list);
void printAttributeList(FILE *out, struct keyedAttributeList *);
int attribute_c_main(int argc, char **argv);
void print_delim_list(char **items, int nitems, char delim);
#endif
