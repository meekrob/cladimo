#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "append_to_array.h"
void *zero(void *ptr, size_t size)
{
    char *base = (char*)ptr;
    for (int i=0; i< size; i++)
        base[i] ^= base[i];
    return ptr;
}
int append_to_array(void *data, void *lineptr, int arrlen, int nitems, size_t itemsize, size_t *linesize, int chunk)
{
    char **line = (char **)lineptr;
    if (arrlen + nitems > *linesize)
    {
        int margin = ((nitems-1)/chunk) + 1;
        *linesize += margin*chunk;
        *line = realloc(*line, *linesize * itemsize);
    }
    size_t datasize = nitems * itemsize;
    memcpy(*line + (itemsize*arrlen), data, datasize);
    return arrlen + nitems;
}
inline int append_str(char addition, char **str, int str_len, size_t *strsize)
{
    int CHUNK = 64;
    return append_to_array(&addition, str, str_len, 1, sizeof(char), strsize, CHUNK);
}
char *next_line(FILE *in)
{
    if (feof(in)) return NULL;
    char *buf = NULL;
    size_t bufsize = 0;
    int linelen = 0;
    char ch = 0;
    while((ch = fgetc(in)) != '\n')
    {
        if (feof(in)) break;
        linelen = append_char(ch, &buf, linelen, &bufsize);
#ifdef DBG_APPEND_TO_ARRAY
        fprintf(DBG_APPEND_TO_ARRAY, "<append_to_array><next_line><buf linelen='%d'>%s</buf></next_line></append_to_array>\n", linelen, buf);
#endif
    }
    char lineend = 0;
    linelen = append_char(lineend, &buf, linelen, &bufsize);
#ifdef DBG_APPEND_TO_ARRAY
        fprintf(DBG_APPEND_TO_ARRAY, "<append_to_array><next_line><buf linelen='%d'>%s</buf></next_line></append_to_array>\n", linelen, buf);
#endif
    return buf;
}
int next_line_buf(FILE *in, char **buf, size_t *bufsize)
{
    if (feof(in)) return -1;
    int ch, linelen = 0;
    bzero(*buf, *bufsize);
    while((ch = fgetc(in)) != '\n')
    {
        if (feof(in)) break;
        linelen = append_char(ch, buf, linelen, bufsize);
    }
    int lineend = 0;
    append_char(lineend, buf, linelen, bufsize);
    return linelen;
}
int next_line_buf_chunk(FILE *in, char **buf, size_t *bufsize, int chunk)
{
    if (feof(in)) return -1;
    int ch, linelen = 0;
    bzero(*buf, *bufsize);
    while((ch = fgetc(in)) != '\n')
    {
        if (feof(in)) break;
        linelen = append_to_array(&ch, buf, linelen, 1, sizeof(char), bufsize, chunk);
    }
    char lineend = '\0';
    append_to_array(&lineend, buf, linelen, 1, sizeof(char), bufsize, chunk);
    return linelen;
}
