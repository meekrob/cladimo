#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "set.h"



static byte offsetmask[8] = { 0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1};

int bytecount[256] = 
{
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

#define base(ix) (ix>>3)
#define offset(ix) (ix&7)
void clearset(struct set *s)
{
    for(int i=0; i < (base(s->len) +1); i++)
        s->bytes[i] ^= s->bytes[i];
}
struct set *makeset(int len)
{
    struct set *newset = malloc(sizeof(struct set));
    newset->len = len; 
    newset->bytes = malloc(sizeof(byte)*(base(len)+1));
    clearset(newset);
    return newset;
}
int readmark(struct set *s, int pos)
{
    return (s->bytes[base(pos)] & offsetmask[offset(pos)]) != 0;
}
void mark(struct set *s, int pos)
{
    s->bytes[base(pos)] |=  offsetmask[offset(pos)];
}
void unmark(struct set *s, int pos)
{
    s->bytes[base(pos)] &= ~offsetmask[offset(pos)];
}
int setsize(struct set *s)
{
    int count = 0;
    for(int i=0; i < (base(s->len) +1); i++)
        count += bytecount[s->bytes[i]];
    return count;
}
struct set* aNotInb(struct set *a, struct set *b)
{
    assert(a->len == b->len);
    struct set* s = makeset(a->len);
    for(int i=0; i < (base(s->len) +1); i++)
        s->bytes[i] = a->bytes[i] & ~(b->bytes[i]);
    return s;
}
struct set *setcopy(struct set *s)
{
    struct set *scopy = malloc(sizeof(struct set));
    scopy->len = s->len; 
    scopy->bytes = malloc(sizeof(byte)*(base(s->len)+1));
    for(int i=0; i < (base(s->len) +1); i++)
        scopy->bytes[i] = s->bytes[i];
    return scopy;
}
void setunion(struct set *a, struct set*b)
{
    assert(a->len == b->len);
    for(int i=0; i < (base(a->len) +1); i++)
        a->bytes[i] |= b->bytes[i];
}
int set_count_overlapping(struct set *a, struct set *b)
{
    struct set *ab_union = setcopy(a);
    setunion(a,b);
    int overlapsize = setsize(ab_union);
    freeset(&ab_union);
    return overlapsize;
}
void set_xunion(struct set *a, struct set*b)
{
    assert(a->len == b->len);
    for(int i=0; i < (base(a->len) +1); i++)
        a->bytes[i] ^= b->bytes[i];
}
void setintersection(struct set *a, struct set*b)
{
    assert(a->len == b->len);
    for(int i=0; i < (base(a->len) +1); i++)
        a->bytes[i] &= b->bytes[i];
}
int nextmarked(struct set *s, int start)
{
    int pos = start;
    for (; pos < s->len; pos++)
    {
        int bit = readmark(s, pos);
        if (bit) break;
    }
    return pos;
}
int nextunmarked(struct set *s, int start)
{
    int pos = start;
    for (; pos < s->len; pos++)
    {
        int bit = readmark(s, pos);
        if (!bit) break;
    }
    return pos;
}
void debugset(struct set *s)
{
    if (s==NULL)
        fprintf(stderr, "NULL set\n");
    else
    {
        for (int i=0; i< s->len; i++)
        {
            fprintf(stderr, "%d", readmark(s, i));
        }
        fprintf(stderr, " %d\n", setsize(s));
    }
}

void freeset(struct set**sptr)
{
    if (*sptr)
        free((*sptr)->bytes);
    free(*sptr);
    *sptr = NULL;
}

int test_main(int argc, char **argv)
{
    struct set *s = makeset(10);
    debugset(s);
    mark(s, 2);
    mark(s, 3);
    debugset(s);
    unmark(s, 2);
    debugset(s);
    freeset(&s);
    return 0;
}
