#ifndef SET_H
#define SET_H
typedef unsigned char byte;
struct set
{
    int len;
    byte *bytes;
};

void clearset(struct set *s);
struct set *makeset(int len);
int readmark(struct set *s, int pos);
void mark(struct set *s, int pos);
void unmark(struct set *s, int pos);
int setsize(struct set *s);
struct set* aNotInb(struct set *a, struct set *b);
struct set *setcopy(struct set *s);
void setunion(struct set *a, struct set*b);
void setintersection(struct set *a, struct set*b);
int nextmarked(struct set *s, int start);
int nextunmarked(struct set *s, int start);
void debugset(struct set *s);
void freeset(struct set**sptr);
int set_count_overlapping(struct set *a, struct set *b);
void set_xunion(struct set *a, struct set*b);
#endif
