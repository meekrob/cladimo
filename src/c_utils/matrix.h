#ifndef MATRIX_H
#include <stdio.h>
void free_matrix(float*** matrix);
void free_char_table(char****table, int rows);
void print_matrix(FILE* out, float **matrix, int nrows, int ncols);
void print_char_table(FILE* out, char*** table, int nrows, int ncols);
void print_char_table_transposed(FILE* out, char*** table, int nrows, int ncols);
char*** read_char_table( FILE* fp, int* rows, int* cols);
float** read_matrix( FILE* fp, int *rows, int* cols);
float **transpose_matrix(float**matrix, int nrows, int ncols);
float **copy_matrix(float**matrix,int nrows,int ncols);
void matrix_scale_rows(float**matrix, int nrows, int ncols);
void matrix_scale_cols(float**matrix, int nrows, int ncols);
float array_cov(float* m1, float* m2, int len);
float array_cor(float*m1, float*m2, int len);
float** shape_array(float* arr, int nrows, int ncols);
float** covariance_matrix(float** m, int rows, int cols);
float** matrix_cov(float** m1, float** m2, int x1cols, int x2cols, int nrows);
float** zeroes(int nrows, int ncols);
float** nans(int nrows, int ncols);
float matrix_sum(float** m, int rows, int cols);
/* untested */
float *unstack_matrix(float** matrix, int nrows, int ncols);
float* sum_covariances(float** x, float** y, int xlen, int ylen, int nrows, int* outlen);

/* simple array functions */
float array_mean(float *, int);
float array_sum(float *, int);
float array_max(float *, int);
float array_min(float *, int);
int array_argmax(float *, int);
int array_argmin(float *, int);
#define MATRIX_H
#endif
