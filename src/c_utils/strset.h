#ifndef STRSET_H
#define STRSET_H
struct strset
{
    struct set* bitset;
    char **strings;
    int len;
};


void strsetFree(struct strset **sptr);
struct strset *build_strset_template(char **allstrings, int nstrings);
struct strset *clone_strset_template(struct strset *setlist);
static int strfind(char *item, char **list, int nitems);
int strset_has(char *str, struct strset *setlist);
int set_string_in_list(char *str, struct strset *setlist);
int unset_string_in_list(char *str, struct strset *setlist);
int is_string_in_strset(char *str, struct strset *setlist);
char **get_sublist(struct strset *setlist);
void strset_union(struct strset *a, struct strset *b);
void strset_intersection(struct strset *a, struct strset *b);
int strset_count_overlapping(struct strset *a, struct strset *b);
void strset_xunion(struct strset *a, struct strset *b);
#endif
