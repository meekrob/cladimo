#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "append_to_array.h"
#include "strsep_all.h"
#include "variableType.h"
#include "attributes.h"
void freeKeyedAttribute(struct keyedAttribute **pptr)
{
    if (pptr==NULL) return;
    struct keyedAttribute *ptr = *pptr;
    if (ptr != NULL)
    {
        free(ptr->key);
        for(int i=0; i<ptr->nvalues; i++)
        {
            freeVariableType(&(ptr->values[i]));
        }
        free(ptr->values);
    }
    free(ptr);
    *pptr = NULL;
}
void freeKeyedAttributeList(struct keyedAttributeList **pptr)
{
    if (pptr==NULL) return;
    struct keyedAttributeList *ptr = *pptr;
    if (ptr != NULL)
    {
        for (int i=0; i < ptr->nattributes; i++)
        {
            freeKeyedAttribute(&(ptr->list[i]));
        }
        free(ptr->list);
    }
    free(ptr);
    *pptr = NULL;
}

int addAttrValues(variableType **val, int nadd, struct keyedAttribute *attr)
{
    int chunk = 4;
    attr->nvalues = append_to_array(val, &(attr->values), attr->nvalues, nadd, sizeof(variableType *), &(attr->size), chunk);
    return attr->nvalues;
}

struct keyedAttribute *keyedAttributeFromStrings(const char *key, char **values, int nvalues)
{
    struct keyedAttribute *kattr = malloczero(kattr);
    kattr->key = strdup(key);
    for(int i=0; i<nvalues; i++)
    {
        variableType *value = variableTypeFromStr(values[i]);
        addAttrValues(&value,1, kattr); 
    }
    return kattr;
}
struct keyedAttribute *keyedAttributeFromArgs(const char * restrict key, const char * restrict fmt, ...)
{
    struct keyedAttribute *kattr = malloczero(kattr);
    kattr->key = strdup(key);
    int nvalues = strlen(fmt);
    va_list ap;
    va_start(ap, fmt);
    variableType **values = vcreateVariableTypeArray(fmt, ap);
    va_end(ap);
    addAttrValues(values, nvalues, kattr);
    free(values);
    return kattr;
}

struct keyedAttributeList *addAttr(struct keyedAttribute *new, struct keyedAttributeList *attributes)
{
    int chunk = 4;
    if (attributes==NULL) 
    {
        attributes = malloc(sizeof(struct keyedAttributeList));
        zero(attributes, sizeof*attributes);
    }
    attributes->nattributes = append_to_array(&(new), &(attributes->list), attributes->nattributes, 1, sizeof(struct keyedAttribute *), &(attributes->size), chunk);
    return attributes;
}
struct keyedAttributeList *addAttrFromStrings(const char *key, char **values, int nvalues, struct keyedAttributeList *attributes)
{
    return addAttr(keyedAttributeFromStrings(key, values, nvalues), attributes);
}

struct keyedAttribute *getAttr(const char *key, struct keyedAttributeList *attributes)
{
    /* returns NULL if not found */
    if (attributes == NULL) return NULL;
    for (int i=0; i< attributes->nattributes; i++)
    {
        if (strcmp(key, attributes->list[i]->key) == 0)
            return attributes->list[i];
    }
    return NULL;
}
int getAttrIndex(const char *key, struct keyedAttributeList *attributes)
{
    /* returns NULL if not found */
    if (attributes == NULL) return -1;
    for (int i=0; i< attributes->nattributes; i++)
    {
        if (strcmp(key, attributes->list[i]->key) == 0)
            return i;
    }
    return -1;
}
variableType *getAttrVal(const char *key, int valueIx, struct keyedAttributeList *attributes)
{
    /* returns NULL if not found, or if valueIx is out of range */
    struct keyedAttribute *attr = getAttr(key, attributes);
    if ((attr == NULL) || (valueIx > attr->nvalues)) 
        return NULL;
    return attr->values[valueIx];
}
float getAttrValFloat(const char *key, int valueIx, struct keyedAttributeList *attributes)
{
    variableType *rval =  getAttrVal(key, valueIx, attributes);
    if (rval == NULL) 
    {
        fprintf(stderr, "attribute \"%s\" not found\n", key);
        abort();
    }
    return rval->fval;
}
int getAttrValInt(const char *key, int valueIx, struct keyedAttributeList *attributes)
{
    variableType *rval =  getAttrVal(key, valueIx, attributes);
    if (rval == NULL) 
    {
        fprintf(stderr, "attribute \"%s\" not found\n", key);
        abort();
    }
    return rval->ival;
}
struct keyedAttribute *popAttr(const char *key, struct keyedAttributeList *attributes)
{
    /* returns the result of getAttr(key, list), removing it from list if non-null */
    struct keyedAttribute *target = getAttr(key, attributes);
    if (target != NULL)
    {
        int ix = getAttrIndex(key, attributes);
        /*fprintf(stderr, "popAttr: ix %d out of %ld\n", ix, attributes->nattributes);
        fprintf(stderr, "pre:"); printAttributeList(stderr, attributes);*/

        int nright = (attributes->nattributes-1) - ix;
        if (nright > 0) // shift left
        {
            struct keyedAttribute **src = &(attributes->list[ix+1]);
            struct keyedAttribute **dest = &(attributes->list[ix]);
            memmove(dest, src, sizeof(struct keyedAttribute *) * nright);
        }
        attributes->nattributes--;
        /*fprintf(stderr, "post:"); printAttributeList(stderr, attributes);*/
    }
    return target;
}
void delAttr(const char *key, struct keyedAttributeList *attributes)
{
    /* deletes the item from the list */
    struct keyedAttribute *target = popAttr(key, attributes);
    freeKeyedAttribute(&target);
}

void printAttributeList(FILE *out, struct keyedAttributeList *attributes)
{
    fprintf(out, "list with %ld elements:\n", attributes->nattributes);
    for(int i=0; i<attributes->nattributes; i++)
    {
        fprintf(out, "%d: ", i);
        printKeyedAttribute(out, attributes->list[i]);
    }
    fprintf(out, "\n");
}

void printKeyedAttribute(FILE *out, struct keyedAttribute *attr)
    /*replace this fucking function*/
{
    const char *formats[] = {"%si","%sf","\"%s\"","%s!!"};
    fprintf(out, "<attr %s: ", attr->key);
    for (int i=0; i< attr->nvalues; i++)
    {
        if (i) fprintf(out, ", ");
        variableType *v = attr->values[i];
        fprintf(out, formats[v->mode], v->sval);
    }
    fprintf(out, ">\n");
}
int attribute_c_main(int argc, char **argv)
{
    struct keyedAttributeList * attributes = NULL;
    char *defaultdata[] = {"scrambled", "eggs"};
    attributes = addAttrFromStrings("default", defaultdata, 2, attributes);
    for (int i=1; i < argc; i++)
    {
        char **terms = NULL;
        int nterms = 0;
        char *arg = strdup(argv[i]);
        terms = strsep_all(arg, "=", &nterms);
        char *key = NULL;
        char **values = NULL;
        int nvalues = 0;

        if(nterms > 0) 
        {
            key = terms[0];
            if(nterms == 2) 
            {
                values = strsep_qsall(terms[1], &nvalues);
            }
            attributes = addAttrFromStrings(key, values, nvalues, attributes);
            printAttributeList(stdout, attributes);
        }
        free(terms);
        free(values);
        free(arg);
    }
    //struct keyedAttribute *attr = getAttr("a", attributes);
    printAttributeList(stdout, attributes);
    //float val = 11;
    attributes = addAttr(keyedAttributeFromArgs("arg1", "s", "new arg"), attributes);

    printAttributeList(stdout, attributes);
    delAttr("default", attributes);
    printAttributeList(stdout, attributes);
    freeKeyedAttributeList(&attributes);
    return 0;
}
void print_delim_list(char **items, int nitems, char delim)
{
    for (int i=0; i < nitems; i++)
    {
        if (i==0)
            printf("%s", items[i]);
        else
            printf(",%s", items[i]);
    }
}
