#ifndef VARIABLETYPE_H
#define VARIABLETYPE_H
typedef struct 
{
    int mode;
    int ival;
    float fval;
    char * sval;
} variableType;
void freeVariableType(variableType **);
variableType * variableTypeFromStr(char *);
variableType * variableTypeFromFloat(float);
variableType * variableTypeFromInt(int);
variableType * variableTypeFromMode(char *, int);
const char *valueType(variableType *val);
enum { INT, FLOAT, STRING, CONVERSION_ERROR};

// typing _to_ an array of variableTypes
#include <stdarg.h>
variableType **vcreateVariableTypeArray(const char *fmt, va_list ap);
variableType **createVariableTypeArray(const char *fmt, ...);

// typing _from_ an array of variableTypes
float *variableTypes_toFloats(variableType **array, int nitems);
char **variableTypes_toStrings(variableType **array, int nitems);
int *variableTypes_toInts(variableType **array, int nitems);
#endif
