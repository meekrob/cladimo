#include <limits.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <err.h>
#include <errno.h>
#include <stdarg.h>
#include "variableType.h"
#include "append_to_array.h"

const char* vartypes[] = {"int", "float", "string", "conversion error"};

const char *valueType(variableType *val)
{
    return vartypes[val->mode];
}

void freeVariableType(variableType **pptr)
{
    if((pptr != NULL) && (*pptr != NULL))
    {
        variableType *ptr = *pptr;
        free(ptr->sval);
        free(ptr);
    }
    *pptr = NULL;
}

char **variableTypes_toStrings(variableType **array, int nitems)
{
    char **arr = calloc(sizeof(char*), nitems);
    for(int i=0; i<nitems; i++)
        arr[i] = strdup(array[i]->sval);
    return arr;
}
int *variableTypes_toInts(variableType **array, int nitems)
{
    int *arr = calloc(sizeof(int), nitems);
    for(int i=0; i<nitems; i++)
        arr[i] = array[i]->ival;
    return arr;
}
float *variableTypes_toFloats(variableType **array, int nitems)
{
    float *arr = calloc(sizeof(float), nitems);
    for(int i=0; i<nitems; i++)
        arr[i] = array[i]->fval;
    return arr;
}

variableType **createVariableTypeArray(const char *fmt, ...)
{
    int nitems = strlen(fmt);
    variableType **array = malloc(sizeof(variableType *)*nitems);

    int array_i = 0;
    va_list ap;
    va_start(ap, fmt);
    while(*fmt)
    {
        switch(*fmt++)
        {
            case 'd':
                array[array_i++] = variableTypeFromInt(va_arg(ap, int));
                break;
            case 'f':
                array[array_i++] = variableTypeFromFloat((float)va_arg(ap, double));
                break;
            case 's':
                array[array_i++] = variableTypeFromStr(va_arg(ap, char*));
                break;
        }
    }
    va_end(ap);
    return array;
}
variableType **vcreateVariableTypeArray(const char *fmt, va_list ap)
{
    int nitems = strlen(fmt);
    variableType **array = malloc(sizeof(variableType *)*nitems);

    int array_i = 0;
    while(*fmt)
    {
        switch(*fmt)
        {
            case 'i':
            case 'd':
                array[array_i++] = variableTypeFromInt(va_arg(ap, int));
                break;
            case 'f':
                array[array_i++] = variableTypeFromFloat((float)va_arg(ap, double));
                break;
            case 's':
                array[array_i++] = variableTypeFromStr(va_arg(ap, char*));
                break;
            default:
                fprintf(stderr, "error: variableType code `%c' is unrecognized\n", *fmt);
                abort();
        }
        fmt++;
    }
    va_end(ap);
    return array;
}

variableType * variableTypeFromStr(char *str)
{
    variableType *new = malloczero(new);
    new->sval = strdup(str);
    new->mode = STRING;

    char *endptr = NULL;

    // floating-point
    float fval = (float)strtof(str, &endptr);
    if (errno == ERANGE)
    {
        new->mode = CONVERSION_ERROR;
        return new;
    }
    if (*endptr != 0)//non-numeric
    {
        new->mode = STRING;
        return new;
    }
    if (fval == HUGE_VALF)// overflow
    {
        new->mode = CONVERSION_ERROR;
        return new;
    }
    if (fval == -HUGE_VALF)// overflow
    {
        new->mode = CONVERSION_ERROR;
        return new;
    }
    // integral
    endptr = NULL;
    long lval = strtol(str, &endptr, 10);
    if (lval == LONG_MAX || lval == LONG_MIN)
    {
        new->mode = CONVERSION_ERROR;
        return new;
    }

    if ((float)lval == fval)
    {
        new->mode = INT;
        new->ival = (int)lval;
        new->fval = fval;
    }
    else
    {
        new->mode = FLOAT;
        new->ival = (int)fval;
        new->fval = fval;
    }
    return new;
}

variableType * variableTypeFromFloat(float fval)
{
    variableType *new = malloczero(new);
    new->mode = FLOAT;
    new->fval = fval;
    new->ival = (int)fval;
    asprintf(&(new->sval), "%f", fval);
    return new;
}
variableType * variableTypeFromInt(int ival)
{
    variableType *new = malloczero(new);
    new->mode = INT;
    new->ival = ival;
    new->fval = (float)ival;
    asprintf(&(new->sval), "%i", ival);
    return new;
}
variableType * variableTypeFromMode(char *str, int mode)
{
    variableType *new = variableTypeFromStr(str);
    if (new->mode == mode)
        return new;
    else
        free(new);
    return NULL;
}

void printvariable(FILE *out, variableType *v)
{
    fprintf(out, "%p:\n", v);
    fprintf(out, "  int mode %d; // %s\n", v->mode, vartypes[v->mode]);
    fprintf(out, "  int ival %d;\n", v->ival);
    fprintf(out, "  float fval %.3f;\n", v->fval);
    fprintf(out, "  char *sval: \"%s\";\n", v->sval);
}

