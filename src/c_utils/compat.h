#ifndef COMPAT_H
#define COMPAT_H
char * compat_strndup (char const *s, size_t n);
size_t compat_strnlen (const char *string, size_t maxlen);
#endif
