/*
   Additions to string.h strsep-like functions.
   
*/
#ifndef STRSEP_ALL
#define STRSEP_ALL
#include <string.h>
#define WHITESPACE " \t\n\v\f\r"

char ** strsep_all(char *string, const char *delim, int *nsep);
/* Return a char** array resulting from the parsing of the string */

char *strsep_qs(char **stringp);
/* Separate on whitespace, preserving quoted strings */

char ** strsep_qsall(char *string, int *nsep);
/* Like strsep_all, but using strsep_qs */
#endif
