#ifndef LINESPLIT_H
#define LINESPLIT_H
char **partition_strings(char *line, int n, int *nfields);
inline int append_to_array(void *data, void *lineptr, int arrlen, int nitems, size_t itemsize, size_t *linesize, int chunk);

char ** linesplit(FILE *fp, int *nfields);
char ** stringsplit(char *sentence, int *nfields);
/* acts like getline to read in a line of characters from a file,
   and then delivers an array of strings separated by white space.
   stringsplit is the equivalent that works on a line instead of a file.
*/

char ** linesplit_tabs(FILE *fp, int *nfields);
/* splitting is done as above but tabs are replaced by terminators, i.e., multiple tabs are not condensed
*/

char ** optionsplit(FILE *fp, int *nfields);
/*If the line contains tabs, run linesplit_tabs, else run stringsplit (on whitespace) on the single tab-split field 
 */

void fieldsfree(char ***fptr);
/* a safe function to free the string array */

char* join_fields(char *delim, char **fields, int nfields);
/* Creates a single string out of the fields, concatenated by delim.
*/

int str_match(char* a, char*b);
/* returns false upon first mismatch, including unequal length. */

void lstrip(char *line);
/* shift non-whitespace start of the line to the front. The trailing space is zeroed. */

int one_of(char* tag, char* list[], int listlen);
/* does list contain the string tag. uses str_match for direct matching. */

void debugfields(char **fields, int nfields);
#endif
