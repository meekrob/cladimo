#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "strsep_all.h"
#include "append_to_array.h"
char *strsep_qs(char **stringp)
{
    if (*stringp == NULL) return NULL;
    int len = strlen(*stringp);
    char *orig = *stringp;

    int first = strspn(orig, WHITESPACE);
    if (first < len)
    {

        if ((orig[first] == '"')||(orig[first] == '\''))
        {   /* quoted string */
            char endquote[] = {orig[first], '\0'};
            char *left = &(orig[first+1]);
            char *right = strpbrk(left, endquote);
            if (right != NULL)
            {   /* closed string */
                *right = '\0';
                *stringp = right + 1;
                return left;
            }
            else
            {   /* unclosed string */
                *stringp = NULL;
                return left;
            }
        }
        else
        {   /* non-whitespace character */
            char *left = &(orig[first]);
            char *right = strpbrk(left, WHITESPACE);
            if (right != NULL)
            {   /* space follows */
                *right = '\0';
                *stringp = right + 1;
                return left;
            }
            else
            {   /* word ends the string */
                *stringp = NULL;
                return left;
            }
        }
    }
    /* all whitespace, or blank */
    *stringp = NULL;
    return NULL;
}
void shiftLeftOverWS(char *string)
{
    size_t s_t = strlen(string);
    int i = 0;
    while (isspace(string[i]))
    {
        i++;
    }
    if (i > 0)
    {
        memmove(string, &(string[i]), (s_t - i)+1);
    }
}
char ** strsep_qsall(char *string, int *nsep)
{
    int chunk = 5;
    size_t arr_t = 0;
    char **array = NULL;
    char *ap = NULL;
    *nsep = 0;
    shiftLeftOverWS(string);
    while ((ap = strsep_qs(&string)) != NULL)
    {
        *nsep  = append_to_array(&ap, &array, *nsep, 1, sizeof(char*), &arr_t, chunk);
    }
    return array;
}
char ** strsep_all(char *string, const char *delim, int *nsep)
{
    int chunk = 5;
    size_t arr_t = 0;
    char **array = NULL;
    char *ap = NULL;
    *nsep = 0;
    shiftLeftOverWS(string);
    while ((ap = strsep(&string, delim)) != NULL)
    {
        if (ap[0] != '\0')
        {
            *nsep  = append_to_array(&ap, &array, *nsep, 1, sizeof(char*), &arr_t, chunk);
        }
    }
    return array;
}
