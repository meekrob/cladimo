#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "matrix.h"
#ifndef MIN
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#endif
#ifndef MAX
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#endif
// NAN
#define DOUBLE_NAN (0.0/0.0)

void free_matrix(float*** matrix)
{
    if(*matrix==NULL) return;
    free((*matrix)[0]);
    free(*matrix);
    *matrix=NULL;
}
void free_char_table(char****table, int rows)
{
    int i;
    for(i=0;i<rows;i++) free((*table)[i][0]);
    free((*table)[0]);
    free(*table);
    *table=NULL;
}
void print_matrix(FILE* out, float **matrix, int nrows, int ncols)
{
    int i,j;
    for (i=0; i<nrows; i++) 
    {
        for(j=0; j<ncols; j++)
        {
            if (matrix[i][j] == (int)matrix[i][j])
            {
                fprintf(out, "%d\t", (int)matrix[i][j]);
            }
            else 
            {
                fprintf(out, "% .2f\t", matrix[i][j] );
            }
        }
        fputc('\n',out);
    }
}
void print_char_table(FILE* out, char*** table, int nrows, int ncols)
{
    int i,j;
    for (i=0; i<nrows; i++) 
    {
        for(j=0; j<ncols; j++) fprintf(out, "%s ", table[i][j] );
        fputc('\n',out);
    }
}
void print_char_table_transposed(FILE* out, char*** table, int nrows, int ncols)
{
    int i,j;
    for(j=0; j<ncols; j++) 
    {
        for (i=0; i<nrows; i++) fprintf(out, "%s ", table[i][j] );
        fputc('\n',out);
    }
}
float array_max(float *arr, int len)
{
    int i;
    float max = -(1.0/0.0);
    for (i=0; i<len; i++)
    {
        if (arr[i] > max) max = arr[i];
    }
    return max;
}
float array_min(float *arr, int len)
{
    int i;
    float min = (1.0/0.0);
    for (i=0; i<len; i++)
    {
        if (arr[i] < min) min = arr[i];
    }
    return min;
}
int array_argmax(float *arr, int len)
{
    int i;
    int max_i=0;
    float max = -(1.0/0.0);
    for (i=0; i<len; i++)
    {
        if (arr[i] > max) 
        {
            max = arr[i];
            max_i = i;
        }
    }
    return max_i;
}
int array_argmin(float *arr, int len)
{
    int i;
    int min_i=0;
    float min = (1.0/0.0);
    for (i=0; i<len; i++)
    {
        if (arr[i] < min) 
        {
            min = arr[i];
            min_i = i;
        }
    }
    return min_i;
}
float array_sd(float* arr, int len)
{
    int i;
    float mean = array_mean(arr,len);
    float var = 0;
    for (i=0;i<len;i++) var += (arr[i] - mean)*(arr[i] - mean);
    return sqrt(var/(len-1));
}
float array_sum(float* arr, int len)
{
    int i;
    float sum = 0.0;
    for (i=0;i<len;i++) sum += arr[i];
    return sum;
}
float array_mean(float* arr, int len)
{
    return array_sum(arr,len)/len;
}
float array_cov(float* m1, float* m2, int len)
{
    float mean1 = array_mean(m1,len);
    float mean2 = array_mean(m2,len);
    int i; float ss = 0; //sum of squares
    for (i=0;i<len;i++) ss += ((m1[i] - mean1) * (m2[i] - mean2)); 
    return ss/(len-1);
}
float array_cor(float*m1, float*m2, int len)
{
    return array_cov(m1,m2,len) / (array_sd(m1,len)*array_sd(m2,len));
}
float *unstack_matrix(float** matrix, int nrows, int ncols)
{ 
    int i,j;
    float* arr = calloc(nrows*ncols,sizeof(float));
    if(arr==NULL) return NULL;
    int count=0;
    for (i=0; i<nrows; i++) 
        for(j=0; j<ncols; j++)
            arr[count++] = matrix[i][j];
    return arr;
}
float** nans(int nrows, int ncols)
{
    float* arr = calloc(nrows*ncols,sizeof(float));
    int i; for(i=0;i<nrows*ncols;i++) arr[i] = DOUBLE_NAN;
    return shape_array(arr, nrows, ncols);
}
float** zeroes(int nrows, int ncols)
{
    float* arr = calloc(nrows*ncols,sizeof(float));
    return shape_array(arr, nrows, ncols);
}
float** covariance_matrix(float** m, int rows, int cols)
{
    return matrix_cov(m,m,cols,cols,rows);
}
float** matrix_cov(float** m1, float** m2, int x1cols, int x2cols, int nrows)
{
    float** outv = zeroes(x1cols,x2cols);
    // transpose to column-ordered 2d arrays
    float** cox = transpose_matrix(m1, nrows, x1cols);
    float** coy = transpose_matrix(m2, nrows, x2cols);
    int x1col, x2col;
    for (x1col=0;x1col<x1cols;x1col++)
    {
        for(x2col=x1col;x2col<x2cols;x2col++)
        {
            int i = x1col;
            int j = x2col;
            outv[i][j] = outv[j][i] = array_cov(cox[i],cox[j],nrows);
        }
    }
    free_matrix(&cox);
    free_matrix(&coy);
    return outv;
}
float matrix_sum(float** m, int rows, int cols)
{
    float sum = 0.;
    float* flat = m[0]; 
    int i=0; while(i<rows*cols) sum += flat[i++];
    return sum;
}
float** shape_array(float* arr, int nrows, int ncols)
{
    int i;
    float** new_matrix = calloc(nrows, sizeof(float*));
    int count=0;
    for (i=0; i<nrows; i++) 
    {
        new_matrix[i] = &arr[count];
        count += ncols;
    }
    return new_matrix;
}
float **copy_flat_matrix(float**matrix,int nrows,int ncols)
{
    float* arr = calloc(nrows*ncols,sizeof(float));
    memcpy(arr, matrix[0], nrows*ncols*sizeof(float));
    return shape_array(arr, nrows, ncols);
}
float **copy_cell_matrix(float**matrix,int nrows,int ncols)
{
    int i,j;
    //float* arr = malloc(nrows*ncols*sizeof(float));
    float* arr = calloc(nrows*ncols,sizeof(float));
    if(arr==NULL) return NULL;
    float** new_matrix = calloc(nrows, sizeof(float*));
    int count=0;
    for (i=0; i<nrows; i++) 
    {
        new_matrix[i] = &arr[count];
        for(j=0; j<ncols; j++)
        {
            arr[count++] = matrix[i][j];
        }
    }
    return new_matrix;
}
float **copy_matrix(float**matrix,int nrows,int ncols)
{
    return copy_flat_matrix(matrix, nrows, ncols);
}
void matrix_scale_rows(float**matrix, int nrows, int ncols)
{
/* divide each cell by its row sum */
    int i,j;
    for (i=0;i<nrows;i++)
    {
        float rowsum=0;
        for(j=0;j<ncols;j++) rowsum += matrix[i][j]; 
        for(j=0;j<ncols;j++) matrix[i][j] /= rowsum;
    }
}
void matrix_scale_cols(float**matrix, int nrows, int ncols)
{
/* divide each cell by its column sum */
    int i,j;
    for (i=0;i<ncols;i++)
    {
        float colsum=0;
        for(j=0;j<nrows;j++) colsum += matrix[j][i];
        for(j=0;j<nrows;j++) matrix[j][i] /= colsum; 
    }
}
float **transpose_matrix(float**matrix, int nrows, int ncols)
{
    int i,j;
    //float* arr = malloc(nrows*ncols*sizeof(float));// new flat array
    float* arr = calloc(nrows*ncols,sizeof(float));// new flat array
    float** new_matrix = calloc(ncols, sizeof(float*));// new table
    int d=0;
    for (j=0;j<ncols;j++)
    {
        new_matrix[j] = &arr[j*nrows];
        for (i=0; i<nrows; i++) arr[d++] = matrix[i][j];
    }
    return new_matrix;
}
char*** read_char_table(FILE* fp, int* rows, int* cols)
{
    int chunk = 8;
    int flatsize = chunk;
    int words = 0;
    int width = 0;
    char linebuf[1024];
    char **strings = calloc(chunk,sizeof(char*));
    char* line = NULL;
    int textsize = 0;
    while ((line = fgets(linebuf,1024,fp)) != NULL)
    {
        *(index(line,'\n')) = 0;
        if(strlen(line)==0) break;
        width = 0;
        char* sptr;
        // split fields
        //char *scratch = malloc(sizeof(char)*1024);
        char *scratch = calloc(1024,sizeof(char));
        char* next_field = scratch;
        while((sptr = strsep(&line, " \t")) != NULL)
        {
            if (strlen(sptr)>0) 
            {
                if (words+1 > flatsize) 
                {
                    strings = realloc(strings,sizeof(char*)*(flatsize+chunk));
                    flatsize += chunk;
                }
                // append fields
                strings[words] = strcpy(next_field,sptr);
                next_field = next_field + strlen(sptr) +1; 
                textsize += strlen(sptr) + 1;
                //fprintf(stderr,"%p=%s<\n",&strings[i],strings[i]);
                words++;
                width++;
            }
        }
    }
    *cols = width;
    *rows = words / width;
    char*** table = calloc(*rows, sizeof(char**));
    int r = *rows;
    int c = *cols;
    int i;
    for(i=0;i<r;i++) table[i] = &strings[i*c];
    strings=NULL;
    return table;
}
float** read_matrix( FILE* fp, int *rows, int* cols)
{
    char linebuf[1024];
    char* line = NULL;
    int chunk = 8;
    //float *data = malloc(sizeof(float)*chunk);
    float *data = calloc(chunk,sizeof(float));
    int flatsize = chunk;
    int i = 0;
    int width = 0;
    int brk = 0;
    int lines = 0;
    while ((line = fgets(linebuf,1024,fp)) != NULL)
    {
        int linelen = strlen(line);
        *(index(line,'\n')) = 0;
        if(strlen(line)==0) break;
        width = 0;
        char* sptr;
        while((sptr = strsep(&line, " \t")) != NULL)
        {
            if (strlen(sptr)>0) 
            {
                char* nptr = sptr;
                if (i+1 > flatsize) 
                {
                    data = realloc(data,sizeof(float)*(flatsize+chunk));
                    flatsize += chunk;
                }
                char* endptr = NULL;
                data[i] = strtod(nptr, &endptr);
                if (nptr==endptr) 
                {
                    fseek(fp, -linelen, SEEK_CUR);
                    brk = 1;
                    break;
                }
                i++;
                width++;
            }
        }
        if (brk) break;
        lines++;
    }
    if (i==0 && width==0) 
    {
        free(data);
        return NULL;
    }
    // reduce array to actual size
    float* arr = calloc(i, sizeof(float));
    int j;
    for (j=0;j<i;j++) arr[j] = data[j];
    free(data);
    data = arr;
    arr = NULL;
    if ((width==0) && i>0)
    {
        *cols = i/lines;
        *rows = lines;
    }
    else if (i/width != (float)i/width) 
    { // irregular dimensions encountered
        *cols = i;
        *rows = 1;
    }
    else 
    {
        *cols=width;
        *rows=i/width;
    }
    //return data;
    float** matrix = calloc(*rows, sizeof(float*));
    int r = *rows;
    int c = *cols;
    for(i=0;i<r;i++) matrix[i] = &data[i*c];
    data=NULL;
    return matrix;
}
void printvec(float* vec, int len)
{
    int i=0; while(i<len) fprintf(stdout, "%d\t", (int)vec[i++]);
}

