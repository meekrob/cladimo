#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pwm.h"
#include "matrix.h"
#include "linesplit.h"
#define true 1
#define false 0
#define FLAT_SIZE 252

#define LISTLEN(list) (sizeof(list)/sizeof(list[0]))
struct pwmll
{
    PWM *pwm;
    float array[FLAT_SIZE];
    int arr_size;
    int row_size; // 4 if A,C,G,T per row
    struct pwmll *next;
};

struct pwmll * new_item()
{
    struct pwmll *it = malloc(sizeof(struct pwmll));
    int i=0; while (i < FLAT_SIZE) it->array[i++] = 0;
    it->arr_size = 0;
    it->row_size = 0;
    it->pwm = new_pwm();
    it->next = NULL;
    return it;
}
void debugll(struct pwmll *list)
{
    fprintf(stderr, "*list %p\n", list);
    fprintf(stderr, "*->arr_size %d\n", list->arr_size);
    fprintf(stderr, "*->array[0] %.2f\n", list->array[0]);
}
int str_match(char* a, char*b)
{
    int i;
    int a_len = strlen(a);
    int b_len = strlen(b);
    if (a_len != b_len) return 0;
    for(i=0; i< a_len; i++) if (a[i] != b[i]) return 0;
    return 1;
}

void lstrip(char *line)
{
    int len = strlen(line);
    char *ch = line;
    while (*ch == ' ') ch++;
    memmove(line, ch, sizeof(char) * (len - (ch - line)));
    line[len - (ch - line)] = 0;
}

int one_of(char* tag, char* list[], int listlen)
{
    int i;
    for (i=0; i< listlen; i++)
        if (str_match(tag,list[i])) return 1;

    return 0;
}

void matrix_row(struct pwmll *list, char** fields, int nfields)
{
    if (!list->row_size) list->row_size = nfields;
    int pos;
    char *endptr;
    for (pos=0;pos<nfields; pos++)
        list->array[list->arr_size++] = strtod(fields[pos], &endptr);
}
void make_jaspar_label(char* label, char **fields, int nfields, char *join);
void jaspar_header(struct pwmll **listptr, char** fields, int nfields)
{
    struct pwmll *list = *listptr;
    *listptr = new_item(); list = *listptr;
    strncpy(list->pwm->accession, &(fields[0][1]), LONGLINE-1);
    lstrip(list->pwm->accession);
    if (nfields == 5)
    {
        /* fields[0] acc */
        /* fields[1] information content */
        strncpy(list->pwm->name, fields[2], LONGLINE-1); // name
        /* fields[3] class */
        /* fields[4] ';'- delimited data */
        make_jaspar_label(list->pwm->label, fields, nfields, "\t");
    }
    else if (nfields>1) 
    {
        strncpy(list->pwm->name, fields[1], LONGLINE-1);
        make_jaspar_label(list->pwm->label, fields, nfields, " ");
    }
    else if (nfields==1)
    {
        strcpy(list->pwm->name, list->pwm->accession);
        make_jaspar_label(list->pwm->label, fields, nfields, " ");
    }
}
void jaspar_body(struct pwmll *list, char** fields, int nfields)
{
    matrix_row(list, fields, nfields);
}

PWM **process_list(struct pwmll *list, int *npwm)
{
    PWM **rval = NULL;
    struct pwmll *headptr = list;
    *npwm = 0;
    for(; list != NULL; list = list->next)
    {
        int i, len, is_long_matrix = 0;
        float *flat_array = calloc(list->arr_size, sizeof(float));
        memcpy(flat_array, list->array, list->arr_size * sizeof(float));

        is_long_matrix = list->row_size == 4 ? 1 : 0; // row_size is 4 for long matrices, pwm length for wide matrices
        if (is_long_matrix)
            len = list->arr_size / 4;
        else
            len = list->row_size;

        // wide matrices (4 rows) are already in the right orientation, longs must be transposed
        if (is_long_matrix)
        {
            float** a = shape_array(flat_array, len, 4);
            for(i=0;i<len;i++) 
                list->pwm->motif[i] = consensus_char(a[i]);
            list->pwm->motif[i] = 0;

            list->pwm->matrix = transpose_matrix(a, len, 4);
            free_matrix(&a);
        }
        else
        {
            list->pwm->matrix = shape_array(flat_array, 4,len);
            float** a = transpose_matrix(list->pwm->matrix, 4, len);
            for(i=0;i<len;i++) list->pwm->motif[i] = consensus_char(a[i]);
            list->pwm->motif[i] = 0;
            free_matrix(&a);
        }

        list->pwm->length = len;
        pwm_transform(list->pwm);
        list->pwm->threshold = thresholdFunc(list->pwm);
        (*npwm)++;
    }
    rval = calloc(*npwm, sizeof(PWM*));
    *npwm = 0;
    list = headptr;
    while(list!=NULL)
    {
        rval[(*npwm)++] = list->pwm;
        list->pwm = NULL;
        struct pwmll *tmp = list;
        list = list->next;
        tmp->next = NULL;
        free(tmp);
    }
    return rval;
}

void make_jaspar_label(char* label, char **fields, int nfields, char *join)
{
    memmove(fields[0], &(fields[0][1]), strlen(fields[0]) * sizeof(char)); // shift the whole string 1 position to the left to exclude the '>'
    char* newlabel = join_fields(join, fields, nfields);
    strncpy(label, newlabel, LONGLINE-1);
    free(newlabel);
    
}
struct pwmll * read_jaspar_format(FILE* in, struct pwmll **listptr)
{
    struct pwmll *headptr = *listptr;
    char **fields = NULL;
    int nfields=0;
    fields = optionsplit(in, &nfields);

    while (! feof(in))
    {
        if (fields)
        {
            if (fields[0][0] == '>') 
            {
                if (*listptr) listptr = &((*listptr)->next);
                jaspar_header(listptr, fields, nfields);
            }
            else 
                jaspar_body(*listptr, fields, nfields);
        }

        fieldsfree(&fields);
        fields = optionsplit(in, &nfields);
    }
    fieldsfree(&fields);
    return headptr;
}

enum {AC, NA, DE };
char* tfx_headers[] = {"AC", "NA", "DE"};
int match_tfx_header(char* tag)
{
    if (str_match(tag, "AC")) return AC;
    if (str_match(tag, "NA")) return NA;
    if (str_match(tag, "DE")) return DE;
    return -1;
}

void transfac_header(char *tag, struct pwmll **listptr, char** fields, int nfields)
{
    char *description;
    struct pwmll *list = *listptr;

    switch(match_tfx_header(tag))
    {
        case AC: 
            *listptr = new_item(); list = *listptr;
            strncpy(list->pwm->accession, fields[1], LONGLINE-1); break;
        case NA: 
            strncpy(list->pwm->name, fields[1], LONGLINE-1); break;
        case DE: 
            description = join_fields(" ", &(fields[1]), nfields-1);
            strncpy(list->pwm->label, description, LONGLINE-1);
            free(description);
            break;
    }
}

void transfac_body(char *tag, struct pwmll ***listptr, char **fields, int nfields)
{
    // end of record
    if (str_match(tag, "//"))  
    {
        if (**listptr != NULL) *listptr = &((**listptr)->next);
    }
    // possible matrix line
    else
    {
        // check for numeric tag
        char *endptr = NULL;
        strtol(tag, &endptr, 10); 
        //if (*endptr==0) matrix_row(**listptr, fields, nfields);
        if (*endptr==0) matrix_row(**listptr, fields, 4);
        /*else if (! str_match(tag,"XX"))
        {
            fprintf(stderr, "%s data skipped:", tag);
            int i = 0;
            while(i < nfields) fprintf(stderr, "%s ", fields[i++]);
            fputc('\n', stderr);
        }*/
    }
}
struct pwmll * read_transfac_format(FILE* in, struct pwmll **listptr)
{
    struct pwmll *headptr = *listptr;
    int nfields=0;
    char **fields = optionsplit(in, &nfields);

    while (!feof(in))
    {
        if (fields)
        {
            char* tag = fields[0];
            if (headptr != NULL) 
            {
                if (one_of(tag, tfx_headers, LISTLEN(tfx_headers)))
                    transfac_header(tag, listptr, fields, nfields);
                else
                    transfac_body(tag, &listptr, &(fields[1]), nfields-1);
            }
            else if (str_match(fields[0], "AC")) // header is unitialiized, happens when AC was not the first tag in the file
            {
                transfac_header(tag, listptr, fields, nfields);
                headptr = *listptr;
            }
        }

        fieldsfree(&fields);
        fields = optionsplit(in, &nfields);
    }
    fieldsfree(&fields);
    return headptr;
}

PWM ** read_pwm_file(char* filename, int *npwm)
{
    FILE * in = fopen(filename, "r");
    if (in==NULL) return NULL;
    struct pwmll *list = NULL;
    char **fields = NULL;
    int nfields = 0;

    while (! feof(in))
    {
        fields = optionsplit(in, &nfields);
        if (fields)
        {
            if (fields[0][0] == '>')
            {
                jaspar_header(&list, fields, nfields);
                list = read_jaspar_format(in, &list);
            }
            else if (str_match(fields[0], "VV"))
            {
                list = read_transfac_format(in, &list);
            }
            else if (str_match(fields[0], "AC"))
            {
                transfac_header(fields[0], &list, fields, nfields);
                list = read_transfac_format(in, &list);
            }
            else
            {
                fprintf(stderr, "couldn't recognize PWM format\n");
                fprintf(stderr, "Header must start with \">\" (for JASPAR-like) or \"AC\" (for TRANSFAC)\n");
                fieldsfree(&fields);
                fclose(in);
                exit(1);
            }
            fieldsfree(&fields);
        }
        fieldsfree(&fields);
    }
    fclose(in);

    return process_list(list, npwm);
}
