#ifndef PWM_H
#define PWM_H

#include <stdio.h>
#define Large_Buf (1024*8)
#define Max_align_size Large_Buf*2
#ifndef MIN
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#endif
#ifndef MAX
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#endif
#define MAX_MOTIF 64
#define LONGLINE 1024

#define GLOBAL_A 0.28
#define GLOBAL_C 0.22
#define GLOBAL_G 0.22
#define GLOBAL_T 0.28
#define DEFAULT_PWM_THRESHOLD 0.90

// -INF
#define DOUBLE_MIN -(1.0/0.0)
// INF
#define DOUBLE_MAX  (1.0/0.0) 
// NAN
#define DOUBLE_NAN (0.0/0.0)
typedef char longline[LONGLINE]; 
typedef long long bitsequence;
#define MAXWORDSIZE (4 * sizeof(bitsequence))

/* _pwm_indexing_ */
#define _pwm_indexing_ extern const signed char degenerate_symbol_bit_encoding[256]; extern const signed char dna_encoding[256]; 
#define index_len(a) (1 << (2*a->length))
#define mscore(l, s) (((1<<(l)) & degenerate_symbol_bit_encoding[(int)s]) != 0)
#define letterval(a) dna_encoding[(int)a]

/* _pwm_loop_ Oft-used pwm loop macros */
#define _pwm_loop_ int cell,base,position; cell = base = position = 0;
#define base_wise base=cell/pwmlen,position=cell%pwmlen
#define position_wise base=cell%4,position=cell/4;
#define for_each_base for(base=0;base<4;base++)
#define for_each_position(PWMLEN) for(position=0;position<PWMLEN;position++)
#define for_each_base_position(PWMLEN) for (cell=0, base_wise; cell <PWMLEN*4; cell++, base_wise)
#define for_each_position_base(PWMLEN) for (cell=0, position_wise; cell <PWMLEN*4; cell++, position_wise)

/* pwm_scoring */
#define pwm_match(nt, i, motif) (motif->matrix[base_offset(nt)][i])

typedef struct pwm_obj
{
    longline accession;
    longline name;
    longline label;
    longline motif;
    longline library_name;
    int length;
    float **matrix;
    float background[4];
    float min_score;
    float max_score;
    float threshold;
    float information_content;
    float base_counts[4];
    float base_pseudocounts[4];
    float *index;
    int Nsites;

} PWM;
char* pwm_max_char(PWM *pwm_1);

PWM *new_pwm_motif(const char *);
PWM *new_pwm();
PWM *cpy_pwm(PWM *dest, PWM *src);
void free_pwm(PWM **);
float **format_matrix(float* arr, int arr_len);
int base_offset(char s);
PWM * read_jaspar_with_background(FILE* in, float,float,float,float);
PWM *read_jaspar(FILE *in);
char * human_readable(long size);
float* pwm_compare(PWM* pwm1, PWM* pwm2, int* comparisons);
char consensus_char(float* v);

void pwm_transform(PWM* pwm);
void pwm_transform_with_background(PWM*, float, float, float, float);
void pwm_retransform(PWM *pwm, float A, float C, float G, float T);
float pwm_min(PWM *pwm);
float pwm_max(PWM *pwm);
void print_pwm(FILE* out, PWM *pwm);
void print_deparse(FILE *out, bitsequence index, int windowsize);
PWM *read_preformatted(FILE *in);

float pwm_stringscore(char *word, int strand, PWM *pwm);
float wordscore(unsigned char*, PWM *);
float *scoreseq_both_strands(unsigned char *seq, int len, PWM *pwm);
float *matchseq_both_strands(unsigned char *seq, int len, char *pattern);

float wordmatch(unsigned char* word, char* pattern, int len, int sign);
int patmatch(unsigned char n, char symbol);
int validchar(unsigned char c);
float GC_content(unsigned char* wordstart, int windowsize);
float *GC_scoreseq(unsigned char* seq, int len, int windowsize);
void fieldsfree(char ***fptr);
int valid_motif(char *);
float **pwm_counts(PWM *);
float **pwm_pmatrix(PWM *);
float max_score_left(PWM *pwm, int position);
float max_score_right(PWM *pwm, int position);
unsigned char *revcomp(unsigned char *seq);
float totalICfunc(PWM *pwm);
float** ICfunc(PWM *pwm);
float estimated_threshold_3_per_kb(PWM *pwm);
float thresholdFunc(PWM *pwm);

/* indexing */
void pwm_free_index(PWM *pwm);
float *build_index(PWM *pwm);
bitsequence dnashiftone(char, bitsequence, int);
bitsequence uchar_to_bitseq(char *, int);
void bitseq_to_uchar(char *, bitsequence, int);
void bitseq_to_uchar_revcomp(char *, bitsequence, int);
float numeric_wordscore(bitsequence, PWM *);

bitsequence matrix_min_bitsequence(float **mx, int len);
bitsequence matrix_max_bitsequence(float **mx, int len);
#endif
