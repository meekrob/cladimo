#include <string.h>
#include <stdlib.h>
#include "pwm.h"
#include "matrix.h"
#define __ (-1)

#define _A (1 << 0)
#define _C (1 << 1)
#define _G (1 << 2)
#define _T (1 << 3)
#define _N (_A | _C | _G | _T)
#define _R (_A | _G)
#define _Y (_C | _T)
#define _W (_A | _T)
#define _S (_C | _G)
#define _K (_G | _T)
#define _M (_A | _C)
extern const signed char degenerate_symbol_bit_encoding[256];
const signed char degenerate_symbol_bit_encoding[] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0, _A,  0, _C,  0,  0,  0, _G,  0,  0,  0, _K,  0, _M, _N,  0,
  0,  0, _R, _S, _T,  0,  0, _W,  0, _Y,  0,  0,  0,  0,  0,  0,
  0, _A,  0, _C,  0,  0,  0, _G,  0,  0,  0, _K,  0, _M, _N,  0,
  0,  0, _R, _S, _T,  0,  0, _W,  0, _Y,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
};
extern const signed char dna_encoding[256];
const signed char dna_encoding[] = {
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __,  0, __,  1, __, __, __,  2, __, __, __, __, __, __, __, __,
 __, __, __, __,  3, __, __, __, __, __, __, __, __, __, __, __,
 __,  0, __,  1, __, __, __,  2, __, __, __, __, __, __, __, __,
 __, __, __, __,  3, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
 __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
};
int numeric_motifmatch(bitsequence word, PWM *pwm)
{
    int i;
    int revscore = 1;
    if (pwm->index) return pwm->index[word & (index_len(pwm) - 1)];
    int len = strlen(pwm->motif);
    int score = (len > 1);

    for(i=0; i < len; i++)
    {
        int j = len - i - 1;
        int forward =  word & 3;
        int reverse = 3 - (word & 3);
        score &= mscore(forward, pwm->motif[j]);
        revscore &= mscore(reverse, pwm->motif[i]);
        if ((score==0)&&(revscore==0)) return 0; 
        word >>= 2;
    }
    if (score >= revscore) return score;
    else return - revscore;
}
float numeric_wordscore(bitsequence word, PWM *pwm)
{
    int i;
    float score=0.;
    float revscore=0.;
    // pre-computed scores
    if (pwm->index) return pwm->index[ word & (index_len(pwm) - 1)];
    // score pwm->motif if no matrix 
    if (pwm->matrix == NULL) return (int)numeric_motifmatch(word, pwm);

    for(i=0; i < pwm->length; i++)
    {
        score += pwm->matrix[word & 3][pwm->length - i - 1];
        revscore += pwm->matrix[3 - (word & 3)][i];
        word >>= 2;
    }
    // adjusted scores range from 0 to 1, min to max raw score
    float forward_adj = (score - pwm->min_score) / (pwm->max_score - pwm->min_score);
    float reverse_adj = (revscore - pwm->min_score) / (pwm->max_score - pwm->min_score);
    if (forward_adj > reverse_adj) return forward_adj;
    else return - reverse_adj;
}
inline bitsequence dnashiftone(char letter, bitsequence code, int windowsize)
{
    return (code << 2) | letterval(letter);
}

bitsequence uchar_to_bitseq(char *sequence, int length)
{
    int i = 0;
    bitsequence bitseq = 0;
    while (i < length) bitseq = dnashiftone(sequence[i++], bitseq, length);

    return bitseq;
}
void bitseq_to_uchar(char *buf, bitsequence index, int windowsize)
{
    char letters[] = "ACGT";
    int i;
    buf[windowsize] = 0;
    for (i=0; i<windowsize; i++)
    {
        buf[windowsize-(i+1)] = letters[ index & 3 ];
        index >>= 2;
    }
}
void bitseq_to_uchar_revcomp(char *buf, bitsequence index, int windowsize)
{
    char letters[] = "ACGT";
    int i;
    buf[windowsize] = 0;
    for (i=0; i<windowsize; i++)
    {
        buf[i] = letters[ 3 - (index & 3) ];
        index >>= 2;
    }
}

bitsequence matrix_min_bitsequence(float **mx, int len)
{
    _pwm_loop_
    char letters[] = "ACGT";
    bitsequence seq = 0;
    float **t = transpose_matrix(mx, 4, len);
    for_each_position( len )
    {
        int min_i = array_argmin(t[position], 4);
        char nt = letters[min_i];
        seq = dnashiftone(nt, seq, MAXWORDSIZE);
    }
    free_matrix(&t);
    return seq;
}
bitsequence matrix_max_bitsequence(float **mx, int len)
{
    _pwm_loop_
    char letters[] = "ACGT";
    bitsequence seq = 0;
    float **t = transpose_matrix(mx, 4, len);
    for_each_position( len )
    {
        int max_i = array_argmax(t[position], 4);
        char nt = letters[max_i];
        seq = dnashiftone(nt, seq, MAXWORDSIZE);
    }
    free_matrix(&t);
    return seq;
}

void print_numeric_sequence(FILE *out, bitsequence index, int windowsize)
{
    char *buf = calloc(windowsize+1, sizeof(char));
    bitseq_to_uchar(buf, index, windowsize);
    fprintf(out, "%s", buf);
    free(buf);
}
float *build_index(PWM *pwm)
{
    int index_size = index_len(pwm);
    float *index = calloc(index_size, sizeof(float));
    int i;
    for (i=0; i < index_size; i++)
    {
        index[i] = numeric_wordscore( i, pwm);
    }
    return index;
}

void pwm_free_index(PWM *pwm)
{
    if (pwm->index != NULL) free(pwm->index);
    pwm->index = NULL;
}

