#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "pwm.h"
/*from seq/seq.c*/
static const unsigned char dna_complement[] = 
  "                                                                "
  " TVGH  CD  M KN   YSA BWXR       tvgh  cd  m kn   ysa bwxr      "
  "                                                                "
  "                                                                ";
/* ................................................................ */
/* @ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~. */
/* ................................................................ */
/* ................................................................ */

#define scale_matrix_score(raw, pwm) ((raw - pwm->min_score) / (pwm->max_score - pwm->min_score))
#ifndef NAN
#define NAN (0.0/0.0)
#endif
int pwm_stringmatch(char *word, int strand, PWM *pwm);
float pwm_stringscore(char *word, int strand, PWM *pwm);
float pwm_stringscore(char *word, int strand, PWM *pwm)
{
    _pwm_loop_
    extern const signed char dna_encoding[256];  // _pwm_indexing_
    if (pwm->matrix == NULL) return (float)pwm_stringmatch(word, strand, pwm);
    float fscore = 0;
    float rscore = 0;
    for_each_position(pwm->length)
    {
        int sym = letterval(word[position]);
        if (sym == -1) return 0;
        if (strand >= 0) fscore += pwm->matrix[sym][position];
        if (strand <= 0) rscore += pwm->matrix[3-sym][(pwm->length - position - 1)];
    }
    // adjusted scores range from 0 to 1, min to max raw score
    float forward_adj = scale_matrix_score(fscore, pwm);
    float reverse_adj = scale_matrix_score(rscore, pwm);

    if (strand == 1)
        return forward_adj;
    else if (strand == -1)
        return reverse_adj;
    else // return signed maximum
    {
        if (forward_adj > reverse_adj) return forward_adj;
        else return - reverse_adj;
    }
    return NAN;
}

int pwm_stringmatch(char *word, int strand, PWM *pwm)
{
    _pwm_indexing_
    _pwm_loop_
    int fscore = 1;
    int rscore = 1;
    for_each_position(pwm->length)
    {
        int sym = letterval(word[position]);
        if (strand >= 0) fscore &= mscore(sym, pwm->motif[position]);
        if (strand <= 0) rscore &= mscore(3-sym, pwm->motif[(pwm->length-1)-position]);
        if ((!fscore) && (!rscore)) return 0;
    }
    if (strand == 1)
        return fscore;
    else if (strand == -1)
        return rscore;
    else // return signed maximum
    {
        if (fscore > rscore) return fscore;
        else return - rscore;
    }
    return 1;
}

float wordscore(unsigned char* word, PWM *pwm)
{
    int i;
    float score=0.;
    float revscore=0.;
    for(i=0; i < pwm->length; i++)
    {
        unsigned char forward = word[i];
        unsigned char reverse = dna_complement[word[i]];
        score += pwm_match(forward, i, pwm);
        revscore += pwm_match(reverse, pwm->length - (i+1), pwm);
    }
    // adjusted scores range from 0 to 1, min to max raw score
    float forward_adj = (score - pwm->min_score) / (pwm->max_score - pwm->min_score);
    float reverse_adj = (revscore - pwm->min_score) / (pwm->max_score - pwm->min_score);
    if (forward_adj > reverse_adj) return forward_adj;
    else return - reverse_adj;
}

float wordmatch(unsigned char* wordstart, char* pattern, int len, int sign)
{
    int w = 0;
    unsigned char* wp = wordstart;
    while(w<len)
    {
        
        unsigned char c = wp[0];
        if(!validchar(c)) return 0;
        if (!patmatch(c,pattern[w])) return 0;
        wp += sign;
        w++;
    }
    return 1;
}
int patmatch(unsigned char n, char symbol)
{
    n = toupper(n);
    symbol = toupper(symbol);
    switch(symbol)
    {
        case 'N': return 1;
        case 'R':
            return (patmatch(n,'A') || patmatch(n,'G'));
        case 'Y':
            return (patmatch(n,'C') || patmatch(n,'T'));
        case 'M':
            return (patmatch(n,'A') || patmatch(n,'C'));
        case 'K':
            return (patmatch(n,'G') || patmatch(n,'T'));
        case 'S':
            return (patmatch(n,'G') || patmatch(n,'C'));
        case 'W':
            return (patmatch(n,'A') || patmatch(n,'T'));
        default:
            return (n==symbol);
    }
    return 0;
}
int validchar(unsigned char c)
{
    switch(c)
    {
        case 'A':
        case 'a':
        case 'C':
        case 'c':
        case 'G':
        case 'g':
        case 'T':
        case 't':
            return 1;
    }
    return 0;
}
int validsym(unsigned char c)
{
    switch(c)
    {
        case 'N': 
        case 'R':
        case 'Y':
        case 'M':
        case 'K':
        case 'S':
        case 'W': 
        case 'n': 
        case 'r':
        case 'y':
        case 'm':
        case 'k':
        case 's':
        case 'w': return 1;
        default:
                  return validchar(c);
    }
}
int valid_motif(char* word)
{
    char *c = &(word[0]);
    while(*c != 0)
    {
        if (! validsym((unsigned char)(*c))) return 0;
        c++;
    }
    return 1;
}
int base_offset(char s)
{
    switch(s)
    {
        case 'A':
        case 'a': return 0;
        case 'C':
        case 'c': return 1;
        case 'G':
        case 'g': return 2;
        case 'T':
        case 't': return 3;
    }
    return -1;
}
float *max_score_incr(PWM *pwm)
{
    int i;
    float *col_max = calloc(pwm->length, sizeof(float));
    float *cum_max = calloc(pwm->length, sizeof(float));
    cum_max[pwm->length-1] = 0;
    for (i=pwm->length-1; i >=0; i--)
    {
        float column_max = DOUBLE_MIN;
        int j;
        for (j=0; j<4; j++) column_max = MAX(column_max, pwm->matrix[j][i]);
        col_max[i] = column_max;
        if (i < pwm->length-1) cum_max[i] = cum_max[i+1] + col_max[i+1];
    }
    free(col_max);
    return cum_max;
}
float *partials(PWM *pwm)
{
    return max_score_incr(pwm);
}
float *max_score_decr(PWM *pwm)
{
    int i;
    float *col_max = calloc(pwm->length, sizeof(float));
    float *cum_max = calloc(pwm->length, sizeof(float));
    cum_max[0] = 0;
    for (i=0; i < pwm->length; i++)
    {
        float column_max = DOUBLE_MIN;
        int j;
        for (j=0; j<4; j++) column_max = MAX(column_max, pwm->matrix[j][i]);
        col_max[i] = column_max;

        if (i > 0)
            cum_max[i] = cum_max[i-1] + col_max[i-1];
    }
    free(col_max);
    return cum_max;
}
float max_score_left(PWM *pwm, int position)
{
    float * msd = max_score_decr(pwm);
    float left = msd[position];
    free(msd);
    return left;
}
float max_score_right(PWM *pwm, int position)
{
    float * msi = max_score_incr(pwm);
    float right = msi[position];
    free(msi);
    return right;
}
char* pwm_max_char(PWM *pwm_1)
{
    int motif_len = pwm_1->length;
    char* word = calloc(motif_len+1,sizeof(char));
    int i=0;
    for (; i < motif_len; i++) 
    {
        int b=0;
        char nt[] = {'A','C','G','T'};
        float position_max = DOUBLE_MIN;
        char max_char = 0;
        for(;b<4;b++)
        {
            char c=nt[b];
            float pscore = pwm_match(c,i,pwm_1);
            if (pscore > position_max)
            {
                position_max = pscore;
                max_char = c;
            }
        }
        word[i] = max_char;
    }
    word[motif_len] = 0;
    return word;
}

unsigned char *revcomp(unsigned char *seq)
{
    int i;
    int len = strlen((const char*)seq);
    unsigned char *rc = calloc(len+1, sizeof(unsigned char));
    for(i=0;i<len;i++)
        rc[i] = dna_complement[seq[len -(i+1)]];

    return rc;
}
