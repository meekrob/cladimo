#ifndef COMBO_LIB_H
#include "pwm.h"
struct combo_lib
{
    int file_n;
    int volume_n;
    int total_n;
    int statlibn;
    int userlibn;
    PWM **static_library; //= build_static_library(&statlibn); // PWMs are defined by literals in libraries/library.c, should not be freed
    PWM **user_library; //= calloc(max_wmx,  sizeof(PWM*)); // PWMs created in this session, must be freed
    PWM **pwm_library; // = calloc(max_wmx, sizeof(PWM*)); // master list
    int max_pwm_length;
};
void free_combo_lib(struct combo_lib** clp);
struct combo_lib * parse_arg_list(char **argv, int argc);
#define COMBO_LIB_H 1
#endif
