#include "pwm.h"
#include "jaspar_core.h"
/* created by ./build_library_code using JASPAR_CORE_2008.wmx on Tue Mar  3 16:49:11 2009
*/
	float jaspar_core_pwm_0_matrix_row_0_[10] = {  0.932793, -0.668692, -0.668692, -5.283813, -5.283813,  1.268695,  1.268695, -5.283813,  0.423298, -5.283813};
	float jaspar_core_pwm_0_matrix_row_1_[10] = { -0.427530,  1.173956,  1.355944, -5.042651, -5.042651, -5.042651, -5.042651, -5.042651, -0.427530,  0.260654};
	float jaspar_core_pwm_0_matrix_row_2_[10] = { -0.427530, -0.427530, -5.042651,  1.509857,  1.509857, -5.042651, -5.042651,  1.509857, -0.427530,  0.951311};
	float jaspar_core_pwm_0_matrix_row_3_[10] = { -5.283813, -5.283813, -5.283813, -5.283813, -5.283813, -5.283813, -5.283813, -5.283813,  0.019492, -0.668692};
	float *jaspar_core_pwm_0_matrix[4] = { jaspar_core_pwm_0_matrix_row_0_, jaspar_core_pwm_0_matrix_row_1_, jaspar_core_pwm_0_matrix_row_2_, jaspar_core_pwm_0_matrix_row_3_};
	PWM jaspar_core_pwm_0_ = {
/* accession        */ "MA0062",
/* name             */ "GABPA",
/* label            */ " MA0062	13.8895030261926	GABPA	ETS	; acc \"Q06546\" ; medline \"8383622\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"13.8920\" ; type \"COMPILED\" ",
/* motif            */ "ACCGGAAGNG",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_0_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -47.981846,
/* max_score        */ 11.904264,
/* threshold        */ 0.802,
/* info content     */ 13.573000,
/* base_counts      */ {24, 15, 28, 3},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 7
};

	float jaspar_core_pwm_1_matrix_row_0_[20] = { -0.026619, -1.118609, -0.026619, -5.733730,  0.818778,  1.069776,  0.952131,  0.260232, -5.733730,  1.270245,  0.260232, -1.118609, -0.026619,  0.260232, -0.430425,  0.260232,  0.260232,  0.260232, -1.118609,  0.260232};
	float jaspar_core_pwm_1_matrix_row_1_[20] = {  1.193293,  1.416187,  1.193293,  1.511407,  0.501394, -0.189263,  0.214543,  0.906027,  1.511407, -5.492568,  1.059940,  1.416187,  1.193293,  0.906027,  1.310938,  0.724039,  0.724039,  0.906027,  1.059940,  0.501394};
	float jaspar_core_pwm_1_matrix_row_2_[20] = { -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -5.492568, -0.877447, -5.492568, -5.492568, -0.877447, -5.492568,  0.214543, -0.189263};
	float jaspar_core_pwm_1_matrix_row_3_[20] = { -5.733730, -5.733730, -5.733730, -5.733730, -5.733730, -5.733730, -5.733730, -1.118609, -5.733730, -5.733730, -5.733730, -5.733730, -5.733730, -5.733730, -5.733730, -0.430425, -1.118609, -1.118609, -5.733730, -1.118609};
	float *jaspar_core_pwm_1_matrix[4] = { jaspar_core_pwm_1_matrix_row_0_, jaspar_core_pwm_1_matrix_row_1_, jaspar_core_pwm_1_matrix_row_2_, jaspar_core_pwm_1_matrix_row_3_};
	PWM jaspar_core_pwm_1_ = {
/* accession        */ "MA0073",
/* name             */ "RREB1",
/* label            */ " MA0073	22.2782723704014	RREB1	ZN-FINGER, C2H2	; acc \"Q92766\" ; medline \"8816445\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"22.2800\" ; type \"SELEX\" ",
/* motif            */ "CCCCAAACCACCCCCMMCCN",
/* library_name     */ "jaspar_core",
/* length           */ 20,
/* matrixname       */ jaspar_core_pwm_1_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.720856,
/* max_score        */ 21.644373,
/* threshold        */ 0.823,
/* info content     */ 21.920675,
/* base_counts      */ {77, 130, 7, 6},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 11
};

	float jaspar_core_pwm_2_matrix_row_0_[6] = {  1.271539,  1.271539,  1.271539, -6.378630, -0.671520,  0.173878};
	float jaspar_core_pwm_2_matrix_row_1_[6] = { -6.137468, -6.137468, -6.137468, -6.137468,  1.107474,  0.261127};
	float jaspar_core_pwm_2_matrix_row_2_[6] = { -6.137468, -6.137468, -6.137468,  1.512701, -0.834163, -0.430358};
	float jaspar_core_pwm_2_matrix_row_3_[6] = { -6.378630, -6.378630, -6.378630, -6.378630, -1.075325, -0.162024};
	float *jaspar_core_pwm_2_matrix[4] = { jaspar_core_pwm_2_matrix_row_0_, jaspar_core_pwm_2_matrix_row_1_, jaspar_core_pwm_2_matrix_row_2_, jaspar_core_pwm_2_matrix_row_3_};
	PWM jaspar_core_pwm_2_ = {
/* accession        */ "MA0020",
/* name             */ "Dof2",
/* label            */ " MA0020	8.6241100995044	Dof2	ZN-FINGER, DOF	; acc \"Q41800\" ; medline \"10074718\" ; species \"Zea mays\" ; sysgroup \"plant\" ; total_ic \"8.6250\" ; type \"SELEX\" ",
/* motif            */ "AAAGCN",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_2_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -27.020203,
/* max_score        */ 6.695918,
/* threshold        */ 0.904,
/* info content     */ 8.550580,
/* base_counts      */ {73, 20, 26, 7},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_3_matrix_row_0_[6] = { -1.028122,  0.416888, -7.022083, -7.022083, -7.022083, -0.805477};
	float jaspar_core_pwm_3_matrix_row_1_[6] = {  0.597462, -6.780921, -2.165801,  1.488067,  1.488067, -1.073811};
	float jaspar_core_pwm_3_matrix_row_2_[6] = { -0.786960, -6.780921, -6.780921, -2.165801, -6.780921,  0.658050};
	float jaspar_core_pwm_3_matrix_row_3_[6] = {  0.356300,  0.719016,  1.246905, -7.022083, -2.406963,  0.291803};
	float *jaspar_core_pwm_3_matrix[4] = { jaspar_core_pwm_3_matrix_row_0_, jaspar_core_pwm_3_matrix_row_1_, jaspar_core_pwm_3_matrix_row_2_, jaspar_core_pwm_3_matrix_row_3_};
	PWM jaspar_core_pwm_3_ = {
/* accession        */ "MA0098",
/* name             */ "ETS1",
/* label            */ " MA0098	7.07782163341168	ETS1	ETS	; acc \"CAG47050\" ; medline \"1542566\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"7.0800\" ; type \"SELEX\" ",
/* motif            */ "YTTCCK",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_3_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -29.949104,
/* max_score        */ 6.197566,
/* threshold        */ 0.922,
/* info content     */ 7.046766,
/* base_counts      */ {26, 98, 22, 94},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 40
};

	float jaspar_core_pwm_4_matrix_row_0_[12] = { -0.151041, -0.151041, -1.614819, -2.706809,  1.272410, -2.706809, -2.706809, -2.706809, -7.321929, -1.614819, -2.018624, -1.105323};
	float jaspar_core_pwm_4_matrix_row_1_[12] = {  0.090122,  1.188221, -0.864161,  1.494884, -7.080767, -2.465647,  1.436626, -2.465647, -7.080767,  1.135591, -7.080767,  0.358205};
	float jaspar_core_pwm_4_matrix_row_2_[12] = {  0.358205, -1.777462,  1.135591, -7.080767, -7.080767,  1.475839, -1.373657, -7.080767,  1.494884, -0.394906,  1.135591,  0.010143};
	float jaspar_core_pwm_4_matrix_row_3_[12] = { -0.317955, -7.321929, -0.518424, -7.321929, -7.321929, -7.321929, -7.321929,  1.234677, -2.706809, -0.923334, -0.008042,  0.279473};
	float *jaspar_core_pwm_4_matrix[4] = { jaspar_core_pwm_4_matrix_row_0_, jaspar_core_pwm_4_matrix_row_1_, jaspar_core_pwm_4_matrix_row_2_, jaspar_core_pwm_4_matrix_row_3_};
	PWM jaspar_core_pwm_4_ = {
/* accession        */ "MA0048",
/* name             */ "NHLH1",
/* label            */ " MA0048	14.1315572207903	NHLH1	bHLH	; acc \"Q02575\" ; medline \"8289804\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.1300\" ; type \"SELEX\" ",
/* motif            */ "NCGCAGCTGCGN",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_4_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -62.746029,
/* max_score        */ 13.720725,
/* threshold        */ 0.781,
/* info content     */ 14.090129,
/* base_counts      */ {97, 216, 221, 114},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 54
};

	float jaspar_core_pwm_5_matrix_row_0_[12] = { -0.179229,  0.823498,  0.600438, -2.568082, -0.092293, -7.183203, -2.568082, -0.966597,  0.061739, -0.784608, -2.568082, -7.183203};
	float jaspar_core_pwm_5_matrix_row_1_[12] = { -1.234931, -1.638736, -2.326920, -0.948079, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -0.389533, -1.638736,  0.148869};
	float jaspar_core_pwm_5_matrix_row_2_[12] = {  0.921611, -6.942041, -6.942041, -6.942041,  1.189784, -6.942041, -6.942041, -0.033286,  0.708128, -6.942041, -0.948079, -6.942041};
	float jaspar_core_pwm_5_matrix_row_3_[12] = { -0.630695,  0.130684,  0.513464,  1.159875, -2.568082,  1.272328,  1.250826,  0.888016, -0.092293,  0.948622,  1.111097,  0.977601};
	float *jaspar_core_pwm_5_matrix[4] = { jaspar_core_pwm_5_matrix_row_0_, jaspar_core_pwm_5_matrix_row_1_, jaspar_core_pwm_5_matrix_row_2_, jaspar_core_pwm_5_matrix_row_3_};
	PWM jaspar_core_pwm_5_ = {
/* accession        */ "MA0041",
/* name             */ "Foxd3",
/* label            */ " MA0041	12.9448284802064	Foxd3	FORKHEAD	; acc \"Q63245\" ; medline \"8139574\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"12.9450\" ; type \"SELEX\" ",
/* motif            */ "GAATGTTTNTTT",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_5_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -73.705742,
/* max_score        */ 11.851822,
/* threshold        */ 0.782,
/* info content     */ 12.896553,
/* base_counts      */ {105, 31, 95, 333},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_core_pwm_6_matrix_row_0_[5] = {  1.146186,  1.272457, -7.410420, -7.410420,  1.255366};
	float jaspar_core_pwm_6_matrix_row_1_[5] = { -1.865953, -7.169258, -7.169258, -7.169258, -7.169258};
	float jaspar_core_pwm_6_matrix_row_2_[5] = { -1.175296, -7.169258, -2.554137, -7.169258, -2.554137};
	float jaspar_core_pwm_6_matrix_row_3_[5] = { -2.795299, -7.410420,  1.255366,  1.272457, -7.410420};
	float *jaspar_core_pwm_6_matrix[4] = { jaspar_core_pwm_6_matrix_row_0_, jaspar_core_pwm_6_matrix_row_1_, jaspar_core_pwm_6_matrix_row_2_, jaspar_core_pwm_6_matrix_row_3_};
	PWM jaspar_core_pwm_6_ = {
/* accession        */ "MA0075",
/* name             */ "Prrx2",
/* label            */ " MA0075	9.06306510239135	Prrx2	HOMEO	; acc \"Q06348\" ; medline \"7901837\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"9.0620\" ; type \"SELEX\" ",
/* motif            */ "AATTA",
/* library_name     */ "jaspar_core",
/* length           */ 5,
/* matrixname       */ jaspar_core_pwm_6_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -32.436977,
/* max_score        */ 6.201833,
/* threshold        */ 0.917,
/* info content     */ 9.035529,
/* base_counts      */ {169, 2, 6, 118},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 59
};

	float jaspar_core_pwm_7_matrix_row_0_[16] = {  0.045685, -0.707756, -1.533632,  0.579646,  0.451052, -8.086140, -2.782835,  1.237619,  1.272707, -8.086140, -0.841198,  0.708837, -0.915251,  0.182849,  0.102827, -0.261694};
	float jaspar_core_pwm_7_matrix_row_1_[16] = {  0.371381,  0.257003,  0.692214, -0.600036, -1.851016,  1.513869,  1.487669, -7.844977, -7.844977, -3.229857,  0.934734, -1.446382, -0.243575,  0.521625, -1.041472,  0.315826};
	float jaspar_core_pwm_7_matrix_row_2_[16] = {  0.056400,  0.018674, -0.020531,  0.474009,  0.785723, -7.844977, -3.229857, -3.229857, -7.844977, -7.844977,  0.257003,  0.498100,  1.050789, -0.148310,  0.610553,  0.127833};
	float jaspar_core_pwm_7_matrix_row_3_[16] = { -0.590042,  0.232847,  0.015841, -2.379029, -1.869533, -8.086140, -8.086140, -2.379029, -8.086140,  1.264050, -2.092178, -2.782835, -1.177385, -0.995230, -0.302499, -0.184762};
	float *jaspar_core_pwm_7_matrix[4] = { jaspar_core_pwm_7_matrix_row_0_, jaspar_core_pwm_7_matrix_row_1_, jaspar_core_pwm_7_matrix_row_2_, jaspar_core_pwm_7_matrix_row_3_};
	PWM jaspar_core_pwm_7_ = {
/* accession        */ "MA0060",
/* name             */ "NFYA",
/* label            */ " MA0060	12.9251245779655	NFYA	CAAT-BOX	; acc \"P23511\" ; medline \"9469818\" ; species \"-\" ; sysgroup \"vertebrate\" ; total_ic \"12.9280\" ; type \"COMPILED\" ",
/* motif            */ "NNNRRCCAATCAGNNN",
/* library_name     */ "jaspar_core",
/* length           */ 16,
/* matrixname       */ jaspar_core_pwm_7_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -55.620316,
/* max_score        */ 13.604246,
/* threshold        */ 0.737,
/* info content     */ 12.906879,
/* base_counts      */ {589, 547, 423, 297},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 116
};

	float jaspar_core_pwm_8_matrix_row_0_[7] = {  0.425192, -6.888695, -6.888695,  1.272109, -6.888695, -6.888695, -6.888695};
	float jaspar_core_pwm_8_matrix_row_1_[7] = {  0.666354, -6.647533, -6.647533, -6.647533,  1.513271, -6.647533, -6.647533};
	float jaspar_core_pwm_8_matrix_row_2_[7] = { -0.430927, -6.647533,  1.513271, -6.647533, -6.647533,  1.513271, -6.647533};
	float jaspar_core_pwm_8_matrix_row_3_[7] = { -6.888695,  1.272109, -6.888695, -6.888695, -6.888695, -6.888695,  1.272109};
	float *jaspar_core_pwm_8_matrix[4] = { jaspar_core_pwm_8_matrix_row_0_, jaspar_core_pwm_8_matrix_row_1_, jaspar_core_pwm_8_matrix_row_2_, jaspar_core_pwm_8_matrix_row_3_};
	PWM jaspar_core_pwm_8_ = {
/* accession        */ "MA0096",
/* name             */ "bZIP910",
/* label            */ " MA0096	12.5511843642748	bZIP910	bZIP	; acc \"CAA74022\" ; medline \"9680995\" ; species \"Antirrhinum majus\" ; sysgroup \"plant\" ; total_ic \"12.5510\" ; type \"SELEX\" ",
/* motif            */ "MTGACGT",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_8_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.220863,
/* max_score        */ 9.022495,
/* threshold        */ 0.843,
/* info content     */ 12.479671,
/* base_counts      */ {50, 50, 75, 70},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 35
};

	float jaspar_core_pwm_9_matrix_row_0_[7] = {  0.814639, -1.230879,  0.558800, -1.921536, -2.609720,  1.251739,  1.230690};
	float jaspar_core_pwm_9_matrix_row_1_[7] = { -6.983679,  0.261263,  0.394705, -6.983679, -6.983679, -6.983679, -6.983679};
	float jaspar_core_pwm_9_matrix_row_2_[7] = { -6.983679,  0.989132, -0.431171,  1.471852,  1.471852, -6.983679, -6.983679};
	float jaspar_core_pwm_9_matrix_row_3_[7] = {  0.271257, -1.921536, -1.921536, -7.224841, -2.609720, -2.609720, -1.921536};
	float *jaspar_core_pwm_9_matrix[4] = { jaspar_core_pwm_9_matrix_row_0_, jaspar_core_pwm_9_matrix_row_1_, jaspar_core_pwm_9_matrix_row_2_, jaspar_core_pwm_9_matrix_row_3_};
	PWM jaspar_core_pwm_9_ = {
/* accession        */ "MA0081",
/* name             */ "SPIB",
/* label            */ " MA0081	9.06007121381261	SPIB	ETS	; acc \"Q01892\" ; medline \"7624145\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"9.0600\" ; type \"SELEX\" ",
/* motif            */ "AGMGGAA",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_9_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -39.002625,
/* max_score        */ 7.788702,
/* threshold        */ 0.879,
/* info content     */ 9.028917,
/* base_counts      */ {157, 30, 130, 26},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_core_pwm_10_matrix_row_0_[9] = { -0.517962, -1.896803,  0.658965,  1.271717, -6.511924, -6.511924, -0.113329, -0.517962,  0.291582};
	float jaspar_core_pwm_10_matrix_row_1_[9] = {  0.281746, -0.276800, -1.655641, -6.270761, -6.270761, -6.270761, -6.270761,  0.127833,  0.281746};
	float jaspar_core_pwm_10_matrix_row_2_[9] = { -0.276800, -0.054155,  0.281746, -6.270761,  1.512879, -6.270761,  1.225336,  0.820148, -0.054155};
	float jaspar_core_pwm_10_matrix_row_3_[9] = {  0.291582,  0.733018, -0.804813, -6.511924, -6.511924,  1.271717, -6.511924, -1.208619, -0.804813};
	float *jaspar_core_pwm_10_matrix[4] = { jaspar_core_pwm_10_matrix_row_0_, jaspar_core_pwm_10_matrix_row_1_, jaspar_core_pwm_10_matrix_row_2_, jaspar_core_pwm_10_matrix_row_3_};
	PWM jaspar_core_pwm_10_ = {
/* accession        */ "MA0122",
/* name             */ "Bapx1",
/* label            */ " MA0122	8.54224977193481	Bapx1	HOMEO	; acc \"P97503\" ; medline \"12746429\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"8.54224977193481\" ; type \"SELEX\" ",
/* motif            */ "NTAAGTGSN",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_10_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -32.131535,
/* max_score        */ 8.076942,
/* threshold        */ 0.845,
/* info content     */ 8.479681,
/* base_counts      */ {61, 25, 75, 55},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 24
};

	float jaspar_core_pwm_11_matrix_row_0_[9] = {  0.999754, -6.378630, -6.378630,  1.271539,  1.271539, -6.378630, -6.378630,  0.999754,  0.530125};
	float jaspar_core_pwm_11_matrix_row_1_[9] = { -6.137468,  0.261127, -6.137468, -6.137468, -6.137468, -6.137468, -1.522347, -6.137468, -1.522347};
	float jaspar_core_pwm_11_matrix_row_2_[9] = { -6.137468, -0.430358, -6.137468, -6.137468, -6.137468, -6.137468, -6.137468, -1.522347, -0.430358};
	float jaspar_core_pwm_11_matrix_row_3_[9] = { -0.162024,  0.712280,  1.271539, -6.378630, -6.378630,  1.271539,  1.222772, -0.384668,  0.173878};
	float *jaspar_core_pwm_11_matrix[4] = { jaspar_core_pwm_11_matrix_row_0_, jaspar_core_pwm_11_matrix_row_1_, jaspar_core_pwm_11_matrix_row_2_, jaspar_core_pwm_11_matrix_row_3_};
	PWM jaspar_core_pwm_11_ = {
/* accession        */ "MA0134",
/* name             */ "Lhx3",
/* label            */ " MA0134	12.9411780190952	Lhx3	HOMEO	; acc \"AAF36809\" ; comment \"two isoforms with different DNA models\" ; medline \"15865204\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"12.941\" ; type \"SELEX\" ",
/* motif            */ "ATTAATTAW",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_11_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -52.069061,
/* max_score        */ 9.550839,
/* threshold        */ 0.817,
/* info content     */ 12.830067,
/* base_counts      */ {84, 8, 7, 90},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_12_matrix_row_0_[14] = { -1.791030, -1.791030, -1.100373,  1.174654, -1.387224, -2.479214, -2.479214,  0.555834, -2.479214, -1.791030,  1.094632, -0.090360, -2.479214, -2.479214};
	float jaspar_core_pwm_12_matrix_row_1_[14] = { -0.859211, -1.549868,  1.010479, -1.549868,  1.278652, -6.853172,  1.363186, -1.549868, -0.859211,  0.391769, -6.853172,  0.150802, -0.636566, -6.853172};
	float jaspar_core_pwm_12_matrix_row_2_[14] = { -0.859211, -6.853172, -2.238052, -2.238052, -2.238052,  1.465814, -0.859211, -1.549868, -2.238052,  0.971274, -0.454577,  0.317716, -1.146062,  0.585799};
	float jaspar_core_pwm_12_matrix_row_3_[14] = {  1.007646,  1.174654, -0.003425, -2.479214, -0.877728, -2.479214, -2.479214,  0.401763,  1.122024, -1.791030, -2.479214, -0.408473,  1.037490,  0.730111};
	float *jaspar_core_pwm_12_matrix[4] = { jaspar_core_pwm_12_matrix_row_0_, jaspar_core_pwm_12_matrix_row_1_, jaspar_core_pwm_12_matrix_row_2_, jaspar_core_pwm_12_matrix_row_3_};
	PWM jaspar_core_pwm_12_ = {
/* accession        */ "MA0069",
/* name             */ "Pax6",
/* label            */ " MA0069	13.7977195202109	Pax6	PAIRED	; acc \"P26367\" ; medline \"8132558\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"13.7960\" ; type \"SELEX\" ",
/* motif            */ "TTCACGCWTGANTT",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_12_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -47.346046,
/* max_score        */ 14.304164,
/* threshold        */ 0.762,
/* info content     */ 13.761353,
/* base_counts      */ {125, 141, 119, 217},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 43
};

	float jaspar_core_pwm_13_matrix_row_0_[11] = { -6.224796, -6.224796, -6.224796, -0.921491,  0.779178, -0.008190, -6.224796, -6.224796, -6.224796, -6.224796, -1.609676};
	float jaspar_core_pwm_13_matrix_row_1_[11] = { -5.983634, -5.983634, -5.983634, -5.983634, -1.368514, -5.983634,  0.232972,  1.187254,  1.455338,  1.512463,  1.330253};
	float jaspar_core_pwm_13_matrix_row_2_[11] = {  1.512463,  1.512463,  1.512463,  1.394750,  0.414961, -0.680329, -0.680329, -5.983634, -5.983634, -5.983634, -1.368514};
	float jaspar_core_pwm_13_matrix_row_3_[11] = { -6.224796, -6.224796, -6.224796, -6.224796, -6.224796,  0.779178,  0.779178, -0.008190, -1.609676, -6.224796, -1.609676};
	float *jaspar_core_pwm_13_matrix[4] = { jaspar_core_pwm_13_matrix_row_0_, jaspar_core_pwm_13_matrix_row_1_, jaspar_core_pwm_13_matrix_row_2_, jaspar_core_pwm_13_matrix_row_3_};
	PWM jaspar_core_pwm_13_ = {
/* accession        */ "MA0105",
/* name             */ "NFKB1",
/* label            */ " MA0105	15.6269629573419	NFKB1	REL	; acc \"P19838\" ; medline \"1406630\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"15.6280\" ; type \"SELEX\" ",
/* motif            */ "GGGGATTCCCC",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_13_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -63.616478,
/* max_score        */ 13.754982,
/* threshold        */ 0.787,
/* info content     */ 15.475903,
/* base_counts      */ {19, 69, 81, 29},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18
};

	float jaspar_core_pwm_14_matrix_row_0_[12] = { -1.858410, -0.358210, -7.161715, -2.546595,  0.909503, -1.454605,  1.272314, -2.546595,  1.204888,  0.152172, -1.858410, -1.858410};
	float jaspar_core_pwm_14_matrix_row_1_[12] = { -2.305433,  1.181428,  1.491502,  1.491502, -2.305433, -2.305433, -6.920553, -6.920553, -6.920553, -2.305433, -6.920553, -2.305433};
	float jaspar_core_pwm_14_matrix_row_2_[12] = {  1.348435, -1.617248, -2.305433, -6.920553, -6.920553, -6.920553, -6.920553, -6.920553, -6.920553, -6.920553,  1.469034,  1.446050};
	float jaspar_core_pwm_14_matrix_row_3_[12] = { -1.167754, -1.858410, -7.161715, -7.161715,  0.009173,  1.181363, -7.161715,  1.250340, -1.454605,  0.844986, -7.161715, -7.161715};
	float *jaspar_core_pwm_14_matrix[4] = { jaspar_core_pwm_14_matrix_row_0_, jaspar_core_pwm_14_matrix_row_1_, jaspar_core_pwm_14_matrix_row_2_, jaspar_core_pwm_14_matrix_row_3_};
	PWM jaspar_core_pwm_14_ = {
/* accession        */ "MA0083",
/* name             */ "SRF",
/* label            */ " MA0083	17.9647716467558	SRF	MADS	; acc \"P11831\" ; medline \"2243767\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"17.9670\" ; type \"SELEX\" ",
/* motif            */ "GCCCATATATGG",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_14_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -74.575180,
/* max_score        */ 15.091343,
/* threshold        */ 0.778,
/* info content     */ 17.901018,
/* base_counts      */ {156, 128, 129, 139},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_core_pwm_15_matrix_row_0_[6] = { -0.131016, -1.381996, -1.668847,  1.254743,  1.254743, -1.668847};
	float jaspar_core_pwm_15_matrix_row_1_[6] = {  0.515374, -1.831490, -7.134795, -2.519675, -7.134795,  0.361302};
	float jaspar_core_pwm_15_matrix_row_2_[6] = {  0.415340,  1.341784,  1.421811, -7.134795, -7.134795,  0.997029};
	float jaspar_core_pwm_15_matrix_row_3_[6] = { -1.668847, -1.668847, -2.072652, -7.375957, -2.760837, -2.072652};
	float *jaspar_core_pwm_15_matrix[4] = { jaspar_core_pwm_15_matrix_row_0_, jaspar_core_pwm_15_matrix_row_1_, jaspar_core_pwm_15_matrix_row_2_, jaspar_core_pwm_15_matrix_row_3_};
	PWM jaspar_core_pwm_15_ = {
/* accession        */ "MA0080",
/* name             */ "SPI1",
/* label            */ " MA0080	7.21720396462768	SPI1	ETS	; acc \"P17947\" ; medline \"7624145\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"7.2180\" ; type \"SELEX\" ",
/* motif            */ "NGGAAG",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_15_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -27.218538,
/* max_score        */ 6.785484,
/* threshold        */ 0.920,
/* info content     */ 7.198740,
/* base_counts      */ {136, 42, 153, 11},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 57
};

	float jaspar_core_pwm_16_matrix_row_0_[10] = { -5.533864, -5.533864, -5.533864, -5.533864,  0.460098, -0.230559, -5.533864, -5.533864, -5.533864, -0.918743};
	float jaspar_core_pwm_16_matrix_row_1_[10] = { -0.677581, -5.292702, -5.292702, -5.292702, -0.677581, -0.677581, -5.292702, -0.677581,  1.510804,  1.393159};
	float jaspar_core_pwm_16_matrix_row_2_[10] = {  1.259806,  1.259806,  1.510804,  1.393159, -5.292702, -5.292702, -5.292702, -5.292702, -5.292702, -5.292702};
	float jaspar_core_pwm_16_matrix_row_3_[10] = { -0.918743, -0.230559, -5.533864, -0.918743,  0.460098,  0.864731,  1.269642,  1.151997, -5.533864, -5.533864};
	float *jaspar_core_pwm_16_matrix[4] = { jaspar_core_pwm_16_matrix_row_0_, jaspar_core_pwm_16_matrix_row_1_, jaspar_core_pwm_16_matrix_row_2_, jaspar_core_pwm_16_matrix_row_3_};
	PWM jaspar_core_pwm_16_ = {
/* accession        */ "MA0023",
/* name             */ "Dl_2",
/* label            */ " MA0023	14.1230520913631	Dl_2	REL	; acc \"AAF53611\" ; comment \"dl has a dual binding specificity and therefore two models: MA0022 and MA0023\" ; medline \"1582412\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"14.1210\" ; type \"SELEX\" ",
/* motif            */ "GGGGWTTTCC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_16_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -54.856308,
/* max_score        */ 12.074007,
/* threshold        */ 0.800,
/* info content     */ 13.866007,
/* base_counts      */ {7, 21, 31, 31},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 9
};

	float jaspar_core_pwm_17_matrix_row_0_[10] = { -6.224796, -6.224796, -6.224796,  0.779178,  0.683959, -0.921491, -6.224796, -6.224796, -6.224796, -6.224796};
	float jaspar_core_pwm_17_matrix_row_1_[10] = {  0.010327, -5.983634, -5.983634, -5.983634, -0.276524, -5.983634, -5.983634, -0.680329,  1.512463,  1.512463};
	float jaspar_core_pwm_17_matrix_row_2_[10] = {  1.020340,  1.455338,  1.512463,  0.568874,  0.010327, -5.983634, -5.983634, -5.983634, -5.983634, -5.983634};
	float jaspar_core_pwm_17_matrix_row_3_[10] = { -0.517686, -1.609676, -6.224796, -6.224796, -1.609676,  1.153588,  1.271301,  1.153588, -6.224796, -6.224796};
	float *jaspar_core_pwm_17_matrix[4] = { jaspar_core_pwm_17_matrix_row_0_, jaspar_core_pwm_17_matrix_row_1_, jaspar_core_pwm_17_matrix_row_2_, jaspar_core_pwm_17_matrix_row_3_};
	PWM jaspar_core_pwm_17_ = {
/* accession        */ "MA0107",
/* name             */ "RELA",
/* label            */ " MA0107	14.7568335646172	RELA	REL	; acc \"Q04206\" ; medline \"1406630\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.7550\" ; type \"SELEX\" ",
/* motif            */ "GGGAATTTCC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_17_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -57.391682,
/* max_score        */ 12.054682,
/* threshold        */ 0.797,
/* info content     */ 14.610112,
/* base_counts      */ {23, 45, 57, 55},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18
};

	float jaspar_core_pwm_18_matrix_row_0_[7] = {  0.216425,  1.180732,  1.227231, -6.469437,  1.271663,  1.227231, -6.469437};
	float jaspar_core_pwm_18_matrix_row_1_[7] = { -0.234313, -6.228274, -6.228274,  1.468393, -6.228274, -1.613154, -6.228274};
	float jaspar_core_pwm_18_matrix_row_2_[7] = { -0.521164, -1.613154, -1.613154, -6.228274, -6.228274, -6.228274, -6.228274};
	float jaspar_core_pwm_18_matrix_row_3_[7] = {  0.216425, -1.854316, -6.469437, -1.854316, -6.469437, -6.469437,  1.271663};
	float *jaspar_core_pwm_18_matrix[4] = { jaspar_core_pwm_18_matrix_row_0_, jaspar_core_pwm_18_matrix_row_1_, jaspar_core_pwm_18_matrix_row_2_, jaspar_core_pwm_18_matrix_row_3_};
	PWM jaspar_core_pwm_18_ = {
/* accession        */ "MA0087",
/* name             */ "Sox5",
/* label            */ " MA0087	10.8307137071314	Sox5	HMG	; acc \"P35710\" ; medline \"1396566\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"10.8320\" ; type \"SELEX\" ",
/* motif            */ "NAACAAT",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_18_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -39.096622,
/* max_score        */ 7.863337,
/* threshold        */ 0.861,
/* info content     */ 10.751419,
/* base_counts      */ {96, 27, 5, 33},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 23
};

	float jaspar_core_pwm_19_matrix_row_0_[9] = { -1.237093, -2.329082,  1.062498,  1.272155, -6.944203, -1.640898,  1.272155, -6.944203, -6.944203};
	float jaspar_core_pwm_19_matrix_row_1_[9] = {  0.847095,  1.269770, -0.304446, -6.703041, -6.703041, -0.486435, -6.703041, -6.703041, -6.703041};
	float jaspar_core_pwm_19_matrix_row_2_[9] = {  0.100464, -0.709079, -6.703041, -6.703041, -6.703041,  0.205714, -6.703041, -6.703041, -6.703041};
	float jaspar_core_pwm_19_matrix_row_3_[9] = { -0.545608, -1.237093, -2.329082, -6.944203,  1.272155,  0.657199, -6.944203,  1.272155,  1.272155};
	float *jaspar_core_pwm_19_matrix[4] = { jaspar_core_pwm_19_matrix_row_0_, jaspar_core_pwm_19_matrix_row_1_, jaspar_core_pwm_19_matrix_row_2_, jaspar_core_pwm_19_matrix_row_3_};
	PWM jaspar_core_pwm_19_ = {
/* accession        */ "MA0110",
/* name             */ "ATHB5",
/* label            */ " MA0110	12.8142114788194	ATHB5	HOMEO-ZIP	; acc \"11762265\" ; comment \"updated matrix since last release\" ; medline \"11247607\" ; species \"Arabidopsis thaliana\" ; sysgroup \"plant\" ; total_ic \"12.814\" ; type \"SELEX\" ",
/* motif            */ "CCAATTATT",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_19_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.631126,
/* max_score        */ 10.197338,
/* threshold        */ 0.817,
/* info content     */ 12.751672,
/* base_counts      */ {110, 59, 23, 141},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 37
};

	float jaspar_core_pwm_20_matrix_row_0_[9] = { -0.031136,  1.182938,  1.167193, -1.587310, -0.672516, -0.672516, -0.410383, -1.874161,  0.356461};
	float jaspar_core_pwm_20_matrix_row_1_[9] = { -1.632999, -2.724988, -7.340109,  1.392357,  0.561268, -2.036804, -0.654248,  0.098863, -2.724988};
	float jaspar_core_pwm_20_matrix_row_2_[9] = { -2.036804, -2.036804, -2.036804, -1.632999,  0.038275,  1.235542, -7.340109, -2.724988, -7.340109};
	float jaspar_core_pwm_20_matrix_row_3_[9] = {  0.852758, -1.874161, -1.364665, -2.966151, -0.142299, -1.364665,  0.915923,  0.915923,  0.737715};
	float *jaspar_core_pwm_20_matrix[4] = { jaspar_core_pwm_20_matrix_row_0_, jaspar_core_pwm_20_matrix_row_1_, jaspar_core_pwm_20_matrix_row_2_, jaspar_core_pwm_20_matrix_row_3_};
	PWM jaspar_core_pwm_20_ = {
/* accession        */ "MA0054",
/* name             */ "MYB.ph3",
/* label            */ " MA0054	8.56143865224759	MYB.ph3	TRP-CLUSTER	; acc \"Q02994\" ; medline \"7737128\" ; species \"Petunia hybrida\" ; sysgroup \"plant\" ; total_ic \"8.5610\" ; type \"SELEX\" ",
/* motif            */ "TAACNGTTT",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_20_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -35.182579,
/* max_score        */ 8.961617,
/* threshold        */ 0.845,
/* info content     */ 8.546443,
/* base_counts      */ {214, 121, 79, 216},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 70
};

	float jaspar_core_pwm_21_matrix_row_0_[10] = { -0.801513, -0.113329, -5.416634, -5.416634, -5.416634, -0.113329, -5.416634, -5.416634, -0.801513, -0.113329};
	float jaspar_core_pwm_21_matrix_row_1_[10] = { -0.560351, -0.560351, -5.175472, -5.175472,  1.041135, -5.175472, -0.560351, -5.175472, -0.560351, -5.175472};
	float jaspar_core_pwm_21_matrix_row_2_[10] = {  0.818490,  0.818490,  1.510389,  1.510389,  0.127833,  0.818490,  1.041135,  1.223123,  1.223123, -5.175472};
	float jaspar_core_pwm_21_matrix_row_3_[10] = { -0.113329, -0.801513, -5.416634, -5.416634, -0.801513, -0.113329, -0.113329, -0.113329, -5.416634,  0.981961};
	float *jaspar_core_pwm_21_matrix[4] = { jaspar_core_pwm_21_matrix_row_0_, jaspar_core_pwm_21_matrix_row_1_, jaspar_core_pwm_21_matrix_row_2_, jaspar_core_pwm_21_matrix_row_3_};
	PWM jaspar_core_pwm_21_ = {
/* accession        */ "MA0079",
/* name             */ "SP1",
/* label            */ " MA0079	9.7185757452318	SP1	ZN-FINGER, C2H2	; acc \"P08047\" ; medline \"2192357\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"9.7160\" ; type \"SELEX\" ",
/* motif            */ "KRGGCRGGGT",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_21_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -44.453773,
/* max_score        */ 10.986727,
/* threshold        */ 0.821,
/* info content     */ 9.519905,
/* base_counts      */ {8, 9, 47, 16},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 8
};

	float jaspar_core_pwm_22_matrix_row_0_[14] = {  0.173688, -0.739613,  0.961056,  1.270969,  1.202024,  0.642943, -1.427797, -6.042918,  1.270969,  1.270969,  1.270969, -6.042918, -1.427797, -0.048956};
	float jaspar_core_pwm_22_matrix_row_1_[14] = { -1.186635, -5.801756, -5.801756, -5.801756, -1.186635, -0.094645,  0.596839, -5.801756, -5.801756, -5.801756, -5.801756,  1.443186, -0.094645, -0.498451};
	float jaspar_core_pwm_22_matrix_row_2_[14] = {  1.001750,  1.369133,  0.192206, -5.801756, -5.801756, -0.094645, -0.498451,  1.512131, -5.801756, -5.801756, -5.801756, -1.186635, -5.801756,  0.750752};
	float jaspar_core_pwm_22_matrix_row_3_[14] = { -6.042918, -6.042918, -6.042918, -6.042918, -6.042918, -1.427797,  0.355677, -6.042918, -6.042918, -6.042918, -6.042918, -6.042918,  0.961056, -0.739613};
	float *jaspar_core_pwm_22_matrix[4] = { jaspar_core_pwm_22_matrix_row_0_, jaspar_core_pwm_22_matrix_row_1_, jaspar_core_pwm_22_matrix_row_2_, jaspar_core_pwm_22_matrix_row_3_};
	PWM jaspar_core_pwm_22_ = {
/* accession        */ "MA0137",
/* name             */ "STAT1",
/* label            */ " MA0137	18.4309054542189	STAT1	Stat	; acc \"Q53XW4\" ; medline \"17558387\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"18.431\" ; type \"COMPILED\" ",
/* motif            */ "GGAAAAYGAAACTN",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_22_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -69.826149,
/* max_score        */ 15.524748,
/* threshold        */ 0.776,
/* info content     */ 18.220993,
/* base_counts      */ {106, 30, 54, 20},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 15
};

	float jaspar_core_pwm_23_matrix_row_0_[8] = {  0.903279, -1.285106,  1.270662, -5.900226, -5.900226, -5.900226, -5.900226, -5.900226};
	float jaspar_core_pwm_23_matrix_row_1_[8] = { -5.659064,  0.893444, -5.659064,  1.431846, -5.659064, -5.659064, -1.043944, -1.043944};
	float jaspar_core_pwm_23_matrix_row_2_[8] = { -5.659064,  0.334897, -5.659064, -5.659064,  1.511824, -5.659064,  1.431846,  1.431846};
	float jaspar_core_pwm_23_matrix_row_3_[8] = {  0.093735, -1.285106, -5.900226, -1.285106, -5.900226,  1.270662, -5.900226, -5.900226};
	float *jaspar_core_pwm_23_matrix[4] = { jaspar_core_pwm_23_matrix_row_0_, jaspar_core_pwm_23_matrix_row_1_, jaspar_core_pwm_23_matrix_row_2_, jaspar_core_pwm_23_matrix_row_3_};
	PWM jaspar_core_pwm_23_ = {
/* accession        */ "MA0128",
/* name             */ "EMBP1",
/* label            */ " MA0128	12.362375216475	EMBP1	bZIP	; acc \"P25032\" ; medline \"10561063\" ; species \"Triticum aestivum\" ; sysgroup \"PLANT\" ; total_ic \"12.362\" ; type \"SELEX\" ",
/* motif            */ "ACACGTGG",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_23_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -42.345528,
/* max_score        */ 10.145410,
/* threshold        */ 0.833,
/* info content     */ 12.201694,
/* base_counts      */ {23, 21, 41, 19},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 13
};

	float jaspar_core_pwm_24_matrix_row_0_[9] = {  0.120177,  0.930875,  1.019413, -7.663464,  1.116247,  1.204527, -1.669503,  0.120177, -0.859959};
	float jaspar_core_pwm_24_matrix_row_1_[9] = { -0.869794, -1.023707, -1.428341,  1.459673, -1.428341, -2.118997, -7.422302, -1.023707, -0.618797};
	float jaspar_core_pwm_24_matrix_row_2_[9] = {  0.617178, -0.869794, -7.422302, -2.118997, -7.422302, -2.807182, -2.807182,  0.820717,  1.190383};
	float jaspar_core_pwm_24_matrix_row_3_[9] = { -0.418522, -0.859959, -0.492576, -2.360159, -1.110956, -2.360159,  1.204527, -0.977603, -1.956354};
	float *jaspar_core_pwm_24_matrix[4] = { jaspar_core_pwm_24_matrix_row_0_, jaspar_core_pwm_24_matrix_row_1_, jaspar_core_pwm_24_matrix_row_2_, jaspar_core_pwm_24_matrix_row_3_};
	PWM jaspar_core_pwm_24_ = {
/* accession        */ "MA0077",
/* name             */ "SOX9",
/* label            */ " MA0077	9.07881462267179	SOX9	HMG	; acc \"P48436\" ; medline \"9973626\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"9.0810\" ; type \"SELEX\" ",
/* motif            */ "NAACAATRG",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_24_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -37.611115,
/* max_score        */ 9.563541,
/* threshold        */ 0.841,
/* info content     */ 9.063681,
/* base_counts      */ {310, 110, 135, 129},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 76
};

	float jaspar_core_pwm_25_matrix_row_0_[6] = {  1.271539,  1.271539,  1.271539, -6.378630, -6.378630,  0.019965};
	float jaspar_core_pwm_25_matrix_row_1_[6] = { -6.137468, -6.137468, -6.137468, -6.137468,  0.771287,  0.261127};
	float jaspar_core_pwm_25_matrix_row_2_[6] = { -6.137468, -6.137468, -6.137468,  1.512701, -0.430358,  0.666037};
	float jaspar_core_pwm_25_matrix_row_3_[6] = { -6.378630, -6.378630, -6.378630, -6.378630,  0.307231, -6.378630};
	float *jaspar_core_pwm_25_matrix[4] = { jaspar_core_pwm_25_matrix_row_0_, jaspar_core_pwm_25_matrix_row_1_, jaspar_core_pwm_25_matrix_row_2_, jaspar_core_pwm_25_matrix_row_3_};
	PWM jaspar_core_pwm_25_ = {
/* accession        */ "MA0021",
/* name             */ "Dof3",
/* label            */ " MA0021	9.00217674837923	Dof3	ZN-FINGER, DOF	; acc \"Q41801\" ; medline \"10074718\" ; species \"Zea mays\" ; sysgroup \"plant\" ; total_ic \"9.0030\" ; type \"SELEX\" ",
/* motif            */ "AAAGYN",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_25_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -38.271782,
/* max_score        */ 6.764642,
/* threshold        */ 0.899,
/* info content     */ 8.920026,
/* base_counts      */ {69, 16, 33, 8},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_26_matrix_row_0_[11] = { -0.515488,  0.863353,  0.172696, -0.515488,  1.085998,  1.267986,  1.085998, -0.515488,  0.576502,  1.085998,  0.576502};
	float jaspar_core_pwm_26_matrix_row_1_[11] = { -0.274326, -4.889446, -4.889446, -4.889446, -4.889446, -4.889446, -4.889446,  0.817664, -4.889446, -4.889446, -0.274326};
	float jaspar_core_pwm_26_matrix_row_2_[11] = { -4.889446, -4.889446,  1.104515, -4.889446, -0.274326, -4.889446, -0.274326, -0.274326, -0.274326, -4.889446, -4.889446};
	float jaspar_core_pwm_26_matrix_row_3_[11] = {  0.863353,  0.172696, -5.130609,  1.085998, -5.130609, -5.130609, -5.130609, -0.515488,  0.172696, -0.515488,  0.172696};
	float *jaspar_core_pwm_26_matrix[4] = { jaspar_core_pwm_26_matrix_row_0_, jaspar_core_pwm_26_matrix_row_1_, jaspar_core_pwm_26_matrix_row_2_, jaspar_core_pwm_26_matrix_row_3_};
	PWM jaspar_core_pwm_26_ = {
/* accession        */ "MA0013",
/* name             */ "Broad-complex_4",
/* label            */ " MA0013	11.6009123934947	Broad-complex_4	ZN-FINGER, C2H2	; acc \"CAA38474\" ; medline \"8062827\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"11.6020\" ; type \"COMPILED\" ",
/* motif            */ "TAGTAAANWAW",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_26_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -50.374596,
/* max_score        */ 10.413865,
/* threshold        */ 0.798,
/* info content     */ 11.284406,
/* base_counts      */ {36, 5, 8, 17},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 6
};

	float jaspar_core_pwm_27_matrix_row_0_[12] = { -6.107290, -0.400180, -6.107290, -0.803985,  0.109316, -6.107290, -6.107290,  1.271093, -6.107290, -6.107290, -1.492170,  0.109316};
	float jaspar_core_pwm_27_matrix_row_1_[12] = {  0.686380,  0.350478, -0.159018, -0.159018, -1.251008, -5.866128, -5.866128, -5.866128,  1.512256, -5.866128,  0.350478,  0.532467};
	float jaspar_core_pwm_27_matrix_row_2_[12] = {  0.350478,  0.127833,  0.532467,  1.137846,  0.686380, -5.866128,  1.447759, -5.866128, -5.866128,  1.512256, -5.866128, -0.159018};
	float jaspar_core_pwm_27_matrix_row_3_[12] = { -0.113329, -0.113329,  0.445218, -6.107290, -0.400180,  1.271093, -1.492170, -6.107290, -6.107290, -6.107290,  0.801465, -0.803985};
	float *jaspar_core_pwm_27_matrix[4] = { jaspar_core_pwm_27_matrix_row_0_, jaspar_core_pwm_27_matrix_row_1_, jaspar_core_pwm_27_matrix_row_2_, jaspar_core_pwm_27_matrix_row_3_};
	PWM jaspar_core_pwm_27_ = {
/* accession        */ "MA0018",
/* name             */ "CREB1",
/* label            */ " MA0018	12.6045644339298	CREB1	bZIP	; acc \"P16220\" ; medline \"8264613\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"12.6060\" ; type \"SELEX\" ",
/* motif            */ "SNKGRTGACGTN",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_27_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -57.179626,
/* max_score        */ 11.741939,
/* threshold        */ 0.783,
/* info content     */ 12.464798,
/* base_counts      */ {32, 46, 67, 47},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_28_matrix_row_0_[7] = { -0.048956,  1.127971, -6.042918, -0.739613, -1.427797, -6.042918,  1.127971};
	float jaspar_core_pwm_28_matrix_row_1_[7] = {  0.192206, -5.801756,  1.443186, -5.801756, -5.801756,  1.512131, -5.801756};
	float jaspar_core_pwm_28_matrix_row_2_[7] = { -5.801756, -1.186635, -5.801756,  1.369133, -5.801756, -5.801756, -1.186635};
	float jaspar_core_pwm_28_matrix_row_3_[7] = {  0.509590, -1.427797, -1.427797, -6.042918,  1.202024, -6.042918, -1.427797};
	float *jaspar_core_pwm_28_matrix[4] = { jaspar_core_pwm_28_matrix_row_0_, jaspar_core_pwm_28_matrix_row_1_, jaspar_core_pwm_28_matrix_row_2_, jaspar_core_pwm_28_matrix_row_3_};
	PWM jaspar_core_pwm_28_ = {
/* accession        */ "MA0129",
/* name             */ "TGA1a",
/* label            */ " MA0129	9.7969611783168	TGA1a	bZIP	; acc \"CAA34468\" ; medline \"10561063\" ; species \"Nicotiana sp.\" ; sysgroup \"PLANT\" ; total_ic \"9.797\" ; type \"SELEX\" ",
/* motif            */ "NACGTCA",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_28_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -41.335777,
/* max_score        */ 8.292006,
/* threshold        */ 0.872,
/* info content     */ 9.691118,
/* base_counts      */ {33, 33, 15, 24},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 15
};

	float jaspar_core_pwm_29_matrix_row_0_[10] = { -7.907328, -2.200217,  1.067417,  0.386972,  0.887649,  0.569252,  0.872383, -0.903354,  0.872383, -7.907328};
	float jaspar_core_pwm_29_matrix_row_1_[10] = {  1.482406,  1.256626, -1.672204, -1.959055, -3.051045, -2.362861, -1.449560, -2.362861, -1.959055, -1.959055};
	float jaspar_core_pwm_29_matrix_row_2_[10] = { -3.051045, -7.666166, -1.959055, -1.672204, -3.051045, -7.666166, -1.449560, -1.959055,  0.271566,  1.416455};
	float jaspar_core_pwm_29_matrix_row_3_[10] = { -2.604023, -0.357192, -0.903354,  0.610065,  0.065483,  0.548203, -0.210661,  1.092415, -3.292207, -1.508733};
	float *jaspar_core_pwm_29_matrix[4] = { jaspar_core_pwm_29_matrix_row_0_, jaspar_core_pwm_29_matrix_row_1_, jaspar_core_pwm_29_matrix_row_2_, jaspar_core_pwm_29_matrix_row_3_};
	PWM jaspar_core_pwm_29_ = {
/* accession        */ "MA0001",
/* name             */ "AGL3",
/* label            */ " MA0001	10.5882147138157	AGL3	MADS	; acc \"P29383\" ; medline \"7632923\" ; species \"Arabidopsis thaliana\" ; sysgroup \"plant\" ; total_ic \"10.5900\" ; type \"SELEX\" ",
/* motif            */ "CCATAWATAG",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_29_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -45.220772,
/* max_score        */ 10.127051,
/* threshold        */ 0.816,
/* info content     */ 10.574041,
/* base_counts      */ {377, 192, 133, 268},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 97
};

	float jaspar_core_pwm_30_matrix_row_0_[10] = { -6.970843, -2.355722, -6.970843, -6.970843, -6.970843, -6.970843,  1.189961, -0.754237, -0.754237, -0.572248};
	float jaspar_core_pwm_30_matrix_row_1_[10] = { -2.114560, -6.729681, -6.729681, -6.729681, -6.729681,  1.459286, -6.729681,  1.094765, -0.043820,  0.179074};
	float jaspar_core_pwm_30_matrix_row_2_[10] = {  1.486678,  1.459286,  1.513339,  1.513339, -6.729681, -2.114560, -1.022570, -1.022570,  0.966986,  0.648703};
	float jaspar_core_pwm_30_matrix_row_3_[10] = { -6.970843, -2.355722, -6.970843, -6.970843,  1.272177, -2.355722, -6.970843, -0.754237, -1.263733, -0.572248};
	float *jaspar_core_pwm_30_matrix[4] = { jaspar_core_pwm_30_matrix_row_0_, jaspar_core_pwm_30_matrix_row_1_, jaspar_core_pwm_30_matrix_row_2_, jaspar_core_pwm_30_matrix_row_3_};
	PWM jaspar_core_pwm_30_ = {
/* accession        */ "MA0016",
/* name             */ "usp",
/* label            */ " MA0016	13.7920230760767	usp	NUCLEAR RECEPTOR	; acc \"NP_476781\" ; medline \"1280827\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"13.7890\" ; type \"SELEX\" ",
/* motif            */ "GGGGTCACGN",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_30_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -51.413292,
/* max_score        */ 12.604520,
/* threshold        */ 0.801,
/* info content     */ 13.730608,
/* base_counts      */ {52, 80, 194, 54},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 38
};

	float jaspar_core_pwm_31_matrix_row_0_[12] = { -1.205318,  0.983066, -5.820439,  1.270471, -5.820439, -5.820439, -5.820439, -5.820439,  0.396167, -1.205318, -0.517134, -5.820439};
	float jaspar_core_pwm_31_matrix_row_1_[12] = {  0.819318, -5.579277,  1.511633, -5.579277, -5.579277, -5.579277,  1.511633,  1.424697, -5.579277,  0.973231,  0.414685, -0.275972};
	float jaspar_core_pwm_31_matrix_row_2_[12] = { -0.964156,  0.127833, -5.579277, -5.579277, -5.579277, -5.579277, -5.579277, -5.579277, -5.579277,  0.414685,  0.127833,  1.106584};
	float jaspar_core_pwm_31_matrix_row_3_[12] = {  0.173523, -5.820439, -5.820439, -5.820439,  1.270471,  1.270471, -5.820439, -1.205318,  0.732069, -5.820439, -0.113329, -0.517134};
	float *jaspar_core_pwm_31_matrix[4] = { jaspar_core_pwm_31_matrix_row_0_, jaspar_core_pwm_31_matrix_row_1_, jaspar_core_pwm_31_matrix_row_2_, jaspar_core_pwm_31_matrix_row_3_};
	PWM jaspar_core_pwm_31_ = {
/* accession        */ "MA0090",
/* name             */ "TEAD1",
/* label            */ " MA0090	15.6777724709562	TEAD1	TEA	; acc \"P28347\" ; medline \"9571041\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"15.6790\" ; type \"COMPILED\" ",
/* motif            */ "YACATTCCTCNG",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_31_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -59.685680,
/* max_score        */ 13.288328,
/* threshold        */ 0.780,
/* info content     */ 15.451029,
/* base_counts      */ {30, 54, 19, 41},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12
};

	float jaspar_core_pwm_32_matrix_row_0_[14] = { -2.014401,  0.279233,  0.809450,  0.541367, -0.922412, -0.077014, -6.629522,  1.271856,  1.271856,  1.271856, -6.629522,  1.271856,  0.748862, -0.077014};
	float jaspar_core_pwm_32_matrix_row_1_[14] = {  0.520395,  0.164148, -0.394398, -0.171754,  0.615614, -6.388360, -6.388360, -6.388360, -6.388360, -6.388360,  1.436086, -6.388360, -0.394398, -0.394398};
	float jaspar_core_pwm_32_matrix_row_2_[14] = {  0.164148, -0.171754, -1.085055, -0.171754,  0.297501,  1.213043, -6.388360, -6.388360, -6.388360, -6.388360, -6.388360, -6.388360, -1.085055,  0.010235};
	float jaspar_core_pwm_32_matrix_row_3_[14] = {  0.173983, -0.412916, -0.635560, -0.412916, -6.629522, -6.629522,  1.271856, -6.629522, -6.629522, -6.629522, -1.326217, -6.629522, -0.412916,  0.279233};
	float *jaspar_core_pwm_32_matrix[4] = { jaspar_core_pwm_32_matrix_row_0_, jaspar_core_pwm_32_matrix_row_1_, jaspar_core_pwm_32_matrix_row_2_, jaspar_core_pwm_32_matrix_row_3_};
	PWM jaspar_core_pwm_32_ = {
/* accession        */ "MA0030",
/* name             */ "FOXF2",
/* label            */ " MA0030	14.8237690744435	FOXF2	FORKHEAD	; acc \"Q12947\" ; medline \"7957066\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.8250\" ; type \"SELEX\" ",
/* motif            */ "NNANSGTAAACAAN",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_32_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -58.440914,
/* max_score        */ 12.802560,
/* threshold        */ 0.765,
/* info content     */ 14.732682,
/* base_counts      */ {182, 70, 55, 67},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 27
};

	float jaspar_core_pwm_33_matrix_row_0_[20] = { -0.958726, -0.958726,  0.884299, -6.665837, -2.050716, -6.665837, -1.362532,  1.197815, -0.449230, -0.449230, -0.671875, -2.050716, -1.362532,  1.030831, -2.050716, -6.665837, -0.958726,  1.030831, -0.449230, -0.113329};
	float jaspar_core_pwm_33_matrix_row_1_[20] = {  0.261186, -6.424675, -6.424675, -1.809554, -6.424675, -1.809554,  1.316425, -1.809554,  0.889212,  0.127833, -1.121370, -6.424675, -0.208068, -0.208068,  1.476703,  1.399771,  0.666235, -0.208068,  0.666235, -6.424675};
	float jaspar_core_pwm_33_matrix_row_2_[20] = {  0.820267, -6.424675,  0.378831,  1.476703,  1.438977, -0.430713, -0.717564, -6.424675, -0.430713,  0.484080,  1.071423, -1.121370,  1.176728, -6.424675, -6.424675, -6.424675, -6.424675, -6.424675, -0.026080, -1.809554};
	float jaspar_core_pwm_33_matrix_row_3_[20] = { -0.958726,  1.158609, -6.665837, -6.665837, -2.050716,  1.075262, -6.665837, -2.050716, -0.671875, -0.267242, -0.671875,  1.158609, -2.050716, -2.050716, -6.665837, -0.958726,  0.505052, -2.050716, -0.449230,  0.935566};
	float *jaspar_core_pwm_33_matrix[4] = { jaspar_core_pwm_33_matrix_row_0_, jaspar_core_pwm_33_matrix_row_1_, jaspar_core_pwm_33_matrix_row_2_, jaspar_core_pwm_33_matrix_row_3_};
	PWM jaspar_core_pwm_33_ = {
/* accession        */ "MA0066",
/* name             */ "PPARG",
/* label            */ " MA0066	20.3650558402138	PPARG	NUCLEAR RECEPTOR	; acc \"P37231\" ; medline \"11139380\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"20.3660\" ; type \"SELEX\" ",
/* motif            */ "STAGGTCACNGTGACCYANT",
/* library_name     */ "jaspar_core",
/* length           */ 20,
/* matrixname       */ jaspar_core_pwm_33_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.093575,
/* max_score        */ 21.354580,
/* threshold        */ 0.799,
/* info content     */ 20.253382,
/* base_counts      */ {131, 149, 144, 136},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 28
};

	float jaspar_core_pwm_34_matrix_row_0_[16] = { -0.267005,  0.578393,  0.829390, -0.267005,  1.029859,  1.029859,  1.029859, -0.267005,  0.019847, -0.267005,  0.711746,  0.711746,  0.829390,  0.829390,  1.029859, -0.670810};
	float jaspar_core_pwm_34_matrix_row_1_[16] = {  0.483653, -5.732953, -1.117832,  0.665642, -5.732953, -5.732953, -5.732953, -0.025843, -1.117832,  0.261009,  0.483653, -1.117832, -5.732953,  0.483653, -5.732953,  0.819555};
	float jaspar_core_pwm_34_matrix_row_2_[16] = {  0.261009, -0.025843, -1.117832,  0.261009, -0.025843, -0.429648, -0.429648, -0.429648,  0.952908,  0.665642, -1.117832,  0.261009, -0.429648, -5.732953, -0.025843, -5.732953};
	float jaspar_core_pwm_34_matrix_row_3_[16] = { -0.670810,  0.019847, -0.267005, -1.358994, -5.974115, -1.358994, -1.358994,  0.424480, -1.358994, -1.358994, -5.974115, -1.358994, -0.267005, -5.974115, -5.974115,  0.242491};
	float *jaspar_core_pwm_34_matrix[4] = { jaspar_core_pwm_34_matrix_row_0_, jaspar_core_pwm_34_matrix_row_1_, jaspar_core_pwm_34_matrix_row_2_, jaspar_core_pwm_34_matrix_row_3_};
	PWM jaspar_core_pwm_34_ = {
/* accession        */ "MA0045",
/* name             */ "HMG-IY",
/* label            */ " MA0045	10.3410853256607	HMG-IY	HMG	; acc \"CAA61747\" ; medline \"9161031\" ; species \"Pisum sativum\" ; sysgroup \"plant\" ; total_ic \"10.3400\" ; type \"SELEX\" ",
/* motif            */ "NWANAAANGNAAAAAY",
/* library_name     */ "jaspar_core",
/* length           */ 16,
/* matrixname       */ jaspar_core_pwm_34_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -60.215485,
/* max_score        */ 12.621374,
/* threshold        */ 0.718,
/* info content     */ 10.223422,
/* base_counts      */ {112, 38, 45, 29},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 14
};

	float jaspar_core_pwm_35_matrix_row_0_[11] = { -7.832458, -2.529154,  0.410561,  0.883749,  1.063308,  0.207021, -0.091359, -0.231056, -0.135791, -3.217338,  0.068919};
	float jaspar_core_pwm_35_matrix_row_1_[11] = {  1.479897,  1.468337, -0.787791, -1.374690, -2.976176, -1.884186, -0.212913, -0.420408, -7.591297, -7.591297,  0.233149};
	float jaspar_core_pwm_35_matrix_row_2_[11] = { -2.287992, -7.591297, -1.038789, -2.287992, -2.976176, -1.597335, -0.152325,  0.272355,  1.172913,  1.420715, -0.787791};
	float jaspar_core_pwm_35_matrix_row_3_[11] = { -3.217338, -2.529154,  0.356508, -0.135791, -0.518572,  0.724148,  0.299366,  0.207021, -1.838497, -1.279951,  0.140352};
	float *jaspar_core_pwm_35_matrix[4] = { jaspar_core_pwm_35_matrix_row_0_, jaspar_core_pwm_35_matrix_row_1_, jaspar_core_pwm_35_matrix_row_2_, jaspar_core_pwm_35_matrix_row_3_};
	PWM jaspar_core_pwm_35_ = {
/* accession        */ "MA0005",
/* name             */ "Agamous",
/* label            */ " MA0005	9.09439842911724	Agamous	MADS	; acc \"P17839\" ; medline \"7901838\" ; species \"Arabidopsis thaliana\" ; sysgroup \"plant\" ; total_ic \"9.0940 , 11.598\" ; type \"SELEX\" ",
/* motif            */ "CCWAATNNGGN",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_35_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -40.214603,
/* max_score        */ 9.428498,
/* threshold        */ 0.804,
/* info content     */ 9.080895,
/* base_counts      */ {298, 245, 214, 233},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 90
};

	float jaspar_core_pwm_36_matrix_row_0_[8] = {  0.141274, -0.214973, -1.060371, -6.767481,  1.096170, -1.464176, -1.464176, -2.152361};
	float jaspar_core_pwm_36_matrix_row_1_[8] = {  0.026189, -1.911199, -1.223014,  1.411413, -1.911199,  0.912652, -6.526319,  0.477655};
	float jaspar_core_pwm_36_matrix_row_2_[8] = { -0.309713,  1.123850, -6.526319, -1.911199, -0.819209, -1.911199,  1.023816,  0.477655};
	float jaspar_core_pwm_36_matrix_row_3_[8] = {  0.036024, -1.464176,  1.096170, -1.464176, -2.152361,  0.236493,  0.141274, -0.081620};
	float *jaspar_core_pwm_36_matrix[4] = { jaspar_core_pwm_36_matrix_row_0_, jaspar_core_pwm_36_matrix_row_1_, jaspar_core_pwm_36_matrix_row_2_, jaspar_core_pwm_36_matrix_row_3_};
	PWM jaspar_core_pwm_36_ = {
/* accession        */ "MA0067",
/* name             */ "Pax2",
/* label            */ " MA0067	6.20567063978618	Pax2	PAIRED	; acc \"P32114\" ; medline \"8132558\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"6.2060\" ; type \"SELEX\" ",
/* motif            */ "NGTCACGN",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_36_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -28.256950,
/* max_score        */ 7.283000,
/* threshold        */ 0.884,
/* info content     */ 6.180756,
/* base_counts      */ {51, 67, 61, 69},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 31
};

	float jaspar_core_pwm_37_matrix_row_0_[4] = { -2.668777, -8.375888, -8.375888,  0.672051};
	float jaspar_core_pwm_37_matrix_row_1_[4] = {  0.747250, -2.140764, -2.427615, -1.331220};
	float jaspar_core_pwm_37_matrix_row_2_[4] = {  0.840019, -3.519605, -1.736131, -2.831421};
	float jaspar_core_pwm_37_matrix_row_3_[4] = { -3.760767, -2.159281, -2.381926,  0.586120};
	float *jaspar_core_pwm_37_matrix[4] = { jaspar_core_pwm_37_matrix_row_0_, jaspar_core_pwm_37_matrix_row_1_, jaspar_core_pwm_37_matrix_row_2_, jaspar_core_pwm_37_matrix_row_3_};
	PWM jaspar_core_pwm_37_ = {
/* accession        */ "MA0094",
/* name             */ "Ubx",
/* label            */ " MA0094	5.57649394986978	Ubx	HOMEO	; acc \"P83949\" ; medline \"1673656\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"5.5770\" ; type \"SELEX\" ",
/* motif            */ "GYKW",
/* library_name     */ "jaspar_core",
/* length           */ 4,
/* matrixname       */ jaspar_core_pwm_37_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -23.343964,
/* max_score        */ -2.364824,
/* threshold        */ 1.004,
/* info content     */ 4.764989,
/* base_counts      */ {88, 88, 88, 88},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 155
};

	float jaspar_core_pwm_38_matrix_row_0_[14] = {  0.173247, -0.918743,  0.682742,  1.018644,  0.173247,  0.864731,  0.460098,  1.018644, -0.918743,  1.269642,  1.151997,  0.682742,  0.460098, -0.230559};
	float jaspar_core_pwm_38_matrix_row_1_[14] = { -0.677581, -0.677581,  0.010603, -5.292702, -5.292702, -5.292702, -0.677581, -5.292702,  1.393159, -5.292702, -5.292702, -5.292702, -5.292702,  0.414409};
	float jaspar_core_pwm_38_matrix_row_2_[14] = {  0.701260, -0.677581, -0.677581, -0.677581, -5.292702, -5.292702,  0.701260, -0.677581, -5.292702, -5.292702, -0.677581,  0.414409, -5.292702,  0.010603};
	float jaspar_core_pwm_38_matrix_row_3_[14] = { -0.918743,  0.864731, -0.918743, -0.918743,  0.864731,  0.173247, -5.533864, -0.918743, -5.533864, -5.533864, -5.533864, -0.918743,  0.682742, -0.230559};
	float *jaspar_core_pwm_38_matrix[4] = { jaspar_core_pwm_38_matrix_row_0_, jaspar_core_pwm_38_matrix_row_1_, jaspar_core_pwm_38_matrix_row_2_, jaspar_core_pwm_38_matrix_row_3_};
	PWM jaspar_core_pwm_38_ = {
/* accession        */ "MA0010",
/* name             */ "Broad-complex_1",
/* label            */ " MA0010	12.6171586319072	Broad-complex_1	ZN-FINGER, C2H2	; acc \"CAA38474\" ; comment \"Different splice forms affacts DNA binding: four matrices\" ; medline \"8062827\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"12.6150\" ; type \"COMPILED\" ",
/* motif            */ "RTAATARACAAATN",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_38_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -56.878452,
/* max_score        */ 12.311436,
/* threshold        */ 0.758,
/* info content     */ 12.391441,
/* base_counts      */ {65, 16, 18, 27},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 9
};

	float jaspar_core_pwm_39_matrix_row_0_[15] = { -0.277416, -6.829924,  0.548460,  0.609048, -6.829924, -6.829924, -2.214803,  0.953717,  0.340965, -6.829924, -6.829924, -6.829924, -6.829924, -6.829924,  0.174051};
	float jaspar_core_pwm_39_matrix_row_1_[15] = { -0.594800,  0.789622,  0.850210,  0.789622,  1.513219,  1.513219,  0.850210, -6.588761, -6.588761, -6.588761, -6.588761, -6.588761, -6.588761,  0.097100,  1.107906};
	float jaspar_core_pwm_39_matrix_row_2_[15] = {  1.107906,  0.850210, -6.588761, -6.588761, -6.588761, -6.588761, -6.588761, -6.588761,  0.789622,  1.513219,  1.513219,  0.850210,  0.789622,  0.789622, -6.588761};
	float jaspar_core_pwm_39_matrix_row_3_[15] = { -6.829924, -6.829924, -6.829924, -6.829924, -6.829924, -6.829924,  0.483963, -0.026418, -0.835962, -6.829924, -6.829924,  0.548460,  0.609048, -0.026418, -6.829924};
	float *jaspar_core_pwm_39_matrix[4] = { jaspar_core_pwm_39_matrix_row_0_, jaspar_core_pwm_39_matrix_row_1_, jaspar_core_pwm_39_matrix_row_2_, jaspar_core_pwm_39_matrix_row_3_};
	PWM jaspar_core_pwm_39_ = {
/* accession        */ "MA0116",
/* name             */ "Roaz",
/* label            */ " MA0116	17.9252482810031	Roaz	ZN-FINGER, C2H2	; acc \"O08961\" ; medline \"9774661\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"17.9252482810031\" ; type \"SELEX\" ",
/* motif            */ "GGCACCCARGGGTKC",
/* library_name     */ "jaspar_core",
/* length           */ 15,
/* matrixname       */ jaspar_core_pwm_39_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.725380,
/* max_score        */ 15.781734,
/* threshold        */ 0.773,
/* info content     */ 17.809155,
/* base_counts      */ {89, 166, 170, 70},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 33
};

	float jaspar_core_pwm_40_matrix_row_0_[6] = { -2.431631,  1.196268, -7.046752, -7.046752, -7.046752, -0.137997};
	float jaspar_core_pwm_40_matrix_row_1_[6] = {  1.326235, -6.805590,  1.488710,  1.437430, -2.190469, -2.190469};
	float jaspar_core_pwm_40_matrix_row_2_[6] = { -2.190469, -1.502285, -2.190469, -1.098479, -6.805590,  0.572794};
	float jaspar_core_pwm_40_matrix_row_3_[6] = { -0.830146, -2.431631, -7.046752, -7.046752,  1.247548,  0.198190};
	float *jaspar_core_pwm_40_matrix[4] = { jaspar_core_pwm_40_matrix_row_0_, jaspar_core_pwm_40_matrix_row_1_, jaspar_core_pwm_40_matrix_row_2_, jaspar_core_pwm_40_matrix_row_3_};
	PWM jaspar_core_pwm_40_ = {
/* accession        */ "MA0103",
/* name             */ "ZEB1",
/* label            */ " MA0103	8.30486487593768	ZEB1	ZN-FINGER, C2H2	; acc \"P36197\" ; medline \"8065305\" ; species \"Gallus gallus\" ; sysgroup \"vertebrate\" ; total_ic \"8.3070\" ; type \"SELEX\" ",
/* motif            */ "CACCTN",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_40_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -32.567947,
/* max_score        */ 7.268985,
/* threshold        */ 0.907,
/* info content     */ 8.273649,
/* base_counts      */ {49, 114, 23, 60},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 41
};

	float jaspar_core_pwm_41_matrix_row_0_[10] = { -2.778216, -7.393337,  1.255060, -2.090032, -0.589832, -0.994742,  0.823021, -2.090032,  1.237363, -0.994742};
	float jaspar_core_pwm_41_matrix_row_1_[10] = {  1.365218, -7.152175, -2.537054, -2.537054, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175};
	float jaspar_core_pwm_41_matrix_row_2_[10] = { -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -1.848870,  1.365218};
	float jaspar_core_pwm_41_matrix_row_3_[10] = { -0.840829,  1.272449, -7.393337,  1.219348,  1.103858,  1.163269,  0.256832,  1.237363, -7.393337, -2.090032};
	float *jaspar_core_pwm_41_matrix[4] = { jaspar_core_pwm_41_matrix_row_0_, jaspar_core_pwm_41_matrix_row_1_, jaspar_core_pwm_41_matrix_row_2_, jaspar_core_pwm_41_matrix_row_3_};
	PWM jaspar_core_pwm_41_ = {
/* accession        */ "MA0052",
/* name             */ "MEF2A",
/* label            */ " MA0052	15.7090560936245	MEF2A	MADS	; acc \"EAX02249\" ; medline \"1748287\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"15.7090\" ; type \"SELEX\" ",
/* motif            */ "CTATTTATAG",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_41_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.245239,
/* max_score        */ 12.042169,
/* threshold        */ 0.792,
/* info content     */ 15.659736,
/* base_counts      */ {176, 52, 52, 300},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 58
};

	float jaspar_core_pwm_42_matrix_row_0_[7] = { -0.864463,  0.048838, -6.167768, -6.167768,  1.271204,  1.271204,  0.048838};
	float jaspar_core_pwm_42_matrix_row_1_[7] = {  1.164304,  1.164304, -5.926606, -5.926606, -5.926606, -5.926606, -1.311486};
	float jaspar_core_pwm_42_matrix_row_2_[7] = { -0.623301, -5.926606,  1.512366,  1.512366, -5.926606, -5.926606,  0.982149};
	float jaspar_core_pwm_42_matrix_row_3_[7] = { -1.552648, -6.167768, -6.167768, -6.167768, -6.167768, -6.167768, -1.552648};
	float *jaspar_core_pwm_42_matrix[4] = { jaspar_core_pwm_42_matrix_row_0_, jaspar_core_pwm_42_matrix_row_1_, jaspar_core_pwm_42_matrix_row_2_, jaspar_core_pwm_42_matrix_row_3_};
	PWM jaspar_core_pwm_42_ = {
/* accession        */ "MA0026",
/* name             */ "Eip74EF",
/* label            */ " MA0026	10.3539445300757	Eip74EF	ETS	; acc \"P20105\" ; medline \"2208281\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"10.3540\" ; type \"SELEX\" ",
/* motif            */ "CCGGAAG",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_42_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -33.944134,
/* max_score        */ 8.877894,
/* threshold        */ 0.866,
/* info content     */ 10.248544,
/* base_counts      */ {46, 25, 46, 2},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_43_matrix_row_0_[5] = { -0.132362, -7.303251,  1.253355, -7.303251,  0.521195};
	float jaspar_core_pwm_43_matrix_row_1_[5] = {  0.108800, -0.845483, -7.062089, -7.062089, -0.509581};
	float jaspar_core_pwm_43_matrix_row_2_[5] = {  0.434009,  1.414491, -2.446968, -7.062089,  0.251798};
	float jaspar_core_pwm_43_matrix_row_3_[5] = { -0.499746, -7.303251, -7.303251,  1.272400, -0.904656};
	float *jaspar_core_pwm_43_matrix[4] = { jaspar_core_pwm_43_matrix_row_0_, jaspar_core_pwm_43_matrix_row_1_, jaspar_core_pwm_43_matrix_row_2_, jaspar_core_pwm_43_matrix_row_3_};
	PWM jaspar_core_pwm_43_ = {
/* accession        */ "MA0036",
/* name             */ "GATA2",
/* label            */ " MA0036	5.68777359118543	GATA2	ZN-FINGER, GATA	; acc \"P23769\" ; medline \"8321207\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"5.6870\" ; type \"SELEX\" ",
/* motif            */ "NGATR",
/* library_name     */ "jaspar_core",
/* length           */ 5,
/* matrixname       */ jaspar_core_pwm_43_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -23.314154,
/* max_score        */ 4.895449,
/* threshold        */ 0.964,
/* info content     */ 5.667809,
/* base_counts      */ {90, 25, 82, 68},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 53
};

	float jaspar_core_pwm_44_matrix_row_0_[12] = { -1.609676,  0.173799, -1.609676, -6.224796,  0.946092, -6.224796,  0.173799, -6.224796,  0.946092,  1.089091, -0.921491, -0.008190};
	float jaspar_core_pwm_44_matrix_row_1_[12] = {  0.010327, -5.983634, -5.983634, -5.983634, -1.368514,  1.330253, -5.983634,  0.819871,  0.010327, -5.983634, -0.276524,  0.232972};
	float jaspar_core_pwm_44_matrix_row_2_[12] = {  0.702227,  1.107276, -5.983634, -0.276524, -0.680329, -1.368514,  1.107276, -5.983634, -1.368514, -1.368514, -1.368514, -0.276524};
	float jaspar_core_pwm_44_matrix_row_3_[12] = { -0.008190, -6.224796,  1.214175,  1.089091, -0.921491, -0.921491, -6.224796,  0.578709, -6.224796, -0.921491,  0.866114, -0.008190};
	float *jaspar_core_pwm_44_matrix[4] = { jaspar_core_pwm_44_matrix_row_0_, jaspar_core_pwm_44_matrix_row_1_, jaspar_core_pwm_44_matrix_row_2_, jaspar_core_pwm_44_matrix_row_3_};
	PWM jaspar_core_pwm_44_ = {
/* accession        */ "MA0043",
/* name             */ "HLF",
/* label            */ " MA0043	11.1469251071435	HLF	bZIP	; acc \"Q16534\" ; medline \"8065331\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"11.1450\" ; type \"SELEX\" ",
/* motif            */ "NGTTACGYAATN",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_44_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -53.939274,
/* max_score        */ 11.450530,
/* threshold        */ 0.784,
/* info content     */ 11.045853,
/* base_counts      */ {62, 41, 44, 69},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18
};

	float jaspar_core_pwm_45_matrix_row_0_[10] = { -0.113329,  0.242918,  0.137669, -0.449230, -1.362532, -6.665837, -2.050716,  1.235541,  0.984332,  0.505052};
	float jaspar_core_pwm_45_matrix_row_1_[10] = {  0.127833, -0.026080, -0.430713,  1.125461,  1.358966, -6.424675, -6.424675, -6.424675, -0.208068, -6.424675};
	float jaspar_core_pwm_45_matrix_row_2_[10] = {  0.484080, -0.026080,  0.484080, -1.809554, -1.809554,  1.358966,  1.476703, -1.809554, -6.424675,  0.820267};
	float jaspar_core_pwm_45_matrix_row_3_[10] = { -0.671875, -0.267242, -0.449230, -0.958726, -2.050716, -0.671875, -6.665837, -6.665837, -1.362532, -2.050716};
	float *jaspar_core_pwm_45_matrix[4] = { jaspar_core_pwm_45_matrix_row_0_, jaspar_core_pwm_45_matrix_row_1_, jaspar_core_pwm_45_matrix_row_2_, jaspar_core_pwm_45_matrix_row_3_};
	PWM jaspar_core_pwm_45_ = {
/* accession        */ "MA0028",
/* name             */ "ELK1",
/* label            */ " MA0028	8.81228541287854	ELK1	ETS	; acc \"P19419\" ; medline \"1425594\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"8.8130\" ; type \"SELEX\" ",
/* motif            */ "NNNCCGGAAR",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_45_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -38.095474,
/* max_score        */ 9.571314,
/* threshold        */ 0.824,
/* info content     */ 8.764382,
/* base_counts      */ {95, 65, 94, 26},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 28
};

	float jaspar_core_pwm_46_matrix_row_0_[22] = {  0.291582,  0.291582,  0.492050,  0.866460, -6.511924,  0.578986,  1.138245, -6.511924,  0.801963, -0.517962, -0.295318, -0.113329, -0.804813, -6.511924, -0.517962,  0.492050, -1.896803, -0.804813, -0.113329, -0.113329,  0.396831, -0.295318};
	float jaspar_core_pwm_46_matrix_row_1_[22] = {  0.281746, -0.967457, -0.563651, -1.655641, -6.270761,  0.127833, -0.967457,  1.512879, -6.270761,  0.532744,  0.733213,  0.532744, -0.054155, -6.270761, -6.270761, -0.054155,  1.425905,  1.107622,  0.281746, -0.054155,  0.733213,  0.733213};
	float jaspar_core_pwm_46_matrix_row_2_[22] = { -0.967457, -0.563651, -0.563651,  0.281746,  1.425905, -1.655641, -6.270761, -6.270761,  0.415099, -0.967457, -0.967457,  0.532744, -1.655641,  1.512879, -6.270761, -1.655641, -1.655641, -1.655641, -0.054155,  0.532744, -6.270761,  0.127833};
	float jaspar_core_pwm_46_matrix_row_3_[22] = { -0.113329,  0.396831,  0.040584, -6.511924, -1.208619, -0.295318, -1.896803, -6.511924, -1.896803,  0.291582, -0.113329, -6.511924,  0.801963, -6.511924,  1.089479,  0.040584, -6.511924, -0.517962, -0.113329, -0.517962, -0.804813, -1.208619};
	float *jaspar_core_pwm_46_matrix[4] = { jaspar_core_pwm_46_matrix_row_0_, jaspar_core_pwm_46_matrix_row_1_, jaspar_core_pwm_46_matrix_row_2_, jaspar_core_pwm_46_matrix_row_3_};
	PWM jaspar_core_pwm_46_ = {
/* accession        */ "MA0007",
/* name             */ "Ar",
/* label            */ " MA0007	15.703395549218	Ar	NUCLEAR RECEPTOR	; acc \"P15207\" ; medline \"1491700\" ; species \"Rattus rattus\" ; sysgroup \"vertebrate\" ; total_ic \"15.7070\" ; type \"SELEX\" ",
/* motif            */ "NWWAGMACAYNSTGTWCCNNMN",
/* library_name     */ "jaspar_core",
/* length           */ 22,
/* matrixname       */ jaspar_core_pwm_46_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.050529,
/* max_score        */ 18.014418,
/* threshold        */ 0.719,
/* info content     */ 15.599894,
/* base_counts      */ {157, 156, 107, 108},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 24
};

	float jaspar_core_pwm_47_matrix_row_0_[8] = { -6.042918, -1.427797, -6.042918, -1.427797,  1.047992, -0.335808, -0.335808, -0.739613};
	float jaspar_core_pwm_47_matrix_row_1_[8] = { -5.801756,  1.289154, -0.094645, -5.801756, -0.498451,  0.884105, -1.186635,  0.414850};
	float jaspar_core_pwm_47_matrix_row_2_[8] = {  1.512131, -1.186635, -1.186635,  1.289154, -5.801756, -1.186635,  0.750752,  0.414850};
	float jaspar_core_pwm_47_matrix_row_3_[8] = { -6.042918, -1.427797,  0.961056, -0.739613, -1.427797, -0.335808, -0.048956, -0.335808};
	float *jaspar_core_pwm_47_matrix[4] = { jaspar_core_pwm_47_matrix_row_0_, jaspar_core_pwm_47_matrix_row_1_, jaspar_core_pwm_47_matrix_row_2_, jaspar_core_pwm_47_matrix_row_3_};
	PWM jaspar_core_pwm_47_ = {
/* accession        */ "MA0117",
/* name             */ "Mafb",
/* label            */ " MA0117	6.76893921374478	Mafb	bZIP, MAF	; acc \"P54842\" ; medline \"9571165\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"6.76893921374478\" ; type \"SELEX\" ",
/* motif            */ "GCTGACNN",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_47_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -28.230028,
/* max_score        */ 8.149195,
/* threshold        */ 0.880,
/* info content     */ 6.705714,
/* base_counts      */ {22, 31, 42, 25},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 15
};

	float jaspar_core_pwm_48_matrix_row_0_[10] = { -6.167768, -6.167768, -1.552648,  0.048838,  0.230827,  0.048838, -1.552648, -0.864463, -6.167768, -1.552648};
	float jaspar_core_pwm_48_matrix_row_1_[10] = {  0.290000, -1.311486, -5.926606, -1.311486,  0.290000, -1.311486, -5.926606, -5.926606,  1.387281,  1.451778};
	float jaspar_core_pwm_48_matrix_row_2_[10] = {  0.759255,  1.387281,  1.387281,  0.876899, -0.219496, -1.311486, -5.926606, -5.926606, -5.926606, -5.926606};
	float jaspar_core_pwm_48_matrix_row_3_[10] = { -0.173807, -1.552648, -1.552648, -0.864463, -0.460658,  0.740987,  1.210616,  1.146119, -0.864463, -6.167768};
	float *jaspar_core_pwm_48_matrix[4] = { jaspar_core_pwm_48_matrix_row_0_, jaspar_core_pwm_48_matrix_row_1_, jaspar_core_pwm_48_matrix_row_2_, jaspar_core_pwm_48_matrix_row_3_};
	PWM jaspar_core_pwm_48_ = {
/* accession        */ "MA0101",
/* name             */ "REL",
/* label            */ " MA0101	10.514792387777	REL	REL	; acc \"Q04864\" ; medline \"1406630\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"10.5180\" ; type \"SELEX\" ",
/* motif            */ "SGGGNTTTCC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_48_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -45.534515,
/* max_score        */ 10.637495,
/* threshold        */ 0.816,
/* info content     */ 10.420120,
/* base_counts      */ {21, 44, 51, 54},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_49_matrix_row_0_[6] = { -7.022083,  1.246905, -1.314973, -1.718779, -7.022083, -7.022083};
	float jaspar_core_pwm_49_matrix_row_1_[6] = {  1.488067, -6.780921, -6.780921, -6.780921, -6.780921, -6.780921};
	float jaspar_core_pwm_49_matrix_row_2_[6] = { -2.165801, -6.780921,  1.435437,  1.462098, -6.780921,  1.462098};
	float jaspar_core_pwm_49_matrix_row_3_[6] = { -7.022083, -2.406963, -7.022083, -7.022083,  1.272216, -1.718779};
	float *jaspar_core_pwm_49_matrix[4] = { jaspar_core_pwm_49_matrix_row_0_, jaspar_core_pwm_49_matrix_row_1_, jaspar_core_pwm_49_matrix_row_2_, jaspar_core_pwm_49_matrix_row_3_};
	PWM jaspar_core_pwm_49_ = {
/* accession        */ "MA0086",
/* name             */ "sna",
/* label            */ " MA0086	10.7055726786483	sna	ZN-FINGER, C2H2	; acc \"P08044\" ; medline \"8371971\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"10.7080\" ; type \"SELEX\" ",
/* motif            */ "CAGGTG",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_49_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -41.891338,
/* max_score        */ 8.366820,
/* threshold        */ 0.878,
/* info content     */ 10.657499,
/* base_counts      */ {44, 39, 114, 43},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 40
};

	float jaspar_core_pwm_50_matrix_row_0_[11] = { -0.113329,  0.983066,  1.088316,  1.270471, -5.820439,  0.173523,  1.088316,  0.578156,  0.578156,  0.173523, -0.517134};
	float jaspar_core_pwm_50_matrix_row_1_[11] = { -0.964156, -0.275972, -5.579277, -5.579277,  1.329478, -5.579277, -5.579277, -5.579277, -0.964156,  0.127833,  0.127833};
	float jaspar_core_pwm_50_matrix_row_2_[11] = { -0.964156, -0.964156, -5.579277, -5.579277, -0.964156, -5.579277, -5.579277,  0.414685, -0.275972, -0.275972,  0.637329};
	float jaspar_core_pwm_50_matrix_row_3_[11] = {  0.732069, -5.820439, -0.517134, -5.820439, -1.205318,  0.865422, -0.517134, -0.517134, -0.113329, -0.113329, -0.517134};
	float *jaspar_core_pwm_50_matrix[4] = { jaspar_core_pwm_50_matrix_row_0_, jaspar_core_pwm_50_matrix_row_1_, jaspar_core_pwm_50_matrix_row_2_, jaspar_core_pwm_50_matrix_row_3_};
	PWM jaspar_core_pwm_50_ = {
/* accession        */ "MA0012",
/* name             */ "Broad-complex_3",
/* label            */ " MA0012	9.33723168511451	Broad-complex_3	ZN-FINGER, C2H2	; acc \"NP_726754\" ; comment \"Different splice forms affacts DNA binding: four matrices\" ; medline \"8062827\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"9.3380\" ; type \"COMPILED\" ",
/* motif            */ "TAAACTARWNN",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_50_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -42.499844,
/* max_score        */ 9.324303,
/* threshold        */ 0.804,
/* info content     */ 9.210832,
/* base_counts      */ {66, 20, 16, 30},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12
};

	float jaspar_core_pwm_51_matrix_row_0_[10] = { -0.499746,  0.634481,  1.272400,  1.272400, -2.688130, -2.688130,  0.736229, -0.617390,  0.246884, -0.750743};
	float jaspar_core_pwm_51_matrix_row_1_[10] = {  0.539314,  0.316295, -7.062089, -7.062089, -7.062089,  1.494517, -0.509581,  0.875643, -0.058115,  0.108800};
	float jaspar_core_pwm_51_matrix_row_2_[10] = { -0.258584, -0.509581, -7.062089, -7.062089, -2.446968, -7.062089, -1.758784, -0.058115, -1.758784,  0.875643};
	float jaspar_core_pwm_51_matrix_row_3_[10] = {  0.010636, -1.999946, -7.303251, -7.303251,  1.233941, -7.303251, -0.132362, -0.904656,  0.346918, -1.086645};
	float *jaspar_core_pwm_51_matrix[4] = { jaspar_core_pwm_51_matrix_row_0_, jaspar_core_pwm_51_matrix_row_1_, jaspar_core_pwm_51_matrix_row_2_, jaspar_core_pwm_51_matrix_row_3_};
	PWM jaspar_core_pwm_51_ = {
/* accession        */ "MA0038",
/* name             */ "Gfi",
/* label            */ " MA0038	9.47016680639999	Gfi	ZN-FINGER, C2H2	; acc \"Q07120\" ; medline \"8754800\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"9.4670\" ; type \"SELEX\" ",
/* motif            */ "NAAATCACWG",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_51_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -36.980400,
/* max_score        */ 9.281486,
/* threshold        */ 0.821,
/* info content     */ 9.440625,
/* base_counts      */ {210, 147, 60, 113},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 53
};

	float jaspar_core_pwm_52_matrix_row_0_[5] = {  1.271093,  1.271093,  1.271093, -6.107290, -1.492170};
	float jaspar_core_pwm_52_matrix_row_1_[5] = { -5.866128, -5.866128, -5.866128, -5.866128,  0.937377};
	float jaspar_core_pwm_52_matrix_row_2_[5] = { -5.866128, -5.866128, -5.866128,  1.512256, -1.251008};
	float jaspar_core_pwm_52_matrix_row_3_[5] = { -6.107290, -6.107290, -6.107290, -6.107290,  0.109316};
	float *jaspar_core_pwm_52_matrix[4] = { jaspar_core_pwm_52_matrix_row_0_, jaspar_core_pwm_52_matrix_row_1_, jaspar_core_pwm_52_matrix_row_2_, jaspar_core_pwm_52_matrix_row_3_};
	PWM jaspar_core_pwm_52_ = {
/* accession        */ "MA0064",
/* name             */ "PBF",
/* label            */ " MA0064	8.5086853429636	PBF	ZN-FINGER, DOF	; acc \"O24463\" ; medline \"10074718\" ; species \"Zea mays\" ; sysgroup \"plant\" ; total_ic \"8.5090\" ; type \"SELEX\" ",
/* motif            */ "AAAGC",
/* library_name     */ "jaspar_core",
/* length           */ 5,
/* matrixname       */ jaspar_core_pwm_52_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -25.921331,
/* max_score        */ 6.262913,
/* threshold        */ 0.925,
/* info content     */ 8.415419,
/* base_counts      */ {49, 9, 17, 5},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_53_matrix_row_0_[12] = {  0.248150,  0.007183,  0.499306, -6.996791, -6.996791, -1.002830,  1.246228,  1.192176, -6.996791,  0.248150, -1.002830, -6.996791};
	float jaspar_core_pwm_53_matrix_row_1_[12] = { -0.203121, -0.203121, -1.048519, -2.140509, -6.755629,  1.346352, -2.140509, -1.452324, -0.357034,  0.683342,  0.985470,  1.108022};
	float jaspar_core_pwm_53_matrix_row_2_[12] = {  0.335281,  0.489312,  0.558258, -6.755629,  1.487390, -6.755629, -6.755629, -2.140509, -6.755629, -0.539023,  0.047876, -0.357034};
	float jaspar_core_pwm_53_matrix_row_3_[12] = { -0.598196, -0.444283, -1.289681,  1.246228, -2.381671, -1.693486, -6.996791, -6.996791,  1.105189, -1.289681, -1.289681, -0.444283};
	float *jaspar_core_pwm_53_matrix[4] = { jaspar_core_pwm_53_matrix_row_0_, jaspar_core_pwm_53_matrix_row_1_, jaspar_core_pwm_53_matrix_row_2_, jaspar_core_pwm_53_matrix_row_3_};
	PWM jaspar_core_pwm_53_ = {
/* accession        */ "MA0019",
/* name             */ "Ddit3-Cebpa",
/* label            */ " MA0019	11.6518212262485	Ddit3-Cebpa	bZIP	; acc \"Q62857 , P05554\" ; comment \"dimer between Ddit3 and Cebpa\" ; medline \"8657121\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"11.6510\" ; type \"SELEX\" ",
/* motif            */ "NNRTGCAATMCC",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_53_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -53.647900,
/* max_score        */ 11.783247,
/* threshold        */ 0.783,
/* info content     */ 11.604292,
/* base_counts      */ {139, 126, 100, 103},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 39
};

	float jaspar_core_pwm_54_matrix_row_0_[12] = { -5.900226, -5.900226, -5.900226, -5.900226, -1.285106, -1.285106, -1.285106, -5.900226, -5.900226, -1.285106, -1.285106, -0.193116};
	float jaspar_core_pwm_54_matrix_row_1_[12] = {  0.557542, -5.659064, -1.043944, -5.659064, -1.043944, -5.659064, -5.659064, -5.659064,  0.048046,  1.144441,  1.144441,  0.557542};
	float jaspar_core_pwm_54_matrix_row_2_[12] = {  0.739531,  1.431846,  1.344910,  1.249691,  0.048046, -0.355759, -5.659064, -5.659064, -5.659064, -5.659064, -1.043944,  0.557542};
	float jaspar_core_pwm_54_matrix_row_3_[12] = { -0.596921, -1.285106, -1.285106, -0.193116,  0.785635,  1.008529,  1.190684,  1.270662,  1.008529, -0.193116, -0.596921, -5.900226};
	float *jaspar_core_pwm_54_matrix[4] = { jaspar_core_pwm_54_matrix_row_0_, jaspar_core_pwm_54_matrix_row_1_, jaspar_core_pwm_54_matrix_row_2_, jaspar_core_pwm_54_matrix_row_3_};
	PWM jaspar_core_pwm_54_ = {
/* accession        */ "MA0022",
/* name             */ "dl_1",
/* label            */ " MA0022	12.9045842260907	dl_1	REL	; acc \"AAF53611\" ; comment \"dl has a dual binding specificity and therefore two models: MA0022 and MA0023\" ; medline \"1582412\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"12.9060\" ; type \"SELEX\" ",
/* motif            */ "SGGGTTTTTCCS",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_54_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -60.848988,
/* max_score        */ 12.876442,
/* threshold        */ 0.782,
/* info content     */ 12.744682,
/* base_counts      */ {8, 33, 50, 65},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 13
};

	float jaspar_core_pwm_55_matrix_row_0_[6] = { -2.152361, -6.767481,  1.271999,  1.271999, -6.767481, -2.152361};
	float jaspar_core_pwm_55_matrix_row_1_[6] = {  1.023816, -6.526319, -6.526319, -6.526319, -6.526319, -1.911199};
	float jaspar_core_pwm_55_matrix_row_2_[6] = { -0.309713, -6.526319, -6.526319, -6.526319, -1.911199,  0.382436};
	float jaspar_core_pwm_55_matrix_row_3_[6] = { -0.368886,  1.271999, -6.767481, -6.767481,  1.239220,  0.782654};
	float *jaspar_core_pwm_55_matrix[4] = { jaspar_core_pwm_55_matrix_row_0_, jaspar_core_pwm_55_matrix_row_1_, jaspar_core_pwm_55_matrix_row_2_, jaspar_core_pwm_55_matrix_row_3_};
	PWM jaspar_core_pwm_55_ = {
/* accession        */ "MA0132",
/* name             */ "Pdx1",
/* label            */ " MA0132	9.03956138518612	Pdx1	HOMEO	; acc \"NP_032840\" ; medline \"14704343\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"9.040\" ; type \"SELEX\" ",
/* motif            */ "CTAATT",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_55_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -31.374645,
/* max_score        */ 6.861687,
/* threshold        */ 0.898,
/* info content     */ 8.988174,
/* base_counts      */ {64, 20, 16, 86},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 31
};

	float jaspar_core_pwm_56_matrix_row_0_[7] = {  0.219552, -1.100373,  1.224652,  1.094632, -0.541827,  0.455801, -1.387224};
	float jaspar_core_pwm_56_matrix_row_1_[7] = {  0.150802,  1.307631, -2.238052, -1.549868,  1.119638,  0.391769,  0.843495};
	float jaspar_core_pwm_56_matrix_row_2_[7] = {  0.055582, -1.549868, -2.238052, -0.859211, -0.454577, -0.300664,  0.460714};
	float jaspar_core_pwm_56_matrix_row_3_[7] = { -0.541827, -1.791030, -7.094335, -2.479214, -2.479214, -1.387224, -1.387224};
	float *jaspar_core_pwm_56_matrix[4] = { jaspar_core_pwm_56_matrix_row_0_, jaspar_core_pwm_56_matrix_row_1_, jaspar_core_pwm_56_matrix_row_2_, jaspar_core_pwm_56_matrix_row_3_};
	PWM jaspar_core_pwm_56_ = {
/* accession        */ "MA0133",
/* name             */ "BRCA1",
/* label            */ " MA0133	5.26063535577042	BRCA1	-	; acc \"-\" ; comment \"Multi-protein complex\" ; medline \"14502648\" ; species \"Homo sapiens\" ; sysgroup \"\" ; total_ic \"5.261\" ; type \"SELEX\" ",
/* motif            */ "NCAACMC",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_56_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -17.160067,
/* max_score        */ 6.265402,
/* threshold        */ 0.918,
/* info content     */ 5.247941,
/* base_counts      */ {125, 114, 45, 17},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 43
};

	float jaspar_core_pwm_57_matrix_row_0_[8] = { -1.714814, -6.329935,  1.220201,  1.271468,  1.166162, -1.714814,  1.271468,  0.222573};
	float jaspar_core_pwm_57_matrix_row_1_[8] = { -1.473652, -6.088773, -1.473652, -6.088773, -1.473652,  1.407325, -6.088773, -0.785468};
	float jaspar_core_pwm_57_matrix_row_2_[8] = {  1.350199, -6.088773, -6.088773, -6.088773, -1.473652, -6.088773, -6.088773, -0.381663};
	float jaspar_core_pwm_57_matrix_row_3_[8] = { -1.714814,  1.271468, -6.329935, -6.329935, -6.329935, -1.714814, -6.329935,  0.355926};
	float *jaspar_core_pwm_57_matrix[4] = { jaspar_core_pwm_57_matrix_row_0_, jaspar_core_pwm_57_matrix_row_1_, jaspar_core_pwm_57_matrix_row_2_, jaspar_core_pwm_57_matrix_row_3_};
	PWM jaspar_core_pwm_57_ = {
/* accession        */ "MA0031",
/* name             */ "FOXD1",
/* label            */ " MA0031	11.9264176788219	FOXD1	FORKHEAD	; acc \"Q16676\" ; medline \"7957066\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"11.9300\" ; type \"SELEX\" ",
/* motif            */ "GTAAACAW",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_57_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -40.238731,
/* max_score        */ 9.314216,
/* threshold        */ 0.837,
/* info content     */ 11.832067,
/* base_counts      */ {86, 23, 21, 30},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_58_matrix_row_0_[14] = { -0.113329,  0.522138,  0.397053,  1.243970,  0.824265, -1.613529, -6.916834,  1.020898, -6.916834, -6.916834, -6.916834, -6.916834,  1.272133,  0.397053};
	float jaspar_core_pwm_58_matrix_row_1_[14] = {  0.010189, -1.372367, -6.675672, -2.060551, -6.675672,  0.415238, -6.675672, -6.675672, -6.675672, -6.675672, -6.675672,  1.513295, -6.675672, -0.277077};
	float jaspar_core_pwm_58_matrix_row_2_[14] = {  0.010189, -0.123164, -0.968562, -6.675672, -6.675672,  0.495217, -6.675672,  0.010189,  1.513295,  1.513295, -6.675672, -6.675672, -6.675672,  0.233083};
	float jaspar_core_pwm_58_matrix_row_3_[14] = {  0.087140, -0.008079,  0.579263, -6.916834,  0.254055, -0.113329,  1.272133, -6.916834, -6.916834, -6.916834,  1.272133, -6.916834, -6.916834, -0.700228};
	float *jaspar_core_pwm_58_matrix[4] = { jaspar_core_pwm_58_matrix_row_0_, jaspar_core_pwm_58_matrix_row_1_, jaspar_core_pwm_58_matrix_row_2_, jaspar_core_pwm_58_matrix_row_3_};
	PWM jaspar_core_pwm_58_ = {
/* accession        */ "MA0072",
/* name             */ "RORA_2",
/* label            */ " MA0072	17.4248426117905	RORA_2	NUCLEAR RECEPTOR	; acc \"NP_599022\" ; comment \"isoform type \"b\" with different DNA-binding specificity. Alternative model exist for isoform \"a\" in MA0071\" ; medline \"7926749\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"17.4230\" ; type \"SELEX\" ",
/* motif            */ "NWWAANTAGGTCAN",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_58_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.485458,
/* max_score        */ 13.526227,
/* threshold        */ 0.773,
/* info content     */ 17.330471,
/* base_counts      */ {180, 65, 121, 138},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 36
};

	float jaspar_core_pwm_59_matrix_row_0_[6] = {  0.230827, -6.167768, -6.167768,  1.271204, -6.167768, -0.460658};
	float jaspar_core_pwm_59_matrix_row_1_[6] = { -1.311486,  1.451778,  1.512366, -5.926606, -5.926606,  0.759255};
	float jaspar_core_pwm_59_matrix_row_2_[6] = {  0.625902, -5.926606, -5.926606, -5.926606, -5.926606, -0.219496};
	float jaspar_core_pwm_59_matrix_row_3_[6] = { -0.460658, -1.552648, -6.167768, -6.167768,  1.271204, -0.460658};
	float *jaspar_core_pwm_59_matrix[4] = { jaspar_core_pwm_59_matrix_row_0_, jaspar_core_pwm_59_matrix_row_1_, jaspar_core_pwm_59_matrix_row_2_, jaspar_core_pwm_59_matrix_row_3_};
	PWM jaspar_core_pwm_59_ = {
/* accession        */ "MA0095",
/* name             */ "YY1",
/* label            */ " MA0095	8.10118850374216	YY1	ZN-FINGER, C2H2	; acc \"P25490\" ; medline \"7816599\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"8.1020\" ; type \"COMPILED\" ",
/* motif            */ "RCCATN",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_59_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -26.443216,
/* max_score        */ 6.891707,
/* threshold        */ 0.910,
/* info content     */ 8.018751,
/* base_counts      */ {26, 42, 10, 24},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_60_matrix_row_0_[10] = { -1.026630,  0.840954,  1.271468, -1.026630, -6.329935, -6.329935, -6.329935, -6.329935, -6.329935, -1.026630};
	float jaspar_core_pwm_60_matrix_row_1_[10] = {  0.309822, -1.473652, -6.088773,  1.350199, -6.088773, -1.473652,  1.461363,  1.407325, -6.088773,  1.082116};
	float jaspar_core_pwm_60_matrix_row_2_[10] = {  0.127833, -6.088773, -6.088773, -1.473652,  1.461363, -6.088773, -6.088773, -0.785468,  1.461363, -1.473652};
	float jaspar_core_pwm_60_matrix_row_3_[10] = {  0.222573,  0.068660, -6.329935, -6.329935, -1.714814,  1.220201, -1.714814, -6.329935, -1.714814, -0.335973};
	float *jaspar_core_pwm_60_matrix[4] = { jaspar_core_pwm_60_matrix_row_0_, jaspar_core_pwm_60_matrix_row_1_, jaspar_core_pwm_60_matrix_row_2_, jaspar_core_pwm_60_matrix_row_3_};
	PWM jaspar_core_pwm_60_ = {
/* accession        */ "MA0131",
/* name             */ "MIZF",
/* label            */ " MA0131	13.1966529939183	MIZF	ZN-FINGER, C2H2 	; acc \"Q9BQA5\" ; medline \"14752047\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"13.197\" ; type \"SELEX\" ",
/* motif            */ "NAACGTCCGC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_60_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -52.898594,
/* max_score        */ 11.866172,
/* threshold        */ 0.804,
/* info content     */ 13.090030,
/* base_counts      */ {39, 75, 47, 39},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_61_matrix_row_0_[12] = {  0.053586, -0.313798,  1.151685,  0.484100, -7.117303,  1.272284, -7.117303, -0.026393, -7.117303, -7.117303, -7.117303, -1.123341};
	float jaspar_core_pwm_61_matrix_row_1_[12] = {  0.368801,  0.032614, -6.876141,  0.907500,  1.490462, -6.876141, -2.261020,  1.195078, -6.876141, -6.876141, -1.169031, -0.882179};
	float jaspar_core_pwm_61_matrix_row_2_[12] = { -0.190280,  0.725262, -1.169031, -6.876141, -2.261020, -6.876141,  0.127833, -6.876141, -6.876141,  1.490462,  0.725262, -1.572836};
	float jaspar_core_pwm_61_matrix_row_3_[12] = { -0.313798, -0.900697, -1.813998, -7.117303, -7.117303, -7.117303,  0.953916, -7.117303,  1.272284, -2.502182,  0.532866,  1.014522};
	float *jaspar_core_pwm_61_matrix[4] = { jaspar_core_pwm_61_matrix_row_0_, jaspar_core_pwm_61_matrix_row_1_, jaspar_core_pwm_61_matrix_row_2_, jaspar_core_pwm_61_matrix_row_3_};
	PWM jaspar_core_pwm_61_ = {
/* accession        */ "MA0091",
/* name             */ "TAL1-TCF3",
/* label            */ " MA0091	14.0701377487026	TAL1-TCF3	bHLH	; acc \"P17542 , P15923\" ; comment \"Heterodimer between TAL1 and TCF3\" ; medline \"8289805\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.0710\" ; type \"SELEX\" ",
/* motif            */ "NNACCATCTGKT",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_61_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.601891,
/* max_score        */ 12.567516,
/* threshold        */ 0.781,
/* info content     */ 14.011782,
/* base_counts      */ {141, 131, 108, 148},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 44
};

	float jaspar_core_pwm_62_matrix_row_0_[10] = { -1.492170,  0.291305,  0.696215, -0.113329,  1.063598,  1.271093,  1.271093,  1.137651,  1.206597,  0.696215};
	float jaspar_core_pwm_62_matrix_row_1_[10] = {  0.350478,  0.819733, -0.159018, -0.159018, -1.251008, -5.866128, -5.866128, -5.866128, -1.251008, -0.562823};
	float jaspar_core_pwm_62_matrix_row_2_[10] = {  0.819733, -0.562823,  0.127833, -1.251008, -5.866128, -5.866128, -5.866128, -0.562823, -5.866128, -0.562823};
	float jaspar_core_pwm_62_matrix_row_3_[10] = { -0.803985, -6.107290, -6.107290,  0.578571, -0.803985, -6.107290, -6.107290, -6.107290, -6.107290, -0.400180};
	float *jaspar_core_pwm_62_matrix[4] = { jaspar_core_pwm_62_matrix_row_0_, jaspar_core_pwm_62_matrix_row_1_, jaspar_core_pwm_62_matrix_row_2_, jaspar_core_pwm_62_matrix_row_3_};
	PWM jaspar_core_pwm_62_ = {
/* accession        */ "MA0049",
/* name             */ "hb",
/* label            */ " MA0049	10.403423261874	hb	ZN-FINGER, C2H2	; acc \"P05084\" ; medline \"2507923\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"10.4030\" ; type \"COMPILED\" ",
/* motif            */ "SMAWAAAAAA",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_62_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -45.815876,
/* max_score        */ 9.560499,
/* threshold        */ 0.817,
/* info content     */ 10.295989,
/* base_counts      */ {103, 23, 19, 15},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_63_matrix_row_0_[10] = {  0.113570,  0.336588,  0.598837, -7.264814,  0.559632, -7.264814,  0.518826, -7.264814,  0.049073, -0.173904};
	float jaspar_core_pwm_63_matrix_row_1_[10] = { -0.114897,  0.067258,  1.078329,  1.659225, -7.023652, -7.023652, -0.807046, -7.023652,  0.221289,  0.067258};
	float jaspar_core_pwm_63_matrix_row_2_[10] = {  0.067258, -0.807046, -7.023652, -7.023652, -7.023652, -7.023652,  0.067258,  0.673015,  0.221289, -0.114897};
	float jaspar_core_pwm_63_matrix_row_3_[10] = { -0.093926,  0.113570, -7.264814, -7.264814,  0.867011,  1.418063,  0.231283,  0.951544, -0.578953,  0.049073};
	float *jaspar_core_pwm_63_matrix[4] = { jaspar_core_pwm_63_matrix_row_0_, jaspar_core_pwm_63_matrix_row_1_, jaspar_core_pwm_63_matrix_row_2_, jaspar_core_pwm_63_matrix_row_3_};
	PWM jaspar_core_pwm_63_ = {
/* accession        */ "MA0109",
/* name             */ "RUSH1-alfa",
/* label            */ " MA0109	7.46926530914378	RUSH1-alfa	ZN-FINGER, GATA	; acc \"AAM33847\" ; comment \"updated matrix sincle last release\" ; medline \"12198246\" ; species \"Oryctolagus cuniculus\" ; sysgroup \"vertebrate\" ; total_ic \"7.46926530914378\" ; type \"SELEX\" ",
/* motif            */ "NNCCTTNTNN",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_63_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -38.564754,
/* max_score        */ 7.231703,
/* threshold        */ 0.827,
/* info content     */ 8.158154,
/* base_counts      */ {138, 145, 75, 200},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 51
};

	float jaspar_core_pwm_64_matrix_row_0_[13] = {  0.400238, -2.234189, -0.446584, -1.320888, -1.830384,  1.145383,  1.038157,  1.093206, -1.543532, -1.138899, -1.830384, -1.543532,  0.805584};
	float jaspar_core_pwm_64_matrix_row_1_[13] = { -0.743824, -1.993027, -1.302370,  0.444767,  1.240860, -2.681211, -1.993027, -2.681211, -1.302370, -1.993027,  0.400335,  1.200863, -0.743824};
	float jaspar_core_pwm_64_matrix_row_2_[13] = {  0.605045,  1.334368,  0.864472,  0.305070, -1.302370, -1.589222, -0.387577, -0.610471,  1.369454,  0.805649, -0.292358, -1.079726, -0.387577};
	float jaspar_core_pwm_64_matrix_row_3_[13] = { -1.320888, -0.984986, -0.159110,  0.012641, -0.733989, -1.543532, -2.234189, -2.234189, -2.922374,  0.326157,  0.501986, -0.733989, -0.851633};
	float *jaspar_core_pwm_64_matrix[4] = { jaspar_core_pwm_64_matrix_row_0_, jaspar_core_pwm_64_matrix_row_1_, jaspar_core_pwm_64_matrix_row_2_, jaspar_core_pwm_64_matrix_row_3_};
	PWM jaspar_core_pwm_64_ = {
/* accession        */ "MA0114",
/* name             */ "HNF4A",
/* label            */ " MA0114	9.61741421120951	HNF4A	NUCLEAR RECEPTOR	; acc \"P41235\" ; medline \"12385991\" ; species \"-\" ; sysgroup \"vertebrate\" ; total_ic \"9.6174\" ; type \"COMPILED\" ",
/* motif            */ "RGGNCAAAGKYCA",
/* library_name     */ "jaspar_core",
/* length           */ 13,
/* matrixname       */ jaspar_core_pwm_64_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -24.746281,
/* max_score        */ 12.449794,
/* threshold        */ 0.767,
/* info content     */ 9.604528,
/* base_counts      */ {277, 175, 280, 139},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 67
};

	float jaspar_core_pwm_65_matrix_row_0_[15] = {  0.068329, -5.638782, -5.638782, -5.638782, -5.638782,  1.164724,  0.355180, -0.335477, -0.335477,  0.577825, -5.638782, -5.638782, -1.023661, -5.638782,  0.913726};
	float jaspar_core_pwm_65_matrix_row_1_[15] = { -5.397620, -5.397620, -5.397620, -5.397620,  1.405886, -5.397620, -0.094315,  0.596342, -5.397620, -5.397620, -5.397620, -5.397620, -5.397620,  1.405886, -0.782499};
	float jaspar_core_pwm_65_matrix_row_2_[15] = {  1.154888,  1.511135,  1.405886, -5.397620, -5.397620, -0.782499, -5.397620, -0.094315,  1.288241,  0.818987,  1.511135, -5.397620, -5.397620, -5.397620, -0.094315};
	float jaspar_core_pwm_65_matrix_row_3_[15] = { -5.638782, -5.638782, -1.023661,  1.269973, -1.023661, -5.638782,  0.355180, -0.335477, -5.638782, -5.638782, -5.638782,  1.269973,  1.164724, -1.023661, -5.638782};
	float *jaspar_core_pwm_65_matrix[4] = { jaspar_core_pwm_65_matrix_row_0_, jaspar_core_pwm_65_matrix_row_1_, jaspar_core_pwm_65_matrix_row_2_, jaspar_core_pwm_65_matrix_row_3_};
	PWM jaspar_core_pwm_65_ = {
/* accession        */ "MA0074",
/* name             */ "RXRA-VDR",
/* label            */ " MA0074	20.4511671987138	RXRA-VDR	NUCLEAR RECEPTOR	; acc \"P19793  , P11473\" ; comment \"heterodimer between RXRA and VDR\" ; medline \"8674817\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"20.4520\" ; type \"SELEX\" ",
/* motif            */ "GGGTCAWNGRGTTCA",
/* library_name     */ "jaspar_core",
/* length           */ 15,
/* matrixname       */ jaspar_core_pwm_65_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.796097,
/* max_score        */ 17.236687,
/* threshold        */ 0.784,
/* info content     */ 20.104731,
/* base_counts      */ {33, 25, 54, 38},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 10
};

	float jaspar_core_pwm_66_matrix_row_0_[9] = {  1.048449, -6.329935, -6.329935, -6.329935, -6.329935,  1.271468,  1.048449, -0.335973, -1.714814};
	float jaspar_core_pwm_66_matrix_row_1_[9] = { -1.473652,  1.512630,  1.512630, -6.088773, -6.088773, -6.088773, -6.088773, -1.473652,  0.309822};
	float jaspar_core_pwm_66_matrix_row_2_[9] = { -0.785468, -6.088773, -6.088773,  1.512630,  1.512630, -6.088773, -6.088773,  1.225114, -6.088773};
	float jaspar_core_pwm_66_matrix_row_3_[9] = { -1.714814, -6.329935, -6.329935, -6.329935, -6.329935, -6.329935, -0.335973, -6.329935,  0.840954};
	float *jaspar_core_pwm_66_matrix[4] = { jaspar_core_pwm_66_matrix_row_0_, jaspar_core_pwm_66_matrix_row_1_, jaspar_core_pwm_66_matrix_row_2_, jaspar_core_pwm_66_matrix_row_3_};
	PWM jaspar_core_pwm_66_ = {
/* accession        */ "MA0076",
/* name             */ "ELK4",
/* label            */ " MA0076	14.123230134165	ELK4	ETS	; acc \"P28324\" ; medline \"8524663\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.1230\" ; type \"SELEX\" ",
/* motif            */ "ACCGGAAGT",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_66_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -51.871967,
/* max_score        */ 11.484951,
/* threshold        */ 0.809,
/* info content     */ 13.999010,
/* base_counts      */ {57, 48, 57, 18},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_67_matrix_row_0_[8] = { -6.970843,  1.218124,  1.272177, -2.355722, -1.667538,  0.343044, -0.976881, -1.667538};
	float jaspar_core_pwm_67_matrix_row_1_[8] = { -2.114560, -6.729681, -6.729681, -2.114560, -6.729681, -6.729681,  0.361229,  0.441208};
	float jaspar_core_pwm_67_matrix_row_2_[8] = { -6.729681, -6.729681, -6.729681, -1.426376, -0.735719,  0.966986,  0.766417, -0.331086};
	float jaspar_core_pwm_67_matrix_row_3_[8] = {  1.245515, -1.667538, -6.970843,  1.160982,  1.100376, -2.355722, -0.976881,  0.468129};
	float *jaspar_core_pwm_67_matrix[4] = { jaspar_core_pwm_67_matrix_row_0_, jaspar_core_pwm_67_matrix_row_1_, jaspar_core_pwm_67_matrix_row_2_, jaspar_core_pwm_67_matrix_row_3_};
	PWM jaspar_core_pwm_67_ = {
/* accession        */ "MA0125",
/* name             */ "Nobox",
/* label            */ " MA0125	9.57325017965508	Nobox	HOMEO	; acc \"Q8VIH1\" ; medline \"16997917\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"9.573\" ; type \"SELEX\" ",
/* motif            */ "TAATTGSY",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_67_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -39.130871,
/* max_score        */ 8.198706,
/* threshold        */ 0.856,
/* info content     */ 9.533091,
/* base_counts      */ {98, 27, 52, 127},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 38
};

	float jaspar_core_pwm_68_matrix_row_0_[20] = {  0.173523,  0.173523,  0.173523, -1.205318, -0.517134,  0.732069, -0.517134, -5.820439, -1.205318,  0.578156,  0.578156, -5.820439, -0.517134, -0.113329, -1.205318,  0.578156,  0.396167, -0.517134, -0.517134,  0.396167};
	float jaspar_core_pwm_68_matrix_row_1_[20] = { -0.964156, -5.579277,  0.127833, -0.275972,  0.973231, -0.275972,  0.637329,  0.127833, -0.275972, -0.964156, -5.579277, -5.579277,  1.106584, -5.579277, -5.579277, -0.964156, -0.964156,  1.329478,  0.637329, -5.579277};
	float jaspar_core_pwm_68_matrix_row_2_[20] = {  0.414685,  1.106584,  0.127833,  0.637329, -0.964156, -0.964156,  0.127833, -0.275972,  1.106584,  0.127833, -0.275972,  1.511633, -0.964156,  1.224228,  0.414685,  0.637329,  0.637329, -5.579277,  0.637329,  0.819318};
	float jaspar_core_pwm_68_matrix_row_3_[20] = { -0.113329, -5.820439, -0.517134,  0.173523, -0.517134, -0.517134, -0.517134,  0.732069, -1.205318, -0.517134,  0.173523, -5.820439, -1.205318, -5.820439,  0.732069, -5.820439, -1.205318, -5.820439, -5.820439, -1.205318};
	float *jaspar_core_pwm_68_matrix[4] = { jaspar_core_pwm_68_matrix_row_0_, jaspar_core_pwm_68_matrix_row_1_, jaspar_core_pwm_68_matrix_row_2_, jaspar_core_pwm_68_matrix_row_3_};
	PWM jaspar_core_pwm_68_ = {
/* accession        */ "MA0014",
/* name             */ "Pax5",
/* label            */ " MA0014	12.4319608285825	Pax5	PAIRED	; acc \"Q02650\" ; medline \"8406007\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"12.4280\" ; type \"COMPILED\" ",
/* motif            */ "NGNKCANTGRWGCGTRRCSR",
/* library_name     */ "jaspar_core",
/* length           */ 20,
/* matrixname       */ jaspar_core_pwm_68_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -67.193069,
/* max_score        */ 16.305012,
/* threshold        */ 0.682,
/* info content     */ 12.267103,
/* base_counts      */ {63, 51, 87, 39},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12
};

	float jaspar_core_pwm_69_matrix_row_0_[18] = { -0.918743, -0.918743,  1.018644, -0.230559, -5.533864, -5.533864, -5.533864,  0.864731, -0.918743, -0.230559,  0.173247, -0.918743, -0.918743,  0.682742, -5.533864, -5.533864, -0.230559,  0.173247};
	float jaspar_core_pwm_69_matrix_row_1_[18] = {  0.923905,  0.923905, -0.677581, -5.292702, -5.292702, -5.292702,  1.259806, -5.292702,  1.259806,  0.923905,  0.010603, -0.677581, -5.292702, -0.677581,  1.393159,  1.510804,  0.701260,  0.701260};
	float jaspar_core_pwm_69_matrix_row_2_[18] = { -0.677581, -0.677581, -0.677581,  1.259806,  1.510804, -5.292702,  0.010603,  0.010603, -0.677581, -0.677581,  0.701260, -0.677581,  1.393159,  0.414409, -5.292702, -5.292702, -5.292702, -0.677581};
	float jaspar_core_pwm_69_matrix_row_3_[18] = { -0.230559, -0.230559, -5.533864, -5.533864, -5.533864,  1.269642, -5.533864, -0.918743, -5.533864, -0.918743, -5.533864,  0.864731, -5.533864, -5.533864, -0.918743, -5.533864,  0.173247, -0.918743};
	float *jaspar_core_pwm_69_matrix[4] = { jaspar_core_pwm_69_matrix_row_0_, jaspar_core_pwm_69_matrix_row_1_, jaspar_core_pwm_69_matrix_row_2_, jaspar_core_pwm_69_matrix_row_3_};
	PWM jaspar_core_pwm_69_ = {
/* accession        */ "MA0112",
/* name             */ "ESR1",
/* label            */ " MA0112	17.6826360597729	ESR1	NUCLEAR RECEPTOR	; acc \"P03372\" ; medline \"15563547\" ; species \"-\" ; sysgroup \"vertebrate\" ; total_ic \"17.6826\" ; type \"COMPILED\" ",
/* motif            */ "CCAGGTCACCRTGACCYM",
/* library_name     */ "jaspar_core",
/* length           */ 18,
/* matrixname       */ jaspar_core_pwm_69_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.051620,
/* max_score        */ 19.163328,
/* threshold        */ 0.762,
/* info content     */ 17.373508,
/* base_counts      */ {35, 59, 42, 26},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 9
};

	float jaspar_core_pwm_70_matrix_row_0_[10] = { -1.492170, -0.803985,  1.206597, -6.107290, -6.107290, -6.107290, -6.107290, -0.400180,  0.801465,  0.578571};
	float jaspar_core_pwm_70_matrix_row_1_[10] = {  0.127833, -5.866128, -1.251008, -5.866128, -5.866128, -0.562823, -5.866128, -1.251008, -5.866128, -0.562823};
	float jaspar_core_pwm_70_matrix_row_2_[10] = {  0.686380,  0.686380, -5.866128,  1.137846,  1.447759,  1.378813,  1.378813,  0.819733,  0.127833,  0.127833};
	float jaspar_core_pwm_70_matrix_row_3_[10] = { -0.113329,  0.445218, -6.107290,  0.109316, -1.492170, -6.107290, -0.803985, -0.113329, -0.803985, -0.803985};
	float *jaspar_core_pwm_70_matrix[4] = { jaspar_core_pwm_70_matrix_row_0_, jaspar_core_pwm_70_matrix_row_1_, jaspar_core_pwm_70_matrix_row_2_, jaspar_core_pwm_70_matrix_row_3_};
	PWM jaspar_core_pwm_70_ = {
/* accession        */ "MA0057",
/* name             */ "MZF1_5-13",
/* label            */ " MA0057	9.40029161122264	MZF1_5-13	ZN-FINGER, C2H2	; acc \"P28698\" ; comment \"\" ; medline \"8114711\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"9.3990\" ; type \"SELEX\" ",
/* motif            */ "NKAGGGGKAR",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_70_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -45.815868,
/* max_score        */ 10.122355,
/* threshold        */ 0.822,
/* info content     */ 9.301724,
/* base_counts      */ {39, 10, 84, 27},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_71_matrix_row_0_[14] = {  0.615420,  0.971881, -6.629522,  1.271856, -2.014401,  1.271856,  1.234130, -6.629522,  1.271856, -6.629522,  1.154119,  1.111577, -0.230927,  0.684365};
	float jaspar_core_pwm_71_matrix_row_1_[14] = { -1.085055, -1.773239, -1.773239, -6.388360,  0.520395, -6.388360, -6.388360, -6.388360, -6.388360, -0.681249, -1.773239, -6.388360,  0.164148,  0.010235};
	float jaspar_core_pwm_71_matrix_row_2_[14] = {  0.010235, -1.085055,  1.436086, -6.388360, -6.388360, -6.388360, -1.773239,  1.513018, -6.388360, -6.388360, -6.388360, -0.394398,  0.164148, -0.681249};
	float jaspar_core_pwm_71_matrix_row_3_[14] = { -0.412916, -0.635560, -2.014401, -6.629522,  0.748862, -6.629522, -6.629522, -6.629522, -6.629522,  1.154119, -1.326217, -6.629522, -0.077014, -0.922412};
	float *jaspar_core_pwm_71_matrix[4] = { jaspar_core_pwm_71_matrix_row_0_, jaspar_core_pwm_71_matrix_row_1_, jaspar_core_pwm_71_matrix_row_2_, jaspar_core_pwm_71_matrix_row_3_};
	PWM jaspar_core_pwm_71_ = {
/* accession        */ "MA0029",
/* name             */ "Evi1",
/* label            */ " MA0029	17.9085403297222	Evi1	ZN-FINGER, C2H2	; acc \"AAI39763\" ; medline \"8321231\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"17.9080\" ; type \"SELEX\" ",
/* motif            */ "AAGATAAGATAANA",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_71_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -69.824516,
/* max_score        */ 14.603290,
/* threshold        */ 0.775,
/* info content     */ 17.796101,
/* base_counts      */ {210, 31, 75, 62},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 27
};

	float jaspar_core_pwm_72_matrix_row_0_[8] = { -5.638782, -5.638782, -5.638782, -5.638782, -5.638782, -5.638782, -5.638782, -5.638782};
	float jaspar_core_pwm_72_matrix_row_1_[8] = { -5.397620, -5.397620, -5.397620,  0.596342, -0.094315,  1.511135, -5.397620,  1.405886};
	float jaspar_core_pwm_72_matrix_row_2_[8] = { -5.397620, -5.397620, -5.397620,  1.000975,  1.288241, -5.397620,  1.511135, -0.782499};
	float jaspar_core_pwm_72_matrix_row_3_[8] = {  1.269973,  1.269973,  1.269973, -5.638782, -5.638782, -5.638782, -5.638782, -5.638782};
	float *jaspar_core_pwm_72_matrix[4] = { jaspar_core_pwm_72_matrix_row_0_, jaspar_core_pwm_72_matrix_row_1_, jaspar_core_pwm_72_matrix_row_2_, jaspar_core_pwm_72_matrix_row_3_};
	PWM jaspar_core_pwm_72_ = {
/* accession        */ "MA0024",
/* name             */ "E2F1",
/* label            */ " MA0024	13.8381257170687	E2F1	E2F_TDP	; acc \"NP_005216\" ; medline \"1411535\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"13.8380\" ; type \"COMPILED\" ",
/* motif            */ "TTTGGCGC",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_72_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -45.110256,
/* max_score        */ 10.527292,
/* threshold        */ 0.822,
/* info content     */ 13.599899,
/* base_counts      */ {0, 25, 25, 30},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 10
};

	float jaspar_core_pwm_73_matrix_row_0_[8] = {  0.083071,  0.439318, -0.070841,  0.701452, -0.475475,  1.180732, -6.469437,  1.227231};
	float jaspar_core_pwm_73_matrix_row_1_[8] = { -1.613154, -0.234313, -0.521164, -0.234313,  0.680480, -6.228274, -0.924969, -1.613154};
	float jaspar_core_pwm_73_matrix_row_2_[8] = { -0.234313, -0.924969,  0.170321, -0.234313, -0.924969, -0.924969, -6.228274, -6.228274};
	float jaspar_core_pwm_73_matrix_row_3_[8] = {  0.534538,  0.083071,  0.216425, -1.166131,  0.083071, -6.469437,  1.180732, -6.469437};
	float *jaspar_core_pwm_73_matrix[4] = { jaspar_core_pwm_73_matrix_row_0_, jaspar_core_pwm_73_matrix_row_1_, jaspar_core_pwm_73_matrix_row_2_, jaspar_core_pwm_73_matrix_row_3_};
	PWM jaspar_core_pwm_73_ = {
/* accession        */ "MA0033",
/* name             */ "FOXL1",
/* label            */ " MA0033	6.06853671151551	FOXL1	FORKHEAD	; acc \"Q12952\" ; medline \"7957066\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"6.0680\" ; type \"SELEX\" ",
/* motif            */ "WNNANATA",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_73_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -24.558697,
/* max_score        */ 6.160909,
/* threshold        */ 0.886,
/* info content     */ 6.028038,
/* base_counts      */ {83, 25, 20, 56},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 23
};

	float jaspar_core_pwm_74_matrix_row_0_[8] = { -0.062140, -6.278747, -6.278747,  1.217351, -1.663626, -1.663626,  0.119848,  1.217351};
	float jaspar_core_pwm_74_matrix_row_1_[8] = { -0.734280, -6.037584, -1.422464, -1.422464,  0.361010, -1.422464,  1.133304, -6.037584};
	float jaspar_core_pwm_74_matrix_row_2_[8] = {  1.053325, -6.037584,  1.458513, -6.037584,  0.765921, -6.037584, -6.037584, -6.037584};
	float jaspar_core_pwm_74_matrix_row_3_[8] = { -6.278747,  1.271389, -6.278747, -6.278747, -0.571636,  1.160225, -6.278747, -1.663626};
	float *jaspar_core_pwm_74_matrix[4] = { jaspar_core_pwm_74_matrix_row_0_, jaspar_core_pwm_74_matrix_row_1_, jaspar_core_pwm_74_matrix_row_2_, jaspar_core_pwm_74_matrix_row_3_};
	PWM jaspar_core_pwm_74_ = {
/* accession        */ "MA0099",
/* name             */ "Fos",
/* label            */ " MA0099	10.6698488649404	Fos	bZIP	; acc \"P01101\" ; medline \"2243767\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"10.6720\" ; type \"SELEX\" ",
/* motif            */ "GTGASTCA",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_74_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -45.132530,
/* max_score        */ 9.277378,
/* threshold        */ 0.847,
/* info content     */ 10.576176,
/* base_counts      */ {49, 24, 39, 40},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 19
};

	float jaspar_core_pwm_75_matrix_row_0_[18] = {  0.173247,  0.460098,  0.173247,  1.018644,  0.864731, -0.918743,  1.151997, -0.230559,  0.173247,  0.682742, -0.918743, -5.533864, -0.918743, -0.230559, -5.533864, -0.918743,  0.460098,  0.173247};
	float jaspar_core_pwm_75_matrix_row_1_[18] = {  0.010603, -5.292702, -5.292702, -5.292702, -0.677581,  1.105893, -5.292702, -5.292702,  0.010603,  0.414409,  0.414409, -5.292702, -5.292702,  0.701260,  1.393159,  0.010603, -5.292702, -0.677581};
	float jaspar_core_pwm_75_matrix_row_2_[18] = {  0.701260,  0.923905,  1.105893, -0.677581, -0.677581,  0.010603, -0.677581,  0.010603, -5.292702, -5.292702, -5.292702,  1.393159, -5.292702, -5.292702, -0.677581, -0.677581,  0.701260,  0.010603};
	float jaspar_core_pwm_75_matrix_row_3_[18] = { -5.533864, -5.533864, -5.533864, -0.918743, -0.918743, -5.533864, -5.533864,  0.682742,  0.460098, -0.918743,  0.682742, -0.918743,  1.151997,  0.173247, -5.533864,  0.682742, -0.918743,  0.173247};
	float *jaspar_core_pwm_75_matrix[4] = { jaspar_core_pwm_75_matrix_row_0_, jaspar_core_pwm_75_matrix_row_1_, jaspar_core_pwm_75_matrix_row_2_, jaspar_core_pwm_75_matrix_row_3_};
	PWM jaspar_core_pwm_75_ = {
/* accession        */ "MA0113",
/* name             */ "NR3C1",
/* label            */ " MA0113	14.7489179581357	NR3C1	NUCLEAR RECEPTOR	; acc \"P04150\" ; medline \"15563547\" ; species \"-\" ; sysgroup \"vertebrate\" ; total_ic \"14.7489\" ; type \"COMPILED\" ",
/* motif            */ "RGGAACATWATGTYCTRN",
/* library_name     */ "jaspar_core",
/* length           */ 18,
/* matrixname       */ jaspar_core_pwm_75_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.593727,
/* max_score        */ 15.577472,
/* threshold        */ 0.731,
/* info content     */ 14.476534,
/* base_counts      */ {54, 32, 38, 38},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 9
};

	float jaspar_core_pwm_76_matrix_row_0_[14] = { -0.162024, -1.763509, -1.763509, -1.763509,  1.222772,  0.999754, -1.763509,  0.307231,  0.866312, -1.075325, -6.378630,  0.792259,  0.307231, -0.162024};
	float jaspar_core_pwm_76_matrix_row_1_[14] = { -6.137468, -6.137468, -6.137468, -6.137468, -6.137468, -0.834163, -6.137468, -0.834163, -6.137468, -6.137468, -0.143506, -1.522347,  0.548393,  1.033421};
	float jaspar_core_pwm_76_matrix_row_2_[14] = {  1.107474,  1.463934, -6.137468, -6.137468, -6.137468, -1.522347, -6.137468, -0.143506, -1.522347, -6.137468, -6.137468, -0.430358, -0.430358, -6.137468};
	float jaspar_core_pwm_76_matrix_row_3_[14] = { -1.075325, -6.378630,  1.222772,  1.222772, -1.763509, -1.075325,  1.222772,  0.173878,  0.019965,  1.171505,  1.060342, -0.384668, -1.075325, -0.671520};
	float *jaspar_core_pwm_76_matrix[4] = { jaspar_core_pwm_76_matrix_row_0_, jaspar_core_pwm_76_matrix_row_1_, jaspar_core_pwm_76_matrix_row_2_, jaspar_core_pwm_76_matrix_row_3_};
	PWM jaspar_core_pwm_76_ = {
/* accession        */ "MA0046",
/* name             */ "HNF1A",
/* label            */ " MA0046	15.5481691262781	HNF1A	HOMEO	; acc \"ABR09270\" ; medline \"9047360\" ; species \"Vertebrates\" ; sysgroup \"vertebrate\" ; total_ic \"15.5480\" ; type \"COMPILED\" ",
/* motif            */ "GGTTAATNATTAMC",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_76_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.811188,
/* max_score        */ 14.241713,
/* threshold        */ 0.767,
/* info content     */ 15.429892,
/* base_counts      */ {95, 30, 46, 123},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_77_matrix_row_0_[14] = {  0.269240, -6.734735,  1.048906,  0.643649,  1.006365,  0.704237,  1.048906, -0.048873,  0.510207, -2.119614,  0.510207,  1.089712,  0.915434, -0.182226};
	float jaspar_core_pwm_77_matrix_row_1_[14] = {  0.751369,  1.203095, -6.493572, -1.190267, -6.493572, -6.493572, -1.878452, -6.493572, -1.878452, -1.878452, -0.276966, -0.276966, -1.190267, -0.276966};
	float jaspar_core_pwm_77_matrix_row_2_[14] = { -1.878452, -6.493572, -0.499611, -1.878452, -6.493572, -6.493572, -6.493572, -1.878452,  0.820315,  1.444160, -1.878452, -6.493572, -0.786462,  0.192289};
	float jaspar_core_pwm_77_matrix_row_3_[14] = { -0.740773, -0.048873, -1.431430,  0.269240, -0.182226,  0.436154, -0.518128,  0.915434, -6.734735, -6.734735,  0.174020, -6.734735, -0.740773,  0.174020};
	float *jaspar_core_pwm_77_matrix[4] = { jaspar_core_pwm_77_matrix_row_0_, jaspar_core_pwm_77_matrix_row_1_, jaspar_core_pwm_77_matrix_row_2_, jaspar_core_pwm_77_matrix_row_3_};
	PWM jaspar_core_pwm_77_ = {
/* accession        */ "MA0082",
/* name             */ "SQUA",
/* label            */ " MA0082	12.3886856970394	SQUA	MADS	; acc \"CAA45228\" ; medline \"9826749\" ; species \"Antirrhinum majus\" ; sysgroup \"plant\" ; total_ic \"12.3870\" ; type \"SELEX\" ",
/* motif            */ "MCAAAAATRGWAAN",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_77_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.509392,
/* max_score        */ 12.294078,
/* threshold        */ 0.758,
/* info content     */ 12.320154,
/* base_counts      */ {205, 58, 62, 95},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 30
};

	float jaspar_core_pwm_78_matrix_row_0_[11] = { -1.854316, -6.469437,  1.227231, -6.469437, -1.166131, -6.469437,  1.271663,  1.227231, -6.469437,  0.083071, -0.252830};
	float jaspar_core_pwm_78_matrix_row_1_[11] = { -6.228274, -6.228274, -6.228274,  0.457587, -6.228274, -6.228274, -6.228274, -6.228274,  0.775700, -0.011668, -0.011668};
	float jaspar_core_pwm_78_matrix_row_2_[11] = { -6.228274, -0.924969, -6.228274, -6.228274,  1.421894, -6.228274, -6.228274, -1.613154, -0.234313,  0.324234, -0.521164};
	float jaspar_core_pwm_78_matrix_row_3_[11] = {  1.227231,  1.180732, -1.854316,  0.844450, -6.469437,  1.271663, -6.469437, -6.469437,  0.216425, -0.475475,  0.439318};
	float *jaspar_core_pwm_78_matrix[4] = { jaspar_core_pwm_78_matrix_row_0_, jaspar_core_pwm_78_matrix_row_1_, jaspar_core_pwm_78_matrix_row_2_, jaspar_core_pwm_78_matrix_row_3_};
	PWM jaspar_core_pwm_78_ = {
/* accession        */ "MA0025",
/* name             */ "NFIL3",
/* label            */ " MA0025	14.1385069219678	NFIL3	bZIP	; acc \"NP_005375\" ; medline \"1620116\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.1410\" ; type \"SELEX\" ",
/* motif            */ "TTATGTAAYNN",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_78_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -58.739246,
/* max_score        */ 11.211348,
/* threshold        */ 0.791,
/* info content     */ 14.028620,
/* base_counts      */ {82, 29, 38, 104},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 23
};

	float jaspar_core_pwm_79_matrix_row_0_[8] = {  1.202024, -6.042918,  1.202024, -6.042918, -0.048956, -1.427797, -6.042918, -0.335808};
	float jaspar_core_pwm_79_matrix_row_1_[8] = { -1.186635, -5.801756, -5.801756, -5.801756,  0.596839,  0.884105, -5.801756,  0.596839};
	float jaspar_core_pwm_79_matrix_row_2_[8] = { -5.801756,  1.512131, -1.186635, -5.801756, -5.801756, -5.801756,  1.001750,  0.192206};
	float jaspar_core_pwm_79_matrix_row_3_[8] = { -6.042918, -6.042918, -6.042918,  1.270969,  0.173688,  0.355677,  0.355677, -0.739613};
	float *jaspar_core_pwm_79_matrix[4] = { jaspar_core_pwm_79_matrix_row_0_, jaspar_core_pwm_79_matrix_row_1_, jaspar_core_pwm_79_matrix_row_2_, jaspar_core_pwm_79_matrix_row_3_};
	PWM jaspar_core_pwm_79_ = {
/* accession        */ "MA0121",
/* name             */ "ARR10",
/* label            */ " MA0121	9.59458248131567	ARR10	TRP-CLUSTER	; acc \"O49397\" ; medline \"12215502\" ; species \"Arabidopsis thaliana\" ; sysgroup \"plant\" ; total_ic \"9.59458248131567\" ; type \"SELEX\" ",
/* motif            */ "AGATNCGN",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_79_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -42.557716,
/* max_score        */ 8.266681,
/* threshold        */ 0.856,
/* info content     */ 9.480363,
/* base_counts      */ {36, 21, 29, 34},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 15
};

	float jaspar_core_pwm_80_matrix_row_0_[14] = { -6.107290, -6.107290, -6.107290, -6.107290,  1.137651, -0.803985, -0.803985,  0.445218, -0.113329, -6.107290, -6.107290, -6.107290,  1.271093,  1.137651};
	float jaspar_core_pwm_80_matrix_row_1_[14] = { -5.866128, -5.866128, -5.866128,  1.512256, -1.251008,  0.819733,  0.819733, -1.251008, -0.159018, -5.866128,  1.512256,  1.512256, -5.866128, -5.866128};
	float jaspar_core_pwm_80_matrix_row_2_[14] = { -5.866128,  1.512256,  1.512256, -5.866128, -5.866128,  0.350478,  0.127833,  0.350478, -0.562823,  1.512256, -5.866128, -5.866128, -5.866128, -1.251008};
	float jaspar_core_pwm_80_matrix_row_3_[14] = {  1.271093, -6.107290, -6.107290, -6.107290, -1.492170, -1.492170, -0.803985, -0.400180,  0.445218, -6.107290, -6.107290, -6.107290, -6.107290, -1.492170};
	float *jaspar_core_pwm_80_matrix[4] = { jaspar_core_pwm_80_matrix_row_0_, jaspar_core_pwm_80_matrix_row_1_, jaspar_core_pwm_80_matrix_row_2_, jaspar_core_pwm_80_matrix_row_3_};
	PWM jaspar_core_pwm_80_ = {
/* accession        */ "MA0119",
/* name             */ "TLX1-NFIC",
/* label            */ " MA0119	19.6648726674762	TLX1-NFIC	HOMEO/CAAT	; acc \"P31314 , NP_995315.1\" ; comment \"heterodimer between TLX1 and NFIC\" ; medline \"10327073\" ; species \"Homo sapiens\" ; sysgroup \"veterbrate\" ; total_ic \"19.6648726674762\" ; type \"SELEX\" ",
/* motif            */ "TGGCASSRNGCCAA",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_80_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -64.700569,
/* max_score        */ 16.420925,
/* threshold        */ 0.780,
/* info content     */ 19.456713,
/* base_counts      */ {59, 69, 65, 31},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_81_matrix_row_0_[12] = { -2.214803,  0.609048, -6.829924, -6.829924,  1.272057, -6.829924, -6.829924, -6.829924, -6.829924, -2.214803,  0.174051, -2.214803};
	float jaspar_core_pwm_81_matrix_row_1_[12] = { -6.588761, -6.588761, -6.588761, -6.588761, -6.588761,  1.513219, -6.588761, -6.588761, -1.973641, -6.588761,  1.012641,  1.482457};
	float jaspar_core_pwm_81_matrix_row_2_[12] = {  1.450719,  0.789622, -6.588761,  1.513219, -6.588761, -6.588761,  1.513219, -6.588761,  1.482457,  1.107906, -1.973641, -6.588761};
	float jaspar_core_pwm_81_matrix_row_3_[12] = { -2.214803, -6.829924,  1.272057, -6.829924, -6.829924, -6.829924, -6.829924,  1.272057, -6.829924,  0.078831, -2.214803, -6.829924};
	float *jaspar_core_pwm_81_matrix[4] = { jaspar_core_pwm_81_matrix_row_0_, jaspar_core_pwm_81_matrix_row_1_, jaspar_core_pwm_81_matrix_row_2_, jaspar_core_pwm_81_matrix_row_3_};
	PWM jaspar_core_pwm_81_ = {
/* accession        */ "MA0097",
/* name             */ "bZIP911",
/* label            */ " MA0097	19.8816971076341	bZIP911	bZIP	; acc \"CAA74023\" ; medline \"9680995\" ; species \"Antirrhinum majus\" ; sysgroup \"plant\" ; total_ic \"19.8800\" ; type \"SELEX\" ",
/* motif            */ "GATGACGTGGCC",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_81_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.861649,
/* max_score        */ 15.681631,
/* threshold        */ 0.777,
/* info content     */ 19.770611,
/* base_counts      */ {64, 86, 168, 78},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 33
};

	float jaspar_core_pwm_82_matrix_row_0_[9] = { -5.900226, -5.900226, -5.900226, -5.900226, -5.900226,  0.498369, -0.193116, -5.900226, -5.900226};
	float jaspar_core_pwm_82_matrix_row_1_[9] = {  0.557542, -5.659064,  0.334897, -0.355759, -1.043944, -1.043944,  0.557542, -1.043944,  1.026797};
	float jaspar_core_pwm_82_matrix_row_2_[9] = {  1.026797,  0.048046, -5.659064,  1.249691, -5.659064,  0.048046, -5.659064, -0.355759, -1.043944};
	float jaspar_core_pwm_82_matrix_row_3_[9] = { -5.900226,  1.008529,  0.903279, -1.285106,  1.190684, -0.193116,  0.316380,  1.008529,  0.093735};
	float *jaspar_core_pwm_82_matrix[4] = { jaspar_core_pwm_82_matrix_row_0_, jaspar_core_pwm_82_matrix_row_1_, jaspar_core_pwm_82_matrix_row_2_, jaspar_core_pwm_82_matrix_row_3_};
	PWM jaspar_core_pwm_82_ = {
/* accession        */ "MA0044",
/* name             */ "HMG-1",
/* label            */ " MA0044	8.43180902169531	HMG-1	HMG	; acc \"CAA54168\" ; medline \"9161031\" ; species \"Pisum sativum\" ; sysgroup \"plant\" ; total_ic \"8.4330\" ; type \"SELEX\" ",
/* motif            */ "GTTGTNYTC",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_82_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.004593,
/* max_score        */ 8.470216,
/* threshold        */ 0.846,
/* info content     */ 8.318785,
/* base_counts      */ {9, 27, 27, 54},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 13
};

	float jaspar_core_pwm_83_matrix_row_0_[30] = {  0.173878,  1.222772,  0.999754,  0.625344,  0.792259,  0.625344,  0.019965,  0.424875, -0.162024, -0.162024,  0.019965,  0.173878,  0.307231,  0.019965,  0.530125, -0.384668, -0.671520,  0.173878, -0.384668,  0.424875, -0.162024, -0.162024,  0.173878,  0.019965, -0.671520,  0.173878, -0.671520, -1.763509,  0.019965, -0.671520};
	float jaspar_core_pwm_83_matrix_row_1_[30] = { -0.834163, -6.137468, -0.834163, -1.522347, -1.522347, -0.430358, -1.522347, -1.522347, -0.430358,  0.866506,  0.866506,  0.415040,  0.415040,  0.079138,  0.079138,  0.548393,  0.261127,  0.548393,  0.666037,  0.261127,  0.415040,  0.261127,  0.866506,  0.666037,  0.953442,  0.666037,  0.953442,  0.866506,  0.866506,  1.033421};
	float jaspar_core_pwm_83_matrix_row_2_[30] = {  0.866506, -1.522347, -1.522347, -1.522347, -0.430358, -1.522347, -0.834163, -1.522347,  0.261127, -1.522347, -0.143506,  0.079138, -0.834163, -1.522347, -0.143506, -0.143506, -0.143506, -0.430358, -0.430358, -0.834163,  0.079138, -0.834163, -6.137468, -1.522347, -0.834163, -0.834163, -0.834163, -0.430358, -1.522347, -0.834163};
	float jaspar_core_pwm_83_matrix_row_3_[30] = { -1.763509, -6.378630, -1.075325,  0.307231, -0.384668,  0.019965,  0.712280,  0.530125,  0.173878, -0.384668, -6.378630, -1.075325, -0.384668,  0.424875, -1.075325, -0.162024,  0.307231, -0.671520, -0.162024, -0.384668, -0.384668,  0.307231, -0.671520, -0.162024, -0.384668, -0.671520, -0.384668,  0.019965, -0.671520, -0.671520};
	float *jaspar_core_pwm_83_matrix[4] = { jaspar_core_pwm_83_matrix_row_0_, jaspar_core_pwm_83_matrix_row_1_, jaspar_core_pwm_83_matrix_row_2_, jaspar_core_pwm_83_matrix_row_3_};
	PWM jaspar_core_pwm_83_ = {
/* accession        */ "MA0068",
/* name             */ "Pax4",
/* label            */ " MA0068	11.0044478832836	Pax4	PAIRED-HOMEO	; acc \"P32115\" ; medline \"10567552\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"11.0000\" ; type \"SELEX\" ",
/* motif            */ "GAAAAATWNCCNNNNNNNNNNNCNCMCCCC",
/* library_name     */ "jaspar_core",
/* length           */ 30,
/* matrixname       */ jaspar_core_pwm_83_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.608105,
/* max_score        */ 20.242702,
/* threshold        */ 0.670,
/* info content     */ 10.945747,
/* base_counts      */ {213, 198, 80, 139},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_84_matrix_row_0_[18] = { -5.820439, -0.517134,  1.270471,  1.183535,  1.270471, -0.517134, -5.820439, -5.820439,  1.270471,  1.270471,  1.270471, -5.820439, -5.820439,  0.396167,  0.578156,  0.578156,  0.396167, -0.113329};
	float jaspar_core_pwm_84_matrix_row_1_[18] = {  0.414685, -5.579277, -5.579277, -5.579277, -5.579277, -5.579277,  0.819318, -5.579277, -5.579277, -5.579277, -5.579277,  0.819318,  0.973231, -0.275972, -5.579277, -0.964156, -0.275972,  0.819318};
	float jaspar_core_pwm_84_matrix_row_2_[18] = {  0.973231,  1.329478, -5.579277, -5.579277, -5.579277,  1.329478, -5.579277,  1.511633, -5.579277, -5.579277, -5.579277,  0.819318, -0.275972,  0.127833, -0.275972, -0.964156, -0.964156, -0.964156};
	float jaspar_core_pwm_84_matrix_row_3_[18] = { -1.205318, -5.820439, -5.820439, -1.205318, -5.820439, -5.820439,  0.578156, -5.820439, -5.820439, -5.820439, -5.820439, -5.820439, -0.113329, -0.517134,  0.173523,  0.173523,  0.173523, -0.517134};
	float *jaspar_core_pwm_84_matrix[4] = { jaspar_core_pwm_84_matrix_row_0_, jaspar_core_pwm_84_matrix_row_1_, jaspar_core_pwm_84_matrix_row_2_, jaspar_core_pwm_84_matrix_row_3_};
	PWM jaspar_core_pwm_84_ = {
/* accession        */ "MA0051",
/* name             */ "IRF2",
/* label            */ " MA0051	21.1344205827808	IRF2	TRP-CLUSTER	; acc \"P14316\" ; medline \"7687740\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"21.1360\" ; type \"SELEX\" ",
/* motif            */ "GGAAAGYGAAASCNWWWM",
/* library_name     */ "jaspar_core",
/* length           */ 18,
/* matrixname       */ jaspar_core_pwm_84_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.413429,
/* max_score        */ 18.059540,
/* threshold        */ 0.799,
/* info content     */ 20.828207,
/* base_counts      */ {100, 34, 55, 27},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12
};

	float jaspar_core_pwm_85_matrix_row_0_[10] = {  0.935875, -0.672231, -2.455706, -1.076865, -1.363716, -7.070826, -1.767521,  1.000392, -1.076865, -1.767521};
	float jaspar_core_pwm_85_matrix_row_1_[10] = { -1.526359,  0.720471, -6.829664, -6.829664,  1.272317, -1.526359, -6.829664, -0.277156, -1.122554, -0.613058};
	float jaspar_core_pwm_85_matrix_row_2_[10] = { -0.143803, -0.277156, -1.526359, -0.835703, -1.526359, -1.526359, -2.214544, -1.526359, -6.829664,  0.415278};
	float jaspar_core_pwm_85_matrix_row_3_[10] = { -1.767521, -0.162071,  1.198162,  1.060999, -1.076865,  1.172193,  1.198162, -2.455706,  1.089978,  0.579343};
	float *jaspar_core_pwm_85_matrix[4] = { jaspar_core_pwm_85_matrix_row_0_, jaspar_core_pwm_85_matrix_row_1_, jaspar_core_pwm_85_matrix_row_2_, jaspar_core_pwm_85_matrix_row_3_};
	PWM jaspar_core_pwm_85_ = {
/* accession        */ "MA0127",
/* name             */ "PEND",
/* label            */ " MA0127	10.0591277357396	PEND	bZIP	; acc \"BAD90707\" ; medline \"9973626\" ; species \"Pisum sativum\" ; sysgroup \"plant\" ; total_ic \"10.059\" ; type \"SELEX\" ",
/* motif            */ "ANTTCTTATK",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_85_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -42.578823,
/* max_score        */ 10.227892,
/* threshold        */ 0.818,
/* info content     */ 10.029187,
/* base_counts      */ {84, 71, 42, 223},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 42
};

	float jaspar_core_pwm_86_matrix_row_0_[12] = { -0.008190, -0.517686,  1.153588, -1.609676, -6.224796,  1.214175,  1.214175, -6.224796, -6.224796,  1.153588,  0.866114,  0.461065};
	float jaspar_core_pwm_86_matrix_row_1_[12] = {  0.414961,  0.819871, -1.368514, -1.368514,  1.512463, -1.368514, -5.983634, -5.983634,  1.512463, -1.368514, -5.983634, -0.680329};
	float jaspar_core_pwm_86_matrix_row_2_[12] = { -0.680329, -0.276524, -1.368514, -5.983634, -5.983634, -5.983634, -5.983634, -1.368514, -5.983634, -5.983634, -1.368514, -0.680329};
	float jaspar_core_pwm_86_matrix_row_3_[12] = { -0.008190, -0.517686, -6.224796,  1.153588, -6.224796, -6.224796, -1.609676,  1.214175, -6.224796, -1.609676, -0.008190,  0.173799};
	float *jaspar_core_pwm_86_matrix[4] = { jaspar_core_pwm_86_matrix_row_0_, jaspar_core_pwm_86_matrix_row_1_, jaspar_core_pwm_86_matrix_row_2_, jaspar_core_pwm_86_matrix_row_3_};
	PWM jaspar_core_pwm_86_ = {
/* accession        */ "MA0070",
/* name             */ "PBX1",
/* label            */ " MA0070	14.6408952002356	PBX1	HOMEO	; acc \"Q5T486\" ; medline \"7910944\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.6440\" ; type \"SELEX\" ",
/* motif            */ "NNATCAATCAAW",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_86_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -56.936863,
/* max_score        */ 12.690227,
/* threshold        */ 0.781,
/* info content     */ 14.513494,
/* base_counts      */ {95, 57, 10, 54},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18
};

	float jaspar_core_pwm_87_matrix_row_0_[6] = { -6.767481,  1.205330, -6.767481, -1.464176, -6.767481, -6.767481};
	float jaspar_core_pwm_87_matrix_row_1_[6] = {  1.513161, -6.526319,  1.480382, -1.911199, -0.819209, -6.526319};
	float jaspar_core_pwm_87_matrix_row_2_[6] = { -6.526319, -6.526319, -6.526319,  1.411413, -6.526319,  1.513161};
	float jaspar_core_pwm_87_matrix_row_3_[6] = { -6.767481, -1.464176, -2.152361, -6.767481,  1.170251, -6.767481};
	float *jaspar_core_pwm_87_matrix[4] = { jaspar_core_pwm_87_matrix_row_0_, jaspar_core_pwm_87_matrix_row_1_, jaspar_core_pwm_87_matrix_row_2_, jaspar_core_pwm_87_matrix_row_3_};
	PWM jaspar_core_pwm_87_ = {
/* accession        */ "MA0104",
/* name             */ "Mycn",
/* label            */ " MA0104	10.4430512056894	Mycn	bHLH-ZIP	; acc \"P03966\" ; medline \"1594445\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"10.4440\" ; type \"SELEX\" ",
/* motif            */ "CACGTG",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_87_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -40.363728,
/* max_score        */ 8.293696,
/* threshold        */ 0.881,
/* info content     */ 10.383476,
/* base_counts      */ {31, 65, 59, 31},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 31
};

	float jaspar_core_pwm_88_matrix_row_0_[12] = {  0.068660,  1.220201,  1.220201,  1.271468, -0.113329, -6.329935, -1.714814,  1.271468,  1.220201,  1.271468, -1.714814, -1.714814};
	float jaspar_core_pwm_88_matrix_row_1_[12] = { -0.094811, -6.088773, -6.088773, -6.088773, -0.381663,  0.819982, -1.473652, -6.088773, -1.473652, -6.088773,  1.082116,  1.082116};
	float jaspar_core_pwm_88_matrix_row_2_[12] = {  0.819982, -1.473652, -6.088773, -6.088773,  0.915201, -6.088773,  1.407325, -6.088773, -6.088773, -6.088773,  0.309822, -1.473652};
	float jaspar_core_pwm_88_matrix_row_3_[12] = { -6.329935, -6.329935, -1.714814, -6.329935, -1.714814,  0.578820, -6.329935, -6.329935, -6.329935, -6.329935, -6.329935, -0.113329};
	float *jaspar_core_pwm_88_matrix[4] = { jaspar_core_pwm_88_matrix_row_0_, jaspar_core_pwm_88_matrix_row_1_, jaspar_core_pwm_88_matrix_row_2_, jaspar_core_pwm_88_matrix_row_3_};
	PWM jaspar_core_pwm_88_ = {
/* accession        */ "MA0050",
/* name             */ "IRF1",
/* label            */ " MA0050	16.0080101497756	IRF1	TRP-CLUSTER	; acc \"P10914\" ; medline \"7687740\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"16.0090\" ; type \"SELEX\" ",
/* motif            */ "RAAAGYGAAACC",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_88_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.487808,
/* max_score        */ 13.601726,
/* threshold        */ 0.780,
/* info content     */ 15.871733,
/* base_counts      */ {131, 45, 47, 17},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_89_matrix_row_0_[10] = {  0.230827,  0.836206,  0.048838, -6.167768,  1.210616, -6.167768, -1.552648, -6.167768, -6.167768,  0.230827};
	float jaspar_core_pwm_89_matrix_row_1_[10] = { -1.311486, -1.311486,  0.625902,  1.512366, -1.311486,  1.512366, -5.926606, -5.926606, -5.926606, -0.219496};
	float jaspar_core_pwm_89_matrix_row_2_[10] = {  0.471989,  0.290000, -1.311486, -5.926606, -5.926606, -5.926606,  1.451778, -5.926606,  1.512366,  0.290000};
	float jaspar_core_pwm_89_matrix_row_3_[10] = { -0.173807, -6.167768, -0.173807, -6.167768, -6.167768, -6.167768, -6.167768,  1.271204, -6.167768, -0.460658};
	float *jaspar_core_pwm_89_matrix[4] = { jaspar_core_pwm_89_matrix_row_0_, jaspar_core_pwm_89_matrix_row_1_, jaspar_core_pwm_89_matrix_row_2_, jaspar_core_pwm_89_matrix_row_3_};
	PWM jaspar_core_pwm_89_ = {
/* accession        */ "MA0058",
/* name             */ "MAX",
/* label            */ " MA0058	12.6854143466175	MAX	bHLH-ZIP	; acc \"AAH36092\" ; medline \"8265351\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"12.6860\" ; type \"SELEX\" ",
/* motif            */ "NANCACGTGN",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_89_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.258003,
/* max_score        */ 10.694789,
/* threshold        */ 0.806,
/* info content     */ 12.556767,
/* base_counts      */ {45, 47, 50, 28},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_90_matrix_row_0_[11] = { -0.230835,  0.946092, -0.008190, -0.517686, -6.224796, -6.224796, -6.224796, -6.224796,  1.214175, -6.224796,  0.173799};
	float jaspar_core_pwm_90_matrix_row_1_[11] = {  0.010327, -1.368514, -0.680329, -5.983634, -5.983634, -5.983634, -5.983634, -5.983634, -5.983634, -1.368514, -5.983634};
	float jaspar_core_pwm_90_matrix_row_2_[11] = { -0.276524, -0.276524, -5.983634, -5.983634,  1.512463, -5.983634, -5.983634, -5.983634, -1.368514,  0.010327, -0.276524};
	float jaspar_core_pwm_90_matrix_row_3_[11] = {  0.327712, -1.609676,  0.779178,  1.089091, -6.224796,  1.271301,  1.271301,  1.271301, -6.224796,  0.946092,  0.578709};
	float *jaspar_core_pwm_90_matrix[4] = { jaspar_core_pwm_90_matrix_row_0_, jaspar_core_pwm_90_matrix_row_1_, jaspar_core_pwm_90_matrix_row_2_, jaspar_core_pwm_90_matrix_row_3_};
	PWM jaspar_core_pwm_90_ = {
/* accession        */ "MA0040",
/* name             */ "Foxq1",
/* label            */ " MA0040	14.0702573602851	Foxq1	FORKHEAD	; acc \"Q63244\" ; medline \"8139574\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"14.0720\" ; type \"SELEX\" ",
/* motif            */ "NATTGTTTATW",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_90_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -57.185879,
/* max_score        */ 11.207417,
/* threshold        */ 0.791,
/* info content     */ 13.934083,
/* base_counts      */ {48, 8, 32, 110},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18
};

	float jaspar_core_pwm_91_matrix_row_0_[7] = { -6.734735,  1.271966, -6.734735, -2.119614, -6.734735, -6.734735,  0.068771};
	float jaspar_core_pwm_91_matrix_row_1_[7] = {  1.513129, -6.493572,  1.444160, -6.493572, -1.878452, -6.493572, -1.190267};
	float jaspar_core_pwm_91_matrix_row_2_[7] = { -6.493572, -6.493572, -6.493572,  1.479239, -1.878452,  1.513129,  0.751369};
	float jaspar_core_pwm_91_matrix_row_3_[7] = { -6.734735, -6.734735, -1.431430, -6.734735,  1.202997, -6.734735, -0.518128};
	float *jaspar_core_pwm_91_matrix[4] = { jaspar_core_pwm_91_matrix_row_0_, jaspar_core_pwm_91_matrix_row_1_, jaspar_core_pwm_91_matrix_row_2_, jaspar_core_pwm_91_matrix_row_3_};
	PWM jaspar_core_pwm_91_ = {
/* accession        */ "MA0093",
/* name             */ "USF1",
/* label            */ " MA0093	11.2902795805709	USF1	bHLH-ZIP	; acc \"P22415\" ; medline \"8052536\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"11.2920\" ; type \"SELEX\" ",
/* motif            */ "CACGTGR",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_91_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -41.598671,
/* max_score        */ 9.175988,
/* threshold        */ 0.856,
/* info content     */ 11.223946,
/* base_counts      */ {40, 61, 74, 35},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 30
};

	float jaspar_core_pwm_92_matrix_row_0_[13] = {  0.473570,  1.048449,  1.220201, -6.329935, -6.329935,  1.220201,  1.271468, -1.026630, -6.329935,  1.048449,  0.473570, -1.026630, -1.026630};
	float jaspar_core_pwm_92_matrix_row_1_[13] = { -6.088773, -0.785468, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773, -0.785468, -1.473652, -1.473652, -6.088773,  0.597088,  0.714733};
	float jaspar_core_pwm_92_matrix_row_2_[13] = { -0.381663, -0.785468, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773, -0.381663, -6.088773, -0.381663};
	float jaspar_core_pwm_92_matrix_row_3_[13] = {  0.355926, -6.329935, -1.714814,  1.271468,  1.271468, -1.714814, -6.329935,  1.048449,  1.220201, -0.622825,  0.355926,  0.578820,  0.068660};
	float *jaspar_core_pwm_92_matrix[4] = { jaspar_core_pwm_92_matrix_row_0_, jaspar_core_pwm_92_matrix_row_1_, jaspar_core_pwm_92_matrix_row_2_, jaspar_core_pwm_92_matrix_row_3_};
	PWM jaspar_core_pwm_92_ = {
/* accession        */ "MA0135",
/* name             */ "Lhx3",
/* label            */ " MA0135	16.3541415144804	Lhx3	HOMEO	; acc \"P50481\" ; comment \"two isoforms with different DNA models\" ; medline \"11602361\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"16.354\" ; type \"SELEX\" ",
/* motif            */ "WAATTAATTAWYY",
/* library_name     */ "jaspar_core",
/* length           */ 13,
/* matrixname       */ jaspar_core_pwm_92_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.297714,
/* max_score        */ 12.879313,
/* threshold        */ 0.775,
/* info content     */ 16.213665,
/* base_counts      */ {114, 23, 11, 112},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_93_matrix_row_0_[12] = { -0.517686, -1.609676, -0.230835, -0.921491, -0.230835, -0.921491,  1.271301,  1.271301, -6.224796, -0.008190, -0.921491, -1.609676};
	float jaspar_core_pwm_93_matrix_row_1_[12] = { -5.983634, -1.368514, -1.368514,  0.819871, -0.680329,  1.330253, -5.983634, -5.983634,  0.414961,  0.414961,  0.232972,  0.414961};
	float jaspar_core_pwm_93_matrix_row_2_[12] = { -5.983634,  0.010327,  0.414961, -0.680329,  0.925121, -5.983634, -5.983634, -5.983634, -0.680329,  0.232972, -0.276524,  0.232972};
	float jaspar_core_pwm_93_matrix_row_3_[12] = {  1.089091,  0.866114,  0.327712, -0.008190, -0.921491, -1.609676, -6.224796, -6.224796,  0.683959, -0.921491,  0.461065,  0.173799};
	float *jaspar_core_pwm_93_matrix[4] = { jaspar_core_pwm_93_matrix_row_0_, jaspar_core_pwm_93_matrix_row_1_, jaspar_core_pwm_93_matrix_row_2_, jaspar_core_pwm_93_matrix_row_3_};
	PWM jaspar_core_pwm_93_ = {
/* accession        */ "MA0102",
/* name             */ "Cebpa",
/* label            */ " MA0102	9.18699952114279	Cebpa	bZIP	; acc \"\" ; medline \"1672737\" ; species \"-\" ; sysgroup \"vertebrate\" ; total_ic \"9.1860\" ; type \"COMPILED\" ",
/* motif            */ "TTNYGCAATNNN",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_93_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -38.915485,
/* max_score        */ 9.962959,
/* threshold        */ 0.785,
/* info content     */ 9.106795,
/* base_counts      */ {60, 51, 37, 68},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18
};

	float jaspar_core_pwm_94_matrix_row_0_[11] = {  0.316916,  0.499071, -0.597877, -1.976718, -1.288533, -1.288533, -6.591838, -6.591838, -6.591838,  0.094023,  0.579050};
	float jaspar_core_pwm_94_matrix_row_1_[11] = { -1.047371, -1.047371,  0.201832, -1.735556, -6.350677,  0.335185, -6.350677, -6.350677, -1.735556, -1.047371, -1.047371};
	float jaspar_core_pwm_94_matrix_row_2_[11] = { -0.643566, -1.735556, -1.735556, -6.350677,  1.390423, -6.350677,  1.512975,  1.512975, -6.350677, -6.350677, -0.356715};
	float jaspar_core_pwm_94_matrix_row_3_[11] = {  0.412136,  0.412136,  0.653103,  1.191802, -1.976718,  0.786545, -6.591838, -6.591838,  1.232608,  0.786545, -0.039330};
	float *jaspar_core_pwm_94_matrix[4] = { jaspar_core_pwm_94_matrix_row_0_, jaspar_core_pwm_94_matrix_row_1_, jaspar_core_pwm_94_matrix_row_2_, jaspar_core_pwm_94_matrix_row_3_};
	PWM jaspar_core_pwm_94_ = {
/* accession        */ "MA0002",
/* name             */ "RUNX1",
/* label            */ " MA0002	11.6956693818896	RUNX1	RUNT	; acc \"Q01196 \" ; comment \"Matrix changed since last release: removal of primers and sites overlapping primers\" ; medline \"8413232\" ; species \"Arabidopsis thaliana\" ; sysgroup \"plant\" ; total_ic \"11.696\" ; type \"SELEX\" ",
/* motif            */ "WWTTGTGGTTW",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_94_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -50.744080,
/* max_score        */ 10.557234,
/* threshold        */ 0.797,
/* info content     */ 11.623886,
/* base_counts      */ {52, 25, 84, 125},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 26
};

	float jaspar_core_pwm_95_matrix_row_0_[7] = {  0.840954, -6.329935,  1.271468, -6.329935, -1.714814, -1.714814,  1.220201};
	float jaspar_core_pwm_95_matrix_row_1_[7] = { -1.473652, -1.473652, -6.088773,  1.461363, -6.088773, -6.088773, -6.088773};
	float jaspar_core_pwm_95_matrix_row_2_[7] = { -0.381663, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773, -1.473652};
	float jaspar_core_pwm_95_matrix_row_3_[7] = { -0.622825,  1.220201, -6.329935, -1.714814,  1.220201,  1.220201, -6.329935};
	float *jaspar_core_pwm_95_matrix[4] = { jaspar_core_pwm_95_matrix_row_0_, jaspar_core_pwm_95_matrix_row_1_, jaspar_core_pwm_95_matrix_row_2_, jaspar_core_pwm_95_matrix_row_3_};
	PWM jaspar_core_pwm_95_ = {
/* accession        */ "MA0124",
/* name             */ "NKX3-1",
/* label            */ " MA0124	11.1268616865409	NKX3-1	HOMEO	; acc \"Q99801\" ; medline \"10871372\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"11.127\" ; type \"SELEX\" ",
/* motif            */ "ATACTTA",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_95_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -38.970940,
/* max_score        */ 8.454586,
/* threshold        */ 0.858,
/* info content     */ 11.035941,
/* base_counts      */ {54, 21, 4, 61},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_96_matrix_row_0_[12] = {  0.445218,  0.696215, -0.113329, -6.107290,  1.271093,  0.445218, -6.107290,  0.291305, -6.107290, -6.107290,  0.291305, -6.107290};
	float jaspar_core_pwm_96_matrix_row_1_[12] = {  0.819733, -5.866128, -0.562823,  1.447759, -5.866128, -5.866128,  1.447759, -5.866128, -5.866128,  1.042627, -5.866128, -5.866128};
	float jaspar_core_pwm_96_matrix_row_2_[12] = { -1.251008,  0.686380,  1.042627, -1.251008, -5.866128,  0.937377, -1.251008, -5.866128,  1.512256,  0.532467, -5.866128,  1.512256};
	float jaspar_core_pwm_96_matrix_row_3_[12] = { -6.107290, -6.107290, -6.107290, -6.107290, -6.107290, -6.107290, -6.107290,  0.801465, -6.107290, -6.107290,  0.801465, -6.107290};
	float *jaspar_core_pwm_96_matrix[4] = { jaspar_core_pwm_96_matrix_row_0_, jaspar_core_pwm_96_matrix_row_1_, jaspar_core_pwm_96_matrix_row_2_, jaspar_core_pwm_96_matrix_row_3_};
	PWM jaspar_core_pwm_96_ = {
/* accession        */ "MA0055",
/* name             */ "Myf",
/* label            */ " MA0055	15.9141418791189	Myf	bHLH	; acc \"\" ; medline \"9571041\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"15.9130\" ; type \"COMPILED\" ",
/* motif            */ "MAGCAGCTGCTG",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_96_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.805161,
/* max_score        */ 13.332630,
/* threshold        */ 0.780,
/* info content     */ 15.726956,
/* base_counts      */ {55, 50, 67, 20},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_97_matrix_row_0_[9] = { -8.552776, -8.552776, -8.552776, -0.856109, -1.002641,  0.059909,  0.022874, -1.002641, -1.749271};
	float jaspar_core_pwm_97_matrix_row_1_[9] = { -8.311614,  1.513966,  1.513966,  0.556376,  0.336782,  0.077973, -0.304914, -0.933231,  0.650393};
	float jaspar_core_pwm_97_matrix_row_2_[9] = {  1.513966, -8.311614, -8.311614,  0.122414,  0.404594,  0.498398,  0.804525,  1.213610,  0.663130};
	float jaspar_core_pwm_97_matrix_row_3_[9] = { -8.552776, -8.552776, -8.552776, -0.118748, -0.076197, -1.002641, -1.548802, -1.381888, -1.002641};
	float *jaspar_core_pwm_97_matrix[4] = { jaspar_core_pwm_97_matrix_row_0_, jaspar_core_pwm_97_matrix_row_1_, jaspar_core_pwm_97_matrix_row_2_, jaspar_core_pwm_97_matrix_row_3_};
	PWM jaspar_core_pwm_97_ = {
/* accession        */ "MA0003",
/* name             */ "TFAP2A",
/* label            */ " MA0003	7.81209200796467	TFAP2A	AP2	; acc \"P05549\" ; medline \"10497269\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"7.8120\" ; type \"SELEX\" ",
/* motif            */ "GCCNNNRGS",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_97_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -33.199680,
/* max_score        */ 8.682529,
/* threshold        */ 0.850,
/* info content     */ 7.803656,
/* base_counts      */ {177, 666, 666, 156},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 185
};

	float jaspar_core_pwm_98_matrix_row_0_[19] = { -1.809943,  0.127444, -1.809943, -6.425064, -6.425064, -1.809943, -6.425064, -6.425064, -6.425064,  0.578911, -0.208457, -1.121759, -6.425064, -1.809943, -6.425064, -6.425064, -6.425064, -1.809943,  0.888823};
	float jaspar_core_pwm_98_matrix_row_1_[19] = { -0.880597,  0.619604, -0.476791,  1.512766, -1.568781, -0.189940, -6.183901,  1.366234,  1.466267, -0.476791, -1.568781, -6.183901, -1.568781, -6.183901, -0.476791,  1.466267, -1.568781, -1.568781, -1.568781};
	float jaspar_core_pwm_98_matrix_row_2_[19] = {  1.129985, -1.568781,  1.129985, -6.183901, -6.183901,  1.255070, -0.880597, -1.568781, -6.183901,  0.032705, -0.476791,  1.417501,  1.466267, -1.568781,  1.194482, -6.183901, -6.183901,  1.417501,  0.032705};
	float jaspar_core_pwm_98_matrix_row_3_[19] = { -0.431102, -0.208457, -0.717953, -6.425064,  1.225105, -6.425064,  1.176339, -1.121759, -1.809943, -0.717953,  0.745825, -6.425064, -6.425064,  1.176339, -0.717953, -1.809943,  1.225105, -6.425064, -1.809943};
	float *jaspar_core_pwm_98_matrix[4] = { jaspar_core_pwm_98_matrix_row_0_, jaspar_core_pwm_98_matrix_row_1_, jaspar_core_pwm_98_matrix_row_2_, jaspar_core_pwm_98_matrix_row_3_};
	PWM jaspar_core_pwm_98_ = {
/* accession        */ "MA0138",
/* name             */ "REST",
/* label            */ " MA0138	22.9583305401171	REST	ZN-FINGER, C2H2 	; acc \"NP_005603\" ; medline \"-\" ; species \"Homo Sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"22.958\" ; type \"COMPILED\" ",
/* motif            */ "GNGCTGTCCNTGGTGCTGA",
/* library_name     */ "jaspar_core",
/* length           */ 19,
/* matrixname       */ jaspar_core_pwm_98_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.570015,
/* max_score        */ 22.458378,
/* threshold        */ 0.828,
/* info content     */ 22.800755,
/* base_counts      */ {45, 113, 142, 118},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 22
};

	float jaspar_core_pwm_99_matrix_row_0_[5] = {  1.270969,  1.270969,  1.270969, -6.042918, -0.335808};
	float jaspar_core_pwm_99_matrix_row_1_[5] = { -5.801756, -5.801756, -5.801756, -5.801756,  1.001750};
	float jaspar_core_pwm_99_matrix_row_2_[5] = { -5.801756, -5.801756, -5.801756,  1.512131, -5.801756};
	float jaspar_core_pwm_99_matrix_row_3_[5] = { -6.042918, -6.042918, -6.042918, -6.042918, -0.335808};
	float *jaspar_core_pwm_99_matrix[4] = { jaspar_core_pwm_99_matrix_row_0_, jaspar_core_pwm_99_matrix_row_1_, jaspar_core_pwm_99_matrix_row_2_, jaspar_core_pwm_99_matrix_row_3_};
	PWM jaspar_core_pwm_99_ = {
/* accession        */ "MA0053",
/* name             */ "MNB1A",
/* label            */ " MA0053	8.62904940554533	MNB1A	ZN-FINGER, DOF	; acc \"P38564\" ; medline \"10074718\" ; species \"Zea mays\" ; sysgroup \"plant\" ; total_ic \"8.6290\" ; type \"SELEX\" ",
/* motif            */ "AAAGC",
/* library_name     */ "jaspar_core",
/* length           */ 5,
/* matrixname       */ jaspar_core_pwm_99_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -29.973427,
/* max_score        */ 6.326788,
/* threshold        */ 0.924,
/* info content     */ 8.525431,
/* base_counts      */ {48, 9, 15, 3},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 15
};

	float jaspar_core_pwm_100_matrix_row_0_[20] = {  0.048838, -0.460658, -0.173807,  0.048838,  1.003120, -6.167768,  1.271204, -6.167768, -6.167768, -6.167768, -6.167768, -6.167768, -1.552648, -1.552648, -0.173807, -1.552648,  1.146119, -0.864463, -1.552648, -1.552648};
	float jaspar_core_pwm_100_matrix_row_1_[20] = {  0.759255,  0.625902, -5.926606, -5.926606, -5.926606,  1.512366, -5.926606, -5.926606, -5.926606,  1.077368,  1.451778,  1.451778, -5.926606, -5.926606, -5.926606,  1.318336, -5.926606, -5.926606, -1.311486, -0.623301};
	float jaspar_core_pwm_100_matrix_row_2_[20] = { -0.623301,  0.471989,  1.244282,  1.164304,  0.067355, -5.926606, -5.926606, -5.926606,  1.512366, -5.926606, -5.926606, -5.926606,  1.387281,  1.318336,  1.244282, -0.623301, -5.926606, -5.926606,  1.318336, -1.311486};
	float jaspar_core_pwm_100_matrix_row_3_[20] = { -0.864463, -1.552648, -6.167768, -6.167768, -6.167768, -6.167768, -6.167768,  1.271204, -6.167768,  0.230827, -1.552648, -1.552648, -1.552648, -0.864463, -6.167768, -6.167768, -0.864463,  1.146119, -1.552648,  1.003120};
	float *jaspar_core_pwm_100_matrix[4] = { jaspar_core_pwm_100_matrix_row_0_, jaspar_core_pwm_100_matrix_row_1_, jaspar_core_pwm_100_matrix_row_2_, jaspar_core_pwm_100_matrix_row_3_};
	PWM jaspar_core_pwm_100_ = {
/* accession        */ "MA0106",
/* name             */ "TP53",
/* label            */ " MA0106	26.2394386341062	TP53	P53	; acc \"P04637\" ; medline \"1588974\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"26.2420\" ; type \"SELEX\" ",
/* motif            */ "MSGGACATGCCCGGGCATGT",
/* library_name     */ "jaspar_core",
/* length           */ 20,
/* matrixname       */ jaspar_core_pwm_100_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.242050,
/* max_score        */ 24.226852,
/* threshold        */ 0.882,
/* info content     */ 25.981657,
/* base_counts      */ {73, 92, 113, 62},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_101_matrix_row_0_[20] = { -5.638782,  0.759813, -0.335477, -5.638782, -0.335477, -1.023661, -5.638782, -5.638782,  1.164724, -5.638782,  0.355180,  0.759813, -5.638782, -5.638782, -5.638782,  0.355180,  0.068329, -0.335477, -1.023661,  0.068329};
	float jaspar_core_pwm_101_matrix_row_1_[20] = {  0.309491, -0.094315,  0.309491,  0.309491, -0.782499,  1.154888,  1.511135,  1.511135, -0.782499, -0.782499,  0.596342, -0.094315, -5.397620, -0.782499,  1.511135,  0.818987, -0.782499,  0.309491, -0.782499,  1.000975};
	float jaspar_core_pwm_101_matrix_row_2_[20] = {  0.596342, -0.782499, -0.782499, -0.094315, -5.397620, -5.397620, -5.397620, -5.397620, -5.397620,  0.596342, -0.094315, -0.094315, -0.782499,  1.000975, -5.397620, -5.397620, -0.782499, -5.397620,  1.154888, -0.782499};
	float jaspar_core_pwm_101_matrix_row_3_[20] = {  0.068329, -1.023661,  0.355180,  0.577825,  0.913726, -0.335477, -5.638782, -5.638782, -5.638782,  0.577825, -5.638782, -5.638782,  1.164724,  0.068329, -5.638782, -1.023661,  0.577825,  0.577825, -1.023661, -5.638782};
	float *jaspar_core_pwm_101_matrix[4] = { jaspar_core_pwm_101_matrix_row_0_, jaspar_core_pwm_101_matrix_row_1_, jaspar_core_pwm_101_matrix_row_2_, jaspar_core_pwm_101_matrix_row_3_};
	PWM jaspar_core_pwm_101_ = {
/* accession        */ "MA0088",
/* name             */ "Staf",
/* label            */ " MA0088	17.5411729665119	Staf	ZN-FINGER, C2H2	; acc \"Q91853\" ; medline \"9009278\" ; species \"Xenopus laevis\" ; sysgroup \"vertebrate\" ; total_ic \"17.5380\" ; type \"COMPILED\" ",
/* motif            */ "NANYTCCCAKMATGCMWYGC",
/* library_name     */ "jaspar_core",
/* length           */ 20,
/* matrixname       */ jaspar_core_pwm_101_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.868179,
/* max_score        */ 18.304598,
/* threshold        */ 0.755,
/* info content     */ 17.252653,
/* base_counts      */ {43, 74, 32, 51},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 10
};

	float jaspar_core_pwm_102_matrix_row_0_[9] = { -0.214973, -0.081620, -1.060371,  1.239220, -6.767481, -6.767481, -6.767481, -6.767481, -6.767481};
	float jaspar_core_pwm_102_matrix_row_1_[9] = {  0.277186,  0.159542,  0.969778, -6.526319, -1.911199, -6.526319, -6.526319, -6.526319,  0.912652};
	float jaspar_core_pwm_102_matrix_row_2_[9] = { -0.127724, -0.532358, -1.911199, -6.526319, -6.526319, -6.526319,  1.513161, -1.223014,  0.382436};
	float jaspar_core_pwm_102_matrix_row_3_[9] = {  0.036024,  0.236493,  0.036024, -2.152361,  1.239220,  1.271999, -6.767481,  1.205330, -0.773520};
	float *jaspar_core_pwm_102_matrix[4] = { jaspar_core_pwm_102_matrix_row_0_, jaspar_core_pwm_102_matrix_row_1_, jaspar_core_pwm_102_matrix_row_2_, jaspar_core_pwm_102_matrix_row_3_};
	PWM jaspar_core_pwm_102_ = {
/* accession        */ "MA0078",
/* name             */ "Sox17",
/* label            */ " MA0078	10.5018372361999	Sox17	HMG	; acc \"Q61473\" ; medline \"8636240\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"10.5030\" ; type \"SELEX\" ",
/* motif            */ "NNCATTGTC",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_102_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -43.022255,
/* max_score        */ 8.865038,
/* threshold        */ 0.832,
/* info content     */ 10.441796,
/* base_counts      */ {48, 53, 54, 124},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 31
};

	float jaspar_core_pwm_103_matrix_row_0_[7] = {  0.384740, -6.167768,  1.271204,  1.271204, -6.167768, -0.173807, -0.864463};
	float jaspar_core_pwm_103_matrix_row_1_[7] = { -5.926606,  0.067355, -5.926606, -5.926606, -5.926606, -0.623301, -1.311486};
	float jaspar_core_pwm_103_matrix_row_2_[7] = { -1.311486, -5.926606, -5.926606, -5.926606,  0.625902, -5.926606,  1.077368};
	float jaspar_core_pwm_103_matrix_row_3_[7] = {  0.635737,  1.003120, -6.167768, -6.167768,  0.740987,  0.836206, -0.460658};
	float *jaspar_core_pwm_103_matrix[4] = { jaspar_core_pwm_103_matrix_row_0_, jaspar_core_pwm_103_matrix_row_1_, jaspar_core_pwm_103_matrix_row_2_, jaspar_core_pwm_103_matrix_row_3_};
	PWM jaspar_core_pwm_103_ = {
/* accession        */ "MA0063",
/* name             */ "Nkx2-5",
/* label            */ " MA0063	8.26972545955295	Nkx2-5	HOMEO	; acc \"P42582\" ; medline \"7797561\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"8.2690\" ; type \"SELEX\" ",
/* motif            */ "TTAATTG",
/* library_name     */ "jaspar_core",
/* length           */ 7,
/* matrixname       */ jaspar_core_pwm_103_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -37.835770,
/* max_score        */ 6.835826,
/* threshold        */ 0.888,
/* info content     */ 8.181642,
/* base_counts      */ {47, 7, 19, 46},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_104_matrix_row_0_[11] = {  0.983066, -5.820439, -0.517134, -5.820439, -1.205318,  0.865422,  0.578156, -1.205318,  1.088316, -5.820439, -0.517134};
	float jaspar_core_pwm_104_matrix_row_1_[11] = { -5.579277, -0.275972, -5.579277, -5.579277, -5.579277, -5.579277, -5.579277,  1.224228, -5.579277, -0.964156,  1.106584};
	float jaspar_core_pwm_104_matrix_row_2_[11] = {  0.127833,  1.224228,  1.329478,  1.424697, -0.964156, -5.579277, -0.275972, -0.275972, -0.275972,  1.224228, -0.275972};
	float jaspar_core_pwm_104_matrix_row_3_[11] = { -5.820439, -1.205318, -5.820439, -1.205318,  1.088316,  0.173523,  0.173523, -5.820439, -5.820439, -0.517134, -5.820439};
	float *jaspar_core_pwm_104_matrix[4] = { jaspar_core_pwm_104_matrix_row_0_, jaspar_core_pwm_104_matrix_row_1_, jaspar_core_pwm_104_matrix_row_2_, jaspar_core_pwm_104_matrix_row_3_};
	PWM jaspar_core_pwm_104_ = {
/* accession        */ "MA0111",
/* name             */ "Spz1",
/* label            */ " MA0111	11.9065419262377	Spz1	bHLH-ZIP	; acc \"AAK15458\" ; medline \"11165476\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"11.9065419262377\" ; type \"SELEX\" ",
/* motif            */ "AGGGTAWCAGC",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_104_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -63.301338,
/* max_score        */ 12.136721,
/* threshold        */ 0.797,
/* info content     */ 11.741680,
/* base_counts      */ {39, 20, 51, 22},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12
};

	float jaspar_core_pwm_105_matrix_row_0_[9] = {  0.530125, -1.763509, -0.384668,  1.222772,  1.271539, -6.378630,  0.173878, -6.378630, -0.162024};
	float jaspar_core_pwm_105_matrix_row_1_[9] = { -0.143506, -0.143506, -0.834163, -6.137468, -6.137468,  1.512701,  0.079138, -6.137468, -0.143506};
	float jaspar_core_pwm_105_matrix_row_2_[9] = { -6.137468,  1.107474, -0.834163, -6.137468, -6.137468, -6.137468, -0.430358,  1.512701, -0.834163};
	float jaspar_core_pwm_105_matrix_row_3_[9] = {  0.173878, -1.075325,  0.792259, -1.763509, -6.378630, -6.378630,  0.019965, -6.378630,  0.530125};
	float *jaspar_core_pwm_105_matrix[4] = { jaspar_core_pwm_105_matrix_row_0_, jaspar_core_pwm_105_matrix_row_1_, jaspar_core_pwm_105_matrix_row_2_, jaspar_core_pwm_105_matrix_row_3_};
	PWM jaspar_core_pwm_105_ = {
/* accession        */ "MA0126",
/* name             */ "ovo",
/* label            */ " MA0126	9.60193933972294	ovo	ZN-FINGER, C2H2 	; acc \"P51521\" ; medline \"10637336\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"9.602\" ; type \"SELEX\" ",
/* motif            */ "WGTAACNGN",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_105_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -35.273018,
/* max_score        */ 8.653573,
/* threshold        */ 0.838,
/* info content     */ 9.524233,
/* base_counts      */ {68, 40, 42, 39},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_106_matrix_row_0_[6] = { -0.132362, -7.303251,  1.214142, -2.688130, -0.132362, -0.904656};
	float jaspar_core_pwm_106_matrix_row_1_[6] = {  0.182853, -7.062089, -1.758784, -7.062089, -0.663494,  0.316295};
	float jaspar_core_pwm_106_matrix_row_2_[6] = {  0.251798,  1.513562, -7.062089, -2.446968,  0.762357,  0.634578};
	float jaspar_core_pwm_106_matrix_row_3_[6] = { -0.299277, -7.303251, -2.688130,  1.233941, -0.499746, -0.499746};
	float *jaspar_core_pwm_106_matrix[4] = { jaspar_core_pwm_106_matrix_row_0_, jaspar_core_pwm_106_matrix_row_1_, jaspar_core_pwm_106_matrix_row_2_, jaspar_core_pwm_106_matrix_row_3_};
	PWM jaspar_core_pwm_106_ = {
/* accession        */ "MA0035",
/* name             */ "Gata1",
/* label            */ " MA0035	5.73662622978588	Gata1	ZN-FINGER, GATA	; acc \"P17679\" ; medline \"8321207\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"5.7370\" ; type \"SELEX\" ",
/* motif            */ "NGATNN",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_106_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -23.294855,
/* max_score        */ 5.610379,
/* threshold        */ 0.939,
/* info content     */ 5.719357,
/* base_counts      */ {83, 38, 116, 81},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 53
};

	float jaspar_core_pwm_107_matrix_row_0_[8] = { -0.113329,  0.396167, -5.820439, -5.820439,  1.270471, -1.205318, -0.517134, -1.205318};
	float jaspar_core_pwm_107_matrix_row_1_[8] = { -0.964156, -0.275972,  1.329478, -0.964156, -5.579277, -0.964156, -5.579277, -0.275972};
	float jaspar_core_pwm_107_matrix_row_2_[8] = { -0.964156, -0.964156, -5.579277, -5.579277, -5.579277, -0.275972, -0.964156, -0.964156};
	float jaspar_core_pwm_107_matrix_row_3_[8] = {  0.732069,  0.173523, -0.517134,  1.183535, -5.820439,  0.865422,  0.983066,  0.865422};
	float *jaspar_core_pwm_107_matrix[4] = { jaspar_core_pwm_107_matrix_row_0_, jaspar_core_pwm_107_matrix_row_1_, jaspar_core_pwm_107_matrix_row_2_, jaspar_core_pwm_107_matrix_row_3_};
	PWM jaspar_core_pwm_107_ = {
/* accession        */ "MA0011",
/* name             */ "Broad-complex_2",
/* label            */ " MA0011	7.72345967810759	Broad-complex_2	ZN-FINGER, C2H2	; acc \"CAA38476\" ; comment \"Different splice forms affacts DNA binding: four matrices\" ; medline \"8062827\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"7.7210\" ; type \"COMPILED\" ",
/* motif            */ "TWCTATTT",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_107_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -27.379543,
/* max_score        */ 7.625631,
/* threshold        */ 0.872,
/* info content     */ 7.629575,
/* base_counts      */ {24, 17, 6, 49},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12
};

	float jaspar_core_pwm_108_matrix_row_0_[6] = { -0.804813, -6.511924, -6.511924, -6.511924, -6.511924, -6.511924};
	float jaspar_core_pwm_108_matrix_row_1_[6] = {  0.415099, -6.270761,  1.470338, -6.270761, -6.270761, -6.270761};
	float jaspar_core_pwm_108_matrix_row_2_[6] = { -0.967457,  1.470338, -6.270761,  1.470338, -6.270761,  1.512879};
	float jaspar_core_pwm_108_matrix_row_3_[6] = {  0.492050, -1.896803, -1.896803, -1.896803,  1.271717, -6.511924};
	float *jaspar_core_pwm_108_matrix[4] = { jaspar_core_pwm_108_matrix_row_0_, jaspar_core_pwm_108_matrix_row_1_, jaspar_core_pwm_108_matrix_row_2_, jaspar_core_pwm_108_matrix_row_3_};
	PWM jaspar_core_pwm_108_ = {
/* accession        */ "MA0006",
/* name             */ "Arnt-Ahr",
/* label            */ " MA0006	9.53241709191153	Arnt-Ahr	bHLH	; acc \"P30561 , P53762\" ; comment \"dimer\" ; medline \"7592839\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"9.5320\" ; type \"SELEX\" ",
/* motif            */ "YGCGTG",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_108_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -33.527077,
/* max_score        */ 7.687659,
/* threshold        */ 0.892,
/* info content     */ 9.463607,
/* base_counts      */ {3, 31, 72, 38},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 24
};

	float jaspar_core_pwm_109_matrix_row_0_[17] = {  0.886292,  0.886292,  1.048723, -6.552679, -6.552679, -6.552679, -6.552679,  1.271767,  1.230961,  1.271767, -6.552679, -6.552679, -6.552679, -6.552679,  1.271767,  1.048723, -6.552679};
	float jaspar_core_pwm_109_matrix_row_1_[17] = { -0.094911, -1.696396, -6.311517, -6.311517, -6.311517, -6.311517,  1.512929, -6.311517, -6.311517, -6.311517, -6.311517, -6.311517, -6.311517,  1.512929, -6.311517, -1.696396,  1.002370};
	float jaspar_core_pwm_109_matrix_row_2_[17] = { -6.311517, -0.094911, -0.094911,  1.512929,  1.472124, -6.311517, -6.311517, -6.311517, -6.311517, -6.311517,  1.512929,  1.512929, -6.311517, -6.311517, -6.311517, -1.008212,  0.087078};
	float jaspar_core_pwm_109_matrix_row_3_[17] = { -0.845569, -1.249374, -6.552679, -6.552679, -1.937559,  1.271767, -6.552679, -6.552679, -1.937559, -6.552679, -6.552679, -6.552679,  1.271767, -6.552679, -6.552679, -1.249374, -0.558718};
	float *jaspar_core_pwm_109_matrix[4] = { jaspar_core_pwm_109_matrix_row_0_, jaspar_core_pwm_109_matrix_row_1_, jaspar_core_pwm_109_matrix_row_2_, jaspar_core_pwm_109_matrix_row_3_};
	PWM jaspar_core_pwm_109_ = {
/* accession        */ "MA0115",
/* name             */ "NR1H2-RXRA",
/* label            */ " MA0115	27.8780468034389	NR1H2-RXRA	NUCLEAR RECEPTOR	; acc \"P55055 , P19793\" ; comment \"heterodimer between NR1H2 and RXR\" ; medline \"10187832\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"27.8780\" ; type \"SELEX\" ",
/* motif            */ "AAAGGTCAAAGGTCAAC",
/* library_name     */ "jaspar_core",
/* length           */ 17,
/* matrixname       */ jaspar_core_pwm_109_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.200668,
/* max_score        */ 21.498964,
/* threshold        */ 0.856,
/* info content     */ 27.675919,
/* base_counts      */ {173, 72, 117, 63},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 25
};

	float jaspar_core_pwm_110_matrix_row_0_[6] = { -0.622825, -6.329935, -1.026630, -6.329935, -6.329935,  1.166162};
	float jaspar_core_pwm_110_matrix_row_1_[6] = {  0.127833, -6.088773, -6.088773, -6.088773, -6.088773, -6.088773};
	float jaspar_core_pwm_110_matrix_row_2_[6] = { -0.094811,  1.461363,  1.407325,  1.461363,  1.512630, -0.785468};
	float jaspar_core_pwm_110_matrix_row_3_[6] = {  0.355926, -1.714814, -6.329935, -1.714814, -6.329935, -6.329935};
	float *jaspar_core_pwm_110_matrix[4] = { jaspar_core_pwm_110_matrix_row_0_, jaspar_core_pwm_110_matrix_row_1_, jaspar_core_pwm_110_matrix_row_2_, jaspar_core_pwm_110_matrix_row_3_};
	PWM jaspar_core_pwm_110_ = {
/* accession        */ "MA0056",
/* name             */ "MZF1_1-4",
/* label            */ " MA0056	8.58551320253218	MZF1_1-4	ZN-FINGER, C2H2	; acc \"P28698\" ; comment \"\" ; medline \"8114711\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"8.5860\" ; type \"SELEX\" ",
/* motif            */ "NGGGGA",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_110_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -32.272499,
/* max_score        */ 7.364768,
/* threshold        */ 0.904,
/* info content     */ 8.512179,
/* base_counts      */ {23, 5, 82, 10},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_111_matrix_row_0_[10] = {  0.761208,  0.250826, -0.154084,  0.451295,  1.097490, -6.552679, -6.552679, -6.552679, -6.552679,  1.271767};
	float jaspar_core_pwm_111_matrix_row_1_[10] = { -1.696396, -1.696396,  0.779393, -1.008212, -6.311517, -6.311517, -6.311517, -6.311517,  1.512929, -6.311517};
	float jaspar_core_pwm_111_matrix_row_2_[10] = { -1.008212, -6.311517, -0.317556, -0.094911, -0.317556,  1.512929,  1.512929, -6.311517, -6.311517, -6.311517};
	float jaspar_core_pwm_111_matrix_row_3_[10] = { -0.000171,  0.761208, -0.845569, -0.000171, -6.552679, -6.552679, -6.552679,  1.271767, -6.552679, -6.552679};
	float *jaspar_core_pwm_111_matrix[4] = { jaspar_core_pwm_111_matrix_row_0_, jaspar_core_pwm_111_matrix_row_1_, jaspar_core_pwm_111_matrix_row_2_, jaspar_core_pwm_111_matrix_row_3_};
	PWM jaspar_core_pwm_111_ = {
/* accession        */ "MA0071",
/* name             */ "RORA_1",
/* label            */ " MA0071	13.1897301896459	RORA_1	NUCLEAR RECEPTOR	; acc \"NP_599023\" ; comment \"isoform type \"a\" with different DNA-binding specificty. Alternative model exist for isoform \"b\" in MA0072\" ; medline \"7926749\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"13.1920\" ; type \"SELEX\" ",
/* motif            */ "ATNNAGGTCA",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_111_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -49.177773,
/* max_score        */ 10.932914,
/* threshold        */ 0.804,
/* info content     */ 13.093750,
/* base_counts      */ {87, 41, 65, 57},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 25
};

	float jaspar_core_pwm_112_matrix_row_0_[10] = { -7.224841, -0.133931, -7.224841, -7.224841, -2.609720, -7.224841, -1.008235, -1.517731, -1.517731, -1.230879};
	float jaspar_core_pwm_112_matrix_row_1_[10] = {  1.513516, -6.983679,  0.617724,  0.757420, -1.276569,  1.428376,  0.954053,  0.840767,  1.055801,  0.879973};
	float jaspar_core_pwm_112_matrix_row_2_[10] = { -6.983679,  1.232679,  0.989132, -2.368558,  1.428376, -0.989717, -0.585084,  0.020295, -0.767073, -0.431171};
	float jaspar_core_pwm_112_matrix_row_3_[10] = { -7.224841, -7.224841, -7.224841,  0.599605, -7.224841, -7.224841, -0.316086, -0.316086, -0.316086, -0.133931};
	float *jaspar_core_pwm_112_matrix[4] = { jaspar_core_pwm_112_matrix_row_0_, jaspar_core_pwm_112_matrix_row_1_, jaspar_core_pwm_112_matrix_row_2_, jaspar_core_pwm_112_matrix_row_3_};
	PWM jaspar_core_pwm_112_ = {
/* accession        */ "MA0123",
/* name             */ "ABI4",
/* label            */ " MA0123	9.74035932391708	ABI4	AP2	; acc \"Q8L7W9\" ; medline \"12368505\" ; species \"Zea mays\" ; sysgroup \"plant\" ; total_ic \"9.74035932391708\" ; type \"SELEX\" ",
/* motif            */ "CGGTGCCCCC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_112_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.623623,
/* max_score        */ 11.080094,
/* threshold        */ 0.820,
/* info content     */ 9.704910,
/* base_counts      */ {28, 250, 145, 67},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_core_pwm_113_matrix_row_0_[10] = {  0.109715, -3.099611,  1.194639, -7.714731,  1.247276, -3.099611,  0.604255, -2.411426,  0.860920, -0.623821};
	float jaspar_core_pwm_113_matrix_row_1_[10] = { -0.302680, -2.170264, -2.170264, -0.670064, -2.170264, -1.479607, -2.170264, -0.670064, -1.766459,  0.499242};
	float jaspar_core_pwm_113_matrix_row_2_[10] = {  0.820731, -2.858449, -1.479607, -1.479607, -7.473569, -2.858449,  0.715398, -2.858449,  0.127833, -0.159682};
	float jaspar_core_pwm_113_matrix_row_3_[10] = { -2.411426,  1.221304, -7.714731,  1.095281, -7.714731,  1.194639, -3.099611,  1.110094, -1.720770,  0.068910};
	float *jaspar_core_pwm_113_matrix[4] = { jaspar_core_pwm_113_matrix_row_0_, jaspar_core_pwm_113_matrix_row_1_, jaspar_core_pwm_113_matrix_row_2_, jaspar_core_pwm_113_matrix_row_3_};
	PWM jaspar_core_pwm_113_ = {
/* accession        */ "MA0015",
/* name             */ "CF2-II",
/* label            */ " MA0015	10.9770931714416	CF2-II	ZN-FINGER, C2H2	; acc \"P20385\" ; medline \"1290524\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"10.9780\" ; type \"SELEX\" ",
/* motif            */ "RTATATATAN",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_113_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -40.103180,
/* max_score        */ 9.959524,
/* threshold        */ 0.814,
/* info content     */ 10.959745,
/* base_counts      */ {287, 75, 122, 316},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 80
};

	float jaspar_core_pwm_114_matrix_row_0_[8] = { -0.845569,  1.097490,  1.271767, -6.552679, -6.552679,  1.230961, -1.937559, -6.552679};
	float jaspar_core_pwm_114_matrix_row_1_[8] = {  0.859371, -1.696396, -6.311517, -6.311517, -0.094911, -6.311517, -6.311517, -6.311517};
	float jaspar_core_pwm_114_matrix_row_2_[8] = { -0.317556, -6.311517, -6.311517, -6.311517, -6.311517, -1.696396, -6.311517, -1.008212};
	float jaspar_core_pwm_114_matrix_row_3_[8] = { -0.336073, -0.845569, -6.552679,  1.271767,  1.048723, -6.552679,  1.230961,  1.188420};
	float *jaspar_core_pwm_114_matrix[4] = { jaspar_core_pwm_114_matrix_row_0_, jaspar_core_pwm_114_matrix_row_1_, jaspar_core_pwm_114_matrix_row_2_, jaspar_core_pwm_114_matrix_row_3_};
	PWM jaspar_core_pwm_114_ = {
/* accession        */ "MA0008",
/* name             */ "Athb-1",
/* label            */ " MA0008	11.8821478649142	Athb-1	HOMEO-ZIP	; acc \"Q02283\" ; medline \"8253077\" ; species \"Arabidopsis thaliana\" ; sysgroup \"plant\" ; total_ic \"11.8810\" ; type \"SELEX\" ",
/* motif            */ "CAATTATT",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_114_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.231995,
/* max_score        */ 9.199461,
/* threshold        */ 0.837,
/* info content     */ 11.799393,
/* base_counts      */ {74, 19, 7, 100},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 25
};

	float jaspar_core_pwm_115_matrix_row_0_[9] = { -0.449230,  0.020024,  0.648050,  0.830261,  1.158609, -6.665837,  1.271895,  1.235541, -0.113329};
	float jaspar_core_pwm_115_matrix_row_1_[9] = { -0.208068, -0.717564, -6.424675, -0.717564, -6.424675,  1.438977, -6.424675, -6.424675, -6.424675};
	float jaspar_core_pwm_115_matrix_row_2_[9] = {  0.484080, -0.430713, -0.717564, -0.717564, -6.424675, -6.424675, -6.424675, -1.809554, -1.121370};
	float jaspar_core_pwm_115_matrix_row_3_[9] = {  0.020024,  0.505052,  0.242918, -0.671875, -0.958726, -1.362532, -6.665837, -6.665837,  0.884299};
	float *jaspar_core_pwm_115_matrix[4] = { jaspar_core_pwm_115_matrix_row_0_, jaspar_core_pwm_115_matrix_row_1_, jaspar_core_pwm_115_matrix_row_2_, jaspar_core_pwm_115_matrix_row_3_};
	PWM jaspar_core_pwm_115_ = {
/* accession        */ "MA0084",
/* name             */ "SRY",
/* label            */ " MA0084	9.193014653455	SRY	HMG	; acc \"Q05066\" ; medline \"8190643\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"9.1930\" ; type \"SELEX\" ",
/* motif            */ "NWAAACAAT",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_115_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -41.155891,
/* max_score        */ 8.456764,
/* threshold        */ 0.841,
/* info content     */ 9.136725,
/* base_counts      */ {133, 37, 23, 59},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 28
};

	float jaspar_core_pwm_116_matrix_row_0_[15] = { -0.579679, -1.917503,  1.172943, -3.588776,  1.178609,  0.900308,  1.195415,  0.712006,  0.352773, -0.665186, -0.271755, -0.283875, -0.283875, -0.471062, -0.346781};
	float jaspar_core_pwm_116_matrix_row_1_[15] = {  0.527248, -0.620696, -9.054725, -2.145970, -9.054725, -9.054725, -3.347614, -3.751420, -0.665138,  0.455794,  0.540946,  0.394711,  0.321215,  0.223368,  0.165665};
	float jaspar_core_pwm_116_matrix_row_2_[15] = {  0.574392, -1.558627, -3.751420, -3.751420, -2.838119, -9.054725, -2.145970, -0.665138,  0.606755,  0.561147,  0.402554,  0.402554,  0.402554,  0.484991,  0.492159};
	float jaspar_core_pwm_116_matrix_row_3_[15] = { -1.256407,  1.042657, -1.135083,  1.233566, -1.289186,  0.105157, -2.897292,  0.105157, -1.193906, -0.819307, -1.256407, -0.739281, -0.579679, -0.373095, -0.427896};
	float *jaspar_core_pwm_116_matrix[4] = { jaspar_core_pwm_116_matrix_row_0_, jaspar_core_pwm_116_matrix_row_1_, jaspar_core_pwm_116_matrix_row_2_, jaspar_core_pwm_116_matrix_row_3_};
	PWM jaspar_core_pwm_116_ = {
/* accession        */ "MA0108",
/* name             */ "TBP",
/* label            */ " MA0108	10.1980738528127	TBP	TATA-box	; medline \"2329577\" ; species \"-\" ; sysgroup \"-\" ; total_ic \"10.1981 \" ",
/* motif            */ "STATAAAARNNNNNN",
/* library_name     */ "jaspar_core",
/* length           */ 15,
/* matrixname       */ jaspar_core_pwm_116_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.676067,
/* max_score        */ 11.501001,
/* threshold        */ 0.735,
/* info content     */ 10.166988,
/* base_counts      */ {2239, 985, 1203, 1398},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 389
};

	float jaspar_core_pwm_117_matrix_row_0_[12] = { -1.896803, -0.804813, -1.896803, -6.511924, -6.511924, -1.208619, -0.804813, -1.896803,  0.173937, -6.511924, -6.511924, -1.208619};
	float jaspar_core_pwm_117_matrix_row_1_[12] = { -1.655641, -0.563651, -0.967457,  0.532744,  0.900127,  0.733213,  0.415099, -1.655641, -0.054155,  0.127833,  1.168210, -6.270761};
	float jaspar_core_pwm_117_matrix_row_2_[12] = { -0.967457, -0.276800,  0.532744, -0.563651, -0.276800, -0.276800, -0.967457, -1.655641, -1.655641,  0.281746, -6.270761,  1.225336};
	float jaspar_core_pwm_117_matrix_row_3_[12] = {  1.089479,  0.733018,  0.578986,  0.578986,  0.040584,  0.040584,  0.492050,  1.138245,  0.396831,  0.492050,  0.040584, -0.517962};
	float *jaspar_core_pwm_117_matrix[4] = { jaspar_core_pwm_117_matrix_row_0_, jaspar_core_pwm_117_matrix_row_1_, jaspar_core_pwm_117_matrix_row_2_, jaspar_core_pwm_117_matrix_row_3_};
	PWM jaspar_core_pwm_117_ = {
/* accession        */ "MA0120",
/* name             */ "id1",
/* label            */ " MA0120	7.72439203551961	id1	ZN-FINGER, C2H2	; acc \"AAC18941\" ; medline \"15020707\" ; species \"Zea mays\" ; sysgroup \"plant\" ; total_ic \"7.72439203551961\" ; type \"SELEX\" ",
/* motif            */ "TTKYCYYTWKCG",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_117_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -42.645401,
/* max_score        */ 9.526532,
/* threshold        */ 0.786,
/* info content     */ 7.678516,
/* base_counts      */ {21, 76, 55, 136},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 24
};

	float jaspar_core_pwm_118_matrix_row_0_[11] = {  0.173878,  0.935257, -1.075325, -1.763509,  1.271539, -6.378630, -1.763509, -6.378630, -6.378630, -1.763509, -0.671520};
	float jaspar_core_pwm_118_matrix_row_1_[11] = { -1.522347, -1.522347,  0.666037,  1.463934, -6.137468,  1.463934, -6.137468, -1.522347, -6.137468, -1.522347,  0.079138};
	float jaspar_core_pwm_118_matrix_row_2_[11] = {  0.666037, -0.143506,  0.666037, -6.137468, -6.137468, -6.137468,  1.463934, -6.137468,  1.512701,  1.358629, -6.137468};
	float jaspar_core_pwm_118_matrix_row_3_[11] = { -0.384668, -1.763509, -1.763509, -6.378630, -6.378630, -1.763509, -6.378630,  1.222772, -6.378630, -1.763509,  0.792259};
	float *jaspar_core_pwm_118_matrix[4] = { jaspar_core_pwm_118_matrix_row_0_, jaspar_core_pwm_118_matrix_row_1_, jaspar_core_pwm_118_matrix_row_2_, jaspar_core_pwm_118_matrix_row_3_};
	PWM jaspar_core_pwm_118_ = {
/* accession        */ "MA0059",
/* name             */ "MYC-MAX",
/* label            */ " MA0059	14.2370460691273	MYC-MAX	bHLH-ZIP	; acc \"AAH36092 , Q6LBK7\" ; comment \"Heterodimer of MYC and MAX \" ; medline \"8265351\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"14.2360\" ; type \"SELEX\" ",
/* motif            */ "RASCACGTGGT",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_118_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -51.222122,
/* max_score        */ 12.817035,
/* threshold        */ 0.790,
/* info content     */ 14.130392,
/* base_counts      */ {51, 58, 81, 41},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_core_pwm_119_matrix_row_0_[10] = { -6.970843, -6.970843, -2.355722,  0.853603,  0.579293, -0.418335, -2.355722, -1.667538, -1.667538, -6.970843};
	float jaspar_core_pwm_119_matrix_row_1_[10] = { -6.729681, -6.729681, -6.729681, -6.729681,  0.441208, -2.114560, -1.426376,  0.709291,  1.431123,  1.459286};
	float jaspar_core_pwm_119_matrix_row_2_[10] = {  1.513339,  1.513339,  1.486678,  0.441208, -2.114560, -1.022570, -1.426376, -6.729681, -6.729681, -6.729681};
	float jaspar_core_pwm_119_matrix_row_3_[10] = { -6.970843, -6.970843, -6.970843, -6.970843, -0.754237,  0.930535,  1.131138,  0.579293, -2.355722, -1.667538};
	float *jaspar_core_pwm_119_matrix[4] = { jaspar_core_pwm_119_matrix_row_0_, jaspar_core_pwm_119_matrix_row_1_, jaspar_core_pwm_119_matrix_row_2_, jaspar_core_pwm_119_matrix_row_3_};
	PWM jaspar_core_pwm_119_ = {
/* accession        */ "MA0061",
/* name             */ "NF-kappaB",
/* label            */ " MA0061	13.3447617958635	NF-kappaB	REL	; medline \"8449662\" ; species \"-\" ; sysgroup \"vertebrate\" ; total_ic \"13.3440\" ; type \"COMPILED\" ",
/* motif            */ "GGGAMTTYCC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_119_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -54.898415,
/* max_score        */ 11.607622,
/* threshold        */ 0.803,
/* info content     */ 13.285789,
/* base_counts      */ {57, 104, 132, 87},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 38
};

	float jaspar_core_pwm_120_matrix_row_0_[14] = { -5.900226, -1.285106,  1.190684,  0.498369, -5.900226, -5.900226, -5.900226, -1.285106, -0.596921,  0.498369,  0.498369, -1.285106, -0.193116, -5.900226};
	float jaspar_core_pwm_120_matrix_row_1_[14] = { -5.659064, -5.659064, -5.659064,  0.893444,  1.511824,  0.048046, -0.355759, -5.659064, -5.659064,  0.334897,  0.557542,  1.249691,  0.739531,  0.048046};
	float jaspar_core_pwm_120_matrix_row_2_[14] = { -0.355759,  1.431846, -1.043944, -5.659064, -5.659064, -5.659064, -5.659064, -5.659064,  1.344910,  0.048046, -1.043944, -1.043944, -5.659064,  0.048046};
	float jaspar_core_pwm_120_matrix_row_3_[14] = {  1.103748, -5.900226, -5.900226, -5.900226, -5.900226,  1.008529,  1.103748,  1.190684, -5.900226, -5.900226, -1.285106, -1.285106,  0.093735,  0.652282};
	float *jaspar_core_pwm_120_matrix[4] = { jaspar_core_pwm_120_matrix_row_0_, jaspar_core_pwm_120_matrix_row_1_, jaspar_core_pwm_120_matrix_row_2_, jaspar_core_pwm_120_matrix_row_3_};
	PWM jaspar_core_pwm_120_ = {
/* accession        */ "MA0017",
/* name             */ "NR2F1",
/* label            */ " MA0017	15.9238684526357	NR2F1	NUCLEAR RECEPTOR	; acc \"P10589\" ; medline \"8496174\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"15.9250\" ; type \"COMPILED\" ",
/* motif            */ "TGACCTTTGMMCYT",
/* library_name     */ "jaspar_core",
/* length           */ 14,
/* matrixname       */ jaspar_core_pwm_120_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.890602,
/* max_score        */ 14.476831,
/* threshold        */ 0.768,
/* info content     */ 15.715148,
/* base_counts      */ {38, 53, 34, 57},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 13
};

	float jaspar_core_pwm_121_matrix_row_0_[6] = {  0.445218, -0.400180, -6.107290, -6.107290,  1.271093, -6.107290};
	float jaspar_core_pwm_121_matrix_row_1_[6] = {  0.532467, -0.562823,  1.512256,  1.512256, -5.866128,  1.447759};
	float jaspar_core_pwm_121_matrix_row_2_[6] = { -0.159018, -5.866128, -5.866128, -5.866128, -5.866128, -1.251008};
	float jaspar_core_pwm_121_matrix_row_3_[6] = { -6.107290,  0.896684, -6.107290, -6.107290, -6.107290, -6.107290};
	float *jaspar_core_pwm_121_matrix[4] = { jaspar_core_pwm_121_matrix_row_0_, jaspar_core_pwm_121_matrix_row_1_, jaspar_core_pwm_121_matrix_row_2_, jaspar_core_pwm_121_matrix_row_3_};
	PWM jaspar_core_pwm_121_ = {
/* accession        */ "MA0130",
/* name             */ "ZNF354C",
/* label            */ " MA0130	8.9580088251372	ZNF354C	ZN-FINGER, C2H2 	; acc \"Q86Y25\" ; medline \"15555547\" ; species \"Homo sapiens\" ; sysgroup \"Vertebrate\" ; total_ic \"8.958\" ; type \"SELEX\" ",
/* motif            */ "MTCCAC",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_121_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -36.402580,
/* max_score        */ 7.172514,
/* threshold        */ 0.900,
/* info content     */ 8.858225,
/* base_counts      */ {26, 55, 4, 11},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_122_matrix_row_0_[6] = {  0.348472, -7.475974,  1.240234, -7.475974,  0.793014, -0.162087};
	float jaspar_core_pwm_122_matrix_row_1_[6] = {  0.010130, -2.619691, -7.234812, -7.234812, -2.619691, -1.527702};
	float jaspar_core_pwm_122_matrix_row_2_[6] = { -1.240850,  1.497654, -2.619691, -1.018206, -1.240850,  0.981546};
	float jaspar_core_pwm_122_matrix_row_3_[6] = {  0.125428, -7.475974, -2.860853,  1.189812,  0.074161, -0.790113};
	float *jaspar_core_pwm_122_matrix[4] = { jaspar_core_pwm_122_matrix_row_0_, jaspar_core_pwm_122_matrix_row_1_, jaspar_core_pwm_122_matrix_row_2_, jaspar_core_pwm_122_matrix_row_3_};
	PWM jaspar_core_pwm_122_ = {
/* accession        */ "MA0037",
/* name             */ "GATA3",
/* label            */ " MA0037	6.62989113890047	GATA3	ZN-FINGER, GATA	; acc \"P23771\" ; medline \"8321207\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"6.6310\" ; type \"SELEX\" ",
/* motif            */ "NGATAG",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_122_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -27.575005,
/* max_score        */ 6.050732,
/* threshold        */ 0.928,
/* info content     */ 6.613662,
/* base_counts      */ {140, 19, 113, 106},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 63
};

	float jaspar_core_pwm_123_matrix_row_0_[6] = { -0.335973,  1.220201, -6.329935, -6.329935, -6.329935, -6.329935};
	float jaspar_core_pwm_123_matrix_row_1_[6] = {  1.289611, -6.088773,  1.512630, -6.088773, -6.088773, -6.088773};
	float jaspar_core_pwm_123_matrix_row_2_[6] = { -6.088773, -1.473652, -6.088773,  1.512630, -6.088773,  1.512630};
	float jaspar_core_pwm_123_matrix_row_3_[6] = { -6.329935, -6.329935, -6.329935, -6.329935,  1.271468, -6.329935};
	float *jaspar_core_pwm_123_matrix[4] = { jaspar_core_pwm_123_matrix_row_0_, jaspar_core_pwm_123_matrix_row_1_, jaspar_core_pwm_123_matrix_row_2_, jaspar_core_pwm_123_matrix_row_3_};
	PWM jaspar_core_pwm_123_ = {
/* accession        */ "MA0004",
/* name             */ "Arnt",
/* label            */ " MA0004	10.9916749479967	Arnt	bHLH	; acc \"P53762\" ; medline \"7592839\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"10.9920\" ; type \"SELEX\" ",
/* motif            */ "CACGTG",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_123_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -37.979610,
/* max_score        */ 8.319168,
/* threshold        */ 0.875,
/* info content     */ 10.891092,
/* base_counts      */ {23, 36, 41, 20},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 20
};

	float jaspar_core_pwm_124_matrix_row_0_[12] = {  0.230827,  0.836206,  0.518093, -6.167768,  0.836206, -6.167768, -6.167768, -6.167768,  0.635737, -6.167768, -1.552648, -6.167768};
	float jaspar_core_pwm_124_matrix_row_1_[12] = {  0.625902, -0.219496, -0.219496, -1.311486, -5.926606, -1.311486, -1.311486, -5.926606, -5.926606,  0.876899, -1.311486,  0.290000};
	float jaspar_core_pwm_124_matrix_row_2_[12] = { -0.219496, -1.311486, -1.311486, -5.926606,  0.471989, -5.926606, -5.926606,  0.471989,  0.759255, -5.926606, -1.311486, -5.926606};
	float jaspar_core_pwm_124_matrix_row_3_[12] = { -1.552648, -0.864463,  0.048838,  1.210616, -6.167768,  1.210616,  1.210616,  0.836206, -6.167768,  0.518093,  1.077173,  0.923142};
	float *jaspar_core_pwm_124_matrix[4] = { jaspar_core_pwm_124_matrix_row_0_, jaspar_core_pwm_124_matrix_row_1_, jaspar_core_pwm_124_matrix_row_2_, jaspar_core_pwm_124_matrix_row_3_};
	PWM jaspar_core_pwm_124_ = {
/* accession        */ "MA0047",
/* name             */ "Foxa2",
/* label            */ " MA0047	12.4332203333019	Foxa2	FORKHEAD	; acc \"P32182\" ; medline \"8139574\" ; species \"Rattus norvegicus\" ; sysgroup \"vertebrate\" ; total_ic \"12.4370\" ; type \"COMPILED\" ",
/* motif            */ "MAWTATTTACTT",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_124_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -55.070404,
/* max_score        */ 10.920929,
/* threshold        */ 0.783,
/* info content     */ 12.308082,
/* base_counts      */ {46, 31, 26, 101},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 17
};

	float jaspar_core_pwm_125_matrix_row_0_[16] = { -1.023661, -5.638782, -5.638782, -5.638782, -5.638782,  0.068329, -5.638782,  1.269973,  1.269973,  0.577825, -1.023661, -1.023661,  0.355180,  0.068329,  0.355180, -0.335477};
	float jaspar_core_pwm_125_matrix_row_1_[16] = {  0.309491,  0.596342, -5.397620, -5.397620, -5.397620, -5.397620, -5.397620, -5.397620, -5.397620,  0.818987,  1.154888, -0.094315, -0.094315, -0.782499,  0.309491, -0.782499};
	float jaspar_core_pwm_125_matrix_row_2_[16] = {  0.309491, -5.397620,  1.511135, -5.397620,  1.511135,  1.154888,  1.511135, -5.397620, -5.397620, -5.397620, -0.782499,  0.596342, -0.094315,  0.596342, -0.094315, -0.094315};
	float jaspar_core_pwm_125_matrix_row_3_[16] = {  0.068329,  0.759813, -5.638782,  1.269973, -5.638782, -5.638782, -5.638782, -5.638782, -5.638782, -5.638782, -1.023661,  0.068329, -0.335477, -0.335477, -1.023661,  0.577825};
	float *jaspar_core_pwm_125_matrix[4] = { jaspar_core_pwm_125_matrix_row_0_, jaspar_core_pwm_125_matrix_row_1_, jaspar_core_pwm_125_matrix_row_2_, jaspar_core_pwm_125_matrix_row_3_};
	PWM jaspar_core_pwm_125_ = {
/* accession        */ "MA0085",
/* name             */ "Su(H)",
/* label            */ " MA0085	16.6733068362852	Su(H)	IPT/TIG domain	; acc \"P28159\" ; medline \"7590239\" ; species \"Drosophila melanogaster\" ; sysgroup \"insect\" ; total_ic \"16.6710\" ; type \"COMPILED\" ",
/* motif            */ "NTGTGGGAAMCNNNNN",
/* library_name     */ "jaspar_core",
/* length           */ 16,
/* matrixname       */ jaspar_core_pwm_125_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -56.744160,
/* max_score        */ 15.022260,
/* threshold        */ 0.761,
/* info content     */ 16.393187,
/* base_counts      */ {44, 28, 55, 33},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 10
};

	float jaspar_core_pwm_126_matrix_row_0_[20] = { -0.137997, -0.494244,  0.503384,  0.124137,  0.392220, -7.046752, -7.046752, -7.046752, -1.743447,  1.272234,  1.272234,  1.222236, -7.046752, -7.046752, -7.046752, -1.743447,  1.196268, -0.137997,  0.124137, -0.494244};
	float jaspar_core_pwm_126_matrix_row_1_[20] = {  0.285320,  0.365299, -0.811628, -2.190469, -6.805590, -6.805590, -6.805590, -1.502285,  1.410769, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590, -1.502285,  1.383377, -2.190469,  0.285320,  0.508297,  0.103165};
	float jaspar_core_pwm_126_matrix_row_2_[20] = { -0.253082, -0.119729, -0.406995,  1.095788,  0.978051,  1.513396,  1.463398, -6.805590, -1.502285, -6.805590, -6.805590, -1.502285,  1.513396,  1.463398, -6.805590, -2.190469, -1.502285, -0.811628, -0.002084,  0.285320};
	float jaspar_core_pwm_126_matrix_row_3_[20] = {  0.044158,  0.124137,  0.044158, -7.046752, -7.046752, -7.046752, -1.743447,  1.222236, -7.046752, -7.046752, -7.046752, -7.046752, -7.046752, -1.743447,  1.222236, -1.743447, -7.046752,  0.267135, -1.052790,  0.044158};
	float *jaspar_core_pwm_126_matrix[4] = { jaspar_core_pwm_126_matrix_row_0_, jaspar_core_pwm_126_matrix_row_1_, jaspar_core_pwm_126_matrix_row_2_, jaspar_core_pwm_126_matrix_row_3_};
	PWM jaspar_core_pwm_126_ = {
/* accession        */ "MA0065",
/* name             */ "PPARG-RXRA",
/* label            */ " MA0065	23.4488895332387	PPARG-RXRA	NUCLEAR RECEPTOR	; acc \"P37231 , P19793\" ; comment \"Heterodimer between PPARG and RXRA\" ; medline \"11139380\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"23.4450\" ; type \"SELEX\" ",
/* motif            */ "NNWGGGGTCAAAGGTCANNN",
/* library_name     */ "jaspar_core",
/* length           */ 20,
/* matrixname       */ jaspar_core_pwm_126_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.715866,
/* max_score        */ 20.461960,
/* threshold        */ 0.844,
/* info content     */ 23.348431,
/* base_counts      */ {259, 145, 264, 152},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 41
};

	float jaspar_core_pwm_127_matrix_row_0_[11] = { -1.718779, -2.406963,  1.272216, -7.022083, -7.022083, -7.022083, -7.022083, -7.022083, -2.406963,  1.272216,  1.017396};
	float jaspar_core_pwm_127_matrix_row_1_[11] = {  1.156810, -2.165801, -6.780921, -6.780921, -6.780921, -6.780921, -6.780921, -1.477617, -0.228414, -6.780921, -0.564315};
	float jaspar_core_pwm_127_matrix_row_2_[11] = { -0.095060, -6.780921, -6.780921,  1.513378,  1.513378, -6.780921,  1.513378, -6.780921,  1.156810, -6.780921, -6.780921};
	float jaspar_core_pwm_127_matrix_row_3_[11] = { -1.718779,  1.220936, -7.022083, -7.022083, -7.022083,  1.272216, -7.022083,  1.220936, -1.028122, -7.022083, -1.028122};
	float *jaspar_core_pwm_127_matrix[4] = { jaspar_core_pwm_127_matrix_row_0_, jaspar_core_pwm_127_matrix_row_1_, jaspar_core_pwm_127_matrix_row_2_, jaspar_core_pwm_127_matrix_row_3_};
	PWM jaspar_core_pwm_127_ = {
/* accession        */ "MA0009",
/* name             */ "T",
/* label            */ " MA0009	17.8627489557575	T	T-BOX	; acc \"P20293\" ; medline \"8344258\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"17.8630\" ; type \"SELEX\" ",
/* motif            */ "CTAGGTGTGAA",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_127_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.842171,
/* max_score        */ 14.129671,
/* threshold        */ 0.780,
/* info content     */ 17.781982,
/* base_counts      */ {115, 43, 156, 126},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 40
};

	float jaspar_core_pwm_128_matrix_row_0_[10] = { -0.706917,  0.207876, -1.397574, -6.700879, -6.700879, -6.700879, -6.700879,  0.102627,  0.677505, -0.484273};
	float jaspar_core_pwm_128_matrix_row_1_[10] = {  0.226144, -6.459717, -1.156412,  1.478015, -6.459717, -6.459717, -0.752606,  0.785225, -6.459717, -0.465755};
	float jaspar_core_pwm_128_matrix_row_2_[10] = {  0.449038,  0.854170, -1.844596, -6.459717, -6.459717,  1.513094,  1.364729, -1.844596, -0.752606, -0.465755};
	float jaspar_core_pwm_128_matrix_row_3_[10] = { -0.148371, -0.706917,  1.082762, -2.085758,  1.271932, -6.700879, -2.085758, -0.484273,  0.207876,  0.677505};
	float *jaspar_core_pwm_128_matrix[4] = { jaspar_core_pwm_128_matrix_row_0_, jaspar_core_pwm_128_matrix_row_1_, jaspar_core_pwm_128_matrix_row_2_, jaspar_core_pwm_128_matrix_row_3_};
	PWM jaspar_core_pwm_128_ = {
/* accession        */ "MA0092",
/* name             */ "Hand1-Tcfe2a",
/* label            */ " MA0092	10.1435823386149	Hand1-Tcfe2a	bHLH	; acc \"Q64279 , P15806\" ; medline \"7791788\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"10.1450\" ; type \"SELEX\" ",
/* motif            */ "NGTCTGGMAT",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_128_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -44.603329,
/* max_score        */ 10.153975,
/* threshold        */ 0.818,
/* info content     */ 10.085868,
/* base_counts      */ {46, 59, 88, 97},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 29
};

	float jaspar_core_pwm_129_matrix_row_0_[8] = { -0.113329, -0.803985, -0.113329,  0.291305,  0.445218, -6.107290, -6.107290,  1.271093};
	float jaspar_core_pwm_129_matrix_row_1_[8] = {  0.127833,  0.350478,  0.127833,  0.127833, -0.159018, -5.866128, -5.866128, -5.866128};
	float jaspar_core_pwm_129_matrix_row_2_[8] = {  0.350478,  0.532467, -0.159018,  0.350478, -0.562823,  1.512256, -5.866128, -5.866128};
	float jaspar_core_pwm_129_matrix_row_3_[8] = { -0.400180, -0.400180,  0.109316, -1.492170, -0.113329, -6.107290,  1.271093, -6.107290};
	float *jaspar_core_pwm_129_matrix[4] = { jaspar_core_pwm_129_matrix_row_0_, jaspar_core_pwm_129_matrix_row_1_, jaspar_core_pwm_129_matrix_row_2_, jaspar_core_pwm_129_matrix_row_3_};
	PWM jaspar_core_pwm_129_ = {
/* accession        */ "MA0032",
/* name             */ "FOXC1",
/* label            */ " MA0032	6.50807164809114	FOXC1	FORKHEAD	; acc \"Q12948\" ; medline \"7957066\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"6.5100\" ; type \"SELEX\" ",
/* motif            */ "NNNNNGTA",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_129_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -21.740047,
/* max_score        */ 5.860916,
/* threshold        */ 0.882,
/* info content     */ 6.437486,
/* base_counts      */ {39, 20, 37, 32},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 16
};

	float jaspar_core_pwm_130_matrix_row_0_[10] = {  0.271257,  0.877140,  0.781860,  0.516258, -0.053952,  0.376562,  0.964126,  0.846378, -0.053952, -1.921536};
	float jaspar_core_pwm_130_matrix_row_1_[10] = { -0.180173, -6.983679, -1.276569, -2.368558, -0.585084, -0.989717, -0.989717, -1.680374, -2.368558, -2.368558};
	float jaspar_core_pwm_130_matrix_row_2_[10] = { -1.680374,  0.187210,  0.330208,  0.566457,  0.989132,  0.757420, -0.767073,  0.187210,  1.118302,  1.450350};
	float jaspar_core_pwm_130_matrix_row_3_[10] = {  0.376562, -1.517731, -2.609720, -0.826246, -2.609720, -1.921536, -1.230879, -1.921536, -1.921536, -7.224841};
	float *jaspar_core_pwm_130_matrix[4] = { jaspar_core_pwm_130_matrix_row_0_, jaspar_core_pwm_130_matrix_row_1_, jaspar_core_pwm_130_matrix_row_2_, jaspar_core_pwm_130_matrix_row_3_};
	PWM jaspar_core_pwm_130_ = {
/* accession        */ "MA0039",
/* name             */ "Klf4",
/* label            */ " MA0039	7.23339319085871	Klf4	ZN-FINGER, C2H2	; acc \"Q60793\" ; medline \"9443972\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"7.2340\" ; type \"SELEX\" ",
/* motif            */ "WAARGRAAGG",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_130_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -30.919401,
/* max_score        */ 8.727727,
/* threshold        */ 0.831,
/* info content     */ 7.216378,
/* base_counts      */ {220, 31, 198, 41},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_core_pwm_131_matrix_row_0_[8] = { -0.578953,  0.431853, -1.961509,  0.113570, -2.649694, -1.961509, -7.264814, -0.866219};
	float jaspar_core_pwm_131_matrix_row_1_[8] = { -1.029691, -2.408532,  1.452927,  0.415319, -7.023652, -2.408532, -7.023652, -7.023652};
	float jaspar_core_pwm_131_matrix_row_2_[8] = {  1.108173,  0.800794, -2.408532,  0.354732,  1.493741, -7.023652, -7.023652,  1.365935};
	float jaspar_core_pwm_131_matrix_row_3_[8] = { -1.048208, -1.557704, -7.264814, -1.961509, -7.264814,  1.211765,  1.272378, -2.649694};
	float *jaspar_core_pwm_131_matrix[4] = { jaspar_core_pwm_131_matrix_row_0_, jaspar_core_pwm_131_matrix_row_1_, jaspar_core_pwm_131_matrix_row_2_, jaspar_core_pwm_131_matrix_row_3_};
	PWM jaspar_core_pwm_131_ = {
/* accession        */ "MA0100",
/* name             */ "Myb",
/* label            */ " MA0100	9.88327768496603	Myb	TRP-CLUSTER	; acc \"P06876\" ; medline \"1861984\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"9.8840\" ; type \"SELEX\" ",
/* motif            */ "GRCNGTTG",
/* library_name     */ "jaspar_core",
/* length           */ 8,
/* matrixname       */ jaspar_core_pwm_131_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -41.259995,
/* max_score        */ 9.121033,
/* threshold        */ 0.853,
/* info content     */ 9.853304,
/* base_counts      */ {57, 71, 170, 110},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 51
};

	float jaspar_core_pwm_132_matrix_row_0_[6] = { -0.307233,  1.113070, -6.859741, -2.244620,  1.272084, -6.859741};
	float jaspar_core_pwm_132_matrix_row_1_[6] = {  0.385395, -1.315274, -6.618579, -6.618579, -6.618579,  1.245072};
	float jaspar_core_pwm_132_matrix_row_2_[6] = {  0.290176, -0.911469, -6.618579,  1.452640, -6.618579, -1.315274};
	float jaspar_core_pwm_132_matrix_row_3_[6] = { -0.461146, -6.859741,  1.272084, -2.244620, -6.859741, -0.461146};
	float *jaspar_core_pwm_132_matrix[4] = { jaspar_core_pwm_132_matrix_row_0_, jaspar_core_pwm_132_matrix_row_1_, jaspar_core_pwm_132_matrix_row_2_, jaspar_core_pwm_132_matrix_row_3_};
	PWM jaspar_core_pwm_132_ = {
/* accession        */ "MA0089",
/* name             */ "TCF11-MafG",
/* label            */ " MA0089	7.9381437011596	TCF11-MafG	bZIP	; acc \"Q14494 , Q90889\" ; comment \"Heterodimer between TCF11 and Mafg\" ; medline \"9421508\" ; species \"Gallus gallus\" ; sysgroup \"vertebrate\" ; total_ic \"7.9400\" ; type \"SELEX\" ",
/* motif            */ "NATGAC",
/* library_name     */ "jaspar_core",
/* length           */ 6,
/* matrixname       */ jaspar_core_pwm_132_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -34.518688,
/* max_score        */ 6.740345,
/* threshold        */ 0.912,
/* info content     */ 7.898324,
/* base_counts      */ {71, 39, 47, 47},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 34
};

	float jaspar_core_pwm_133_matrix_row_0_[11] = {  0.355180,  0.577825,  0.068329, -5.638782,  0.355180,  0.068329,  0.068329, -0.335477, -1.023661, -1.023661, -1.023661};
	float jaspar_core_pwm_133_matrix_row_1_[11] = { -0.782499, -0.094315, -5.397620, -5.397620, -5.397620, -5.397620, -5.397620, -0.782499,  0.309491,  0.596342,  1.000975};
	float jaspar_core_pwm_133_matrix_row_2_[11] = { -0.094315, -0.094315,  1.154888, -0.094315,  0.309491,  1.154888, -5.397620,  0.596342,  0.309491, -0.782499, -0.782499};
	float jaspar_core_pwm_133_matrix_row_3_[11] = {  0.068329, -1.023661, -5.638782,  1.047079,  0.068329, -5.638782,  0.913726,  0.068329,  0.068329,  0.355180, -0.335477};
	float *jaspar_core_pwm_133_matrix[4] = { jaspar_core_pwm_133_matrix_row_0_, jaspar_core_pwm_133_matrix_row_1_, jaspar_core_pwm_133_matrix_row_2_, jaspar_core_pwm_133_matrix_row_3_};
	PWM jaspar_core_pwm_133_ = {
/* accession        */ "MA0027",
/* name             */ "En1",
/* label            */ " MA0027	6.42106534259983	En1	HOMEO	; acc \"P09065\" ; medline \"8096059\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"6.4210\" ; type \"SELEX\" ",
/* motif            */ "NNGTNGTNNYC",
/* library_name     */ "jaspar_core",
/* length           */ 11,
/* matrixname       */ jaspar_core_pwm_133_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -33.371227,
/* max_score        */ 8.061917,
/* threshold        */ 0.811,
/* info content     */ 6.313039,
/* base_counts      */ {27, 17, 32, 34},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 10
};

	float jaspar_core_pwm_134_matrix_row_0_[10] = { -0.558718,  0.356076, -0.845569,  1.188420,  1.271767, -1.937559, -0.845569, -0.154084,  0.356076, -0.336073};
	float jaspar_core_pwm_134_matrix_row_1_[10] = {  0.087078, -0.094911,  0.859371, -1.696396, -6.311517,  1.472124,  0.933425, -6.311517,  0.692457,  1.238618};
	float jaspar_core_pwm_134_matrix_row_2_[10] = {  0.692457,  0.240991, -6.311517, -1.696396, -6.311517, -6.311517,  0.087078,  1.238618, -6.311517, -1.696396};
	float jaspar_core_pwm_134_matrix_row_3_[10] = { -0.558718, -0.845569,  0.250826, -6.552679, -6.552679, -6.552679, -1.249374, -6.552679, -0.558718, -6.552679};
	float *jaspar_core_pwm_134_matrix[4] = { jaspar_core_pwm_134_matrix_row_0_, jaspar_core_pwm_134_matrix_row_1_, jaspar_core_pwm_134_matrix_row_2_, jaspar_core_pwm_134_matrix_row_3_};
	PWM jaspar_core_pwm_134_ = {
/* accession        */ "MA0034",
/* name             */ "GAMYB",
/* label            */ " MA0034	9.31133794025198	GAMYB	TRP-CLUSTER	; acc \"CAA61021\" ; medline \"10069063\" ; species \"Hordeum vulgare\" ; sysgroup \"plant\" ; total_ic \"9.3080\" ; type \"SELEX\" ",
/* motif            */ "NNCAACCGMC",
/* library_name     */ "jaspar_core",
/* length           */ 10,
/* matrixname       */ jaspar_core_pwm_134_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.040092,
/* max_score        */ 9.943333,
/* threshold        */ 0.822,
/* info content     */ 9.248045,
/* base_counts      */ {90, 93, 45, 22},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 25
};

	float jaspar_core_pwm_135_matrix_row_0_[9] = { -0.026393,  0.855508, -1.813998,  0.127639, -7.117303, -7.117303, -7.117303, -1.123341, -1.123341};
	float jaspar_core_pwm_135_matrix_row_1_[9] = { -0.072635, -1.169031,  0.725262, -6.876141, -2.261020,  1.513446,  1.513446, -1.169031, -0.190280};
	float jaspar_core_pwm_135_matrix_row_2_[9] = { -1.169031, -0.323633, -0.659535, -2.261020, -6.876141, -6.876141, -6.876141,  0.127833,  0.127833};
	float jaspar_core_pwm_135_matrix_row_3_[9] = {  0.484100, -0.900697,  0.321669,  0.855508,  1.249300, -7.117303, -7.117303,  0.746349,  0.532866};
	float *jaspar_core_pwm_135_matrix[4] = { jaspar_core_pwm_135_matrix_row_0_, jaspar_core_pwm_135_matrix_row_1_, jaspar_core_pwm_135_matrix_row_2_, jaspar_core_pwm_135_matrix_row_3_};
	PWM jaspar_core_pwm_135_ = {
/* accession        */ "MA0136",
/* name             */ "ELF5",
/* label            */ " MA0136	8.69309549377962	ELF5	ETS	; acc \"\" ; medline \"16704374\" ; species \"Mus musculus\" ; sysgroup \"vertebrate\" ; total_ic \"8.693\" ; type \"SELEX\" ",
/* motif            */ "NAYTTCCTN",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_135_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -34.672478,
/* max_score        */ 8.475783,
/* threshold        */ 0.844,
/* info content     */ 8.659829,
/* base_counts      */ {65, 132, 38, 161},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 44
};

	float jaspar_core_pwm_136_matrix_row_0_[12] = { -0.368886, -0.773520,  0.236493, -6.767481,  0.403407, -6.767481, -6.767481, -6.767481,  0.546406, -6.767481, -0.081620, -1.464176};
	float jaspar_core_pwm_136_matrix_row_1_[12] = { -1.911199,  0.159542, -0.532358, -6.526319, -6.526319, -6.526319, -6.526319, -6.526319, -6.526319, -6.526319, -1.911199, -1.911199};
	float jaspar_core_pwm_136_matrix_row_2_[12] = {  0.718622,  0.564591,  0.159542, -6.526319,  0.969778, -6.526319, -6.526319, -6.526319,  0.852064,  0.026189, -0.532358, -0.532358};
	float jaspar_core_pwm_136_matrix_row_3_[12] = {  0.141274, -0.214973, -0.081620,  1.271999, -6.767481,  1.271999,  1.271999,  1.271999, -6.767481,  1.016159,  0.728616,  1.016159};
	float *jaspar_core_pwm_136_matrix[4] = { jaspar_core_pwm_136_matrix_row_0_, jaspar_core_pwm_136_matrix_row_1_, jaspar_core_pwm_136_matrix_row_2_, jaspar_core_pwm_136_matrix_row_3_};
	PWM jaspar_core_pwm_136_ = {
/* accession        */ "MA0042",
/* name             */ "FOXI1",
/* label            */ " MA0042	13.1829736117753	FOXI1	FORKHEAD	; acc \"Q12951\" ; medline \"9153225\" ; species \"Homo sapiens\" ; sysgroup \"vertebrate\" ; total_ic \"13.1850\" ; type \"SELEX\" ",
/* motif            */ "KNNTGTTTGTTT",
/* library_name     */ "jaspar_core",
/* length           */ 12,
/* matrixname       */ jaspar_core_pwm_136_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -54.411842,
/* max_score        */ 11.190477,
/* threshold        */ 0.782,
/* info content     */ 13.103147,
/* base_counts      */ {59, 15, 83, 215},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 31
};

	float jaspar_core_pwm_137_matrix_row_0_[9] = { -0.854220, -0.267321, -7.070826, -7.070826, -1.363716, -7.070826, -0.854220, -0.854220,  0.100062};
	float jaspar_core_pwm_137_matrix_row_1_[9] = {  0.484223, -1.526359, -6.829664, -6.829664, -0.835703, -6.829664, -0.277156,  0.415278,  0.415278};
	float jaspar_core_pwm_137_matrix_row_2_[9] = { -0.431069,  0.911435,  1.513414,  1.513414,  0.911435,  1.513414,  0.484223, -0.835703,  0.079091};
	float jaspar_core_pwm_137_matrix_row_3_[9] = {  0.307558, -0.384965, -7.070826, -7.070826,  0.020084, -7.070826,  0.243061,  0.479309, -0.854220};
	float *jaspar_core_pwm_137_matrix[4] = { jaspar_core_pwm_137_matrix_row_0_, jaspar_core_pwm_137_matrix_row_1_, jaspar_core_pwm_137_matrix_row_2_, jaspar_core_pwm_137_matrix_row_3_};
	PWM jaspar_core_pwm_137_ = {
/* accession        */ "MA0118",
/* name             */ "Macho-1",
/* label            */ " MA0118	7.46607328520911	Macho-1	ZN-FINGER, C2H2	; acc \"Q9GRA5\" ; medline \"15661650\" ; species \"Halocynthia roretzi\" ; sysgroup \"chordate\" ; total_ic \"7.46607328520911\" ; type \"SELEX\" ",
/* motif            */ "NGGGGGNYN",
/* library_name     */ "jaspar_core",
/* length           */ 9,
/* matrixname       */ jaspar_core_pwm_137_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -27.519430,
/* max_score        */ 8.226144,
/* threshold        */ 0.852,
/* info content     */ 7.434219,
/* base_counts      */ {40, 56, 207, 75},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 42
};

PWM *jaspar_core[JASPAR_CORE_N] = { &jaspar_core_pwm_0_, &jaspar_core_pwm_1_, &jaspar_core_pwm_2_, &jaspar_core_pwm_3_, &jaspar_core_pwm_4_, &jaspar_core_pwm_5_, &jaspar_core_pwm_6_, &jaspar_core_pwm_7_, &jaspar_core_pwm_8_, &jaspar_core_pwm_9_, &jaspar_core_pwm_10_, &jaspar_core_pwm_11_, &jaspar_core_pwm_12_, &jaspar_core_pwm_13_, &jaspar_core_pwm_14_, &jaspar_core_pwm_15_, &jaspar_core_pwm_16_, &jaspar_core_pwm_17_, &jaspar_core_pwm_18_, &jaspar_core_pwm_19_, &jaspar_core_pwm_20_, &jaspar_core_pwm_21_, &jaspar_core_pwm_22_, &jaspar_core_pwm_23_, &jaspar_core_pwm_24_, &jaspar_core_pwm_25_, &jaspar_core_pwm_26_, &jaspar_core_pwm_27_, &jaspar_core_pwm_28_, &jaspar_core_pwm_29_, &jaspar_core_pwm_30_, &jaspar_core_pwm_31_, &jaspar_core_pwm_32_, &jaspar_core_pwm_33_, &jaspar_core_pwm_34_, &jaspar_core_pwm_35_, &jaspar_core_pwm_36_, &jaspar_core_pwm_37_, &jaspar_core_pwm_38_, &jaspar_core_pwm_39_, &jaspar_core_pwm_40_, &jaspar_core_pwm_41_, &jaspar_core_pwm_42_, &jaspar_core_pwm_43_, &jaspar_core_pwm_44_, &jaspar_core_pwm_45_, &jaspar_core_pwm_46_, &jaspar_core_pwm_47_, &jaspar_core_pwm_48_, &jaspar_core_pwm_49_, &jaspar_core_pwm_50_, &jaspar_core_pwm_51_, &jaspar_core_pwm_52_, &jaspar_core_pwm_53_, &jaspar_core_pwm_54_, &jaspar_core_pwm_55_, &jaspar_core_pwm_56_, &jaspar_core_pwm_57_, &jaspar_core_pwm_58_, &jaspar_core_pwm_59_, &jaspar_core_pwm_60_, &jaspar_core_pwm_61_, &jaspar_core_pwm_62_, &jaspar_core_pwm_63_, &jaspar_core_pwm_64_, &jaspar_core_pwm_65_, &jaspar_core_pwm_66_, &jaspar_core_pwm_67_, &jaspar_core_pwm_68_, &jaspar_core_pwm_69_, &jaspar_core_pwm_70_, &jaspar_core_pwm_71_, &jaspar_core_pwm_72_, &jaspar_core_pwm_73_, &jaspar_core_pwm_74_, &jaspar_core_pwm_75_, &jaspar_core_pwm_76_, &jaspar_core_pwm_77_, &jaspar_core_pwm_78_, &jaspar_core_pwm_79_, &jaspar_core_pwm_80_, &jaspar_core_pwm_81_, &jaspar_core_pwm_82_, &jaspar_core_pwm_83_, &jaspar_core_pwm_84_, &jaspar_core_pwm_85_, &jaspar_core_pwm_86_, &jaspar_core_pwm_87_, &jaspar_core_pwm_88_, &jaspar_core_pwm_89_, &jaspar_core_pwm_90_, &jaspar_core_pwm_91_, &jaspar_core_pwm_92_, &jaspar_core_pwm_93_, &jaspar_core_pwm_94_, &jaspar_core_pwm_95_, &jaspar_core_pwm_96_, &jaspar_core_pwm_97_, &jaspar_core_pwm_98_, &jaspar_core_pwm_99_, &jaspar_core_pwm_100_, &jaspar_core_pwm_101_, &jaspar_core_pwm_102_, &jaspar_core_pwm_103_, &jaspar_core_pwm_104_, &jaspar_core_pwm_105_, &jaspar_core_pwm_106_, &jaspar_core_pwm_107_, &jaspar_core_pwm_108_, &jaspar_core_pwm_109_, &jaspar_core_pwm_110_, &jaspar_core_pwm_111_, &jaspar_core_pwm_112_, &jaspar_core_pwm_113_, &jaspar_core_pwm_114_, &jaspar_core_pwm_115_, &jaspar_core_pwm_116_, &jaspar_core_pwm_117_, &jaspar_core_pwm_118_, &jaspar_core_pwm_119_, &jaspar_core_pwm_120_, &jaspar_core_pwm_121_, &jaspar_core_pwm_122_, &jaspar_core_pwm_123_, &jaspar_core_pwm_124_, &jaspar_core_pwm_125_, &jaspar_core_pwm_126_, &jaspar_core_pwm_127_, &jaspar_core_pwm_128_, &jaspar_core_pwm_129_, &jaspar_core_pwm_130_, &jaspar_core_pwm_131_, &jaspar_core_pwm_132_, &jaspar_core_pwm_133_, &jaspar_core_pwm_134_, &jaspar_core_pwm_135_, &jaspar_core_pwm_136_, &jaspar_core_pwm_137_};
extern PWM *jaspar_core[JASPAR_CORE_N];
