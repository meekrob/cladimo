#include "pwm.h"
#include "jaspar_polII.h"
/* created by ./build_library_code using JASPAR_POLII_2008.wmx on Tue Mar  3 16:19:03 2009
*/
	float jaspar_polII_pwm_0_matrix_row_0_[5] = { -0.575734,  1.272688, -8.014706, -8.014706, -0.413304};
	float jaspar_polII_pwm_0_matrix_row_1_[5] = {  0.593059, -7.773544, -7.773544,  1.513850,  0.415423};
	float jaspar_polII_pwm_0_matrix_row_2_[5] = {  0.387260, -7.773544,  1.513850, -7.773544,  0.199267};
	float jaspar_polII_pwm_0_matrix_row_3_[5] = { -0.843818, -8.014706, -8.014706, -8.014706, -0.273607};
	float *jaspar_polII_pwm_0_matrix[4] = { jaspar_polII_pwm_0_matrix_row_0_, jaspar_polII_pwm_0_matrix_row_1_, jaspar_polII_pwm_0_matrix_row_2_, jaspar_polII_pwm_0_matrix_row_3_};
	PWM jaspar_polII_pwm_0_ = {
/* accession        */ "POL010",
/* name             */ "DCE_S_III",
/* label            */ " POL010	6.19324453715487	DCE_S_III	Unknown	; Description \"The DCE consists of three subelements, and each is distinct from the DPE sequence: S_I is CTTC, S_II is CTGT, and S_III is AGC. S_I resides approximately from +6 to +11, S_II from +16 to +21, and S_III from +30 to +34. (Lee et al.). The matrix are built based on the aligned motifs found in 108 promoters from DBTSS containing all three subelements (see supplemental material).\" ; End relative to TSS \"+34\" ; Start relative to TSS \"+30\" ; medline \"16227614\" ; species \"-\" ; total_ic \"6.1932\" ",
/* motif            */ "NAGCN",
/* library_name     */ "jaspar_polII",
/* length           */ 5,
/* matrixname       */ jaspar_polII_pwm_0_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -25.301239,
/* max_score        */ 5.308870,
/* threshold        */ 0.957,
/* info content     */ 6.180733,
/* base_counts      */ {145, 187, 172, 36},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 108
};

	float jaspar_polII_pwm_1_matrix_row_0_[8] = { -0.548875, -9.046069,  1.222096, -1.182418, -0.096964, -0.236057, -0.634015, -0.528676};
	float jaspar_polII_pwm_1_matrix_row_1_[8] = { -0.328328,  1.514029, -8.804908,  0.194836,  0.354245,  0.571032,  0.243032,  0.364715};
	float jaspar_polII_pwm_1_matrix_row_2_[8] = {  0.034514, -8.804908, -8.804908,  0.553939, -8.804908, -0.370878,  0.090859, -0.174207};
	float jaspar_polII_pwm_1_matrix_row_3_[8] = {  0.479155, -9.046069, -1.732183, -0.058747,  0.434374, -0.164094,  0.164371,  0.174320};
	float *jaspar_polII_pwm_1_matrix[4] = { jaspar_polII_pwm_1_matrix_row_0_, jaspar_polII_pwm_1_matrix_row_1_, jaspar_polII_pwm_1_matrix_row_2_, jaspar_polII_pwm_1_matrix_row_3_};
	PWM jaspar_polII_pwm_1_ = {
/* accession        */ "POL002",
/* name             */ "INR",
/* label            */ " POL002	4.67022136152347	INR	Unknown	; Description \"The INR consensus PyPyA(+1)NT/APyPy (Smale, Kadonaga 2003) is characterized by an A at the TSS (+1) and a cytosine at position -1. TFIID specific.\" ; End relative to TSS \"+6\" ; Start relative to TSS \"-2\" ; medline \"2329577\" ; species \"-\" ; total_ic \"4.6702\" ",
/* motif            */ "NCANNNNN",
/* library_name     */ "jaspar_polII",
/* length           */ 8,
/* matrixname       */ jaspar_polII_pwm_1_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -29.920750,
/* max_score        */ 5.382373,
/* threshold        */ 0.897,
/* info content     */ 4.666787,
/* base_counts      */ {602, 826, 360, 636},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 303
};

	float jaspar_polII_pwm_2_matrix_row_0_[19] = { -0.918743, -0.918743, -0.918743, -5.533864, -0.230559,  1.269642,  0.460098, -5.533864, -5.533864, -0.230559,  1.269642,  0.460098, -5.533864, -5.533864, -5.533864,  0.460098, -0.230559, -0.918743, -0.918743};
	float jaspar_polII_pwm_2_matrix_row_1_[19] = {  0.010603,  0.414409,  0.010603,  1.510804, -5.292702, -5.292702, -5.292702,  1.510804,  0.701260,  0.414409, -5.292702, -5.292702,  1.510804, -5.292702,  0.010603, -5.292702,  0.701260,  0.414409,  0.414409};
	float jaspar_polII_pwm_2_matrix_row_2_[19] = {  0.010603, -5.292702, -0.677581, -5.292702,  1.259806, -5.292702,  0.923905, -5.292702,  0.923905,  0.701260, -5.292702,  0.701260, -5.292702,  1.510804,  1.259806, -5.292702, -5.292702,  0.923905,  0.010603};
	float jaspar_polII_pwm_2_matrix_row_3_[19] = {  0.460098,  0.682742,  0.682742, -5.533864, -5.533864, -5.533864, -5.533864, -5.533864, -5.533864, -5.533864, -5.533864, -0.918743, -5.533864, -5.533864, -5.533864,  0.682742,  0.173247, -5.533864,  0.173247};
	float *jaspar_polII_pwm_2_matrix[4] = { jaspar_polII_pwm_2_matrix_row_0_, jaspar_polII_pwm_2_matrix_row_1_, jaspar_polII_pwm_2_matrix_row_2_, jaspar_polII_pwm_2_matrix_row_3_};
	PWM jaspar_polII_pwm_2_ = {
/* accession        */ "POL001",
/* name             */ "MTE",
/* label            */ " POL001	20.9565457332953	MTE	Unknown	; Description \"The matrix is made of 9 sequences that have been tested positively as sites of transcription initiation in vivo.\" ; End relative to TSS \"+33\" ; Start relative to TSS \"+15\" ; medline \"15231738\" ; species \"Drosophila melanogaster\" ; total_ic \"20.9565\" ",
/* motif            */ "NTTCGAGCGSARCGGTYGN",
/* library_name     */ "jaspar_polII",
/* length           */ 19,
/* matrixname       */ jaspar_polII_pwm_2_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.333405,
/* max_score        */ 18.900335,
/* threshold        */ 0.805,
/* info content     */ 20.549864,
/* base_counts      */ {41, 53, 51, 26},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 9
};

	float jaspar_polII_pwm_3_matrix_row_0_[6] = { -5.974115, -5.974115, -5.974115, -5.974115, -5.974115, -5.974115};
	float jaspar_polII_pwm_3_matrix_row_1_[6] = { -5.732953,  1.511989, -5.732953,  1.511989,  1.511989,  0.483653};
	float jaspar_polII_pwm_3_matrix_row_2_[6] = {  1.511989, -5.732953, -5.732953, -5.732953, -5.732953,  1.070552};
	float jaspar_polII_pwm_3_matrix_row_3_[6] = { -5.974115, -5.974115,  1.270827, -5.974115, -5.974115, -5.974115};
	float *jaspar_polII_pwm_3_matrix[4] = { jaspar_polII_pwm_3_matrix_row_0_, jaspar_polII_pwm_3_matrix_row_1_, jaspar_polII_pwm_3_matrix_row_2_, jaspar_polII_pwm_3_matrix_row_3_};
	PWM jaspar_polII_pwm_3_ = {
/* accession        */ "POL013",
/* name             */ "MED-1",
/* label            */ " POL013	11.0597140413294	MED-1	Unknown	; Description \"Downstream core promoter element found in TATA-less promoters sharing similar features with the pgp1 promoter. The matrix is built from 14 promoters identified in the literature similar to the pgp1 promoter. It is often found in promoters with multiple start sites, wchich makes the relative coordinates reported above less applicable\" ; End relative to TSS \"+6 to +116\" ; Start relative to TSS \"+1 to +110\" ; medline \"8530439 \" ; species \"-\" ; total_ic \"11.0597\" ",
/* motif            */ "GCTCCG",
/* library_name     */ "jaspar_polII",
/* length           */ 6,
/* matrixname       */ jaspar_polII_pwm_3_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -35.844688,
/* max_score        */ 8.389334,
/* threshold        */ 0.873,
/* info content     */ 10.916836,
/* base_counts      */ {0, 47, 23, 14},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 14
};

	float jaspar_polII_pwm_4_matrix_row_0_[10] = { -6.378630, -6.378630, -6.378630, -6.378630, -6.378630, -6.378630, -0.671520,  1.271539, -6.378630, -1.075325};
	float jaspar_polII_pwm_4_matrix_row_1_[10] = { -6.137468,  0.261127, -6.137468,  1.412668, -6.137468, -6.137468, -6.137468, -6.137468,  1.107474,  1.412668};
	float jaspar_polII_pwm_4_matrix_row_2_[10] = {  1.512701,  1.176419,  1.512701, -6.137468,  1.512701,  1.512701,  1.358629, -6.137468,  0.415040, -6.137468};
	float jaspar_polII_pwm_4_matrix_row_3_[10] = { -6.378630, -6.378630, -6.378630, -1.075325, -6.378630, -6.378630, -6.378630, -6.378630, -6.378630, -6.378630};
	float *jaspar_polII_pwm_4_matrix[4] = { jaspar_polII_pwm_4_matrix_row_0_, jaspar_polII_pwm_4_matrix_row_1_, jaspar_polII_pwm_4_matrix_row_2_, jaspar_polII_pwm_4_matrix_row_3_};
	PWM jaspar_polII_pwm_4_ = {
/* accession        */ "POL011",
/* name             */ "XCPE1",
/* label            */ " POL011	16.7194781404227	XCPE1	Unknown	; Description \"The matrix is made of the human promoter sequences that are shown in the supplementary material (Table S1). We included only the sites that show XCPE1 activity in vitro.\" ; End relative to TSS \"+2\" ; Start relative to TSS \"-8\" ; medline \"17210644\" ; species \"-\" ; total_ic \"16.7194\" ",
/* motif            */ "GGGCGGGACC",
/* library_name     */ "jaspar_polII",
/* length           */ 10,
/* matrixname       */ jaspar_polII_pwm_4_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -63.786308,
/* max_score        */ 13.790199,
/* threshold        */ 0.787,
/* info content     */ 16.570498,
/* base_counts      */ {26, 58, 124, 2},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 21
};

	float jaspar_polII_pwm_5_matrix_row_0_[6] = { -0.636322, -2.307596, -1.616111, -2.711401, -0.769764, -0.636322};
	float jaspar_polII_pwm_5_matrix_row_1_[6] = {  0.358281,  1.436896, -1.556938, -0.769570, -0.123375,  0.164188};
	float jaspar_polII_pwm_5_matrix_row_2_[6] = {  0.593059, -2.066434, -0.602656,  1.331547, -0.602656,  0.638511};
	float jaspar_polII_pwm_5_matrix_row_3_[6] = { -0.700819, -2.711401,  1.021400, -1.798100,  0.684975, -0.464571};
	float *jaspar_polII_pwm_5_matrix[4] = { jaspar_polII_pwm_5_matrix_row_0_, jaspar_polII_pwm_5_matrix_row_1_, jaspar_polII_pwm_5_matrix_row_2_, jaspar_polII_pwm_5_matrix_row_3_};
	PWM jaspar_polII_pwm_5_ = {
/* accession        */ "POL009",
/* name             */ "DCE_S_II",
/* label            */ " POL009	4.13199510752977	DCE_S_II	Unknown	; Description \"The DCE consists of three subelements, and each is distinct from the DPE sequence: S_I is CTTC, S_II is CTGT, and S_III is AGC. S_I resides approximately from +6 to +11, S_II from +16 to +21, and S_III from +30 to +34. (Lee et al.). The matrix are built based on the aligned motifs found in 108 promoters from DBTSS containing all three subelements (see supplemental material).\" ; End relative to TSS \"+21\" ; Start relative to TSS \"+16\" ; medline \"16227614\" ; species \"-\" ; total_ic \"4.1320\" ",
/* motif            */ "NCTGTN",
/* library_name     */ "jaspar_polII",
/* length           */ 6,
/* matrixname       */ jaspar_polII_pwm_5_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -9.145820,
/* max_score        */ 5.706388,
/* threshold        */ 0.958,
/* info content     */ 4.128592,
/* base_counts      */ {57, 199, 207, 185},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 108
};

	float jaspar_polII_pwm_6_matrix_row_0_[15] = { -0.579679, -1.917503,  1.172943, -3.588776,  1.178609,  0.900308,  1.195415,  0.712006,  0.352773, -0.665186, -0.271755, -0.283875, -0.283875, -0.471062, -0.346781};
	float jaspar_polII_pwm_6_matrix_row_1_[15] = {  0.527248, -0.620696, -9.054725, -2.145970, -9.054725, -9.054725, -3.347614, -3.751420, -0.665138,  0.455794,  0.540946,  0.394711,  0.321215,  0.223368,  0.165665};
	float jaspar_polII_pwm_6_matrix_row_2_[15] = {  0.574392, -1.558627, -3.751420, -3.751420, -2.838119, -9.054725, -2.145970, -0.665138,  0.606755,  0.561147,  0.402554,  0.402554,  0.402554,  0.484991,  0.492159};
	float jaspar_polII_pwm_6_matrix_row_3_[15] = { -1.256407,  1.042657, -1.135083,  1.233566, -1.289186,  0.105157, -2.897292,  0.105157, -1.193906, -0.819307, -1.256407, -0.739281, -0.579679, -0.373095, -0.427896};
	float *jaspar_polII_pwm_6_matrix[4] = { jaspar_polII_pwm_6_matrix_row_0_, jaspar_polII_pwm_6_matrix_row_1_, jaspar_polII_pwm_6_matrix_row_2_, jaspar_polII_pwm_6_matrix_row_3_};
	PWM jaspar_polII_pwm_6_ = {
/* accession        */ "POL012",
/* name             */ "TATA-Box",
/* label            */ " POL012	10.1980738528127	TATA-Box	Unknown	; Description \"The dominating  motif TATA ( first T in TATAT) is located in a preferred region from -32 to -28 relative to the TSS (see pub med 16916456). This model is also housed in JASPAR CORE (MA0108)\" ; End relative to TSS \"-25 to -9\" ; Start relative to TSS \"-39 to -23\" ; medline \"2329577\" ; species \"-\" ; total_ic \"10.1980\" ",
/* motif            */ "STATAAAARNNNNNN",
/* library_name     */ "jaspar_polII",
/* length           */ 15,
/* matrixname       */ jaspar_polII_pwm_6_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.676067,
/* max_score        */ 11.501001,
/* threshold        */ 0.735,
/* info content     */ 10.166988,
/* base_counts      */ {2239, 985, 1203, 1398},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 389
};

	float jaspar_polII_pwm_7_matrix_row_0_[9] = {  0.044229,  0.449555,  0.579575, -2.436106,  0.627191, -1.186903, -0.361028, -0.425525, -0.189276};
	float jaspar_polII_pwm_7_matrix_row_1_[9] = { -0.002152,  0.151919, -1.791139,  0.508451, -2.194944,  0.508451,  0.541230,  0.151919,  0.198418};
	float jaspar_polII_pwm_7_matrix_row_2_[9] = {  0.508451, -1.099654,  0.439482,  0.957281, -2.883129, -1.504288,  0.662555, -0.494275, -0.059278};
	float jaspar_polII_pwm_7_matrix_row_3_[9] = { -0.830657, -0.189276, -0.830657, -2.032301,  0.449555,  0.579575, -7.739411,  0.421392,  0.044229};
	float *jaspar_polII_pwm_7_matrix[4] = { jaspar_polII_pwm_7_matrix_row_0_, jaspar_polII_pwm_7_matrix_row_1_, jaspar_polII_pwm_7_matrix_row_2_, jaspar_polII_pwm_7_matrix_row_3_};
	PWM jaspar_polII_pwm_7_ = {
/* accession        */ "POL005",
/* name             */ "DPE",
/* label            */ " POL005	3.30887839177199	DPE	Unknown	; Description \"The matrix is generated from the DPE containing sequences found in the Drosophila Core Promoter Database http://www-biology.ucsd.edu/labs/Kadonaga/DCPD.htm .\" ; End relative to TSS \"+34\" ; Start relative to TSS \"+26\" ; medline \"10848601\" ; species \"Drosophila melanogaster\" ; total_ic \"3.3088\" ",
/* motif            */ "NNRGAYSNN",
/* library_name     */ "jaspar_polII",
/* length           */ 9,
/* matrixname       */ jaspar_polII_pwm_7_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -18.967937,
/* max_score        */ 4.983993,
/* threshold        */ 0.879,
/* info content     */ 3.303800,
/* base_counts      */ {203, 178, 179, 178},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 82
};

	float jaspar_polII_pwm_8_matrix_row_0_[8] = {  0.665846, -0.717953, -1.809943, -0.026469, -0.431102, -6.425064, -6.425064, -1.809943};
	float jaspar_polII_pwm_8_matrix_row_1_[8] = {  0.368606,  0.501960,  1.061040, -0.189940,  1.129985, -6.183901,  1.417501,  1.312196};
	float jaspar_polII_pwm_8_matrix_row_2_[8] = { -0.880597,  0.820073,  0.368606,  0.907008, -0.880597,  1.512766, -1.568781, -1.568781};
	float jaspar_polII_pwm_8_matrix_row_3_[8] = { -1.809943, -6.425064, -6.425064, -6.425064, -1.809943, -6.425064, -1.809943, -1.121759};
	float *jaspar_polII_pwm_8_matrix[4] = { jaspar_polII_pwm_8_matrix_row_0_, jaspar_polII_pwm_8_matrix_row_1_, jaspar_polII_pwm_8_matrix_row_2_, jaspar_polII_pwm_8_matrix_row_3_};
	PWM jaspar_polII_pwm_8_ = {
/* accession        */ "POL006",
/* name             */ "BREu",
/* label            */ " POL006	7.65052498791428	BREu	Unknown	; Description \"Core promoter element that is recognised by TFIIB located uptream of the TATA-box.\" ; End relative to TSS \"-32\" ; Start relative to TSS \"-39\" ; medline \"9420329\" ; species \"-\" ; total_ic \"7.6505\" ",
/* motif            */ "ASCGCGCC",
/* library_name     */ "jaspar_polII",
/* length           */ 8,
/* matrixname       */ jaspar_polII_pwm_8_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -37.555149,
/* max_score        */ 8.826416,
/* threshold        */ 0.872,
/* info content     */ 7.598439,
/* base_counts      */ {27, 86, 58, 5},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 22
};

	float jaspar_polII_pwm_9_matrix_row_0_[6] = { -0.518609, -2.020745, -2.307596, -2.020745, -2.711401, -0.318039};
	float jaspar_polII_pwm_9_matrix_row_1_[6] = { -0.223409,  1.364333, -0.682634, -0.682634,  1.364333,  0.358281};
	float jaspar_polII_pwm_9_matrix_row_2_[6] = {  0.660485, -1.374949, -1.087683, -0.172142, -1.221036,  0.127833};
	float jaspar_polII_pwm_9_matrix_row_3_[6] = { -0.190260, -1.798100,  1.033233,  0.867269, -1.616111, -0.190260};
	float *jaspar_polII_pwm_9_matrix[4] = { jaspar_polII_pwm_9_matrix_row_0_, jaspar_polII_pwm_9_matrix_row_1_, jaspar_polII_pwm_9_matrix_row_2_, jaspar_polII_pwm_9_matrix_row_3_};
	PWM jaspar_polII_pwm_9_ = {
/* accession        */ "POL008",
/* name             */ "DCE_S_I",
/* label            */ " POL008	4.14047794920485	DCE_S_I	Unknown	; Description \"The DCE consists of three subelements, and each is distinct from the DPE sequence: S_I is CTTC, S_II is CTGT, and S_III is AGC. S_I resides approximately from +6 to +11, S_II from +16 to +21, and S_III from +30 to +34. (Lee et al.). The matrix is built based on the aligned motifs found in 108 promoters from DBTSS containing all three subelements (see supplemental material of the source publication).\" ; End relative to TSS \"+11\" ; Start relative to TSS \"+6\" ; medline \"16227614\" ; species \"-\" ; total_ic \"4.1404\" ",
/* motif            */ "NCTTCN",
/* library_name     */ "jaspar_polII",
/* length           */ 6,
/* matrixname       */ jaspar_polII_pwm_9_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -9.897134,
/* max_score        */ 5.647934,
/* threshold        */ 0.958,
/* info content     */ 4.137136,
/* base_counts      */ {53, 263, 114, 218},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 108
};

	float jaspar_polII_pwm_10_matrix_row_0_[7] = { -0.137997, -0.830146, -0.137997, -1.339641, -1.052790, -1.743447, -0.648157};
	float jaspar_polII_pwm_10_matrix_row_1_[7] = { -0.811628, -0.119729, -0.406995, -0.406995, -0.119729, -0.406995, -0.002084};
	float jaspar_polII_pwm_10_matrix_row_2_[7] = {  0.744546, -0.119729,  0.198384,  0.508297,  0.508297,  0.365299,  0.103165};
	float jaspar_polII_pwm_10_matrix_row_3_[7] = { -0.360891,  0.554651,  0.198190,  0.392220,  0.198190,  0.554651,  0.331632};
	float *jaspar_polII_pwm_10_matrix[4] = { jaspar_polII_pwm_10_matrix_row_0_, jaspar_polII_pwm_10_matrix_row_1_, jaspar_polII_pwm_10_matrix_row_2_, jaspar_polII_pwm_10_matrix_row_3_};
	PWM jaspar_polII_pwm_10_ = {
/* accession        */ "POL007",
/* name             */ "BREd",
/* label            */ " POL007	1.31756354163441	BREd	Unknown	; Description \"Core promoter element that is recognised by TFIIB located downstream of the TATA-box.\" ; End relative to TSS \"-17 \" ; Start relative to TSS \"-23\" ; medline \"16230532 \" ; species \"-\" ; total_ic \"1.3175\" ",
/* motif            */ "NNNKNKN",
/* library_name     */ "jaspar_polII",
/* length           */ 7,
/* matrixname       */ jaspar_polII_pwm_10_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -6.832804,
/* max_score        */ 3.400458,
/* threshold        */ 0.959,
/* info content     */ 1.314804,
/* base_counts      */ {40, 47, 91, 109},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 41
};

	float jaspar_polII_pwm_11_matrix_row_0_[12] = {  0.133481, -0.426000, -0.672773,  0.733022,  0.039973, -8.497219, -8.497219,  1.272794,  0.887159, -1.058247, -0.756120,  0.861628};
	float jaspar_polII_pwm_11_matrix_row_1_[12] = {  0.356628,  0.300549,  0.199474, -3.640936, -1.857462,  1.502463,  1.508226, -8.256057, -1.570196, -8.256057,  0.849034, -1.857462};
	float jaspar_polII_pwm_11_matrix_row_2_[12] = { -1.165147,  0.110546, -0.472416,  0.597751,  0.944334, -3.640936, -8.256057, -8.256057, -0.605888, -0.942170,  0.426820,  0.300549};
	float jaspar_polII_pwm_11_matrix_row_3_[12] = {  0.059387, -0.020640,  0.477526, -3.193914, -0.947084, -3.882098, -3.882098, -8.497219, -0.595842,  1.070866, -2.790109, -3.882098};
	float *jaspar_polII_pwm_11_matrix[4] = { jaspar_polII_pwm_11_matrix_row_0_, jaspar_polII_pwm_11_matrix_row_1_, jaspar_polII_pwm_11_matrix_row_2_, jaspar_polII_pwm_11_matrix_row_3_};
	PWM jaspar_polII_pwm_11_ = {
/* accession        */ "POL004",
/* name             */ "CCAAT-box",
/* label            */ " POL004	10.7511552886618	CCAAT-box	Unknown	; Description \"The CCAAT-box is recognised by the CCAAT-binding proteins NY-I (Dorn et al. 1987), NF-I and C/EBP.\" ; End relative to TSS \"-208 to -52\" ; Start relative to TSS \"-219 to -64\" ; medline \"2329577\" ; species \"-\" ; total_ic \"10.7511\" ",
/* motif            */ "NNNAGCCAATCA",
/* library_name     */ "jaspar_polII",
/* length           */ 12,
/* matrixname       */ jaspar_polII_pwm_11_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -49.752434,
/* max_score        */ 10.764229,
/* threshold        */ 0.784,
/* info content     */ 10.741189,
/* base_counts      */ {716, 612, 396, 376},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 175
};

	float jaspar_polII_pwm_12_matrix_row_0_[14] = {  0.284762,  0.234506, -0.428085, -0.135467, -8.945478, -3.642174, -0.351139, -0.511450, -4.330358,  0.029266, -1.204379, -8.945478, -1.344076, -0.651179};
	float jaspar_polII_pwm_12_matrix_row_1_[14] = { -0.410017, -0.664837, -2.305722, -4.089196, -8.704316, -8.704316,  1.036711, -4.089196, -2.997206, -8.704316, -1.265345,  1.012902,  0.355317, -0.920676};
	float jaspar_polII_pwm_12_matrix_row_2_[14] = { -0.186923,  0.619442,  0.937871,  1.228778,  1.514018,  1.506693, -8.704316,  1.312544,  1.303576,  1.042576,  1.158401, -0.543513, -0.147710,  0.592293};
	float jaspar_polII_pwm_12_matrix_row_3_[14] = {  0.066533, -0.813654, -0.181269, -8.945478, -8.945478, -8.945478, -0.428085, -3.238368, -0.468899, -1.161838, -0.602401, -0.049712,  0.413368,  0.274911};
	float *jaspar_polII_pwm_12_matrix[4] = { jaspar_polII_pwm_12_matrix_row_0_, jaspar_polII_pwm_12_matrix_row_1_, jaspar_polII_pwm_12_matrix_row_2_, jaspar_polII_pwm_12_matrix_row_3_};
	PWM jaspar_polII_pwm_12_ = {
/* accession        */ "POL003",
/* name             */ "GC-box",
/* label            */ " POL003	11.5380830281189	GC-box	Unknown	; Description \"The GC-box is recognised by transcription factor Sp1 and typically occurs in housekeeping genes (Kadonaga et al. 1986). \" ; End relative to TSS \"-157 to +8\" ; Start relative to TSS \"-170 to -6\" ; medline \"2329577\" ; species \"-\" ; total_ic \"11.5380\" ",
/* motif            */ "NRGGGGCGGGGCNK",
/* library_name     */ "jaspar_polII",
/* length           */ 14,
/* matrixname       */ jaspar_polII_pwm_12_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -68.669594,
/* max_score        */ 13.963936,
/* threshold        */ 0.755,
/* info content     */ 11.530543,
/* base_counts      */ {581, 545, 2073, 637},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 274
};

PWM *jaspar_polII[JASPAR_POLII_N] = { &jaspar_polII_pwm_0_, &jaspar_polII_pwm_1_, &jaspar_polII_pwm_2_, &jaspar_polII_pwm_3_, &jaspar_polII_pwm_4_, &jaspar_polII_pwm_5_, &jaspar_polII_pwm_6_, &jaspar_polII_pwm_7_, &jaspar_polII_pwm_8_, &jaspar_polII_pwm_9_, &jaspar_polII_pwm_10_, &jaspar_polII_pwm_11_, &jaspar_polII_pwm_12_};
extern PWM *jaspar_polII[JASPAR_POLII_N];
