#ifndef STAT_PWM_LIB_H
#include "pwm.h"

struct pwm_library
{
    PWM **lib;
    int npwm;
    int max_pwm_len;
};

PWM** build_static_library(int *total_n);
PWM *retrieve_library_accession(PWM **lib, int npwm, char* accession);
void sort_library(PWM **lib, int npwm, char* by);
PWM** search_volume_name(char *name, int *size);
int condense_pointer_array(PWM **base, size_t nmemb);
#define STAT_PWM_LIB_H 1
#endif
