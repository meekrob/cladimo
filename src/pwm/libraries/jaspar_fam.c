#include "pwm.h"
#include "jaspar_fam.h"
/* created by ./build_library_code using JASPAR_FAM_2008.wmx on Tue Mar  3 16:19:03 2009
*/
	float jaspar_fam_pwm_0_matrix_row_0_[9] = {  0.762073, -2.331973, -7.937775,  0.368450, -2.082703,  0.585003, -0.579581,  0.844702,  0.953324};
	float jaspar_fam_pwm_0_matrix_row_1_[9] = { -0.931574, -1.960040, -2.783958, -7.696612,  1.260512, -1.651607, -0.064696, -0.059378, -1.076539};
	float jaspar_fam_pwm_0_matrix_row_2_[9] = {  0.263013, -2.860331, -1.293038,  0.900869, -2.040621,  0.662288, -1.552427, -1.331862, -1.614394};
	float jaspar_fam_pwm_0_matrix_row_3_[9] = { -2.331973,  1.199565,  1.195901, -1.653640, -0.556273, -2.281783,  0.745780, -1.216349, -0.591764};
	float *jaspar_fam_pwm_0_matrix[4] = { jaspar_fam_pwm_0_matrix_row_0_, jaspar_fam_pwm_0_matrix_row_1_, jaspar_fam_pwm_0_matrix_row_2_, jaspar_fam_pwm_0_matrix_row_3_};
	PWM jaspar_fam_pwm_0_ = {
/* accession        */ "MF0006",
/* name             */ "bZIP cEBP-like subclass",
/* label            */ " MF0006	7.89193388107753	bZIP cEBP-like subclass	bZIP	; included_models \"MA0019,MA0025,MA0043,MA0102\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "ATTGCATAA",
/* library_name     */ "jaspar_fam",
/* length           */ 9,
/* matrixname       */ jaspar_fam_pwm_0_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -29.689857,
/* max_score        */ 8.525014,
/* threshold        */ 0.849,
/* info content     */ 7.882792,
/* base_counts      */ {310, 144, 150, 295},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_1_matrix_row_0_[6] = {  0.769047, -2.807876, -2.850178, -0.907802, -0.823819, -1.024037};
	float jaspar_fam_pwm_1_matrix_row_1_[6] = { -0.893107, -1.242988, -1.488023, -1.195323, -0.964402, -0.226388};
	float jaspar_fam_pwm_1_matrix_row_2_[6] = { -7.696612, -0.449532, -3.164013,  1.219894, -1.944040, -0.720264};
	float jaspar_fam_pwm_1_matrix_row_3_[6] = {  0.088069,  1.023463,  1.194820, -1.312382,  1.000888,  0.790004};
	float *jaspar_fam_pwm_1_matrix[4] = { jaspar_fam_pwm_1_matrix_row_0_, jaspar_fam_pwm_1_matrix_row_1_, jaspar_fam_pwm_1_matrix_row_2_, jaspar_fam_pwm_1_matrix_row_3_};
	PWM jaspar_fam_pwm_1_ = {
/* accession        */ "MF0011",
/* name             */ "HMG class",
/* label            */ " MF0011	5.33180767133444	HMG class	HMG	; included_models \"MA0044,MA0045,MA0077,MA0078,MA0084,MA0087\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "ATKGTT",
/* library_name     */ "jaspar_fam",
/* length           */ 6,
/* matrixname       */ jaspar_fam_pwm_1_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -17.948959,
/* max_score        */ 5.998116,
/* threshold        */ 0.944,
/* info content     */ 5.326073,
/* base_counts      */ {97, 53, 103, 346},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_2_matrix_row_0_[10] = { -7.937775, -7.937775, -2.974930,  0.041564,  0.413600, -0.364757, -2.974930, -2.285285, -2.557877, -1.916751};
	float jaspar_fam_pwm_2_matrix_row_1_[10] = { -0.742928, -2.271662, -7.696612, -2.271662, -0.308285, -1.903599, -1.384878,  0.206245,  1.415894,  1.421393};
	float jaspar_fam_pwm_2_matrix_row_2_[10] = {  1.287078,  1.417877,  1.449403,  0.906758, -0.369489, -1.665927, -2.083484, -7.696612, -7.696612, -2.836800};
	float jaspar_fam_pwm_2_matrix_row_3_[10] = { -1.046149, -1.401083, -1.757758, -0.688560, -0.063036,  0.959908,  1.171197,  0.917746, -1.361305, -2.094230};
	float *jaspar_fam_pwm_2_matrix[4] = { jaspar_fam_pwm_2_matrix_row_0_, jaspar_fam_pwm_2_matrix_row_1_, jaspar_fam_pwm_2_matrix_row_2_, jaspar_fam_pwm_2_matrix_row_3_};
	PWM jaspar_fam_pwm_2_ = {
/* accession        */ "MF0003",
/* name             */ "REL class",
/* label            */ " MF0003	10.9553785258347	REL class	REL	; included_models \"MA0022,MA0023,MA0061,MA0101,MA0105,MA0107\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "GGGGNTTTCC",
/* library_name     */ "jaspar_fam",
/* length           */ 10,
/* matrixname       */ jaspar_fam_pwm_2_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -49.321869,
/* max_score        */ 11.360856,
/* threshold        */ 0.813,
/* info content     */ 10.940903,
/* base_counts      */ {103, 249, 342, 306},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_3_matrix_row_0_[8] = {  0.186080, -0.084170,  0.198451, -1.567874, -0.058862, -2.920495, -2.482454, -2.739278};
	float jaspar_fam_pwm_3_matrix_row_1_[8] = { -2.023289, -1.618970, -1.740775,  1.168417, -0.099718, -0.067609, -3.081492, -2.666175};
	float jaspar_fam_pwm_3_matrix_row_2_[8] = { -1.241414,  0.509425, -0.047396, -1.416217,  0.761467,  1.180351, -7.696612, -2.900822};
	float jaspar_fam_pwm_3_matrix_row_3_[8] = {  0.711148,  0.172653,  0.381699, -0.438351, -1.448570, -1.490469,  1.238802,  1.226417};
	float *jaspar_fam_pwm_3_matrix[4] = { jaspar_fam_pwm_3_matrix_row_0_, jaspar_fam_pwm_3_matrix_row_1_, jaspar_fam_pwm_3_matrix_row_2_, jaspar_fam_pwm_3_matrix_row_3_};
	PWM jaspar_fam_pwm_3_ = {
/* accession        */ "MF0009",
/* name             */ "TRP(MYB) class",
/* label            */ " MF0009	6.40223313941086	TRP(MYB) class	TRP	; included_models \" MA0034,MA0050,MA0051,MA0054,MA0100\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "TNWCNGTT",
/* library_name     */ "jaspar_fam",
/* length           */ 8,
/* matrixname       */ jaspar_fam_pwm_3_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -21.917408,
/* max_score        */ 7.177726,
/* threshold        */ 0.883,
/* info content     */ 6.395206,
/* base_counts      */ {131, 125, 189, 354},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_4_matrix_row_0_[9] = { -2.210927, -0.129452, -7.937775, -4.354256, -3.025120,  0.999575, -2.530603, -0.503517, -1.436485};
	float jaspar_fam_pwm_4_matrix_row_1_[9] = { -2.203551, -7.696612, -2.503656, -3.052222, -7.696612, -7.696612,  0.614540, -0.756390, -0.081314};
	float jaspar_fam_pwm_4_matrix_row_2_[9] = { -7.696612,  1.216669, -3.339904, -7.696612, -0.932728, -0.063243, -1.238274,  0.027834, -0.641300};
	float jaspar_fam_pwm_4_matrix_row_3_[9] = {  1.216313, -3.219276,  1.246735,  1.258872,  1.167316, -2.150877,  0.594110,  0.582414,  0.786433};
	float *jaspar_fam_pwm_4_matrix[4] = { jaspar_fam_pwm_4_matrix_row_0_, jaspar_fam_pwm_4_matrix_row_1_, jaspar_fam_pwm_4_matrix_row_2_, jaspar_fam_pwm_4_matrix_row_3_};
	PWM jaspar_fam_pwm_4_ = {
/* accession        */ "MF0005",
/* name             */ "Forkhead class",
/* label            */ " MF0005	10.3094355478249	Forkhead class	Forkhead	; included_models \"MA0030,MA0031,MA0032,MA0033,MA0040,MA0041,MA0042,MA0047\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "TGTTTATTT",
/* library_name     */ "jaspar_fam",
/* length           */ 9,
/* matrixname       */ jaspar_fam_pwm_4_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -51.144314,
/* max_score        */ 9.088867,
/* threshold        */ 0.833,
/* info content     */ 10.294285,
/* base_counts      */ {131, 77, 145, 547},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_5_matrix_row_0_[6] = {  0.556764, -2.178873, -1.611625, -0.654326, -0.754663,  0.976448};
	float jaspar_fam_pwm_5_matrix_row_1_[6] = { -1.835826, -3.448117, -2.228553, -1.533298,  1.261799, -0.234972};
	float jaspar_fam_pwm_5_matrix_row_2_[6] = {  0.522444,  1.370319,  1.291459, -1.177465, -1.682897, -1.199838};
	float jaspar_fam_pwm_5_matrix_row_3_[6] = { -0.977427, -1.078160, -0.847698,  0.970785, -1.713216, -2.837908};
	float *jaspar_fam_pwm_5_matrix[4] = { jaspar_fam_pwm_5_matrix_row_0_, jaspar_fam_pwm_5_matrix_row_1_, jaspar_fam_pwm_5_matrix_row_2_, jaspar_fam_pwm_5_matrix_row_3_};
	PWM jaspar_fam_pwm_5_ = {
/* accession        */ "MF0004",
/* name             */ "Nuclear Receptor class",
/* label            */ " MF0004	5.37601831905227	Nuclear Receptor class	Nuclear receptor	; included_models \"MA0007,MA0016,MA0017,MA0065,MA0066,MA0071,MA0072,MA0074\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "RGGTCA",
/* library_name     */ "jaspar_fam",
/* length           */ 6,
/* matrixname       */ jaspar_fam_pwm_5_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -13.596918,
/* max_score        */ 6.427574,
/* threshold        */ 0.944,
/* info content     */ 5.371048,
/* base_counts      */ {160, 106, 221, 113},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_6_matrix_row_0_[10] = { -2.172584, -3.219276,  0.916462, -0.457346,  0.632770, -0.534714,  0.638442, -0.445014,  0.004232, -1.649916};
	float jaspar_fam_pwm_6_matrix_row_1_[10] = {  1.419966,  0.808306, -1.080547, -1.442784, -3.196803, -3.889950, -2.393308, -1.482005, -7.696612, -2.509227};
	float jaspar_fam_pwm_6_matrix_row_2_[10] = { -2.520463, -2.791338, -1.767023, -1.414346, -2.987082, -3.889950, -1.813290, -1.897520,  1.162467,  1.346019};
	float jaspar_fam_pwm_6_matrix_row_3_[10] = { -1.938838,  0.542340, -0.400878,  0.940723,  0.480482,  1.082978,  0.390676,  0.968619, -2.913894, -1.215145};
	float *jaspar_fam_pwm_6_matrix[4] = { jaspar_fam_pwm_6_matrix_row_0_, jaspar_fam_pwm_6_matrix_row_1_, jaspar_fam_pwm_6_matrix_row_2_, jaspar_fam_pwm_6_matrix_row_3_};
	PWM jaspar_fam_pwm_6_ = {
/* accession        */ "MF0008",
/* name             */ "MADS class",
/* label            */ " MF0008	9.66247378819726	MADS class	MADS	; included_models \"MA0001,MA0005,MA0052,MA0082,MA0083\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "CYATMNATGG",
/* library_name     */ "jaspar_fam",
/* length           */ 10,
/* matrixname       */ jaspar_fam_pwm_6_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -30.532967,
/* max_score        */ 9.916751,
/* threshold        */ 0.819,
/* info content     */ 9.651711,
/* base_counts      */ {266, 163, 176, 396},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_7_matrix_row_0_[8] = {  0.790651, -0.988877, -0.202778, -2.675085, -2.887919,  1.258568,  1.129503, -0.175604};
	float jaspar_fam_pwm_7_matrix_row_1_[8] = { -0.951376,  1.131149,  1.086937, -3.764787, -4.113094, -4.438516, -2.143653, -0.925823};
	float jaspar_fam_pwm_7_matrix_row_2_[8] = { -0.370805, -0.241893, -0.973983,  1.460116,  1.486665, -3.219276, -7.696612,  1.067597};
	float jaspar_fam_pwm_7_matrix_row_3_[8] = { -0.652268, -1.911909, -2.051671, -2.295868, -3.568327, -4.570479, -0.954912, -1.994975};
	float *jaspar_fam_pwm_7_matrix[4] = { jaspar_fam_pwm_7_matrix_row_0_, jaspar_fam_pwm_7_matrix_row_1_, jaspar_fam_pwm_7_matrix_row_2_, jaspar_fam_pwm_7_matrix_row_3_};
	PWM jaspar_fam_pwm_7_ = {
/* accession        */ "MF0001",
/* name             */ "ETS class",
/* label            */ " MF0001	8.99242242623466	ETS class	ETS	; included_models \"MA0026,MA0028,MA0062,MA0076,MA0080,MA0081,MA0098\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "ACCGKRAG",
/* library_name     */ "jaspar_fam",
/* length           */ 8,
/* matrixname       */ jaspar_fam_pwm_7_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -27.054901,
/* max_score        */ 9.411186,
/* threshold        */ 0.860,
/* info content     */ 8.982250,
/* base_counts      */ {307, 154, 298, 41},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_8_matrix_row_0_[6] = { -7.937775, -3.620286,  1.272666, -7.937775, -7.937775, -2.605056};
	float jaspar_fam_pwm_8_matrix_row_1_[6] = { -7.696612, -7.696612, -7.696612,  1.452597, -7.696612, -0.756390};
	float jaspar_fam_pwm_8_matrix_row_2_[6] = { -7.696612,  1.483062, -7.696612, -2.692666,  1.513828, -7.696612};
	float jaspar_fam_pwm_8_matrix_row_3_[6] = {  1.272666, -2.499695, -7.937775, -1.835216, -7.937775,  1.140405};
	float *jaspar_fam_pwm_8_matrix[4] = { jaspar_fam_pwm_8_matrix_row_0_, jaspar_fam_pwm_8_matrix_row_1_, jaspar_fam_pwm_8_matrix_row_2_, jaspar_fam_pwm_8_matrix_row_3_};
	PWM jaspar_fam_pwm_8_ = {
/* accession        */ "MF0002",
/* name             */ "bZIP CREB/G-box-like subclass",
/* label            */ " MF0002	10.786011975578	bZIP CREB/G-box-like subclass	bZIP	; included_models \"MA0018,MA0089,MA0096,MA0097\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "TGACGT",
/* library_name     */ "jaspar_fam",
/* length           */ 6,
/* matrixname       */ jaspar_fam_pwm_8_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -47.144325,
/* max_score        */ 8.135223,
/* threshold        */ 0.876,
/* info content     */ 10.765519,
/* base_counts      */ {103, 104, 198, 194},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_9_matrix_row_0_[8] = {  0.784154, -0.251153, -2.010849,  1.096664, -1.936360, -1.571304, -1.709264, -2.281783};
	float jaspar_fam_pwm_9_matrix_row_1_[8] = { -0.595761,  0.465904,  1.418977, -2.934439,  1.085557,  0.156604, -1.670747, -7.696612};
	float jaspar_fam_pwm_9_matrix_row_2_[8] = {  0.043182,  0.187964, -2.692666, -0.933883, -0.043118,  1.125120, -4.752173,  1.462119};
	float jaspar_fam_pwm_9_matrix_row_3_[8] = { -2.060039, -0.522599, -1.989740, -1.485726, -1.054312, -3.718267,  1.174291, -2.544147};
	float *jaspar_fam_pwm_9_matrix[4] = { jaspar_fam_pwm_9_matrix_row_0_, jaspar_fam_pwm_9_matrix_row_1_, jaspar_fam_pwm_9_matrix_row_2_, jaspar_fam_pwm_9_matrix_row_3_};
	PWM jaspar_fam_pwm_9_ = {
/* accession        */ "MF0007",
/* name             */ "bHLH(zip) class",
/* label            */ " MF0007	7.72446306243099	bHLH(zip) class	bHLH(zip)	; included_models \"MA0004,MA0006,MA0048,MA0055,MA0058, MA0059,MA0091,MA0092,MA0093,MA0104\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "ANCACKTG",
/* library_name     */ "jaspar_fam",
/* length           */ 8,
/* matrixname       */ jaspar_fam_pwm_9_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -26.313156,
/* max_score        */ 8.612785,
/* threshold        */ 0.871,
/* info content     */ 7.715985,
/* base_counts      */ {188, 234, 244, 134},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

	float jaspar_fam_pwm_10_matrix_row_0_[7] = {  0.987813,  0.763572, -1.672473,  0.845315,  1.059868, -1.263213, -0.790215};
	float jaspar_fam_pwm_10_matrix_row_1_[7] = { -0.990973, -1.855971, -1.552427, -2.267267, -2.465504, -3.101493, -1.022051};
	float jaspar_fam_pwm_10_matrix_row_2_[7] = { -1.227362, -0.431183, -1.853068, -0.037441, -0.709122, -1.120143, -1.005770};
	float jaspar_fam_pwm_10_matrix_row_3_[7] = { -1.010217, -0.233413,  1.129619, -0.905150, -1.460802,  1.097617,  0.935133};
	float *jaspar_fam_pwm_10_matrix[4] = { jaspar_fam_pwm_10_matrix_row_0_, jaspar_fam_pwm_10_matrix_row_1_, jaspar_fam_pwm_10_matrix_row_2_, jaspar_fam_pwm_10_matrix_row_3_};
	PWM jaspar_fam_pwm_10_ = {
/* accession        */ "MF0010",
/* name             */ "Homeobox class",
/* label            */ " MF0010	6.0772209483364	Homeobox class	Homeo	; included_models \"MA0008,MA0027,MA0046,MA0063,MA0068,MA0070,MA0075,MA0094\" ; medline \"15066426\" ; type \"METAMODEL\" ",
/* motif            */ "AATAACT",
/* library_name     */ "jaspar_fam",
/* length           */ 7,
/* matrixname       */ jaspar_fam_pwm_10_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -13.792716,
/* max_score        */ 6.818936,
/* threshold        */ 0.909,
/* info content     */ 6.071924,
/* base_counts      */ {307, 29, 71, 292},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 100
};

PWM *jaspar_fam[JASPAR_FAM_N] = { &jaspar_fam_pwm_0_, &jaspar_fam_pwm_1_, &jaspar_fam_pwm_2_, &jaspar_fam_pwm_3_, &jaspar_fam_pwm_4_, &jaspar_fam_pwm_5_, &jaspar_fam_pwm_6_, &jaspar_fam_pwm_7_, &jaspar_fam_pwm_8_, &jaspar_fam_pwm_9_, &jaspar_fam_pwm_10_};
extern PWM *jaspar_fam[JASPAR_FAM_N];
