#include <stdlib.h>
#include <string.h>
#include "pwm.h"
#include "library.h"
#include "jaspar_core.h"
#include "jaspar_cne.h"
#include "jaspar_fam.h"
#include "jaspar_phylofacts.h"
#include "jaspar_polII.h"
#include "jaspar_splice.h"
/* pre-compiled JASPAR volumes created with build_library_code: jaspar_core.c, etc., 
 * The PWM structures are loaded with literals with build_volume()
 *
 *
 * */
extern PWM *jaspar_core[JASPAR_CORE_N];
extern PWM *jaspar_cne[JASPAR_CNE_N];
extern PWM *jaspar_fam[JASPAR_FAM_N];
extern PWM *jaspar_phylofacts[JASPAR_PHYLOFACTS_N];
extern PWM *jaspar_splice[JASPAR_SPLICE_N];
extern PWM *jaspar_polII[JASPAR_POLII_N];
PWM **volumes[] = { jaspar_core, jaspar_cne, jaspar_fam, jaspar_phylofacts, jaspar_splice, jaspar_polII };
int volume_sizes[] = { JASPAR_CORE_N, JASPAR_CNE_N, JASPAR_FAM_N, JASPAR_PHYLOFACTS_N, JASPAR_SPLICE_N, JASPAR_POLII_N};
int nvolumes = sizeof(volumes)/sizeof(volumes[0]);

PWM** build_volume(int v_i)
{
    PWM **out = calloc(volume_sizes[v_i], sizeof(PWM*));
    memcpy(out, volumes[v_i], sizeof(PWM*) * volume_sizes[v_i]);
    return out;
}


int search_volumes_index(char *name)
{
    // set static variables
    int len = strlen(name);

    if (strncasecmp(name, "core", len)==0)        return 0;
    if (strncasecmp(name, "jaspar_core", len)==0) return 0; 

    if (strncasecmp(name, "cne", len)==0)         return 1;
    if (strncasecmp(name, "jaspar_cne", len)==0)  return 1; 

    if (strncasecmp(name, "fam", len)==0)         return 2; 
    if (strncasecmp(name, "jaspar_fam", len)==0)  return 2; 

    if (strncasecmp(name, "phylofacts", len)==0)  return 3; 
    if (strncasecmp(name, "jaspar_phylofacts", len)==0) return 3;

    if (strncasecmp(name, "splice", len)==0)        return 4;
    if (strncasecmp(name, "jaspar_splice", len)==0) return 4;

    if (strncasecmp(name, "polII", len)==0)         return 5; 
    if (strncasecmp(name, "jaspar_polII", len)==0)  return 5; 

    return -1;
}

PWM** search_volume_name(char *name, int *size)
{
    *size = 0;
    int volume_i = search_volumes_index(name);
    if (volume_i < 0) 
        return NULL;

    *size = volume_sizes[volume_i];
    return volumes[volume_i];
}

PWM** build_static_library(int *total_n)
{
    *total_n = JASPAR_CORE_N + JASPAR_CNE_N + JASPAR_FAM_N + JASPAR_PHYLOFACTS_N + JASPAR_POLII_N + JASPAR_SPLICE_N;

    PWM **merge = calloc(*total_n, sizeof(PWM*));
    int amount_added = 0;
    int i;
    for (i = 0; i < nvolumes; i++)
    {
        memcpy( &(merge[amount_added]), volumes[i], sizeof(PWM*) * volume_sizes[i]);
        amount_added += volume_sizes[i];
    }
    sort_library(merge, *total_n, "accession");
    return merge;
}


/* ************ General routines **************
 */
void zeromem(void *base, size_t nbytes)
{
    int i = 0;
    unsigned char *array = (unsigned char*)base;
    while (i < nbytes) array[i++] = 0;
}
int condense_array(void *base, size_t nmemb, size_t size, int(*compar)(const void *, const void *))
{ /* I wrote this. Beware how you use it, the objects in the array must not be sole pointers to memory that needs to be freed. 
    */
    int i;
    size_t nrmemb = 1;
    for (i = nmemb - 2; i >= 0; i--)
    {
        void *a = base + size*i;
        void *b = base + size*(i + 1);
        if (compar(a, b) == 0)
            memmove(a,b, size*nrmemb);
        else
            nrmemb++;
    }
    zeromem(base + size*(nrmemb), size*(nmemb - nrmemb));
    return nrmemb;
}
// basic methods for comparisons on pointer array items
static int by_length(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    return p1->length < p2->length ? -1 : 1;
}
static int by_information_content(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    return p1->information_content < p2->information_content ? -1 : 1;
}
static int by_accession(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    return strcmp(p1->accession, p2->accession);
}
static int by_name(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    return strcmp(p1->name, p2->name);
}
static int by_label(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    return strcmp(p1->label, p2->label);
}
static int by_motif(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    return strcmp(p1->motif, p2->motif);
}
static int by_pointer(const void *a, const void *b)
{
    PWM * p1 = *(PWM**)a;
    PWM * p2 = *(PWM**)b;
    if (p1 != p2) return p1 < p2 ? -1 : 1;
    return 0;
}
int condense_pointer_array(PWM **base, size_t nmemb)
{
    qsort(base, nmemb, sizeof(PWM*), by_pointer);
    return condense_array(base, nmemb, sizeof(PWM*), by_pointer);
}

void sort_library(PWM **lib, int npwm, char* by)
{
    if (0 == strcmp(by, "length")) qsort(lib, npwm, sizeof(PWM*), by_length);
    else if (0 == strcmp(by, "name")) qsort(lib, npwm, sizeof(PWM*), by_name);
    else if (0 == strcmp(by, "label")) qsort(lib, npwm, sizeof(PWM*), by_label);
    else if (0 == strcmp(by, "accession")) qsort(lib, npwm, sizeof(PWM*), by_accession);
    else if (0 == strcmp(by, "motif")) qsort(lib, npwm, sizeof(PWM*), by_motif);
    else if (0 == strcmp(by, "information_content")) qsort(lib, npwm, sizeof(PWM*), by_information_content);
}

PWM *retrieve_library_accession(PWM **lib, int npwm, char* accession)
{
    PWM key, *keyp, **res;
    strcpy(key.accession, accession);
    keyp = &key;
    res = bsearch(&keyp, lib, npwm, sizeof(PWM *), by_accession);
    if (res) return *res;

    return NULL;
}

