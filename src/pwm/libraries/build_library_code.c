#define _GNU_SOURCE // for getline
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "matrix.h"
#include "pwm.h"
#include "pwm_readers.h"
void print_matrix_code(FILE* out, char* obj_name, float **matrix, int nrows, int ncols);
void quote_label_in_place(longline label)
{
    longline scratch;
    int a,b;
    a=b=0;
    while(label[a] != 0)
    {
        if (label[a] == '\'') scratch[b++] = '\\';
        else if (label[a] == '\"') scratch[b++] = '\\';
        else if (label[a] == '\\') scratch[b++] = '\\';
        scratch[b++] = label[a++];
    }
    memcpy(label, scratch, sizeof(char) * b);
}

char *strtoupper(char *str)
{
    int i;
    int len = strlen(str);
    char *upt = calloc(len+1, sizeof(char));
    for(i=0; i< len; i++) upt[i] = toupper(str[i]);
    return upt;
}

int main(int argc, char** argv)
{
    int i=0;

    if(argc < 2)
    {
        fprintf(stderr,"%s jasparfile.wmx library_name\n", argv[0]);
        return 1;
    }

    int npwm = 0;
    PWM **pwmlist = read_pwm_file(argv[1], &npwm);
    fprintf(stderr, "read %d from %s\n", npwm, argv[1]);

    // output filename
    char *library_name = argv[2];
    char *outfilename = calloc(strlen(library_name) + 3, sizeof(char));
    sprintf(outfilename, "%s.c", library_name);
    FILE *cfile = fopen(outfilename, "w");
    sprintf(outfilename, "%s.h", library_name);
    FILE *headerfile = fopen(outfilename, "w");
    free(outfilename);

    // create an uppcase lib_NPWM
    char *uppercase = strtoupper(library_name);
    char* NPWM_name = NULL;
    asprintf(&NPWM_name, "%s_N", uppercase);
    free(uppercase);

    //includes and defines
    fprintf(cfile, "#include \"pwm.h\"\n");
    fprintf(cfile, "#include \"%s.h\"\n", library_name);
    fprintf(headerfile, "#define %s %d\n", NPWM_name, npwm);

    // stamp it by time
    time_t t;
    t = time(&t);
    fprintf( cfile, "/* created by %s using %s on %s*/\n", argv[0], argv[1], asctime(localtime(&t)));
    // root variable name
    char *variable_template = NULL;
    asprintf(&variable_template, "%s_pwm_%%d", library_name);

    int pwmi;
    PWM *pwm = NULL;

    for(pwmi=0; pwmi < npwm; pwmi++)
    {
        pwm = pwmlist[pwmi];

        char * objname = NULL;
        char * matrixname = NULL;
        asprintf(&objname, variable_template, i);
        asprintf(&matrixname, "%s_matrix", objname);
        print_matrix_code(cfile, matrixname, pwm->matrix, 4, pwm->length);

        // make pwm object in a single declaration
        quote_label_in_place(pwm->label);
        fprintf(cfile, "\tPWM %s_ = {\n",objname);
        fprintf(cfile, "\
/* accession        */ \"%s\",\n\
/* name             */ \"%s\",\n\
/* label            */ \"%s\",\n\
/* motif            */ \"%s\",\n\
/* library_name     */ \"%s\",\n\
/* length           */ %d,\n\
/* matrixname       */ %s,\n\
", pwm->accession, pwm->name, pwm->label, pwm->motif, library_name, pwm->length, matrixname);

        fprintf(cfile, "\
/* background       */ { %.3f, %.3f, %.3f, %.3f},\n", pwm->background[0], pwm->background[1], pwm->background[2], pwm->background[3]);
        fprintf(cfile, "\
/* min_score        */ %f,\n\
/* max_score        */ %f,\n\
/* threshold        */ %.3f,\n\
/* info content     */ %f,\n\
", pwm->min_score, pwm->max_score, pwm->threshold, pwm->information_content);
        fprintf(cfile, "\
/* base_counts      */ {%.0f, %.0f, %.0f, %.0f},\n", pwm->base_counts[0], pwm->base_counts[1], pwm->base_counts[2], pwm->base_counts[3]);
        fprintf(cfile, "\
/* base_pseudocounts*/ { %.3f, %.3f, %.3f, %.3f},\n", pwm->base_pseudocounts[0], pwm->base_pseudocounts[1], pwm->base_pseudocounts[2], pwm->base_pseudocounts[3]);
        fprintf(cfile, "\
/* index            */ NULL,\n\
/* Nsites           */ %d\n", pwm->Nsites);
        fprintf(cfile, "};\n\n");

        free(objname);
        free(matrixname);
        i++;
    }
    fprintf( cfile, "PWM *%s[%s] = {", library_name, NPWM_name);
    for(pwmi=0; pwmi < npwm; pwmi++)
    {
        fprintf(cfile, " &%s_pwm_%d_", library_name, pwmi);
        if(pwmi < npwm-1) fputc(',', cfile);
        pwm = pwmlist[pwmi];
        free_pwm(&pwm);
    }
    fprintf(cfile, "};\n");

    fprintf(cfile, "extern PWM *%s[%s];\n", library_name, NPWM_name);

    fclose(cfile);
    fclose(headerfile);
    free(pwmlist);

    free(variable_template);
    free(NPWM_name);
    return 0;
}
void print_matrix_code(FILE* out, char* obj_name, float **matrix, int nrows, int ncols)
{
    int i,j;
    for (i=0; i<nrows; i++) 
    {
        fprintf(out, "\tfloat %s_row_%d_[%d] = {", obj_name, i, ncols);
        for(j=0; j<ncols; j++)
        {
            fprintf(out, " % f", matrix[i][j]);
            if (j < ncols-1) fputc(',', out);
        }
        fprintf(out, "};\n");
    }

    fprintf(out, "\tfloat *%s[%d] = {", obj_name, nrows);
    for (i=0; i<nrows; i++) 
    {
        fprintf(out, " %s_row_%d_", obj_name, i);
        if (i < nrows-1) fputc(',', out);
    }
    fprintf(out, "};\n");
}
