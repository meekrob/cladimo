#include "pwm.h"
#include "jaspar_phylofacts.h"
/* created by ./build_library_code using JASPAR_PHYLOFACTS_2008.wmx on Tue Mar  3 16:19:03 2009
*/
	float jaspar_phylofacts_pwm_0_matrix_row_0_[11] = { -9.681414, -9.681414,  1.272913,  0.964035, -9.681414, -0.384804,  0.433186, -9.681414,  1.272913,  1.272913,  0.558582};
	float jaspar_phylofacts_pwm_0_matrix_row_1_[11] = {  1.514075,  1.514075, -9.440251, -9.440251,  0.001280,  0.282973, -4.825131, -9.440251, -9.440251, -9.440251, -9.440251};
	float jaspar_phylofacts_pwm_0_matrix_row_2_[11] = { -9.440251, -9.440251, -9.440251, -9.440251, -9.440251,  0.168932,  0.502505,  1.514075, -9.440251, -9.440251,  0.841707};
	float jaspar_phylofacts_pwm_0_matrix_row_3_[11] = { -9.681414, -9.681414, -9.681414, -0.052297,  1.024098, -0.085743, -0.322567, -9.681414, -9.681414, -9.681414, -9.681414};
	float *jaspar_phylofacts_pwm_0_matrix[4] = { jaspar_phylofacts_pwm_0_matrix_row_0_, jaspar_phylofacts_pwm_0_matrix_row_1_, jaspar_phylofacts_pwm_0_matrix_row_2_, jaspar_phylofacts_pwm_0_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_0_ = {
/* accession        */ "PF0124",
/* name             */ "CCAWYNNGAAR",
/* label            */ " PF0124	15.8840652682399	CCAWYNNGAAR	Unknown	; MCS \"11.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCAATNRGAAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_0_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.101494,
/* max_score        */ 11.976284,
/* threshold        */ 0.785,
/* info content     */ 15.876749,
/* base_counts      */ {2772, 1438, 1221, 861},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 572
};

	float jaspar_phylofacts_pwm_1_matrix_row_0_[9] = { -9.722512, -9.722512,  1.272915, -9.722512, -9.722512, -0.897687,  1.272915, -9.722512, -9.722512};
	float jaspar_phylofacts_pwm_1_matrix_row_1_[9] = { -9.481350, -9.481350, -9.481350,  1.152123, -9.481350,  1.392935, -9.481350, -9.481350,  1.514077};
	float jaspar_phylofacts_pwm_1_matrix_row_2_[9] = { -9.481350,  1.514077, -9.481350,  0.322372, -9.481350, -9.481350, -9.481350,  1.514077, -9.481350};
	float jaspar_phylofacts_pwm_1_matrix_row_3_[9] = {  1.272915, -9.722512, -9.722512, -9.722512,  1.272915, -9.722512, -9.722512, -9.722512, -9.722512};
	float *jaspar_phylofacts_pwm_1_matrix[4] = { jaspar_phylofacts_pwm_1_matrix_row_0_, jaspar_phylofacts_pwm_1_matrix_row_1_, jaspar_phylofacts_pwm_1_matrix_row_2_, jaspar_phylofacts_pwm_1_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_1_ = {
/* accession        */ "PF0032",
/* name             */ "TGASTMAGC",
/* label            */ " PF0032	16.6021012093703	TGASTMAGC	Unknown	; MCS \"27.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"NF-E2\" ; type \"phylogenetic\" ",
/* motif            */ "TGACTCAGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_1_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.502609,
/* max_score        */ 12.178951,
/* threshold        */ 0.792,
/* info content     */ 16.594845,
/* base_counts      */ {1260, 1539, 1373, 1192},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 596
};

	float jaspar_phylofacts_pwm_2_matrix_row_0_[8] = {  0.783046,  1.272927, -9.981241, -9.981241,  1.272927, -9.981241,  1.272927,  0.632767};
	float jaspar_phylofacts_pwm_2_matrix_row_1_[8] = { -9.740079, -9.740079,  1.514089,  1.514089, -9.740079,  1.514089, -9.740079, -9.740079};
	float jaspar_phylofacts_pwm_2_matrix_row_2_[8] = {  0.565568, -9.740079, -9.740079, -9.740079, -9.740079, -9.740079, -9.740079,  0.765016};
	float jaspar_phylofacts_pwm_2_matrix_row_3_[8] = { -9.981241, -9.981241, -9.981241, -9.981241, -9.981241, -9.981241, -9.981241, -9.981241};
	float *jaspar_phylofacts_pwm_2_matrix[4] = { jaspar_phylofacts_pwm_2_matrix_row_0_, jaspar_phylofacts_pwm_2_matrix_row_1_, jaspar_phylofacts_pwm_2_matrix_row_2_, jaspar_phylofacts_pwm_2_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_2_ = {
/* accession        */ "PF0111",
/* name             */ "RACCACAR",
/* label            */ " PF0111	14.0390973145781	RACCACAR	Unknown	; MCS \"12.3\" ; jaspar \"RUNX1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"AML\" ; type \"phylogenetic\" ",
/* motif            */ "AACCACAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_2_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -79.849922,
/* max_score        */ 9.909110,
/* threshold        */ 0.818,
/* info content     */ 14.034110,
/* base_counts      */ {3196, 2316, 664, 0},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 772
};

	float jaspar_phylofacts_pwm_3_matrix_row_0_[7] = { -10.173862,  1.272934, -10.173862,  1.272934,  1.272934, -10.173862,  0.569056};
	float jaspar_phylofacts_pwm_3_matrix_row_1_[7] = { -9.932701, -9.932701, -9.932701, -9.932701, -9.932701, -9.932701, -9.932701};
	float jaspar_phylofacts_pwm_3_matrix_row_2_[7] = {  1.514096, -9.932701, -9.932701, -9.932701, -9.932701,  1.514096,  0.831586};
	float jaspar_phylofacts_pwm_3_matrix_row_3_[7] = { -10.173862, -10.173862,  1.272934, -10.173862, -10.173862, -10.173862, -10.173862};
	float *jaspar_phylofacts_pwm_3_matrix[4] = { jaspar_phylofacts_pwm_3_matrix_row_0_, jaspar_phylofacts_pwm_3_matrix_row_1_, jaspar_phylofacts_pwm_3_matrix_row_2_, jaspar_phylofacts_pwm_3_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_3_ = {
/* accession        */ "PF0131",
/* name             */ "GATAAGR",
/* label            */ " PF0131	13.0000823381506	GATAAGR	Unknown	; MCS \"11.2\" ; jaspar \"Evi1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"GATA-X\" ; type \"phylogenetic\" ",
/* motif            */ "GATAAGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_3_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -71.217041,
/* max_score        */ 8.951512,
/* threshold        */ 0.838,
/* info content     */ 12.996267,
/* base_counts      */ {3271, 0, 2345, 936},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 936
};

	float jaspar_phylofacts_pwm_4_matrix_row_0_[7] = {  0.493299, -11.311557,  1.272955,  1.272955,  1.272955, -11.311557,  1.272955};
	float jaspar_phylofacts_pwm_4_matrix_row_1_[7] = { -11.070395, -11.070395, -11.070395, -11.070395, -11.070395,  1.514117, -11.070395};
	float jaspar_phylofacts_pwm_4_matrix_row_2_[7] = {  0.900594, -11.070395, -11.070395, -11.070395, -11.070395, -11.070395, -11.070395};
	float jaspar_phylofacts_pwm_4_matrix_row_3_[7] = { -11.311557,  1.272955, -11.311557, -11.311557, -11.311557, -11.311557, -11.311557};
	float *jaspar_phylofacts_pwm_4_matrix[4] = { jaspar_phylofacts_pwm_4_matrix_row_0_, jaspar_phylofacts_pwm_4_matrix_row_1_, jaspar_phylofacts_pwm_4_matrix_row_2_, jaspar_phylofacts_pwm_4_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_4_ = {
/* accession        */ "PF0036",
/* name             */ "RTAAACA",
/* label            */ " PF0036	13.0049602985846	RTAAACA	Unknown	; MCS \"25.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"FREAC-2\" ; type \"phylogenetic\" ",
/* motif            */ "GTAAACA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_4_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -79.180893,
/* max_score        */ 8.779490,
/* threshold        */ 0.838,
/* info content     */ 13.003625,
/* base_counts      */ {13019, 2920, 1581, 2920},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2920
};

	float jaspar_phylofacts_pwm_5_matrix_row_0_[11] = { -9.137460, -0.543121, -9.137460, -9.137460,  1.272875, -1.641363, -0.921102,  0.255285, -9.137460, -9.137460, -9.137460};
	float jaspar_phylofacts_pwm_5_matrix_row_1_[11] = {  1.093413, -8.896298, -8.896298,  1.514037, -8.896298,  0.664770,  0.591750, -8.896298,  1.514037, -8.896298,  1.514037};
	float jaspar_phylofacts_pwm_5_matrix_row_2_[11] = { -8.896298,  1.336529, -8.896298, -8.896298, -8.896298,  0.362928,  0.462549,  1.065506, -8.896298,  1.514037, -8.896298};
	float jaspar_phylofacts_pwm_5_matrix_row_3_[11] = {  0.203996, -9.137460,  1.272875, -9.137460, -9.137460, -0.327448, -0.681930, -9.137460, -9.137460, -9.137460, -9.137460};
	float *jaspar_phylofacts_pwm_5_matrix[4] = { jaspar_phylofacts_pwm_5_matrix_row_0_, jaspar_phylofacts_pwm_5_matrix_row_1_, jaspar_phylofacts_pwm_5_matrix_row_2_, jaspar_phylofacts_pwm_5_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_5_ = {
/* accession        */ "PF0115",
/* name             */ "YRTCANNRCGC",
/* label            */ " PF0115	15.9330467030515	YRTCANNRCGC	Unknown	; MCS \"12.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CGTCANNGCGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_5_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.799599,
/* max_score        */ 13.353868,
/* threshold        */ 0.785,
/* info content     */ 15.921113,
/* base_counts      */ {561, 1488, 1043, 560},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 332
};

	float jaspar_phylofacts_pwm_6_matrix_row_0_[10] = { -9.674397, -9.674397, -9.674397, -9.674397, -1.634916, -9.674397,  0.873600, -9.674397, -9.674397, -9.674397};
	float jaspar_phylofacts_pwm_6_matrix_row_1_[10] = { -9.433234,  1.514075,  1.514075, -9.433234,  1.003848,  1.202645, -9.433234, -9.433234, -9.433234, -9.433234};
	float jaspar_phylofacts_pwm_6_matrix_row_2_[10] = {  1.088165, -9.433234, -9.433234,  1.514075,  0.162437,  0.195882, -9.433234, -9.433234, -9.433234, -9.433234};
	float jaspar_phylofacts_pwm_6_matrix_row_3_[10] = {  0.214028, -9.674397, -9.674397, -9.674397, -1.177202, -9.674397,  0.161936,  1.272913,  1.272913,  1.272913};
	float *jaspar_phylofacts_pwm_6_matrix[4] = { jaspar_phylofacts_pwm_6_matrix_row_0_, jaspar_phylofacts_pwm_6_matrix_row_1_, jaspar_phylofacts_pwm_6_matrix_row_2_, jaspar_phylofacts_pwm_6_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_6_ = {
/* accession        */ "PF0157",
/* name             */ "KCCGNSWTTT",
/* label            */ " PF0157	15.8360959354586	KCCGNSWTTT	Unknown	; MCS \"10.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GCCGCCATTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_6_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.463318,
/* max_score        */ 12.529222,
/* threshold        */ 0.791,
/* info content     */ 15.828808,
/* base_counts      */ {412, 1893, 1238, 2137},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 568
};

	float jaspar_phylofacts_pwm_7_matrix_row_0_[12] = { -8.707668,  1.272827,  1.272827,  0.397423, -8.707668, -0.734857, -8.707668, -8.707668,  1.272827, -8.707668, -0.094983, -8.707668};
	float jaspar_phylofacts_pwm_7_matrix_row_1_[12] = { -8.466506, -8.466506, -8.466506,  0.265960,  0.743934,  0.692646,  0.010073, -8.466506, -8.466506,  1.513989,  0.545505,  0.792720};
	float jaspar_phylofacts_pwm_7_matrix_row_2_[12] = {  1.513989, -8.466506, -8.466506, -0.364525, -8.466506, -0.223487, -8.466506,  1.513989, -8.466506, -8.466506, -0.565129, -8.466506};
	float jaspar_phylofacts_pwm_7_matrix_row_3_[12] = { -8.707668, -8.707668, -8.707668, -0.668188,  0.651179, -0.113329,  1.021526, -8.707668, -8.707668, -8.707668, -0.151062,  0.607122};
	float *jaspar_phylofacts_pwm_7_matrix[4] = { jaspar_phylofacts_pwm_7_matrix_row_0_, jaspar_phylofacts_pwm_7_matrix_row_1_, jaspar_phylofacts_pwm_7_matrix_row_2_, jaspar_phylofacts_pwm_7_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_7_ = {
/* accession        */ "PF0137",
/* name             */ "GAANYNYGACNY",
/* label            */ " PF0137	15.6273819419734	GAANYNYGACNY	Unknown	; MCS \"11.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GAANTNTGACNT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_7_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.337189,
/* max_score        */ 12.554201,
/* threshold        */ 0.780,
/* info content     */ 15.609846,
/* base_counts      */ {822, 708, 530, 532},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 216
};

	float jaspar_phylofacts_pwm_8_matrix_row_0_[8] = { -9.645825, -9.645825,  1.272911,  1.272911, -9.645825, -9.645825,  1.272911,  1.272911};
	float jaspar_phylofacts_pwm_8_matrix_row_1_[8] = {  0.523566, -9.404663, -9.404663, -9.404663, -9.404663, -9.404663, -9.404663, -9.404663};
	float jaspar_phylofacts_pwm_8_matrix_row_2_[8] = { -9.404663, -9.404663, -9.404663, -9.404663, -9.404663, -9.404663, -9.404663, -9.404663};
	float jaspar_phylofacts_pwm_8_matrix_row_3_[8] = {  0.808699,  1.272911, -9.645825, -9.645825,  1.272911,  1.272911, -9.645825, -9.645825};
	float *jaspar_phylofacts_pwm_8_matrix[4] = { jaspar_phylofacts_pwm_8_matrix_row_0_, jaspar_phylofacts_pwm_8_matrix_row_1_, jaspar_phylofacts_pwm_8_matrix_row_2_, jaspar_phylofacts_pwm_8_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_8_ = {
/* accession        */ "PF0061",
/* name             */ "YTAATTAA",
/* label            */ " PF0061	15.0482766179379	YTAATTAA	Unknown	; MCS \"19.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"LHX3\" ; type \"phylogenetic\" ",
/* motif            */ "TTAATTAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_8_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.166603,
/* max_score        */ 9.719079,
/* threshold        */ 0.809,
/* info content     */ 15.041143,
/* base_counts      */ {2208, 205, 0, 2003},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 552
};

	float jaspar_phylofacts_pwm_9_matrix_row_0_[12] = {  0.644739, -10.697410, -10.697410, -10.697410, -0.512472, -0.044844, -0.076058,  0.100143,  0.045509,  1.272947,  1.272947,  1.272947};
	float jaspar_phylofacts_pwm_9_matrix_row_1_[12] = { -10.456248, -10.456248, -10.456248, -10.456248, -0.205595,  0.059746,  0.065151, -0.609278, -0.039907, -10.456248, -10.456248, -10.456248};
	float jaspar_phylofacts_pwm_9_matrix_row_2_[12] = { -10.456248,  1.514109, -10.456248, -10.456248,  0.032273,  0.037828, -0.313704,  0.280171,  0.155300, -10.456248, -10.456248, -10.456248};
	float jaspar_phylofacts_pwm_9_matrix_row_3_[12] = {  0.510362, -10.697410,  1.272947,  1.272947,  0.419520, -0.035432,  0.208564, -0.000907, -0.159968, -10.697410, -10.697410, -10.697410};
	float *jaspar_phylofacts_pwm_9_matrix[4] = { jaspar_phylofacts_pwm_9_matrix_row_0_, jaspar_phylofacts_pwm_9_matrix_row_1_, jaspar_phylofacts_pwm_9_matrix_row_2_, jaspar_phylofacts_pwm_9_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_9_ = {
/* accession        */ "PF0155",
/* name             */ "WGTTNNNNNAAA",
/* label            */ " PF0155	13.2589289259166	WGTTNNNNNAAA	Unknown	; MCS \"10.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AGTTNNNNNAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_9_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.280983,
/* max_score        */ 9.646883,
/* threshold        */ 0.782,
/* info content     */ 13.256558,
/* base_counts      */ {7633, 1546, 3420, 6361},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1580
};

	float jaspar_phylofacts_pwm_10_matrix_row_0_[8] = { -10.335306, -10.335306,  1.272938, -10.335306, -10.335306, -10.335306, -10.335306,  1.272938};
	float jaspar_phylofacts_pwm_10_matrix_row_1_[8] = { -10.094145, -10.094145, -10.094145,  1.514100, -10.094145, -10.094145,  1.514100, -10.094145};
	float jaspar_phylofacts_pwm_10_matrix_row_2_[8] = { -10.094145,  1.514100, -10.094145, -10.094145,  1.514100, -10.094145, -10.094145, -10.094145};
	float jaspar_phylofacts_pwm_10_matrix_row_3_[8] = {  1.272938, -10.335306, -10.335306, -10.335306, -10.335306,  1.272938, -10.335306, -10.335306};
	float *jaspar_phylofacts_pwm_10_matrix[4] = { jaspar_phylofacts_pwm_10_matrix_row_0_, jaspar_phylofacts_pwm_10_matrix_row_1_, jaspar_phylofacts_pwm_10_matrix_row_2_, jaspar_phylofacts_pwm_10_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_10_ = {
/* accession        */ "PF0014",
/* name             */ "TGACGTCA",
/* label            */ " PF0014	16	TGACGTCA	Unknown	; MCS \"44.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"ATF3\" ; type \"phylogenetic\" ",
/* motif            */ "TGACGTCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_10_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.682442,
/* max_score        */ 11.148155,
/* threshold        */ 0.801,
/* info content     */ 15.996032,
/* base_counts      */ {2200, 2200, 2200, 2200},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1100
};

	float jaspar_phylofacts_pwm_11_matrix_row_0_[8] = {  0.533545, -0.113329, -11.341249, -11.341249, -11.341249, -11.341249, -11.341249, -11.341249};
	float jaspar_phylofacts_pwm_11_matrix_row_1_[8] = { -11.100086,  0.051883, -11.100086, -11.100086, -11.100086, -11.100086, -11.100086,  1.514118};
	float jaspar_phylofacts_pwm_11_matrix_row_2_[8] = {  0.865194,  0.523494,  1.514118, -11.100086,  1.514118,  1.514118,  1.514118, -11.100086};
	float jaspar_phylofacts_pwm_11_matrix_row_3_[8] = { -11.341249, -0.644746, -11.341249,  1.272956, -11.341249, -11.341249, -11.341249, -11.341249};
	float *jaspar_phylofacts_pwm_11_matrix[4] = { jaspar_phylofacts_pwm_11_matrix_row_0_, jaspar_phylofacts_pwm_11_matrix_row_1_, jaspar_phylofacts_pwm_11_matrix_row_2_, jaspar_phylofacts_pwm_11_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_11_ = {
/* accession        */ "PF0140",
/* name             */ "RNGTGGGC",
/* label            */ " PF0140	13.0753988391757	RNGTGGGC	Unknown	; MCS \"10.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GNGTGGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_11_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.033485,
/* max_score        */ 10.232234,
/* threshold        */ 0.826,
/* info content     */ 13.074099,
/* base_counts      */ {2188, 3705, 14721, 3450},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3008
};

	float jaspar_phylofacts_pwm_12_matrix_row_0_[9] = {  1.272888, -9.293313, -9.293313, -9.293313,  0.504870,  0.107730, -9.293313, -9.293313, -9.293313};
	float jaspar_phylofacts_pwm_12_matrix_row_1_[9] = { -9.052151, -9.052151,  1.514050,  0.914358, -9.052151, -9.052151, -9.052151, -9.052151,  1.514050};
	float jaspar_phylofacts_pwm_12_matrix_row_2_[9] = { -9.052151,  1.514050, -9.052151, -9.052151,  0.890605, -9.052151, -9.052151, -9.052151, -9.052151};
	float jaspar_phylofacts_pwm_12_matrix_row_3_[9] = { -9.293313, -9.293313, -9.293313,  0.476700, -9.293313,  0.899143,  1.272888,  1.272888, -9.293313};
	float *jaspar_phylofacts_pwm_12_matrix[4] = { jaspar_phylofacts_pwm_12_matrix_row_0_, jaspar_phylofacts_pwm_12_matrix_row_1_, jaspar_phylofacts_pwm_12_matrix_row_2_, jaspar_phylofacts_pwm_12_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_12_ = {
/* accession        */ "PF0135",
/* name             */ "AGCYRWTTC",
/* label            */ " PF0135	15.1153861586506	AGCYRWTTC	Unknown	; MCS \"11.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AGCCGTTTC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_12_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.398651,
/* max_score        */ 11.064923,
/* threshold        */ 0.802,
/* info content     */ 15.105202,
/* base_counts      */ {689, 989, 596, 1218},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 388
};

	float jaspar_phylofacts_pwm_13_matrix_row_0_[9] = {  0.620290, -10.875499,  1.272950, -10.875499, -10.875499,  1.272950,  1.272950,  0.028639, -10.875499};
	float jaspar_phylofacts_pwm_13_matrix_row_1_[9] = { -10.634337, -10.634337, -10.634337, -10.634337, -10.634337, -10.634337, -10.634337, -10.634337,  0.703056};
	float jaspar_phylofacts_pwm_13_matrix_row_2_[9] = {  0.778779,  1.514112, -10.634337,  1.514112,  1.514112, -10.634337, -10.634337,  1.174246, -10.634337};
	float jaspar_phylofacts_pwm_13_matrix_row_3_[9] = { -10.875499, -10.875499, -10.875499, -10.875499, -10.875499, -10.875499, -10.875499, -10.875499,  0.685273};
	float *jaspar_phylofacts_pwm_13_matrix[4] = { jaspar_phylofacts_pwm_13_matrix_row_0_, jaspar_phylofacts_pwm_13_matrix_row_1_, jaspar_phylofacts_pwm_13_matrix_row_2_, jaspar_phylofacts_pwm_13_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_13_ = {
/* accession        */ "PF0050",
/* name             */ "RGAGGAARY",
/* label            */ " PF0050	15.1438737744164	RGAGGAARY	Unknown	; MCS \"22.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"PU.1\" ; type \"phylogenetic\" ",
/* motif            */ "AGAGGAAGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_13_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.879478,
/* max_score        */ 11.017265,
/* threshold        */ 0.802,
/* info content     */ 15.141489,
/* base_counts      */ {7191, 839, 7913, 1049},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1888
};

	float jaspar_phylofacts_pwm_14_matrix_row_0_[11] = { -9.240396, -9.240396, -9.240396, -0.873793,  1.272884, -0.400975, -9.240396, -9.240396, -9.240396, -0.317605, -9.240396};
	float jaspar_phylofacts_pwm_14_matrix_row_1_[11] = {  1.514046, -8.999234,  1.514046,  1.196960, -8.999234,  0.434330, -8.999234, -8.999234, -8.999234,  0.350955,  1.135405};
	float jaspar_phylofacts_pwm_14_matrix_row_2_[11] = { -8.999234, -8.999234, -8.999234, -1.397832, -8.999234,  0.376705,  1.514046, -8.999234,  1.514046,  0.315556, -8.999234};
	float jaspar_phylofacts_pwm_14_matrix_row_3_[11] = { -9.240396,  1.272884, -9.240396, -1.024038, -9.240396, -0.609696, -9.240396,  1.272884, -9.240396, -0.430384,  0.118450};
	float *jaspar_phylofacts_pwm_14_matrix[4] = { jaspar_phylofacts_pwm_14_matrix_row_0_, jaspar_phylofacts_pwm_14_matrix_row_1_, jaspar_phylofacts_pwm_14_matrix_row_2_, jaspar_phylofacts_pwm_14_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_14_ = {
/* accession        */ "PF0171",
/* name             */ "CTCNANGTGNY",
/* label            */ " PF0171	15.9617462877539	CTCNANGTGNY	Unknown	; MCS \"9.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTCCANGTGNC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_14_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.361076,
/* max_score        */ 12.992488,
/* threshold        */ 0.785,
/* info content     */ 15.951212,
/* base_counts      */ {555, 1496, 985, 1012},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 368
};

	float jaspar_phylofacts_pwm_15_matrix_row_0_[13] = {  0.338570, -8.456407, -8.456407,  1.272787,  1.272787,  0.567725, -0.240048, -0.089804, -2.239800, -8.456407, -8.456407,  1.272787, -8.456407};
	float jaspar_phylofacts_pwm_15_matrix_row_1_[13] = { -8.215244,  0.127833, -8.215244, -8.215244, -8.215244, -8.215244, -0.054441, -0.665109,  0.360406, -8.215244, -8.215244, -8.215244,  0.922633};
	float jaspar_phylofacts_pwm_15_matrix_row_2_[13] = {  1.014997, -8.215244, -8.215244, -8.215244, -8.215244, -8.215244,  0.360406,  0.261335,  0.053744, -8.215244,  1.513949, -8.215244, -8.215244};
	float jaspar_phylofacts_pwm_15_matrix_row_3_[13] = { -8.456407,  0.985125,  1.272787, -8.456407, -8.456407,  0.591533, -0.089804,  0.209379,  0.411584,  1.272787, -8.456407, -8.456407,  0.466385};
	float *jaspar_phylofacts_pwm_15_matrix[4] = { jaspar_phylofacts_pwm_15_matrix_row_0_, jaspar_phylofacts_pwm_15_matrix_row_1_, jaspar_phylofacts_pwm_15_matrix_row_2_, jaspar_phylofacts_pwm_15_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_15_ = {
/* accession        */ "PF0133",
/* name             */ "RYTAAWNNNTGAY",
/* label            */ " PF0133	16.6548422231793	RYTAAWNNNTGAY	Unknown	; MCS \"11.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTTAATNNNTGAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_15_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.467857,
/* max_score        */ 12.425498,
/* threshold        */ 0.775,
/* info content     */ 16.631025,
/* base_counts      */ {738, 242, 410, 794},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 168
};

	float jaspar_phylofacts_pwm_16_matrix_row_0_[11] = { -8.761725,  1.272834, -8.761725, -0.205119, -8.761725, -8.761725, -8.761725, -0.545367,  1.272834,  1.272834,  1.272834};
	float jaspar_phylofacts_pwm_16_matrix_row_1_[11] = {  1.513996, -8.520563, -8.520563,  0.928873, -0.359760, -8.520563, -8.520563,  0.584527, -8.520563, -8.520563, -8.520563};
	float jaspar_phylofacts_pwm_16_matrix_row_2_[11] = { -8.520563, -8.520563,  1.513996, -0.513863, -8.520563,  1.513996,  0.888710,  0.415472, -8.520563, -8.520563, -8.520563};
	float jaspar_phylofacts_pwm_16_matrix_row_3_[11] = { -8.761725, -8.761725, -8.761725, -1.211590,  1.106187, -8.761725,  0.506978, -0.937280, -8.761725, -8.761725, -8.761725};
	float *jaspar_phylofacts_pwm_16_matrix[4] = { jaspar_phylofacts_pwm_16_matrix_row_0_, jaspar_phylofacts_pwm_16_matrix_row_1_, jaspar_phylofacts_pwm_16_matrix_row_2_, jaspar_phylofacts_pwm_16_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_16_ = {
/* accession        */ "PF0160",
/* name             */ "CAGNYGKNAAA",
/* label            */ " PF0160	16.9115857648467	CAGNYGKNAAA	Unknown	; MCS \"9.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CAGCTGGNAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_16_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.004402,
/* max_score        */ 13.141620,
/* threshold        */ 0.783,
/* info content     */ 16.894009,
/* base_counts      */ {1001, 480, 684, 343},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 228
};

	float jaspar_phylofacts_pwm_17_matrix_row_0_[9] = { -10.286881, -10.286881, -10.286881, -10.286881, -0.164218,  0.492096, -10.286881,  1.272937,  1.272937};
	float jaspar_phylofacts_pwm_17_matrix_row_1_[9] = { -10.045719, -10.045719,  1.514099,  1.050189,  0.064823, -10.045719, -10.045719, -10.045719, -10.045719};
	float jaspar_phylofacts_pwm_17_matrix_row_2_[9] = { -10.045719, -10.045719, -10.045719, -10.045719,  0.068879,  0.901590,  1.514099, -10.045719, -10.045719};
	float jaspar_phylofacts_pwm_17_matrix_row_3_[9] = {  1.272937,  1.272937, -10.286881,  0.281894,  0.041906, -10.286881, -10.286881, -10.286881, -10.286881};
	float *jaspar_phylofacts_pwm_17_matrix[4] = { jaspar_phylofacts_pwm_17_matrix_row_0_, jaspar_phylofacts_pwm_17_matrix_row_1_, jaspar_phylofacts_pwm_17_matrix_row_2_, jaspar_phylofacts_pwm_17_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_17_ = {
/* accession        */ "PF0040",
/* name             */ "TTCYNRGAA",
/* label            */ " PF0040	14.0600799181782	TTCYNRGAA	Unknown	; MCS \"24.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"STAT5A\" ; type \"phylogenetic\" ",
/* motif            */ "TTCCNGGAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_17_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.459267,
/* max_score        */ 10.140605,
/* threshold        */ 0.809,
/* info content     */ 14.056311,
/* base_counts      */ {2825, 1953, 1863, 2791},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1048
};

	float jaspar_phylofacts_pwm_18_matrix_row_0_[15] = {  0.139829, -8.796206, -8.796206,  1.272839, -8.796206,  0.165801,  0.165801,  0.165801,  0.330862, -0.362177,  0.463020, -0.319626, -8.796206,  1.272839, -8.796206};
	float jaspar_phylofacts_pwm_18_matrix_row_1_[15] = { -8.555044,  0.955475,  1.514001, -8.555044,  1.514001,  0.177422, -0.312024, -8.555044, -0.286056,  0.039296, -8.555044, -0.017852,  1.514001, -8.555044, -8.555044};
	float jaspar_phylofacts_pwm_18_matrix_row_2_[15] = {  1.125363, -8.555044, -8.555044, -8.555044, -8.555044,  0.110742,  0.481062,  1.112785,  0.550047,  0.367748,  0.925400,  0.723049, -8.555044, -8.555044,  1.514001};
	float jaspar_phylofacts_pwm_18_matrix_row_3_[15] = { -8.796206,  0.424184, -8.796206, -8.796206, -8.796206, -0.553186, -0.607239, -8.796206, -1.482319, -0.079998, -8.796206, -0.789505, -8.796206, -8.796206, -8.796206};
	float *jaspar_phylofacts_pwm_18_matrix[4] = { jaspar_phylofacts_pwm_18_matrix_row_0_, jaspar_phylofacts_pwm_18_matrix_row_1_, jaspar_phylofacts_pwm_18_matrix_row_2_, jaspar_phylofacts_pwm_18_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_18_ = {
/* accession        */ "PF0128",
/* name             */ "RYCACNNRNNRNCAG",
/* label            */ " PF0128	16.7895256979172	RYCACNNRNNRNCAG	Unknown	; MCS \"11.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GCCACNNGRNGNCAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 15,
/* matrixname       */ jaspar_phylofacts_pwm_18_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.756470,
/* max_score        */ 15.020032,
/* threshold        */ 0.768,
/* info content     */ 16.772013,
/* base_counts      */ {1073, 1087, 1099, 281},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 236
};

	float jaspar_phylofacts_pwm_19_matrix_row_0_[9] = { -10.997039, -10.997039, -10.997039, -10.997039, -10.997039,  0.036253, -10.997039, -10.997039, -10.997039};
	float jaspar_phylofacts_pwm_19_matrix_row_1_[9] = {  1.082756, -10.755877,  1.514114, -10.755877,  1.118220, -10.755877,  1.514114, -10.755877,  1.514114};
	float jaspar_phylofacts_pwm_19_matrix_row_2_[9] = { -10.755877,  1.514114, -10.755877,  1.514114, -10.755877,  1.171149, -10.755877,  1.514114, -10.755877};
	float jaspar_phylofacts_pwm_19_matrix_row_3_[9] = {  0.224210, -10.997039, -10.997039, -10.997039,  0.154931, -10.997039, -10.997039, -10.997039, -10.997039};
	float *jaspar_phylofacts_pwm_19_matrix[4] = { jaspar_phylofacts_pwm_19_matrix_row_0_, jaspar_phylofacts_pwm_19_matrix_row_1_, jaspar_phylofacts_pwm_19_matrix_row_2_, jaspar_phylofacts_pwm_19_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_19_ = {
/* accession        */ "PF0030",
/* name             */ "YGCGYRCGC",
/* label            */ " PF0030	15.2846884255224	YGCGYRCGC	Unknown	; MCS \"30.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CGCGCGCGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_19_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.973351,
/* max_score        */ 12.456806,
/* threshold        */ 0.801,
/* info content     */ 15.282553,
/* base_counts      */ {619, 9216, 7909, 1444},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2132
};

	float jaspar_phylofacts_pwm_20_matrix_row_0_[16] = {  0.933882, -8.813010,  1.272841,  1.272841,  1.272841, -8.813010,  0.280909, -0.518711, -0.741792, -0.840199, -0.336431,  0.199001, -8.813010, -8.813010, -8.813010,  0.397430};
	float jaspar_phylofacts_pwm_20_matrix_row_1_[16] = { -8.571848,  1.063825, -8.571848, -8.571848, -8.571848, -8.571848, -0.328829, -0.054455,  0.337522,  0.377258,  0.778341,  0.003803, -8.571848, -8.571848, -8.571848, -8.571848};
	float jaspar_phylofacts_pwm_20_matrix_row_2_[16] = {  0.267574, -8.571848, -8.571848, -8.571848, -8.571848,  0.989220,  0.238164,  0.223129,  0.176616, -0.328829, -0.634116, -0.440023, -8.571848, -8.571848,  1.514003, -8.571848};
	float jaspar_phylofacts_pwm_20_matrix_row_3_[16] = { -8.813010,  0.258183, -8.813010, -8.813010, -8.813010,  0.377230, -0.378981,  0.223096,  0.054981,  0.356613, -0.315816,  0.054981,  1.272841,  1.272841, -8.813010,  0.733874};
	float *jaspar_phylofacts_pwm_20_matrix[4] = { jaspar_phylofacts_pwm_20_matrix_row_0_, jaspar_phylofacts_pwm_20_matrix_row_1_, jaspar_phylofacts_pwm_20_matrix_row_2_, jaspar_phylofacts_pwm_20_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_20_ = {
/* accession        */ "PF0151",
/* name             */ "RYAAAKNNNNNNTTGW",
/* label            */ " PF0151	16.8532575371761	RYAAAKNNNNNNTTGW	Unknown	; MCS \"10.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACAAAGNNNNNNTTGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 16,
/* matrixname       */ jaspar_phylofacts_pwm_20_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.442764,
/* max_score        */ 13.795167,
/* threshold        */ 0.764,
/* info content     */ 16.835997,
/* base_counts      */ {1311, 560, 747, 1222},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 240
};

	float jaspar_phylofacts_pwm_21_matrix_row_0_[13] = { -9.709000, -9.709000, -9.709000, -0.992792,  0.733930, -9.709000,  0.421663,  0.527418, -0.213405, -0.772964, -9.709000, -9.709000, -9.709000};
	float jaspar_phylofacts_pwm_21_matrix_row_1_[13] = { -9.467837, -9.467837,  1.514077,  0.807248, -9.467837, -9.467837,  0.180822, -0.672861,  0.174350,  0.512657, -9.467837, -9.467837,  1.514077};
	float jaspar_phylofacts_pwm_21_matrix_row_2_[13] = { -9.467837, -9.467837, -9.467837, -2.915329,  0.638632,  1.514077, -0.267446, -0.531802,  0.193642, -0.108991, -9.467837, -9.467837, -9.467837};
	float jaspar_phylofacts_pwm_21_matrix_row_3_[13] = {  1.272915,  1.272915, -9.709000,  0.334294, -9.709000, -9.709000, -0.684868,  0.014224, -0.133947,  0.089183,  1.272915,  1.272915, -9.709000};
	float *jaspar_phylofacts_pwm_21_matrix[4] = { jaspar_phylofacts_pwm_21_matrix_row_0_, jaspar_phylofacts_pwm_21_matrix_row_1_, jaspar_phylofacts_pwm_21_matrix_row_2_, jaspar_phylofacts_pwm_21_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_21_ = {
/* accession        */ "PF0141",
/* name             */ "TTCNRGNNNNTTC",
/* label            */ " PF0141	16.0536657128651	TTCNRGNNNNTTC	Unknown	; MCS \"10.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"HSF\" ; type \"phylogenetic\" ",
/* motif            */ "TTCYAGNWNNTTC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_21_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.931427,
/* max_score        */ 12.830450,
/* threshold        */ 0.774,
/* info content     */ 16.046764,
/* base_counts      */ {1143, 2057, 1288, 3156},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 588
};

	float jaspar_phylofacts_pwm_22_matrix_row_0_[11] = { -8.547358, -8.547358, -8.547358,  0.794099, -8.547358, -8.547358, -8.547358, -0.135303,  1.272803, -8.547358, -8.547358};
	float jaspar_phylofacts_pwm_22_matrix_row_1_[11] = {  1.513965, -8.306195, -8.306195, -8.306195,  0.842376,  0.971897,  0.170384,  0.503816, -8.306195, -8.306195, -8.306195};
	float jaspar_phylofacts_pwm_22_matrix_row_2_[11] = { -8.306195, -8.306195,  1.513965,  0.547613, -8.306195, -8.306195, -8.306195, -0.145392, -8.306195, -8.306195, -8.306195};
	float jaspar_phylofacts_pwm_22_matrix_row_3_[11] = { -8.547358,  1.272803, -8.547358, -8.547358,  0.557733,  0.401748,  0.970541, -0.330999, -8.547358,  1.272803,  1.272803};
	float *jaspar_phylofacts_pwm_22_matrix[4] = { jaspar_phylofacts_pwm_22_matrix_row_0_, jaspar_phylofacts_pwm_22_matrix_row_1_, jaspar_phylofacts_pwm_22_matrix_row_2_, jaspar_phylofacts_pwm_22_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_22_ = {
/* accession        */ "PF0153",
/* name             */ "CTGRYYYNATT",
/* label            */ " PF0153	16.2848322108467	CTGRYYYNATT	Unknown	; MCS \"10.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTGACCTNATT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_22_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.804565,
/* max_score        */ 12.201868,
/* threshold        */ 0.784,
/* info content     */ 16.263113,
/* base_counts      */ {343, 500, 289, 892},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 184
};

	float jaspar_phylofacts_pwm_23_matrix_row_0_[12] = { -9.578389, -9.578389,  1.272908,  1.272908, -0.768377, -9.578389, -9.578389, -9.578389,  1.272908,  1.272908, -0.272647, -9.578389};
	float jaspar_phylofacts_pwm_23_matrix_row_1_[12] = { -9.337227, -9.337227, -9.337227, -9.337227,  0.488353,  1.514070, -9.337227, -9.337227, -9.337227, -9.337227, -0.881696,  0.697333};
	float jaspar_phylofacts_pwm_23_matrix_row_2_[12] = {  1.514070,  1.514070, -9.337227, -9.337227,  0.843930, -9.337227,  1.514070,  1.514070, -9.337227, -9.337227,  1.125905, -9.337227};
	float jaspar_phylofacts_pwm_23_matrix_row_3_[12] = { -9.578389, -9.578389, -9.578389, -9.578389, -9.578389, -9.578389, -9.578389, -9.578389, -9.578389, -9.578389, -2.774884,  0.689777};
	float *jaspar_phylofacts_pwm_23_matrix[4] = { jaspar_phylofacts_pwm_23_matrix_row_0_, jaspar_phylofacts_pwm_23_matrix_row_1_, jaspar_phylofacts_pwm_23_matrix_row_2_, jaspar_phylofacts_pwm_23_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_23_ = {
/* accession        */ "PF0021",
/* name             */ "GGAANCGGAANY",
/* label            */ " PF0021	20.3301929929989	GGAANCGGAANY	Unknown	; MCS \"37.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGAAGCGGAAGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_23_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -108.137177,
/* max_score        */ 15.329146,
/* threshold        */ 0.776,
/* info content     */ 20.320160,
/* base_counts      */ {2241, 976, 2678, 297},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 516
};

	float jaspar_phylofacts_pwm_24_matrix_row_0_[9] = {  1.272927,  1.272927,  1.272927, -9.986408,  1.106461,  1.272927,  1.272927, -9.986408,  0.978353};
	float jaspar_phylofacts_pwm_24_matrix_row_1_[9] = { -9.745247, -9.745247, -9.745247,  0.888226, -9.745247, -9.745247, -9.745247,  1.514089,  0.148241};
	float jaspar_phylofacts_pwm_24_matrix_row_2_[9] = { -9.745247, -9.745247, -9.745247, -9.745247, -9.745247, -9.745247, -9.745247, -9.745247, -9.745247};
	float jaspar_phylofacts_pwm_24_matrix_row_3_[9] = { -9.986408, -9.986408, -9.986408,  0.507667, -0.602031, -9.986408, -9.986408, -9.986408, -9.986408};
	float *jaspar_phylofacts_pwm_24_matrix[4] = { jaspar_phylofacts_pwm_24_matrix_row_0_, jaspar_phylofacts_pwm_24_matrix_row_1_, jaspar_phylofacts_pwm_24_matrix_row_2_, jaspar_phylofacts_pwm_24_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_24_ = {
/* accession        */ "PF0139",
/* name             */ "AAAYWAACM",
/* label            */ " PF0139	15.5659877857067	AAAYWAACM	Unknown	; MCS \"11.0\" ; jaspar \"FOXI1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"HFH-4\" ; type \"phylogenetic\" ",
/* motif            */ "AAACAAACA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_24_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.636505,
/* max_score        */ 10.851764,
/* threshold        */ 0.799,
/* info content     */ 15.560549,
/* base_counts      */ {5115, 1389, 0, 480},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 776
};

	float jaspar_phylofacts_pwm_25_matrix_row_0_[8] = { -10.182373, -10.182373, -10.182373, -10.182373, -10.182373, -10.182373, -10.182373, -10.182373};
	float jaspar_phylofacts_pwm_25_matrix_row_1_[8] = { -9.941211, -9.941211, -9.941211, -9.941211, -9.941211, -9.941211, -9.941211, -9.941211};
	float jaspar_phylofacts_pwm_25_matrix_row_2_[8] = {  1.514096, -9.941211,  1.514096,  1.514096,  1.514096, -9.941211,  1.514096,  1.092081};
	float jaspar_phylofacts_pwm_25_matrix_row_3_[8] = { -10.182373,  1.272934, -10.182373, -10.182373, -10.182373,  1.272934, -10.182373,  0.206653};
	float *jaspar_phylofacts_pwm_25_matrix[4] = { jaspar_phylofacts_pwm_25_matrix_row_0_, jaspar_phylofacts_pwm_25_matrix_row_1_, jaspar_phylofacts_pwm_25_matrix_row_2_, jaspar_phylofacts_pwm_25_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_25_ = {
/* accession        */ "PF0091",
/* name             */ "GTGGGTGK",
/* label            */ " PF0091	15.0711447112168	GTGGGTGK	Unknown	; MCS \"14.1\" ; jaspar \"RREB1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTGGGTGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_25_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.458984,
/* max_score        */ 11.208429,
/* threshold        */ 0.809,
/* info content     */ 15.066783,
/* base_counts      */ {0, 0, 5339, 2213},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 944
};

	float jaspar_phylofacts_pwm_26_matrix_row_0_[6] = { -10.929119, -10.929119,  1.272951, -10.929119, -10.929119,  1.272951};
	float jaspar_phylofacts_pwm_26_matrix_row_1_[6] = { -10.687957, -10.687957, -10.687957, -10.687957, -10.687957, -10.687957};
	float jaspar_phylofacts_pwm_26_matrix_row_2_[6] = {  1.514113,  1.514113, -10.687957, -10.687957, -10.687957, -10.687957};
	float jaspar_phylofacts_pwm_26_matrix_row_3_[6] = { -10.929119, -10.929119, -10.929119,  1.272951,  1.272951, -10.929119};
	float *jaspar_phylofacts_pwm_26_matrix[4] = { jaspar_phylofacts_pwm_26_matrix_row_0_, jaspar_phylofacts_pwm_26_matrix_row_1_, jaspar_phylofacts_pwm_26_matrix_row_2_, jaspar_phylofacts_pwm_26_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_26_ = {
/* accession        */ "PF0093",
/* name             */ "GGATTA",
/* label            */ " PF0093	12	GGATTA	Unknown	; MCS \"14.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"PITX2\" ; type \"phylogenetic\" ",
/* motif            */ "GGATTA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_26_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -65.574715,
/* max_score        */ 8.120028,
/* threshold        */ 0.861,
/* info content     */ 11.998279,
/* base_counts      */ {3984, 0, 3984, 3984},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1992
};

	float jaspar_phylofacts_pwm_27_matrix_row_0_[9] = { -9.601377, -9.601377, -9.601377, -1.860277, -9.601377,  0.951855,  1.272909, -9.601377,  1.272909};
	float jaspar_phylofacts_pwm_27_matrix_row_1_[9] = {  1.514071,  1.514071, -9.360214, -0.677337, -9.360214,  0.221758, -9.360214, -9.360214, -9.360214};
	float jaspar_phylofacts_pwm_27_matrix_row_2_[9] = { -9.360214, -9.360214, -9.360214, -0.169975, -9.360214, -9.360214, -9.360214,  1.514071, -9.360214};
	float jaspar_phylofacts_pwm_27_matrix_row_3_[9] = { -9.601377, -9.601377,  1.272909,  0.856025,  1.272909, -9.601377, -9.601377, -9.601377, -9.601377};
	float *jaspar_phylofacts_pwm_27_matrix[4] = { jaspar_phylofacts_pwm_27_matrix_row_0_, jaspar_phylofacts_pwm_27_matrix_row_1_, jaspar_phylofacts_pwm_27_matrix_row_2_, jaspar_phylofacts_pwm_27_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_27_ = {
/* accession        */ "PF0167",
/* name             */ "CCTNTMAGA",
/* label            */ " PF0167	15.7543754111121	CCTNTMAGA	Unknown	; MCS \"9.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCTTTAAGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_27_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.671295,
/* max_score        */ 11.441729,
/* threshold        */ 0.798,
/* info content     */ 15.746840,
/* base_counts      */ {1462, 1260, 626, 1404},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 528
};

	float jaspar_phylofacts_pwm_28_matrix_row_0_[13] = { -0.896525, -9.472176, -9.472176, -9.472176, -9.472176, -1.153189,  0.274717,  0.421312, -9.472176, -0.412542,  1.272901,  1.272901, -9.472176};
	float jaspar_phylofacts_pwm_28_matrix_row_1_[13] = { -9.231013, -0.335247, -9.231013, -9.231013,  1.514063,  0.702082, -0.636674, -9.231013, -9.231013,  0.957691, -9.231013, -9.231013,  1.514063};
	float jaspar_phylofacts_pwm_28_matrix_row_2_[13] = {  1.392775, -9.231013, -9.231013,  1.514063, -9.231013, -2.427508, -0.040774,  0.957691,  1.514063, -0.936714, -9.231013, -9.231013, -9.231013};
	float jaspar_phylofacts_pwm_28_matrix_row_3_[13] = { -9.472176,  1.101728,  1.272901, -9.472176, -9.472176,  0.470581,  0.081826, -9.472176, -9.472176, -0.590200, -9.472176, -9.472176, -9.472176};
	float *jaspar_phylofacts_pwm_28_matrix[4] = { jaspar_phylofacts_pwm_28_matrix_row_0_, jaspar_phylofacts_pwm_28_matrix_row_1_, jaspar_phylofacts_pwm_28_matrix_row_2_, jaspar_phylofacts_pwm_28_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_28_ = {
/* accession        */ "PF0053",
/* name             */ "RYTGCNNRGNAAC",
/* label            */ " PF0053	18.8958345972212	RYTGCNNRGNAAC	Unknown	; MCS \"21.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"MIF-1\" ; type \"phylogenetic\" ",
/* motif            */ "GTTGCYNGGCAAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_28_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.722656,
/* max_score        */ 15.261637,
/* threshold        */ 0.778,
/* info content     */ 18.885775,
/* base_counts      */ {1477, 1527, 1752, 1276},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 464
};

	float jaspar_phylofacts_pwm_29_matrix_row_0_[7] = { -12.132475,  1.272961, -12.132475, -12.132475, -12.132475, -12.132475, -12.132475};
	float jaspar_phylofacts_pwm_29_matrix_row_1_[7] = { -11.891314, -11.891314, -11.891314, -11.891314, -11.891314, -11.891314,  1.159771};
	float jaspar_phylofacts_pwm_29_matrix_row_2_[7] = {  1.514123, -11.891314, -11.891314, -11.891314,  1.514123,  1.514123, -11.891314};
	float jaspar_phylofacts_pwm_29_matrix_row_3_[7] = { -12.132475, -12.132475,  1.272961,  1.272961, -12.132475, -12.132475,  0.063552};
	float *jaspar_phylofacts_pwm_29_matrix[4] = { jaspar_phylofacts_pwm_29_matrix_row_0_, jaspar_phylofacts_pwm_29_matrix_row_1_, jaspar_phylofacts_pwm_29_matrix_row_2_, jaspar_phylofacts_pwm_29_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_29_ = {
/* accession        */ "PF0005",
/* name             */ "GATTGGY",
/* label            */ " PF0005	13.1207076355866	GATTGGY	Unknown	; MCS \"64.6\" ; jaspar \"NF-Y\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"NF-Y\" ; type \"phylogenetic\" ",
/* motif            */ "GATTGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_29_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.927330,
/* max_score        */ 9.521024,
/* threshold        */ 0.837,
/* info content     */ 13.120083,
/* base_counts      */ {6636, 4656, 19908, 15252},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 6636
};

	float jaspar_phylofacts_pwm_30_matrix_row_0_[13] = { -9.125340, -9.125340,  1.272874,  0.778197, -0.831041,  0.737378, -0.065706, -0.125597, -0.713285, -0.031421, -9.125340, -9.125340, -9.125340};
	float jaspar_phylofacts_pwm_30_matrix_row_1_[13] = {  1.514036,  1.514036, -8.884178, -8.884178,  0.209741, -8.884178, -8.884178, -0.135714, -0.184497,  0.662706, -8.884178, -8.884178,  1.514036};
	float jaspar_phylofacts_pwm_30_matrix_row_2_[13] = { -8.884178, -8.884178, -8.884178, -8.884178,  0.430612, -8.884178, -8.884178,  0.789959,  0.897198, -0.346986,  1.514036,  1.514036, -8.884178};
	float jaspar_phylofacts_pwm_30_matrix_row_3_[13] = { -9.125340, -9.125340, -9.125340,  0.331938, -0.042719,  0.392558,  0.968809, -1.300894, -0.691311, -0.648761, -9.125340, -9.125340, -9.125340};
	float *jaspar_phylofacts_pwm_30_matrix[4] = { jaspar_phylofacts_pwm_30_matrix_row_0_, jaspar_phylofacts_pwm_30_matrix_row_1_, jaspar_phylofacts_pwm_30_matrix_row_2_, jaspar_phylofacts_pwm_30_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_30_ = {
/* accession        */ "PF0165",
/* name             */ "CCAWNWWNNNGGC",
/* label            */ " PF0165	15.9848802580746	CCAWNWWNNNGGC	Unknown	; MCS \"9.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCAANATNGNGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_30_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.898567,
/* max_score        */ 14.107916,
/* threshold        */ 0.774,
/* info content     */ 15.972802,
/* base_counts      */ {1061, 1336, 1154, 713},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 328
};

	float jaspar_phylofacts_pwm_31_matrix_row_0_[10] = { -9.755516, -9.755516, -9.755516,  1.272917, -9.755516, -9.755516, -9.755516,  1.272917, -0.306080,  0.387027};
	float jaspar_phylofacts_pwm_31_matrix_row_1_[10] = {  0.817696,  0.685565,  1.514079, -9.514355, -9.514355, -9.514355,  1.514079, -9.514355, -9.514355, -9.514355};
	float jaspar_phylofacts_pwm_31_matrix_row_2_[10] = { -9.514355, -9.514355, -9.514355, -9.514355, -9.514355, -9.514355, -9.514355, -9.514355, -9.514355, -9.514355};
	float jaspar_phylofacts_pwm_31_matrix_row_3_[10] = {  0.583027,  0.699007, -9.755516, -9.755516,  1.272917,  1.272917, -9.755516, -9.755516,  1.042037,  0.741326};
	float *jaspar_phylofacts_pwm_31_matrix[4] = { jaspar_phylofacts_pwm_31_matrix_row_0_, jaspar_phylofacts_pwm_31_matrix_row_1_, jaspar_phylofacts_pwm_31_matrix_row_2_, jaspar_phylofacts_pwm_31_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_31_ = {
/* accession        */ "PF0052",
/* name             */ "YYCATTCAWW",
/* label            */ " PF0052	16.299797260239	YYCATTCAWW	Unknown	; MCS \"21.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"POU1F1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "TTCATTCATT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_31_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.072845,
/* max_score        */ 11.419891,
/* threshold        */ 0.789,
/* info content     */ 16.292574,
/* base_counts      */ {1613, 1808, 0, 2739},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 616
};

	float jaspar_phylofacts_pwm_32_matrix_row_0_[9] = {  1.272937,  1.272937,  1.272937, -10.275366,  0.759540, -0.381878, -10.275366, -10.275366, -10.275366};
	float jaspar_phylofacts_pwm_32_matrix_row_1_[9] = { -10.034204, -10.034204, -10.034204,  0.775545, -10.034204, -0.208623,  1.514099, -10.034204, -10.034204};
	float jaspar_phylofacts_pwm_32_matrix_row_2_[9] = { -10.034204, -10.034204, -10.034204, -10.034204,  0.601676,  0.567444, -10.034204, -10.034204,  1.514099};
	float jaspar_phylofacts_pwm_32_matrix_row_3_[9] = { -10.275366, -10.275366, -10.275366,  0.623242, -10.275366, -0.144703, -10.275366,  1.272937, -10.275366};
	float *jaspar_phylofacts_pwm_32_matrix[4] = { jaspar_phylofacts_pwm_32_matrix_row_0_, jaspar_phylofacts_pwm_32_matrix_row_1_, jaspar_phylofacts_pwm_32_matrix_row_2_, jaspar_phylofacts_pwm_32_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_32_ = {
/* accession        */ "PF0077",
/* name             */ "AAAYRNCTG",
/* label            */ " PF0077	14.1039821338926	AAAYRNCTG	Unknown	; MCS \"16.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AAATANCTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_32_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.584808,
/* max_score        */ 10.222473,
/* threshold        */ 0.808,
/* info content     */ 14.100168,
/* base_counts      */ {3926, 1716, 1854, 1828},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1036
};

	float jaspar_phylofacts_pwm_33_matrix_row_0_[7] = { -11.517566, -11.517566, -11.517566, -11.517566,  1.272957,  1.272957,  0.685006};
	float jaspar_phylofacts_pwm_33_matrix_row_1_[7] = {  1.514119, -11.276403, -11.276403, -11.276403, -11.276403, -11.276403, -11.276403};
	float jaspar_phylofacts_pwm_33_matrix_row_2_[7] = { -11.276403, -11.276403, -11.276403, -11.276403, -11.276403, -11.276403,  0.703402};
	float jaspar_phylofacts_pwm_33_matrix_row_3_[7] = { -11.517566,  1.272957,  1.272957,  1.272957, -11.517566, -11.517566, -11.517566};
	float *jaspar_phylofacts_pwm_33_matrix[4] = { jaspar_phylofacts_pwm_33_matrix_row_0_, jaspar_phylofacts_pwm_33_matrix_row_1_, jaspar_phylofacts_pwm_33_matrix_row_2_, jaspar_phylofacts_pwm_33_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_33_ = {
/* accession        */ "PF0029",
/* name             */ "CTTTAAR",
/* label            */ " PF0029	13.0088940575263	CTTTAAR	Unknown	; MCS \"30.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTTTAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_33_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.622955,
/* max_score        */ 8.582308,
/* threshold        */ 0.838,
/* info content     */ 13.007792,
/* base_counts      */ {9169, 3588, 1595, 10764},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3588
};

	float jaspar_phylofacts_pwm_34_matrix_row_0_[7] = { -11.526444, -11.526444,  1.272957, -0.591536, -11.526444, -11.526444,  1.272957};
	float jaspar_phylofacts_pwm_34_matrix_row_1_[7] = { -11.285283, -11.285283, -11.285283,  0.372617, -11.285283,  1.514120, -11.285283};
	float jaspar_phylofacts_pwm_34_matrix_row_2_[7] = { -11.285283,  1.514120, -11.285283,  0.523300, -11.285283, -11.285283, -11.285283};
	float jaspar_phylofacts_pwm_34_matrix_row_3_[7] = {  1.272957, -11.526444, -11.526444, -0.595107,  1.272957, -11.526444, -11.526444};
	float *jaspar_phylofacts_pwm_34_matrix[4] = { jaspar_phylofacts_pwm_34_matrix_row_0_, jaspar_phylofacts_pwm_34_matrix_row_1_, jaspar_phylofacts_pwm_34_matrix_row_2_, jaspar_phylofacts_pwm_34_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_34_ = {
/* accession        */ "PF0007",
/* name             */ "TGANTCA",
/* label            */ " PF0007	12.110349846871	TGANTCA	Unknown	; MCS \"62.8\" ; jaspar \"Fos\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"AP-1\" ; type \"phylogenetic\" ",
/* motif            */ "TGANTCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_34_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -69.753777,
/* max_score        */ 8.643369,
/* threshold        */ 0.847,
/* info content     */ 12.109358,
/* base_counts      */ {7801, 4776, 4964, 7799},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3620
};

	float jaspar_phylofacts_pwm_35_matrix_row_0_[14] = { -10.734680, -10.734680, -10.734680, -0.049554, -0.268696, -0.171060, -0.432383, -0.186685, -0.040443, -10.734680, -10.734680, -10.734680,  1.272947,  0.021114};
	float jaspar_phylofacts_pwm_35_matrix_row_1_[14] = { -10.493519, -10.493519, -10.493519,  0.632288,  0.253711,  0.223141,  0.376972,  0.149547, -0.089225, -10.493519,  1.514109,  1.514109, -10.493519, -10.493519};
	float jaspar_phylofacts_pwm_35_matrix_row_2_[14] = { -10.493519,  1.514109,  1.514109, -0.021852,  0.072683,  0.231971,  0.416119,  0.301988,  0.322272,  0.989989, -10.493519, -10.493519, -10.493519,  1.177274};
	float jaspar_phylofacts_pwm_35_matrix_row_3_[14] = {  1.272947, -10.734680, -10.734680, -0.987788, -0.051845, -0.280157, -0.534762, -0.265851, -0.213281,  0.376289, -10.734680, -10.734680, -10.734680, -10.734680};
	float *jaspar_phylofacts_pwm_35_matrix[4] = { jaspar_phylofacts_pwm_35_matrix_row_0_, jaspar_phylofacts_pwm_35_matrix_row_1_, jaspar_phylofacts_pwm_35_matrix_row_2_, jaspar_phylofacts_pwm_35_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_35_ = {
/* accession        */ "PF0027",
/* name             */ "TGGNNNNNNKCCAR",
/* label            */ " PF0027	14.42554536176	TGGNNNNNNKCCAR	Unknown	; MCS \"32.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGGNNNNNNGCCAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_35_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.427979,
/* max_score        */ 12.927943,
/* threshold        */ 0.764,
/* info content     */ 14.423033,
/* base_counts      */ {4404, 6150, 8151, 4255},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1640
};

	float jaspar_phylofacts_pwm_36_matrix_row_0_[7] = { -11.135247, -11.135247, -11.135247, -11.135247, -11.135247, -11.135247, -11.135247};
	float jaspar_phylofacts_pwm_36_matrix_row_1_[7] = { -10.894086, -10.894086, -10.894086, -10.894086, -10.894086, -10.894086,  0.751630};
	float jaspar_phylofacts_pwm_36_matrix_row_2_[7] = { -10.894086,  1.514115, -10.894086, -10.894086, -10.894086,  1.514115, -10.894086};
	float jaspar_phylofacts_pwm_36_matrix_row_3_[7] = {  1.272953, -11.135247,  1.272953,  1.272953,  1.272953, -11.135247,  0.644655};
	float *jaspar_phylofacts_pwm_36_matrix[4] = { jaspar_phylofacts_pwm_36_matrix_row_0_, jaspar_phylofacts_pwm_36_matrix_row_1_, jaspar_phylofacts_pwm_36_matrix_row_2_, jaspar_phylofacts_pwm_36_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_36_ = {
/* accession        */ "PF0083",
/* name             */ "TGTTTGY",
/* label            */ " PF0083	13.0032399233547	TGTTTGY	Unknown	; MCS \"15.1\" ; jaspar \"Foxd3\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"HNF-3\" ; type \"phylogenetic\" ",
/* motif            */ "TGTTTGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_36_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.946732,
/* max_score        */ 8.871675,
/* threshold        */ 0.838,
/* info content     */ 13.001667,
/* base_counts      */ {0, 1142, 4896, 11098},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2448
};

	float jaspar_phylofacts_pwm_37_matrix_row_0_[11] = {  0.949051, -10.134644,  1.272932,  1.272932,  0.213562, -0.566559, -1.040725, -10.134644, -10.134644,  1.272932, -10.134644};
	float jaspar_phylofacts_pwm_37_matrix_row_1_[11] = { -9.893481,  1.514094, -9.893481, -9.893481,  0.342936, -0.703242,  1.028871,  1.249843,  1.514094, -9.893481, -9.893481};
	float jaspar_phylofacts_pwm_37_matrix_row_2_[11] = { -9.893481, -9.893481, -9.893481, -9.893481, -0.232002, -0.100870, -0.634256, -9.893481, -9.893481, -9.893481,  1.514094};
	float jaspar_phylofacts_pwm_37_matrix_row_3_[11] = { -0.011980, -10.134644, -10.134644, -10.134644, -0.505527,  0.644333, -0.505527, -0.187091, -10.134644, -10.134644, -10.134644};
	float *jaspar_phylofacts_pwm_37_matrix[4] = { jaspar_phylofacts_pwm_37_matrix_row_0_, jaspar_phylofacts_pwm_37_matrix_row_1_, jaspar_phylofacts_pwm_37_matrix_row_2_, jaspar_phylofacts_pwm_37_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_37_ = {
/* accession        */ "PF0152",
/* name             */ "WCAANNNYCAG",
/* label            */ " PF0152	15.1677708035863	WCAANNNYCAG	Unknown	; MCS \"10.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACAANTCCCAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_37_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.085480,
/* max_score        */ 12.576115,
/* threshold        */ 0.787,
/* info content     */ 15.163343,
/* base_counts      */ {3895, 3422, 1341, 1242},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 900
};

	float jaspar_phylofacts_pwm_38_matrix_row_0_[8] = {  0.220541,  0.514637, -9.428120, -9.428120, -9.428120, -9.428120, -9.428120,  1.272898};
	float jaspar_phylofacts_pwm_38_matrix_row_1_[8] = { -9.186957, -9.186957,  1.514060,  1.514060, -9.186957, -9.186957, -9.186957, -9.186957};
	float jaspar_phylofacts_pwm_38_matrix_row_2_[8] = {  1.084674,  0.882087, -9.186957, -9.186957,  1.514060, -9.186957, -9.186957, -9.186957};
	float jaspar_phylofacts_pwm_38_matrix_row_3_[8] = { -9.428120, -9.428120, -9.428120, -9.428120, -9.428120,  1.272898,  1.272898, -9.428120};
	float *jaspar_phylofacts_pwm_38_matrix[4] = { jaspar_phylofacts_pwm_38_matrix_row_0_, jaspar_phylofacts_pwm_38_matrix_row_1_, jaspar_phylofacts_pwm_38_matrix_row_2_, jaspar_phylofacts_pwm_38_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_38_ = {
/* accession        */ "PF0146",
/* name             */ "RRCCGTTA",
/* label            */ " PF0146	14.0696097674866	RRCCGTTA	Unknown	; MCS \"10.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCCGTTA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_38_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.424957,
/* max_score        */ 10.327637,
/* threshold        */ 0.818,
/* info content     */ 14.061327,
/* base_counts      */ {807, 888, 969, 888},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 444
};

	float jaspar_phylofacts_pwm_39_matrix_row_0_[6] = {  1.272962,  1.272962, -12.411073, -12.411073, -12.411073, -12.411073};
	float jaspar_phylofacts_pwm_39_matrix_row_1_[6] = { -12.169911, -12.169911,  1.514124, -12.169911, -12.169911, -12.169911};
	float jaspar_phylofacts_pwm_39_matrix_row_2_[6] = { -12.169911, -12.169911, -12.169911, -12.169911, -12.169911, -12.169911};
	float jaspar_phylofacts_pwm_39_matrix_row_3_[6] = { -12.411073, -12.411073, -12.411073,  1.272962,  1.272962,  1.272962};
	float *jaspar_phylofacts_pwm_39_matrix[4] = { jaspar_phylofacts_pwm_39_matrix_row_0_, jaspar_phylofacts_pwm_39_matrix_row_1_, jaspar_phylofacts_pwm_39_matrix_row_2_, jaspar_phylofacts_pwm_39_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_39_ = {
/* accession        */ "PF0017",
/* name             */ "AACTTT",
/* label            */ " PF0017	12	AACTTT	Unknown	; MCS \"42.1\" ; jaspar \"NR2F1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"IRF1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "AACTTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_39_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -74.466438,
/* max_score        */ 7.878935,
/* threshold        */ 0.861,
/* info content     */ 11.999565,
/* base_counts      */ {17536, 8768, 0, 26304},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 8768
};

	float jaspar_phylofacts_pwm_40_matrix_row_0_[7] = { -11.156266, -11.156266,  1.272954, -11.156266,  1.272954, -11.156266, -11.156266};
	float jaspar_phylofacts_pwm_40_matrix_row_1_[7] = { -10.915105, -10.915105, -10.915105,  1.514116, -10.915105, -10.915105,  0.945664};
	float jaspar_phylofacts_pwm_40_matrix_row_2_[7] = { -10.915105,  1.514116, -10.915105, -10.915105, -10.915105, -10.915105, -10.915105};
	float jaspar_phylofacts_pwm_40_matrix_row_3_[7] = {  1.272954, -11.156266, -11.156266, -11.156266, -11.156266,  1.272954,  0.437326};
	float *jaspar_phylofacts_pwm_40_matrix[4] = { jaspar_phylofacts_pwm_40_matrix_row_0_, jaspar_phylofacts_pwm_40_matrix_row_1_, jaspar_phylofacts_pwm_40_matrix_row_2_, jaspar_phylofacts_pwm_40_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_40_ = {
/* accession        */ "PF0042",
/* name             */ "TGACATY",
/* label            */ " PF0042	13.0127592283486	TGACATY	Unknown	; MCS \"23.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGACATC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_40_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.093864,
/* max_score        */ 9.065710,
/* threshold        */ 0.838,
/* info content     */ 13.011217,
/* base_counts      */ {5000, 3916, 2500, 6084},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2500
};

	float jaspar_phylofacts_pwm_41_matrix_row_0_[8] = {  1.272903, -9.497706, -9.497706, -9.497706, -9.497706, -9.497706, -9.497706,  1.272903};
	float jaspar_phylofacts_pwm_41_matrix_row_1_[8] = { -9.256544, -9.256544, -9.256544, -9.256544,  1.342612, -9.256544, -9.256544, -9.256544};
	float jaspar_phylofacts_pwm_41_matrix_row_2_[8] = { -9.256544, -9.256544,  1.514065,  1.514065, -9.256544,  1.514065,  1.514065, -9.256544};
	float jaspar_phylofacts_pwm_41_matrix_row_3_[8] = { -9.497706,  1.272903, -9.497706, -9.497706, -0.574915, -9.497706, -9.497706, -9.497706};
	float *jaspar_phylofacts_pwm_41_matrix[4] = { jaspar_phylofacts_pwm_41_matrix_row_0_, jaspar_phylofacts_pwm_41_matrix_row_1_, jaspar_phylofacts_pwm_41_matrix_row_2_, jaspar_phylofacts_pwm_41_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_41_ = {
/* accession        */ "PF0102",
/* name             */ "ATGGYGGA",
/* label            */ " PF0102	15.371552469456	ATGGYGGA	Unknown	; MCS \"13.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ATGGCGGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_41_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.981651,
/* max_score        */ 11.217579,
/* threshold        */ 0.807,
/* info content     */ 15.363340,
/* base_counts      */ {952, 401, 1904, 551},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 476
};

	float jaspar_phylofacts_pwm_42_matrix_row_0_[11] = {  1.272890, -9.313720, -9.313720,  1.056673, -9.313720,  0.968239,  1.272890, -9.313720, -9.313720, -2.068778, -9.313720};
	float jaspar_phylofacts_pwm_42_matrix_row_1_[11] = { -9.072557,  1.514052, -9.072557, -9.072557,  1.456895, -0.617027, -9.072557,  1.514052, -9.072557,  1.313387,  1.464884};
	float jaspar_phylofacts_pwm_42_matrix_row_2_[11] = { -9.072557, -9.072557, -9.072557, -9.072557, -1.375891, -0.682971, -9.072557, -9.072557, -9.072557, -2.673963, -9.072557};
	float jaspar_phylofacts_pwm_42_matrix_row_3_[11] = { -9.313720, -9.313720,  1.272890, -0.364614, -9.313720, -2.142831, -9.313720, -9.313720,  1.272890, -0.757114, -1.763584};
	float *jaspar_phylofacts_pwm_42_matrix[4] = { jaspar_phylofacts_pwm_42_matrix_row_0_, jaspar_phylofacts_pwm_42_matrix_row_1_, jaspar_phylofacts_pwm_42_matrix_row_2_, jaspar_phylofacts_pwm_42_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_42_ = {
/* accession        */ "PF0105",
/* name             */ "ACTWSNACTNY",
/* label            */ " PF0105	18.6154406563166	ACTWSNACTNY	Unknown	; MCS \"13.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACTACAACTCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_42_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.399117,
/* max_score        */ 14.379742,
/* threshold        */ 0.778,
/* info content     */ 18.604717,
/* base_counts      */ {1417, 1914, 72, 953},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 396
};

	float jaspar_phylofacts_pwm_43_matrix_row_0_[17] = { -9.088074, -9.088074, -9.088074, -9.088074, -9.088074, -0.698486, -1.843132, -0.178703, -0.819085, -0.986093,  0.038994,  0.652954, -9.088074, -9.088074,  1.272871, -9.088074, -9.088074};
	float jaspar_phylofacts_pwm_43_matrix_row_1_[17] = { -8.846911, -8.846911,  1.514033, -8.846911,  1.514033, -0.051935,  0.467879,  0.189195,  0.467879,  0.258180, -1.601970, -0.775693, -8.846911, -8.846911, -8.846911,  1.441873, -8.846911};
	float jaspar_phylofacts_pwm_43_matrix_row_2_[17] = {  1.514033,  1.061614, -8.846911,  1.514033, -8.846911,  0.888217,  0.618149,  0.618149,  0.476847,  0.115096,  0.795276,  0.373478, -8.846911,  1.514033, -8.846911, -8.846911,  1.514033};
	float jaspar_phylofacts_pwm_43_matrix_row_3_[17] = { -9.088074,  0.262116, -9.088074, -9.088074, -9.088074, -0.871715, -0.355608, -1.115263, -0.493734,  0.262116, -0.457373, -1.917185,  1.272871, -9.088074, -9.088074, -1.391406, -9.088074};
	float *jaspar_phylofacts_pwm_43_matrix[4] = { jaspar_phylofacts_pwm_43_matrix_row_0_, jaspar_phylofacts_pwm_43_matrix_row_1_, jaspar_phylofacts_pwm_43_matrix_row_2_, jaspar_phylofacts_pwm_43_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_43_ = {
/* accession        */ "PF0019",
/* name             */ "GKCGCNNNNNNNTGAYG",
/* label            */ " PF0019	20.4656414702423	GKCGCNNNNNNNTGAYG	Unknown	; MCS \"40.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCGCGSNNNRATGACG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 17,
/* matrixname       */ jaspar_phylofacts_pwm_43_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.035179,
/* max_score        */ 18.445133,
/* threshold        */ 0.792,
/* info content     */ 20.450586,
/* base_counts      */ {782, 1434, 2337, 819},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 316
};

	float jaspar_phylofacts_pwm_44_matrix_row_0_[9] = { -9.601377, -9.601377, -9.601377,  1.272909, -0.868911, -9.601377, -9.601377, -9.601377,  0.627847};
	float jaspar_phylofacts_pwm_44_matrix_row_1_[9] = {  0.904473, -9.360214,  1.514071, -9.360214,  0.150305, -9.360214, -9.360214,  1.514071, -9.360214};
	float jaspar_phylofacts_pwm_44_matrix_row_2_[9] = { -9.360214,  1.514071, -9.360214, -9.360214,  0.404068, -9.360214,  1.514071, -9.360214,  0.770449};
	float jaspar_phylofacts_pwm_44_matrix_row_3_[9] = {  0.488632, -9.601377, -9.601377, -9.601377,  0.060103,  1.272909, -9.601377, -9.601377, -9.601377};
	float *jaspar_phylofacts_pwm_44_matrix[4] = { jaspar_phylofacts_pwm_44_matrix_row_0_, jaspar_phylofacts_pwm_44_matrix_row_1_, jaspar_phylofacts_pwm_44_matrix_row_2_, jaspar_phylofacts_pwm_44_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_44_ = {
/* accession        */ "PF0096",
/* name             */ "YGCANTGCR",
/* label            */ " PF0096	14.0932504690537	YGCANTGCR	Unknown	; MCS \"13.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CGCANTGCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_44_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.679924,
/* max_score        */ 10.681091,
/* threshold        */ 0.808,
/* info content     */ 14.086174,
/* base_counts      */ {867, 1478, 1481, 926},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 528
};

	float jaspar_phylofacts_pwm_45_matrix_row_0_[12] = { -0.527483, -9.463518, -9.463518, -9.463518, -9.463518, -1.073931,  1.138129, -9.463518, -9.463518, -9.463518, -0.263127,  0.592734};
	float jaspar_phylofacts_pwm_45_matrix_row_1_[12] = { -9.222356,  0.386828, -9.222356, -9.222356,  1.514063,  1.086630, -9.222356, -9.222356, -9.222356, -9.222356,  0.838178, -9.222356};
	float jaspar_phylofacts_pwm_45_matrix_row_2_[12] = {  1.333483, -9.222356, -9.222356,  1.514063, -9.222356, -1.783384, -9.222356, -9.222356,  1.514063,  1.514063, -0.506148,  0.807808};
	float jaspar_phylofacts_pwm_45_matrix_row_3_[12] = { -9.463518,  0.881477,  1.272900, -9.463518, -9.463518, -0.263127, -0.797732,  1.272900, -9.463518, -9.463518, -0.668541, -9.463518};
	float *jaspar_phylofacts_pwm_45_matrix[4] = { jaspar_phylofacts_pwm_45_matrix_row_0_, jaspar_phylofacts_pwm_45_matrix_row_1_, jaspar_phylofacts_pwm_45_matrix_row_2_, jaspar_phylofacts_pwm_45_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_45_ = {
/* accession        */ "PF0087",
/* name             */ "RYTGCNWTGGNR",
/* label            */ " PF0087	17.7580202976952	RYTGCNWTGGNR	Unknown	; MCS \"14.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTTGCCATGGCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_45_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.845940,
/* max_score        */ 14.687757,
/* threshold        */ 0.778,
/* info content     */ 17.748337,
/* base_counts      */ {854, 1143, 2069, 1454},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 460
};

	float jaspar_phylofacts_pwm_46_matrix_row_0_[10] = { -0.782262, -9.125340, -9.125340, -9.125340, -3.131379,  1.272874, -9.125340, -9.125340, -9.125340, -9.125340};
	float jaspar_phylofacts_pwm_46_matrix_row_1_[10] = { -8.884178,  1.514036, -8.884178,  1.514036,  0.573101, -8.884178, -8.884178, -8.884178, -8.884178,  0.541354};
	float jaspar_phylofacts_pwm_46_matrix_row_2_[10] = { -8.884178, -8.884178, -8.884178, -8.884178, -0.752353, -8.884178, -8.884178,  1.514036,  1.514036, -8.884178};
	float jaspar_phylofacts_pwm_46_matrix_row_3_[10] = {  1.135857, -9.125340,  1.272874, -9.125340,  0.567488, -9.125340,  1.272874, -9.125340, -9.125340,  0.797999};
	float *jaspar_phylofacts_pwm_46_matrix[4] = { jaspar_phylofacts_pwm_46_matrix_row_0_, jaspar_phylofacts_pwm_46_matrix_row_1_, jaspar_phylofacts_pwm_46_matrix_row_2_, jaspar_phylofacts_pwm_46_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_46_ = {
/* accession        */ "PF0059",
/* name             */ "WCTCNATGGY",
/* label            */ " PF0059	17.0423594843022	WCTCNATGGY	Unknown	; MCS \"19.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TCTCYATGGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_46_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.018280,
/* max_score        */ 12.381724,
/* threshold        */ 0.786,
/* info content     */ 17.029661,
/* base_counts      */ {374, 908, 690, 1308},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 328
};

	float jaspar_phylofacts_pwm_47_matrix_row_0_[8] = { -10.178127, -10.178127,  1.272934, -10.178127, -10.178127, -10.178127,  0.602931, -10.178127};
	float jaspar_phylofacts_pwm_47_matrix_row_1_[8] = { -9.936965, -9.936965, -9.936965, -9.936965, -9.936965, -9.936965, -9.936965,  0.439678};
	float jaspar_phylofacts_pwm_47_matrix_row_2_[8] = { -9.936965,  1.514096, -9.936965, -9.936965, -9.936965, -9.936965,  0.797277, -9.936965};
	float jaspar_phylofacts_pwm_47_matrix_row_3_[8] = {  1.272934, -10.178127, -10.178127,  1.272934,  1.272934,  1.272934, -10.178127,  0.855165};
	float *jaspar_phylofacts_pwm_47_matrix[4] = { jaspar_phylofacts_pwm_47_matrix_row_0_, jaspar_phylofacts_pwm_47_matrix_row_1_, jaspar_phylofacts_pwm_47_matrix_row_2_, jaspar_phylofacts_pwm_47_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_47_ = {
/* accession        */ "PF0094",
/* name             */ "TGATTTRY",
/* label            */ " PF0094	14.0741583645298	TGATTTRY	Unknown	; MCS \"13.9\" ; jaspar \"Gfi\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"GFI-1\" ; type \"phylogenetic\" ",
/* motif            */ "TGATTTAT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_47_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.425018,
/* max_score        */ 9.531207,
/* threshold        */ 0.818,
/* info content     */ 14.069992,
/* base_counts      */ {1421, 321, 1399, 4379},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 940
};

	float jaspar_phylofacts_pwm_48_matrix_row_0_[8] = { -12.119125, -12.119125, -12.119125, -12.119125,  1.272961,  1.272961, -12.119125, -12.119125};
	float jaspar_phylofacts_pwm_48_matrix_row_1_[8] = {  1.187620,  1.514123, -11.877963, -11.877963, -11.877963, -11.877963, -11.877963,  0.454306};
	float jaspar_phylofacts_pwm_48_matrix_row_2_[8] = {  0.235999, -11.877963,  1.514123,  1.514123, -11.877963, -11.877963,  1.514123, -11.877963};
	float jaspar_phylofacts_pwm_48_matrix_row_3_[8] = { -12.119125, -12.119125, -12.119125, -12.119125, -12.119125, -12.119125, -12.119125,  0.847521};
	float *jaspar_phylofacts_pwm_48_matrix[4] = { jaspar_phylofacts_pwm_48_matrix_row_0_, jaspar_phylofacts_pwm_48_matrix_row_1_, jaspar_phylofacts_pwm_48_matrix_row_2_, jaspar_phylofacts_pwm_48_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_48_ = {
/* accession        */ "PF0003",
/* name             */ "SCGGAAGY",
/* label            */ " PF0003	14.215601148395	SCGGAAGY	Unknown	; MCS \"80.4\" ; jaspar \"ELK4\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"ELK-1\" ; type \"phylogenetic\" ",
/* motif            */ "CCGGAAGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_48_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.953003,
/* max_score        */ 10.637556,
/* threshold        */ 0.816,
/* info content     */ 14.214907,
/* base_counts      */ {13096, 13541, 21468, 4279},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 6548
};

	float jaspar_phylofacts_pwm_49_matrix_row_0_[7] = { -11.312926, -11.312926,  1.272955, -0.004555, -11.312926, -11.312926,  1.272955};
	float jaspar_phylofacts_pwm_49_matrix_row_1_[7] = { -11.071764, -11.071764, -11.071764, -0.101826, -11.071764,  1.514117, -11.071764};
	float jaspar_phylofacts_pwm_49_matrix_row_2_[7] = { -11.071764, -11.071764, -11.071764,  0.033208, -11.071764, -11.071764, -11.071764};
	float jaspar_phylofacts_pwm_49_matrix_row_3_[7] = {  1.272955,  1.272955, -11.312926,  0.052670,  1.272955, -11.312926, -11.312926};
	float *jaspar_phylofacts_pwm_49_matrix[4] = { jaspar_phylofacts_pwm_49_matrix_row_0_, jaspar_phylofacts_pwm_49_matrix_row_1_, jaspar_phylofacts_pwm_49_matrix_row_2_, jaspar_phylofacts_pwm_49_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_49_ = {
/* accession        */ "PF0064",
/* name             */ "TTANTCA",
/* label            */ " PF0064	12.0175401946116	TTANTCA	Unknown	; MCS \"18.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"AP-1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "TTANTCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_49_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -67.979385,
/* max_score        */ 7.931565,
/* threshold        */ 0.848,
/* info content     */ 12.016333,
/* base_counts      */ {6663, 3505, 665, 9635},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2924
};

	float jaspar_phylofacts_pwm_50_matrix_row_0_[15] = { -8.877539, -8.877539, -8.877539, -8.877539, -0.264853, -1.136439, -0.775558,  0.489891, -0.129075,  0.640360, -8.877539, -8.877539,  1.272848,  0.662178,  0.109784};
	float jaspar_phylofacts_pwm_50_matrix_row_1_[15] = { -8.636376, -8.636376, -8.636376,  0.574064, -0.772725,  0.660234,  0.756369, -0.042037,  0.079832, -8.636376, -8.636376,  1.514011, -8.636376, -8.636376,  1.139335};
	float jaspar_phylofacts_pwm_50_matrix_row_2_[15] = { -8.636376,  1.514011, -8.636376, -8.636376,  0.231615, -0.475572,  0.063305, -0.060725, -0.504551,  0.756369,  1.514011, -8.636376, -8.636376,  0.731053, -8.636376};
	float jaspar_phylofacts_pwm_50_matrix_row_3_[15] = {  1.272848, -8.877539,  1.272848,  0.777552,  0.372119,  0.216381, -0.510935, -0.806320,  0.312702, -8.877539, -8.877539, -8.877539, -8.877539, -8.877539, -8.877539};
	float *jaspar_phylofacts_pwm_50_matrix[4] = { jaspar_phylofacts_pwm_50_matrix_row_0_, jaspar_phylofacts_pwm_50_matrix_row_1_, jaspar_phylofacts_pwm_50_matrix_row_2_, jaspar_phylofacts_pwm_50_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_50_ = {
/* accession        */ "PF0117",
/* name             */ "TGTYNNNNNRGCARM",
/* label            */ " PF0117	16.9753244542901	TGTYNNNNNRGCARM	Unknown	; MCS \"12.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGTTNYNNNAGCAAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 15,
/* matrixname       */ jaspar_phylofacts_pwm_50_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.770988,
/* max_score        */ 14.356201,
/* threshold        */ 0.769,
/* info content     */ 16.959000,
/* base_counts      */ {902, 902, 1002, 1034},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 256
};

	float jaspar_phylofacts_pwm_51_matrix_row_0_[14] = { -8.630722,  1.272816,  1.272816, -8.630722, -0.074116,  1.272816, -0.196693,  0.693036, -0.018037,  0.052155, -0.729344, -8.630722,  1.272816, -8.630722};
	float jaspar_phylofacts_pwm_51_matrix_row_1_[14] = {  0.943087, -8.389560, -8.389560, -8.389560,  0.670074, -8.389560, -0.046482, -8.389560,  0.167046, -0.200593,  0.420452,  1.513978, -8.389560, -8.389560};
	float jaspar_phylofacts_pwm_51_matrix_row_2_[14] = { -8.389560, -8.389560, -8.389560, -8.389560,  0.022495, -8.389560,  0.519811,  0.693061,  0.276226,  0.258837,  0.634572, -8.389560, -8.389560,  1.513978};
	float jaspar_phylofacts_pwm_51_matrix_row_3_[14] = {  0.440471, -8.630722, -8.630722,  1.272816, -1.191750, -8.630722, -0.387702, -8.630722, -0.469918, -0.154142, -0.889623, -8.630722, -8.630722, -8.630722};
	float *jaspar_phylofacts_pwm_51_matrix[4] = { jaspar_phylofacts_pwm_51_matrix_row_0_, jaspar_phylofacts_pwm_51_matrix_row_1_, jaspar_phylofacts_pwm_51_matrix_row_2_, jaspar_phylofacts_pwm_51_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_51_ = {
/* accession        */ "PF0147",
/* name             */ "YAATNANRNNNCAG",
/* label            */ " PF0147	16.5063350145917	YAATNANRNNNCAG	Unknown	; MCS \"10.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CAATNANANNSCAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_51_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.816086,
/* max_score        */ 13.387701,
/* threshold        */ 0.771,
/* info content     */ 16.486664,
/* base_counts      */ {1151, 596, 605, 448},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 200
};

	float jaspar_phylofacts_pwm_52_matrix_row_0_[6] = { -11.689239, -11.689239, -11.689239, -11.689239, -11.689239,  1.272959};
	float jaspar_phylofacts_pwm_52_matrix_row_1_[6] = {  1.514121, -11.448076, -11.448076, -11.448076, -11.448076, -11.448076};
	float jaspar_phylofacts_pwm_52_matrix_row_2_[6] = { -11.448076, -11.448076, -11.448076, -11.448076,  1.514121, -11.448076};
	float jaspar_phylofacts_pwm_52_matrix_row_3_[6] = { -11.689239,  1.272959,  1.272959,  1.272959, -11.689239, -11.689239};
	float *jaspar_phylofacts_pwm_52_matrix[4] = { jaspar_phylofacts_pwm_52_matrix_row_0_, jaspar_phylofacts_pwm_52_matrix_row_1_, jaspar_phylofacts_pwm_52_matrix_row_2_, jaspar_phylofacts_pwm_52_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_52_ = {
/* accession        */ "PF0073",
/* name             */ "CTTTGA",
/* label            */ " PF0073	12	CTTTGA	Unknown	; MCS \"17.0\" ; jaspar \"NR1H2-RXR\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"LEF1\" ; type \"phylogenetic\" ",
/* motif            */ "CTTTGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_52_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -70.135429,
/* max_score        */ 8.120076,
/* threshold        */ 0.861,
/* info content     */ 11.999148,
/* base_counts      */ {4260, 4260, 4260, 12780},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 4260
};

	float jaspar_phylofacts_pwm_53_matrix_row_0_[10] = { -10.451873, -10.451873, -10.451873, -0.333233, -0.187185, -10.451873, -10.451873, -10.451873,  1.272941,  0.333335};
	float jaspar_phylofacts_pwm_53_matrix_row_1_[10] = {  1.514103,  1.514103,  1.514103,  0.549339, -0.244201, -10.210711, -10.210711, -10.210711, -10.210711, -10.210711};
	float jaspar_phylofacts_pwm_53_matrix_row_2_[10] = { -10.210711, -10.210711, -10.210711, -0.084039,  0.636702,  1.514103,  1.514103,  1.514103, -10.210711,  1.018538};
	float jaspar_phylofacts_pwm_53_matrix_row_3_[10] = { -10.451873, -10.451873, -10.451873, -0.259416, -0.443980, -10.451873, -10.451873, -10.451873, -10.451873, -10.451873};
	float *jaspar_phylofacts_pwm_53_matrix[4] = { jaspar_phylofacts_pwm_53_matrix_row_0_, jaspar_phylofacts_pwm_53_matrix_row_1_, jaspar_phylofacts_pwm_53_matrix_row_2_, jaspar_phylofacts_pwm_53_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_53_ = {
/* accession        */ "PF0130",
/* name             */ "CCCNNGGGAR",
/* label            */ " PF0130	15.1978017393408	CCCNNGGGAR	Unknown	; MCS \"11.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"OLF-1\" ; type \"phylogenetic\" ",
/* motif            */ "CCCNNGGGAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_53_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.392197,
/* max_score        */ 12.562142,
/* threshold        */ 0.794,
/* info content     */ 15.194390,
/* base_counts      */ {2254, 4392, 5225, 489},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1236
};

	float jaspar_phylofacts_pwm_54_matrix_row_0_[7] = { -11.555842, -11.555842,  1.272958, -11.555842, -11.555842, -11.555842, -11.555842};
	float jaspar_phylofacts_pwm_54_matrix_row_1_[7] = { -11.314680, -11.314680, -11.314680,  1.514120,  1.514120, -11.314680,  0.623519};
	float jaspar_phylofacts_pwm_54_matrix_row_2_[7] = { -11.314680,  1.514120, -11.314680, -11.314680, -11.314680, -11.314680, -11.314680};
	float jaspar_phylofacts_pwm_54_matrix_row_3_[7] = {  1.272958, -11.555842, -11.555842, -11.555842, -11.555842,  1.272958,  0.744635};
	float *jaspar_phylofacts_pwm_54_matrix[4] = { jaspar_phylofacts_pwm_54_matrix_row_0_, jaspar_phylofacts_pwm_54_matrix_row_1_, jaspar_phylofacts_pwm_54_matrix_row_2_, jaspar_phylofacts_pwm_54_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_54_ = {
/* accession        */ "PF0025",
/* name             */ "TGACCTY",
/* label            */ " PF0025	13.0232859340055	TGACCTY	Unknown	; MCS \"33.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"ESRRA\" ; type \"phylogenetic\" ",
/* motif            */ "TGACCTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_54_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.890892,
/* max_score        */ 9.105867,
/* threshold        */ 0.838,
/* info content     */ 13.022221,
/* base_counts      */ {3728, 8986, 3728, 9654},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3728
};

	float jaspar_phylofacts_pwm_55_matrix_row_0_[13] = { -9.229468, -9.229468, -9.229468, -0.267461, -9.229468, -9.229468,  0.622779, -0.019028, -9.229468, -9.229468, -9.229468,  0.469513,  1.272883};
	float jaspar_phylofacts_pwm_55_matrix_row_1_[13] = { -8.988306, -8.988306, -8.988306, -0.981605,  1.276381,  1.037443, -0.026299, -0.745287, -8.988306, -8.988306, -8.988306, -0.981605, -8.988306};
	float jaspar_phylofacts_pwm_55_matrix_row_2_[13] = {  1.514045, -8.988306, -8.988306,  0.853359, -8.988306, -8.988306, -0.451114, -0.322521,  1.514045,  1.514045, -8.988306,  0.181316, -8.988306};
	float jaspar_phylofacts_pwm_55_matrix_row_3_[13] = { -9.229468,  1.272883,  1.272883, -0.404643, -0.280363,  0.303028, -0.817413,  0.499725, -9.229468, -9.229468,  1.272883, -0.306677, -9.229468};
	float *jaspar_phylofacts_pwm_55_matrix[4] = { jaspar_phylofacts_pwm_55_matrix_row_0_, jaspar_phylofacts_pwm_55_matrix_row_1_, jaspar_phylofacts_pwm_55_matrix_row_2_, jaspar_phylofacts_pwm_55_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_55_ = {
/* accession        */ "PF0089",
/* name             */ "GTTNYYNNGGTNA",
/* label            */ " PF0089	17.2628290239644	GTTNYYNNGGTNA	Unknown	; MCS \"14.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTTGCCANGGTNA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_55_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.591125,
/* max_score        */ 14.392870,
/* threshold        */ 0.776,
/* info content     */ 17.251274,
/* base_counts      */ {895, 689, 1485, 1663},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 364
};

	float jaspar_phylofacts_pwm_56_matrix_row_0_[10] = { -9.372555,  1.272894,  1.272894, -0.385233,  0.729825, -9.372555, -9.372555, -9.372555, -9.372555, -9.372555};
	float jaspar_phylofacts_pwm_56_matrix_row_1_[10] = { -9.131392, -9.131392, -9.131392,  0.510795, -0.336416,  0.371692,  0.156002, -9.131392,  1.514056, -9.131392};
	float jaspar_phylofacts_pwm_56_matrix_row_2_[10] = { -9.131392, -9.131392, -9.131392,  0.174349, -0.537053, -9.131392,  1.216813,  1.514056, -9.131392,  1.514056};
	float jaspar_phylofacts_pwm_56_matrix_row_3_[10] = {  1.272894, -9.372555, -9.372555, -0.436519, -0.741854,  0.888643, -9.372555, -9.372555, -9.372555, -9.372555};
	float *jaspar_phylofacts_pwm_56_matrix[4] = { jaspar_phylofacts_pwm_56_matrix_row_0_, jaspar_phylofacts_pwm_56_matrix_row_1_, jaspar_phylofacts_pwm_56_matrix_row_2_, jaspar_phylofacts_pwm_56_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_56_ = {
/* accession        */ "PF0162",
/* name             */ "TAANNYSGCG",
/* label            */ " PF0162	14.692517104862	TAANNYSGCG	Unknown	; MCS \"9.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TAANATGGCG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_56_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.158813,
/* max_score        */ 11.706927,
/* threshold        */ 0.797,
/* info content     */ 14.683696,
/* base_counts      */ {1164, 882, 1316, 838},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 420
};

	float jaspar_phylofacts_pwm_57_matrix_row_0_[10] = { -0.623676, -11.967010, -11.967010, -11.967010,  1.272960, -1.281884, -11.967010, -11.967010, -11.967010, -11.967010};
	float jaspar_phylofacts_pwm_57_matrix_row_1_[10] = { -11.725848,  1.514122, -11.725848,  1.514122, -11.725848, -0.148062, -11.725848,  1.514122, -11.725848,  1.373863};
	float jaspar_phylofacts_pwm_57_matrix_row_2_[10] = {  1.351520, -11.725848,  1.514122, -11.725848, -11.725848,  0.662134,  1.514122, -11.725848,  1.514122, -11.725848};
	float jaspar_phylofacts_pwm_57_matrix_row_3_[10] = { -11.967010, -11.967010, -11.967010, -11.967010, -11.967010,  0.088827, -11.967010, -11.967010, -11.967010, -0.760596};
	float *jaspar_phylofacts_pwm_57_matrix[4] = { jaspar_phylofacts_pwm_57_matrix_row_0_, jaspar_phylofacts_pwm_57_matrix_row_1_, jaspar_phylofacts_pwm_57_matrix_row_2_, jaspar_phylofacts_pwm_57_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_57_ = {
/* accession        */ "PF0001",
/* name             */ "RCGCANGCGY",
/* label            */ " PF0001	17.0417134599259	RCGCANGCGY	Unknown	; MCS \"107.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"NRF-1\" ; type \"phylogenetic\" ",
/* motif            */ "GCGCANGCGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_57_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -108.984978,
/* max_score        */ 13.745210,
/* threshold        */ 0.786,
/* info content     */ 17.040794,
/* base_counts      */ {6905, 22827, 24051, 2457},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 5624
};

	float jaspar_phylofacts_pwm_58_matrix_row_0_[11] = { -10.263716, -10.263716, -10.263716, -10.263716,  0.836735, -0.830151, -0.203181,  0.930395, -10.263716, -10.263716, -10.263716};
	float jaspar_phylofacts_pwm_58_matrix_row_1_[11] = { -10.022553,  1.514098,  1.514098,  1.514098, -10.022553, -0.497329,  0.826803, -10.022553, -10.022553, -10.022553,  1.514098};
	float jaspar_phylofacts_pwm_58_matrix_row_2_[11] = { -10.022553, -10.022553, -10.022553, -10.022553,  0.474289,  0.976224,  0.025078,  0.276383, -10.022553,  1.514098, -10.022553};
	float jaspar_phylofacts_pwm_58_matrix_row_3_[11] = {  1.272936, -10.263716, -10.263716, -10.263716, -10.263716, -0.558618, -1.874128, -10.263716,  1.272936, -10.263716, -10.263716};
	float *jaspar_phylofacts_pwm_58_matrix[4] = { jaspar_phylofacts_pwm_58_matrix_row_0_, jaspar_phylofacts_pwm_58_matrix_row_1_, jaspar_phylofacts_pwm_58_matrix_row_2_, jaspar_phylofacts_pwm_58_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_58_ = {
/* accession        */ "PF0039",
/* name             */ "TCCCRNNRTGC",
/* label            */ " PF0039	16.8939815609571	TCCCRNNRTGC	Unknown	; MCS \"24.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TCCCAGCATGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_58_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.077728,
/* max_score        */ 13.686522,
/* threshold        */ 0.783,
/* info content     */ 16.889534,
/* base_counts      */ {1748, 4748, 2512, 2256},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1024
};

	float jaspar_phylofacts_pwm_59_matrix_row_0_[15] = {  0.214822, -9.514372,  1.272904,  1.272904, -9.514372, -0.431751, -0.003853, -0.938721,  0.190726, -0.313981, -0.466433, -0.011288, -9.514372, -9.514372, -9.514372};
	float jaspar_phylofacts_pwm_59_matrix_row_1_[15] = {  1.087734,  1.240070, -9.273210, -9.273210, -9.273210,  0.400927,  0.237309,  0.102730,  0.368978, -0.350418,  0.287858,  0.437966, -9.273210, -9.273210,  1.514066};
	float jaspar_phylofacts_pwm_59_matrix_row_2_[15] = { -9.273210, -9.273210, -9.273210, -9.273210, -9.273210,  0.467817,  0.388270,  0.937799, -0.082970,  0.820939,  0.693299,  0.119535,  1.514066,  1.514066, -9.273210};
	float jaspar_phylofacts_pwm_59_matrix_row_3_[15] = { -9.514372, -0.155525, -9.514372, -9.514372,  1.272904, -0.704360, -0.865975, -1.195386, -0.689547, -0.689547, -1.124785, -0.734661, -9.514372, -9.514372, -9.514372};
	float *jaspar_phylofacts_pwm_59_matrix[4] = { jaspar_phylofacts_pwm_59_matrix_row_0_, jaspar_phylofacts_pwm_59_matrix_row_1_, jaspar_phylofacts_pwm_59_matrix_row_2_, jaspar_phylofacts_pwm_59_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_59_ = {
/* accession        */ "PF0138",
/* name             */ "MYAATNNNNNNNGGC",
/* label            */ " PF0138	15.4141113795629	MYAATNNNNNNNGGC	Unknown	; MCS \"11.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCAATNNGNNNNGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 15,
/* matrixname       */ jaspar_phylofacts_pwm_59_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.119247,
/* max_score        */ 14.803782,
/* threshold        */ 0.761,
/* info content     */ 15.406238,
/* base_counts      */ {1894, 2116, 2240, 1010},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 484
};

	float jaspar_phylofacts_pwm_60_matrix_row_0_[11] = {  1.272910,  1.272910, -9.616413,  1.084604, -0.457261, -0.680378, -1.226826, -9.616413, -9.616413, -9.616413, -9.616413};
	float jaspar_phylofacts_pwm_60_matrix_row_1_[11] = { -9.375251, -9.375251, -9.375251, -9.375251, -9.375251, -9.375251, -1.158893,  1.140743, -9.375251, -9.375251,  1.514072};
	float jaspar_phylofacts_pwm_60_matrix_row_2_[11] = { -9.375251, -9.375251,  1.514072, -9.375251, -9.375251,  1.361167,  1.260628, -9.375251,  1.514072,  1.514072, -9.375251};
	float jaspar_phylofacts_pwm_60_matrix_row_3_[11] = { -9.616413, -9.616413, -9.616413, -0.489346,  1.077824, -9.616413, -1.347425,  0.106811, -9.616413, -9.616413, -9.616413};
	float *jaspar_phylofacts_pwm_60_matrix[4] = { jaspar_phylofacts_pwm_60_matrix_row_0_, jaspar_phylofacts_pwm_60_matrix_row_1_, jaspar_phylofacts_pwm_60_matrix_row_2_, jaspar_phylofacts_pwm_60_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_60_ = {
/* accession        */ "PF0063",
/* name             */ "AAGWWRNYGGC",
/* label            */ " PF0063	18.0595027713403	AAGWWRNYGGC	Unknown	; MCS \"19.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AAGATGGCGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_60_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.029221,
/* max_score        */ 14.527072,
/* threshold        */ 0.780,
/* info content     */ 18.051060,
/* base_counts      */ {1731, 942, 2484, 739},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 536
};

	float jaspar_phylofacts_pwm_61_matrix_row_0_[8] = {  0.679707, -11.746709, -11.746709, -11.746709, -11.746709, -11.746709, -11.746709, -11.746709};
	float jaspar_phylofacts_pwm_61_matrix_row_1_[8] = { -11.505548,  1.002114, -11.505548, -11.505548,  1.514121,  1.514121, -11.505548, -11.505548};
	float jaspar_phylofacts_pwm_61_matrix_row_2_[8] = {  0.709986, -11.505548, -11.505548, -11.505548, -11.505548, -11.505548, -11.505548,  1.514121};
	float jaspar_phylofacts_pwm_61_matrix_row_3_[8] = { -11.746709,  0.358443,  1.272959,  1.272959, -11.746709, -11.746709,  1.272959, -11.746709};
	float *jaspar_phylofacts_pwm_61_matrix[4] = { jaspar_phylofacts_pwm_61_matrix_row_0_, jaspar_phylofacts_pwm_61_matrix_row_1_, jaspar_phylofacts_pwm_61_matrix_row_2_, jaspar_phylofacts_pwm_61_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_61_ = {
/* accession        */ "PF0016",
/* name             */ "RYTTCCTG",
/* label            */ " PF0016	14.0366116741218	RYTTCCTG	Unknown	; MCS \"43.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"C-ETS-2\" ; type \"phylogenetic\" ",
/* motif            */ "ACTTCCTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_61_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.973679,
/* max_score        */ 10.073339,
/* threshold        */ 0.818,
/* info content     */ 14.035634,
/* base_counts      */ {2493, 11728, 6531, 15344},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 4512
};

	float jaspar_phylofacts_pwm_62_matrix_row_0_[8] = { -11.919677, -11.919677, -11.919677, -11.919677, -11.919677, -11.919677, -0.114821, -0.238690};
	float jaspar_phylofacts_pwm_62_matrix_row_1_[8] = { -11.678515, -11.678515, -11.678515, -11.678515, -11.678515, -11.678515, -11.678515, -11.678515};
	float jaspar_phylofacts_pwm_62_matrix_row_2_[8] = {  1.514122,  1.514122,  1.514122, -11.678515,  1.514122,  1.514122,  1.226938,  1.264963};
	float jaspar_phylofacts_pwm_62_matrix_row_3_[8] = { -11.919677, -11.919677, -11.919677,  1.272960, -11.919677, -11.919677, -11.919677, -11.919677};
	float *jaspar_phylofacts_pwm_62_matrix[4] = { jaspar_phylofacts_pwm_62_matrix_row_0_, jaspar_phylofacts_pwm_62_matrix_row_1_, jaspar_phylofacts_pwm_62_matrix_row_2_, jaspar_phylofacts_pwm_62_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_62_ = {
/* accession        */ "PF0056",
/* name             */ "GGGTGGRR",
/* label            */ " PF0056	14.4281531120845	GGGTGGRR	Unknown	; MCS \"20.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"PAX-4\" ; type \"phylogenetic\" ",
/* motif            */ "GGGTGGGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_62_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.357414,
/* max_score        */ 11.335472,
/* threshold        */ 0.815,
/* info content     */ 14.427315,
/* base_counts      */ {2522, 0, 35026, 5364},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 5364
};

	float jaspar_phylofacts_pwm_63_matrix_row_0_[12] = {  0.596112, -9.229468, -9.229468, -9.229468, -0.158275, -0.732274,  0.057926, -9.229468, -9.229468, -9.229468, -9.229468,  0.751027};
	float jaspar_phylofacts_pwm_63_matrix_row_1_[12] = { -8.988306, -8.988306, -8.988306, -8.988306, -0.106331,  0.551410, -0.134498,  0.222134, -8.988306, -8.988306, -8.988306,  0.614144};
	float jaspar_phylofacts_pwm_63_matrix_row_2_[12] = {  0.804306, -8.988306, -8.988306, -8.988306,  0.344340, -0.178294, -0.645228, -8.988306, -8.988306,  1.514045,  1.514045, -8.988306};
	float jaspar_phylofacts_pwm_63_matrix_row_3_[12] = { -9.229468,  1.272883,  1.272883,  1.272883, -0.102401,  0.067142,  0.345585,  0.951689,  1.272883, -9.229468, -9.229468, -9.229468};
	float *jaspar_phylofacts_pwm_63_matrix[4] = { jaspar_phylofacts_pwm_63_matrix_row_0_, jaspar_phylofacts_pwm_63_matrix_row_1_, jaspar_phylofacts_pwm_63_matrix_row_2_, jaspar_phylofacts_pwm_63_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_63_ = {
/* accession        */ "PF0107",
/* name             */ "RTTTNNNYTGGM",
/* label            */ " PF0107	15.4395660004642	RTTTNNNYTGGM	Unknown	; MCS \"12.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ATTTNNNTTGGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_63_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.600998,
/* max_score        */ 11.867979,
/* threshold        */ 0.780,
/* info content     */ 15.428700,
/* base_counts      */ {645, 529, 1129, 2065},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 364
};

	float jaspar_phylofacts_pwm_64_matrix_row_0_[13] = { -8.923341, -0.629041, -8.923341,  1.272854,  1.272854,  0.863669, -0.916640,  0.225231, -0.489312, -1.427243, -8.923341, -8.923341,  1.272854};
	float jaspar_phylofacts_pwm_64_matrix_row_1_[13] = {  1.514016, -8.682178, -8.682178, -8.682178, -8.682178, -8.682178,  0.329833, -0.610960, -0.205599,  1.206246,  1.514016, -8.682178, -8.682178};
	float jaspar_phylofacts_pwm_64_matrix_row_2_[13] = { -8.682178,  1.352381,  1.514016, -8.682178, -8.682178,  0.422912,  0.171630, -0.016393,  1.016803, -0.439159, -8.682178,  1.514016, -8.682178};
	float jaspar_phylofacts_pwm_64_matrix_row_3_[13] = { -8.923341, -8.923341, -8.923341, -8.923341, -8.923341, -8.923341,  0.136293,  0.112765, -1.919367, -1.609454, -8.923341, -8.923341, -8.923341};
	float *jaspar_phylofacts_pwm_64_matrix[4] = { jaspar_phylofacts_pwm_64_matrix_row_0_, jaspar_phylofacts_pwm_64_matrix_row_1_, jaspar_phylofacts_pwm_64_matrix_row_2_, jaspar_phylofacts_pwm_64_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_64_ = {
/* accession        */ "PF0100",
/* name             */ "CRGAARNNNNCGA",
/* label            */ " PF0100	17.9373932225313	CRGAARNNNNCGA	Unknown	; MCS \"13.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CGGAAANNGCCGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_64_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.366486,
/* max_score        */ 14.868788,
/* threshold        */ 0.777,
/* info content     */ 17.921900,
/* base_counts      */ {1210, 895, 1183, 196},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 268
};

	float jaspar_phylofacts_pwm_65_matrix_row_0_[11] = { -9.261900, -0.613503, -9.261900,  1.272886, -9.261900, -1.101096, -1.324168,  0.079556, -9.261900, -9.261900,  1.272886};
	float jaspar_phylofacts_pwm_65_matrix_row_1_[11] = { -9.020738,  1.349655,  1.514048, -9.020738, -9.020738,  0.482347, -0.241027, -9.020738, -9.020738, -9.020738, -9.020738};
	float jaspar_phylofacts_pwm_65_matrix_row_2_[11] = {  1.093861, -9.020738, -9.020738, -9.020738, -9.020738,  0.095402, -0.372341, -9.020738,  1.514048,  1.514048, -9.020738};
	float jaspar_phylofacts_pwm_65_matrix_row_3_[11] = {  0.203160, -9.261900, -9.261900, -9.261900,  1.272886,  0.096947,  0.763849,  0.911653, -9.261900, -9.261900, -9.261900};
	float *jaspar_phylofacts_pwm_65_matrix[4] = { jaspar_phylofacts_pwm_65_matrix_row_0_, jaspar_phylofacts_pwm_65_matrix_row_1_, jaspar_phylofacts_pwm_65_matrix_row_2_, jaspar_phylofacts_pwm_65_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_65_ = {
/* accession        */ "PF0116",
/* name             */ "KMCATNNWGGA",
/* label            */ " PF0116	16.1343473680798	KMCATNNWGGA	Unknown	; MCS \"12.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GCCATNTTGGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_65_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.541206,
/* max_score        */ 12.962166,
/* threshold        */ 0.785,
/* info content     */ 16.123667,
/* base_counts      */ {986, 894, 1147, 1109},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 376
};

	float jaspar_phylofacts_pwm_66_matrix_row_0_[12] = {  0.703569,  1.272908,  1.272908, -9.578389, -0.603644, -9.578389,  0.024061, -0.177345, -9.578389, -9.578389, -9.578389, -9.578389};
	float jaspar_phylofacts_pwm_66_matrix_row_1_[12] = { -9.337227, -9.337227, -9.337227, -9.337227, -0.097230,  0.679634, -0.087569,  0.237826,  1.514070, -9.337227, -9.337227,  0.706066};
	float jaspar_phylofacts_pwm_66_matrix_row_2_[12] = {  0.679634, -9.337227, -9.337227,  1.514070,  0.444150, -9.337227,  0.150821,  0.021620, -9.337227, -9.337227, -9.337227, -9.337227};
	float jaspar_phylofacts_pwm_66_matrix_row_3_[12] = { -9.578389, -9.578389, -9.578389, -9.578389,  0.083091,  0.703569, -0.090341, -0.067870, -9.578389,  1.272908,  1.272908,  0.682808};
	float *jaspar_phylofacts_pwm_66_matrix[4] = { jaspar_phylofacts_pwm_66_matrix_row_0_, jaspar_phylofacts_pwm_66_matrix_row_1_, jaspar_phylofacts_pwm_66_matrix_row_2_, jaspar_phylofacts_pwm_66_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_66_ = {
/* accession        */ "PF0125",
/* name             */ "RAAGNYNNCTTY",
/* label            */ " PF0125	15.1198084909393	RAAGNYNNCTTY	Unknown	; MCS \"11.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AAAGNTNNCTTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_66_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.076836,
/* max_score        */ 11.065772,
/* threshold        */ 0.780,
/* info content     */ 15.111957,
/* base_counts      */ {1672, 1321, 1165, 2034},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 516
};

	float jaspar_phylofacts_pwm_67_matrix_row_0_[8] = {  0.982197, -11.015627, -11.015627, -11.015627, -11.015627, -11.015627, -11.015627, -11.015627};
	float jaspar_phylofacts_pwm_67_matrix_row_1_[8] = { -10.774465, -10.774465, -10.774465, -10.774465, -10.774465,  1.514114, -10.774465, -10.774465};
	float jaspar_phylofacts_pwm_67_matrix_row_2_[8] = { -10.774465, -10.774465, -10.774465,  1.514114,  1.234991, -10.774465, -10.774465,  1.514114};
	float jaspar_phylofacts_pwm_67_matrix_row_3_[8] = { -0.104163,  1.272952,  1.272952, -11.015627, -0.139449, -11.015627,  1.272952, -11.015627};
	float *jaspar_phylofacts_pwm_67_matrix[4] = { jaspar_phylofacts_pwm_67_matrix_row_0_, jaspar_phylofacts_pwm_67_matrix_row_1_, jaspar_phylofacts_pwm_67_matrix_row_2_, jaspar_phylofacts_pwm_67_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_67_ = {
/* accession        */ "PF0046",
/* name             */ "WTTGKCTG",
/* label            */ " PF0046	14.3841923888905	WTTGKCTG	Unknown	; MCS \"23.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ATTGGCTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_67_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.883842,
/* max_score        */ 10.578384,
/* threshold        */ 0.815,
/* info content     */ 14.382257,
/* base_counts      */ {1624, 2172, 5987, 7593},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2172
};

	float jaspar_phylofacts_pwm_68_matrix_row_0_[9] = {  0.503335, -11.145003,  1.272954,  1.272954,  0.158447, -0.368112, -11.145003, -11.145003, -11.145003};
	float jaspar_phylofacts_pwm_68_matrix_row_1_[9] = { -10.903841, -10.903841, -10.903841, -10.903841,  0.046983,  0.211602, -10.903841, -10.903841,  1.514116};
	float jaspar_phylofacts_pwm_68_matrix_row_2_[9] = {  0.892012,  1.514116, -10.903841, -10.903841,  0.165933,  0.170595, -10.903841, -10.903841, -10.903841};
	float jaspar_phylofacts_pwm_68_matrix_row_3_[9] = { -11.145003, -11.145003, -11.145003, -11.145003, -0.432788, -0.023625,  1.272954,  1.272954, -11.145003};
	float *jaspar_phylofacts_pwm_68_matrix[4] = { jaspar_phylofacts_pwm_68_matrix_row_0_, jaspar_phylofacts_pwm_68_matrix_row_1_, jaspar_phylofacts_pwm_68_matrix_row_2_, jaspar_phylofacts_pwm_68_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_68_ = {
/* accession        */ "PF0068",
/* name             */ "RGAANNTTC",
/* label            */ " PF0068	13.0493301441172	RGAANNTTC	Unknown	; MCS \"17.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"HSF1\" ; type \"phylogenetic\" ",
/* motif            */ "GGAANNTTC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_68_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.815926,
/* max_score        */ 9.389592,
/* threshold        */ 0.815,
/* info content     */ 13.047771,
/* base_counts      */ {7379, 3714, 5086, 6069},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2472
};

	float jaspar_phylofacts_pwm_69_matrix_row_0_[11] = { -9.608923,  1.272909,  1.272909, -9.608923,  0.006949,  0.726379, -0.330831, -0.191487, -9.608923, -9.608923, -9.608923};
	float jaspar_phylofacts_pwm_69_matrix_row_1_[11] = { -9.367761, -9.367761, -9.367761,  0.402252, -0.240694, -9.367761,  0.489735,  0.193307, -9.367761,  1.514071,  1.514071};
	float jaspar_phylofacts_pwm_69_matrix_row_2_[11] = { -9.367761, -9.367761, -9.367761, -9.367761, -0.343630,  0.649100,  0.024984, -0.331655, -9.367761, -9.367761, -9.367761};
	float jaspar_phylofacts_pwm_69_matrix_row_3_[11] = {  1.272909, -9.608923, -9.608923,  0.874011,  0.329014, -9.608923, -0.267467,  0.205788,  1.272909, -9.608923, -9.608923};
	float *jaspar_phylofacts_pwm_69_matrix[4] = { jaspar_phylofacts_pwm_69_matrix_row_0_, jaspar_phylofacts_pwm_69_matrix_row_1_, jaspar_phylofacts_pwm_69_matrix_row_2_, jaspar_phylofacts_pwm_69_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_69_ = {
/* accession        */ "PF0136",
/* name             */ "TAAYNRNNTCC",
/* label            */ " PF0136	14.2994433643608	TAAYNRNNTCC	Unknown	; MCS \"11.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TAATNANNTCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_69_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.877502,
/* max_score        */ 10.744707,
/* threshold        */ 0.790,
/* info content     */ 14.292391,
/* base_counts      */ {1752, 1664, 511, 1925},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 532
};

	float jaspar_phylofacts_pwm_70_matrix_row_0_[14] = { -9.293313, -4.678193,  1.272888, -9.293313,  1.272888,  0.610225, -0.775920, -1.429662, -9.293313, -9.293313, -9.293313,  1.272888, -9.293313,  1.272888};
	float jaspar_phylofacts_pwm_70_matrix_row_1_[14] = {  1.436370, -0.554956, -9.052151,  1.514050, -9.052151, -0.618122,  0.412909, -0.554956,  1.346063,  1.469248,  1.503688, -9.052151, -9.052151, -9.052151};
	float jaspar_phylofacts_pwm_70_matrix_row_2_[14] = { -1.079340, -1.268510, -9.052151, -9.052151, -9.052151, -0.662564, -0.596620, -1.807209, -9.052151, -1.613179, -9.052151, -9.052151,  1.514050, -9.052151};
	float jaspar_phylofacts_pwm_70_matrix_row_3_[14] = { -9.293313,  1.061282, -9.293313, -9.293313, -9.293313, -0.103073,  0.399515,  1.012334, -0.593632, -9.293313, -3.299351, -9.293313, -9.293313, -9.293313};
	float *jaspar_phylofacts_pwm_70_matrix[4] = { jaspar_phylofacts_pwm_70_matrix_row_0_, jaspar_phylofacts_pwm_70_matrix_row_1_, jaspar_phylofacts_pwm_70_matrix_row_2_, jaspar_phylofacts_pwm_70_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_70_ = {
/* accession        */ "PF0080",
/* name             */ "SNACANNNYSYAGA",
/* label            */ " PF0080	21.1382301888602	SNACANNNYSYAGA	Unknown	; MCS \"15.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTACAAYTCCCAGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_70_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.857010,
/* max_score        */ 16.971775,
/* threshold        */ 0.785,
/* info content     */ 21.126112,
/* base_counts      */ {1829, 2103, 563, 937},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 388
};

	float jaspar_phylofacts_pwm_71_matrix_row_0_[14] = { -9.638553, -9.638553,  1.272911, -0.489981, -9.638553,  0.572456, -2.199581,  0.762706,  0.165170, -1.395533, -9.638553, -9.638553, -9.638553, -9.638553};
	float jaspar_phylofacts_pwm_71_matrix_row_1_[14] = { -9.397390, -9.397390, -9.397390,  1.325899, -9.397390,  0.349501,  1.287736, -1.181032, -0.840785,  0.313786, -9.397390,  1.514073,  1.514073,  1.457774};
	float jaspar_phylofacts_pwm_71_matrix_row_2_[14] = {  1.514073,  1.514073, -9.397390, -9.397390, -9.397390, -0.941860, -1.078405,  0.113128, -0.337757, -0.602414, -9.397390, -9.397390, -9.397390, -9.397390};
	float jaspar_phylofacts_pwm_71_matrix_row_3_[14] = { -9.638553, -9.638553, -9.638553, -9.638553,  1.272911, -0.972767, -1.062902, -1.183022,  0.400383,  0.597865,  1.272911, -9.638553, -9.638553, -1.631852};
	float *jaspar_phylofacts_pwm_71_matrix[4] = { jaspar_phylofacts_pwm_71_matrix_row_0_, jaspar_phylofacts_pwm_71_matrix_row_1_, jaspar_phylofacts_pwm_71_matrix_row_2_, jaspar_phylofacts_pwm_71_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_71_ = {
/* accession        */ "PF0074",
/* name             */ "GGAMTNNNNNTCCY",
/* label            */ " PF0074	19.3843172792854	GGAMTNNNNNTCCY	Unknown	; MCS \"16.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGACTMCANTTCCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_71_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.338654,
/* max_score        */ 16.279844,
/* threshold        */ 0.779,
/* info content     */ 19.376055,
/* base_counts      */ {1479, 2930, 1471, 1792},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 548
};

	float jaspar_phylofacts_pwm_72_matrix_row_0_[10] = { -9.514372, -9.514372, -9.514372, -0.466433, -9.514372, -9.514372,  1.272904, -9.514372, -9.514372, -0.443179};
	float jaspar_phylofacts_pwm_72_matrix_row_1_[10] = { -9.273210, -9.273210,  1.514066, -1.335478,  1.402721,  1.220866, -9.273210, -9.273210, -9.273210, -9.273210};
	float jaspar_phylofacts_pwm_72_matrix_row_2_[10] = {  1.514066, -9.273210, -9.273210,  0.168321, -9.273210, -9.273210, -9.273210, -9.273210,  1.514066,  1.315922};
	float jaspar_phylofacts_pwm_72_matrix_row_3_[10] = { -9.514372,  1.272904, -9.514372,  0.592097, -0.977180, -0.096936, -9.514372,  1.272904, -9.514372, -9.514372};
	float *jaspar_phylofacts_pwm_72_matrix[4] = { jaspar_phylofacts_pwm_72_matrix_row_0_, jaspar_phylofacts_pwm_72_matrix_row_1_, jaspar_phylofacts_pwm_72_matrix_row_2_, jaspar_phylofacts_pwm_72_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_72_ = {
/* accession        */ "PF0098",
/* name             */ "GTCNYYATGR",
/* label            */ " PF0098	16.3356838349734	GTCNYYATGR	Unknown	; MCS \"13.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTCTCCATGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_72_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.964836,
/* max_score        */ 12.892513,
/* threshold        */ 0.789,
/* info content     */ 16.327181,
/* base_counts      */ {656, 1306, 1491, 1387},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 484
};

	float jaspar_phylofacts_pwm_73_matrix_row_0_[10] = { -9.781157, -9.781157, -9.781157, -9.781157, -9.781157,  0.815503, -1.032694, -9.781157, -9.781157, -9.781157};
	float jaspar_phylofacts_pwm_73_matrix_row_1_[10] = {  1.044086, -9.539995, -9.539995, -9.539995,  1.514080, -9.539995,  0.494565, -9.539995, -9.539995, -9.539995};
	float jaspar_phylofacts_pwm_73_matrix_row_2_[10] = {  0.533277, -9.539995, -9.539995, -9.539995, -9.539995,  0.511955,  0.252617, -9.539995, -9.539995, -9.539995};
	float jaspar_phylofacts_pwm_73_matrix_row_3_[10] = { -9.781157,  1.272918,  1.272918,  1.272918, -9.781157, -9.781157, -0.088329,  1.272918,  1.272918,  1.272918};
	float *jaspar_phylofacts_pwm_73_matrix[4] = { jaspar_phylofacts_pwm_73_matrix_row_0_, jaspar_phylofacts_pwm_73_matrix_row_1_, jaspar_phylofacts_pwm_73_matrix_row_2_, jaspar_phylofacts_pwm_73_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_73_ = {
/* accession        */ "PF0085",
/* name             */ "STTTCRNTTT",
/* label            */ " PF0085	16.2160430288651	STTTCRNTTT	Unknown	; MCS \"14.9\" ; jaspar \"IRF2\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"IRF\" ; type \"phylogenetic\" ",
/* motif            */ "CTTTCANTTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_73_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.063110,
/* max_score        */ 11.505745,
/* threshold        */ 0.790,
/* info content     */ 16.209204,
/* base_counts      */ {463, 1255, 648, 3954},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 632
};

	float jaspar_phylofacts_pwm_74_matrix_row_0_[13] = { -9.009095,  0.358334, -9.009095, -9.009095,  1.272863, -9.009095, -0.740107, -3.015134, -9.009095, -1.071364, -9.009095, -9.009095, -9.009095};
	float jaspar_phylofacts_pwm_74_matrix_row_1_[13] = {  1.370666, -8.767933,  1.514025,  1.514025, -8.767933, -8.767933,  0.056892, -0.312403, -8.767933,  0.391219,  1.514025, -8.767933,  1.514025};
	float jaspar_phylofacts_pwm_74_matrix_row_2_[13] = { -8.767933,  1.002080, -8.767933, -8.767933, -8.767933,  1.330340,  0.967195,  0.764563,  1.514025,  0.861183, -8.767933,  1.514025, -8.767933};
	float jaspar_phylofacts_pwm_74_matrix_row_3_[13] = { -0.740107, -9.009095, -9.009095, -9.009095, -9.009095, -0.511901, -1.630712,  0.230901, -9.009095, -1.570124, -9.009095, -9.009095, -9.009095};
	float *jaspar_phylofacts_pwm_74_matrix[4] = { jaspar_phylofacts_pwm_74_matrix_row_0_, jaspar_phylofacts_pwm_74_matrix_row_1_, jaspar_phylofacts_pwm_74_matrix_row_2_, jaspar_phylofacts_pwm_74_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_74_ = {
/* accession        */ "PF0156",
/* name             */ "YRCCAKNNGNCGC",
/* label            */ " PF0156	19.114358671457	YRCCAKNNGNCGC	Unknown	; MCS \"10.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CGCCAGGKGGCGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_74_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.306915,
/* max_score        */ 16.653038,
/* threshold        */ 0.778,
/* info content     */ 19.098904,
/* base_counts      */ {480, 1631, 1461, 224},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 292
};

	float jaspar_phylofacts_pwm_75_matrix_row_0_[12] = { -8.981319, -8.981319,  1.272860, -2.295459, -0.591733,  1.272860, -8.981319,  0.593733, -8.981319, -8.981319, -8.981319, -8.981319};
	float jaspar_phylofacts_pwm_75_matrix_row_1_[12] = {  1.514022,  1.514022, -8.740157,  0.799558,  0.895516, -8.740157, -8.740157, -8.740157, -8.740157, -8.740157, -8.740157,  1.514022};
	float jaspar_phylofacts_pwm_75_matrix_row_2_[12] = { -8.740157, -8.740157, -8.740157,  0.701374, -0.733457, -8.740157,  1.514022,  0.806726,  0.940249,  1.514022,  1.514022, -8.740157};
	float jaspar_phylofacts_pwm_75_matrix_row_3_[12] = { -8.981319, -8.981319, -8.981319, -1.977345, -0.332923, -8.981319, -8.981319, -8.981319,  0.444213, -8.981319, -8.981319, -8.981319};
	float *jaspar_phylofacts_pwm_75_matrix[4] = { jaspar_phylofacts_pwm_75_matrix_row_0_, jaspar_phylofacts_pwm_75_matrix_row_1_, jaspar_phylofacts_pwm_75_matrix_row_2_, jaspar_phylofacts_pwm_75_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_75_ = {
/* accession        */ "PF0045",
/* name             */ "CCANNAGRKGGC",
/* label            */ " PF0045	18.9552307732213	CCANNAGRKGGC	Unknown	; MCS \"23.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCASCAGAGGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_75_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.842117,
/* max_score        */ 15.071902,
/* threshold        */ 0.777,
/* info content     */ 18.939049,
/* base_counts      */ {764, 1144, 1308, 192},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 284
};

	float jaspar_phylofacts_pwm_76_matrix_row_0_[8] = {  0.728459, -10.378016, -10.378016,  1.272940,  1.272940, -10.378016, -10.378016, -10.378016};
	float jaspar_phylofacts_pwm_76_matrix_row_1_[8] = { -10.136854, -10.136854, -10.136854, -10.136854, -10.136854, -10.136854, -10.136854,  0.660699};
	float jaspar_phylofacts_pwm_76_matrix_row_2_[8] = { -10.136854,  1.514102,  1.514102, -10.136854, -10.136854, -10.136854,  1.514102, -10.136854};
	float jaspar_phylofacts_pwm_76_matrix_row_3_[8] = {  0.405119, -10.378016, -10.378016, -10.378016, -10.378016,  1.272940, -10.378016,  0.717893};
	float *jaspar_phylofacts_pwm_76_matrix[4] = { jaspar_phylofacts_pwm_76_matrix_row_0_, jaspar_phylofacts_pwm_76_matrix_row_1_, jaspar_phylofacts_pwm_76_matrix_row_2_, jaspar_phylofacts_pwm_76_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_76_ = {
/* accession        */ "PF0079",
/* name             */ "WGGAATGY",
/* label            */ " PF0079	14.0344876453022	WGGAATGY	Unknown	; MCS \"16.1\" ; jaspar \"TEAD\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"TEF-1\" ; type \"phylogenetic\" ",
/* motif            */ "AGGAATGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_76_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.782967,
/* max_score        */ 9.807475,
/* threshold        */ 0.818,
/* info content     */ 14.031023,
/* base_counts      */ {2962, 489, 3444, 2289},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1148
};

	float jaspar_phylofacts_pwm_77_matrix_row_0_[7] = { -9.883342, -9.883342, -9.883342, -9.883342,  1.272923, -9.883342, -9.883342};
	float jaspar_phylofacts_pwm_77_matrix_row_1_[7] = {  1.514085, -9.642179, -9.642179,  0.731343, -9.642179,  1.514085, -9.642179};
	float jaspar_phylofacts_pwm_77_matrix_row_2_[7] = { -9.642179,  1.514085, -9.642179,  0.903188, -9.642179, -9.642179,  1.514085};
	float jaspar_phylofacts_pwm_77_matrix_row_3_[7] = { -9.883342, -9.883342,  1.272923, -9.883342, -9.883342, -9.883342, -9.883342};
	float *jaspar_phylofacts_pwm_77_matrix[4] = { jaspar_phylofacts_pwm_77_matrix_row_0_, jaspar_phylofacts_pwm_77_matrix_row_1_, jaspar_phylofacts_pwm_77_matrix_row_2_, jaspar_phylofacts_pwm_77_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_77_ = {
/* accession        */ "PF0070",
/* name             */ "CGTSACG",
/* label            */ " PF0070	13.0053062046387	CGTSACG	Unknown	; MCS \"17.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"PAX-3\" ; type \"phylogenetic\" ",
/* motif            */ "CGTGACG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_77_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -69.183395,
/* max_score        */ 9.505373,
/* threshold        */ 0.838,
/* info content     */ 13.000324,
/* base_counts      */ {700, 1720, 1780, 700},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 700
};

	float jaspar_phylofacts_pwm_78_matrix_row_0_[10] = { -9.062434, -9.062434, -9.062434, -9.062434, -9.062434, -0.505828, -9.062434, -9.062434,  1.272868,  1.272868};
	float jaspar_phylofacts_pwm_78_matrix_row_1_[10] = { -8.821272, -8.821272, -8.821272, -8.821272,  1.289270, -8.821272, -0.011260, -8.821272, -8.821272, -8.821272};
	float jaspar_phylofacts_pwm_78_matrix_row_2_[10] = {  0.767573, -8.821272,  1.514030,  1.514030, -8.821272,  1.329115,  1.268736,  1.514030, -8.821272, -8.821272};
	float jaspar_phylofacts_pwm_78_matrix_row_3_[10] = {  0.630394,  1.272868, -9.062434, -9.062434, -0.329968, -9.062434, -9.062434, -9.062434, -9.062434, -9.062434};
	float *jaspar_phylofacts_pwm_78_matrix[4] = { jaspar_phylofacts_pwm_78_matrix_row_0_, jaspar_phylofacts_pwm_78_matrix_row_1_, jaspar_phylofacts_pwm_78_matrix_row_2_, jaspar_phylofacts_pwm_78_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_78_ = {
/* accession        */ "PF0112",
/* name             */ "KTGGYRSGAA",
/* label            */ " PF0112	16.8667694111057	KTGGYRSGAA	Unknown	; MCS \"12.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTGGCGGGAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_78_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.624336,
/* max_score        */ 13.015390,
/* threshold        */ 0.787,
/* info content     */ 16.853048,
/* base_counts      */ {668, 313, 1567, 532},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 308
};

	float jaspar_phylofacts_pwm_79_matrix_row_0_[7] = { -11.473115, -11.473115, -11.473115,  1.272957, -11.473115, -11.473115, -11.473115};
	float jaspar_phylofacts_pwm_79_matrix_row_1_[7] = { -11.231953, -11.231953, -11.231953, -11.231953,  1.514119, -11.231953,  0.641448};
	float jaspar_phylofacts_pwm_79_matrix_row_2_[7] = {  1.514119, -11.231953,  1.514119, -11.231953, -11.231953,  1.514119, -11.231953};
	float jaspar_phylofacts_pwm_79_matrix_row_3_[7] = { -11.473115,  1.272957, -11.473115, -11.473115, -11.473115, -11.473115,  0.731962};
	float *jaspar_phylofacts_pwm_79_matrix[4] = { jaspar_phylofacts_pwm_79_matrix_row_0_, jaspar_phylofacts_pwm_79_matrix_row_1_, jaspar_phylofacts_pwm_79_matrix_row_2_, jaspar_phylofacts_pwm_79_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_79_ = {
/* accession        */ "PF0020",
/* name             */ "GTGACGY",
/* label            */ " PF0020	13.0195695082253	GTGACGY	Unknown	; MCS \"38.4\" ; jaspar \"CREB1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"E4F1\" ; type \"phylogenetic\" ",
/* motif            */ "GTGACGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_79_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.311806,
/* max_score        */ 9.334352,
/* threshold        */ 0.838,
/* info content     */ 13.018419,
/* base_counts      */ {3432, 4866, 10296, 5430},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3432
};

	float jaspar_phylofacts_pwm_80_matrix_row_0_[13] = {  1.272893,  1.272893, -9.353325, -9.353325,  0.850304, -1.974941,  0.846594, -0.194173, -9.353325, -9.353325, -9.353325, -9.353325, -9.353325};
	float jaspar_phylofacts_pwm_80_matrix_row_1_[13] = { -9.112163, -9.112163,  1.514055, -0.379697, -0.395955,  1.367966, -0.112420, -0.088032, -9.112163, -9.112163,  1.514055,  1.514055,  1.435833};
	float jaspar_phylofacts_pwm_80_matrix_row_2_[13] = { -9.112163, -9.112163, -9.112163, -9.112163, -0.793177, -1.562028, -0.700108, -1.072683, -9.112163, -9.112163, -9.112163, -9.112163, -1.072683};
	float jaspar_phylofacts_pwm_80_matrix_row_3_[13] = { -9.353325, -9.353325, -9.353325,  1.109807, -1.059025, -1.703156, -1.914353,  0.565100,  1.272893,  1.272893, -9.353325, -9.353325, -9.353325};
	float *jaspar_phylofacts_pwm_80_matrix[4] = { jaspar_phylofacts_pwm_80_matrix_row_0_, jaspar_phylofacts_pwm_80_matrix_row_1_, jaspar_phylofacts_pwm_80_matrix_row_2_, jaspar_phylofacts_pwm_80_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_80_ = {
/* accession        */ "PF0113",
/* name             */ "AACYNNNNTTCCS",
/* label            */ " PF0113	19.611252601661	AACYNNNNTTCCS	Unknown	; MCS \"12.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AACTACANTTCCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_80_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.200928,
/* max_score        */ 15.809341,
/* threshold        */ 0.779,
/* info content     */ 19.600473,
/* base_counts      */ {1474, 2260, 167, 1455},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 412
};

	float jaspar_phylofacts_pwm_81_matrix_row_0_[8] = {  0.295120,  0.593219,  1.272932, -10.121222, -10.121222, -10.121222, -10.121222, -10.121222};
	float jaspar_phylofacts_pwm_81_matrix_row_1_[8] = { -9.880059, -9.880059, -9.880059, -9.880059, -9.880059, -9.880059, -9.880059, -9.880059};
	float jaspar_phylofacts_pwm_81_matrix_row_2_[8] = {  1.042294,  0.807353, -9.880059,  1.514094, -9.880059, -9.880059,  1.514094, -9.880059};
	float jaspar_phylofacts_pwm_81_matrix_row_3_[8] = { -10.121222, -10.121222, -10.121222, -10.121222,  1.272932,  1.272932, -10.121222,  1.272932};
	float *jaspar_phylofacts_pwm_81_matrix[4] = { jaspar_phylofacts_pwm_81_matrix_row_0_, jaspar_phylofacts_pwm_81_matrix_row_1_, jaspar_phylofacts_pwm_81_matrix_row_2_, jaspar_phylofacts_pwm_81_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_81_ = {
/* accession        */ "PF0129",
/* name             */ "RRAGTTGT",
/* label            */ " PF0129	14.0448717153072	RRAGTTGT	Unknown	; MCS \"11.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GAAGTTGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_81_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.969780,
/* max_score        */ 9.969563,
/* threshold        */ 0.818,
/* info content     */ 14.040485,
/* base_counts      */ {1672, 0, 2768, 2664},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 888
};

	float jaspar_phylofacts_pwm_82_matrix_row_0_[9] = { -9.562765,  1.272907,  1.272907,  1.120071,  0.859546,  1.272907, -9.562765,  1.272907, -9.562765};
	float jaspar_phylofacts_pwm_82_matrix_row_1_[9] = { -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603};
	float jaspar_phylofacts_pwm_82_matrix_row_2_[9] = { -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603, -9.321603,  1.514069};
	float jaspar_phylofacts_pwm_82_matrix_row_3_[9] = {  1.272907, -9.562765, -9.562765, -0.680789,  0.189958, -9.562765,  1.272907, -9.562765, -9.562765};
	float *jaspar_phylofacts_pwm_82_matrix[4] = { jaspar_phylofacts_pwm_82_matrix_row_0_, jaspar_phylofacts_pwm_82_matrix_row_1_, jaspar_phylofacts_pwm_82_matrix_row_2_, jaspar_phylofacts_pwm_82_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_82_ = {
/* accession        */ "PF0054",
/* name             */ "TAAWWATAG",
/* label            */ " PF0054	16.4877864484419	TAAWWATAG	Unknown	; MCS \"21.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"RSRFC4\" ; type \"phylogenetic\" ",
/* motif            */ "TAAAAATAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_82_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.582565,
/* max_score        */ 11.131125,
/* threshold        */ 0.793,
/* info content     */ 16.479401,
/* base_counts      */ {2804, 0, 508, 1260},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 508
};

	float jaspar_phylofacts_pwm_83_matrix_row_0_[12] = {  1.272843, -8.829536,  1.272843,  1.113220,  0.132471,  0.850870, -1.920782, -8.829536, -2.143676, -8.829536, -8.829536, -8.829536};
	float jaspar_phylofacts_pwm_83_matrix_row_1_[12] = { -8.588374,  1.514005, -8.588374, -8.588374, -0.724723, -8.588374, -0.198788, -0.456550, -8.588374,  1.514005, -8.588374, -8.588374};
	float jaspar_phylofacts_pwm_83_matrix_row_2_[12] = { -8.588374, -8.588374, -8.588374, -8.588374,  0.787565,  0.447731, -0.245297,  1.363951,  1.480670, -8.588374,  1.514005,  1.514005};
	float jaspar_phylofacts_pwm_83_matrix_row_3_[12] = { -8.829536, -8.829536, -8.829536, -0.640570, -1.132870, -8.829536,  0.772913, -8.829536, -8.829536, -8.829536, -8.829536, -8.829536};
	float *jaspar_phylofacts_pwm_83_matrix[4] = { jaspar_phylofacts_pwm_83_matrix_row_0_, jaspar_phylofacts_pwm_83_matrix_row_1_, jaspar_phylofacts_pwm_83_matrix_row_2_, jaspar_phylofacts_pwm_83_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_83_ = {
/* accession        */ "PF0103",
/* name             */ "ACAWNRNSRCGG",
/* label            */ " PF0103	18.4778049020768	ACAWNRNSRCGG	Unknown	; MCS \"13.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACAARATGGCGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_83_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.107857,
/* max_score        */ 14.970893,
/* threshold        */ 0.778,
/* info content     */ 18.460297,
/* base_counts      */ {952, 592, 1178, 206},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 244
};

	float jaspar_phylofacts_pwm_84_matrix_row_0_[12] = { -9.100651, -9.100651, -9.100651, -0.368185, -0.525000, -0.018030, -0.368185, -0.831662, -9.100651, -9.100651,  1.272872, -9.100651};
	float jaspar_phylofacts_pwm_84_matrix_row_1_[12] = { -8.859488, -8.859488, -8.859488,  0.893234, -8.859488,  1.192462,  0.851688, -0.540502,  1.263175,  1.514034, -8.859488, -8.859488};
	float jaspar_phylofacts_pwm_84_matrix_row_2_[12] = {  1.514034,  1.514034,  1.514034, -0.362294,  1.332968, -8.859488, -0.265149,  1.063851, -8.859488, -8.859488, -8.859488, -8.859488};
	float jaspar_phylofacts_pwm_84_matrix_row_3_[12] = { -9.100651, -9.100651, -9.100651, -0.884292, -9.100651, -9.100651, -0.831662, -0.911684, -0.232660, -9.100651, -9.100651,  1.272872};
	float *jaspar_phylofacts_pwm_84_matrix[4] = { jaspar_phylofacts_pwm_84_matrix_row_0_, jaspar_phylofacts_pwm_84_matrix_row_1_, jaspar_phylofacts_pwm_84_matrix_row_2_, jaspar_phylofacts_pwm_84_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_84_ = {
/* accession        */ "PF0119",
/* name             */ "GGGNRMNNYCAT",
/* label            */ " PF0119	16.7523948696153	GGGNRMNNYCAT	Unknown	; MCS \"11.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGGCGCCGCCAT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_84_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.533493,
/* max_score        */ 15.199257,
/* threshold        */ 0.779,
/* info content     */ 16.739885,
/* base_counts      */ {624, 1179, 1534, 503},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 320
};

	float jaspar_phylofacts_pwm_85_matrix_row_0_[9] = { -8.995304, -8.995304, -8.995304, -8.995304, -8.995304,  1.272861, -8.995304, -8.995304, -8.995304};
	float jaspar_phylofacts_pwm_85_matrix_row_1_[9] = { -8.754142, -8.754142,  1.514024, -8.754142,  1.514024, -8.754142, -8.754142, -8.754142,  1.335867};
	float jaspar_phylofacts_pwm_85_matrix_row_2_[9] = {  1.514024,  1.514024, -8.754142,  1.396245, -8.754142, -8.754142, -8.754142,  1.514024, -0.298611};
	float jaspar_phylofacts_pwm_85_matrix_row_3_[9] = { -8.995304, -8.995304, -8.995304, -0.924085, -8.995304, -8.995304,  1.272861, -8.995304, -8.995304};
	float *jaspar_phylofacts_pwm_85_matrix[4] = { jaspar_phylofacts_pwm_85_matrix_row_0_, jaspar_phylofacts_pwm_85_matrix_row_1_, jaspar_phylofacts_pwm_85_matrix_row_2_, jaspar_phylofacts_pwm_85_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_85_ = {
/* accession        */ "PF0159",
/* name             */ "GGCKCATGS",
/* label            */ " PF0159	16.8548444626024	GGCKCATGS	Unknown	; MCS \"9.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCGCATGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_85_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.957733,
/* max_score        */ 12.847953,
/* threshold        */ 0.790,
/* info content     */ 16.840679,
/* base_counts      */ {288, 817, 1167, 320},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 288
};

	float jaspar_phylofacts_pwm_86_matrix_row_0_[12] = {  0.345310,  1.272904, -9.522602, -9.522602,  0.647127, -1.333635,  1.096308, -9.522602, -9.522602, -9.522602, -1.483122, -9.522602};
	float jaspar_phylofacts_pwm_86_matrix_row_1_[12] = { -9.281440, -9.281440,  1.514066, -9.281440,  0.175839,  1.269177, -9.281440, -9.281440, -9.281440, -9.281440,  1.315220,  1.514066};
	float jaspar_phylofacts_pwm_86_matrix_row_2_[12] = {  1.010740, -9.281440, -9.281440, -9.281440, -0.938362, -1.343708, -0.306695, -9.281440, -9.281440, -9.281440, -1.842468, -9.281440};
	float jaspar_phylofacts_pwm_86_matrix_row_3_[12] = { -9.522602, -9.522602, -9.522602,  1.272904, -0.874205, -1.179524, -9.522602,  1.272904,  1.272904,  1.272904, -1.228302, -9.522602};
	float *jaspar_phylofacts_pwm_86_matrix[4] = { jaspar_phylofacts_pwm_86_matrix_row_0_, jaspar_phylofacts_pwm_86_matrix_row_1_, jaspar_phylofacts_pwm_86_matrix_row_2_, jaspar_phylofacts_pwm_86_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_86_ = {
/* accession        */ "PF0066",
/* name             */ "RACTNNRTTTNC",
/* label            */ " PF0066	18.6898753297241	RACTNNRTTTNC	Unknown	; MCS \"18.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GACTACATTTCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_86_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.827957,
/* max_score        */ 14.731226,
/* threshold        */ 0.778,
/* info content     */ 18.680798,
/* base_counts      */ {1418, 1886, 461, 2091},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 488
};

	float jaspar_phylofacts_pwm_87_matrix_row_0_[6] = { -12.483224, -12.483224, -12.483224, -12.483224, -12.483224, -12.483224};
	float jaspar_phylofacts_pwm_87_matrix_row_1_[6] = {  1.514125, -12.242062, -12.242062, -12.242062, -12.242062, -12.242062};
	float jaspar_phylofacts_pwm_87_matrix_row_2_[6] = { -12.242062, -12.242062, -12.242062, -12.242062,  1.514125, -12.242062};
	float jaspar_phylofacts_pwm_87_matrix_row_3_[6] = { -12.483224,  1.272962,  1.272962,  1.272962, -12.483224,  1.272962};
	float *jaspar_phylofacts_pwm_87_matrix[4] = { jaspar_phylofacts_pwm_87_matrix_row_0_, jaspar_phylofacts_pwm_87_matrix_row_1_, jaspar_phylofacts_pwm_87_matrix_row_2_, jaspar_phylofacts_pwm_87_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_87_ = {
/* accession        */ "PF0013",
/* name             */ "CTTTGT",
/* label            */ " PF0013	12	CTTTGT	Unknown	; MCS \"46.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"LEF1\" ; type \"phylogenetic\" ",
/* motif            */ "CTTTGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_87_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -74.899345,
/* max_score        */ 8.120099,
/* threshold        */ 0.861,
/* info content     */ 11.999593,
/* base_counts      */ {0, 9424, 9424, 37696},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 9424
};

	float jaspar_phylofacts_pwm_88_matrix_row_0_[11] = { -8.829536, -8.829536,  1.272843,  0.529310,  0.079834, -8.829536, -8.829536, -8.829536, -8.829536, -8.829536,  1.272843};
	float jaspar_phylofacts_pwm_88_matrix_row_1_[11] = { -8.588374, -8.588374, -8.588374, -8.588374,  0.411368, -8.588374, -8.588374,  0.581248,  1.514005,  1.514005, -8.588374};
	float jaspar_phylofacts_pwm_88_matrix_row_2_[11] = {  1.514005,  1.514005, -8.588374,  0.868904, -0.269388, -8.588374,  0.560197, -8.588374, -8.588374, -8.588374, -8.588374};
	float jaspar_phylofacts_pwm_88_matrix_row_3_[11] = { -8.829536, -8.829536, -8.829536, -8.829536, -0.352957,  1.272843,  0.786335,  0.772913, -8.829536, -8.829536, -8.829536};
	float *jaspar_phylofacts_pwm_88_matrix[4] = { jaspar_phylofacts_pwm_88_matrix_row_0_, jaspar_phylofacts_pwm_88_matrix_row_1_, jaspar_phylofacts_pwm_88_matrix_row_2_, jaspar_phylofacts_pwm_88_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_88_ = {
/* accession        */ "PF0163",
/* name             */ "GGARNTKYCCA",
/* label            */ " PF0163	17.12910262009	GGARNTKYCCA	Unknown	; MCS \"9.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGAGNTTTCCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_88_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.648323,
/* max_score        */ 12.714068,
/* threshold        */ 0.782,
/* info content     */ 17.111589,
/* base_counts      */ {678, 665, 751, 590},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 244
};

	float jaspar_phylofacts_pwm_89_matrix_row_0_[8] = { -10.579313,  1.272944, -10.579313, -10.579313, -10.579313, -10.579313, -10.579313, -10.579313};
	float jaspar_phylofacts_pwm_89_matrix_row_1_[8] = {  1.514106, -10.338152, -10.338152, -10.338152, -10.338152, -10.338152,  0.628338,  0.830888};
	float jaspar_phylofacts_pwm_89_matrix_row_2_[8] = { -10.338152, -10.338152, -10.338152, -10.338152,  1.514106, -10.338152, -10.338152, -10.338152};
	float jaspar_phylofacts_pwm_89_matrix_row_3_[8] = { -10.579313, -10.579313,  1.272944,  1.272944, -10.579313,  1.272944,  0.741252,  0.569783};
	float *jaspar_phylofacts_pwm_89_matrix[4] = { jaspar_phylofacts_pwm_89_matrix_row_0_, jaspar_phylofacts_pwm_89_matrix_row_1_, jaspar_phylofacts_pwm_89_matrix_row_2_, jaspar_phylofacts_pwm_89_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_89_ = {
/* accession        */ "PF0049",
/* name             */ "CATTGTYY",
/* label            */ " PF0049	14.0223316971708	CATTGTYY	Unknown	; MCS \"22.5\" ; jaspar \"SOX9\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"SOX-9\" ; type \"phylogenetic\" ",
/* motif            */ "CATTGTTC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_89_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.634514,
/* max_score        */ 9.692131,
/* threshold        */ 0.818,
/* info content     */ 14.019452,
/* base_counts      */ {1404, 2692, 1404, 5732},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1404
};

	float jaspar_phylofacts_pwm_90_matrix_row_0_[8] = { -9.530765,  1.272905, -9.530765,  0.155872,  0.555086,  1.272905, -9.530765, -9.530765};
	float jaspar_phylofacts_pwm_90_matrix_row_1_[8] = {  1.514067, -9.289602, -9.289602, -9.289602, -9.289602, -9.289602, -9.289602,  1.514067};
	float jaspar_phylofacts_pwm_90_matrix_row_2_[8] = { -9.289602, -9.289602, -9.289602,  1.117716,  0.845036, -9.289602,  1.514067, -9.289602};
	float jaspar_phylofacts_pwm_90_matrix_row_3_[8] = { -9.530765, -9.530765,  1.272905, -9.530765, -9.530765, -9.530765, -9.530765, -9.530765};
	float *jaspar_phylofacts_pwm_90_matrix[4] = { jaspar_phylofacts_pwm_90_matrix_row_0_, jaspar_phylofacts_pwm_90_matrix_row_1_, jaspar_phylofacts_pwm_90_matrix_row_2_, jaspar_phylofacts_pwm_90_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_90_ = {
/* accession        */ "PF0134",
/* name             */ "CATRRAGC",
/* label            */ " PF0134	14.0883519500891	CATRRAGC	Unknown	; MCS \"11.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CATGGAGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_90_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.246117,
/* max_score        */ 10.323668,
/* threshold        */ 0.817,
/* info content     */ 14.080808,
/* base_counts      */ {1385, 984, 1075, 492},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 492
};

	float jaspar_phylofacts_pwm_91_matrix_row_0_[13] = { -8.967137,  1.272859, -8.967137, -1.142691,  1.214020, -1.365735, -8.967137,  0.543382, -0.071370, -0.895919, -8.967137,  1.272859, -8.967137};
	float jaspar_phylofacts_pwm_91_matrix_row_1_[13] = {  1.514021, -8.725975, -8.725975,  1.259139, -8.725975,  1.439915,  1.514021, -0.686495,  0.433177, -4.110855, -8.725975, -8.725975,  1.514021};
	float jaspar_phylofacts_pwm_91_matrix_row_2_[13] = { -8.725975, -8.725975,  1.514021, -0.862324, -8.725975, -8.725975, -8.725975, -0.169369, -0.313920,  1.235829,  1.514021, -8.725975, -8.725975};
	float jaspar_phylofacts_pwm_91_matrix_row_3_[13] = { -8.967137, -8.967137, -8.967137, -1.876227, -1.588753, -8.967137, -8.967137, -0.234671, -0.157125, -0.806333, -8.967137, -8.967137, -8.967137};
	float *jaspar_phylofacts_pwm_91_matrix[4] = { jaspar_phylofacts_pwm_91_matrix_row_0_, jaspar_phylofacts_pwm_91_matrix_row_1_, jaspar_phylofacts_pwm_91_matrix_row_2_, jaspar_phylofacts_pwm_91_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_91_ = {
/* accession        */ "PF0143",
/* name             */ "CAGNWMCNNNGAC",
/* label            */ " PF0143	19.3939103130365	CAGNWMCNNNGAC	Unknown	; MCS \"10.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CAGCACCNNGGAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_91_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.450577,
/* max_score        */ 16.241282,
/* threshold        */ 0.778,
/* info content     */ 19.378534,
/* base_counts      */ {1109, 1444, 895, 192},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 280
};

	float jaspar_phylofacts_pwm_92_matrix_row_0_[6] = { -11.145003,  1.272954, -11.145003, -11.145003, -11.145003,  1.272954};
	float jaspar_phylofacts_pwm_92_matrix_row_1_[6] = {  1.514116, -10.903841, -10.903841, -10.903841, -10.903841, -10.903841};
	float jaspar_phylofacts_pwm_92_matrix_row_2_[6] = { -10.903841, -10.903841,  1.514116,  1.514116, -10.903841, -10.903841};
	float jaspar_phylofacts_pwm_92_matrix_row_3_[6] = { -11.145003, -11.145003, -11.145003, -11.145003,  1.272954, -11.145003};
	float *jaspar_phylofacts_pwm_92_matrix[4] = { jaspar_phylofacts_pwm_92_matrix_row_0_, jaspar_phylofacts_pwm_92_matrix_row_1_, jaspar_phylofacts_pwm_92_matrix_row_2_, jaspar_phylofacts_pwm_92_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_92_ = {
/* accession        */ "PF0076",
/* name             */ "CAGGTA",
/* label            */ " PF0076	12	CAGGTA	Unknown	; MCS \"16.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"AREB6\" ; type \"phylogenetic\" ",
/* motif            */ "CAGGTA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_92_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.870018,
/* max_score        */ 8.361207,
/* threshold        */ 0.861,
/* info content     */ 11.998590,
/* base_counts      */ {4944, 2472, 4944, 2472},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2472
};

	float jaspar_phylofacts_pwm_93_matrix_row_0_[6] = { -12.248447, -12.248447, -12.248447, -12.248447, -12.248447, -12.248447};
	float jaspar_phylofacts_pwm_93_matrix_row_1_[6] = { -12.007285, -12.007285, -12.007285, -12.007285, -12.007285, -12.007285};
	float jaspar_phylofacts_pwm_93_matrix_row_2_[6] = { -12.007285, -12.007285,  1.514124, -12.007285, -12.007285, -12.007285};
	float jaspar_phylofacts_pwm_93_matrix_row_3_[6] = {  1.272962,  1.272962, -12.248447,  1.272962,  1.272962,  1.272962};
	float *jaspar_phylofacts_pwm_93_matrix[4] = { jaspar_phylofacts_pwm_93_matrix_row_0_, jaspar_phylofacts_pwm_93_matrix_row_1_, jaspar_phylofacts_pwm_93_matrix_row_2_, jaspar_phylofacts_pwm_93_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_93_ = {
/* accession        */ "PF0060",
/* name             */ "TTGTTT",
/* label            */ " PF0060	12	TTGTTT	Unknown	; MCS \"19.8\" ; jaspar \"FOXF2\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"FOXO4\" ; type \"phylogenetic\" ",
/* motif            */ "TTGTTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_93_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -73.490685,
/* max_score        */ 7.878932,
/* threshold        */ 0.861,
/* info content     */ 11.999495,
/* base_counts      */ {0, 0, 7452, 37260},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 7452
};

	float jaspar_phylofacts_pwm_94_matrix_row_0_[10] = { -10.704976, -1.123003, -10.704976, -10.704976, -10.704976, -10.704976, -10.704976,  1.272947, -2.698275,  1.119111};
	float jaspar_phylofacts_pwm_94_matrix_row_1_[10] = { -10.463814,  1.418611, -10.463814,  1.514109, -10.463814,  1.514109, -10.463814, -10.463814, -1.514709, -10.463814};
	float jaspar_phylofacts_pwm_94_matrix_row_2_[10] = { -10.463814, -10.463814, -10.463814, -10.463814,  1.514109, -10.463814,  1.514109, -10.463814,  1.313021, -0.433650};
	float jaspar_phylofacts_pwm_94_matrix_row_3_[10] = {  1.272947, -10.704976,  1.272947, -10.704976, -10.704976, -10.704976, -10.704976, -10.704976, -0.890265, -10.704976};
	float *jaspar_phylofacts_pwm_94_matrix[4] = { jaspar_phylofacts_pwm_94_matrix_row_0_, jaspar_phylofacts_pwm_94_matrix_row_1_, jaspar_phylofacts_pwm_94_matrix_row_2_, jaspar_phylofacts_pwm_94_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_94_ = {
/* accession        */ "PF0008",
/* name             */ "TMTCGCGANR",
/* label            */ " PF0008	18.0536165315128	TMTCGCGANR	Unknown	; MCS \"55.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TCTCGCGAGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_94_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.043068,
/* max_score        */ 13.726018,
/* threshold        */ 0.781,
/* info content     */ 18.050600,
/* base_counts      */ {3132, 4708, 4713, 3367},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1592
};

	float jaspar_phylofacts_pwm_95_matrix_row_0_[8] = {  0.328271, -11.556915, -11.556915,  1.272958,  1.272958, -11.556915, -11.556915, -11.556915};
	float jaspar_phylofacts_pwm_95_matrix_row_1_[8] = {  1.021791, -11.315753, -11.315753, -11.315753, -11.315753, -11.315753, -11.315753, -11.315753};
	float jaspar_phylofacts_pwm_95_matrix_row_2_[8] = { -11.315753,  1.514120,  1.514120, -11.315753, -11.315753,  1.514120, -11.315753,  1.514120};
	float jaspar_phylofacts_pwm_95_matrix_row_3_[8] = { -11.556915, -11.556915, -11.556915, -11.556915, -11.556915, -11.556915,  1.272958, -11.556915};
	float *jaspar_phylofacts_pwm_95_matrix[4] = { jaspar_phylofacts_pwm_95_matrix_row_0_, jaspar_phylofacts_pwm_95_matrix_row_1_, jaspar_phylofacts_pwm_95_matrix_row_2_, jaspar_phylofacts_pwm_95_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_95_ = {
/* accession        */ "PF0011",
/* name             */ "MGGAAGTG",
/* label            */ " PF0011	15.0359795013411	MGGAAGTG	Unknown	; MCS \"51.6\" ; jaspar \"GABPA\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"GABP\" ; type \"phylogenetic\" ",
/* motif            */ "CGGAAGTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_95_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.455322,
/* max_score        */ 10.897144,
/* threshold        */ 0.809,
/* info content     */ 15.034756,
/* base_counts      */ {8915, 2281, 14928, 3732},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3732
};

	float jaspar_phylofacts_pwm_96_matrix_row_0_[10] = { -9.812310, -9.812310, -9.812310, -0.230338, -9.812310, -9.812310,  1.272920, -9.812310, -0.083117, -0.479664};
	float jaspar_phylofacts_pwm_96_matrix_row_1_[10] = { -9.571149, -9.571149, -9.571149, -9.571149,  1.358399,  1.514082, -9.571149, -9.571149, -9.571149, -9.571149};
	float jaspar_phylofacts_pwm_96_matrix_row_2_[10] = {  1.514082, -9.571149, -9.571149,  1.262553, -9.571149, -9.571149, -9.571149, -9.571149,  1.216127,  1.323756};
	float jaspar_phylofacts_pwm_96_matrix_row_3_[10] = { -9.812310,  1.272920,  1.272920, -9.812310, -0.663739, -9.812310, -9.812310,  1.272920, -9.812310, -9.812310};
	float *jaspar_phylofacts_pwm_96_matrix[4] = { jaspar_phylofacts_pwm_96_matrix_row_0_, jaspar_phylofacts_pwm_96_matrix_row_1_, jaspar_phylofacts_pwm_96_matrix_row_2_, jaspar_phylofacts_pwm_96_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_96_ = {
/* accession        */ "PF0037",
/* name             */ "GTTRYCATRR",
/* label            */ " PF0037	17.1519912622406	GTTRYCATRR	Unknown	; MCS \"25.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTTGCCATGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_96_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.123100,
/* max_score        */ 13.280678,
/* threshold        */ 0.785,
/* info content     */ 17.145050,
/* base_counts      */ {1078, 1210, 2182, 2050},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 652
};

	float jaspar_phylofacts_pwm_97_matrix_row_0_[6] = { -12.101873,  1.272961, -12.101873, -12.101873, -12.101873, -12.101873};
	float jaspar_phylofacts_pwm_97_matrix_row_1_[6] = {  1.514123, -11.860711, -11.860711,  1.514123, -11.860711, -11.860711};
	float jaspar_phylofacts_pwm_97_matrix_row_2_[6] = { -11.860711, -11.860711,  1.514123, -11.860711, -11.860711,  1.514123};
	float jaspar_phylofacts_pwm_97_matrix_row_3_[6] = { -12.101873, -12.101873, -12.101873, -12.101873,  1.272961, -12.101873};
	float *jaspar_phylofacts_pwm_97_matrix[4] = { jaspar_phylofacts_pwm_97_matrix_row_0_, jaspar_phylofacts_pwm_97_matrix_row_1_, jaspar_phylofacts_pwm_97_matrix_row_2_, jaspar_phylofacts_pwm_97_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_97_ = {
/* accession        */ "PF0015",
/* name             */ "CAGCTG",
/* label            */ " PF0015	12	CAGCTG	Unknown	; MCS \"43.9\" ; jaspar \"NHLH1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"AP-4\" ; type \"phylogenetic\" ",
/* motif            */ "CAGCTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_97_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.611244,
/* max_score        */ 8.602414,
/* threshold        */ 0.861,
/* info content     */ 11.999420,
/* base_counts      */ {6436, 12872, 12872, 6436},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 6436
};

	float jaspar_phylofacts_pwm_98_matrix_row_0_[8] = { -11.461391, -11.461391,  1.272957, -11.461391,  1.272957, -11.461391, -0.815943, -11.461391};
	float jaspar_phylofacts_pwm_98_matrix_row_1_[8] = { -11.220229, -11.220229, -11.220229,  1.514119, -11.220229, -11.220229,  0.634874,  0.743141};
	float jaspar_phylofacts_pwm_98_matrix_row_2_[8] = { -11.220229,  1.514119, -11.220229, -11.220229, -11.220229,  1.514119,  0.362232, -11.220229};
	float jaspar_phylofacts_pwm_98_matrix_row_3_[8] = {  1.272957, -11.461391, -11.461391, -11.461391, -11.461391, -11.461391, -0.657722,  0.652023};
	float *jaspar_phylofacts_pwm_98_matrix[4] = { jaspar_phylofacts_pwm_98_matrix_row_0_, jaspar_phylofacts_pwm_98_matrix_row_1_, jaspar_phylofacts_pwm_98_matrix_row_2_, jaspar_phylofacts_pwm_98_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_98_ = {
/* accession        */ "PF0041",
/* name             */ "TGACAGNY",
/* label            */ " PF0041	13.1751339381587	TGACAGNY	Unknown	; MCS \"24.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"MEIS1\" ; type \"phylogenetic\" ",
/* motif            */ "TGACAGNT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_98_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.045685,
/* max_score        */ 9.739243,
/* threshold        */ 0.825,
/* info content     */ 13.173966,
/* base_counts      */ {7204, 6369, 7856, 5707},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3392
};

	float jaspar_phylofacts_pwm_99_matrix_row_0_[13] = { -9.303569, -9.303569, -9.303569, -9.303569, -9.303569, -0.449760, -0.913981,  0.449154, -9.303569, -9.303569,  1.272889,  1.094646,  0.594957};
	float jaspar_phylofacts_pwm_99_matrix_row_1_[13] = {  0.885146, -9.062407, -9.062407,  1.514051,  1.514051, -0.628377,  0.540044,  0.064661, -9.062407, -9.062407, -9.062407, -0.298197, -9.062407};
	float jaspar_phylofacts_pwm_99_matrix_row_2_[13] = { -9.062407, -9.062407, -9.062407, -9.062407, -9.062407,  0.224988,  0.611731, -0.208598,  1.514051,  1.514051, -9.062407, -9.062407,  0.805506};
	float jaspar_phylofacts_pwm_99_matrix_row_3_[13] = {  0.511142,  1.272889,  1.272889, -9.303569, -9.303569,  0.425625, -0.984582, -0.637783, -9.303569, -9.303569, -9.303569, -9.303569, -9.303569};
	float *jaspar_phylofacts_pwm_99_matrix[4] = { jaspar_phylofacts_pwm_99_matrix_row_0_, jaspar_phylofacts_pwm_99_matrix_row_1_, jaspar_phylofacts_pwm_99_matrix_row_2_, jaspar_phylofacts_pwm_99_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_99_ = {
/* accession        */ "PF0150",
/* name             */ "YTTCCNNNGGAMR",
/* label            */ " PF0150	17.9010562177425	YTTCCNNNGGAMR	Unknown	; MCS \"10.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTTCCNSNGGAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_99_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.286423,
/* max_score        */ 14.146680,
/* threshold        */ 0.777,
/* info content     */ 17.889536,
/* base_counts      */ {1205, 1343, 1314, 1234},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 392
};

	float jaspar_phylofacts_pwm_100_matrix_row_0_[8] = { -11.514215, -11.514215, -11.514215, -11.514215, -11.514215,  1.272957, -1.153272, -11.514215};
	float jaspar_phylofacts_pwm_100_matrix_row_1_[8] = { -11.273054, -11.273054,  1.514119, -11.273054,  1.514119, -11.273054,  0.094857, -11.273054};
	float jaspar_phylofacts_pwm_100_matrix_row_2_[8] = { -11.273054,  1.514119, -11.273054,  1.514119, -11.273054, -11.273054,  0.665799,  1.135556};
	float jaspar_phylofacts_pwm_100_matrix_row_3_[8] = {  1.272957, -11.514215, -11.514215, -11.514215, -11.514215, -11.514215, -0.147461,  0.118278};
	float *jaspar_phylofacts_pwm_100_matrix[4] = { jaspar_phylofacts_pwm_100_matrix_row_0_, jaspar_phylofacts_pwm_100_matrix_row_1_, jaspar_phylofacts_pwm_100_matrix_row_2_, jaspar_phylofacts_pwm_100_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_100_ = {
/* accession        */ "PF0022",
/* name             */ "TGCGCANK",
/* label            */ " PF0022	13.2772616543157	TGCGCANK	Unknown	; MCS \"37.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGCGCANG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_100_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.752777,
/* max_score        */ 10.403747,
/* threshold        */ 0.824,
/* info content     */ 13.276149,
/* base_counts      */ {3892, 8017, 11132, 5567},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3576
};

	float jaspar_phylofacts_pwm_101_matrix_row_0_[12] = { -9.229468, -9.229468, -9.229468, -9.229468, -0.361477, -1.532801, -9.229468, -9.229468, -9.229468, -9.229468,  1.272883,  1.272883};
	float jaspar_phylofacts_pwm_101_matrix_row_1_[12] = {  1.514045, -8.988306, -8.988306,  0.704522,  0.815416,  0.775977,  1.514045, -8.988306, -1.897396, -8.988306, -8.988306, -8.988306};
	float jaspar_phylofacts_pwm_101_matrix_row_2_[12] = { -8.988306, -8.988306,  1.514045, -8.988306, -0.255840, -0.412655, -8.988306, -8.988306, -8.988306, -8.988306, -8.988306, -8.988306};
	float jaspar_phylofacts_pwm_101_matrix_row_3_[12] = { -9.229468,  1.272883, -9.229468,  0.684019, -0.712075,  0.120721, -9.229468,  1.272883,  1.239362,  1.272883, -9.229468, -9.229468};
	float *jaspar_phylofacts_pwm_101_matrix[4] = { jaspar_phylofacts_pwm_101_matrix_row_0_, jaspar_phylofacts_pwm_101_matrix_row_1_, jaspar_phylofacts_pwm_101_matrix_row_2_, jaspar_phylofacts_pwm_101_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_101_ = {
/* accession        */ "PF0082",
/* name             */ "CTGYNNCTYTAA",
/* label            */ " PF0082	19.3265187891226	CTGYNNCTYTAA	Unknown	; MCS \"15.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTGTNYCTTTAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_101_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.539566,
/* max_score        */ 14.441830,
/* threshold        */ 0.777,
/* info content     */ 19.313574,
/* base_counts      */ {821, 1257, 479, 1811},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 364
};

	float jaspar_phylofacts_pwm_102_matrix_row_0_[13] = {  0.174330, -9.400723,  1.272896,  1.272896, -9.400723, -0.241570,  0.072059, -0.966694,  0.048713,  0.008550, -9.400723, -9.400723, -9.400723};
	float jaspar_phylofacts_pwm_102_matrix_row_1_[13] = {  1.108605,  1.514058, -9.159561, -9.159561, -9.159561,  0.469556,  0.127833,  0.164198,  0.520846, -0.088367, -9.159561,  1.514058, -9.159561};
	float jaspar_phylofacts_pwm_102_matrix_row_2_[13] = { -9.159561, -9.159561, -9.159561, -9.159561, -9.159561,  0.593162,  0.587332,  0.926290,  0.155230,  0.649671,  1.514058, -9.159561,  1.514058};
	float jaspar_phylofacts_pwm_102_matrix_row_3_[13] = { -9.400723, -9.400723, -9.400723, -9.400723,  1.272896, -2.229834, -1.659624, -1.268898, -1.268898, -1.081737, -9.400723, -9.400723, -9.400723};
	float *jaspar_phylofacts_pwm_102_matrix[4] = { jaspar_phylofacts_pwm_102_matrix_row_0_, jaspar_phylofacts_pwm_102_matrix_row_1_, jaspar_phylofacts_pwm_102_matrix_row_2_, jaspar_phylofacts_pwm_102_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_102_ = {
/* accession        */ "PF0110",
/* name             */ "MCAATNNNNNGCG",
/* label            */ " PF0110	16.3343490971089	MCAATNNNNNGCG	Unknown	; MCS \"12.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCAATSNGNNGCG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_102_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.714767,
/* max_score        */ 14.260827,
/* threshold        */ 0.775,
/* info content     */ 16.325148,
/* base_counts      */ {1528, 1771, 1740, 577},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 432
};

	float jaspar_phylofacts_pwm_103_matrix_row_0_[8] = { -10.041557, -10.041557,  1.272929, -10.041557,  1.272929, -10.041557, -10.041557, -10.041557};
	float jaspar_phylofacts_pwm_103_matrix_row_1_[8] = {  0.547810, -9.800396, -9.800396,  1.514091, -9.800396, -9.800396, -9.800396, -9.800396};
	float jaspar_phylofacts_pwm_103_matrix_row_2_[8] = { -9.800396,  0.806214, -9.800396, -9.800396, -9.800396, -9.800396, -9.800396, -9.800396};
	float jaspar_phylofacts_pwm_103_matrix_row_3_[8] = {  0.794114,  0.594322, -10.041557, -10.041557, -10.041557,  1.272929,  1.272929,  1.272929};
	float *jaspar_phylofacts_pwm_103_matrix[4] = { jaspar_phylofacts_pwm_103_matrix_row_0_, jaspar_phylofacts_pwm_103_matrix_row_1_, jaspar_phylofacts_pwm_103_matrix_row_2_, jaspar_phylofacts_pwm_103_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_103_ = {
/* accession        */ "PF0145",
/* name             */ "YKACATTT",
/* label            */ " PF0145	14.0417686728717	YKACATTT	Unknown	; MCS \"10.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTACATTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_103_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.332458,
/* max_score        */ 9.479065,
/* threshold        */ 0.818,
/* info content     */ 14.037046,
/* base_counts      */ {1640, 1132, 404, 3384},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 820
};

	float jaspar_phylofacts_pwm_104_matrix_row_0_[15] = {  0.404185, -1.055107, -8.796206, -8.796206, -8.796206, -0.501906, -8.796206, -0.183521,  0.071785, -0.579847, -8.796206, -8.796206, -8.796206, -0.858474, -8.796206};
	float jaspar_phylofacts_pwm_104_matrix_row_1_[15] = { -8.555044,  0.298765,  1.514001, -8.555044, -8.555044,  0.432278,  1.054140, -0.338685, -8.555044,  0.394062,  1.514001, -8.555044, -8.555044,  0.326932,  1.125363};
	float jaspar_phylofacts_pwm_104_matrix_row_2_[15] = {  0.970180,  0.593528, -8.555044, -8.555044,  1.514001, -0.691392, -8.555044,  0.001562,  1.156132,  0.254968, -8.555044, -8.555044,  1.514001,  0.456968, -8.555044};
	float jaspar_phylofacts_pwm_104_matrix_row_3_[15] = { -8.796206, -0.299011, -8.796206,  1.272839, -8.796206,  0.308885,  0.274987,  0.330862, -8.796206, -0.183521, -8.796206,  1.272839, -8.796206, -0.201866,  0.139829};
	float *jaspar_phylofacts_pwm_104_matrix[4] = { jaspar_phylofacts_pwm_104_matrix_row_0_, jaspar_phylofacts_pwm_104_matrix_row_1_, jaspar_phylofacts_pwm_104_matrix_row_2_, jaspar_phylofacts_pwm_104_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_104_ = {
/* accession        */ "PF0154",
/* name             */ "RNCTGNYNRNCTGNY",
/* label            */ " PF0154	16.8135960068403	RNCTGNYNRNCTGNY	Unknown	; MCS \"10.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GNCTGNCNGNCTGNC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 15,
/* matrixname       */ jaspar_phylofacts_pwm_104_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.485550,
/* max_score        */ 15.115191,
/* threshold        */ 0.768,
/* info content     */ 16.796083,
/* base_counts      */ {353, 1117, 1095, 975},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 236
};

	float jaspar_phylofacts_pwm_105_matrix_row_0_[13] = { -9.036120, -9.036120, -9.036120,  0.011819, -0.847154, -0.518727, -9.036120,  0.826597, -9.036120, -1.063310, -9.036120, -9.036120, -9.036120};
	float jaspar_phylofacts_pwm_105_matrix_row_1_[13] = { -8.794958, -8.794958,  1.514028,  0.415482, -0.164258,  1.331713,  1.239601,  0.492436,  0.100808,  0.510783, -8.794958, -8.794958, -8.794958};
	float jaspar_phylofacts_pwm_105_matrix_row_2_[13] = {  1.514028,  1.514028, -8.794958,  0.405433,  1.046707, -8.794958,  0.087017, -8.794958, -8.794958,  0.229173, -8.794958, -8.794958,  1.514028};
	float jaspar_phylofacts_pwm_105_matrix_row_3_[13] = { -9.036120, -9.036120, -9.036120, -1.657737, -1.434718, -9.036120, -9.036120, -9.036120,  0.994044, -0.074113,  1.272866,  1.272866, -9.036120};
	float *jaspar_phylofacts_pwm_105_matrix[4] = { jaspar_phylofacts_pwm_105_matrix_row_0_, jaspar_phylofacts_pwm_105_matrix_row_1_, jaspar_phylofacts_pwm_105_matrix_row_2_, jaspar_phylofacts_pwm_105_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_105_ = {
/* accession        */ "PF0123",
/* name             */ "GGCNNMSMYNTTG",
/* label            */ " PF0123	17.6377292086284	GGCNNMSMYNTTG	Unknown	; MCS \"11.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCNGCCATNTTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_105_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.516960,
/* max_score        */ 14.966770,
/* threshold        */ 0.776,
/* info content     */ 17.623447,
/* base_counts      */ {392, 1225, 1342, 941},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 300
};

	float jaspar_phylofacts_pwm_106_matrix_row_0_[8] = { -9.854356, -9.854356,  1.272922, -9.854356,  0.677767, -9.854356,  1.272922,  1.272922};
	float jaspar_phylofacts_pwm_106_matrix_row_1_[8] = { -9.613194, -9.613194, -9.613194,  0.648003, -9.613194, -9.613194, -9.613194, -9.613194};
	float jaspar_phylofacts_pwm_106_matrix_row_2_[8] = { -9.613194, -9.613194, -9.613194, -9.613194,  0.712321, -9.613194, -9.613194, -9.613194};
	float jaspar_phylofacts_pwm_106_matrix_row_3_[8] = {  1.272922,  1.272922, -9.854356,  0.727190, -9.854356,  1.272922, -9.854356, -9.854356};
	float *jaspar_phylofacts_pwm_106_matrix[4] = { jaspar_phylofacts_pwm_106_matrix_row_0_, jaspar_phylofacts_pwm_106_matrix_row_1_, jaspar_phylofacts_pwm_106_matrix_row_2_, jaspar_phylofacts_pwm_106_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_106_ = {
/* accession        */ "PF0026",
/* name             */ "TTAYRTAA",
/* label            */ " PF0026	14.0259308050782	TTAYRTAA	Unknown	; MCS \"32.6\" ; jaspar \"NFIL3\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"E4BP4\" ; type \"phylogenetic\" ",
/* motif            */ "TTATATAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_106_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.834846,
/* max_score        */ 9.077042,
/* threshold        */ 0.818,
/* info content     */ 14.020329,
/* base_counts      */ {2415, 286, 305, 2434},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 680
};

	float jaspar_phylofacts_pwm_107_matrix_row_0_[6] = { -12.128852, -12.128852, -12.128852,  1.272961,  1.272961,  1.272961};
	float jaspar_phylofacts_pwm_107_matrix_row_1_[6] = { -11.887690, -11.887690, -11.887690, -11.887690, -11.887690, -11.887690};
	float jaspar_phylofacts_pwm_107_matrix_row_2_[6] = { -11.887690,  1.514123,  1.514123, -11.887690, -11.887690, -11.887690};
	float jaspar_phylofacts_pwm_107_matrix_row_3_[6] = {  1.272961, -12.128852, -12.128852, -12.128852, -12.128852, -12.128852};
	float *jaspar_phylofacts_pwm_107_matrix[4] = { jaspar_phylofacts_pwm_107_matrix_row_0_, jaspar_phylofacts_pwm_107_matrix_row_1_, jaspar_phylofacts_pwm_107_matrix_row_2_, jaspar_phylofacts_pwm_107_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_107_ = {
/* accession        */ "PF0055",
/* name             */ "TGGAAA",
/* label            */ " PF0055	12	TGGAAA	Unknown	; MCS \"21.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"NF-AT\" ; type \"phylogenetic\" ",
/* motif            */ "TGGAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_107_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.773109,
/* max_score        */ 8.120090,
/* threshold        */ 0.861,
/* info content     */ 11.999434,
/* base_counts      */ {19836, 0, 13224, 6612},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 6612
};

	float jaspar_phylofacts_pwm_108_matrix_row_0_[9] = { -10.079838, -10.079838, -10.079838, -10.079838, -10.079838, -10.079838,  1.272931,  1.272931,  1.272931};
	float jaspar_phylofacts_pwm_108_matrix_row_1_[9] = {  0.155612, -9.838675,  1.514093, -9.838675,  0.456888,  0.200260, -9.838675, -9.838675, -9.838675};
	float jaspar_phylofacts_pwm_108_matrix_row_2_[9] = {  1.216981,  1.514093, -9.838675,  1.514093,  1.087281,  1.201057, -9.838675, -9.838675, -9.838675};
	float jaspar_phylofacts_pwm_108_matrix_row_3_[9] = { -10.079838, -10.079838, -10.079838, -10.079838, -10.079838, -10.079838, -10.079838, -10.079838, -10.079838};
	float *jaspar_phylofacts_pwm_108_matrix[4] = { jaspar_phylofacts_pwm_108_matrix_row_0_, jaspar_phylofacts_pwm_108_matrix_row_1_, jaspar_phylofacts_pwm_108_matrix_row_2_, jaspar_phylofacts_pwm_108_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_108_ = {
/* accession        */ "PF0069",
/* name             */ "SGCGSSAAA",
/* label            */ " PF0069	15.4063013735119	SGCGSSAAA	Unknown	; MCS \"17.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"E2F-1/DP-2\" ; type \"phylogenetic\" ",
/* motif            */ "GGCGGGAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_108_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.718536,
/* max_score        */ 11.866385,
/* threshold        */ 0.800,
/* info content     */ 15.401323,
/* base_counts      */ {2556, 1596, 3516, 0},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 852
};

	float jaspar_phylofacts_pwm_109_matrix_row_0_[12] = { -9.049364, -9.049364, -9.049364, -9.049364, -9.049364,  0.893393, -1.610392, -0.001425,  1.272867, -9.049364, -9.049364, -9.049364};
	float jaspar_phylofacts_pwm_109_matrix_row_1_[12] = {  1.011958, -8.808202,  1.514029,  1.514029,  1.514029, -8.808202, -0.944550,  0.827472, -8.808202, -8.808202, -8.808202,  1.402807};
	float jaspar_phylofacts_pwm_109_matrix_row_2_[12] = { -8.808202, -8.808202, -8.808202, -8.808202, -8.808202,  0.361421,  1.310438, -0.177501, -8.808202,  1.514029,  1.514029, -8.808202};
	float jaspar_phylofacts_pwm_109_matrix_row_3_[12] = {  0.343381,  1.272867, -9.049364, -9.049364, -9.049364, -9.049364, -1.878475, -2.140609, -9.049364, -9.049364, -9.049364, -0.978145};
	float *jaspar_phylofacts_pwm_109_matrix[4] = { jaspar_phylofacts_pwm_109_matrix_row_0_, jaspar_phylofacts_pwm_109_matrix_row_1_, jaspar_phylofacts_pwm_109_matrix_row_2_, jaspar_phylofacts_pwm_109_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_109_ = {
/* accession        */ "PF0114",
/* name             */ "YTCCCRNNAGGY",
/* label            */ " PF0114	19.0526468210145	YTCCCRNNAGGY	Unknown	; MCS \"12.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTCCCAGCAGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_109_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.512718,
/* max_score        */ 15.561948,
/* threshold        */ 0.777,
/* info content     */ 19.037792,
/* base_counts      */ {614, 1547, 1008, 479},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 304
};

	float jaspar_phylofacts_pwm_110_matrix_row_0_[9] = { -9.125340, -9.125340, -9.125340, -9.125340, -9.125340, -9.125340, -9.125340, -9.125340,  0.399884};
	float jaspar_phylofacts_pwm_110_matrix_row_1_[9] = { -0.135714, -8.884178, -8.884178,  1.514036,  1.514036, -8.884178, -8.884178, -8.884178, -8.884178};
	float jaspar_phylofacts_pwm_110_matrix_row_2_[9] = { -8.884178,  1.514036, -8.884178, -8.884178, -8.884178, -8.884178, -8.884178,  1.514036,  0.973318};
	float jaspar_phylofacts_pwm_110_matrix_row_3_[9] = {  1.059598, -9.125340,  1.272874, -9.125340, -9.125340,  1.272874,  1.272874, -9.125340, -9.125340};
	float *jaspar_phylofacts_pwm_110_matrix[4] = { jaspar_phylofacts_pwm_110_matrix_row_0_, jaspar_phylofacts_pwm_110_matrix_row_1_, jaspar_phylofacts_pwm_110_matrix_row_2_, jaspar_phylofacts_pwm_110_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_110_ = {
/* accession        */ "PF0109",
/* name             */ "YGTCCTTGR",
/* label            */ " PF0109	16.3138527051093	YGTCCTTGR	Unknown	; MCS \"12.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGTCCTTGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_110_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.128067,
/* max_score        */ 11.907683,
/* threshold        */ 0.794,
/* info content     */ 16.301382,
/* base_counts      */ {137, 719, 847, 1249},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 328
};

	float jaspar_phylofacts_pwm_111_matrix_row_0_[9] = { -10.544529, -10.544529,  1.272943,  0.465887,  0.858593,  0.841711,  1.272943, -10.544529,  1.272943};
	float jaspar_phylofacts_pwm_111_matrix_row_1_[9] = {  1.514106, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367};
	float jaspar_phylofacts_pwm_111_matrix_row_2_[9] = { -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367, -10.303367};
	float jaspar_phylofacts_pwm_111_matrix_row_3_[9] = { -10.544529,  1.272943, -10.544529,  0.682061,  0.191890,  0.223978, -10.544529,  1.272943, -10.544529};
	float *jaspar_phylofacts_pwm_111_matrix[4] = { jaspar_phylofacts_pwm_111_matrix_row_0_, jaspar_phylofacts_pwm_111_matrix_row_1_, jaspar_phylofacts_pwm_111_matrix_row_2_, jaspar_phylofacts_pwm_111_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_111_ = {
/* accession        */ "PF0028",
/* name             */ "CTAWWWATA",
/* label            */ " PF0028	15.1499645212671	CTAWWWATA	Unknown	; MCS \"32.3\" ; jaspar \"MEF2A\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"RSRFC4\" ; type \"phylogenetic\" ",
/* motif            */ "CTATAAATA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_111_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.177277,
/* max_score        */ 10.261189,
/* threshold        */ 0.801,
/* info content     */ 15.146729,
/* base_counts      */ {6450, 1356, 0, 4398},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1356
};

	float jaspar_phylofacts_pwm_112_matrix_row_0_[14] = { -9.113071,  1.272873,  1.272873, -9.113071, -0.348862,  0.810268,  0.419425,  0.165021,  0.024806, -9.113071,  0.077168,  1.272873, -9.113071, -9.113071};
	float jaspar_phylofacts_pwm_112_matrix_row_1_[14] = {  0.585369, -8.871909, -8.871909, -8.871909, -0.899099, -8.871909, -0.354516, -0.354516, -0.711106,  0.478280,  0.318330, -8.871909, -8.871909, -8.871909};
	float jaspar_phylofacts_pwm_112_matrix_row_2_[14] = { -8.871909, -8.871909, -8.871909, -8.871909,  0.348480,  0.520836, -0.241209, -0.018101,  0.077196, -8.871909, -0.528832, -8.871909, -8.871909, -8.871909};
	float jaspar_phylofacts_pwm_112_matrix_row_3_[14] = {  0.770264, -9.113071, -9.113071,  1.272873,  0.359710, -9.113071, -0.125750,  0.066913,  0.271306,  0.834481, -0.053438, -9.113071,  1.272873,  1.272873};
	float *jaspar_phylofacts_pwm_112_matrix[4] = { jaspar_phylofacts_pwm_112_matrix_row_0_, jaspar_phylofacts_pwm_112_matrix_row_1_, jaspar_phylofacts_pwm_112_matrix_row_2_, jaspar_phylofacts_pwm_112_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_112_ = {
/* accession        */ "PF0090",
/* name             */ "YAATNRNNNYNATT",
/* label            */ " PF0090	15.6763883525985	YAATNRNNNYNATT	Unknown	; MCS \"14.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"CART-1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "TAATNANNNTNATT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_112_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.865707,
/* max_score        */ 11.586043,
/* threshold        */ 0.768,
/* info content     */ 15.664239,
/* base_counts      */ {1676, 505, 466, 1889},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 324
};

	float jaspar_phylofacts_pwm_113_matrix_row_0_[8] = { -12.653732, -12.653732, -12.653732,  1.272963, -12.653732, -12.653732,  0.173995, -0.214562};
	float jaspar_phylofacts_pwm_113_matrix_row_1_[8] = { -12.412570, -12.412570, -12.412570, -12.412570, -12.412570, -12.412570, -12.412570, -12.412570};
	float jaspar_phylofacts_pwm_113_matrix_row_2_[8] = {  1.514125,  1.514125,  1.514125, -12.412570,  1.514125,  1.514125,  1.108839,  1.258032};
	float jaspar_phylofacts_pwm_113_matrix_row_3_[8] = { -12.653732, -12.653732, -12.653732, -12.653732, -12.653732, -12.653732, -12.653732, -12.653732};
	float *jaspar_phylofacts_pwm_113_matrix[4] = { jaspar_phylofacts_pwm_113_matrix_row_0_, jaspar_phylofacts_pwm_113_matrix_row_1_, jaspar_phylofacts_pwm_113_matrix_row_2_, jaspar_phylofacts_pwm_113_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_113_ = {
/* accession        */ "PF0024",
/* name             */ "GGGAGGRR",
/* label            */ " PF0024	14.3109738838275	GGGAGGRR	Unknown	; MCS \"33.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"MAZ\" ; type \"phylogenetic\" ",
/* motif            */ "GGGAGGGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_113_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.229858,
/* max_score        */ 11.210460,
/* threshold        */ 0.816,
/* info content     */ 14.310552,
/* base_counts      */ {17425, 0, 71983, 0},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 11176
};

	float jaspar_phylofacts_pwm_114_matrix_row_0_[8] = { -10.079838, -10.079838, -10.079838,  1.272931, -10.079838, -10.079838, -10.079838, -0.327115};
	float jaspar_phylofacts_pwm_114_matrix_row_1_[8] = { -9.838675,  1.514093,  1.514093, -9.838675, -9.838675, -9.838675, -9.838675, -9.838675};
	float jaspar_phylofacts_pwm_114_matrix_row_2_[8] = { -9.838675, -9.838675, -9.838675, -9.838675, -9.838675, -9.838675,  0.618726, -9.838675};
	float jaspar_phylofacts_pwm_114_matrix_row_3_[8] = {  1.272931, -10.079838, -10.079838, -10.079838,  1.272931,  1.272931,  0.747928,  1.047440};
	float *jaspar_phylofacts_pwm_114_matrix[4] = { jaspar_phylofacts_pwm_114_matrix_row_0_, jaspar_phylofacts_pwm_114_matrix_row_1_, jaspar_phylofacts_pwm_114_matrix_row_2_, jaspar_phylofacts_pwm_114_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_114_ = {
/* accession        */ "PF0132",
/* name             */ "TCCATTKW",
/* label            */ " PF0132	14.298652112109	TCCATTKW	Unknown	; MCS \"11.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TCCATTTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_114_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.397545,
/* max_score        */ 9.915276,
/* threshold        */ 0.816,
/* info content     */ 14.294077,
/* base_counts      */ {1024, 1704, 348, 3740},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 852
};

	float jaspar_phylofacts_pwm_115_matrix_row_0_[7] = { -13.160324, -13.160324, -13.160324, -13.160324, -13.160324, -13.160324, -0.224529};
	float jaspar_phylofacts_pwm_115_matrix_row_1_[7] = { -12.919162, -12.919162, -12.919162,  1.514126, -12.919162, -12.919162, -12.919162};
	float jaspar_phylofacts_pwm_115_matrix_row_2_[7] = {  1.514126,  1.514126,  1.514126, -12.919162,  1.514126,  1.514126,  1.260923};
	float jaspar_phylofacts_pwm_115_matrix_row_3_[7] = { -13.160324, -13.160324, -13.160324, -13.160324, -13.160324, -13.160324, -13.160324};
	float *jaspar_phylofacts_pwm_115_matrix[4] = { jaspar_phylofacts_pwm_115_matrix_row_0_, jaspar_phylofacts_pwm_115_matrix_row_1_, jaspar_phylofacts_pwm_115_matrix_row_2_, jaspar_phylofacts_pwm_115_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_115_ = {
/* accession        */ "PF0006",
/* name             */ "GGGCGGR",
/* label            */ " PF0006	13.2331518804852	GGGCGGR	Unknown	; MCS \"63.9\" ; jaspar \"SP1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"SP1\" ; type \"phylogenetic\" ",
/* motif            */ "GGGCGGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_115_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.122269,
/* max_score        */ 10.345680,
/* threshold        */ 0.835,
/* info content     */ 13.232913,
/* base_counts      */ {4149, 18548, 107139, 0},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 18548
};

	float jaspar_phylofacts_pwm_116_matrix_row_0_[7] = { -10.761155, -10.761155, -10.761155, -10.761155, -10.761155,  1.272948, -10.761155};
	float jaspar_phylofacts_pwm_116_matrix_row_1_[7] = { -10.519994, -10.519994,  1.514110, -10.519994, -10.519994, -10.519994,  0.990940};
	float jaspar_phylofacts_pwm_116_matrix_row_2_[7] = { -10.519994,  1.514110, -10.519994, -10.519994,  1.514110, -10.519994, -10.519994};
	float jaspar_phylofacts_pwm_116_matrix_row_3_[7] = {  1.272948, -10.761155, -10.761155,  1.272948, -10.761155, -10.761155,  0.374907};
	float *jaspar_phylofacts_pwm_116_matrix[4] = { jaspar_phylofacts_pwm_116_matrix_row_0_, jaspar_phylofacts_pwm_116_matrix_row_1_, jaspar_phylofacts_pwm_116_matrix_row_2_, jaspar_phylofacts_pwm_116_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_116_ = {
/* accession        */ "PF0092",
/* name             */ "TGCTGAY",
/* label            */ " PF0092	13.0249047188108	TGCTGAY	Unknown	; MCS \"14.0\" ; jaspar \"MafB\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGCTGAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_116_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.328087,
/* max_score        */ 9.352114,
/* threshold        */ 0.838,
/* info content     */ 13.022683,
/* base_counts      */ {1684, 2682, 3368, 4054},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1684
};

	float jaspar_phylofacts_pwm_117_matrix_row_0_[9] = {  1.272879,  0.678196, -9.184522, -9.184522, -9.184522, -9.184522, -9.184522,  1.272879,  1.272879};
	float jaspar_phylofacts_pwm_117_matrix_row_1_[9] = { -8.943360, -8.943360, -8.943360, -8.943360, -8.943360, -8.943360, -8.943360, -8.943360, -8.943360};
	float jaspar_phylofacts_pwm_117_matrix_row_2_[9] = { -8.943360,  0.711730,  1.514042,  1.514042,  1.514042, -8.943360, -8.943360, -8.943360, -8.943360};
	float jaspar_phylofacts_pwm_117_matrix_row_3_[9] = { -9.184522, -9.184522, -9.184522, -9.184522, -9.184522,  1.272879,  1.272879, -9.184522, -9.184522};
	float *jaspar_phylofacts_pwm_117_matrix[4] = { jaspar_phylofacts_pwm_117_matrix_row_0_, jaspar_phylofacts_pwm_117_matrix_row_1_, jaspar_phylofacts_pwm_117_matrix_row_2_, jaspar_phylofacts_pwm_117_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_117_ = {
/* accession        */ "PF0065",
/* name             */ "ARGGGTTAA",
/* label            */ " PF0065	17.0077333612805	ARGGGTTAA	Unknown	; MCS \"18.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"FXR(*)\" ; type \"phylogenetic\" ",
/* motif            */ "AAGGGTTAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_117_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.660698,
/* max_score        */ 11.618253,
/* threshold        */ 0.789,
/* info content     */ 16.995441,
/* base_counts      */ {1236, 0, 1200, 696},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 348
};

	float jaspar_phylofacts_pwm_118_matrix_row_0_[9] = { -9.293313, -9.293313,  1.272888,  1.272888, -9.293313,  0.132219, -9.293313, -9.293313, -9.293313};
	float jaspar_phylofacts_pwm_118_matrix_row_1_[9] = {  0.773429, -9.052151, -9.052151, -9.052151,  0.652947, -1.881262, -9.052151,  1.514050, -9.052151};
	float jaspar_phylofacts_pwm_118_matrix_row_2_[9] = { -9.052151, -9.052151, -9.052151, -9.052151, -9.052151,  0.450934,  1.514050, -9.052151, -9.052151};
	float jaspar_phylofacts_pwm_118_matrix_row_3_[9] = {  0.625112,  1.272888, -9.293313, -9.293313,  0.723548,  0.074117, -9.293313, -9.293313,  1.272888};
	float *jaspar_phylofacts_pwm_118_matrix[4] = { jaspar_phylofacts_pwm_118_matrix_row_0_, jaspar_phylofacts_pwm_118_matrix_row_1_, jaspar_phylofacts_pwm_118_matrix_row_2_, jaspar_phylofacts_pwm_118_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_118_ = {
/* accession        */ "PF0168",
/* name             */ "YTAAYNGCT",
/* label            */ " PF0168	14.2775023889509	YTAAYNGCT	Unknown	; MCS \"9.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTAATNGCT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_118_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.227768,
/* max_score        */ 10.067565,
/* threshold        */ 0.807,
/* info content     */ 14.268067,
/* base_counts      */ {900, 750, 522, 1320},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 388
};

	float jaspar_phylofacts_pwm_119_matrix_row_0_[9] = { -11.247932, -11.247932,  1.272955, -1.489413, -0.388914, -11.247932, -11.247932,  1.272955, -11.247932};
	float jaspar_phylofacts_pwm_119_matrix_row_1_[9] = { -11.006770,  1.514117, -11.006770,  0.879104,  0.093681, -11.006770, -11.006770, -11.006770,  1.191779};
	float jaspar_phylofacts_pwm_119_matrix_row_2_[9] = { -11.006770, -11.006770, -11.006770,  0.130749,  0.883228, -11.006770,  1.514117, -11.006770, -11.006770};
	float jaspar_phylofacts_pwm_119_matrix_row_3_[9] = {  1.272955, -11.247932, -11.247932, -0.583616, -2.037492,  1.272955, -11.247932, -11.247932, -0.016031};
	float *jaspar_phylofacts_pwm_119_matrix[4] = { jaspar_phylofacts_pwm_119_matrix_row_0_, jaspar_phylofacts_pwm_119_matrix_row_1_, jaspar_phylofacts_pwm_119_matrix_row_2_, jaspar_phylofacts_pwm_119_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_119_ = {
/* accession        */ "PF0018",
/* name             */ "TCANNTGAY",
/* label            */ " PF0018	13.8860076750062	TCANNTGAY	Unknown	; MCS \"40.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"SREBP-1\" ; type \"phylogenetic\" ",
/* motif            */ "TCACGTGAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_119_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.262428,
/* max_score        */ 11.074164,
/* threshold        */ 0.810,
/* info content     */ 13.884563,
/* base_counts      */ {6173, 6839, 4885, 6763},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2740
};

	float jaspar_phylofacts_pwm_120_matrix_row_0_[9] = { -10.985719, -10.985719,  1.272951, -10.985719, -10.985719, -10.985719, -10.985719, -0.186122,  0.816146};
	float jaspar_phylofacts_pwm_120_matrix_row_1_[9] = {  0.633706, -10.744556, -10.744556, -10.744556, -10.744556, -10.744556, -10.744556, -1.051728, -10.744556};
	float jaspar_phylofacts_pwm_120_matrix_row_2_[9] = { -10.744556, -10.744556, -10.744556, -10.744556, -10.744556, -10.744556, -10.744556,  0.011238,  0.510906};
	float jaspar_phylofacts_pwm_120_matrix_row_3_[9] = {  0.737476,  1.272951, -10.985719,  1.272951,  1.272951,  1.272951,  1.272951,  0.514132, -10.985719};
	float *jaspar_phylofacts_pwm_120_matrix[4] = { jaspar_phylofacts_pwm_120_matrix_row_0_, jaspar_phylofacts_pwm_120_matrix_row_1_, jaspar_phylofacts_pwm_120_matrix_row_2_, jaspar_phylofacts_pwm_120_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_120_ = {
/* accession        */ "PF0033",
/* name             */ "YTATTTTNR",
/* label            */ " PF0033	14.3042683633816	YTATTTTNR	Unknown	; MCS \"26.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"MEF-2\" ; type \"phylogenetic\" ",
/* motif            */ "TTATTTTNA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_120_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.937477,
/* max_score        */ 9.705462,
/* threshold        */ 0.807,
/* info content     */ 14.302279,
/* base_counts      */ {3933, 1036, 1242, 12761},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2108
};

	float jaspar_phylofacts_pwm_121_matrix_row_0_[9] = { -10.338936, -10.338936, -10.338936, -1.024145,  0.788342, -10.338936, -10.338936, -10.338936, -10.338936};
	float jaspar_phylofacts_pwm_121_matrix_row_1_[9] = { -10.097774,  1.514101, -10.097774,  0.306519, -10.097774, -10.097774, -10.097774, -10.097774, -10.097774};
	float jaspar_phylofacts_pwm_121_matrix_row_2_[9] = {  1.514101, -10.097774, -10.097774,  0.885839, -10.097774, -10.097774, -10.097774,  1.514101,  1.072675};
	float jaspar_phylofacts_pwm_121_matrix_row_3_[9] = { -10.338936, -10.338936,  1.272938, -1.429566,  0.315991,  1.272938,  1.272938, -10.338936,  0.242611};
	float *jaspar_phylofacts_pwm_121_matrix[4] = { jaspar_phylofacts_pwm_121_matrix_row_0_, jaspar_phylofacts_pwm_121_matrix_row_1_, jaspar_phylofacts_pwm_121_matrix_row_2_, jaspar_phylofacts_pwm_121_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_121_ = {
/* accession        */ "PF0078",
/* name             */ "GCTNWTTGK",
/* label            */ " PF0078	14.500168388317	GCTNWTTGK	Unknown	; MCS \"16.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GCTGATTGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_121_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.899895,
/* max_score        */ 11.107975,
/* threshold        */ 0.806,
/* info content     */ 14.496543,
/* base_counts      */ {791, 1434, 3507, 4204},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1104
};

	float jaspar_phylofacts_pwm_122_matrix_row_0_[8] = {  0.209115,  0.432246, -9.445975,  1.272899,  1.272899, -9.445975, -9.445975, -9.445975};
	float jaspar_phylofacts_pwm_122_matrix_row_1_[8] = { -9.204813, -9.204813, -9.204813, -9.204813, -9.204813, -9.204813, -9.204813,  1.514061};
	float jaspar_phylofacts_pwm_122_matrix_row_2_[8] = { -9.204813, -9.204813, -9.204813, -9.204813, -9.204813,  1.514061,  1.514061, -9.204813};
	float jaspar_phylofacts_pwm_122_matrix_row_3_[8] = {  0.849588,  0.708310,  1.272899, -9.445975, -9.445975, -9.445975, -9.445975, -9.445975};
	float *jaspar_phylofacts_pwm_122_matrix[4] = { jaspar_phylofacts_pwm_122_matrix_row_0_, jaspar_phylofacts_pwm_122_matrix_row_1_, jaspar_phylofacts_pwm_122_matrix_row_2_, jaspar_phylofacts_pwm_122_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_122_ = {
/* accession        */ "PF0127",
/* name             */ "WWTAAGGC",
/* label            */ " PF0127	14.0839692050207	WWTAAGGC	Unknown	; MCS \"11.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTTAAGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_122_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.085480,
/* max_score        */ 9.918779,
/* threshold        */ 0.818,
/* info content     */ 14.075819,
/* base_counts      */ {1255, 452, 904, 1005},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 452
};

	float jaspar_phylofacts_pwm_123_matrix_row_0_[6] = { -11.893231,  1.272960, -11.893231, -11.893231, -11.893231, -11.893231};
	float jaspar_phylofacts_pwm_123_matrix_row_1_[6] = {  1.514122, -11.652069,  1.514122, -11.652069, -11.652069, -11.652069};
	float jaspar_phylofacts_pwm_123_matrix_row_2_[6] = { -11.652069, -11.652069, -11.652069,  1.514122, -11.652069,  1.514122};
	float jaspar_phylofacts_pwm_123_matrix_row_3_[6] = { -11.893231, -11.893231, -11.893231, -11.893231,  1.272960, -11.893231};
	float *jaspar_phylofacts_pwm_123_matrix[4] = { jaspar_phylofacts_pwm_123_matrix_row_0_, jaspar_phylofacts_pwm_123_matrix_row_1_, jaspar_phylofacts_pwm_123_matrix_row_2_, jaspar_phylofacts_pwm_123_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_123_ = {
/* accession        */ "PF0002",
/* name             */ "CACGTG",
/* label            */ " PF0002	12	CACGTG	Unknown	; MCS \"85.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"MYC\" ; type \"phylogenetic\" ",
/* motif            */ "CACGTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_123_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -71.359390,
/* max_score        */ 8.602407,
/* threshold        */ 0.861,
/* info content     */ 11.999295,
/* base_counts      */ {5224, 10448, 10448, 5224},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 5224
};

	float jaspar_phylofacts_pwm_124_matrix_row_0_[13] = {  1.272953, -11.131974, -11.131974,  1.272953, -11.131974,  1.163944,  0.686235, -0.771030, -1.983403, -11.131974, -11.131974, -11.131974,  1.061018};
	float jaspar_phylofacts_pwm_124_matrix_row_1_[13] = { -10.890812,  1.514115, -10.890812, -10.890812,  1.504231, -10.890812, -0.847519,  0.351629, -1.336811,  1.514115,  1.514115,  1.514115, -10.890812};
	float jaspar_phylofacts_pwm_124_matrix_row_2_[13] = { -10.890812, -10.890812, -10.890812, -10.890812, -10.890812, -0.756173, -1.155684, -0.552268, -2.037004, -10.890812, -10.890812, -10.890812, -0.141435};
	float jaspar_phylofacts_pwm_124_matrix_row_3_[13] = { -11.131974, -11.131974,  1.272953, -11.131974, -3.348334, -11.131974,  0.001169,  0.431654,  1.139423, -11.131974, -11.131974, -11.131974, -11.131974};
	float *jaspar_phylofacts_pwm_124_matrix[4] = { jaspar_phylofacts_pwm_124_matrix_row_0_, jaspar_phylofacts_pwm_124_matrix_row_1_, jaspar_phylofacts_pwm_124_matrix_row_2_, jaspar_phylofacts_pwm_124_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_124_ = {
/* accession        */ "PF0004",
/* name             */ "ACTAYRNNNCCCR",
/* label            */ " PF0004	20.622995901164	ACTAYRNNNCCCR	Unknown	; MCS \"69.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACTACAANTCCCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 13,
/* matrixname       */ jaspar_phylofacts_pwm_124_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -115.283455,
/* max_score        */ 15.861826,
/* threshold        */ 0.780,
/* info content     */ 20.620756,
/* base_counts      */ {10809, 13310, 1266, 6335},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2440
};

	float jaspar_phylofacts_pwm_125_matrix_row_0_[10] = { -8.967137,  1.272859, -8.967137, -8.967137, -8.967137, -0.429945, -0.354452, -8.967137, -8.967137, -8.967137};
	float jaspar_phylofacts_pwm_125_matrix_row_1_[10] = { -8.725975, -8.725975, -8.725975, -8.725975, -8.725975,  1.312961, -8.725975, -8.725975,  1.514021, -8.725975};
	float jaspar_phylofacts_pwm_125_matrix_row_2_[10] = {  1.514021, -8.725975, -8.725975,  1.514021,  1.436062, -8.725975,  1.295340,  1.514021, -8.725975,  1.514021};
	float jaspar_phylofacts_pwm_125_matrix_row_3_[10] = { -8.967137, -8.967137,  1.272859, -8.967137, -1.316968, -8.967137, -8.967137, -8.967137, -8.967137, -8.967137};
	float *jaspar_phylofacts_pwm_125_matrix[4] = { jaspar_phylofacts_pwm_125_matrix_row_0_, jaspar_phylofacts_pwm_125_matrix_row_1_, jaspar_phylofacts_pwm_125_matrix_row_2_, jaspar_phylofacts_pwm_125_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_125_ = {
/* accession        */ "PF0148",
/* name             */ "GATGKMRGCG",
/* label            */ " PF0148	18.2162185369208	GATGKMRGCG	Unknown	; MCS \"10.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GATGGCGGCG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_125_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.671379,
/* max_score        */ 14.160183,
/* threshold        */ 0.780,
/* info content     */ 18.200508,
/* base_counts      */ {386, 509, 1604, 301},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 280
};

	float jaspar_phylofacts_pwm_126_matrix_row_0_[9] = { -11.563325, -11.563325,  1.272958, -1.507074, -11.563325, -11.563325, -11.563325, -1.072023, -11.563325};
	float jaspar_phylofacts_pwm_126_matrix_row_1_[9] = { -11.322163,  1.514120, -11.322163,  0.490875,  1.514120, -11.322163, -11.322163,  0.671341,  0.871842};
	float jaspar_phylofacts_pwm_126_matrix_row_2_[9] = {  1.514120, -11.322163, -11.322163,  0.734835, -11.322163, -11.322163,  1.514120, -0.018712, -11.322163};
	float jaspar_phylofacts_pwm_126_matrix_row_3_[9] = { -11.563325, -11.563325, -11.563325, -0.848885, -11.563325,  1.272958, -11.563325, -0.082912,  0.526219};
	float *jaspar_phylofacts_pwm_126_matrix[4] = { jaspar_phylofacts_pwm_126_matrix_row_0_, jaspar_phylofacts_pwm_126_matrix_row_1_, jaspar_phylofacts_pwm_126_matrix_row_2_, jaspar_phylofacts_pwm_126_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_126_ = {
/* accession        */ "PF0035",
/* name             */ "GCANCTGNY",
/* label            */ " PF0035	13.5107278565549	GCANCTGNY	Unknown	; MCS \"25.7\" ; jaspar \"Myf\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"MYOD\" ; type \"phylogenetic\" ",
/* motif            */ "GCASCTGNC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_126_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.522369,
/* max_score        */ 10.880413,
/* threshold        */ 0.812,
/* info content     */ 13.509659,
/* base_counts      */ {4349, 12455, 10046, 6954},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3756
};

	float jaspar_phylofacts_pwm_127_matrix_row_0_[14] = { -9.372555, -9.372555, -9.372555, -9.372555, -3.378593, -9.372555,  0.648760, -0.410547,  0.036719, -9.372555,  0.061009,  1.272894,  1.272894, -9.372555};
	float jaspar_phylofacts_pwm_127_matrix_row_1_[14] = { -9.131392, -9.131392, -9.131392, -9.131392,  1.220013,  1.281950, -0.195357, -0.675862, -9.131392, -9.131392,  0.294140, -9.131392, -9.131392,  1.514056};
	float jaspar_phylofacts_pwm_127_matrix_row_2_[14] = {  1.514056, -9.131392, -9.131392,  1.514056, -2.222638, -9.131392, -0.321380,  0.058847,  1.170905,  1.514056, -0.555742, -9.131392, -9.131392, -9.131392};
	float jaspar_phylofacts_pwm_127_matrix_row_3_[14] = { -9.372555,  1.272894,  1.272894, -9.372555, -0.234677, -0.301361, -0.815948,  0.515870, -9.372555, -9.372555,  0.003385, -9.372555, -9.372555, -9.372555};
	float *jaspar_phylofacts_pwm_127_matrix[4] = { jaspar_phylofacts_pwm_127_matrix_row_0_, jaspar_phylofacts_pwm_127_matrix_row_1_, jaspar_phylofacts_pwm_127_matrix_row_2_, jaspar_phylofacts_pwm_127_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_127_ = {
/* accession        */ "PF0043",
/* name             */ "GTTGNYNNRGNAAC",
/* label            */ " PF0043	19.9420272952388	GTTGNYNNRGNAAC	Unknown	; MCS \"23.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GTTGCCANGGNAAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_127_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.151703,
/* max_score        */ 16.279440,
/* threshold        */ 0.781,
/* info content     */ 19.930508,
/* base_counts      */ {1394, 1313, 1786, 1387},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 420
};

	float jaspar_phylofacts_pwm_128_matrix_row_0_[6] = { -11.784974,  1.272959, -11.784974,  1.272959,  1.272959,  1.272959};
	float jaspar_phylofacts_pwm_128_matrix_row_1_[6] = { -11.543813, -11.543813, -11.543813, -11.543813, -11.543813, -11.543813};
	float jaspar_phylofacts_pwm_128_matrix_row_2_[6] = { -11.543813, -11.543813, -11.543813, -11.543813, -11.543813, -11.543813};
	float jaspar_phylofacts_pwm_128_matrix_row_3_[6] = {  1.272959, -11.784974,  1.272959, -11.784974, -11.784974, -11.784974};
	float *jaspar_phylofacts_pwm_128_matrix[4] = { jaspar_phylofacts_pwm_128_matrix_row_0_, jaspar_phylofacts_pwm_128_matrix_row_1_, jaspar_phylofacts_pwm_128_matrix_row_2_, jaspar_phylofacts_pwm_128_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_128_ = {
/* accession        */ "PF0051",
/* name             */ "TATAAA",
/* label            */ " PF0051	12	TATAAA	Unknown	; MCS \"22.1\" ; jaspar \"TBP\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"TATA\" ; type \"phylogenetic\" ",
/* motif            */ "TATAAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_128_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -70.709846,
/* max_score        */ 7.637755,
/* threshold        */ 0.861,
/* info content     */ 11.999221,
/* base_counts      */ {18752, 0, 0, 9376},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 4688
};

	float jaspar_phylofacts_pwm_129_matrix_row_0_[8] = { -10.231968,  1.272936, -10.231968, -10.231968, -0.444958,  1.272936, -10.231968, -10.231968};
	float jaspar_phylofacts_pwm_129_matrix_row_1_[8] = {  0.743436, -9.990806, -9.990806, -9.990806,  0.171231, -9.990806, -9.990806,  1.514098};
	float jaspar_phylofacts_pwm_129_matrix_row_2_[8] = { -9.990806, -9.990806, -9.990806, -9.990806, -0.112585, -9.990806, -9.990806, -9.990806};
	float jaspar_phylofacts_pwm_129_matrix_row_3_[8] = {  0.651742, -10.231968,  1.272936,  1.272936,  0.259334, -10.231968,  1.272936, -10.231968};
	float *jaspar_phylofacts_pwm_129_matrix[4] = { jaspar_phylofacts_pwm_129_matrix_row_0_, jaspar_phylofacts_pwm_129_matrix_row_1_, jaspar_phylofacts_pwm_129_matrix_row_2_, jaspar_phylofacts_pwm_129_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_129_ = {
/* accession        */ "PF0097",
/* name             */ "YATTNATC",
/* label            */ " PF0097	13.0614448983193	YATTNATC	Unknown	; MCS \"13.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"CDP(*)\" ; type \"phylogenetic\" ",
/* motif            */ "TATTNATC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_129_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.068733,
/* max_score        */ 8.881545,
/* threshold        */ 0.826,
/* info content     */ 13.057823,
/* base_counts      */ {2162, 1710, 195, 3869},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 992
};

	float jaspar_phylofacts_pwm_130_matrix_row_0_[12] = { -9.022699, -9.022699, -9.022699,  1.272864,  1.272864,  0.104368,  0.309947, -9.022699, -9.022699, -9.022699, -0.525505, -9.022699};
	float jaspar_phylofacts_pwm_130_matrix_row_1_[12] = {  1.514026, -8.781537, -8.781537, -8.781537, -8.781537, -8.781537, -0.487238, -8.781537, -8.781537,  1.514026, -0.592570, -8.781537};
	float jaspar_phylofacts_pwm_130_matrix_row_2_[12] = { -8.781537,  1.514026,  1.514026, -8.781537, -8.781537,  1.141802,  0.334603,  1.514026,  1.514026, -8.781537,  1.022185,  1.514026};
	float jaspar_phylofacts_pwm_130_matrix_row_3_[12] = { -9.022699, -9.022699, -9.022699, -9.022699, -9.022699, -9.022699, -0.466093, -9.022699, -9.022699, -9.022699, -1.015998, -9.022699};
	float *jaspar_phylofacts_pwm_130_matrix[4] = { jaspar_phylofacts_pwm_130_matrix_row_0_, jaspar_phylofacts_pwm_130_matrix_row_1_, jaspar_phylofacts_pwm_130_matrix_row_2_, jaspar_phylofacts_pwm_130_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_130_ = {
/* accession        */ "PF0081",
/* name             */ "CGGAARNGGCNG",
/* label            */ " PF0081	19.6535618616035	CGGAARNGGCNG	Unknown	; MCS \"15.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CGGAAGNGGCGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_130_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.730217,
/* max_score        */ 15.642504,
/* threshold        */ 0.777,
/* info content     */ 19.637497,
/* base_counts      */ {846, 668, 1956, 82},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 296
};

	float jaspar_phylofacts_pwm_131_matrix_row_0_[7] = { -11.658731, -11.658731, -11.658731, -11.658731,  1.272958, -11.658731, -11.658731};
	float jaspar_phylofacts_pwm_131_matrix_row_1_[7] = {  1.514120, -11.417569, -11.417569,  1.514120, -11.417569, -11.417569,  1.105870};
	float jaspar_phylofacts_pwm_131_matrix_row_2_[7] = { -11.417569, -11.417569,  1.514120, -11.417569, -11.417569,  1.514120, -11.417569};
	float jaspar_phylofacts_pwm_131_matrix_row_3_[7] = { -11.658731,  1.272958, -11.658731, -11.658731, -11.658731, -11.658731,  0.179902};
	float *jaspar_phylofacts_pwm_131_matrix[4] = { jaspar_phylofacts_pwm_131_matrix_row_0_, jaspar_phylofacts_pwm_131_matrix_row_1_, jaspar_phylofacts_pwm_131_matrix_row_2_, jaspar_phylofacts_pwm_131_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_131_ = {
/* accession        */ "PF0101",
/* name             */ "CTGCAGY",
/* label            */ " PF0101	13.0798598934366	CTGCAGY	Unknown	; MCS \"13.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CTGCAGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_131_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.611115,
/* max_score        */ 9.708268,
/* threshold        */ 0.837,
/* info content     */ 13.078890,
/* base_counts      */ {4132, 11011, 8264, 5517},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 4132
};

	float jaspar_phylofacts_pwm_132_matrix_row_0_[8] = { -10.247968, -10.247968,  1.272936, -10.247968, -10.247968, -10.247968, -10.247968, -10.247968};
	float jaspar_phylofacts_pwm_132_matrix_row_1_[8] = {  1.000299,  1.117527, -10.006805, -10.006805, -10.006805, -10.006805, -10.006805, -10.006805};
	float jaspar_phylofacts_pwm_132_matrix_row_2_[8] = {  0.602276, -10.006805, -10.006805, -10.006805, -10.006805,  1.514098, -10.006805,  1.514098};
	float jaspar_phylofacts_pwm_132_matrix_row_3_[8] = { -10.247968,  0.156326, -10.247968,  1.272936,  1.272936, -10.247968,  1.272936, -10.247968};
	float *jaspar_phylofacts_pwm_132_matrix[4] = { jaspar_phylofacts_pwm_132_matrix_row_0_, jaspar_phylofacts_pwm_132_matrix_row_1_, jaspar_phylofacts_pwm_132_matrix_row_2_, jaspar_phylofacts_pwm_132_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_132_ = {
/* accession        */ "PF0071",
/* name             */ "SYATTGTG",
/* label            */ " PF0071	14.1157863137353	SYATTGTG	Unknown	; MCS \"17.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCATTGTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_132_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.983742,
/* max_score        */ 10.237765,
/* threshold        */ 0.817,
/* info content     */ 14.111876,
/* base_counts      */ {1008, 1281, 2421, 3354},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1008
};

	float jaspar_phylofacts_pwm_133_matrix_row_0_[12] = { -9.454785, -9.454785, -9.454785, -1.135799, -0.013254,  1.272900, -0.186081,  1.272900, -9.454785, -9.454785, -9.454785,  0.924968};
	float jaspar_phylofacts_pwm_133_matrix_row_1_[12] = { -9.213623, -9.213623, -9.213623, -1.142405,  0.803238, -9.213623, -0.582923, -9.213623, -9.213623,  1.514062,  1.184591, -9.213623};
	float jaspar_phylofacts_pwm_133_matrix_row_2_[12] = { -9.213623, -9.213623, -9.213623, -0.997265, -0.676431, -9.213623,  0.952267, -9.213623,  1.514062, -9.213623, -9.213623,  0.289462};
	float jaspar_phylofacts_pwm_133_matrix_row_3_[12] = {  1.272900,  1.272900,  1.272900,  0.996853, -0.842100, -9.454785, -1.322960, -9.454785, -9.454785, -9.454785,  0.002493, -9.454785};
	float *jaspar_phylofacts_pwm_133_matrix[4] = { jaspar_phylofacts_pwm_133_matrix_row_0_, jaspar_phylofacts_pwm_133_matrix_row_1_, jaspar_phylofacts_pwm_133_matrix_row_2_, jaspar_phylofacts_pwm_133_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_133_ = {
/* accession        */ "PF0169",
/* name             */ "TTTNNANAGCYR",
/* label            */ " PF0169	17.751982119229	TTTNNANAGCYR	Unknown	; MCS \"9.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTTTMAGAGCCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_133_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.400536,
/* max_score        */ 14.254540,
/* threshold        */ 0.778,
/* info content     */ 17.742493,
/* base_counts      */ {1507, 1096, 938, 1931},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 456
};

	float jaspar_phylofacts_pwm_134_matrix_row_0_[9] = {  1.272859, -8.967137,  1.272859,  0.874528, -8.967137,  1.272859,  1.272859,  1.272859, -8.967137};
	float jaspar_phylofacts_pwm_134_matrix_row_1_[9] = { -8.725975,  1.514021, -8.725975, -8.725975,  1.094185, -8.725975, -8.725975, -8.725975, -8.725975};
	float jaspar_phylofacts_pwm_134_matrix_row_2_[9] = { -8.725975, -8.725975, -8.725975, -8.725975, -8.725975, -8.725975, -8.725975, -8.725975,  1.514021};
	float jaspar_phylofacts_pwm_134_matrix_row_3_[9] = { -8.967137, -8.967137, -8.967137,  0.159930,  0.202486, -8.967137, -8.967137, -8.967137, -8.967137};
	float *jaspar_phylofacts_pwm_134_matrix[4] = { jaspar_phylofacts_pwm_134_matrix_row_0_, jaspar_phylofacts_pwm_134_matrix_row_1_, jaspar_phylofacts_pwm_134_matrix_row_2_, jaspar_phylofacts_pwm_134_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_134_ = {
/* accession        */ "PF0142",
/* name             */ "ACAWYAAAG",
/* label            */ " PF0142	16.1590132663114	ACAWYAAAG	Unknown	; MCS \"10.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACAACAAAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_134_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.463074,
/* max_score        */ 11.361048,
/* threshold        */ 0.795,
/* info content     */ 16.144644,
/* base_counts      */ {1588, 464, 280, 188},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 280
};

	float jaspar_phylofacts_pwm_135_matrix_row_0_[9] = { -11.273870, -11.273870, -11.273870, -11.273870, -11.273870, -11.273870, -11.273870, -0.900348, -11.273870};
	float jaspar_phylofacts_pwm_135_matrix_row_1_[9] = { -11.032708, -11.032708, -11.032708,  0.927471, -11.032708, -11.032708, -11.032708, -0.640610,  1.183815};
	float jaspar_phylofacts_pwm_135_matrix_row_2_[9] = {  1.514117,  1.514117,  1.514117, -11.032708,  1.514117, -11.032708,  1.514117,  0.979786, -11.032708};
	float jaspar_phylofacts_pwm_135_matrix_row_3_[9] = { -11.273870, -11.273870, -11.273870,  0.460606, -11.273870,  1.272955, -11.273870, -0.418705,  0.004611};
	float *jaspar_phylofacts_pwm_135_matrix[4] = { jaspar_phylofacts_pwm_135_matrix_row_0_, jaspar_phylofacts_pwm_135_matrix_row_1_, jaspar_phylofacts_pwm_135_matrix_row_2_, jaspar_phylofacts_pwm_135_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_135_ = {
/* accession        */ "PF0031",
/* name             */ "GGGYGTGNY",
/* label            */ " PF0031	14.5333591848674	GGGYGTGNY	Unknown	; MCS \"30.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGGCGTGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_135_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.091316,
/* max_score        */ 11.934612,
/* threshold        */ 0.806,
/* info content     */ 14.531830,
/* base_counts      */ {320, 3911, 15708, 5369},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2812
};

	float jaspar_phylofacts_pwm_136_matrix_row_0_[12] = { -9.251205, -9.251205, -9.251205, -1.387555,  0.988790,  0.863393, -0.714014, -9.251205, -1.313474,  1.272885, -9.251205, -9.251205};
	float jaspar_phylofacts_pwm_136_matrix_row_1_[12] = {  1.514047,  1.514047, -9.010044, -0.620457,  0.117024, -0.128069, -0.878219, -9.010044, -0.643441, -9.010044,  1.514047, -9.010044};
	float jaspar_phylofacts_pwm_136_matrix_row_2_[12] = { -9.010044, -9.010044,  1.514047,  1.278740, -9.010044, -0.938825,  1.211934, -9.010044,  1.251153, -9.010044, -9.010044,  1.514047};
	float jaspar_phylofacts_pwm_136_matrix_row_3_[12] = { -9.251205, -9.251205, -9.251205, -2.565345, -9.251205, -1.601037, -2.160296,  1.272885, -1.937319, -9.251205, -9.251205, -9.251205};
	float *jaspar_phylofacts_pwm_136_matrix[4] = { jaspar_phylofacts_pwm_136_matrix_row_0_, jaspar_phylofacts_pwm_136_matrix_row_1_, jaspar_phylofacts_pwm_136_matrix_row_2_, jaspar_phylofacts_pwm_136_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_136_ = {
/* accession        */ "PF0106",
/* name             */ "CCGNMNNTNACG",
/* label            */ " PF0106	18.4740178263056	CCGNMNNTNACG	Unknown	; MCS \"12.9\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCGGAAGTGACG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_136_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.273643,
/* max_score        */ 15.710016,
/* threshold        */ 0.778,
/* info content     */ 18.463005,
/* base_counts      */ {1004, 1401, 1631, 428},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 372
};

	float jaspar_phylofacts_pwm_137_matrix_row_0_[10] = { -8.923341, -8.923341,  1.272854,  0.609155,  1.019415,  0.364053,  1.272854,  1.272854, -8.923341, -8.923341};
	float jaspar_phylofacts_pwm_137_matrix_row_1_[10] = {  1.514016,  1.514016, -8.682178, -8.682178, -8.682178, -0.270124, -8.682178, -8.682178, -8.682178, -8.682178};
	float jaspar_phylofacts_pwm_137_matrix_row_2_[10] = { -8.682178, -8.682178, -8.682178, -8.682178, -8.682178, -0.744447, -8.682178, -8.682178,  1.514016,  1.514016};
	float jaspar_phylofacts_pwm_137_matrix_row_3_[10] = { -8.923341, -8.923341, -8.923341,  0.549441, -0.223659,  0.147852, -8.923341, -8.923341, -8.923341, -8.923341};
	float *jaspar_phylofacts_pwm_137_matrix[4] = { jaspar_phylofacts_pwm_137_matrix_row_0_, jaspar_phylofacts_pwm_137_matrix_row_1_, jaspar_phylofacts_pwm_137_matrix_row_2_, jaspar_phylofacts_pwm_137_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_137_ = {
/* accession        */ "PF0121",
/* name             */ "CCAWWNAAGG",
/* label            */ " PF0121	16.4054352247606	CCAWWNAAGG	Unknown	; MCS \"11.7\" ; jaspar \"SRF\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"SRF\" ; type \"phylogenetic\" ",
/* motif            */ "CCAAANAAGG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_137_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.572189,
/* max_score        */ 11.867249,
/* threshold        */ 0.789,
/* info content     */ 16.390409,
/* base_counts      */ {1258, 581, 564, 277},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 268
};

	float jaspar_phylofacts_pwm_138_matrix_row_0_[15] = { -8.589909, -0.652176, -8.589909, -8.589909, -8.589909, -0.072515,  0.590076,  0.515183,  1.272809, -0.072515, -8.589909, -8.589909,  1.272809,  0.319462,  1.109074};
	float jaspar_phylofacts_pwm_138_matrix_row_1_[15] = {  0.861694,  0.505062, -8.348746, -8.348746, -8.348746,  0.106784,  0.106784, -0.485095, -8.348746, -1.257836, -8.348746,  1.513971, -8.348746, -8.348746, -0.375935};
	float jaspar_phylofacts_pwm_138_matrix_row_2_[15] = { -8.348746, -0.132388, -8.348746, -8.348746, -8.348746, -0.309266, -0.698577, -0.054447, -8.348746,  0.207860,  1.513971, -8.348746, -8.348746,  1.027194, -8.348746};
	float jaspar_phylofacts_pwm_138_matrix_row_3_[15] = {  0.537159,  0.058489,  1.272809,  1.272809,  1.272809,  0.174301, -0.688531, -0.400941, -8.589909,  0.372099, -8.589909, -8.589909, -8.589909, -8.589909, -8.589909};
	float *jaspar_phylofacts_pwm_138_matrix[4] = { jaspar_phylofacts_pwm_138_matrix_row_0_, jaspar_phylofacts_pwm_138_matrix_row_1_, jaspar_phylofacts_pwm_138_matrix_row_2_, jaspar_phylofacts_pwm_138_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_138_ = {
/* accession        */ "PF0166",
/* name             */ "YNTTTNNNANGCARM",
/* label            */ " PF0166	18.1947520491882	YNTTTNNNANGCARM	Unknown	; MCS \"9.6\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CNTTTNANANGCAGA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 15,
/* matrixname       */ jaspar_phylofacts_pwm_138_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.302032,
/* max_score        */ 14.546673,
/* threshold        */ 0.775,
/* info content     */ 18.172552,
/* base_counts      */ {936, 523, 491, 930},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 192
};

	float jaspar_phylofacts_pwm_139_matrix_row_0_[10] = {  1.272916,  1.272916, -9.735845, -0.278566,  1.103756, -9.735845,  1.272916,  1.272916, -0.074365, -9.735845};
	float jaspar_phylofacts_pwm_139_matrix_row_1_[10] = { -9.494683, -9.494683,  1.514078, -9.494683, -9.494683,  1.514078, -9.494683, -9.494683,  0.535481, -9.494683};
	float jaspar_phylofacts_pwm_139_matrix_row_2_[10] = { -9.494683, -9.494683, -9.494683, -9.494683, -9.494683, -9.494683, -9.494683, -9.494683, -0.264442,  0.210415};
	float jaspar_phylofacts_pwm_139_matrix_row_3_[10] = { -9.735845, -9.735845, -9.735845,  1.034764, -0.587273, -9.735845, -9.735845, -9.735845, -0.359905,  0.956123};
	float *jaspar_phylofacts_pwm_139_matrix[4] = { jaspar_phylofacts_pwm_139_matrix_row_0_, jaspar_phylofacts_pwm_139_matrix_row_1_, jaspar_phylofacts_pwm_139_matrix_row_2_, jaspar_phylofacts_pwm_139_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_139_ = {
/* accession        */ "PF0108",
/* name             */ "AACWWCAANK",
/* label            */ " PF0108	15.8580414595794	AACWWCAANK	Unknown	; MCS \"12.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"FAC1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "AACTACAANT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_139_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.500191,
/* max_score        */ 11.749944,
/* threshold        */ 0.791,
/* info content     */ 15.851167,
/* base_counts      */ {3211, 1435, 266, 1128},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 604
};

	float jaspar_phylofacts_pwm_140_matrix_row_0_[11] = { -9.463518, -9.463518, -9.463518, -0.514413, -0.488773, -1.051463,  0.111535, -9.463518,  1.272900,  1.272900,  0.530770};
	float jaspar_phylofacts_pwm_140_matrix_row_1_[11] = { -9.222356,  1.514063,  1.514063,  0.700983,  0.863495, -0.001966, -9.222356, -9.222356, -9.222356, -9.222356, -9.222356};
	float jaspar_phylofacts_pwm_140_matrix_row_2_[11] = {  1.514063, -9.222356, -9.222356, -0.021965, -1.182876, -1.284624, -9.222356, -9.222356, -9.222356, -9.222356,  0.867653};
	float jaspar_phylofacts_pwm_140_matrix_row_3_[11] = { -9.463518, -9.463518, -9.463518, -0.476196, -0.157777,  0.797679,  0.897426,  1.272900, -9.463518, -9.463518, -9.463518};
	float *jaspar_phylofacts_pwm_140_matrix[4] = { jaspar_phylofacts_pwm_140_matrix_row_0_, jaspar_phylofacts_pwm_140_matrix_row_1_, jaspar_phylofacts_pwm_140_matrix_row_2_, jaspar_phylofacts_pwm_140_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_140_ = {
/* accession        */ "PF0095",
/* name             */ "GCCNNNWTAAR",
/* label            */ " PF0095	15.0748333637152	GCCNNNWTAAR	Unknown	; MCS \"13.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GCCNCTTTAAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_140_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.448891,
/* max_score        */ 12.488125,
/* threshold        */ 0.788,
/* info content     */ 15.066633,
/* base_counts      */ {1484, 1465, 859, 1252},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 460
};

	float jaspar_phylofacts_pwm_141_matrix_row_0_[16] = {  0.511643, -0.087360, -8.356348, -8.356348,  1.272768, -0.454971, -0.383537,  0.618397,  0.055707,  0.033239, -8.356348,  0.180844,  1.272768, -8.356348, -8.356348,  0.770719};
	float jaspar_phylofacts_pwm_141_matrix_row_1_[16] = { -8.115186,  0.382008, -8.115186,  1.513930, -8.115186,  0.274401, -0.177454, -8.115186, -0.142375,  0.274401,  0.460465,  0.296869, -8.115186, -8.115186, -8.115186, -8.115186};
	float jaspar_phylofacts_pwm_141_matrix_row_2_[16] = {  0.884557, -0.177454, -8.115186, -8.115186, -8.115186, -0.418519, -0.418519,  0.780581,  0.045618, -1.206431, -8.115186, -1.429325, -8.115186, -8.115186, -8.115186, -8.115186};
	float jaspar_phylofacts_pwm_141_matrix_row_3_[16] = { -8.356348, -0.167381,  1.272768, -8.356348, -8.356348,  0.326529,  0.539419, -8.356348,  0.010255,  0.237991,  0.844043,  0.120231, -8.356348,  1.272768,  1.272768,  0.343333};
	float *jaspar_phylofacts_pwm_141_matrix[4] = { jaspar_phylofacts_pwm_141_matrix_row_0_, jaspar_phylofacts_pwm_141_matrix_row_1_, jaspar_phylofacts_pwm_141_matrix_row_2_, jaspar_phylofacts_pwm_141_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_141_ = {
/* accession        */ "PF0122",
/* name             */ "RNTCANNRNNYNATTW",
/* label            */ " PF0122	16.8199206715049	RNTCANNRNNYNATTW	Unknown	; MCS \"11.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GNTCANNANNTNATTA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 16,
/* matrixname       */ jaspar_phylofacts_pwm_141_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.151390,
/* max_score        */ 13.032603,
/* threshold        */ 0.764,
/* info content     */ 16.793758,
/* base_counts      */ {781, 444, 279, 928},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 152
};

	float jaspar_phylofacts_pwm_142_matrix_row_0_[14] = { -8.796206, -8.796206,  1.272839,  1.272839, -8.796206, -1.146037,  0.126586, -8.796206, -0.047742, -0.340675, -0.220555, -8.796206, -8.796206, -8.796206};
	float jaspar_phylofacts_pwm_142_matrix_row_1_[14] = {  1.514001,  1.514001, -8.555044, -8.555044, -8.555044,  0.786413, -0.548343,  0.419701,  0.582834, -0.953641,  0.419701, -8.555044,  1.514001, -8.555044};
	float jaspar_phylofacts_pwm_142_matrix_row_2_[14] = { -8.555044, -8.555044, -8.555044, -8.555044, -8.555044,  0.593528,  0.820896,  1.106436,  0.193420,  0.998957,  0.444699,  1.514001, -8.555044,  1.514001};
	float jaspar_phylofacts_pwm_142_matrix_row_3_[14] = { -8.796206, -8.796206, -8.796206, -8.796206,  1.272839, -2.243698, -1.625317, -8.796206, -1.357234, -0.858474, -1.055107, -8.796206, -8.796206, -8.796206};
	float *jaspar_phylofacts_pwm_142_matrix[4] = { jaspar_phylofacts_pwm_142_matrix_row_0_, jaspar_phylofacts_pwm_142_matrix_row_1_, jaspar_phylofacts_pwm_142_matrix_row_2_, jaspar_phylofacts_pwm_142_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_142_ = {
/* accession        */ "PF0104",
/* name             */ "CCAATNNSNNNGCG",
/* label            */ " PF0104	18.6852937354041	CCAATNNSNNNGCG	Unknown	; MCS \"13.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCAATSRGNGNGCG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_142_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.400841,
/* max_score        */ 16.128754,
/* threshold        */ 0.777,
/* info content     */ 18.667154,
/* base_counts      */ {731, 1123, 1126, 324},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 236
};

	float jaspar_phylofacts_pwm_143_matrix_row_0_[9] = {  1.272925,  1.272925,  1.272925, -0.742576,  0.208102,  0.223912, -9.922561, -9.922561, -9.922561};
	float jaspar_phylofacts_pwm_143_matrix_row_1_[9] = { -9.681398, -9.681398, -9.681398,  0.391874, -9.681398, -9.681398, -9.681398, -9.681398,  1.514087};
	float jaspar_phylofacts_pwm_143_matrix_row_2_[9] = { -9.681398, -9.681398, -9.681398,  0.484492, -9.681398, -9.681398, -9.681398,  1.514087, -9.681398};
	float jaspar_phylofacts_pwm_143_matrix_row_3_[9] = { -9.922561, -9.922561, -9.922561, -0.419476,  0.850147,  0.841726,  1.272925, -9.922561, -9.922561};
	float *jaspar_phylofacts_pwm_143_matrix[4] = { jaspar_phylofacts_pwm_143_matrix_row_0_, jaspar_phylofacts_pwm_143_matrix_row_1_, jaspar_phylofacts_pwm_143_matrix_row_2_, jaspar_phylofacts_pwm_143_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_143_ = {
/* accession        */ "PF0144",
/* name             */ "AAANWWTGC",
/* label            */ " PF0144	14.2418901235448	AAANWWTGC	Unknown	; MCS \"10.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "AAANTTTGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_143_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -79.640739,
/* max_score        */ 10.296237,
/* threshold        */ 0.807,
/* info content     */ 14.236605,
/* base_counts      */ {2787, 965, 988, 1812},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 728
};

	float jaspar_phylofacts_pwm_144_matrix_row_0_[8] = { -10.618423,  0.300313, -10.618423, -10.618423, -10.618423, -10.618423, -10.618423, -10.618423};
	float jaspar_phylofacts_pwm_144_matrix_row_1_[8] = {  1.014637,  1.039164, -10.377261, -10.377261, -10.377261, -10.377261, -10.377261, -10.377261};
	float jaspar_phylofacts_pwm_144_matrix_row_2_[8] = {  0.580555, -10.377261, -10.377261, -10.377261, -10.377261, -10.377261,  1.514107, -10.377261};
	float jaspar_phylofacts_pwm_144_matrix_row_3_[8] = { -10.618423, -10.618423,  1.272945,  1.272945,  1.272945,  1.272945, -10.618423,  1.272945};
	float *jaspar_phylofacts_pwm_144_matrix[4] = { jaspar_phylofacts_pwm_144_matrix_row_0_, jaspar_phylofacts_pwm_144_matrix_row_1_, jaspar_phylofacts_pwm_144_matrix_row_2_, jaspar_phylofacts_pwm_144_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_144_ = {
/* accession        */ "PF0062",
/* name             */ "SMTTTTGT",
/* label            */ " PF0062	14.0765210087483	SMTTTTGT	Unknown	; MCS \"19.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCTTTTGT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_144_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.947388,
/* max_score        */ 9.932634,
/* threshold        */ 0.818,
/* info content     */ 14.073742,
/* base_counts      */ {552, 1794, 2034, 7300},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1460
};

	float jaspar_phylofacts_pwm_145_matrix_row_0_[10] = {  1.272878, -9.172962, -9.172962, -0.263592, -1.133483, -9.172962, -9.172962, -9.172962, -9.172962, -9.172962};
	float jaspar_phylofacts_pwm_145_matrix_row_1_[10] = { -8.931801, -8.931801,  1.514041,  1.271829,  1.010956, -8.931801,  1.514041,  1.514041, -8.931801,  1.502345};
	float jaspar_phylofacts_pwm_145_matrix_row_2_[10] = { -8.931801, -8.931801, -8.931801, -8.931801, -0.892320, -8.931801, -8.931801, -8.931801,  1.514041, -8.931801};
	float jaspar_phylofacts_pwm_145_matrix_row_3_[10] = { -9.172962,  1.272878, -9.172962, -9.172962, -0.263592,  1.272878, -9.172962, -9.172962, -9.172962, -3.179001};
	float *jaspar_phylofacts_pwm_145_matrix[4] = { jaspar_phylofacts_pwm_145_matrix_row_0_, jaspar_phylofacts_pwm_145_matrix_row_1_, jaspar_phylofacts_pwm_145_matrix_row_2_, jaspar_phylofacts_pwm_145_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_145_ = {
/* accession        */ "PF0099",
/* name             */ "ATCMNTCCGY",
/* label            */ " PF0099	17.6159262597918	ATCMNTCCGY	Unknown	; MCS \"13.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ATCCCTCCGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_145_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.690140,
/* max_score        */ 13.659928,
/* threshold        */ 0.783,
/* info content     */ 17.603655,
/* base_counts      */ {449, 1850, 375, 766},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 344
};

	float jaspar_phylofacts_pwm_146_matrix_row_0_[6] = { -12.750538,  1.272963, -12.750538, -12.750538, -12.750538, -12.750538};
	float jaspar_phylofacts_pwm_146_matrix_row_1_[6] = {  1.514125, -12.509376, -12.509376, -12.509376, -12.509376, -12.509376};
	float jaspar_phylofacts_pwm_146_matrix_row_2_[6] = { -12.509376, -12.509376,  1.514125,  1.514125, -12.509376,  1.514125};
	float jaspar_phylofacts_pwm_146_matrix_row_3_[6] = { -12.750538, -12.750538, -12.750538, -12.750538,  1.272963, -12.750538};
	float *jaspar_phylofacts_pwm_146_matrix[4] = { jaspar_phylofacts_pwm_146_matrix_row_0_, jaspar_phylofacts_pwm_146_matrix_row_1_, jaspar_phylofacts_pwm_146_matrix_row_2_, jaspar_phylofacts_pwm_146_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_146_ = {
/* accession        */ "PF0012",
/* name             */ "CAGGTG",
/* label            */ " PF0012	12	CAGGTG	Unknown	; MCS \"47.6\" ; jaspar \"ESR1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"E12\" ; type \"phylogenetic\" ",
/* motif            */ "CAGGTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_146_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.503227,
/* max_score        */ 8.602428,
/* threshold        */ 0.861,
/* info content     */ 11.999683,
/* base_counts      */ {12312, 12312, 36936, 12312},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 12312
};

	float jaspar_phylofacts_pwm_147_matrix_row_0_[10] = { -9.313720, -9.313720, -9.313720,  0.204179,  0.151340, -9.313720, -9.313720, -9.313720, -9.313720, -9.313720};
	float jaspar_phylofacts_pwm_147_matrix_row_1_[10] = { -9.072557, -9.072557, -9.072557, -0.060546,  0.137883, -9.072557, -9.072557, -9.072557,  1.514052,  1.514052};
	float jaspar_phylofacts_pwm_147_matrix_row_2_[10] = {  1.514052,  1.514052,  1.514052,  0.361006, -0.136523, -9.072557, -9.072557, -9.072557, -9.072557, -9.072557};
	float jaspar_phylofacts_pwm_147_matrix_row_3_[10] = { -9.313720, -9.313720, -9.313720, -0.738069, -0.197580,  1.272890,  1.272890,  1.272890, -9.313720, -9.313720};
	float *jaspar_phylofacts_pwm_147_matrix[4] = { jaspar_phylofacts_pwm_147_matrix_row_0_, jaspar_phylofacts_pwm_147_matrix_row_1_, jaspar_phylofacts_pwm_147_matrix_row_2_, jaspar_phylofacts_pwm_147_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_147_ = {
/* accession        */ "PF0086",
/* name             */ "GGGNNTTTCC",
/* label            */ " PF0086	16.1135159736544	GGGNNTTTCC	Unknown	; MCS \"14.9\" ; jaspar \"RELA\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"NF-KAPPAB\" ; type \"phylogenetic\" ",
/* motif            */ "GGGNNTTTCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_147_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.445412,
/* max_score        */ 11.901278,
/* threshold        */ 0.790,
/* info content     */ 16.103363,
/* base_counts      */ {265, 974, 1389, 1332},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 396
};

	float jaspar_phylofacts_pwm_148_matrix_row_0_[11] = { -9.871848,  0.507905, -9.871848,  1.272922, -9.871848, -2.700960, -9.871848, -9.871848, -9.871848, -9.871848,  0.844811};
	float jaspar_phylofacts_pwm_148_matrix_row_1_[11] = { -9.630686, -0.135092,  1.514084, -9.630686, -9.630686, -0.142638, -9.630686,  1.514084,  1.514084,  1.289860, -9.630686};
	float jaspar_phylofacts_pwm_148_matrix_row_2_[11] = { -9.630686, -0.461064, -9.630686, -9.630686, -9.630686, -2.134589, -9.630686, -9.630686, -9.630686, -9.630686,  0.459322};
	float jaspar_phylofacts_pwm_148_matrix_row_3_[11] = {  1.272922, -0.317847, -9.871848, -9.871848,  1.272922,  1.004329,  1.272922, -9.871848, -9.871848, -0.332132, -9.871848};
	float *jaspar_phylofacts_pwm_148_matrix[4] = { jaspar_phylofacts_pwm_148_matrix_row_0_, jaspar_phylofacts_pwm_148_matrix_row_1_, jaspar_phylofacts_pwm_148_matrix_row_2_, jaspar_phylofacts_pwm_148_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_148_ = {
/* accession        */ "PF0075",
/* name             */ "TNCATNTCCYR",
/* label            */ " PF0075	17.5131498899234	TNCATNTCCYR	Unknown	; MCS \"16.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"STAT1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "TNCATTTCCCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_148_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.008659,
/* max_score        */ 13.280849,
/* threshold        */ 0.781,
/* info content     */ 17.506689,
/* base_counts      */ {1478, 2894, 355, 2885},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 692
};

	float jaspar_phylofacts_pwm_149_matrix_row_0_[11] = { -9.530765, -9.530765, -9.530765, -0.035170, -9.530765, -9.530765, -9.530765,  1.272905, -9.530765, -1.789665, -9.530765};
	float jaspar_phylofacts_pwm_149_matrix_row_1_[11] = { -9.289602, -9.289602,  1.514067,  0.474680, -9.289602,  1.514067,  1.514067, -9.289602, -9.289602,  0.497407, -9.289602};
	float jaspar_phylofacts_pwm_149_matrix_row_2_[11] = {  1.514067,  1.514067, -9.289602,  0.220917,  1.346277, -9.289602, -9.289602, -9.289602, -9.289602,  0.220917,  0.077827};
	float jaspar_phylofacts_pwm_149_matrix_row_3_[11] = { -9.530765, -9.530765, -9.530765, -1.013371, -0.594729, -9.530765, -9.530765, -9.530765,  1.272905,  0.124326,  1.001358};
	float *jaspar_phylofacts_pwm_149_matrix[4] = { jaspar_phylofacts_pwm_149_matrix_row_0_, jaspar_phylofacts_pwm_149_matrix_row_1_, jaspar_phylofacts_pwm_149_matrix_row_2_, jaspar_phylofacts_pwm_149_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_149_ = {
/* accession        */ "PF0088",
/* name             */ "GGCNKCCATNK",
/* label            */ " PF0088	16.9254523550943	GGCNKCCATNK	Unknown	; MCS \"14.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCNGCCATNT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_149_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.579910,
/* max_score        */ 13.435865,
/* threshold        */ 0.783,
/* info content     */ 16.916737,
/* base_counts      */ {648, 1828, 1787, 1149},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 492
};

	float jaspar_phylofacts_pwm_150_matrix_row_0_[12] = { -8.589909, -8.589909,  1.272809,  0.397414,  0.234917,  0.158555,  1.272809,  0.537159, -8.589909, -8.589909, -8.589909,  0.569244};
	float jaspar_phylofacts_pwm_150_matrix_row_1_[12] = { -8.348746, -8.348746, -8.348746, -0.054447, -8.348746, -0.411014, -8.348746, -0.029760, -8.348746, -8.348746, -8.348746,  0.831238};
	float jaspar_phylofacts_pwm_150_matrix_row_2_[12] = { -8.348746, -8.348746, -8.348746, -0.277528, -8.348746, -0.852649, -8.348746, -0.524300, -8.348746,  1.513971,  1.513971, -8.348746};
	float jaspar_phylofacts_pwm_150_matrix_row_3_[12] = {  1.272809,  1.272809, -8.589909, -0.295609,  0.835624,  0.434223, -8.589909, -0.458083,  1.272809, -8.589909, -8.589909, -8.589909};
	float *jaspar_phylofacts_pwm_150_matrix[4] = { jaspar_phylofacts_pwm_150_matrix_row_0_, jaspar_phylofacts_pwm_150_matrix_row_1_, jaspar_phylofacts_pwm_150_matrix_row_2_, jaspar_phylofacts_pwm_150_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_150_ = {
/* accession        */ "PF0161",
/* name             */ "TTANWNANTGGM",
/* label            */ " PF0161	16.5770786874249	TTANWNANTGGM	Unknown	; MCS \"9.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTANTWANTGGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_150_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.740570,
/* max_score        */ 12.427648,
/* threshold        */ 0.779,
/* info content     */ 16.556648,
/* base_counts      */ {782, 206, 459, 857},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 192
};

	float jaspar_phylofacts_pwm_151_matrix_row_0_[14] = { -9.506074, -9.506074, -9.506074,  0.103110, -0.256417,  0.246649, -0.218680, -0.741864,  0.155406,  1.272903,  1.272903, -9.506074,  0.867448, -9.506074};
	float jaspar_phylofacts_pwm_151_matrix_row_1_[14] = {  1.514065,  1.514065,  1.514065, -0.290167,  0.470216,  0.168652,  0.136131,  0.733931,  0.648575, -9.264912, -9.264912, -9.264912, -9.264912, -9.264912};
	float jaspar_phylofacts_pwm_151_matrix_row_2_[14] = { -9.264912, -9.264912, -9.264912, -0.015255,  0.152524, -0.382937, -0.064521, -0.095289, -0.485201, -9.264912, -9.264912,  1.514065, -9.264912, -9.264912};
	float jaspar_phylofacts_pwm_151_matrix_row_3_[14] = { -9.506074, -9.506074, -9.506074,  0.096376, -0.469968, -0.191283,  0.123042, -0.295634, -0.875374, -9.506074, -9.506074, -9.506074,  0.174332,  1.272903};
	float *jaspar_phylofacts_pwm_151_matrix[4] = { jaspar_phylofacts_pwm_151_matrix_row_0_, jaspar_phylofacts_pwm_151_matrix_row_1_, jaspar_phylofacts_pwm_151_matrix_row_2_, jaspar_phylofacts_pwm_151_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_151_ = {
/* accession        */ "PF0158",
/* name             */ "CCCNNNNNNAAGWT",
/* label            */ " PF0158	15.6163105114081	CCCNNNNNNAAGWT	Unknown	; MCS \"10.2\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCCNNNNNNAAGAT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_151_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.786423,
/* max_score        */ 13.081032,
/* threshold        */ 0.768,
/* info content     */ 15.608110,
/* base_counts      */ {2034, 2356, 1039, 1291},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 480
};

	float jaspar_phylofacts_pwm_152_matrix_row_0_[7] = { -10.997039, -10.997039, -10.997039, -10.997039,  1.272952,  1.272952,  0.576050};
	float jaspar_phylofacts_pwm_152_matrix_row_1_[7] = { -10.755877, -10.755877,  1.514114,  1.514114, -10.755877, -10.755877, -10.755877};
	float jaspar_phylofacts_pwm_152_matrix_row_2_[7] = { -10.755877,  1.514114, -10.755877, -10.755877, -10.755877, -10.755877,  0.824717};
	float jaspar_phylofacts_pwm_152_matrix_row_3_[7] = {  1.272952, -10.997039, -10.997039, -10.997039, -10.997039, -10.997039, -10.997039};
	float *jaspar_phylofacts_pwm_152_matrix[4] = { jaspar_phylofacts_pwm_152_matrix_row_0_, jaspar_phylofacts_pwm_152_matrix_row_1_, jaspar_phylofacts_pwm_152_matrix_row_2_, jaspar_phylofacts_pwm_152_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_152_ = {
/* accession        */ "PF0047",
/* name             */ "TGCCAAR",
/* label            */ " PF0047	13.0000101566652	TGCCAAR	Unknown	; MCS \"22.9\" ; jaspar \"Hox11-CTF1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"NF-1\" ; type \"phylogenetic\" ",
/* motif            */ "TGCCAAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_152_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.979271,
/* max_score        */ 9.185912,
/* threshold        */ 0.838,
/* info content     */ 12.998224,
/* base_counts      */ {5326, 4264, 3202, 2132},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2132
};

	float jaspar_phylofacts_pwm_153_matrix_row_0_[8] = { -11.131974, -11.131974,  1.272953, -11.131974, -0.227836, -11.131974, -11.131974,  1.272953};
	float jaspar_phylofacts_pwm_153_matrix_row_1_[8] = { -10.890812, -10.890812, -10.890812,  1.206567, -10.890812, -10.890812,  1.514115, -10.890812};
	float jaspar_phylofacts_pwm_153_matrix_row_2_[8] = { -10.890812,  1.514115, -10.890812, -10.890812,  1.261865, -10.890812, -10.890812, -10.890812};
	float jaspar_phylofacts_pwm_153_matrix_row_3_[8] = {  1.272953, -11.131974, -11.131974, -0.055989, -11.131974,  1.272953, -11.131974, -11.131974};
	float *jaspar_phylofacts_pwm_153_matrix[4] = { jaspar_phylofacts_pwm_153_matrix_row_0_, jaspar_phylofacts_pwm_153_matrix_row_1_, jaspar_phylofacts_pwm_153_matrix_row_2_, jaspar_phylofacts_pwm_153_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_153_ = {
/* accession        */ "PF0009",
/* name             */ "TGAYRTCA",
/* label            */ " PF0009	14.4006449074309	TGAYRTCA	Unknown	; MCS \"55.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"ATF3\" ; type \"phylogenetic\" ",
/* motif            */ "TGACGTCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_153_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.055794,
/* max_score        */ 10.588476,
/* threshold        */ 0.815,
/* info content     */ 14.398908,
/* base_counts      */ {5424, 4234, 4336, 5526},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2440
};

	float jaspar_phylofacts_pwm_154_matrix_row_0_[7] = { -10.702460, -10.702460,  1.272947, -10.702460, -10.702460,  1.272947,  1.272947};
	float jaspar_phylofacts_pwm_154_matrix_row_1_[7] = {  0.739666,  1.514109, -10.461298, -10.461298, -10.461298, -10.461298, -10.461298};
	float jaspar_phylofacts_pwm_154_matrix_row_2_[7] = { -10.461298, -10.461298, -10.461298, -10.461298, -10.461298, -10.461298, -10.461298};
	float jaspar_phylofacts_pwm_154_matrix_row_3_[7] = {  0.654992, -10.702460, -10.702460,  1.272947,  1.272947, -10.702460, -10.702460};
	float *jaspar_phylofacts_pwm_154_matrix[4] = { jaspar_phylofacts_pwm_154_matrix_row_0_, jaspar_phylofacts_pwm_154_matrix_row_1_, jaspar_phylofacts_pwm_154_matrix_row_2_, jaspar_phylofacts_pwm_154_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_154_ = {
/* accession        */ "PF0058",
/* name             */ "YCATTAA",
/* label            */ " PF0058	13.0044028015871	YCATTAA	Unknown	; MCS \"20.3\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"IPF1(*)\" ; type \"phylogenetic\" ",
/* motif            */ "TCATTAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_154_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -74.917221,
/* max_score        */ 8.618509,
/* threshold        */ 0.838,
/* info content     */ 13.002057,
/* base_counts      */ {4764, 2320, 0, 4032},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1588
};

	float jaspar_phylofacts_pwm_155_matrix_row_0_[11] = { -9.761989, -9.761989, -9.761989, -1.205382, -1.395386,  1.272917, -2.517047, -9.761989, -9.761989, -9.761989, -9.761989};
	float jaspar_phylofacts_pwm_155_matrix_row_1_[11] = { -9.520826,  1.514079, -9.520826,  0.127833,  0.913319, -9.520826,  1.090722, -9.520826, -9.520826,  1.514079,  1.514079};
	float jaspar_phylofacts_pwm_155_matrix_row_2_[11] = {  1.514079, -9.520826,  1.514079,  0.569182,  0.445683, -9.520826, -0.461193, -9.520826, -9.520826, -9.520826, -9.520826};
	float jaspar_phylofacts_pwm_155_matrix_row_3_[11] = { -9.761989, -9.761989, -9.761989, -0.009266, -1.978348, -9.761989, -0.420532,  1.272917,  1.272917, -9.761989, -9.761989};
	float *jaspar_phylofacts_pwm_155_matrix[4] = { jaspar_phylofacts_pwm_155_matrix_row_0_, jaspar_phylofacts_pwm_155_matrix_row_1_, jaspar_phylofacts_pwm_155_matrix_row_2_, jaspar_phylofacts_pwm_155_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_155_ = {
/* accession        */ "PF0048",
/* name             */ "GCGNNANTTCC",
/* label            */ " PF0048	17.335559283646	GCGNNANTTCC	Unknown	; MCS \"22.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"C-REL(*)\" ; type \"phylogenetic\" ",
/* motif            */ "GCGNCACTTCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_155_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.796684,
/* max_score        */ 13.962372,
/* threshold        */ 0.782,
/* info content     */ 17.328638,
/* base_counts      */ {729, 2761, 1780, 1550},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 620
};

	float jaspar_phylofacts_pwm_156_matrix_row_0_[10] = { -9.075336, -9.075336, -9.075336, -9.075336, -9.075336,  0.985198, -0.051205, -9.075336, -9.075336, -9.075336};
	float jaspar_phylofacts_pwm_156_matrix_row_1_[10] = { -8.834174,  1.514032, -8.834174,  1.127630,  1.514032,  0.127833, -0.762955, -8.834174, -8.834174, -8.834174};
	float jaspar_phylofacts_pwm_156_matrix_row_2_[10] = {  1.514032, -8.834174,  1.514032,  0.376267, -8.834174, -8.834174, -1.137507, -8.834174, -8.834174, -8.834174};
	float jaspar_phylofacts_pwm_156_matrix_row_3_[10] = { -9.075336, -9.075336, -9.075336, -9.075336, -9.075336, -9.075336,  0.694677,  1.272869,  1.272869,  1.272869};
	float *jaspar_phylofacts_pwm_156_matrix[4] = { jaspar_phylofacts_pwm_156_matrix_row_0_, jaspar_phylofacts_pwm_156_matrix_row_1_, jaspar_phylofacts_pwm_156_matrix_row_2_, jaspar_phylofacts_pwm_156_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_156_ = {
/* accession        */ "PF0164",
/* name             */ "GCGSCMNTTT",
/* label            */ " PF0164	16.7009381696348	GCGSCMNTTT	Unknown	; MCS \"9.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GCGCCATTTT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_156_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.815521,
/* max_score        */ 12.682240,
/* threshold        */ 0.787,
/* info content     */ 16.687782,
/* base_counts      */ {317, 946, 746, 1111},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 312
};

	float jaspar_phylofacts_pwm_157_matrix_row_0_[10] = { -10.093823, -10.093823,  1.272931, -0.834598, -0.400995, -10.093823,  0.858753, -10.093823, -10.093823,  1.272931};
	float jaspar_phylofacts_pwm_157_matrix_row_1_[10] = { -9.852661, -9.852661, -9.852661,  0.660619, -0.419098,  1.076886, -9.852661, -9.852661,  1.514093, -9.852661};
	float jaspar_phylofacts_pwm_157_matrix_row_2_[10] = { -9.852661,  1.514093, -9.852661,  0.181898,  0.080433, -9.852661,  0.432716,  1.514093, -9.852661, -9.852661};
	float jaspar_phylofacts_pwm_157_matrix_row_3_[10] = {  1.272931, -10.093823, -10.093823, -0.394842,  0.427576,  0.234964, -10.093823, -10.093823, -10.093823, -10.093823};
	float *jaspar_phylofacts_pwm_157_matrix[4] = { jaspar_phylofacts_pwm_157_matrix_row_0_, jaspar_phylofacts_pwm_157_matrix_row_1_, jaspar_phylofacts_pwm_157_matrix_row_2_, jaspar_phylofacts_pwm_157_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_157_ = {
/* accession        */ "PF0067",
/* name             */ "TGANNYRGCA",
/* label            */ " PF0067	14.4099898423089	TGANNYRGCA	Unknown	; MCS \"17.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"TCF11/MAFG\" ; type \"phylogenetic\" ",
/* motif            */ "TGANNCAGCA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 10,
/* matrixname       */ jaspar_phylofacts_pwm_157_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.004288,
/* max_score        */ 11.384906,
/* threshold        */ 0.798,
/* info content     */ 14.405458,
/* base_counts      */ {2566, 1915, 2455, 1704},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 864
};

	float jaspar_phylofacts_pwm_158_matrix_row_0_[8] = { -9.970824, -9.970824,  1.272926, -9.970824, -9.970824, -9.970824, -9.970824, -9.970824};
	float jaspar_phylofacts_pwm_158_matrix_row_1_[8] = { -9.729663, -9.729663, -9.729663,  1.514089,  1.514089, -9.729663, -9.729663, -9.729663};
	float jaspar_phylofacts_pwm_158_matrix_row_2_[8] = { -9.729663,  1.514089, -9.729663, -9.729663, -9.729663, -9.729663, -9.729663,  1.514089};
	float jaspar_phylofacts_pwm_158_matrix_row_3_[8] = {  1.272926, -9.970824, -9.970824, -9.970824, -9.970824,  1.272926,  1.272926, -9.970824};
	float *jaspar_phylofacts_pwm_158_matrix[4] = { jaspar_phylofacts_pwm_158_matrix_row_0_, jaspar_phylofacts_pwm_158_matrix_row_1_, jaspar_phylofacts_pwm_158_matrix_row_2_, jaspar_phylofacts_pwm_158_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_158_ = {
/* accession        */ "PF0038",
/* name             */ "TGACCTTG",
/* label            */ " PF0038	16	TGACCTTG	Unknown	; MCS \"25.2\" ; jaspar \"RORA\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"ERRALPHA\" ; type \"phylogenetic\" ",
/* motif            */ "TGACCTTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_158_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -79.766594,
/* max_score        */ 11.148060,
/* threshold        */ 0.801,
/* info content     */ 15.994452,
/* base_counts      */ {764, 1528, 1528, 2292},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 764
};

	float jaspar_phylofacts_pwm_159_matrix_row_0_[8] = { -10.283058, -10.283058, -10.283058, -10.283058,  0.615550, -10.283058,  1.272937,  1.272937};
	float jaspar_phylofacts_pwm_159_matrix_row_1_[8] = { -10.041896, -10.041896,  1.514099,  0.690166, -10.041896, -10.041896, -10.041896, -10.041896};
	float jaspar_phylofacts_pwm_159_matrix_row_2_[8] = { -10.041896, -10.041896, -10.041896, -10.041896,  0.783885,  1.514099, -10.041896, -10.041896};
	float jaspar_phylofacts_pwm_159_matrix_row_3_[8] = {  1.272937,  1.272937, -10.283058,  0.695449, -10.283058, -10.283058, -10.283058, -10.283058};
	float *jaspar_phylofacts_pwm_159_matrix[4] = { jaspar_phylofacts_pwm_159_matrix_row_0_, jaspar_phylofacts_pwm_159_matrix_row_1_, jaspar_phylofacts_pwm_159_matrix_row_2_, jaspar_phylofacts_pwm_159_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_159_ = {
/* accession        */ "PF0072",
/* name             */ "TTCYRGAA",
/* label            */ " PF0072	14.01182656851	TTCYRGAA	Unknown	; MCS \"17.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTCTAGAA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_159_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.264465,
/* max_score        */ 9.599279,
/* threshold        */ 0.818,
/* info content     */ 14.008047,
/* base_counts      */ {2629, 1502, 1547, 2674},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1044
};

	float jaspar_phylofacts_pwm_160_matrix_row_0_[7] = {  0.493865, -10.770612, -10.770612,  1.272948,  1.272948,  1.272948, -10.770612};
	float jaspar_phylofacts_pwm_160_matrix_row_1_[7] = { -10.529449, -10.529449, -10.529449, -10.529449, -10.529449, -10.529449, -10.529449};
	float jaspar_phylofacts_pwm_160_matrix_row_2_[7] = { -10.529449, -10.529449,  1.514110, -10.529449, -10.529449, -10.529449, -10.529449};
	float jaspar_phylofacts_pwm_160_matrix_row_3_[7] = {  0.658943,  1.272948, -10.770612, -10.770612, -10.770612, -10.770612,  1.272948};
	float *jaspar_phylofacts_pwm_160_matrix[4] = { jaspar_phylofacts_pwm_160_matrix_row_0_, jaspar_phylofacts_pwm_160_matrix_row_1_, jaspar_phylofacts_pwm_160_matrix_row_2_, jaspar_phylofacts_pwm_160_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_160_ = {
/* accession        */ "PF0174",
/* name             */ "WTGAAAT",
/* label            */ " PF0174	13.0048977287358	WTGAAAT	Unknown	; MCS \"8.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTGAAAT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 7,
/* matrixname       */ jaspar_phylofacts_pwm_160_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.153122,
/* max_score        */ 8.537794,
/* threshold        */ 0.838,
/* info content     */ 13.002696,
/* base_counts      */ {5880, 0, 1700, 4320},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1700
};

	float jaspar_phylofacts_pwm_161_matrix_row_0_[6] = { -11.053571,  1.272952,  1.272952, -11.053571, -11.053571,  1.272952};
	float jaspar_phylofacts_pwm_161_matrix_row_1_[6] = { -10.812408, -10.812408, -10.812408, -10.812408, -10.812408, -10.812408};
	float jaspar_phylofacts_pwm_161_matrix_row_2_[6] = { -10.812408, -10.812408, -10.812408, -10.812408, -10.812408, -10.812408};
	float jaspar_phylofacts_pwm_161_matrix_row_3_[6] = {  1.272952, -11.053571, -11.053571,  1.272952,  1.272952, -11.053571};
	float *jaspar_phylofacts_pwm_161_matrix[4] = { jaspar_phylofacts_pwm_161_matrix_row_0_, jaspar_phylofacts_pwm_161_matrix_row_1_, jaspar_phylofacts_pwm_161_matrix_row_2_, jaspar_phylofacts_pwm_161_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_161_ = {
/* accession        */ "PF0023",
/* name             */ "TAATTA",
/* label            */ " PF0023	12	TAATTA	Unknown	; MCS \"37.3\" ; jaspar \"TCF1\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"CHX10\" ; type \"phylogenetic\" ",
/* motif            */ "TAATTA",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 6,
/* matrixname       */ jaspar_phylofacts_pwm_161_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -66.321426,
/* max_score        */ 7.637715,
/* threshold        */ 0.861,
/* info content     */ 11.998466,
/* base_counts      */ {6768, 0, 0, 6768},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2256
};

	float jaspar_phylofacts_pwm_162_matrix_row_0_[9] = { -10.976186, -10.976186, -10.976186,  1.272951, -10.976186, -1.327526, -10.976186, -10.976186, -10.976186};
	float jaspar_phylofacts_pwm_162_matrix_row_1_[9] = { -10.735023,  1.514113,  1.514113, -10.735023, -10.735023,  0.628253, -10.735023, -10.735023, -10.735023};
	float jaspar_phylofacts_pwm_162_matrix_row_2_[9] = {  1.514113, -10.735023, -10.735023, -10.735023, -10.735023, -0.396480, -10.735023, -10.735023,  1.514113};
	float jaspar_phylofacts_pwm_162_matrix_row_3_[9] = { -10.976186, -10.976186, -10.976186, -10.976186,  1.272951,  0.266256,  1.272951,  1.272951, -10.976186};
	float *jaspar_phylofacts_pwm_162_matrix[4] = { jaspar_phylofacts_pwm_162_matrix_row_0_, jaspar_phylofacts_pwm_162_matrix_row_1_, jaspar_phylofacts_pwm_162_matrix_row_2_, jaspar_phylofacts_pwm_162_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_162_ = {
/* accession        */ "PF0010",
/* name             */ "GCCATNTTG",
/* label            */ " PF0010	16.25583700188	GCCATNTTG	Unknown	; MCS \"54.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"YY1\" ; type \"phylogenetic\" ",
/* motif            */ "GCCATYTTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_162_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.137016,
/* max_score        */ 11.776511,
/* threshold        */ 0.794,
/* info content     */ 16.253628,
/* base_counts      */ {2243, 5037, 4485, 7027},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 2088
};

	float jaspar_phylofacts_pwm_163_matrix_row_0_[9] = { -10.432264,  1.272941, -10.432264, -10.432264, -1.783868,  0.963014,  1.272941,  1.272941, -10.432264};
	float jaspar_phylofacts_pwm_163_matrix_row_1_[9] = {  0.364736, -10.191103, -10.191103, -10.191103,  1.107404, -10.191103, -10.191103, -10.191103, -10.191103};
	float jaspar_phylofacts_pwm_163_matrix_row_2_[9] = { -10.191103, -10.191103, -10.191103,  1.514103, -0.688018, -10.191103, -10.191103, -10.191103, -10.191103};
	float jaspar_phylofacts_pwm_163_matrix_row_3_[9] = {  0.891931, -10.432264,  1.272941, -10.432264, -0.461072, -0.049411, -10.432264, -10.432264,  1.272941};
	float *jaspar_phylofacts_pwm_163_matrix[4] = { jaspar_phylofacts_pwm_163_matrix_row_0_, jaspar_phylofacts_pwm_163_matrix_row_1_, jaspar_phylofacts_pwm_163_matrix_row_2_, jaspar_phylofacts_pwm_163_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_163_ = {
/* accession        */ "PF0044",
/* name             */ "YATGNWAAT",
/* label            */ " PF0044	14.8716187921438	YATGNWAAT	Unknown	; MCS \"23.5\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"OCT-X\" ; type \"phylogenetic\" ",
/* motif            */ "TATGCAAAT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_163_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.000824,
/* max_score        */ 10.841156,
/* threshold        */ 0.803,
/* info content     */ 14.868268,
/* base_counts      */ {4582, 1191, 1346, 3789},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1212
};

	float jaspar_phylofacts_pwm_164_matrix_row_0_[11] = { -9.149435, -9.149435,  1.272876, -9.149435,  0.209412, -0.855135, -9.149435,  1.272876, -9.149435,  1.272876,  0.549547};
	float jaspar_phylofacts_pwm_164_matrix_row_1_[11] = { -1.211606, -8.908273, -8.908273,  1.514038,  0.014519, -0.806292,  1.404041, -8.908273,  1.514038, -8.908273, -8.908273};
	float jaspar_phylofacts_pwm_164_matrix_row_2_[11] = { -8.908273,  1.514038, -8.908273, -8.908273,  0.379121, -1.529889, -8.908273, -8.908273, -8.908273, -8.908273,  0.850247};
	float jaspar_phylofacts_pwm_164_matrix_row_3_[11] = {  1.205160, -9.149435, -9.149435, -9.149435, -0.933076,  0.965164, -0.988631, -9.149435, -9.149435, -9.149435, -9.149435};
	float *jaspar_phylofacts_pwm_164_matrix[4] = { jaspar_phylofacts_pwm_164_matrix_row_0_, jaspar_phylofacts_pwm_164_matrix_row_1_, jaspar_phylofacts_pwm_164_matrix_row_2_, jaspar_phylofacts_pwm_164_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_164_ = {
/* accession        */ "PF0149",
/* name             */ "YGACNNYACAR",
/* label            */ " PF0149	17.0504660691425	YGACNNYACAR	Unknown	; MCS \"10.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TGACNTCACAG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_164_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.807884,
/* max_score        */ 13.164478,
/* threshold        */ 0.782,
/* info content     */ 17.038416,
/* base_counts      */ {1327, 1103, 633, 633},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 336
};

	float jaspar_phylofacts_pwm_165_matrix_row_0_[8] = {  1.272903, -9.506074, -9.506074, -9.506074, -9.506074, -9.506074, -9.506074, -9.506074};
	float jaspar_phylofacts_pwm_165_matrix_row_1_[8] = { -9.264912,  1.514065,  1.514065, -9.264912, -9.264912, -9.264912, -9.264912, -9.264912};
	float jaspar_phylofacts_pwm_165_matrix_row_2_[8] = { -9.264912, -9.264912, -9.264912, -9.264912,  1.514065, -9.264912, -9.264912,  1.514065};
	float jaspar_phylofacts_pwm_165_matrix_row_3_[8] = { -9.506074, -9.506074, -9.506074,  1.272903, -9.506074,  1.272903,  1.272903, -9.506074};
	float *jaspar_phylofacts_pwm_165_matrix[4] = { jaspar_phylofacts_pwm_165_matrix_row_0_, jaspar_phylofacts_pwm_165_matrix_row_1_, jaspar_phylofacts_pwm_165_matrix_row_2_, jaspar_phylofacts_pwm_165_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_165_ = {
/* accession        */ "PF0057",
/* name             */ "ACCTGTTG",
/* label            */ " PF0057	16	ACCTGTTG	Unknown	; MCS \"20.7\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "ACCTGTTG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 8,
/* matrixname       */ jaspar_phylofacts_pwm_165_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.048592,
/* max_score        */ 11.147875,
/* threshold        */ 0.801,
/* info content     */ 15.991503,
/* base_counts      */ {480, 960, 960, 1440},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 480
};

	float jaspar_phylofacts_pwm_166_matrix_row_0_[12] = { -9.207248, -9.207248, -9.207248, -0.670056, -0.964228, -0.412271,  1.050447, -9.207248, -9.207248, -9.207248, -9.207248, -9.207248};
	float jaspar_phylofacts_pwm_166_matrix_row_1_[12] = { -8.966085, -8.966085,  1.514043, -0.576499, -8.966085, -0.300300, -8.966085,  1.514043, -8.966085, -8.966085,  1.222619,  1.322698};
	float jaspar_phylofacts_pwm_166_matrix_row_2_[12] = {  1.514043,  1.514043, -8.966085,  0.962143,  1.401168,  0.917250, -8.966085, -8.966085, -8.966085, -8.966085, -8.966085, -0.233620};
	float jaspar_phylofacts_pwm_166_matrix_row_3_[12] = { -9.207248, -9.207248, -9.207248, -0.576547, -9.207248, -1.018281, -0.339257, -9.207248,  1.272881,  1.272881, -0.102157, -9.207248};
	float *jaspar_phylofacts_pwm_166_matrix[4] = { jaspar_phylofacts_pwm_166_matrix_row_0_, jaspar_phylofacts_pwm_166_matrix_row_1_, jaspar_phylofacts_pwm_166_matrix_row_2_, jaspar_phylofacts_pwm_166_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_166_ = {
/* accession        */ "PF0118",
/* name             */ "GGCNRNWCTTYS",
/* label            */ " PF0118	17.9681171046028	GGCNRNWCTTYS	Unknown	; MCS \"12.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCGGGACTTCC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_166_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.519646,
/* max_score        */ 15.478262,
/* threshold        */ 0.778,
/* info content     */ 17.955868,
/* base_counts      */ {440, 1374, 1493, 965},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 356
};

	float jaspar_phylofacts_pwm_167_matrix_row_0_[9] = { -9.742445, -9.742445, -9.742445,  1.272916, -9.742445, -9.742445,  1.272916,  1.272916, -9.742445};
	float jaspar_phylofacts_pwm_167_matrix_row_1_[9] = {  1.514078,  0.981651, -9.501283, -9.501283, -9.501283,  1.514078, -9.501283, -9.501283,  1.480631};
	float jaspar_phylofacts_pwm_167_matrix_row_2_[9] = { -9.501283, -9.501283, -9.501283, -9.501283,  1.514078, -9.501283, -9.501283, -9.501283, -9.501283};
	float jaspar_phylofacts_pwm_167_matrix_row_3_[9] = { -9.742445,  0.388218,  1.272916, -9.742445, -9.742445, -9.742445, -9.742445, -9.742445, -2.141043};
	float *jaspar_phylofacts_pwm_167_matrix[4] = { jaspar_phylofacts_pwm_167_matrix_row_0_, jaspar_phylofacts_pwm_167_matrix_row_1_, jaspar_phylofacts_pwm_167_matrix_row_2_, jaspar_phylofacts_pwm_167_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_167_ = {
/* accession        */ "PF0034",
/* name             */ "CYTAGCAAY",
/* label            */ " PF0034	16.8133306424575	CYTAGCAAY	Unknown	; MCS \"26.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "CCTAGCAAC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_167_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.682014,
/* max_score        */ 12.096184,
/* threshold        */ 0.791,
/* info content     */ 16.806166,
/* base_counts      */ {1824, 2161, 608, 879},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 608
};

	float jaspar_phylofacts_pwm_168_matrix_row_0_[11] = { -10.156622,  0.009269, -10.156622, -10.156622, -10.156622, -0.772244, -0.527505,  0.201153,  1.272933, -10.156622, -10.156622};
	float jaspar_phylofacts_pwm_168_matrix_row_1_[11] = {  0.816602, -0.675463, -9.915460, -9.915460, -9.915460, -0.340407, -0.073794,  0.123476, -9.915460, -9.915460, -9.915460};
	float jaspar_phylofacts_pwm_168_matrix_row_2_[11] = { -9.915460, -0.292943,  1.514095, -9.915460, -9.915460,  0.046344,  0.051050, -0.052742, -9.915460, -9.915460, -9.915460};
	float jaspar_phylofacts_pwm_168_matrix_row_3_[11] = {  0.584135,  0.454926, -10.156622,  1.272933,  1.272933,  0.546645,  0.353938, -0.336461, -10.156622,  1.272933,  1.272933};
	float *jaspar_phylofacts_pwm_168_matrix[4] = { jaspar_phylofacts_pwm_168_matrix_row_0_, jaspar_phylofacts_pwm_168_matrix_row_1_, jaspar_phylofacts_pwm_168_matrix_row_2_, jaspar_phylofacts_pwm_168_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_168_ = {
/* accession        */ "PF0170",
/* name             */ "YNGTTNNNATT",
/* label            */ " PF0170	13.5072984828633	YNGTTNNNATT	Unknown	; MCS \"9.1\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TNGTTNNNATT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_168_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -73.408028,
/* max_score        */ 10.252024,
/* threshold        */ 0.792,
/* info content     */ 13.503378,
/* base_counts      */ {1766, 1122, 1688, 5544},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 920
};

	float jaspar_phylofacts_pwm_169_matrix_row_0_[15] = { -9.616413, -2.930552, -9.616413, -9.616413, -9.616413, -2.812908, -1.791967,  0.564744,  0.545624,  0.781801,  1.272910, -2.707659,  1.272910, -9.616413, -9.616413};
	float jaspar_phylofacts_pwm_169_matrix_row_1_[15] = { -9.375251, -9.375251,  1.514072, -9.375251,  1.514072, -2.061364, -3.381290, -2.284341, -9.375251,  0.567505, -9.375251, -1.634152, -9.375251, -9.375251,  1.514072};
	float jaspar_phylofacts_pwm_169_matrix_row_2_[15] = {  1.483767,  1.499034, -9.375251, -9.375251, -9.375251, -9.375251,  0.562686, -2.822743, -1.473874, -9.375251, -9.375251,  1.448539, -9.375251,  1.514072, -9.375251};
	float jaspar_phylofacts_pwm_169_matrix_row_3_[15] = { -2.238029, -9.616413, -9.616413,  1.272910, -9.616413,  1.227101,  0.692573,  0.522186,  0.510258, -9.616413, -9.616413, -5.001293, -9.616413, -9.616413, -9.616413};
	float *jaspar_phylofacts_pwm_169_matrix[4] = { jaspar_phylofacts_pwm_169_matrix_row_0_, jaspar_phylofacts_pwm_169_matrix_row_1_, jaspar_phylofacts_pwm_169_matrix_row_2_, jaspar_phylofacts_pwm_169_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_169_ = {
/* accession        */ "PF0120",
/* name             */ "KRCTCNNNNMANAGC",
/* label            */ " PF0120	24.3028392363272	KRCTCNNNNMANAGC	Unknown	; MCS \"11.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "GGCTCTTWWAAGAGC",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 15,
/* matrixname       */ jaspar_phylofacts_pwm_169_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -126.119949,
/* max_score        */ 18.118200,
/* threshold        */ 0.805,
/* info content     */ 24.292534,
/* base_counts      */ {1975, 1870, 2327, 1868},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 536
};

	float jaspar_phylofacts_pwm_170_matrix_row_0_[14] = {  0.174312, -8.813010,  1.272841,  1.272841,  1.272841,  0.123025, -0.147224, -0.200325, -0.596652, -0.875278, -0.237359, -8.813010, -8.813010, -8.813010};
	float jaspar_phylofacts_pwm_170_matrix_row_1_[14] = { -8.571848,  0.487786, -8.571848, -8.571848, -8.571848, -0.228770, -0.500629, -8.571848,  0.127833,  1.037336,  0.510773, -8.571848,  1.514003, -8.571848};
	float jaspar_phylofacts_pwm_170_matrix_row_2_[14] = { -8.571848, -8.571848, -8.571848, -8.571848, -8.571848,  0.696856,  0.658393,  1.253732,  0.820897, -0.116318,  0.427895,  1.514003, -8.571848,  1.514003};
	float jaspar_phylofacts_pwm_170_matrix_row_3_[14] = {  0.867396,  0.829178, -8.813010, -8.813010, -8.813010, -1.434626, -0.336431, -8.813010, -1.071911, -1.434626, -1.316913, -8.813010, -8.813010, -8.813010};
	float *jaspar_phylofacts_pwm_170_matrix[4] = { jaspar_phylofacts_pwm_170_matrix_row_0_, jaspar_phylofacts_pwm_170_matrix_row_1_, jaspar_phylofacts_pwm_170_matrix_row_2_, jaspar_phylofacts_pwm_170_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_170_ = {
/* accession        */ "PF0126",
/* name             */ "WYAAANNRNNNGCG",
/* label            */ " PF0126	16.6746705013525	WYAAANNRNNNGCG	Unknown	; MCS \"11.4\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTAAARNGSCNGCG",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 14,
/* matrixname       */ jaspar_phylofacts_pwm_170_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.834641,
/* max_score        */ 15.035089,
/* threshold        */ 0.771,
/* info content     */ 16.658375,
/* base_counts      */ {1107, 697, 1121, 435},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 240
};

	float jaspar_phylofacts_pwm_171_matrix_row_0_[9] = { -8.329687, -8.329687, -8.329687, -8.329687,  0.645058, -8.329687,  1.272763,  1.272763, -8.329687};
	float jaspar_phylofacts_pwm_171_matrix_row_1_[9] = { -8.088525, -8.088525, -8.088525,  1.513925, -8.088525,  1.513925, -8.088525, -8.088525,  0.807242};
	float jaspar_phylofacts_pwm_171_matrix_row_2_[9] = { -8.088525, -8.088525,  1.513925, -8.088525, -8.088525, -8.088525, -8.088525, -8.088525, -8.088525};
	float jaspar_phylofacts_pwm_171_matrix_row_3_[9] = {  1.272763,  1.272763, -8.329687, -8.329687,  0.509735, -8.329687, -8.329687, -8.329687,  0.593105};
	float *jaspar_phylofacts_pwm_171_matrix[4] = { jaspar_phylofacts_pwm_171_matrix_row_0_, jaspar_phylofacts_pwm_171_matrix_row_1_, jaspar_phylofacts_pwm_171_matrix_row_2_, jaspar_phylofacts_pwm_171_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_171_ = {
/* accession        */ "PF0172",
/* name             */ "TTGCWCAAY",
/* label            */ " PF0172	16.003427466101	TTGCWCAAY	Unknown	; MCS \"9.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"C/EBPBETA\" ; type \"phylogenetic\" ",
/* motif            */ "TTGCACAAT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 9,
/* matrixname       */ jaspar_phylofacts_pwm_171_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -74.726028,
/* max_score        */ 11.085127,
/* threshold        */ 0.796,
/* info content     */ 15.977866,
/* base_counts      */ {375, 369, 148, 440},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 148
};

	float jaspar_phylofacts_pwm_172_matrix_row_0_[12] = { -8.726014,  0.261308,  1.272829, -8.726014, -8.726014,  0.422558, -0.150363, -0.026332,  0.777071, -8.726014, -8.726014, -8.726014};
	float jaspar_phylofacts_pwm_172_matrix_row_1_[12] = {  0.597769, -8.484852, -8.484852, -8.484852, -8.484852, -8.484852, -0.118249, -0.008272, -8.484852, -8.484852,  1.513991, -8.484852};
	float jaspar_phylofacts_pwm_172_matrix_row_2_[12] = { -8.484852, -8.484852, -8.484852, -8.484852, -8.484852, -8.484852,  0.127833, -0.834683,  0.574782,  1.513991, -8.484852, -8.484852};
	float jaspar_phylofacts_pwm_172_matrix_row_3_[12] = {  0.762034,  0.820870, -8.726014,  1.272829,  1.272829,  0.715518,  0.113408,  0.390126, -8.726014, -8.726014, -8.726014,  1.272829};
	float *jaspar_phylofacts_pwm_172_matrix[4] = { jaspar_phylofacts_pwm_172_matrix_row_0_, jaspar_phylofacts_pwm_172_matrix_row_1_, jaspar_phylofacts_pwm_172_matrix_row_2_, jaspar_phylofacts_pwm_172_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_172_ = {
/* accession        */ "PF0173",
/* name             */ "YWATTWNNRGCT",
/* label            */ " PF0173	16.3129279607441	YWATTWNNRGCT	Unknown	; MCS \"8.8\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"-\" ; type \"phylogenetic\" ",
/* motif            */ "TTATTTNNAGCT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 12,
/* matrixname       */ jaspar_phylofacts_pwm_172_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.762856,
/* max_score        */ 11.712750,
/* threshold        */ 0.780,
/* info content     */ 16.294436,
/* base_counts      */ {641, 399, 382, 1218},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 220
};

	float jaspar_phylofacts_pwm_173_matrix_row_0_[11] = {  0.678450, -8.688979, -8.688979, -8.688979,  1.272824,  0.712064,  0.393641, -0.500013,  1.272824, -8.688979, -8.688979};
	float jaspar_phylofacts_pwm_173_matrix_row_1_[11] = { -8.447818, -8.447818, -8.447818, -8.447818, -8.447818,  0.668322, -8.447818,  0.235060, -8.447818, -8.447818, -8.447818};
	float jaspar_phylofacts_pwm_173_matrix_row_2_[11] = {  0.711335,  1.513986, -8.447818, -8.447818, -8.447818, -8.447818, -8.447818,  0.284648, -8.447818, -8.447818, -8.447818};
	float jaspar_phylofacts_pwm_173_matrix_row_3_[11] = { -8.688979, -8.688979,  1.272824,  1.272824, -8.688979, -8.688979,  0.736553, -0.076294, -8.688979,  1.272824,  1.272824};
	float *jaspar_phylofacts_pwm_173_matrix[4] = { jaspar_phylofacts_pwm_173_matrix_row_0_, jaspar_phylofacts_pwm_173_matrix_row_1_, jaspar_phylofacts_pwm_173_matrix_row_2_, jaspar_phylofacts_pwm_173_matrix_row_3_};
	PWM jaspar_phylofacts_pwm_173_ = {
/* accession        */ "PF0084",
/* name             */ "RGTTAMWNATT",
/* label            */ " PF0084	17.0715221408565	RGTTAMWNATT	Unknown	; MCS \"15.0\" ; jaspar \"-\" ; medline \"15735639\" ; species \"-\" ; sysgroup \"mammals\" ; transfac \"HNF-1\" ; type \"phylogenetic\" ",
/* motif            */ "AGTTAATNATT",
/* library_name     */ "jaspar_phylofacts",
/* length           */ 11,
/* matrixname       */ jaspar_phylofacts_pwm_173_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.148643,
/* max_score        */ 11.595531,
/* threshold        */ 0.782,
/* info content     */ 17.051645,
/* base_counts      */ {786, 150, 369, 1027},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 212
};

PWM *jaspar_phylofacts[JASPAR_PHYLOFACTS_N] = { &jaspar_phylofacts_pwm_0_, &jaspar_phylofacts_pwm_1_, &jaspar_phylofacts_pwm_2_, &jaspar_phylofacts_pwm_3_, &jaspar_phylofacts_pwm_4_, &jaspar_phylofacts_pwm_5_, &jaspar_phylofacts_pwm_6_, &jaspar_phylofacts_pwm_7_, &jaspar_phylofacts_pwm_8_, &jaspar_phylofacts_pwm_9_, &jaspar_phylofacts_pwm_10_, &jaspar_phylofacts_pwm_11_, &jaspar_phylofacts_pwm_12_, &jaspar_phylofacts_pwm_13_, &jaspar_phylofacts_pwm_14_, &jaspar_phylofacts_pwm_15_, &jaspar_phylofacts_pwm_16_, &jaspar_phylofacts_pwm_17_, &jaspar_phylofacts_pwm_18_, &jaspar_phylofacts_pwm_19_, &jaspar_phylofacts_pwm_20_, &jaspar_phylofacts_pwm_21_, &jaspar_phylofacts_pwm_22_, &jaspar_phylofacts_pwm_23_, &jaspar_phylofacts_pwm_24_, &jaspar_phylofacts_pwm_25_, &jaspar_phylofacts_pwm_26_, &jaspar_phylofacts_pwm_27_, &jaspar_phylofacts_pwm_28_, &jaspar_phylofacts_pwm_29_, &jaspar_phylofacts_pwm_30_, &jaspar_phylofacts_pwm_31_, &jaspar_phylofacts_pwm_32_, &jaspar_phylofacts_pwm_33_, &jaspar_phylofacts_pwm_34_, &jaspar_phylofacts_pwm_35_, &jaspar_phylofacts_pwm_36_, &jaspar_phylofacts_pwm_37_, &jaspar_phylofacts_pwm_38_, &jaspar_phylofacts_pwm_39_, &jaspar_phylofacts_pwm_40_, &jaspar_phylofacts_pwm_41_, &jaspar_phylofacts_pwm_42_, &jaspar_phylofacts_pwm_43_, &jaspar_phylofacts_pwm_44_, &jaspar_phylofacts_pwm_45_, &jaspar_phylofacts_pwm_46_, &jaspar_phylofacts_pwm_47_, &jaspar_phylofacts_pwm_48_, &jaspar_phylofacts_pwm_49_, &jaspar_phylofacts_pwm_50_, &jaspar_phylofacts_pwm_51_, &jaspar_phylofacts_pwm_52_, &jaspar_phylofacts_pwm_53_, &jaspar_phylofacts_pwm_54_, &jaspar_phylofacts_pwm_55_, &jaspar_phylofacts_pwm_56_, &jaspar_phylofacts_pwm_57_, &jaspar_phylofacts_pwm_58_, &jaspar_phylofacts_pwm_59_, &jaspar_phylofacts_pwm_60_, &jaspar_phylofacts_pwm_61_, &jaspar_phylofacts_pwm_62_, &jaspar_phylofacts_pwm_63_, &jaspar_phylofacts_pwm_64_, &jaspar_phylofacts_pwm_65_, &jaspar_phylofacts_pwm_66_, &jaspar_phylofacts_pwm_67_, &jaspar_phylofacts_pwm_68_, &jaspar_phylofacts_pwm_69_, &jaspar_phylofacts_pwm_70_, &jaspar_phylofacts_pwm_71_, &jaspar_phylofacts_pwm_72_, &jaspar_phylofacts_pwm_73_, &jaspar_phylofacts_pwm_74_, &jaspar_phylofacts_pwm_75_, &jaspar_phylofacts_pwm_76_, &jaspar_phylofacts_pwm_77_, &jaspar_phylofacts_pwm_78_, &jaspar_phylofacts_pwm_79_, &jaspar_phylofacts_pwm_80_, &jaspar_phylofacts_pwm_81_, &jaspar_phylofacts_pwm_82_, &jaspar_phylofacts_pwm_83_, &jaspar_phylofacts_pwm_84_, &jaspar_phylofacts_pwm_85_, &jaspar_phylofacts_pwm_86_, &jaspar_phylofacts_pwm_87_, &jaspar_phylofacts_pwm_88_, &jaspar_phylofacts_pwm_89_, &jaspar_phylofacts_pwm_90_, &jaspar_phylofacts_pwm_91_, &jaspar_phylofacts_pwm_92_, &jaspar_phylofacts_pwm_93_, &jaspar_phylofacts_pwm_94_, &jaspar_phylofacts_pwm_95_, &jaspar_phylofacts_pwm_96_, &jaspar_phylofacts_pwm_97_, &jaspar_phylofacts_pwm_98_, &jaspar_phylofacts_pwm_99_, &jaspar_phylofacts_pwm_100_, &jaspar_phylofacts_pwm_101_, &jaspar_phylofacts_pwm_102_, &jaspar_phylofacts_pwm_103_, &jaspar_phylofacts_pwm_104_, &jaspar_phylofacts_pwm_105_, &jaspar_phylofacts_pwm_106_, &jaspar_phylofacts_pwm_107_, &jaspar_phylofacts_pwm_108_, &jaspar_phylofacts_pwm_109_, &jaspar_phylofacts_pwm_110_, &jaspar_phylofacts_pwm_111_, &jaspar_phylofacts_pwm_112_, &jaspar_phylofacts_pwm_113_, &jaspar_phylofacts_pwm_114_, &jaspar_phylofacts_pwm_115_, &jaspar_phylofacts_pwm_116_, &jaspar_phylofacts_pwm_117_, &jaspar_phylofacts_pwm_118_, &jaspar_phylofacts_pwm_119_, &jaspar_phylofacts_pwm_120_, &jaspar_phylofacts_pwm_121_, &jaspar_phylofacts_pwm_122_, &jaspar_phylofacts_pwm_123_, &jaspar_phylofacts_pwm_124_, &jaspar_phylofacts_pwm_125_, &jaspar_phylofacts_pwm_126_, &jaspar_phylofacts_pwm_127_, &jaspar_phylofacts_pwm_128_, &jaspar_phylofacts_pwm_129_, &jaspar_phylofacts_pwm_130_, &jaspar_phylofacts_pwm_131_, &jaspar_phylofacts_pwm_132_, &jaspar_phylofacts_pwm_133_, &jaspar_phylofacts_pwm_134_, &jaspar_phylofacts_pwm_135_, &jaspar_phylofacts_pwm_136_, &jaspar_phylofacts_pwm_137_, &jaspar_phylofacts_pwm_138_, &jaspar_phylofacts_pwm_139_, &jaspar_phylofacts_pwm_140_, &jaspar_phylofacts_pwm_141_, &jaspar_phylofacts_pwm_142_, &jaspar_phylofacts_pwm_143_, &jaspar_phylofacts_pwm_144_, &jaspar_phylofacts_pwm_145_, &jaspar_phylofacts_pwm_146_, &jaspar_phylofacts_pwm_147_, &jaspar_phylofacts_pwm_148_, &jaspar_phylofacts_pwm_149_, &jaspar_phylofacts_pwm_150_, &jaspar_phylofacts_pwm_151_, &jaspar_phylofacts_pwm_152_, &jaspar_phylofacts_pwm_153_, &jaspar_phylofacts_pwm_154_, &jaspar_phylofacts_pwm_155_, &jaspar_phylofacts_pwm_156_, &jaspar_phylofacts_pwm_157_, &jaspar_phylofacts_pwm_158_, &jaspar_phylofacts_pwm_159_, &jaspar_phylofacts_pwm_160_, &jaspar_phylofacts_pwm_161_, &jaspar_phylofacts_pwm_162_, &jaspar_phylofacts_pwm_163_, &jaspar_phylofacts_pwm_164_, &jaspar_phylofacts_pwm_165_, &jaspar_phylofacts_pwm_166_, &jaspar_phylofacts_pwm_167_, &jaspar_phylofacts_pwm_168_, &jaspar_phylofacts_pwm_169_, &jaspar_phylofacts_pwm_170_, &jaspar_phylofacts_pwm_171_, &jaspar_phylofacts_pwm_172_, &jaspar_phylofacts_pwm_173_};
extern PWM *jaspar_phylofacts[JASPAR_PHYLOFACTS_N];
