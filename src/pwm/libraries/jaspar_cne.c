#include "pwm.h"
#include "jaspar_cne.h"
/* created by ./build_library_code using JASPAR_CNE_2008.wmx on Tue Mar  3 16:19:03 2009
*/
	float jaspar_cne_pwm_0_matrix_row_0_[22] = { -0.135781, -2.524635, -1.836451, -1.432645,  1.249831,  1.249831, -7.139756, -2.524635,  1.049211, -0.135781, -0.336250,  0.410380, -7.139756, -7.139756, -7.139756,  0.723896,  0.931463, -0.336250, -0.048846,  0.962225, -0.741161, -0.231001};
	float jaspar_cne_pwm_0_matrix_row_1_[22] = { -2.283473, -0.681987,  1.420393, -6.898593, -6.898593, -2.283473, -6.898593, -6.898593, -6.898593, -2.283473,  1.262210, -6.898593, -2.283473,  0.415293,  1.002784, -0.346085, -2.283473, -0.904632, -1.595289, -6.898593, -0.346085,  1.074217};
	float jaspar_cne_pwm_0_matrix_row_2_[22] = { -0.904632,  1.317765, -6.898593, -6.898593, -6.898593, -6.898593, -2.283473, -0.499998, -0.095088,  1.172625, -2.283473, -6.898593,  0.798074, -2.283473, -1.595289, -2.283473, -0.499998, -2.283473, -6.898593, -1.595289,  0.798074, -0.904632};
	float jaspar_cne_pwm_0_matrix_row_3_[22] = {  0.833055, -1.836451, -1.836451,  1.203322, -2.524635, -7.139756,  1.249831,  1.103264, -7.139756, -2.524635, -7.139756,  0.723896,  0.556912,  0.833055,  0.238628, -0.135781, -0.741161,  0.899724,  0.899724, -0.231001, -0.231001, -1.836451};
	float *jaspar_cne_pwm_0_matrix[4] = { jaspar_cne_pwm_0_matrix_row_0_, jaspar_cne_pwm_0_matrix_row_1_, jaspar_cne_pwm_0_matrix_row_2_, jaspar_cne_pwm_0_matrix_row_3_};
	PWM jaspar_cne_pwm_0_ = {
/* accession        */ "CN0069",
/* name             */ "LM69",
/* label            */ " CN0069	23.3889916959147	LM69	unknown	; consensus \"WGCTAATTRRMTKYYWAWTANM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCTAATTAGCTKTCAATTANC",
/* library_name     */ "jaspar_cne",
/* length           */ 22,
/* matrixname       */ jaspar_cne_pwm_0_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -115.029198,
/* max_score        */ 22.758472,
/* threshold        */ 0.862,
/* info content     */ 23.307047,
/* base_counts      */ {309, 177, 151, 353},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 45
};

	float jaspar_cne_pwm_1_matrix_row_0_[14] = { -7.204238, -7.204238,  1.064750, -7.204238,  1.272341, -7.204238, -7.204238,  1.229790,  1.272341, -7.204238, -7.204238,  1.272341, -7.204238,  0.291859};
	float jaspar_cne_pwm_1_matrix_row_1_[14] = { -6.963077, -6.963077, -0.969115,  1.513503, -6.963077, -6.963077, -0.746470, -1.659772, -6.963077, -6.963077, -0.746470, -6.963077, -6.963077,  0.974655};
	float jaspar_cne_pwm_1_matrix_row_2_[14] = { -6.963077,  1.513503, -2.347956, -6.963077, -6.963077,  1.513503, -6.963077, -6.963077, -6.963077, -6.963077,  0.127833, -6.963077,  1.513503, -1.659772};
	float jaspar_cne_pwm_1_matrix_row_3_[14] = {  1.272341, -7.204238, -1.210277, -7.204238, -7.204238, -7.204238,  1.162364, -7.204238, -7.204238,  1.272341,  0.835241, -7.204238, -7.204238, -7.204238};
	float *jaspar_cne_pwm_1_matrix[4] = { jaspar_cne_pwm_1_matrix_row_0_, jaspar_cne_pwm_1_matrix_row_1_, jaspar_cne_pwm_1_matrix_row_2_, jaspar_cne_pwm_1_matrix_row_3_};
	PWM jaspar_cne_pwm_1_ = {
/* accession        */ "CN0057",
/* name             */ "LM57",
/* label            */ " CN0057	23.8882588586762	LM57	unknown	; consensus \"TGACAGTAATTAGM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGACAGTAATTAGC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_1_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.003059,
/* max_score        */ 17.682516,
/* threshold        */ 0.793,
/* info content     */ 23.790733,
/* base_counts      */ {247, 92, 159, 174},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_2_matrix_row_0_[20] = { -1.210277, -2.589118, -2.589118, -7.204238, -7.204238,  1.185349, -1.900934, -2.589118, -2.589118, -7.204238,  1.138839,  1.251292,  1.162364, -0.400733, -1.497128,  0.866980,  1.064750,  0.984728, -0.518378,  0.109648};
	float jaspar_cne_pwm_2_matrix_row_1_[20] = { -2.347956, -0.564481,  0.415307, -2.347956,  1.426511, -0.969115, -6.963077, -2.347956, -6.963077, -0.969115, -1.659772, -2.347956, -6.963077, -6.963077, -0.969115, -0.564481, -1.255966, -1.659772, -1.255966, -6.963077};
	float jaspar_cne_pwm_2_matrix_row_2_[20] = { -2.347956,  0.778023, -6.963077, -1.659772, -1.659772, -6.963077, -6.963077, -6.963077, -6.963077,  1.403526, -0.969115, -6.963077, -1.659772,  1.305912,  1.305912, -0.564481, -0.746470, -0.410569, -1.255966,  0.638326};
	float jaspar_cne_pwm_2_matrix_row_3_[20] = {  1.138839,  0.291859,  0.835241,  1.207816, -1.900934, -7.204238,  1.229790,  1.229790,  1.251292, -2.589118, -7.204238, -7.204238, -1.497128, -7.204238, -1.900934, -1.210277, -2.589118, -1.497128,  0.927586, -0.033350};
	float *jaspar_cne_pwm_2_matrix[4] = { jaspar_cne_pwm_2_matrix_row_0_, jaspar_cne_pwm_2_matrix_row_1_, jaspar_cne_pwm_2_matrix_row_2_, jaspar_cne_pwm_2_matrix_row_3_};
	PWM jaspar_cne_pwm_2_ = {
/* accession        */ "CN0189",
/* name             */ "LM189",
/* label            */ " CN0189	24.709840615613	LM189	unknown	; consensus \"TGYTCATTTGAAAGGWAATR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TKTTCATTTGAAAGGAAATN",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_2_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -105.761269,
/* max_score        */ 22.332865,
/* threshold        */ 0.864,
/* info content     */ 24.634563,
/* base_counts      */ {328, 98, 196, 338},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_3_matrix_row_0_[19] = {  0.859913, -2.234710, -3.436355, -2.052555, -0.900446, -2.139491,  1.195079, -2.590957, -0.194360, -1.829578, -1.205734,  1.162182, -2.744870, -3.149504, -1.829578, -1.242088, -1.704494,  1.220638, -1.205734};
	float jaspar_cne_pwm_3_matrix_row_1_[19] = { -0.659284, -2.349795, -1.406206, -4.287183, -1.205636,  1.406683, -2.349795, -1.993548,  0.465126, -2.216442,  0.814915, -1.657362, -4.287183, -2.349795, -3.598998, -1.077857,  1.393260, -2.908342, -1.205636};
	float jaspar_cne_pwm_3_matrix_row_2_[19] = { -1.000926, -1.993548, -1.657362, -2.098798,  1.271250, -3.195193, -3.195193, -2.503708, -0.186095, -2.503708,  0.506970, -1.657362, -3.598998, -4.287183, -2.685697,  1.263587, -4.287183, -3.598998, -2.685697};
	float jaspar_cne_pwm_3_matrix_row_3_[19] = { -0.666886,  1.188585,  1.162182,  1.204740, -2.052555, -1.542063, -1.829578,  1.201530, -0.168721,  1.182050, -1.647368, -2.590957,  1.245561,  1.236287,  1.204740, -1.446798, -1.542063, -2.139491,  1.092953};
	float *jaspar_cne_pwm_3_matrix[4] = { jaspar_cne_pwm_3_matrix_row_0_, jaspar_cne_pwm_3_matrix_row_1_, jaspar_cne_pwm_3_matrix_row_2_, jaspar_cne_pwm_3_matrix_row_3_};
	PWM jaspar_cne_pwm_3_ = {
/* accession        */ "CN0018",
/* name             */ "LM18",
/* label            */ " CN0018	23.7515789282212	LM18	unknown	; consensus \"ATTTGCATNTSATTTGCAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTTGCATNTSATTTGCAT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_3_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -53.236584,
/* max_score        */ 21.771261,
/* threshold        */ 0.840,
/* info content     */ 23.744539,
/* base_counts      */ {1444, 1065, 813, 3024},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 334
};

	float jaspar_cne_pwm_4_matrix_row_0_[13] = { -0.321215,  1.213144,  1.250481,  1.231097, -2.796838,  1.248734, -3.306998,  1.257437, -3.488986, -3.153085, -3.488986, -3.306998, -1.362515};
	float jaspar_cne_pwm_4_matrix_row_1_[13] = { -0.337363, -2.025459, -3.757320, -3.247824,  1.475812, -2.778569,  1.491643, -4.849310, -3.247824,  1.479352, -3.757320, -2.373521, -0.195727};
	float jaspar_cne_pwm_4_matrix_row_2_[13] = { -1.332606, -2.373521, -3.470469, -2.660925, -2.911922, -4.161126, -3.065835, -4.849310,  1.481117, -2.460456, -4.161126,  1.468694, -0.582455};
	float jaspar_cne_pwm_4_matrix_row_3_[13] = {  0.731490, -3.488986, -3.306998, -2.796838, -3.488986, -3.711631, -5.090472, -3.153085, -2.902087, -4.402287,  1.255703, -3.019732,  0.802238};
	float *jaspar_cne_pwm_4_matrix[4] = { jaspar_cne_pwm_4_matrix_row_0_, jaspar_cne_pwm_4_matrix_row_1_, jaspar_cne_pwm_4_matrix_row_2_, jaspar_cne_pwm_4_matrix_row_3_};
	PWM jaspar_cne_pwm_4_ = {
/* accession        */ "CN0026",
/* name             */ "LM26",
/* label            */ " CN0026	20.2144787376821	LM26	unknown	; consensus \"WAAACACAGCTGT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TAAACACAGCTGT",
/* library_name     */ "jaspar_cne",
/* length           */ 13,
/* matrixname       */ jaspar_cne_pwm_4_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.138542,
/* max_score        */ 16.386942,
/* threshold        */ 0.779,
/* info content     */ 20.210655,
/* base_counts      */ {3036, 1955, 1287, 1340},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 586
};

	float jaspar_cne_pwm_5_matrix_row_0_[14] = { -1.251066, -0.692519, -7.245027, -7.245027, -7.245027, -7.245027, -1.251066,  1.189002,  1.210503,  1.272366, -7.245027,  1.272366,  1.231552, -7.245027};
	float jaspar_cne_pwm_5_matrix_row_1_[14] = {  0.692802,  1.362738, -1.009904, -2.388745, -7.003865, -7.003865, -1.009904, -7.003865, -7.003865, -7.003865,  1.513528, -7.003865, -7.003865, -7.003865};
	float jaspar_cne_pwm_5_matrix_row_2_[14] = {  0.692802, -7.003865, -7.003865, -1.296755, -1.700560,  1.513528, -1.009904, -1.296755, -1.296755, -7.003865, -7.003865, -7.003865, -1.700560,  1.513528};
	float jaspar_cne_pwm_5_matrix_row_3_[14] = { -1.941722, -7.245027,  1.189002,  1.189002,  1.231552, -7.245027,  0.997992, -2.629907, -7.245027, -7.245027, -7.245027, -7.245027, -7.245027, -7.245027};
	float *jaspar_cne_pwm_5_matrix[4] = { jaspar_cne_pwm_5_matrix_row_0_, jaspar_cne_pwm_5_matrix_row_1_, jaspar_cne_pwm_5_matrix_row_2_, jaspar_cne_pwm_5_matrix_row_3_};
	PWM jaspar_cne_pwm_5_ = {
/* accession        */ "CN0156",
/* name             */ "LM156",
/* label            */ " CN0156	22.5724021755861	LM156	unknown	; consensus \"SCTTTGTAAACAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "SCTTTGTAAACAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_5_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.891945,
/* max_score        */ 17.379459,
/* threshold        */ 0.789,
/* info content     */ 22.490267,
/* base_counts      */ {256, 124, 139, 181},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_6_matrix_row_0_[18] = { -0.403507, -0.046893, -0.270001,  0.946268, -4.285581, -1.991947,  0.930269,  1.272851, -8.900702,  1.213897, -8.900702, -8.900702, -2.684096,  1.272851, -1.037050, -0.631714,  0.891910,  0.135404};
	float jaspar_cne_pwm_6_matrix_row_1_[18] = {  0.872957, -0.652839,  0.236227, -0.835094, -8.659539, -8.659539,  0.276495, -8.659539, -8.659539, -8.659539, -8.659539, -8.659539,  1.459100, -8.659539, -4.044419, -0.365240, -1.568630, -1.109404};
	float jaspar_cne_pwm_6_matrix_row_2_[18] = { -0.588321, -1.414598, -0.470573, -4.044419, -8.659539,  1.471123, -8.659539, -8.659539, -4.044419, -8.659539, -8.659539,  0.400094, -2.442934, -8.659539, -8.659539,  1.116171, -0.918441,  0.820904};
	float jaspar_cne_pwm_6_matrix_row_3_[18] = { -0.534099,  0.701748,  0.279283, -0.445171,  1.269027, -4.285581, -8.900702, -8.900702,  1.269027, -1.586815,  1.272851,  0.875009, -2.906740, -8.900702,  1.164097, -2.348194, -0.424122, -0.962970};
	float *jaspar_cne_pwm_6_matrix[4] = { jaspar_cne_pwm_6_matrix_row_0_, jaspar_cne_pwm_6_matrix_row_1_, jaspar_cne_pwm_6_matrix_row_2_, jaspar_cne_pwm_6_matrix_row_3_};
	PWM jaspar_cne_pwm_6_ = {
/* accession        */ "CN0060",
/* name             */ "LM60",
/* label            */ " CN0060	21.994425588456	LM60	unknown	; consensus \"YWYATGAATATTCATRWR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTNATGAATATTCATGAR",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_6_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.493248,
/* max_score        */ 19.099342,
/* threshold        */ 0.812,
/* info content     */ 21.978291,
/* base_counts      */ {1665, 662, 756, 1633},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 262
};

	float jaspar_cne_pwm_7_matrix_row_0_[17] = { -1.000824, -1.000824,  1.245687, -2.631347, -3.727742, -0.498817, -1.363634, -0.175627,  1.159304, -0.552877, -2.263964, -1.884717, -2.430878,  1.229464,  1.201027,  1.229464, -0.340934};
	float jaspar_cne_pwm_7_matrix_row_1_[17] = { -1.948749,  1.105245, -2.977084, -3.199729, -2.390185, -3.890386, -1.032887,  1.182952, -1.452591, -2.390185, -3.890386, -1.292313,  1.400466, -3.890386, -3.890386, -3.486580, -1.452591};
	float jaspar_cne_pwm_7_matrix_row_2_[17] = { -1.815307, -0.477483, -3.486580, -9.193690, -4.578570, -3.486580,  1.297611, -2.390185, -9.193690, -2.795095, -2.507829,  0.653280, -2.795095, -4.578570, -1.543522, -3.199729, -0.545294};
	float jaspar_cne_pwm_7_matrix_row_3_[17] = {  1.086547, -1.068250, -3.440891,  1.243385,  1.243385,  1.072978, -1.833450, -2.343943, -1.610407,  1.056449,  1.220075,  0.526951, -1.395373, -2.056469, -2.748992, -2.343943,  0.797975};
	float *jaspar_cne_pwm_7_matrix[4] = { jaspar_cne_pwm_7_matrix_row_0_, jaspar_cne_pwm_7_matrix_row_1_, jaspar_cne_pwm_7_matrix_row_2_, jaspar_cne_pwm_7_matrix_row_3_};
	PWM jaspar_cne_pwm_7_ = {
/* accession        */ "CN0053",
/* name             */ "LM53",
/* label            */ " CN0053	21.690876286111	LM53	unknown	; consensus \"TCATTTGCATTKCAAAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCATTTGCATTKCAAAT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_7_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -62.356972,
/* max_score        */ 19.425295,
/* threshold        */ 0.803,
/* info content     */ 21.685415,
/* base_counts      */ {2627, 1177, 745, 3050},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 447
};

	float jaspar_cne_pwm_8_matrix_row_0_[13] = {  1.272234, -7.046752, -7.046752, -7.046752, -7.046752,  1.272234,  1.272234, -7.046752, -7.046752,  1.272234, -7.046752, -7.046752,  1.272234};
	float jaspar_cne_pwm_8_matrix_row_1_[13] = { -6.805590, -6.805590, -6.805590,  1.513396, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590,  1.513396, -6.805590};
	float jaspar_cne_pwm_8_matrix_row_2_[13] = { -6.805590, -6.805590,  1.513396, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590, -6.805590,  1.513396, -6.805590, -6.805590};
	float jaspar_cne_pwm_8_matrix_row_3_[13] = { -7.046752,  1.272234, -7.046752, -7.046752,  1.272234, -7.046752, -7.046752,  1.272234,  1.272234, -7.046752, -7.046752, -7.046752, -7.046752};
	float *jaspar_cne_pwm_8_matrix[4] = { jaspar_cne_pwm_8_matrix_row_0_, jaspar_cne_pwm_8_matrix_row_1_, jaspar_cne_pwm_8_matrix_row_2_, jaspar_cne_pwm_8_matrix_row_3_};
	PWM jaspar_cne_pwm_8_ = {
/* accession        */ "CN0013",
/* name             */ "LM13",
/* label            */ " CN0013	26	LM13	unknown	; consensus \"WTGCTAATTAGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATGCTAATTAGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 13,
/* matrixname       */ jaspar_cne_pwm_8_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.607780,
/* max_score        */ 17.503693,
/* threshold        */ 0.786,
/* info content     */ 25.872231,
/* base_counts      */ {205, 82, 82, 164},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 41
};

	float jaspar_cne_pwm_9_matrix_row_0_[15] = { -7.183203,  1.206384,  1.228852,  1.228852,  1.085785, -7.183203, -7.183203, -2.568082, -7.183203, -1.879898, -7.183203, -2.568082, -2.568082, -2.568082, -1.189241};
	float jaspar_cne_pwm_9_matrix_row_1_[15] = { -1.638736, -1.638736, -6.942041, -1.638736, -6.942041, -6.942041, -6.942041, -2.326920, -0.948079,  1.470014, -6.942041, -6.942041, -1.234931, -6.942041, -6.942041};
	float jaspar_cne_pwm_9_matrix_row_2_[15] = {  1.447546, -2.326920, -6.942041, -6.942041, -0.543446, -6.942041,  1.513490,  1.352259,  1.274318, -6.942041, -6.942041,  1.491988, -1.638736, -1.638736, -0.948079};
	float jaspar_cne_pwm_9_matrix_row_3_[15] = { -2.568082, -7.183203, -1.879898, -7.183203, -1.879898,  1.272328, -7.183203, -0.966597, -0.784608, -7.183203,  1.272328, -7.183203,  1.135783,  1.206384,  1.085785};
	float *jaspar_cne_pwm_9_matrix[4] = { jaspar_cne_pwm_9_matrix_row_0_, jaspar_cne_pwm_9_matrix_row_1_, jaspar_cne_pwm_9_matrix_row_2_, jaspar_cne_pwm_9_matrix_row_3_};
	PWM jaspar_cne_pwm_9_ = {
/* accession        */ "CN0218",
/* name             */ "LM218",
/* label            */ " CN0218	23.7930395929081	LM218	unknown	; consensus \"GAAAATGGGCTGTTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GAAAATGGGCTGTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_9_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.553154,
/* max_score        */ 19.272097,
/* threshold        */ 0.802,
/* info content     */ 23.710173,
/* base_counts      */ {183, 59, 229, 234},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_10_matrix_row_0_[14] = { -7.139756, -7.139756,  1.203322,  1.272299,  1.203322, -7.139756, -2.524635,  1.249831, -7.139756, -1.836451,  1.249831, -7.139756, -7.139756, -1.836451};
	float jaspar_cne_pwm_10_matrix_row_1_[14] = { -6.898593,  1.395706, -1.595289, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593,  1.468009, -6.898593, -1.595289,  1.420393, -2.283473};
	float jaspar_cne_pwm_10_matrix_row_2_[14] = {  1.344426, -1.191483, -6.898593, -6.898593, -6.898593, -1.595289, -1.595289, -6.898593,  1.513461, -6.898593, -6.898593,  1.370395, -6.898593, -6.898593};
	float jaspar_cne_pwm_10_matrix_row_3_[14] = { -0.587248, -1.836451, -2.524635, -7.139756, -1.432645,  1.226847,  1.203322, -2.524635, -7.139756, -7.139756, -2.524635, -1.145794, -1.145794,  1.203322};
	float *jaspar_cne_pwm_10_matrix[4] = { jaspar_cne_pwm_10_matrix_row_0_, jaspar_cne_pwm_10_matrix_row_1_, jaspar_cne_pwm_10_matrix_row_2_, jaspar_cne_pwm_10_matrix_row_3_};
	PWM jaspar_cne_pwm_10_ = {
/* accession        */ "CN0038",
/* name             */ "LM38",
/* label            */ " CN0038	23.2143856409627	LM38	unknown	; consensus \"GCAAATTAGCAGCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCAAATTAGCAGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_10_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.509598,
/* max_score        */ 18.324488,
/* threshold        */ 0.791,
/* info content     */ 23.126017,
/* base_counts      */ {222, 129, 129, 150},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 45
};

	float jaspar_cne_pwm_11_matrix_row_0_[19] = { -0.909587, -0.504538,  1.243974, -7.595448, -2.292143,  1.137018, -1.196853, -0.909587,  1.052949,  1.184264, -0.350506,  1.035253, -2.292143,  1.214564,  1.120760,  1.214564, -1.601486, -2.980327,  0.342284};
	float jaspar_cne_pwm_11_matrix_row_1_[19] = {  0.940014,  1.221365, -7.354286, -7.354286, -0.955691, -7.354286, -7.354286, -1.137679, -0.801778, -7.354286, -1.647175, -1.647175,  1.440691, -2.050981, -7.354286, -2.050981, -7.354286, -1.647175, -0.668425};
	float jaspar_cne_pwm_11_matrix_row_2_[19] = { -1.360324, -2.739165, -7.354286, -7.354286, -2.739165, -2.739165,  1.276415,  1.294111, -1.647175, -7.354286,  1.240054, -0.445531, -1.647175, -7.354286, -0.668425, -2.050981, -0.801778,  1.361922,  0.470160};
	float jaspar_cne_pwm_11_matrix_row_3_[19] = { -0.045312, -1.378842, -2.292143,  1.272543,  1.137018, -0.909587, -0.791942, -2.980327, -1.601486, -1.196853, -7.595448, -2.292143, -7.595448, -2.292143, -2.292143, -7.595448,  1.104234, -1.196853, -0.686693};
	float *jaspar_cne_pwm_11_matrix[4] = { jaspar_cne_pwm_11_matrix_row_0_, jaspar_cne_pwm_11_matrix_row_1_, jaspar_cne_pwm_11_matrix_row_2_, jaspar_cne_pwm_11_matrix_row_3_};
	PWM jaspar_cne_pwm_11_ = {
/* accession        */ "CN0162",
/* name             */ "LM162",
/* label            */ " CN0162	23.313636307951	LM162	unknown	; consensus \"YCATTAGGAAGACAAATGR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CCATTAGGAAGACAAATGN",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_11_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.287109,
/* max_score        */ 21.961878,
/* threshold        */ 0.835,
/* info content     */ 23.266590,
/* base_counts      */ {589, 198, 293, 269},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 71
};

	float jaspar_cne_pwm_12_matrix_row_0_[14] = { -0.497342, -0.784608,  1.206384, -0.784608, -7.183203, -2.568082, -7.183203, -7.183203, -7.183203,  1.250826,  1.228852, -7.183203, -7.183203,  1.272328};
	float jaspar_cne_pwm_12_matrix_row_1_[14] = { -6.942041,  1.218763, -6.942041, -6.942041,  1.513490, -2.326920, -6.942041, -2.326920, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041};
	float jaspar_cne_pwm_12_matrix_row_2_[14] = {  1.218763, -0.725435, -6.942041,  1.300979, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041,  1.513490, -6.942041};
	float jaspar_cne_pwm_12_matrix_row_3_[14] = { -1.189241, -2.568082, -1.476093, -1.476093, -7.183203,  1.228852,  1.272328,  1.250826,  1.272328, -2.568082, -1.879898,  1.272328, -7.183203, -7.183203};
	float *jaspar_cne_pwm_12_matrix[4] = { jaspar_cne_pwm_12_matrix_row_0_, jaspar_cne_pwm_12_matrix_row_1_, jaspar_cne_pwm_12_matrix_row_2_, jaspar_cne_pwm_12_matrix_row_3_};
	PWM jaspar_cne_pwm_12_ = {
/* accession        */ "CN0122",
/* name             */ "LM122",
/* label            */ " CN0122	23.7177464638423	LM122	unknown	; consensus \"GMAGCTTTTAATGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCAGCTTTTAATGA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_12_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.502754,
/* max_score        */ 18.020535,
/* threshold        */ 0.793,
/* info content     */ 23.625181,
/* base_counts      */ {203, 84, 125, 246},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_13_matrix_row_0_[14] = { -2.203902, -7.507207,  1.225259, -7.507207, -7.507207, -0.336319,  1.272504, -2.892087, -7.507207,  1.141190, -0.503233, -7.507207, -1.290601, -7.507207};
	float jaspar_cne_pwm_13_matrix_row_1_[14] = { -7.266045, -7.266045, -7.266045, -7.266045, -7.266045, -7.266045, -7.266045, -1.962740, -7.266045, -7.266045, -7.266045,  0.475054, -7.266045, -1.272083};
	float jaspar_cne_pwm_13_matrix_row_2_[14] = { -1.962740,  1.513666, -7.266045, -2.650924, -2.650924, -7.266045, -7.266045, -7.266045,  1.513666, -0.580184,  1.210535,  1.077033, -7.266045,  1.450163};
	float jaspar_cne_pwm_13_matrix_row_3_[14] = {  1.209001, -7.507207, -1.800097,  1.257002,  1.257002,  1.049399, -7.507207,  1.225259, -7.507207, -7.507207, -1.108612, -7.507207,  1.192474, -7.507207};
	float *jaspar_cne_pwm_13_matrix[4] = { jaspar_cne_pwm_13_matrix_row_0_, jaspar_cne_pwm_13_matrix_row_1_, jaspar_cne_pwm_13_matrix_row_2_, jaspar_cne_pwm_13_matrix_row_3_};
	PWM jaspar_cne_pwm_13_ = {
/* accession        */ "CN0063",
/* name             */ "LM63",
/* label            */ " CN0063	22.7972241099222	LM63	unknown	; consensus \"TGATTTATGAGGTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGATTTATGAGGTG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_13_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.653931,
/* max_score        */ 17.594152,
/* threshold        */ 0.790,
/* info content     */ 22.730433,
/* base_counts      */ {216, 29, 293, 372},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 65
};

	float jaspar_cne_pwm_14_matrix_row_0_[17] = { -7.224841, -1.921536, -2.609720, -1.517731, -1.921536, -1.230879, -1.230879, -2.609720,  1.209188,  1.251739,  1.118237, -2.609720, -7.224841,  1.209188,  1.209188, -0.538980, -2.609720};
	float jaspar_cne_pwm_14_matrix_row_1_[17] = { -0.074924, -2.368558, -6.983679,  1.118302, -6.983679,  1.177125, -6.983679, -6.983679, -6.983679, -6.983679, -0.585084, -6.983679, -1.680374, -6.983679, -6.983679, -0.767073, -0.767073};
	float jaspar_cne_pwm_14_matrix_row_2_[17] = {  0.917699, -2.368558, -6.983679, -1.680374, -6.983679, -0.989717, -6.983679,  1.492901, -2.368558, -2.368558, -6.983679, -6.983679,  1.382924, -2.368558, -6.983679,  1.118302, -6.983679};
	float jaspar_cne_pwm_14_matrix_row_3_[17] = { -0.133931,  1.187214,  1.251739, -0.220867,  1.230690, -0.826246,  1.187214, -7.224841, -1.921536, -7.224841, -2.609720,  1.251739, -1.230879, -1.921536, -1.517731, -1.517731,  1.141762};
	float *jaspar_cne_pwm_14_matrix[4] = { jaspar_cne_pwm_14_matrix_row_0_, jaspar_cne_pwm_14_matrix_row_1_, jaspar_cne_pwm_14_matrix_row_2_, jaspar_cne_pwm_14_matrix_row_3_};
	PWM jaspar_cne_pwm_14_ = {
/* accession        */ "CN0193",
/* name             */ "LM193",
/* label            */ " CN0193	23.7471950518708	LM193	unknown	; consensus \"GTTCTCTGAAATGAAGT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GTTCTCTGAAATGAAGT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_14_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.550018,
/* max_score        */ 20.455149,
/* threshold        */ 0.821,
/* info content     */ 23.671980,
/* base_counts      */ {255, 97, 161, 320},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_15_matrix_row_0_[14] = {  1.022398,  0.670507, -5.003157,  1.272910, -9.618278, -2.239893, -9.618278, -9.618278, -3.401671, -2.709522, -1.793831,  0.965804,  1.100598,  1.133244};
	float jaspar_cne_pwm_15_matrix_row_1_[14] = { -1.636016, -1.726946,  1.512208, -9.377115, -9.377115,  1.476117, -9.377115, -9.377115, -0.294494, -9.377115,  0.148109,  0.015630, -0.552290, -1.881018};
	float jaspar_cne_pwm_15_matrix_row_2_[14] = { -0.711329,  0.603380, -9.377115, -9.377115,  1.495275, -3.383153, -9.377115,  1.510341, -1.636016, -2.468360,  1.059967, -2.206226, -2.286205, -1.726946};
	float jaspar_cne_pwm_15_matrix_row_3_[14] = { -1.375258, -3.219682, -9.618278, -9.618278, -2.709522, -9.618278,  1.272910, -4.314972,  1.029550,  1.234955, -1.486452, -2.814772, -3.401671, -1.578797};
	float *jaspar_cne_pwm_15_matrix[4] = { jaspar_cne_pwm_15_matrix_row_0_, jaspar_cne_pwm_15_matrix_row_1_, jaspar_cne_pwm_15_matrix_row_2_, jaspar_cne_pwm_15_matrix_row_3_};
	PWM jaspar_cne_pwm_15_ = {
/* accession        */ "CN0046",
/* name             */ "LM46",
/* label            */ " CN0046	20.0283693584513	LM46	unknown	; consensus \"ARCAGCTGTTSAAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AACAGCTGTTGAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_15_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.235451,
/* max_score        */ 16.756784,
/* threshold        */ 0.781,
/* info content     */ 20.021877,
/* base_counts      */ {2620, 1528, 1760, 1610},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 537
};

	float jaspar_cne_pwm_16_matrix_row_0_[20] = { -1.497128,  1.064750,  1.090061, -1.900934, -1.900934, -1.210277,  0.897742, -0.518378, -2.589118, -2.589118, -7.204238, -2.589118, -1.497128, -0.518378, -2.589118, -7.204238,  1.207816,  1.162364,  1.229790,  0.536861};
	float jaspar_cne_pwm_16_matrix_row_1_[20] = { -6.963077, -0.564481, -2.347956, -6.963077, -6.963077, -1.659772, -2.347956, -2.347956, -6.963077, -1.255966,  1.426511, -0.969115, -0.564481, -2.347956, -6.963077, -1.659772, -6.963077, -6.963077, -2.347956, -2.347956};
	float jaspar_cne_pwm_16_matrix_row_2_[20] = {  1.426511, -1.659772, -0.564481, -6.963077, -6.963077, -2.347956, -0.277216,  1.168748, -2.347956,  1.380001, -1.255966, -6.963077, -0.159571,  1.009734, -0.054322,  1.470952, -1.255966, -0.746470, -6.963077,  0.778023};
	float jaspar_cne_pwm_16_matrix_row_3_[20] = { -2.589118, -2.589118, -2.589118,  1.229790,  1.229790,  1.114748, -0.805644, -0.987632,  1.229790, -1.900934, -2.589118,  1.162364,  0.802462, -0.295484,  1.012120, -7.204238, -7.204238, -7.204238, -2.589118, -2.589118};
	float *jaspar_cne_pwm_16_matrix[4] = { jaspar_cne_pwm_16_matrix_row_0_, jaspar_cne_pwm_16_matrix_row_1_, jaspar_cne_pwm_16_matrix_row_2_, jaspar_cne_pwm_16_matrix_row_3_};
	PWM jaspar_cne_pwm_16_ = {
/* accession        */ "CN0227",
/* name             */ "LM227",
/* label            */ " CN0227	25.4288209025039	LM227	unknown	; consensus \"GAATTTAGTGCTTGTGAAAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GAATTTAGTGCTTGTGAAAR",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_16_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.803917,
/* max_score        */ 23.094070,
/* threshold        */ 0.874,
/* info content     */ 25.354788,
/* base_counts      */ {303, 73, 266, 318},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_17_matrix_row_0_[19] = { -0.824616, -1.355142, -2.048039, -1.952774, -5.034321, -2.271058, -3.432835, -4.346137, -3.096933, -2.963580, -5.034321, -0.299252, -5.034321, -9.649442, -2.335555, -0.184381, -2.048039, -2.048039, -4.346137};
	float jaspar_cne_pwm_17_matrix_row_1_[19] = {  0.706320,  1.222781, -1.306299,  0.897368, -2.855771, -2.722418,  1.391317, -0.890886, -9.408279,  1.484768,  1.492175,  1.263022,  0.009157,  1.508644, -1.139291, -1.065202,  0.784177, -2.604774, -3.701169};
	float jaspar_cne_pwm_17_matrix_row_2_[19] = { -0.598267, -1.858144, -1.912182, -1.018692, -2.163338,  1.431321, -0.832629, -4.793159,  1.466006, -4.104974, -9.408279, -4.104974, -9.408279, -9.408279, -2.855771,  0.656519,  0.681729, -2.404305,  1.503184};
	float jaspar_cne_pwm_17_matrix_row_3_[19] = {  0.103281, -0.649699,  1.135766,  0.197529,  1.232391, -2.048039, -3.250846,  1.172355, -2.099306, -3.250846, -2.645467, -3.250846,  1.019537, -3.942331,  1.156258, -0.046991, -1.785790,  1.197972, -5.034321};
	float *jaspar_cne_pwm_17_matrix[4] = { jaspar_cne_pwm_17_matrix_row_0_, jaspar_cne_pwm_17_matrix_row_1_, jaspar_cne_pwm_17_matrix_row_2_, jaspar_cne_pwm_17_matrix_row_3_};
	PWM jaspar_cne_pwm_17_ = {
/* accession        */ "CN0146",
/* name             */ "LM146",
/* label            */ " CN0146	24.0907530540881	LM146	unknown	; consensus \"CCTYTGCTGCCCTCTWVTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "YCTCTGCTGCCCTCTNSTG",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_17_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.358643,
/* max_score        */ 22.721882,
/* threshold        */ 0.845,
/* info content     */ 24.085138,
/* base_counts      */ {492, 4093, 2300, 3641},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 554
};

	float jaspar_cne_pwm_18_matrix_row_0_[16] = {  0.451640, -0.692519, -0.154117,  1.252167,  1.272366,  1.272366, -0.074139, -7.245027, -1.028421,  1.272366,  1.272366, -7.245027, -7.245027,  1.272366,  1.210503, -7.245027};
	float jaspar_cne_pwm_18_matrix_row_1_[16] = { -1.700560, -1.009904,  0.310022, -2.388745, -7.003865, -7.003865, -7.003865, -7.003865,  1.362738, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865};
	float jaspar_cne_pwm_18_matrix_row_2_[16] = {  0.000109,  0.968946,  0.692802, -7.003865, -7.003865, -7.003865, -1.700560,  1.513528, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -1.296755,  1.513528};
	float jaspar_cne_pwm_18_matrix_row_3_[16] = {  0.068860, -0.336272, -2.629907, -7.245027, -7.245027, -7.245027,  0.915777, -7.245027, -1.941722, -7.245027, -7.245027,  1.272366,  1.272366, -7.245027, -7.245027, -7.245027};
	float *jaspar_cne_pwm_18_matrix[4] = { jaspar_cne_pwm_18_matrix_row_0_, jaspar_cne_pwm_18_matrix_row_1_, jaspar_cne_pwm_18_matrix_row_2_, jaspar_cne_pwm_18_matrix_row_3_};
	PWM jaspar_cne_pwm_18_ = {
/* accession        */ "CN0067",
/* name             */ "LM67",
/* label            */ " CN0067	24.8081709498195	LM67	unknown	; consensus \"WKVAAATGCAATTAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NGNAAATGCAATTAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_18_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.043388,
/* max_score        */ 18.788189,
/* threshold        */ 0.819,
/* info content     */ 24.712366,
/* base_counts      */ {405, 65, 167, 163},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_19_matrix_row_0_[20] = { -0.162551, -0.902555, -1.069574, -0.228920,  1.142486, -2.655656, -4.155856, -0.549791, -1.387943, -1.675520,  1.219077, -2.288273, -1.718062, -1.452460, -1.675520,  1.200472,  1.132486,  1.122386,  0.562154, -0.041725};
	float jaspar_cne_pwm_19_matrix_row_1_[20] = { -0.350008,  0.985630, -2.309244,  0.928474, -1.667864, -3.510889, -1.476900, -4.602879, -1.086174,  1.434567, -2.665491, -1.316622, -1.721902, -1.904112,  1.284353, -2.214025, -2.665491, -2.532138, -2.665491, -0.623660};
	float jaspar_cne_pwm_19_matrix_row_2_[20] = {  0.487099, -0.899013, -2.819404, -0.680807, -2.414494, -3.510889, -3.914694, -2.819404,  1.308776, -3.914694, -9.217999, -2.819404, -1.667864, -1.567830, -0.828412, -3.914694, -1.434358, -1.973057,  0.480983,  0.776289};
	float jaspar_cne_pwm_19_matrix_row_3_[20] = { -0.117705, -0.300009,  1.132486, -0.941768, -1.521429,  1.239601,  1.212140,  1.078281, -1.909026, -2.655656, -2.020190,  1.167057,  1.132486,  1.117296, -1.595510, -1.909026, -1.486350, -1.116083, -0.710697, -0.726695};
	float *jaspar_cne_pwm_19_matrix[4] = { jaspar_cne_pwm_19_matrix_row_0_, jaspar_cne_pwm_19_matrix_row_1_, jaspar_cne_pwm_19_matrix_row_2_, jaspar_cne_pwm_19_matrix_row_3_};
	PWM jaspar_cne_pwm_19_ = {
/* accession        */ "CN0065",
/* name             */ "LM65",
/* label            */ " CN0065	21.7486299931811	LM65	unknown	; consensus \"KCTCATTTGCATTTCAAARR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NCTCATTTGCATTTCAAARN",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_19_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -55.365025,
/* max_score        */ 21.663599,
/* threshold        */ 0.821,
/* info content     */ 21.743765,
/* base_counts      */ {2940, 1627, 1169, 3424},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 458
};

	float jaspar_cne_pwm_20_matrix_row_0_[19] = { -0.475388, -0.721471,  1.119253, -2.535565, -9.088074, -2.179319, -1.186696, -0.819085,  1.173124, -1.391406, -2.284568, -1.081372, -2.084099,  1.234158,  1.183558,  1.260132, -0.871715, -0.611494,  0.500772};
	float jaspar_cne_pwm_20_matrix_row_1_[19] = { -1.533024,  0.333073, -0.909180, -3.543606, -2.294403, -8.846911, -1.350814,  1.334246, -1.938156, -1.938156, -8.846911, -0.840210,  1.299562, -2.852950, -4.231791, -8.846911, -2.294403, -1.468528, -0.082702};
	float jaspar_cne_pwm_20_matrix_row_2_[19] = { -0.290305,  0.633533, -2.448316, -4.231791, -3.543606, -3.543606,  1.283752, -3.543606, -8.846911, -2.852950, -1.245509,  0.845917, -1.245509, -3.543606, -1.063271, -4.231791, -1.938156,  1.095845, -0.290305};
	float jaspar_cne_pwm_20_matrix_row_3_[19] = {  0.785007, -0.676019, -2.084099,  1.240715,  1.243977,  1.234158, -1.486671, -2.084099, -1.486671,  1.151922,  1.176614,  0.060498, -1.081372, -2.689478, -3.784768, -3.380963,  1.085480, -0.698486, -0.493734};
	float *jaspar_cne_pwm_20_matrix[4] = { jaspar_cne_pwm_20_matrix_row_0_, jaspar_cne_pwm_20_matrix_row_1_, jaspar_cne_pwm_20_matrix_row_2_, jaspar_cne_pwm_20_matrix_row_3_};
	PWM jaspar_cne_pwm_20_ = {
/* accession        */ "CN0034",
/* name             */ "LM34",
/* label            */ " CN0034	21.9281803506367	LM34	unknown	; consensus \"WSATTTGCATTGCAAATGM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TNATTTGCATTGCAAATGN",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_20_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.491089,
/* max_score        */ 20.881720,
/* threshold        */ 0.817,
/* info content     */ 21.919733,
/* base_counts      */ {1946, 828, 950, 2280},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 316
};

	float jaspar_cne_pwm_21_matrix_row_0_[17] = { -1.596141, -1.596141,  1.193944, -1.999946, -2.688130, -1.999946, -7.303251, -1.999946, -1.596141,  1.253355,  1.233941,  0.075133, -2.688130, -1.999946, -0.617390,  1.152280,  1.130778};
	float jaspar_cne_pwm_21_matrix_row_1_[17] = { -1.354979,  1.349966, -2.446968, -7.062089, -7.062089, -7.062089, -7.062089, -0.376228,  1.371940, -2.446968, -7.062089, -2.446968, -7.062089, -1.758784,  1.206899, -7.062089, -1.758784};
	float jaspar_cne_pwm_21_matrix_row_2_[17] = { -2.446968, -1.354979, -7.062089, -1.354979, -7.062089, -7.062089, -7.062089,  1.009130, -1.758784, -7.062089, -1.758784,  1.126878, -7.062089,  1.414491, -0.663494, -1.758784, -1.068127};
	float jaspar_cne_pwm_21_matrix_row_3_[17] = {  1.130778, -1.999946, -1.596141,  1.173329,  1.253355,  1.233941,  1.272400, -0.299277, -1.999946, -7.303251, -7.303251, -7.303251,  1.253355, -2.688130, -7.303251, -1.309289, -2.688130};
	float *jaspar_cne_pwm_21_matrix[4] = { jaspar_cne_pwm_21_matrix_row_0_, jaspar_cne_pwm_21_matrix_row_1_, jaspar_cne_pwm_21_matrix_row_2_, jaspar_cne_pwm_21_matrix_row_3_};
	PWM jaspar_cne_pwm_21_ = {
/* accession        */ "CN0182",
/* name             */ "LM182",
/* label            */ " CN0182	24.215150095924	LM182	unknown	; consensus \"TCATTTTGCAAGTGCAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCATTTTGCAAGTGCAA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_21_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.711853,
/* max_score        */ 20.760757,
/* threshold        */ 0.825,
/* info content     */ 24.147339,
/* base_counts      */ {288, 148, 139, 326},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 53
};

	float jaspar_cne_pwm_22_matrix_row_0_[14] = { -2.609720, -7.224841, -7.224841,  1.230690, -2.609720, -0.220867, -2.609720, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -2.609720, -1.921536};
	float jaspar_cne_pwm_22_matrix_row_1_[14] = { -0.297818, -6.983679, -6.983679, -2.368558,  1.471852,  1.259341, -2.368558, -2.368558, -6.983679, -6.983679, -0.585084, -2.368558, -2.368558,  1.405908};
	float jaspar_cne_pwm_22_matrix_row_2_[14] = {  1.177125, -1.680374,  1.513516, -2.368558, -6.983679, -6.983679, -6.983679, -6.983679, -2.368558, -2.368558,  1.382924, -6.983679, -6.983679, -6.983679};
	float jaspar_cne_pwm_22_matrix_row_3_[14] = { -1.008235,  1.230690, -7.224841, -7.224841, -2.609720, -7.224841,  1.230690,  1.251739,  1.251739,  1.251739, -7.224841,  1.251739,  1.230690, -1.517731};
	float *jaspar_cne_pwm_22_matrix[4] = { jaspar_cne_pwm_22_matrix_row_0_, jaspar_cne_pwm_22_matrix_row_1_, jaspar_cne_pwm_22_matrix_row_2_, jaspar_cne_pwm_22_matrix_row_3_};
	PWM jaspar_cne_pwm_22_ = {
/* accession        */ "CN0140",
/* name             */ "LM140",
/* label            */ " CN0140	22.9283589212942	LM140	unknown	; consensus \"GTGACCTTTTGTTC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GTGACCTTTTGTTC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_22_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.568001,
/* max_score        */ 18.140379,
/* threshold        */ 0.790,
/* info content     */ 22.850866,
/* base_counts      */ {64, 148, 132, 342},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_23_matrix_row_0_[15] = { -0.825752, -0.273907, -2.381926, -3.760767, -1.204999,  1.259786,  1.065644,  1.253229, -8.375888, -8.375888, -8.375888,  1.233296,  1.272772,  1.163828,  0.695306};
	float jaspar_cne_pwm_23_matrix_row_1_[15] = {  0.081633, -8.134726, -1.331220,  1.507462,  1.426342, -8.134726, -8.134726, -8.134726, -8.134726, -8.134726, -1.130751, -8.134726, -8.134726, -2.831421, -0.963837};
	float jaspar_cne_pwm_23_matrix_row_2_[15] = { -1.331220, -2.831421,  1.404990, -8.134726, -8.134726, -8.134726, -0.310280, -8.134726, -1.331220,  1.513934,  1.440327, -1.736131, -8.134726, -2.427615, -3.519605};
	float jaspar_cne_pwm_23_matrix_row_3_[15] = {  0.729203,  1.016858, -2.668777, -8.375888, -8.375888, -3.072583, -2.381926, -2.668777,  1.212958, -8.375888, -8.375888, -8.375888, -8.375888, -1.371913,  0.218452};
	float *jaspar_cne_pwm_23_matrix[4] = { jaspar_cne_pwm_23_matrix_row_0_, jaspar_cne_pwm_23_matrix_row_1_, jaspar_cne_pwm_23_matrix_row_2_, jaspar_cne_pwm_23_matrix_row_3_};
	PWM jaspar_cne_pwm_23_ = {
/* accession        */ "CN0124",
/* name             */ "LM124",
/* label            */ " CN0124	22.5881500368539	LM124	unknown	; consensus \"YTGCCAAATGGAAAW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGCCAAATGGAAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_23_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.521141,
/* max_score        */ 18.195934,
/* threshold        */ 0.796,
/* info content     */ 22.561342,
/* base_counts      */ {1031, 368, 493, 433},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 155
};

	float jaspar_cne_pwm_24_matrix_row_0_[15] = { -1.497128, -7.204238,  1.251292, -7.204238,  1.138839, -7.204238, -7.204238, -7.204238, -7.204238, -7.204238,  1.272341,  1.207816,  1.229790,  0.866980, -0.200264};
	float jaspar_cne_pwm_24_matrix_row_1_[15] = {  1.253282,  1.513503, -2.347956,  1.492454, -6.963077, -6.963077,  1.513503, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077, -1.659772, -6.963077, -1.659772};
	float jaspar_cne_pwm_24_matrix_row_2_[15] = { -0.969115, -6.963077, -6.963077, -6.963077, -6.963077, -1.659772, -6.963077, -6.963077,  1.470952,  1.492454, -6.963077, -1.659772, -6.963077, -0.410569,  1.168748};
	float jaspar_cne_pwm_24_matrix_row_3_[15] = { -1.210277, -7.204238, -7.204238, -2.589118, -0.805644,  1.229790, -7.204238,  1.272341, -1.900934, -2.589118, -7.204238, -2.589118, -7.204238, -0.400733, -2.589118};
	float *jaspar_cne_pwm_24_matrix[4] = { jaspar_cne_pwm_24_matrix_row_0_, jaspar_cne_pwm_24_matrix_row_1_, jaspar_cne_pwm_24_matrix_row_2_, jaspar_cne_pwm_24_matrix_row_3_};
	PWM jaspar_cne_pwm_24_ = {
/* accession        */ "CN0231",
/* name             */ "LM231",
/* label            */ " CN0231	24.3421214508928	LM231	unknown	; consensus \"CCACATCTGGAAAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CCACATCTGGAAAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_24_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.017860,
/* max_score        */ 19.374086,
/* threshold        */ 0.805,
/* info content     */ 24.251677,
/* base_counts      */ {274, 185, 142, 119},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_25_matrix_row_0_[19] = {  0.312894, -0.092293,  0.195181,  0.888016,  1.111097, -7.183203, -2.568082,  1.111097,  1.228852, -0.497342, -1.476093, -2.568082, -7.183203, -7.183203, -7.183203,  1.272328, -7.183203, -2.568082, -0.012314};
	float jaspar_cne_pwm_25_matrix_row_1_[19] = { -1.234931, -0.948079, -1.638736, -0.725435, -6.942041, -2.326920,  0.841600, -0.389533, -2.326920, -6.942041, -6.942041, -6.942041, -6.942041, -1.638736,  1.513490, -6.942041, -6.942041,  1.470014, -0.948079};
	float jaspar_cne_pwm_25_matrix_row_2_[19] = { -0.033286,  0.148869,  0.754626, -0.256180, -1.234931,  1.300979,  0.228848, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041,  1.470014,  1.352259, -6.942041, -6.942041,  1.470014, -6.942041, -6.942041};
	float jaspar_cne_pwm_25_matrix_row_3_[19] = {  0.195181,  0.366933, -0.630695, -1.879898, -1.189241, -0.497342, -0.379698, -7.183203, -2.568082,  1.085785,  1.206384,  1.250826, -1.879898, -0.966597, -7.183203, -7.183203, -1.879898, -2.568082,  0.823498};
	float *jaspar_cne_pwm_25_matrix[4] = { jaspar_cne_pwm_25_matrix_row_0_, jaspar_cne_pwm_25_matrix_row_1_, jaspar_cne_pwm_25_matrix_row_2_, jaspar_cne_pwm_25_matrix_row_3_};
	PWM jaspar_cne_pwm_25_ = {
/* accession        */ "CN0137",
/* name             */ "LM137",
/* label            */ " CN0137	23.4214435483514	LM137	unknown	; consensus \"WNRRAGBAATTTGGCAGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NNRAAGCAATTTGGCAGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_25_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -107.146431,
/* max_score        */ 20.830706,
/* threshold        */ 0.836,
/* info content     */ 23.337290,
/* base_counts      */ {277, 145, 236, 235},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_26_matrix_row_0_[15] = { -6.916834,  1.089867, -6.916834, -2.301713, -6.916834,  1.243970, -6.916834, -6.916834, -2.301713, -6.916834, -6.916834, -6.916834, -6.916834, -2.301713, -1.613529};
	float jaspar_cne_pwm_26_matrix_row_1_[15] = {  0.974497, -1.372367, -6.675672,  1.395547,  1.513295, -6.675672, -6.675672,  1.485132, -2.060551, -6.675672, -6.675672,  1.513295, -0.968562,  1.363808,  1.225705};
	float jaspar_cne_pwm_26_matrix_row_2_[15] = {  0.569270, -6.675672,  1.513295, -0.968562, -6.675672, -6.675672,  1.456153, -2.060551, -6.675672,  1.513295,  1.513295, -6.675672, -6.675672, -0.681710, -6.675672};
	float jaspar_cne_pwm_26_matrix_row_3_[15] = { -2.301713, -0.922872, -6.916834, -6.916834, -6.916834, -2.301713, -1.613529, -6.916834,  1.214991, -6.916834, -6.916834, -6.916834,  1.185147, -6.916834, -0.364326};
	float *jaspar_cne_pwm_26_matrix[4] = { jaspar_cne_pwm_26_matrix_row_0_, jaspar_cne_pwm_26_matrix_row_1_, jaspar_cne_pwm_26_matrix_row_2_, jaspar_cne_pwm_26_matrix_row_3_};
	PWM jaspar_cne_pwm_26_ = {
/* accession        */ "CN0176",
/* name             */ "LM176",
/* label            */ " CN0176	24.3378041695922	LM176	unknown	; consensus \"GAGCCAGCTGGCTCM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAGCCAGCTGGCTCC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_26_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.787857,
/* max_score        */ 20.201290,
/* threshold        */ 0.805,
/* info content     */ 24.218515,
/* base_counts      */ {70, 224, 164, 82},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 36
};

	float jaspar_cne_pwm_27_matrix_row_0_[17] = { -0.598356,  1.192362, -2.669097,  1.272389,  1.272389, -1.577107, -1.577107, -0.039276,  1.272389, -1.577107,  1.272389, -0.113329, -7.284217, -7.284217, -1.980912,  0.958802, -2.669097};
	float jaspar_cne_pwm_27_matrix_row_1_[17] = { -7.043055, -7.043055, -7.043055, -7.043055, -7.043055, -2.427935, -0.357194, -2.427935, -7.043055, -2.427935, -7.043055, -1.739750, -1.049094, -7.043055, -1.739750, -7.043055,  1.433524};
	float jaspar_cne_pwm_27_matrix_row_2_[17] = {  1.346532, -1.739750, -7.043055, -7.043055, -7.043055, -7.043055, -2.427935,  1.173303, -7.043055,  1.433524, -7.043055,  1.173303, -7.043055, -2.427935,  1.433524,  0.127833, -7.043055};
	float jaspar_cne_pwm_27_matrix_row_3_[17] = { -7.284217, -1.980912,  1.252975, -7.284217, -7.284217,  1.192362,  1.010082, -7.284217, -7.284217, -7.284217, -7.284217, -7.284217,  1.192362,  1.252975, -7.284217, -2.669097, -1.577107};
	float *jaspar_cne_pwm_27_matrix[4] = { jaspar_cne_pwm_27_matrix_row_0_, jaspar_cne_pwm_27_matrix_row_1_, jaspar_cne_pwm_27_matrix_row_2_, jaspar_cne_pwm_27_matrix_row_3_};
	PWM jaspar_cne_pwm_27_ = {
/* accession        */ "CN0117",
/* name             */ "LM117",
/* label            */ " CN0117	26.4251191691821	LM117	unknown	; consensus \"GATAATTGAGAGTTGAC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GATAATTGAGAGTTGAC",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_27_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -117.769600,
/* max_score        */ 21.135191,
/* threshold        */ 0.845,
/* info content     */ 26.336908,
/* base_counts      */ {342, 67, 231, 244},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_28_matrix_row_0_[17] = {  1.129233, -0.336250,  0.761622, -7.139756,  1.203322,  1.272299,  0.031133, -7.139756, -7.139756,  1.249831, -1.836451, -7.139756, -7.139756,  0.931463, -7.139756, -7.139756, -1.432645};
	float jaspar_cne_pwm_28_matrix_row_1_[17] = { -1.191483, -2.283473, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593,  1.074217,  1.290373, -6.898593,  0.192316, -6.898593, -1.595289,  0.272295, -0.346085,  1.370395, -1.595289};
	float jaspar_cne_pwm_28_matrix_row_2_[17] = { -1.191483,  1.262210,  0.597504,  1.468009, -1.191483, -6.898593,  1.140886, -6.898593, -0.095088, -2.283473, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -2.283473};
	float jaspar_cne_pwm_28_matrix_row_3_[17] = { -7.139756, -7.139756, -7.139756, -1.836451, -7.139756, -7.139756, -2.524635,  0.238628, -7.139756, -7.139756,  0.899724,  1.272299,  1.226847, -7.139756,  1.103264, -0.741161,  1.129233};
	float *jaspar_cne_pwm_28_matrix[4] = { jaspar_cne_pwm_28_matrix_row_0_, jaspar_cne_pwm_28_matrix_row_1_, jaspar_cne_pwm_28_matrix_row_2_, jaspar_cne_pwm_28_matrix_row_3_};
	PWM jaspar_cne_pwm_28_ = {
/* accession        */ "CN0152",
/* name             */ "LM152",
/* label            */ " CN0152	23.8612986107125	LM152	unknown	; consensus \"WRRKAARYSAYTTMTCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGAGAAGCCATTTATCT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_28_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -116.037231,
/* max_score        */ 19.785227,
/* threshold        */ 0.822,
/* info content     */ 23.762356,
/* base_counts      */ {256, 144, 144, 221},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 45
};

	float jaspar_cne_pwm_29_matrix_row_0_[14] = {  0.056455,  1.272410, -7.321929,  0.419170,  1.133601,  1.234677, -1.105323, -7.321929, -7.321929, -7.321929, -7.321929, -7.321929,  1.272410, -1.105323};
	float jaspar_cne_pwm_29_matrix_row_1_[14] = {  1.162252, -7.080767, -7.080767, -7.080767, -1.373657, -7.080767, -7.080767,  1.513572, -7.080767, -7.080767, -1.777462,  1.513572, -7.080767, -1.373657};
	float jaspar_cne_pwm_29_matrix_row_2_[14] = { -7.080767, -7.080767, -7.080767, -7.080767, -7.080767, -1.777462,  1.416427, -7.080767, -7.080767,  1.513572, -7.080767, -7.080767, -7.080767,  1.353262};
	float jaspar_cne_pwm_29_matrix_row_3_[14] = { -7.321929, -7.321929,  1.272410,  0.717551, -1.327968, -7.321929, -7.321929, -7.321929,  1.272410, -7.321929,  1.234677, -7.321929, -7.321929, -7.321929};
	float *jaspar_cne_pwm_29_matrix[4] = { jaspar_cne_pwm_29_matrix_row_0_, jaspar_cne_pwm_29_matrix_row_1_, jaspar_cne_pwm_29_matrix_row_2_, jaspar_cne_pwm_29_matrix_row_3_};
	PWM jaspar_cne_pwm_29_ = {
/* accession        */ "CN0097",
/* name             */ "LM97",
/* label            */ " CN0097	23.806312505803	LM97	unknown	; consensus \"CATWAAGCTGTCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATTAAGCTGTCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_29_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.024689,
/* max_score        */ 17.882809,
/* threshold        */ 0.793,
/* info content     */ 23.718700,
/* base_counts      */ {256, 154, 151, 195},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 54
};

	float jaspar_cne_pwm_30_matrix_row_0_[15] = { -1.845190, -1.335694, -7.552300, -2.937180, -2.937180,  0.690719, -7.552300, -7.552300, -7.552300, -0.748795, -7.552300, -2.937180, -2.937180,  1.272525, -2.937180};
	float jaspar_cne_pwm_30_matrix_row_1_[15] = {  1.405070, -2.007833, -7.311138, -7.311138,  1.498874, -7.311138, -7.311138,  1.498874,  1.483838, -7.311138, -0.307164,  1.437325,  1.498874, -7.311138, -0.507633};
	float jaspar_cne_pwm_30_matrix_row_2_[15] = { -1.604028, -7.311138,  1.513687,  1.483838, -7.311138, -7.311138,  1.513687, -7.311138, -7.311138, -7.311138,  1.319562, -2.696018, -7.311138, -7.311138,  1.301547};
	float jaspar_cne_pwm_30_matrix_row_3_[15] = { -2.937180,  1.163908, -7.552300, -2.937180, -7.552300,  0.454401, -7.552300, -2.937180, -2.248995,  1.130577, -2.937180, -1.845190, -7.552300, -7.552300, -1.845190};
	float *jaspar_cne_pwm_30_matrix[4] = { jaspar_cne_pwm_30_matrix_row_0_, jaspar_cne_pwm_30_matrix_row_1_, jaspar_cne_pwm_30_matrix_row_2_, jaspar_cne_pwm_30_matrix_row_3_};
	PWM jaspar_cne_pwm_30_ = {
/* accession        */ "CN0090",
/* name             */ "LM90",
/* label            */ " CN0090	24.3606619768356	LM90	unknown	; consensus \"CTGGCAGCCTGCCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGGCAGCCTGCCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_30_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.474487,
/* max_score        */ 20.212902,
/* threshold        */ 0.805,
/* info content     */ 24.298607,
/* base_counts      */ {128, 413, 317, 162},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 68
};

	float jaspar_cne_pwm_31_matrix_row_0_[17] = { -1.393421, -4.108022,  1.173262, -2.623555, -1.017309,  1.243656, -3.920029,  0.653220, -2.850592, -3.625303, -3.285857, -1.173026,  0.461358, -1.373082, -1.393421, -0.233573, -0.793542};
	float jaspar_cne_pwm_31_matrix_row_1_[17] = {  1.321915,  1.503274, -3.123463,  0.615217,  1.023924, -3.768453, -4.668783, -2.985863, -3.191274, -4.343574, -2.446926,  1.219755, -2.903636,  1.174886, -0.407945,  0.102442, -0.061301};
	float jaspar_cne_pwm_31_matrix_row_2_[17] = { -1.585491, -4.098572, -1.831779,  0.937666, -1.654733, -3.075462,  1.503755,  0.710704,  0.595133,  1.499578,  1.394951, -2.159265,  0.843260, -0.093248, -2.292787,  0.837657, -0.404696};
	float jaspar_cne_pwm_31_matrix_row_3_[17] = { -1.535465, -4.702449, -1.723059, -3.256008, -0.136768, -3.021199, -4.641861, -4.530698,  0.721052, -4.217182, -1.217976, -0.672147, -2.152604, -2.840837,  1.001127, -2.369657,  0.618413};
	float *jaspar_cne_pwm_31_matrix[4] = { jaspar_cne_pwm_31_matrix_row_0_, jaspar_cne_pwm_31_matrix_row_1_, jaspar_cne_pwm_31_matrix_row_2_, jaspar_cne_pwm_31_matrix_row_3_};
	PWM jaspar_cne_pwm_31_ = {
/* accession        */ "CN0007",
/* name             */ "LM7",
/* label            */ " CN0007	18.3654401914699	LM7	unknown	; consensus \"NNNCCAGCAGATGGCGCTRT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CCAGCAGATGGCGCTGT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_31_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -51.470505,
/* max_score        */ 18.728836,
/* threshold        */ 0.774,
/* info content     */ 18.365118,
/* base_counts      */ {23123, 31252, 36691, 16068},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 6302
};

	float jaspar_cne_pwm_32_matrix_row_0_[14] = { -2.725144, -1.633154, -7.340265, -7.340265, -2.036960, -7.340265, -7.340265,  1.235386,  1.272420, -7.340265, -2.725144, -7.340265, -1.346303,  1.235386};
	float jaspar_cne_pwm_32_matrix_row_1_[14] = {  0.502300, -7.099102, -1.391992,  1.513582, -7.099102,  1.243975, -7.099102, -7.099102, -7.099102, -7.099102, -7.099102, -2.483982,  1.377477, -7.099102};
	float jaspar_cne_pwm_32_matrix_row_2_[14] = { -7.099102, -7.099102,  1.243975, -7.099102, -7.099102, -1.391992, -7.099102, -1.795798, -7.099102, -7.099102, -2.483982,  1.495237, -1.391992, -7.099102};
	float jaspar_cne_pwm_32_matrix_row_3_[14] = {  0.791560,  1.216341, -0.431510, -7.340265,  1.235386, -0.431510,  1.272420, -7.340265, -7.340265,  1.272420,  1.235386, -7.340265, -7.340265, -2.036960};
	float *jaspar_cne_pwm_32_matrix[4] = { jaspar_cne_pwm_32_matrix_row_0_, jaspar_cne_pwm_32_matrix_row_1_, jaspar_cne_pwm_32_matrix_row_2_, jaspar_cne_pwm_32_matrix_row_3_};
	PWM jaspar_cne_pwm_32_ = {
/* accession        */ "CN0126",
/* name             */ "LM126",
/* label            */ " CN0126	22.9392974244307	LM126	unknown	; consensus \"YTGCTCTAATTGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGCTCTAATTGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_32_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.557892,
/* max_score        */ 17.640953,
/* threshold        */ 0.790,
/* info content     */ 22.862886,
/* base_counts      */ {172, 169, 105, 324},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 55
};

	float jaspar_cne_pwm_33_matrix_row_0_[17] = { -3.112027, -0.413261, -1.510541, -1.174639, -0.818393, -7.727148, -7.727148,  1.168619,  1.182223,  1.021316, -1.733186, -7.727148, -7.727148, -1.733186, -7.727148,  1.097678, -7.727148};
	float jaspar_cne_pwm_33_matrix_row_1_[17] = { -0.933477,  1.262478, -1.269379,  0.926070, -7.485985, -2.870865, -7.485985, -1.778875, -1.492024, -7.485985, -0.933477, -2.182680, -1.492024,  1.367823,  1.501337, -7.485985, -0.682480};
	float jaspar_cne_pwm_33_matrix_row_2_[17] = {  1.308991, -2.870865, -1.778875, -0.577231, -7.485985,  1.463120,  1.513757, -1.269379, -2.182680, -1.492024, -2.182680, -2.182680, -7.485985, -2.870865, -7.485985, -2.182680,  1.278224};
	float jaspar_cne_pwm_33_matrix_row_3_[17] = { -1.174639, -2.423842,  1.097678, -0.177012,  1.140844, -2.020037, -7.727148, -7.727148, -3.112027, -0.482206,  1.097678,  1.221958,  1.221958, -1.328552, -3.112027, -0.723173, -1.041286};
	float *jaspar_cne_pwm_33_matrix[4] = { jaspar_cne_pwm_33_matrix_row_0_, jaspar_cne_pwm_33_matrix_row_1_, jaspar_cne_pwm_33_matrix_row_2_, jaspar_cne_pwm_33_matrix_row_3_};
	PWM jaspar_cne_pwm_33_ = {
/* accession        */ "CN0199",
/* name             */ "LM199",
/* label            */ " CN0199	22.6897025497925	LM199	unknown	; consensus \"GCTCTGGAAATTTCCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCTCTGGAAATTTCCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_33_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.649971,
/* max_score        */ 20.871754,
/* threshold        */ 0.812,
/* info content     */ 22.648558,
/* base_counts      */ {324, 300, 320, 433},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 81
};

	float jaspar_cne_pwm_34_matrix_row_0_[16] = { -0.193329, -7.689426, -3.074306, -1.695464, -7.689426,  1.246609, -3.074306, -1.136918,  1.206341,  1.246609,  1.272581, -2.386121, -1.982316, -3.074306, -7.689426,  0.381792};
	float jaspar_cne_pwm_34_matrix_row_1_[16] = { -0.134377, -2.144959, -2.833143,  1.088928,  1.461106, -7.448264, -7.448264, -2.144959, -2.833143, -2.144959, -7.448264, -1.454302, -7.448264, -2.833143,  1.474528,  0.153138};
	float jaspar_cne_pwm_34_matrix_row_2_[16] = { -1.049669, -2.833143, -1.049669,  0.101871, -7.448264, -7.448264,  1.461106,  1.361748, -2.144959, -7.448264, -7.448264, -2.833143, -1.741154,  1.447503, -2.833143, -7.448264};
	float jaspar_cne_pwm_34_matrix_row_3_[16] = {  0.579562,  1.233366,  1.164382, -1.695464, -1.695464, -2.386121, -1.982316, -2.386121, -2.386121, -7.689426, -7.689426,  1.178565,  1.192549, -1.982316, -2.386121,  0.174225};
	float *jaspar_cne_pwm_34_matrix[4] = { jaspar_cne_pwm_34_matrix_row_0_, jaspar_cne_pwm_34_matrix_row_1_, jaspar_cne_pwm_34_matrix_row_2_, jaspar_cne_pwm_34_matrix_row_3_};
	PWM jaspar_cne_pwm_34_ = {
/* accession        */ "CN0078",
/* name             */ "LM78",
/* label            */ " CN0078	22.6061884041905	LM78	unknown	; consensus \"WTTSCAGGAAATTGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NTTCCAGGAAATTGCN",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_34_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.186333,
/* max_score        */ 18.997274,
/* threshold        */ 0.804,
/* info content     */ 22.564079,
/* base_counts      */ {372, 248, 253, 375},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 78
};

	float jaspar_cne_pwm_35_matrix_row_0_[19] = { -2.778216, -1.399375,  1.272449,  1.040692, -2.778216, -1.399375, -0.840829, -0.707476, -1.399375, -2.778216,  1.062194, -7.393337, -1.399375,  1.163269,  1.272449,  1.237363, -1.686227, -1.176731,  1.143855};
	float jaspar_cne_pwm_35_matrix_row_1_[19] = {  0.161712, -0.599667, -7.152175, -2.537054, -1.158213, -7.152175, -2.537054,  0.854526,  1.303356,  1.460510, -1.445065, -2.537054, -7.152175, -2.537054, -7.152175, -2.537054, -7.152175, -0.466314, -2.537054};
	float jaspar_cne_pwm_35_matrix_row_2_[19] = { -7.152175, -1.445065, -7.152175, -0.466314, -1.848870,  1.345020,  1.303356, -0.935569, -0.935569, -7.152175, -2.537054, -0.599667,  1.404431, -1.445065, -7.152175, -7.152175, -1.848870,  1.036792, -0.753580};
	float jaspar_cne_pwm_35_matrix_row_3_[19] = {  0.949741,  0.996250, -7.393337, -1.686227,  1.143855, -1.176731, -1.686227, -0.079450, -2.090032, -2.090032, -0.840829,  1.124056, -2.090032, -2.090032, -7.393337, -2.778216,  1.182314, -0.589832, -7.393337};
	float *jaspar_cne_pwm_35_matrix[4] = { jaspar_cne_pwm_35_matrix_row_0_, jaspar_cne_pwm_35_matrix_row_1_, jaspar_cne_pwm_35_matrix_row_2_, jaspar_cne_pwm_35_matrix_row_3_};
	PWM jaspar_cne_pwm_35_ = {
/* accession        */ "CN0190",
/* name             */ "LM190",
/* label            */ " CN0190	23.7872543408499	LM190	unknown	; consensus \"TTAATGGCCCATGAAATGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTAATGGCCCATGAAATGA",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_35_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.060226,
/* max_score        */ 22.296476,
/* threshold        */ 0.841,
/* info content     */ 23.730961,
/* base_counts      */ {410, 175, 226, 291},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 58
};

	float jaspar_cne_pwm_36_matrix_row_0_[18] = { -0.771866, -0.874802, -0.441655, -1.767108, -3.904972, -2.459962, -2.035282, -5.283813, -5.283813,  0.286005,  0.543997, -2.459962, -4.191823,  0.912833,  1.237129,  1.255902, -3.682327,  0.326674};
	float jaspar_cne_pwm_36_matrix_row_1_[18] = { -0.298924,  0.874352, -0.958090, -4.354466, -2.056369,  0.582224, -2.486883,  1.495633,  1.482655, -0.645760, -3.105263, -9.657771, -4.354466, -0.586578, -2.971910, -3.105263,  1.457672,  0.606916};
	float jaspar_cne_pwm_36_matrix_row_2_[18] = {  0.204947, -0.847759, -0.184990,  1.436619, -2.971910, -3.441165, -0.941563, -3.950661, -9.657771, -5.042651, -1.291168,  1.485553,  1.504192, -1.586553, -2.566861, -4.354466, -2.007602, -1.555790};
	float jaspar_cne_pwm_36_matrix_row_3_[18] = {  0.446062, -0.067963,  0.680072, -2.459962,  1.226873,  0.719977,  1.121350, -3.095428, -2.248765,  0.600667,  0.468320, -4.191823, -4.595628, -0.729311, -3.682327, -4.191823, -2.728045, -0.548744};
	float *jaspar_cne_pwm_36_matrix[4] = { jaspar_cne_pwm_36_matrix_row_0_, jaspar_cne_pwm_36_matrix_row_1_, jaspar_cne_pwm_36_matrix_row_2_, jaspar_cne_pwm_36_matrix_row_3_};
	PWM jaspar_cne_pwm_36_ = {
/* accession        */ "CN0039",
/* name             */ "LM39",
/* label            */ " CN0039	20.8799007782519	LM39	unknown	; consensus \"KNKGTYTCCTAGGAAACM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NCTGTTTCCTWGGAAACM",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_36_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -68.996605,
/* max_score        */ 19.088453,
/* threshold        */ 0.800,
/* info content     */ 20.876328,
/* base_counts      */ {3176, 3405, 2663, 3554},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 711
};

	float jaspar_cne_pwm_37_matrix_row_0_[14] = { -0.587248, -7.139756, -7.139756, -7.139756, -7.139756,  1.272299, -7.139756, -7.139756, -7.139756, -7.139756, -7.139756, -7.139756, -7.139756,  1.272299};
	float jaspar_cne_pwm_37_matrix_row_1_[14] = { -6.898593, -0.346085, -6.898593, -6.898593, -6.898593, -6.898593,  1.513461,  1.513461, -6.898593, -6.898593, -6.898593, -6.898593,  1.513461, -6.898593};
	float jaspar_cne_pwm_37_matrix_row_2_[14] = { -0.499998, -0.212732, -6.898593, -6.898593,  1.513461, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593, -6.898593};
	float jaspar_cne_pwm_37_matrix_row_3_[14] = {  0.931463,  0.866945,  1.272299,  1.272299, -7.139756, -7.139756, -7.139756, -7.139756,  1.272299,  1.272299,  1.272299,  1.272299, -7.139756, -7.139756};
	float *jaspar_cne_pwm_37_matrix[4] = { jaspar_cne_pwm_37_matrix_row_0_, jaspar_cne_pwm_37_matrix_row_1_, jaspar_cne_pwm_37_matrix_row_2_, jaspar_cne_pwm_37_matrix_row_3_};
	PWM jaspar_cne_pwm_37_ = {
/* accession        */ "CN0167",
/* name             */ "LM167",
/* label            */ " CN0167	25.5945040691493	LM167	unknown	; consensus \"WYTTGACCTTTTCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTTGACCTTTTCA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_37_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.715416,
/* max_score        */ 18.030647,
/* threshold        */ 0.798,
/* info content     */ 25.479445,
/* base_counts      */ {97, 142, 59, 332},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 45
};

	float jaspar_cne_pwm_38_matrix_row_0_[19] = { -0.395508, -0.374006, -1.658648, -1.333439,  1.093803,  1.235262, -1.228134, -2.835575, -1.088438, -0.560548,  1.103558,  1.155577,  0.996043, -1.584595,  1.222414, -3.526232, -1.279401, -1.045896, -4.214416};
	float jaspar_cne_pwm_38_matrix_row_1_[19] = { -0.615564, -1.784869, -0.686997, -0.763929, -1.784869, -8.588374, -1.584400, -3.973254, -2.594413, -0.686997, -1.343433, -1.679620, -0.891707,  1.387480, -3.973254, -2.881264,  1.294961, -2.594413, -2.881264};
	float jaspar_cne_pwm_38_matrix_row_2_[19] = {  0.861061, -0.427571,  1.175908, -0.517156, -0.986972, -2.594413, -1.038239, -0.581674,  1.349563,  1.116723, -1.584400, -1.584400, -0.399408, -1.902514, -2.881264,  1.344720, -1.149403, -3.973254,  1.459257};
	float jaspar_cne_pwm_38_matrix_row_3_[19] = { -0.486459,  0.806137, -0.822836,  0.905591, -1.825562, -2.612931,  1.043543,  1.118016, -1.920782, -1.584595, -1.658648, -2.430942, -4.214416, -2.277029, -2.143676, -0.727556, -1.738627,  1.146318, -2.026031};
	float *jaspar_cne_pwm_38_matrix[4] = { jaspar_cne_pwm_38_matrix_row_0_, jaspar_cne_pwm_38_matrix_row_1_, jaspar_cne_pwm_38_matrix_row_2_, jaspar_cne_pwm_38_matrix_row_3_};
	PWM jaspar_cne_pwm_38_ = {
/* accession        */ "CN0044",
/* name             */ "LM44",
/* label            */ " CN0044	20.6570285246285	LM44	unknown	; consensus \"GTGTAATTGGAAACAGCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GTGTAATTGGAAACAGCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_38_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -53.549942,
/* max_score        */ 21.815937,
/* threshold        */ 0.801,
/* info content     */ 20.648544,
/* base_counts      */ {1549, 610, 1336, 1141},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 244
};

	float jaspar_cne_pwm_39_matrix_row_0_[20] = { -7.245027, -7.245027, -2.629907, -1.028421, -0.692519, -1.941722, -1.251066, -7.245027, -7.245027, -0.241053,  0.997992,  1.210503,  0.886797,  0.133357, -1.028421, -1.028421, -1.537917, -0.336272,  1.252167,  1.231552};
	float jaspar_cne_pwm_39_matrix_row_1_[20] = { -1.296755, -2.388745, -1.700560,  1.265123, -0.605270,  1.339213, -0.787259, -2.388745, -7.003865, -0.451357, -0.605270, -1.700560, -1.296755,  0.737234,  1.290434, -7.003865, -7.003865,  1.098116, -7.003865, -2.388745};
	float jaspar_cne_pwm_39_matrix_row_2_[20] = { -7.003865, -1.009904, -1.700560, -1.009904, -7.003865, -1.296755, -7.003865, -2.388745, -7.003865,  0.933867, -0.605270, -2.388745, -0.318004, -0.451357, -1.296755, -7.003865,  1.385722, -1.009904, -7.003865, -7.003865};
	float jaspar_cne_pwm_39_matrix_row_3_[20] = {  1.210503,  1.167028,  1.167028, -1.941722,  0.971331, -1.537917,  1.073959,  1.231552,  1.272366, -1.251066, -7.245027, -7.245027, -1.028421, -1.251066, -1.941722,  1.167028, -1.537917, -1.537917, -2.629907, -2.629907};
	float *jaspar_cne_pwm_39_matrix[4] = { jaspar_cne_pwm_39_matrix_row_0_, jaspar_cne_pwm_39_matrix_row_1_, jaspar_cne_pwm_39_matrix_row_2_, jaspar_cne_pwm_39_matrix_row_3_};
	PWM jaspar_cne_pwm_39_ = {
/* accession        */ "CN0216",
/* name             */ "LM216",
/* label            */ " CN0216	24.1921364522252	LM216	unknown	; consensus \"TTTCTCTTTGAAACCTGCAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTCTCTTTGAAAMCTGCAA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_39_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.285233,
/* max_score        */ 22.889515,
/* threshold        */ 0.856,
/* info content     */ 24.122858,
/* base_counts      */ {285, 214, 115, 386},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_40_matrix_row_0_[16] = {  1.203322, -7.139756,  1.226847,  1.154544,  1.226847,  1.249831, -7.139756, -0.923149, -0.741161,  1.272299, -2.524635, -1.432645,  1.272299,  1.226847,  1.249831, -2.524635};
	float jaspar_cne_pwm_40_matrix_row_1_[16] = { -6.898593,  1.468009, -1.595289, -6.898593, -2.283473, -6.898593, -2.283473,  1.074217,  1.370395, -6.898593, -6.898593, -6.898593, -6.898593, -1.595289, -6.898593, -1.191483};
	float jaspar_cne_pwm_40_matrix_row_2_[16] = { -6.898593, -6.898593, -6.898593, -0.681987, -6.898593, -2.283473,  1.490994, -0.499998, -6.898593, -6.898593,  1.468009,  1.444484, -6.898593, -6.898593, -2.283473, -1.595289};
	float jaspar_cne_pwm_40_matrix_row_3_[16] = { -1.432645, -1.836451, -7.139756, -7.139756, -2.524635, -7.139756, -7.139756, -0.923149, -7.139756, -7.139756, -2.524635, -7.139756, -7.139756, -7.139756, -7.139756,  1.129233};
	float *jaspar_cne_pwm_40_matrix[4] = { jaspar_cne_pwm_40_matrix_row_0_, jaspar_cne_pwm_40_matrix_row_1_, jaspar_cne_pwm_40_matrix_row_2_, jaspar_cne_pwm_40_matrix_row_3_};
	PWM jaspar_cne_pwm_40_ = {
/* accession        */ "CN0138",
/* name             */ "LM138",
/* label            */ " CN0138	26.1002708535711	LM138	unknown	; consensus \"ACAAAAGCCAGGAAAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACAAAAGCCAGGAAAT",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_40_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.680870,
/* max_score        */ 20.528011,
/* threshold        */ 0.828,
/* info content     */ 26.000952,
/* base_counts      */ {405, 120, 144, 51},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 45
};

	float jaspar_cne_pwm_41_matrix_row_0_[20] = {  1.015560, -7.650226, -3.035105, -2.346921, -2.346921,  0.644074, -1.097718, -2.346921, -3.035105, -0.271842, -3.035105, -2.346921, -0.741471, -1.943116,  1.113984,  1.082240,  1.144751, -1.943116,  1.217765, -0.846721};
	float jaspar_cne_pwm_41_matrix_row_1_[20] = { -0.605559,  1.415761,  1.400948,  1.430358, -0.500309, -2.793943, -1.701954, -1.701954, -7.409064, -1.192458,  1.430358,  1.385913, -2.793943, -1.010469, -2.793943, -1.192458, -7.409064, -2.105759, -7.409064, -1.192458};
	float jaspar_cne_pwm_41_matrix_row_2_[20] = { -1.010469, -1.701954, -2.793943, -7.409064, -7.409064,  0.597637,  1.355146, -1.701954,  1.500307,  1.128128, -2.793943, -2.793943, -2.105759,  1.339400, -0.605559, -0.856556, -1.415102,  1.370648, -2.793943,  1.307144};
	float jaspar_cne_pwm_41_matrix_row_3_[20] = { -2.346921, -1.656264, -1.251631, -1.656264,  1.098238, -1.656264, -3.035105,  1.159786, -7.650226, -1.943116, -1.656264, -1.251631,  1.082240, -1.943116, -3.035105, -3.035105, -1.433620, -1.433620, -1.943116, -7.650226};
	float *jaspar_cne_pwm_41_matrix[4] = { jaspar_cne_pwm_41_matrix_row_0_, jaspar_cne_pwm_41_matrix_row_1_, jaspar_cne_pwm_41_matrix_row_2_, jaspar_cne_pwm_41_matrix_row_3_};
	PWM jaspar_cne_pwm_41_ = {
/* accession        */ "CN0135",
/* name             */ "LM135",
/* label            */ " CN0135	25.2610814423723	LM135	unknown	; consensus \"ACCCTRGTGGCCTGAAAGAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACCCTAGTGGCCTGAAAGAG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_41_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.830116,
/* max_score        */ 24.622746,
/* threshold        */ 0.872,
/* info content     */ 25.221046,
/* base_counts      */ {420, 390, 446, 244},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 75
};

	float jaspar_cne_pwm_42_matrix_row_0_[15] = {  1.110372,  1.202014, -1.299670, -1.334749, -1.299670, -2.101593, -1.833509, -2.586620, -9.272481, -2.873886,  1.221595, -0.929403,  1.029816, -1.833509, -2.719973};
	float jaspar_cne_pwm_42_matrix_row_1_[15] = { -2.227814, -2.345458, -1.786377, -4.416198, -1.940409,  1.329625, -1.592347, -2.027345,  1.462757,  1.492772, -9.031319, -1.786377, -0.597290, -3.324209, -1.481184};
	float jaspar_cne_pwm_42_matrix_row_2_[15] = { -1.167668, -2.122564, -3.324209, -0.474713,  1.345323, -1.535222, -4.416198, -1.592347, -2.027345, -4.416198, -2.227814,  1.310456, -0.991839, -1.940409,  1.405763};
	float jaspar_cne_pwm_42_matrix_row_3_[15] = { -1.575814, -2.586620,  1.143860,  1.033166, -1.776384, -1.170500,  1.176262,  1.173360, -2.586620, -4.657361, -2.363726, -2.027539, -3.055875,  1.184920, -2.101593};
	float *jaspar_cne_pwm_42_matrix[4] = { jaspar_cne_pwm_42_matrix_row_0_, jaspar_cne_pwm_42_matrix_row_1_, jaspar_cne_pwm_42_matrix_row_2_, jaspar_cne_pwm_42_matrix_row_3_};
	PWM jaspar_cne_pwm_42_ = {
/* accession        */ "CN0062",
/* name             */ "LM62",
/* label            */ " CN0062	20.1946152010481	LM62	unknown	; consensus \"AATTGCTTCCAGATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AATTGCTTCCAGATG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_42_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -57.688416,
/* max_score        */ 18.622065,
/* threshold        */ 0.785,
/* info content     */ 20.188898,
/* base_counts      */ {1532, 1203, 1163, 1802},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 380
};

	float jaspar_cne_pwm_43_matrix_row_0_[17] = { -0.939606,  1.240985, -2.190586, -2.190586, -3.569427, -2.190586, -2.477437,  1.165642, -1.275792,  0.815196, -2.477437, -3.569427, -1.013659,  1.191392,  1.208198,  1.156909, -1.785952};
	float jaspar_cne_pwm_43_matrix_row_1_[17] = {  1.139236, -2.640080, -1.544790, -1.726779, -7.943385, -1.390877,  1.440993, -1.949424, -2.236275, -7.943385, -2.640080, -0.939411,  1.257006, -3.328264, -3.328264, -7.943385, -1.544790};
	float jaspar_cne_pwm_43_matrix_row_2_[17] = { -1.034630, -7.943385, -2.640080, -2.236275, -7.943385,  1.353225, -7.943385, -3.328264, -1.726779, -1.034630, -3.328264,  1.406804, -2.640080, -7.943385, -2.640080, -2.236275, -0.565001};
	float jaspar_cne_pwm_43_matrix_row_3_[17] = { -0.806163, -2.881242,  1.174299,  1.174299,  1.264889, -1.498686, -1.785952, -1.498686,  1.121194,  0.031811,  1.224726, -3.569427, -0.939606, -1.381042, -1.967941, -1.180573,  1.025893};
	float *jaspar_cne_pwm_43_matrix[4] = { jaspar_cne_pwm_43_matrix_row_0_, jaspar_cne_pwm_43_matrix_row_1_, jaspar_cne_pwm_43_matrix_row_2_, jaspar_cne_pwm_43_matrix_row_3_};
	PWM jaspar_cne_pwm_43_ = {
/* accession        */ "CN0051",
/* name             */ "LM51",
/* label            */ " CN0051	22.8104729274088	LM51	unknown	; consensus \"CATTTGCATATGCAAAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATTTGCATATGCAAAT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_43_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.978409,
/* max_score        */ 20.360886,
/* threshold        */ 0.813,
/* info content     */ 22.788628,
/* base_counts      */ {735, 354, 279, 808},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 128
};

	float jaspar_cne_pwm_44_matrix_row_0_[18] = { -0.741646, -1.029120, -1.116055, -1.567522,  1.194761,  1.212617, -2.126068, -1.434169,  1.264348, -1.434169, -1.721435, -0.949141, -2.816725,  1.203729,  1.129628, -1.316524, -1.316524, -2.412919};
	float jaspar_cne_pwm_44_matrix_row_1_[18] = { -1.075362, -2.575562,  1.215051, -2.171757, -3.263747, -1.884906, -2.575562, -3.263747, -3.263747, -2.171757, -1.326360,  1.192326,  1.389836, -1.662261, -2.575562, -2.171757, -3.263747, -1.884906};
	float jaspar_cne_pwm_44_matrix_row_2_[18] = { -0.970113,  1.331573, -3.263747, -1.884906, -2.171757, -2.575562, -1.662261,  1.426874, -7.878868,  1.389836,  1.389836, -2.575562, -1.662261, -2.575562, -0.874893,  1.380358,  1.389836, -1.326360};
	float jaspar_cne_pwm_44_matrix_row_3_[18] = {  0.927910, -1.721435, -0.569894,  1.148674, -1.903423, -3.504909,  1.176580, -3.504909, -8.120029, -2.412919, -3.504909, -0.623932, -1.567522, -3.504909, -2.412919, -2.412919, -2.126068,  1.148674};
	float *jaspar_cne_pwm_44_matrix[4] = { jaspar_cne_pwm_44_matrix_row_0_, jaspar_cne_pwm_44_matrix_row_1_, jaspar_cne_pwm_44_matrix_row_2_, jaspar_cne_pwm_44_matrix_row_3_};
	PWM jaspar_cne_pwm_44_ = {
/* accession        */ "CN0144",
/* name             */ "LM144",
/* label            */ " CN0144	23.5111415837395	LM144	unknown	; consensus \"TGCTAATGAGGCCAAGGT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCTAATGAGGCCAAGGT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_44_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -55.535755,
/* max_score        */ 22.512445,
/* threshold        */ 0.828,
/* info content     */ 23.490988,
/* base_counts      */ {667, 330, 685, 478},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 120
};

	float jaspar_cne_pwm_45_matrix_row_0_[15] = { -7.623212, -7.623212, -7.623212, -2.319907, -7.623212, -2.319907, -7.623212, -7.623212, -7.623212, -3.008091,  1.244779, -3.008091, -7.623212, -2.319907, -7.623212};
	float jaspar_cne_pwm_45_matrix_row_1_[15] = { -2.078745, -1.165444, -7.382050,  1.317632,  1.485941, -0.378076, -1.388088, -0.696189,  1.350416,  1.499925, -7.382050, -2.078745,  1.412927, -7.382050, -2.078745};
	float jaspar_cne_pwm_45_matrix_row_2_[15] = { -1.674940, -2.766929, -7.382050, -1.165444, -7.382050, -7.382050, -1.674940, -7.382050, -0.696189, -7.382050, -7.382050,  1.427962, -1.165444, -7.382050,  1.471759};
	float jaspar_cne_pwm_45_matrix_row_3_[15] = {  1.201613,  1.186800,  1.272555, -1.224617, -2.319907,  1.076470,  1.171765,  1.156500, -1.916102, -7.623212, -2.319907, -1.916102, -2.319907,  1.244779, -3.008091};
	float *jaspar_cne_pwm_45_matrix[4] = { jaspar_cne_pwm_45_matrix_row_0_, jaspar_cne_pwm_45_matrix_row_1_, jaspar_cne_pwm_45_matrix_row_2_, jaspar_cne_pwm_45_matrix_row_3_};
	PWM jaspar_cne_pwm_45_ = {
/* accession        */ "CN0195",
/* name             */ "LM195",
/* label            */ " CN0195	23.598578655561	LM195	unknown	; consensus \"TTTCCTTTCCAGCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTCCTTTCCAGCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_45_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.706276,
/* max_score        */ 19.521822,
/* threshold        */ 0.802,
/* info content     */ 23.546541,
/* base_counts      */ {79, 365, 162, 489},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 73
};

	float jaspar_cne_pwm_46_matrix_row_0_[15] = {  1.075191, -2.234189,  1.257483, -7.537494, -7.537494, -0.984986,  1.210970, -7.537494,  0.756806,  1.226716,  1.194972, -7.537494, -7.537494,  1.242217, -1.138899};
	float jaspar_cne_pwm_46_matrix_row_1_[15] = { -0.897737,  1.419876, -7.296332, -7.296332,  1.419876,  1.386545, -7.296332, -2.681211,  0.017555, -2.681211, -1.302370, -7.296332,  1.483379, -1.993027,  1.240860};
	float jaspar_cne_pwm_46_matrix_row_2_[15] = { -1.079726, -1.302370, -7.296332,  1.483379, -1.302370, -7.296332, -7.296332, -2.681211, -1.993027, -7.296332, -2.681211, -1.079726, -1.993027, -7.296332, -1.302370};
	float jaspar_cne_pwm_46_matrix_row_3_[15] = { -2.922374, -7.537494, -2.922374, -2.234189, -2.234189, -2.922374, -1.543532,  1.242217, -0.628739, -2.234189, -7.537494,  1.194972, -7.537494, -7.537494, -1.138899};
	float *jaspar_cne_pwm_46_matrix[4] = { jaspar_cne_pwm_46_matrix_row_0_, jaspar_cne_pwm_46_matrix_row_1_, jaspar_cne_pwm_46_matrix_row_2_, jaspar_cne_pwm_46_matrix_row_3_};
	PWM jaspar_cne_pwm_46_ = {
/* accession        */ "CN0133",
/* name             */ "LM133",
/* label            */ " CN0133	22.4359583641439	LM133	unknown	; consensus \"ACAGCCATAAATCAC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACAGCCATAAATCAC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_46_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.703041,
/* max_score        */ 18.835461,
/* threshold        */ 0.796,
/* info content     */ 22.383570,
/* base_counts      */ {430, 326, 93, 156},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 67
};

	float jaspar_cne_pwm_47_matrix_row_0_[18] = { -0.683539, -1.168567, -3.239307,  0.601103,  1.145315,  1.250663, -2.551123, -7.854428, -0.850454,  1.272640,  1.261712,  1.272640, -1.860466, -2.551123,  1.228193, -1.301920,  0.464558, -0.540541};
	float jaspar_cne_pwm_47_matrix_row_1_[18] = { -0.927405, -0.522356,  1.446368, -1.906155, -1.214671, -2.998145, -7.613266, -7.613266,  1.211559, -7.613266, -2.998145, -7.613266, -0.234882,  1.386477, -7.613266, -0.809760, -0.927405,  0.393435};
	float jaspar_cne_pwm_47_matrix_row_2_[18] = { -1.214671,  0.776321, -1.619304, -2.998145, -2.309961, -2.998145, -7.613266,  1.513802, -0.609292, -7.613266, -7.613266, -7.613266, -1.214671, -2.998145, -2.998145,  1.282501,  0.036903, -1.906155};
	float jaspar_cne_pwm_47_matrix_row_3_[18] = {  0.925283,  0.083304, -3.239307,  0.464558, -2.147317, -7.854428,  1.250663, -7.854428, -2.551123, -7.854428, -7.854428, -7.854428,  0.940549, -1.168567, -2.147317, -2.147317, -0.157761,  0.535159};
	float *jaspar_cne_pwm_47_matrix[4] = { jaspar_cne_pwm_47_matrix_row_0_, jaspar_cne_pwm_47_matrix_row_1_, jaspar_cne_pwm_47_matrix_row_2_, jaspar_cne_pwm_47_matrix_row_3_};
	PWM jaspar_cne_pwm_47_ = {
/* accession        */ "CN0079",
/* name             */ "LM79",
/* label            */ " CN0079	22.3462482682605	LM79	unknown	; consensus \"TKCWAATGCAAATCAGNY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TKCAAATGCAAATCAGNY",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_47_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.819939,
/* max_score        */ 19.765509,
/* threshold        */ 0.816,
/* info content     */ 22.309547,
/* base_counts      */ {685, 329, 266, 376},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 92
};

	float jaspar_cne_pwm_48_matrix_row_0_[20] = {  0.169039, -1.441582, -4.659978,  1.190001, -2.414410, -0.476894,  1.180311, -3.784801,  0.766235, -3.237249, -3.436555, -2.750861, -2.645508,  1.116247, -1.250965, -0.966598,  0.571970, -0.364547,  0.207061, -0.130026};
	float jaspar_cne_pwm_48_matrix_row_1_[20] = { -1.074671,  1.359112,  1.508145, -3.124452,  1.183174,  0.448223, -3.371821, -4.706331, -3.777199, -3.586189, -6.026257, -2.829978,  1.459541, -3.337341,  0.439725,  0.431155, -0.130220, -0.413793, -0.461354, -0.700865};
	float jaspar_cne_pwm_48_matrix_row_2_[20] = {  0.598299, -2.173248, -6.313108, -2.371558,  0.060864, -2.012326, -1.509659,  1.504273,  0.572480,  0.586310,  1.500788,  1.353372, -3.337341, -0.690002,  0.902477, -1.140267,  0.211527,  0.100847, -0.273795,  0.716780};
	float jaspar_cne_pwm_48_matrix_row_3_[20] = { -0.371382, -1.677299, -4.564713, -1.734605, -2.502861,  0.479726, -2.159001, -5.257406, -4.711245,  0.741075, -4.323648, -0.867227, -2.398663, -2.342955, -2.054054,  0.548465, -1.972596,  0.397680,  0.221949, -0.364547};
	float *jaspar_cne_pwm_48_matrix[4] = { jaspar_cne_pwm_48_matrix_row_0_, jaspar_cne_pwm_48_matrix_row_1_, jaspar_cne_pwm_48_matrix_row_2_, jaspar_cne_pwm_48_matrix_row_3_};
	PWM jaspar_cne_pwm_48_ = {
/* accession        */ "CN0002",
/* name             */ "LM2",
/* label            */ " CN0002	18.9915557353914	LM2	unknown	; consensus \"YNRCCACYAGATGGCAGYM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NCCACYAGATGGCAGYRNNN",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_48_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -56.401173,
/* max_score        */ 19.299618,
/* threshold        */ 0.780,
/* info content     */ 18.991272,
/* base_counts      */ {40917, 39657, 47074, 23332},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 7549
};

	float jaspar_cne_pwm_49_matrix_row_0_[15] = {  0.877140,  1.118237, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841,  1.272354, -7.224841, -7.224841,  1.272354,  1.141762, -2.609720, -1.230879};
	float jaspar_cne_pwm_49_matrix_row_1_[15] = { -0.767073, -0.585084, -6.983679, -6.983679,  1.513516,  1.055801, -6.983679,  1.513516, -6.983679, -6.983679, -6.983679, -6.983679, -0.585084,  1.359399,  0.107231};
	float jaspar_cne_pwm_49_matrix_row_2_[15] = { -0.180173, -6.983679, -6.983679,  1.513516, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -0.989717, -2.368558};
	float jaspar_cne_pwm_49_matrix_row_3_[15] = { -1.921536, -2.609720,  1.272354, -7.224841, -7.224841,  0.271257,  1.272354, -7.224841, -7.224841,  1.272354,  1.272354, -7.224841, -7.224841, -1.921536,  0.846378};
	float *jaspar_cne_pwm_49_matrix[4] = { jaspar_cne_pwm_49_matrix_row_0_, jaspar_cne_pwm_49_matrix_row_1_, jaspar_cne_pwm_49_matrix_row_2_, jaspar_cne_pwm_49_matrix_row_3_};
	PWM jaspar_cne_pwm_49_ = {
/* accession        */ "CN0072",
/* name             */ "LM72",
/* label            */ " CN0072	24.3845134874948	LM72	unknown	; consensus \"NATGCYTCATTAAMY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AATGCCTCATTAACT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_49_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.356735,
/* max_score        */ 18.573389,
/* threshold        */ 0.805,
/* info content     */ 24.289278,
/* base_counts      */ {221, 200, 63, 251},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_50_matrix_row_0_[17] = { -0.336291, -0.536759, -1.123659, -7.340265,  1.115266, -2.036960,  1.254075, -0.941670,  1.216341,  1.136315,  1.235386, -2.725144, -1.633154, -1.633154, -7.340265,  1.254075,  1.136315};
	float jaspar_cne_pwm_50_matrix_row_1_[17] = { -0.546595,  0.873708, -7.099102, -1.105141, -2.483982,  1.457504, -7.099102, -1.105141, -2.483982, -7.099102, -2.483982, -7.099102, -7.099102, -1.105141,  1.356428, -7.099102, -2.483982};
	float jaspar_cne_pwm_50_matrix_row_2_[17] = { -1.105141, -0.095129, -7.099102,  1.438089, -0.700508, -2.483982, -7.099102,  1.290484, -1.795798, -1.795798, -2.483982, -7.099102,  1.438089, -2.483982, -2.483982, -7.099102, -0.882497};
	float jaspar_cne_pwm_50_matrix_row_3_[17] = {  0.761716, -0.941670,  1.177128, -7.340265, -2.725144, -7.340265, -2.725144, -2.725144, -7.340265, -1.123659, -7.340265,  1.254075, -2.725144,  1.115266, -0.787757, -2.725144, -2.725144};
	float *jaspar_cne_pwm_50_matrix[4] = { jaspar_cne_pwm_50_matrix_row_0_, jaspar_cne_pwm_50_matrix_row_1_, jaspar_cne_pwm_50_matrix_row_2_, jaspar_cne_pwm_50_matrix_row_3_};
	PWM jaspar_cne_pwm_50_ = {
/* accession        */ "CN0188",
/* name             */ "LM188",
/* label            */ " CN0188	23.5208879545282	LM188	unknown	; consensus \"TCTGACAGAAATGTCAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCTGACAGAAATGTCAA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_50_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.002159,
/* max_score        */ 20.510260,
/* threshold        */ 0.819,
/* info content     */ 23.458874,
/* base_counts      */ {396, 151, 180, 208},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 55
};

	float jaspar_cne_pwm_51_matrix_row_0_[19] = { -2.090032, -0.589832, -2.090032, -0.302427, -2.778216,  1.237363,  1.272449, -7.393337, -2.090032,  0.949741, -2.778216,  1.201002, -7.393337, -2.778216, -2.778216, -2.090032, -7.393337, -1.399375, -2.778216};
	float jaspar_cne_pwm_51_matrix_row_1_[19] = { -1.158213, -2.537054, -7.152175, -1.848870, -1.848870, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175,  1.423476, -7.152175, -1.158213, -0.599667,  1.345020, -2.537054, -2.537054,  1.008629,  0.919044};
	float jaspar_cne_pwm_51_matrix_row_2_[19] = { -1.848870,  1.303356,  1.442164,  1.190903, -7.152175, -1.848870, -7.152175, -7.152175, -2.537054,  0.018714, -1.848870, -7.152175, -1.445065, -1.848870, -1.848870, -2.537054,  1.423476, -1.445065, -2.537054};
	float jaspar_cne_pwm_51_matrix_row_3_[19] = {  1.124056, -2.778216, -2.090032, -2.090032,  1.219348, -7.393337, -7.393337,  1.272449,  1.219348, -1.686227, -2.090032, -1.399375,  1.143855,  1.083243, -0.994742,  1.201002, -1.399375, -0.014953,  0.390304};
	float *jaspar_cne_pwm_51_matrix[4] = { jaspar_cne_pwm_51_matrix_row_0_, jaspar_cne_pwm_51_matrix_row_1_, jaspar_cne_pwm_51_matrix_row_2_, jaspar_cne_pwm_51_matrix_row_3_};
	PWM jaspar_cne_pwm_51_ = {
/* accession        */ "CN0050",
/* name             */ "LM50",
/* label            */ " CN0050	25.9998942976244	LM50	unknown	; consensus \"TGGGTAATTACATTCTGYY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGGGTAATTACATTCTGCC",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_51_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.780815,
/* max_score        */ 22.979923,
/* threshold        */ 0.869,
/* info content     */ 25.936750,
/* base_counts      */ {248, 191, 228, 435},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 58
};

	float jaspar_cne_pwm_52_matrix_row_0_[15] = {  1.272284,  1.201683,  1.071664, -1.813998, -0.564795,  1.272284,  1.099055, -7.117303, -7.117303,  1.176997, -7.117303, -7.117303,  1.249300, -1.813998, -1.813998};
	float jaspar_cne_pwm_52_matrix_row_1_[15] = { -6.876141, -2.261020, -2.261020,  1.418159,  1.284663, -6.876141, -1.572836, -6.876141, -0.882179, -6.876141, -6.876141,  1.466937, -2.261020, -1.572836,  1.130560};
	float jaspar_cne_pwm_52_matrix_row_2_[15] = { -6.876141, -2.261020, -1.169031, -6.876141, -1.572836, -6.876141, -6.876141, -6.876141, -6.876141, -6.876141,  1.513446, -2.261020, -6.876141, -0.882179, -6.876141};
	float jaspar_cne_pwm_52_matrix_row_3_[15] = { -7.117303, -2.502182, -1.123341, -1.813998, -7.117303, -7.117303, -0.900697,  1.272284,  1.176997, -1.123341, -7.117303, -2.502182, -7.117303,  1.071664, -0.026393};
	float *jaspar_cne_pwm_52_matrix[4] = { jaspar_cne_pwm_52_matrix_row_0_, jaspar_cne_pwm_52_matrix_row_1_, jaspar_cne_pwm_52_matrix_row_2_, jaspar_cne_pwm_52_matrix_row_3_};
	PWM jaspar_cne_pwm_52_ = {
/* accession        */ "CN0119",
/* name             */ "LM119",
/* label            */ " CN0119	23.0110085658494	LM119	unknown	; consensus \"AAACCAATTAGCATC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAACCAATTAGCATC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_52_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.020172,
/* max_score        */ 18.677979,
/* threshold        */ 0.799,
/* info content     */ 22.924566,
/* base_counts      */ {298, 158, 55, 149},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 44
};

	float jaspar_cne_pwm_53_matrix_row_0_[20] = { -2.058168, -2.345434, -2.191521, -1.740055,  1.250258, -3.440724, -3.440724,  1.134192,  0.713249, -1.835274,  0.781195, -1.835274, -3.036919,  1.208296,  1.263864, -2.058168, -1.653119,  0.004435, -0.246835,  0.985165};
	float jaspar_cne_pwm_53_matrix_row_1_[20] = { -1.006770,  1.106317,  1.232261,  1.317293, -8.502867, -3.887747, -2.795757, -3.887747, -1.188980,  1.267146, -1.498893, -0.026288, -0.530056, -2.104272, -3.887747, -8.502867, -3.887747, -8.502867, -1.188980, -1.950359};
	float jaspar_cne_pwm_53_matrix_row_2_[20] = { -2.104272, -1.699362, -3.887747, -3.887747, -3.887747, -3.887747, -2.508906, -0.719226,  0.350941, -2.508906, -1.188980, -1.188980, -2.795757, -1.950359, -3.887747, -8.502867,  1.415558,  1.139321,  1.119649, -0.530056};
	float jaspar_cne_pwm_53_matrix_row_3_[20] = {  1.118688, -0.044348, -0.288499, -0.806297, -2.750068,  1.254814,  1.231826, -2.750068, -1.740055, -0.583225, -0.027821,  0.878487,  1.102941, -4.128909, -8.744029,  1.236466, -2.058168, -2.191521, -1.940524, -1.142627};
	float *jaspar_cne_pwm_53_matrix[4] = { jaspar_cne_pwm_53_matrix_row_0_, jaspar_cne_pwm_53_matrix_row_1_, jaspar_cne_pwm_53_matrix_row_2_, jaspar_cne_pwm_53_matrix_row_3_};
	PWM jaspar_cne_pwm_53_ = {
/* accession        */ "CN0005",
/* name             */ "LM5",
/* label            */ " CN0005	24.1075170284628	LM5	unknown	; consensus \"TCCCATTARYRTTAATGGGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCCCATTAACATTAATGGGA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_53_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.221634,
/* max_score        */ 22.756985,
/* threshold        */ 0.855,
/* info content     */ 24.094450,
/* base_counts      */ {1470, 833, 699, 1478},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 224
};

	float jaspar_cne_pwm_54_matrix_row_0_[12] = { -0.918596, -9.618278,  0.812923, -9.618278,  1.272910, -9.618278, -1.793831, -9.618278, -9.618278, -9.618278, -2.016875,  1.250310};
	float jaspar_cne_pwm_54_matrix_row_1_[12] = { -9.377115, -9.377115, -9.377115,  1.514072, -9.377115, -9.377115,  1.466399, -9.377115, -9.377115, -9.377115,  1.275451, -2.286205};
	float jaspar_cne_pwm_54_matrix_row_2_[12] = { -9.377115,  1.481903,  0.080164, -9.377115, -9.377115,  1.514072, -9.377115, -9.377115,  1.514072, -0.377372, -0.365104, -9.377115};
	float jaspar_cne_pwm_54_matrix_row_3_[12] = {  1.154431, -2.179305, -0.764469, -9.618278, -9.618278, -9.618278, -9.618278,  1.272910, -9.618278,  1.109408, -2.527367, -9.618278};
	float *jaspar_cne_pwm_54_matrix[4] = { jaspar_cne_pwm_54_matrix_row_0_, jaspar_cne_pwm_54_matrix_row_1_, jaspar_cne_pwm_54_matrix_row_2_, jaspar_cne_pwm_54_matrix_row_3_};
	PWM jaspar_cne_pwm_54_ = {
/* accession        */ "CN0020",
/* name             */ "LM20",
/* label            */ " CN0020	19.9745597835393	LM20	unknown	; consensus \"TGACAGCTGTCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGACAGCTGTCA",
/* library_name     */ "jaspar_cne",
/* length           */ 12,
/* matrixname       */ jaspar_cne_pwm_54_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -107.846100,
/* max_score        */ 15.638861,
/* threshold        */ 0.777,
/* info content     */ 19.965805,
/* base_counts      */ {1506, 1484, 1885, 1569},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 537
};

	float jaspar_cne_pwm_55_matrix_row_0_[20] = { -1.800097, -0.416297,  1.087132, -1.290601, -0.598452, -1.290601, -1.513245, -2.203902, -0.954699, -1.800097, -2.203902, -0.416297, -1.108612, -7.507207,  1.123493,  1.225259,  1.241257,  1.049399, -2.892087, -1.290601};
	float jaspar_cne_pwm_55_matrix_row_1_[20] = { -1.558935,  1.189486, -1.558935, -7.266045,  0.671687, -7.266045, -2.650924, -7.266045,  1.346640, -7.266045, -1.962740,  1.146010, -7.266045, -0.462540, -2.650924, -7.266045, -1.962740, -0.357290, -7.266045, -2.650924};
	float jaspar_cne_pwm_55_matrix_row_2_[20] = { -1.962740, -1.272083, -7.266045, -0.867450, -0.867450, -7.266045, -0.713537,  1.167984, -2.650924, -7.266045,  0.047842, -1.272083, -7.266045,  1.364655, -0.867450, -1.962740, -7.266045, -7.266045, -7.266045,  1.251348};
	float jaspar_cne_pwm_55_matrix_row_3_[20] = {  1.141190, -2.203902, -0.821346,  1.087132,  0.142962,  1.192474,  1.068444, -0.068235, -2.203902,  1.225259,  0.926822, -1.513245,  1.175670, -7.507207, -2.203902, -2.892087, -7.507207, -1.800097,  1.257002, -0.703702};
	float *jaspar_cne_pwm_55_matrix[4] = { jaspar_cne_pwm_55_matrix_row_0_, jaspar_cne_pwm_55_matrix_row_1_, jaspar_cne_pwm_55_matrix_row_2_, jaspar_cne_pwm_55_matrix_row_3_};
	PWM jaspar_cne_pwm_55_ = {
/* accession        */ "CN0210",
/* name             */ "LM210",
/* label            */ " CN0210	24.4720016602023	LM210	unknown	; consensus \"TCATYTTGCTTCTGAAAATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCATYTTGCTTCTGAAAATG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_55_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.763763,
/* max_score        */ 22.938345,
/* threshold        */ 0.860,
/* info content     */ 24.416759,
/* base_counts      */ {364, 207, 205, 524},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 65
};

	float jaspar_cne_pwm_56_matrix_row_0_[15] = {  1.083563, -0.172504, -1.052518, -0.275131,  1.182648,  1.213608,  1.176339, -3.188185,  1.272793, -3.876369,  1.225729, -3.188185, -2.784379, -3.188185, -1.246548};
	float jaspar_cne_pwm_56_matrix_row_1_[15] = { -0.936441, -8.250328, -8.250328, -1.005386, -1.564467, -2.947023, -3.635207,  1.496565, -8.250328, -8.250328, -2.543217, -8.250328, -8.250328, -1.851733, -1.564467};
	float jaspar_cne_pwm_56_matrix_row_2_[15] = { -2.947023, -0.425882,  1.404763,  1.167108, -2.256366, -1.697820, -0.936441, -3.635207, -8.250328,  1.502395, -2.256366, -8.250328,  1.496565,  1.237720, -1.341573};
	float jaspar_cne_pwm_56_matrix_row_3_[15] = { -1.320601,  0.795904, -3.876369, -8.491489, -2.784379, -3.876369, -8.491489, -8.491489, -8.491489, -3.876369, -3.876369,  1.261233, -8.491489, -0.359665,  1.069578};
	float *jaspar_cne_pwm_56_matrix[4] = { jaspar_cne_pwm_56_matrix_row_0_, jaspar_cne_pwm_56_matrix_row_1_, jaspar_cne_pwm_56_matrix_row_2_, jaspar_cne_pwm_56_matrix_row_3_};
	PWM jaspar_cne_pwm_56_ = {
/* accession        */ "CN0073",
/* name             */ "LM73",
/* label            */ " CN0073	21.9843349945475	LM73	unknown	; consensus \"ATGGAAACAGATGGT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATGGAAACAGATGGT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_56_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.695541,
/* max_score        */ 18.586512,
/* threshold        */ 0.793,
/* info content     */ 21.964870,
/* base_counts      */ {1084, 228, 822, 476},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 174
};

	float jaspar_cne_pwm_57_matrix_row_0_[15] = {  0.508572, -0.136492,  1.159832, -8.900702, -2.906740,  1.272851,  1.269027,  0.997823, -4.285581, -8.900702,  1.257467, -1.350566, -2.906740, -1.991947,  1.253583};
	float jaspar_cne_pwm_57_matrix_row_1_[15] = { -1.655566,  0.249831, -8.659539, -8.659539, -3.356235, -8.659539, -4.044419, -3.356235, -3.356235, -8.659539, -4.044419,  0.962977, -2.442934,  1.467131, -3.356235};
	float jaspar_cne_pwm_57_matrix_row_2_[15] = {  0.236227,  0.741504, -1.009371,  1.514013,  1.490847, -8.659539, -8.659539,  0.056668, -4.044419,  1.510189, -4.044419, -0.028839, -8.659539, -8.659539, -4.044419};
	float jaspar_cne_pwm_57_matrix_row_3_[15] = { -0.270001, -3.193591, -2.348194, -8.900702, -8.900702, -8.900702, -8.900702, -8.900702,  1.257467, -4.285581, -3.597397, -0.711735,  1.237898, -3.597397, -3.597397};
	float *jaspar_cne_pwm_57_matrix[4] = { jaspar_cne_pwm_57_matrix_row_0_, jaspar_cne_pwm_57_matrix_row_1_, jaspar_cne_pwm_57_matrix_row_2_, jaspar_cne_pwm_57_matrix_row_3_};
	PWM jaspar_cne_pwm_57_ = {
/* accession        */ "CN0099",
/* name             */ "LM99",
/* label            */ " CN0099	22.4431552384206	LM99	unknown	; consensus \"DSAGGAAATGACTCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NNAGGAAATGACTCA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_57_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.956963,
/* max_score        */ 17.901182,
/* threshold        */ 0.796,
/* info content     */ 22.428574,
/* base_counts      */ {1695, 501, 1114, 620},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 262
};

	float jaspar_cne_pwm_58_matrix_row_0_[14] = { -1.210277, -7.204238, -7.204238, -7.204238, -7.204238,  1.251292, -7.204238, -7.204238, -7.204238, -7.204238, -7.204238, -7.204238, -7.204238, -1.210277};
	float jaspar_cne_pwm_58_matrix_row_1_[14] = { -2.347956, -0.746470,  1.492454,  1.513503,  1.513503, -6.963077, -6.963077,  1.513503, -6.963077, -2.347956, -0.746470, -6.963077, -0.564481,  1.225890};
	float jaspar_cne_pwm_58_matrix_row_2_[14] = { -0.969115, -2.347956, -6.963077, -6.963077, -6.963077, -6.963077,  1.513503, -6.963077, -6.963077,  1.492454, -0.969115, -6.963077,  1.380001, -1.255966};
	float jaspar_cne_pwm_58_matrix_row_3_[14] = {  1.064750,  1.138839, -2.589118, -7.204238, -7.204238, -2.589118, -7.204238, -7.204238,  1.272341, -7.204238,  1.064750,  1.272341, -7.204238, -0.987632};
	float *jaspar_cne_pwm_58_matrix[4] = { jaspar_cne_pwm_58_matrix_row_0_, jaspar_cne_pwm_58_matrix_row_1_, jaspar_cne_pwm_58_matrix_row_2_, jaspar_cne_pwm_58_matrix_row_3_};
	PWM jaspar_cne_pwm_58_ = {
/* accession        */ "CN0169",
/* name             */ "LM169",
/* label            */ " CN0169	23.3541322578337	LM169	unknown	; consensus \"TTCCCAGCTGTTGC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTCCCAGCTGTTGC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_58_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.813622,
/* max_score        */ 18.709126,
/* threshold        */ 0.792,
/* info content     */ 23.265718,
/* base_counts      */ {55, 245, 149, 223},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_59_matrix_row_0_[15] = { -1.179373,  1.272643, -7.865234, -7.865234, -7.865234, -7.865234, -7.865234, -2.561929, -7.865234, -7.865234, -7.865234, -7.865234,  1.083871, -7.865234, -7.865234};
	float jaspar_cne_pwm_59_matrix_row_1_[15] = {  1.423867, -7.624072, -7.624072,  1.513805, -7.624072, -7.624072,  1.337935,  0.536732, -7.624072, -0.715317, -7.624072, -7.624072, -7.624072, -7.624072, -0.073937};
	float jaspar_cne_pwm_59_matrix_row_2_[15] = { -7.624072, -7.624072,  1.140138, -7.624072, -7.624072,  1.513805, -0.310185, -7.624072, -1.916962, -7.624072, -7.624072,  1.513805, -7.624072, -7.624072, -1.071564};
	float jaspar_cne_pwm_59_matrix_row_3_[15] = { -7.865234, -7.865234,  0.107577, -7.865234,  1.272643, -7.865234, -7.865234,  0.765466,  1.239857,  1.158897,  1.272643, -7.865234, -0.486850,  1.272643,  0.944778};
	float *jaspar_cne_pwm_59_matrix[4] = { jaspar_cne_pwm_59_matrix_row_0_, jaspar_cne_pwm_59_matrix_row_1_, jaspar_cne_pwm_59_matrix_row_2_, jaspar_cne_pwm_59_matrix_row_3_};
	PWM jaspar_cne_pwm_59_ = {
/* accession        */ "CN0086",
/* name             */ "LM86",
/* label            */ " CN0086	24.5037516422175	LM86	unknown	; consensus \"MAKCTGMYTTTGATY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAGCTGCTTTTGATT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_59_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -117.496185,
/* max_score        */ 18.726801,
/* threshold        */ 0.806,
/* info content     */ 24.447422,
/* base_counts      */ {180, 320, 275, 620},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 93
};

	float jaspar_cne_pwm_60_matrix_row_0_[20] = { -1.858410, -1.858410, -0.945109, -7.161715, -0.945109,  0.739662, -2.546595, -2.546595,  0.277256, -0.252960, -7.161715, -7.161715, -2.546595, -1.858410,  1.250340, -0.763120, -0.945109,  1.157271,  1.107273,  1.272314};
	float jaspar_cne_pwm_60_matrix_row_1_[20] = { -1.617248, -0.703947, -0.926592, -6.920553, -0.926592, -1.617248, -2.305433, -0.703947,  1.017179, -0.926592, -2.305433, -1.617248,  1.322466,  1.469034, -6.920553, -6.920553, -2.305433, -2.305433, -2.305433, -6.920553};
	float jaspar_cne_pwm_60_matrix_row_2_[20] = { -1.617248,  1.240251, -2.305433, -0.703947,  1.118927, -2.305433, -1.213443, -2.305433, -6.920553, -6.920553, -0.926592, -0.926592, -2.305433, -6.920553, -6.920553,  1.322466,  1.295805, -1.617248, -1.213443, -6.920553};
	float jaspar_cne_pwm_60_matrix_row_3_[20] = {  1.132584, -1.167754,  1.027252,  1.157271, -0.763120,  0.216669,  1.157271,  1.107273, -2.546595,  0.909503,  1.157271,  1.132584, -0.763120, -7.161715, -2.546595, -1.858410, -1.454605, -1.858410, -1.454605, -7.161715};
	float *jaspar_cne_pwm_60_matrix[4] = { jaspar_cne_pwm_60_matrix_row_0_, jaspar_cne_pwm_60_matrix_row_1_, jaspar_cne_pwm_60_matrix_row_2_, jaspar_cne_pwm_60_matrix_row_3_};
	PWM jaspar_cne_pwm_60_ = {
/* accession        */ "CN0233",
/* name             */ "LM233",
/* label            */ " CN0233	24.3505370585227	LM233	unknown	; consensus \"TGTTGATTCTTTCCAGGAAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGTTGATTCTTTCCAGGAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_60_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.319672,
/* max_score        */ 23.093996,
/* threshold        */ 0.859,
/* info content     */ 24.279663,
/* base_counts      */ {255, 143, 168, 354},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_61_matrix_row_0_[16] = { -0.733989, -0.984986, -1.543532, -0.628739, -7.537494, -7.537494, -7.537494, -2.922374,  1.272518, -7.537494, -7.537494,  1.272518,  1.242217,  1.272518, -7.537494, -0.733989};
	float jaspar_cne_pwm_61_matrix_row_1_[16] = { -0.743824, -1.993027, -0.051390, -1.993027, -2.681211, -7.296332, -0.492827,  1.483379, -7.296332, -7.296332, -2.681211, -7.296332, -1.993027, -7.296332, -1.302370,  1.022654};
	float jaspar_cne_pwm_61_matrix_row_2_[16] = { -0.743824,  0.920026,  1.180248, -2.681211, -1.993027, -7.296332, -2.681211, -2.681211, -7.296332, -7.296332, -2.681211, -7.296332, -7.296332, -7.296332,  1.452132, -0.125443};
	float jaspar_cne_pwm_61_matrix_row_3_[16] = {  0.852093,  0.112675, -2.922374,  1.056845,  1.226716,  1.272518,  1.110903, -7.537494, -7.537494,  1.272518,  1.242217, -7.537494, -7.537494, -7.537494, -7.537494, -1.543532};
	float *jaspar_cne_pwm_61_matrix[4] = { jaspar_cne_pwm_61_matrix_row_0_, jaspar_cne_pwm_61_matrix_row_1_, jaspar_cne_pwm_61_matrix_row_2_, jaspar_cne_pwm_61_matrix_row_3_};
	PWM jaspar_cne_pwm_61_ = {
/* accession        */ "CN0085",
/* name             */ "LM85",
/* label            */ " CN0085	23.5160140257339	LM85	unknown	; consensus \"WKSTTTTCATTAAAGM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGGTTTTCATTAAAGC",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_61_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.796394,
/* max_score        */ 19.152020,
/* threshold        */ 0.810,
/* info content     */ 23.455828,
/* base_counts      */ {306, 148, 174, 444},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 67
};

	float jaspar_cne_pwm_62_matrix_row_0_[13] = { -8.974254, -8.974254, -2.575659, -3.267143, -8.974254,  1.136289, -4.359133, -1.535282, -8.974254,  1.258574, -8.974254, -1.595870,  1.060306};
	float jaspar_cne_pwm_62_matrix_row_1_[13] = { -8.733091, -8.733091,  1.485243, -8.733091, -1.419205, -1.036424,  1.297073, -1.419205,  1.496132, -8.733091, -8.733091,  1.389572, -0.991992};
	float jaspar_cne_pwm_62_matrix_row_2_[13] = { -1.824337,  1.481587, -3.429786, -8.733091,  1.444271, -2.739130, -0.601267, -2.334496, -8.733091, -8.733091,  1.514021, -1.354708, -1.419205};
	float jaspar_cne_pwm_62_matrix_row_3_[13] = {  1.236756, -2.170748, -8.974254,  1.262164, -2.980292, -2.065499, -1.372851,  1.128126, -2.757647, -2.980292, -8.974254, -4.359133, -1.595870};
	float *jaspar_cne_pwm_62_matrix[4] = { jaspar_cne_pwm_62_matrix_row_0_, jaspar_cne_pwm_62_matrix_row_1_, jaspar_cne_pwm_62_matrix_row_2_, jaspar_cne_pwm_62_matrix_row_3_};
	PWM jaspar_cne_pwm_62_ = {
/* accession        */ "CN0106",
/* name             */ "LM106",
/* label            */ " CN0106	20.5741289489696	LM106	unknown	; consensus \"TGCTGASTCAGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCTGACTCAGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 13,
/* matrixname       */ jaspar_cne_pwm_62_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.699463,
/* max_score        */ 17.190115,
/* threshold        */ 0.780,
/* info content     */ 20.561844,
/* base_counts      */ {795, 1102, 905, 864},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 282
};

	float jaspar_cne_pwm_63_matrix_row_0_[23] = { -1.227128, -0.891226, -1.045139, -7.443734, -0.439760,  1.186966,  1.222052, -2.140429, -2.828613,  1.222052, -2.140429, -1.449772,  1.272474,  0.419917,  1.112872, -1.449772, -7.443734, -2.828613, -0.534979,  1.255947, -1.045139, -1.227128,  0.899344};
	float jaspar_cne_pwm_63_matrix_row_1_[23] = {  0.986395,  1.091728,  1.209483, -0.985966, -1.495462, -2.587451, -7.202572, -1.495462,  1.410113, -1.899267, -7.202572, -7.202572, -7.202572, -0.985966, -1.495462, -2.587451, -0.031683, -1.899267, -7.202572, -2.587451, -1.899267, -1.495462, -7.202572};
	float jaspar_cne_pwm_63_matrix_row_2_[23] = { -0.985966, -7.202572, -2.587451, -1.208610,  1.231457, -1.208610, -1.899267,  1.428129, -1.495462, -7.202572, -1.495462,  1.428129, -7.202572,  0.735160, -1.208610, -7.202572, -0.985966, -2.587451,  1.314821, -7.202572, -7.202572, -2.587451, -0.031683};
	float jaspar_cne_pwm_63_matrix_row_3_[23] = { -0.129847, -0.198792, -0.640229,  1.112872, -2.828613, -7.443734, -2.828613, -7.443734, -2.140429, -2.828613,  1.186966, -2.828613, -7.443734, -2.140429, -2.140429,  1.186966,  0.922869,  1.204663, -2.828613, -7.443734,  1.131917,  1.112872, -1.045139};
	float *jaspar_cne_pwm_63_matrix[4] = { jaspar_cne_pwm_63_matrix_row_0_, jaspar_cne_pwm_63_matrix_row_1_, jaspar_cne_pwm_63_matrix_row_2_, jaspar_cne_pwm_63_matrix_row_3_};
	PWM jaspar_cne_pwm_63_ = {
/* accession        */ "CN0151",
/* name             */ "LM151",
/* label            */ " CN0151	29.1731745650393	LM151	unknown	; consensus \"MCCTGAAGCATGARATTTGATTR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CCCTGAAGCATGARATTTGATTA",
/* library_name     */ "jaspar_cne",
/* length           */ 23,
/* matrixname       */ jaspar_cne_pwm_63_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -128.654266,
/* max_score        */ 26.866245,
/* threshold        */ 0.989,
/* info content     */ 29.105816,
/* base_counts      */ {477, 220, 282, 424},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 61
};

	float jaspar_cne_pwm_64_matrix_row_0_[18] = {  0.828699, -2.469939, -2.353417,  0.043478, -1.799574, -4.185473,  1.007556, -4.559883, -3.962455, -2.551846, -0.107492, -2.898072, -1.716887, -4.013722, -2.038633, -0.602562,  0.034337,  0.672264};
	float jaspar_cne_pwm_64_matrix_row_1_[18] = { -2.195628, -1.180151,  1.196731, -1.998937,  1.425414,  1.498025, -0.142435,  1.134529,  1.498025, -2.512683, -1.929950,  0.287366, -1.929950, -4.413940, -1.955266,  0.725559,  0.704257, -0.590634};
	float jaspar_cne_pwm_64_matrix_row_2_[18] = {  0.209062,  1.319052, -2.574232,  0.938817, -2.335373, -4.519190, -2.590230, -2.386660, -3.826598, -3.315995,  1.003385,  1.100507, -2.512683,  1.499106,  1.407517, -1.328408, -0.525142,  0.132612};
	float jaspar_cne_pwm_64_matrix_row_3_[18] = { -1.490585, -1.181004, -0.202907, -0.887926, -2.784146, -3.403054, -2.383873,  0.044387, -3.867190,  1.224154, -0.876445, -2.231211,  1.167538, -3.662480, -2.122326,  0.176212, -0.728186, -1.271678};
	float *jaspar_cne_pwm_64_matrix[4] = { jaspar_cne_pwm_64_matrix_row_0_, jaspar_cne_pwm_64_matrix_row_1_, jaspar_cne_pwm_64_matrix_row_2_, jaspar_cne_pwm_64_matrix_row_3_};
	PWM jaspar_cne_pwm_64_ = {
/* accession        */ "CN0023",
/* name             */ "LM23",
/* label            */ " CN0023	18.8800728704782	LM23	unknown	; consensus \"TKACCACCAGGTGGYGCTRW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCGCCACCTGGTGGYNA",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_64_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.175880,
/* max_score        */ 20.351133,
/* threshold        */ 0.779,
/* info content     */ 18.879549,
/* base_counts      */ {11957, 22691, 20102, 12894},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 3758
};

	float jaspar_cne_pwm_65_matrix_row_0_[25] = { -1.614819, -0.923334, -1.105323, -1.327968, -2.018624, -2.706809, -2.706809,  1.215263,  1.234677, -1.327968, -1.327968, -0.769421, -1.327968,  1.272410,  1.133601,  1.195464, -2.706809, -2.018624, -2.706809, -1.614819, -0.769421,  0.921090, -2.018624, -7.321929, -1.614819};
	float jaspar_cne_pwm_65_matrix_row_1_[25] = { -0.864161,  0.702874,  1.051058, -0.394906, -1.086805, -1.086805, -1.777462, -2.465647, -7.080767, -7.080767, -7.080767, -7.080767, -1.777462, -7.080767, -2.465647, -1.373657, -2.465647,  0.958713, -7.080767, -1.777462,  0.990452, -1.086805, -2.465647, -0.682172,  0.233120};
	float jaspar_cne_pwm_65_matrix_row_2_[25] = {  0.660332, -0.277262, -0.864161, -2.465647, -7.080767, -7.080767, -7.080767, -7.080767, -7.080767, -7.080767,  1.395813,  1.331288,  1.353262, -7.080767, -2.465647, -7.080767, -2.465647, -1.086805, -1.777462,  1.395813, -2.465647, -1.086805, -0.864161, -0.528259, -1.373657};
	float jaspar_cne_pwm_65_matrix_row_3_[25] = {  0.419170, -0.008042, -0.413174,  0.997057,  1.154650,  1.175265,  1.215263, -2.018624, -2.018624,  1.195464, -2.018624, -2.018624, -2.018624, -7.321929, -1.105323, -2.706809,  1.215263,  0.117043,  1.215263, -2.706809, -0.076988, -0.636068,  1.112100,  0.997057,  0.780052};
	float *jaspar_cne_pwm_65_matrix[4] = { jaspar_cne_pwm_65_matrix_row_0_, jaspar_cne_pwm_65_matrix_row_1_, jaspar_cne_pwm_65_matrix_row_2_, jaspar_cne_pwm_65_matrix_row_3_};
	PWM jaspar_cne_pwm_65_ = {
/* accession        */ "CN0200",
/* name             */ "LM200",
/* label            */ " CN0200	29.3110239149889	LM200	unknown	; consensus \"KNNNTTTAATGGGAAATYTGMWWWK\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "KNCTTTTAATGGGAAATCTGCATTT",
/* library_name     */ "jaspar_cne",
/* length           */ 25,
/* matrixname       */ jaspar_cne_pwm_65_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -111.110085,
/* max_score        */ 27.869547,
/* threshold        */ 1.032,
/* info content     */ 29.238657,
/* base_counts      */ {352, 180, 253, 565},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 54
};

	float jaspar_cne_pwm_66_matrix_row_0_[15] = {  0.906984, -0.316086, -1.230879, -2.609720, -7.224841, -1.921536, -7.224841, -7.224841, -7.224841, -2.609720,  1.251739,  1.187214, -7.224841, -7.224841,  1.209188};
	float jaspar_cne_pwm_66_matrix_row_1_[15] = { -0.431171, -6.983679, -1.680374, -6.983679, -6.983679, -6.983679,  1.450350, -1.276569, -6.983679, -6.983679, -2.368558, -1.680374, -6.983679, -2.368558, -2.368558};
	float jaspar_cne_pwm_66_matrix_row_2_[15] = { -1.680374, -0.989717, -0.180173, -2.368558,  1.492901,  1.471852, -2.368558, -6.983679, -6.983679, -2.368558, -6.983679, -1.680374, -6.983679,  1.492901, -6.983679};
	float jaspar_cne_pwm_66_matrix_row_3_[15] = { -0.826246,  0.935963,  0.906984,  1.230690, -2.609720, -7.224841, -1.921536,  1.209188,  1.272354,  1.230690, -7.224841, -7.224841,  1.272354, -7.224841, -1.921536};
	float *jaspar_cne_pwm_66_matrix[4] = { jaspar_cne_pwm_66_matrix_row_0_, jaspar_cne_pwm_66_matrix_row_1_, jaspar_cne_pwm_66_matrix_row_2_, jaspar_cne_pwm_66_matrix_row_3_};
	PWM jaspar_cne_pwm_66_ = {
/* accession        */ "CN0181",
/* name             */ "LM181",
/* label            */ " CN0181	23.4159903771542	LM181	unknown	; consensus \"WTTTGGCTTTAATGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTTGGCTTTAATGA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_66_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.319023,
/* max_score        */ 18.521351,
/* threshold        */ 0.801,
/* info content     */ 23.338114,
/* base_counts      */ {191, 63, 163, 318},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_67_matrix_row_0_[19] = { -0.113329, -0.113329, -1.577107, -7.284217, -0.598356,  1.127838, -2.669097,  1.212977, -7.284217,  1.034769, -7.284217, -7.284217,  1.058861, -7.284217, -7.284217,  1.272389,  1.272389, -0.598356, -1.290256};
	float jaspar_cne_pwm_67_matrix_row_1_[19] = { -1.049094, -1.049094,  0.507080, -1.739750, -0.039081, -1.049094,  1.474338, -7.043055, -7.043055, -2.427935,  0.453042,  1.513551, -0.357194, -7.043055, -7.043055, -7.043055, -7.043055,  1.346532, -2.427935};
	float jaspar_cne_pwm_67_matrix_row_2_[19] = { -2.427935,  0.894677, -1.049094, -2.427935, -0.039081, -1.739750, -7.043055, -7.043055,  1.513551, -1.335945, -7.043055, -7.043055, -7.043055, -2.427935, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055};
	float jaspar_cne_pwm_67_matrix_row_3_[19] = {  0.847608, -0.731709,  0.579434,  1.212977,  0.412450, -2.669097, -2.669097, -1.577107, -7.284217, -0.731709,  0.847608, -7.284217, -1.980912,  1.252975,  1.272389, -7.284217, -7.284217, -7.284217,  1.171313};
	float *jaspar_cne_pwm_67_matrix[4] = { jaspar_cne_pwm_67_matrix_row_0_, jaspar_cne_pwm_67_matrix_row_1_, jaspar_cne_pwm_67_matrix_row_2_, jaspar_cne_pwm_67_matrix_row_3_};
	PWM jaspar_cne_pwm_67_ = {
/* accession        */ "CN0142",
/* name             */ "LM142",
/* label            */ " CN0142	25.8247527722756	LM142	unknown	; consensus \"WNNWNACAGATCATTAACT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGYTNACAGATCATTAACT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_67_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.479698,
/* max_score        */ 21.318623,
/* threshold        */ 0.867,
/* info content     */ 25.739193,
/* base_counts      */ {331, 218, 103, 336},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_68_matrix_row_0_[17] = { -2.980327, -7.595448, -7.595448, -2.292143, -1.378842, -2.980327,  1.199529,  1.168762,  1.229377, -7.595448, -2.980327, -1.378842, -2.980327,  1.137018,  0.921945, -2.292143, -7.595448};
	float jaspar_cne_pwm_68_matrix_row_1_[17] = {  1.425426, -0.109344, -7.354286, -2.050981,  1.361922,  1.485136, -2.050981, -1.360324, -2.739165, -7.354286, -2.739165, -7.354286, -7.354286, -0.801778,  0.084686, -1.137679, -2.739165};
	float jaspar_cne_pwm_68_matrix_row_2_[17] = { -1.647175, -7.354286, -1.647175,  1.276415, -7.354286, -2.739165, -2.050981, -1.647175, -2.050981, -0.445531,  1.440691, -0.550780,  1.258400, -2.739165, -2.739165, -0.955691, -0.445531};
	float jaspar_cne_pwm_68_matrix_row_3_[17] = { -2.292143,  1.052949,  1.229377, -0.591474, -1.378842, -7.595448, -2.980327, -7.595448, -7.595448,  1.120760, -1.888337,  1.052949, -0.281561, -2.980327, -1.888337,  1.070338,  1.104234};
	float *jaspar_cne_pwm_68_matrix[4] = { jaspar_cne_pwm_68_matrix_row_0_, jaspar_cne_pwm_68_matrix_row_1_, jaspar_cne_pwm_68_matrix_row_2_, jaspar_cne_pwm_68_matrix_row_3_};
	PWM jaspar_cne_pwm_68_ = {
/* accession        */ "CN0202",
/* name             */ "LM202",
/* label            */ " CN0202	22.9993911672978	LM202	unknown	; consensus \"CTTGCCAAATGTGAATT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTGCCAAATGTGAATT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_68_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.475754,
/* max_score        */ 20.535229,
/* threshold        */ 0.815,
/* info content     */ 22.954115,
/* base_counts      */ {328, 249, 228, 402},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 71
};

	float jaspar_cne_pwm_69_matrix_row_0_[16] = { -0.900697, -1.813998, -0.564795,  1.201683,  1.151685, -7.117303, -2.502182,  1.014522, -2.502182, -0.900697, -7.117303,  1.272284, -2.502182, -7.117303, -7.117303, -7.117303};
	float jaspar_cne_pwm_69_matrix_row_1_[16] = { -6.876141, -0.323633,  1.255684, -6.876141, -6.876141, -6.876141, -0.882179, -6.876141, -6.876141,  1.392847,  1.513446, -6.876141, -6.876141,  1.490462, -6.876141, -6.876141};
	float jaspar_cne_pwm_69_matrix_row_2_[16] = { -1.572836,  1.096670, -6.876141, -6.876141, -1.572836, -6.876141, -2.261020, -2.261020,  1.490462, -6.876141, -6.876141, -6.876141, -0.882179, -6.876141, -6.876141,  1.490462};
	float jaspar_cne_pwm_69_matrix_row_3_[16] = {  1.099055, -0.718708, -1.410193, -1.410193, -1.410193,  1.272284,  1.125717, -0.313798, -7.117303, -7.117303, -7.117303, -7.117303,  1.151685, -2.502182,  1.272284, -2.502182};
	float *jaspar_cne_pwm_69_matrix[4] = { jaspar_cne_pwm_69_matrix_row_0_, jaspar_cne_pwm_69_matrix_row_1_, jaspar_cne_pwm_69_matrix_row_2_, jaspar_cne_pwm_69_matrix_row_3_};
	PWM jaspar_cne_pwm_69_ = {
/* accession        */ "CN0107",
/* name             */ "LM107",
/* label            */ " CN0107	24.6655887499234	LM107	unknown	; consensus \"WKMAATTAGCCATCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCAATTAGCCATCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_69_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.511436,
/* max_score        */ 20.291231,
/* threshold        */ 0.818,
/* info content     */ 24.568909,
/* base_counts      */ {180, 171, 125, 228},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 44
};

	float jaspar_cne_pwm_70_matrix_row_0_[18] = { -7.224841, -1.517731, -7.224841, -1.517731,  1.251739,  1.187214,  1.230690, -1.517731,  0.676537, -0.538980, -2.609720, -2.609720, -2.609720,  1.209188,  1.118237,  1.187214, -7.224841,  1.164746};
	float jaspar_cne_pwm_70_matrix_row_1_[18] = { -1.680374, -6.983679, -6.983679, -1.680374, -6.983679, -1.680374, -6.983679, -1.276569,  0.107231,  0.879973, -6.983679, -0.767073,  1.382924, -1.680374, -6.983679, -2.368558, -0.431171, -6.983679};
	float jaspar_cne_pwm_70_matrix_row_2_[18] = { -2.368558, -0.431171,  1.492901,  1.405908, -2.368558, -2.368558, -1.680374,  1.205288, -0.431171, -0.431171, -6.983679, -1.276569, -0.767073, -2.368558, -0.431171, -1.680374,  1.335307, -0.989717};
	float jaspar_cne_pwm_70_matrix_row_3_[18] = {  1.209188,  1.044147, -2.609720, -7.224841, -7.224841, -2.609720, -7.224841, -0.672333, -1.517731, -0.538980,  1.251739,  1.069459, -7.224841, -7.224841, -7.224841, -2.609720, -2.609720, -2.609720};
	float *jaspar_cne_pwm_70_matrix[4] = { jaspar_cne_pwm_70_matrix_row_0_, jaspar_cne_pwm_70_matrix_row_1_, jaspar_cne_pwm_70_matrix_row_2_, jaspar_cne_pwm_70_matrix_row_3_};
	PWM jaspar_cne_pwm_70_ = {
/* accession        */ "CN0222",
/* name             */ "LM222",
/* label            */ " CN0222	23.9969912530281	LM222	unknown	; consensus \"TTGGAAAGACTTCAAAGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGGAAAGACTTCAAAGA",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_70_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.378189,
/* max_score        */ 21.302397,
/* threshold        */ 0.833,
/* info content     */ 23.925953,
/* base_counts      */ {364, 105, 217, 196},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_71_matrix_row_0_[20] = { -1.476093,  0.977601, -1.189241, -0.630695, -7.183203, -7.183203, -2.568082, -1.879898, -1.879898, -1.879898, -1.879898, -2.568082, -0.966597, -2.568082,  1.085785,  1.159875, -1.476093, -2.568082,  1.085785, -0.497342};
	float jaspar_cne_pwm_71_matrix_row_1_[20] = {  1.246926, -1.638736, -1.234931, -6.942041, -6.942041,  1.300979,  1.401037, -6.942041, -6.942041, -6.942041, -0.389533, -6.942041, -6.942041, -0.389533, -0.948079, -6.942041, -1.234931, -0.256180, -2.326920, -0.725435};
	float jaspar_cne_pwm_71_matrix_row_2_[20] = { -1.638736, -6.942041, -1.234931, -2.326920, -1.234931, -1.638736, -6.942041, -6.942041, -1.638736, -2.326920,  1.300979,  0.496931,  1.376945,  1.300979, -2.326920, -1.234931, -1.234931,  1.246926, -6.942041,  1.097439};
	float jaspar_cne_pwm_71_matrix_row_3_[20] = { -0.784608, -0.274448,  1.033155,  1.085785,  1.206384, -0.630695, -1.189241,  1.228852,  1.183400,  1.206384, -7.183203,  0.789608, -2.568082, -2.568082, -1.476093, -1.879898,  1.059817, -1.879898, -0.630695, -1.476093};
	float *jaspar_cne_pwm_71_matrix[4] = { jaspar_cne_pwm_71_matrix_row_0_, jaspar_cne_pwm_71_matrix_row_1_, jaspar_cne_pwm_71_matrix_row_2_, jaspar_cne_pwm_71_matrix_row_3_};
	PWM jaspar_cne_pwm_71_ = {
/* accession        */ "CN0209",
/* name             */ "LM209",
/* label            */ " CN0209	23.967721068214	LM209	unknown	; consensus \"CATTTCCTTTGTGGAATGAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATTTCCTTTGTGGAATGAG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_71_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.258942,
/* max_score        */ 23.374641,
/* threshold        */ 0.853,
/* info content     */ 23.897655,
/* base_counts      */ {197, 156, 222, 365},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_72_matrix_row_0_[20] = {  1.040692, -0.707476, -0.148395,  1.237363, -2.778216, -7.393337,  1.143855,  1.219348, -2.778216, -7.393337, -2.090032,  0.875651, -2.090032, -2.778216,  1.219348,  1.237363,  1.163269, -2.090032, -2.090032, -0.994742};
	float jaspar_cne_pwm_72_matrix_row_1_[20] = { -2.537054, -7.152175,  0.854526, -7.152175, -1.848870, -0.061265, -1.158213, -7.152175, -7.152175, -2.537054, -1.848870, -7.152175,  1.345020, -1.445065, -7.152175, -2.537054, -2.537054, -2.537054, -7.152175, -7.152175};
	float jaspar_cne_pwm_72_matrix_row_2_[20] = { -0.466314,  1.324405, -7.152175, -2.537054,  1.324405, -7.152175, -1.848870, -1.848870, -7.152175, -2.537054, -2.537054, -0.753580, -1.848870, -1.848870, -2.537054, -7.152175, -0.935569, -7.152175,  1.036792,  1.303356};
	float jaspar_cne_pwm_72_matrix_row_3_[20] = { -1.686227, -2.090032, -0.148395, -2.778216, -0.840829,  1.040692, -2.778216, -2.778216,  1.255060,  1.237363,  1.182314, -0.222448, -1.176731,  1.163269, -2.090032, -2.778216, -7.393337,  1.219348,  0.208065, -1.176731};
	float *jaspar_cne_pwm_72_matrix[4] = { jaspar_cne_pwm_72_matrix_row_0_, jaspar_cne_pwm_72_matrix_row_1_, jaspar_cne_pwm_72_matrix_row_2_, jaspar_cne_pwm_72_matrix_row_3_};
	PWM jaspar_cne_pwm_72_ = {
/* accession        */ "CN0223",
/* name             */ "LM223",
/* label            */ " CN0223	26.752426261018	LM223	unknown	; consensus \"AGMAGTAATTTACTAAATTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCAGTAATTTACTAAATGG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_72_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -116.352730,
/* max_score        */ 23.423443,
/* threshold        */ 0.894,
/* info content     */ 26.687742,
/* base_counts      */ {449, 107, 210, 394},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 58
};

	float jaspar_cne_pwm_73_matrix_row_0_[17] = {  1.118237, -1.517731, -2.609720, -2.609720,  1.044147, -7.224841, -2.609720, -7.224841, -7.224841, -0.826246, -7.224841, -7.224841, -1.921536,  0.935963, -1.230879,  1.209188,  1.069459};
	float jaspar_cne_pwm_73_matrix_row_1_[17] = { -2.368558,  1.177125, -6.983679, -6.983679, -0.767073, -1.680374, -1.680374, -6.983679, -0.767073,  1.285309,  1.471852, -6.983679, -6.983679,  0.107231,  1.232679, -1.680374, -1.680374};
	float jaspar_cne_pwm_73_matrix_row_2_[17] = { -2.368558, -1.276569, -6.983679,  1.428376, -1.680374, -6.983679, -6.983679, -6.983679, -0.989717, -6.983679, -6.983679, -6.983679,  1.471852, -1.680374, -0.767073, -6.983679, -0.989717};
	float jaspar_cne_pwm_73_matrix_row_3_[17] = { -1.008235, -0.538980,  1.251739, -1.517731, -1.517731,  1.230690,  1.209188,  1.272354,  1.069459, -1.230879, -1.921536,  1.272354, -7.224841, -7.224841, -1.517731, -2.609720, -1.517731};
	float *jaspar_cne_pwm_73_matrix[4] = { jaspar_cne_pwm_73_matrix_row_0_, jaspar_cne_pwm_73_matrix_row_1_, jaspar_cne_pwm_73_matrix_row_2_, jaspar_cne_pwm_73_matrix_row_3_};
	PWM jaspar_cne_pwm_73_ = {
/* accession        */ "CN0173",
/* name             */ "LM173",
/* label            */ " CN0173	23.827639429503	LM173	unknown	; consensus \"ACTGATTTTCCTGACAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACTGATTTTCCTGACAA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_73_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.257050,
/* max_score        */ 20.749969,
/* threshold        */ 0.822,
/* info content     */ 23.752859,
/* base_counts      */ {220, 189, 113, 311},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_74_matrix_row_0_[15] = {  1.215101, -8.160838, -3.545718, -2.453728,  0.944253, -3.545718, -8.160838,  1.256598, -8.160838, -8.160838, -3.545718, -8.160838, -8.160838,  0.944253, -1.944232};
	float jaspar_cne_pwm_74_matrix_row_1_[15] = { -2.616371,  1.464702, -7.919676, -7.919676, -7.919676, -3.304556,  1.497760, -7.919676, -2.616371, -7.919676,  1.473069,  0.655975,  1.481367, -1.116171, -0.674735};
	float jaspar_cne_pwm_74_matrix_row_2_[15] = { -2.616371, -2.212566, -2.616371,  1.489597,  0.241128,  1.497760, -3.304556, -2.616371, -3.304556,  1.497760, -7.919676, -2.212566, -3.304556, -0.369541,  1.329981};
	float jaspar_cne_pwm_74_matrix_row_3_[15] = { -2.453728, -2.453728,  1.248435, -8.160838, -8.160838, -8.160838, -3.545718, -8.160838,  1.248435, -2.857533, -2.166877,  0.678583, -2.453728, -1.608330, -2.857533};
	float *jaspar_cne_pwm_74_matrix[4] = { jaspar_cne_pwm_74_matrix_row_0_, jaspar_cne_pwm_74_matrix_row_1_, jaspar_cne_pwm_74_matrix_row_2_, jaspar_cne_pwm_74_matrix_row_3_};
	PWM jaspar_cne_pwm_74_ = {
/* accession        */ "CN0100",
/* name             */ "LM100",
/* label            */ " CN0100	23.6357721369821	LM100	unknown	; consensus \"ACTGAGCATGCTCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACTGAGCATGCTCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_74_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.529968,
/* max_score        */ 19.267653,
/* threshold        */ 0.802,
/* info content     */ 23.605852,
/* base_counts      */ {432, 564, 541, 338},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 125
};

	float jaspar_cne_pwm_75_matrix_row_0_[24] = { -0.241053, -0.692519,  0.579419,  0.068860, -0.692519, -1.941722, -1.251066,  0.496072,  0.656350,  0.826191, -7.245027,  0.068860,  0.451640,  0.826191, -0.154117, -7.245027, -7.245027,  0.579419,  1.272366,  1.023961, -7.245027, -7.245027,  1.167028, -1.537917};
	float jaspar_cne_pwm_75_matrix_row_1_[24] = { -0.605270,  0.779775, -0.095110, -1.009904, -2.388745,  1.156939, -1.009904, -0.787259,  0.692802, -7.003865, -2.388745, -2.388745, -7.003865,  0.435106, -7.003865, -7.003865,  0.897512, -7.003865, -7.003865, -7.003865, -7.003865, -2.388745, -7.003865, -0.605270};
	float jaspar_cne_pwm_75_matrix_row_2_[24] = { -1.009904, -0.451357, -0.605270, -0.787259,  1.212493, -0.200360, -1.296755, -1.700560, -7.003865, -2.388745, -7.003865, -2.388745,  0.933867, -7.003865, -7.003865,  1.098116, -7.003865, -7.003865, -7.003865, -7.003865, -0.200360, -0.095110, -0.787259,  1.265123};
	float jaspar_cne_pwm_75_matrix_row_3_[24] = {  0.727784, -0.154117, -0.441522,  0.618624, -1.028421, -1.251066,  1.023961,  0.356375, -2.629907,  0.193944,  1.252167,  0.856953, -7.245027, -2.629907,  0.997992,  0.193944,  0.496072,  0.579419, -7.245027, -0.241053,  1.073959,  1.023961, -7.245027, -1.941722};
	float *jaspar_cne_pwm_75_matrix[4] = { jaspar_cne_pwm_75_matrix_row_0_, jaspar_cne_pwm_75_matrix_row_1_, jaspar_cne_pwm_75_matrix_row_2_, jaspar_cne_pwm_75_matrix_row_3_};
	PWM jaspar_cne_pwm_75_ = {
/* accession        */ "CN0096",
/* name             */ "LM96",
/* label            */ " CN0096	23.0285780344639	LM96	unknown	; consensus \"WNNNNNWWMWTWRMWKYWAWKTAR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TNNTGCTWAATTGATGCWAATTAG",
/* library_name     */ "jaspar_cne",
/* length           */ 24,
/* matrixname       */ jaspar_cne_pwm_75_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -114.959251,
/* max_score        */ 22.382675,
/* threshold        */ 0.872,
/* info content     */ 22.946945,
/* base_counts      */ {396, 164, 199, 441},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_76_matrix_row_0_[15] = { -1.270853, -0.712306, -7.264814, -1.961509, -7.264814, -2.649694,  1.124773,  1.272378,  1.211765,  1.029485, -1.270853, -2.649694, -7.264814, -7.264814,  1.211765};
	float jaspar_cne_pwm_76_matrix_row_1_[15] = {  1.192706, -1.316542, -7.023652, -7.023652, -7.023652, -7.023652, -2.408532, -7.023652, -7.023652, -1.720347, -2.408532,  1.365935, -7.023652, -7.023652, -7.023652};
	float jaspar_cne_pwm_76_matrix_row_2_[15] = { -0.220147, -7.023652,  1.493741, -7.023652,  1.513540,  1.493741, -0.625057, -7.023652, -1.316542, -1.316542, -7.023652, -0.807046, -7.023652,  1.431878, -7.023652};
	float jaspar_cne_pwm_76_matrix_row_3_[15] = { -2.649694,  1.054172, -2.649694,  1.232380, -7.264814, -7.264814, -7.264814, -7.264814, -7.264814, -0.866219,  1.169215, -2.649694,  1.272378, -1.270853, -1.557704};
	float *jaspar_cne_pwm_76_matrix[4] = { jaspar_cne_pwm_76_matrix_row_0_, jaspar_cne_pwm_76_matrix_row_1_, jaspar_cne_pwm_76_matrix_row_2_, jaspar_cne_pwm_76_matrix_row_3_};
	PWM jaspar_cne_pwm_76_ = {
/* accession        */ "CN0225",
/* name             */ "LM225",
/* label            */ " CN0225	23.388903549835	LM225	unknown	; consensus \"CTGTGGAAAATCTGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGTGGAAAATCTGA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_76_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.232872,
/* max_score        */ 19.069855,
/* threshold        */ 0.800,
/* info content     */ 23.311092,
/* base_counts      */ {250, 88, 224, 203},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 51
};

	float jaspar_cne_pwm_77_matrix_row_0_[22] = { -0.293198, -1.753516, -1.140462,  0.480762, -1.920550, -2.059644, -1.158480, -2.680689, -2.626637, -3.698767, -2.186779,  1.150695,  0.447267,  0.160400, -2.653298, -2.313050, -2.001665,  1.187342, -3.219487, -3.555769, -2.526578, -2.121192};
	float jaspar_cne_pwm_77_matrix_row_1_[22] = { -1.604362, -1.864284,  1.024202,  0.619176, -0.134418, -0.067460,  1.256692, -0.343117, -4.634532, -3.942633, -3.250110, -2.238907, -0.589558, -0.647999, -0.770998, -2.931827, -1.501426, -3.189522, -2.412136, -2.655683,  0.750912,  0.961402};
	float jaspar_cne_pwm_77_matrix_row_2_[22] = {  0.843620,  1.392459,  0.072524, -1.512354, -0.992820,  0.644917, -1.261064, -3.132396,  1.484372,  1.497458,  1.461051, -1.304735,  0.500253, -0.852783, -1.880030,  1.471112, -1.341100, -1.295847,  1.466653, -2.194465, -1.628751, -1.803669};
	float jaspar_cne_pwm_77_matrix_row_3_[22] = { -0.173153, -2.169975, -1.519467, -1.134528,  0.894814,  0.192300, -1.294603,  1.068544, -3.698767, -3.960901, -3.172989, -2.169975, -1.287683,  0.501718,  1.104491, -4.653050,  1.117022, -3.086015, -2.896845,  1.223813,  0.514311,  0.236819};
	float *jaspar_cne_pwm_77_matrix[4] = { jaspar_cne_pwm_77_matrix_row_0_, jaspar_cne_pwm_77_matrix_row_1_, jaspar_cne_pwm_77_matrix_row_2_, jaspar_cne_pwm_77_matrix_row_3_};
	PWM jaspar_cne_pwm_77_ = {
/* accession        */ "CN0004",
/* name             */ "LM4",
/* label            */ " CN0004	22.9781135839097	LM4	unknown	; consensus \"RGCMTKCTGGGARTTGTAGTYY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GGCMTKCTGGGARWTGTAGTYC",
/* library_name     */ "jaspar_cne",
/* length           */ 22,
/* matrixname       */ jaspar_cne_pwm_77_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -55.358829,
/* max_score        */ 23.622719,
/* threshold        */ 0.855,
/* info content     */ 22.976887,
/* base_counts      */ {7080, 7415, 14716, 12083},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1877
};

	float jaspar_cne_pwm_78_matrix_row_0_[14] = { -1.210277,  1.138839, -1.900934, -1.497128, -7.204238, -7.204238, -2.589118, -7.204238, -7.204238, -7.204238, -7.204238, -2.589118, -2.589118, -7.204238};
	float jaspar_cne_pwm_78_matrix_row_1_[14] = { -0.746470, -2.347956, -6.963077, -6.963077, -2.347956,  1.448978,  1.470952, -6.963077, -6.963077, -1.659772, -6.963077, -6.963077, -6.963077, -6.963077};
	float jaspar_cne_pwm_78_matrix_row_2_[14] = {  1.305912, -1.659772, -6.963077, -6.963077, -6.963077, -2.347956, -6.963077, -6.963077,  1.470952, -6.963077, -0.564481,  1.355910, -2.347956,  1.492454};
	float jaspar_cne_pwm_78_matrix_row_3_[14] = { -7.204238, -1.497128,  1.229790,  1.207816,  1.251292, -1.900934, -2.589118,  1.272341, -1.900934,  1.229790,  1.138839, -0.805644,  1.229790, -2.589118};
	float *jaspar_cne_pwm_78_matrix[4] = { jaspar_cne_pwm_78_matrix_row_0_, jaspar_cne_pwm_78_matrix_row_1_, jaspar_cne_pwm_78_matrix_row_2_, jaspar_cne_pwm_78_matrix_row_3_};
	PWM jaspar_cne_pwm_78_ = {
/* accession        */ "CN0165",
/* name             */ "LM165",
/* label            */ " CN0165	22.8059318311726	LM165	unknown	; consensus \"GATTTCCTGTTGTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GATTTCCTGTTGTG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_78_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.797241,
/* max_score        */ 18.243656,
/* threshold        */ 0.790,
/* info content     */ 22.727777,
/* base_counts      */ {54, 100, 183, 335},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_79_matrix_row_0_[14] = { -7.224841, -7.224841, -7.224841,  1.187214,  1.272354, -7.224841, -7.224841,  1.209188, -7.224841, -7.224841, -7.224841, -7.224841,  0.271257,  0.906984};
	float jaspar_cne_pwm_79_matrix_row_1_[14] = {  1.513516, -1.680374, -6.983679, -0.989717, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.513516, -6.983679, -6.983679,  0.879973, -6.983679};
	float jaspar_cne_pwm_79_matrix_row_2_[14] = { -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -0.767073, -6.983679,  1.513516, -6.983679, -6.983679,  1.513516, -6.983679, -2.368558};
	float jaspar_cne_pwm_79_matrix_row_3_[14] = { -7.224841,  1.230690,  1.272354, -7.224841, -7.224841,  1.272354,  1.164746, -1.517731, -7.224841, -7.224841,  1.272354, -7.224841, -1.008235,  0.020101};
	float *jaspar_cne_pwm_79_matrix[4] = { jaspar_cne_pwm_79_matrix_row_0_, jaspar_cne_pwm_79_matrix_row_1_, jaspar_cne_pwm_79_matrix_row_2_, jaspar_cne_pwm_79_matrix_row_3_};
	PWM jaspar_cne_pwm_79_ = {
/* accession        */ "CN0134",
/* name             */ "LM134",
/* label            */ " CN0134	24.189676919438	LM134	unknown	; consensus \"CTTAATTAGCTGWA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTAATTAGCTGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_79_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.424278,
/* max_score        */ 17.722271,
/* threshold        */ 0.794,
/* info content     */ 24.091757,
/* base_counts      */ {192, 130, 104, 260},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_80_matrix_row_0_[19] = { -1.251066, -7.245027, -1.537917, -7.245027, -7.245027, -7.245027,  1.073959, -1.941722,  1.231552,  1.231552,  1.210503, -1.941722, -7.245027,  0.356375,  0.618624,  1.023961, -1.537917, -1.251066, -0.336272};
	float jaspar_cne_pwm_80_matrix_row_1_[19] = { -1.009904,  1.408190, -7.003865, -2.388745, -7.003865,  1.385722, -7.003865, -7.003865, -2.388745, -7.003865, -7.003865, -7.003865,  0.374519,  0.968946,  0.492232, -1.700560, -0.787259, -0.605270, -1.009904};
	float jaspar_cne_pwm_80_matrix_row_2_[19] = {  1.035615, -7.003865, -7.003865, -2.388745, -7.003865, -2.388745, -7.003865,  1.430164, -7.003865, -1.700560, -7.003865, -7.003865,  1.067353, -7.003865, -7.003865, -1.700560, -1.009904, -0.787259, -1.296755};
	float jaspar_cne_pwm_80_matrix_row_3_[19] = { -0.241053, -1.028421,  1.210503,  1.231552,  1.272366, -1.028421, -0.441522, -1.941722, -2.629907, -7.245027, -1.537917,  1.231552, -1.941722, -2.629907, -0.846432, -0.692519,  0.997992,  0.915777,  0.856953};
	float *jaspar_cne_pwm_80_matrix[4] = { jaspar_cne_pwm_80_matrix_row_0_, jaspar_cne_pwm_80_matrix_row_1_, jaspar_cne_pwm_80_matrix_row_2_, jaspar_cne_pwm_80_matrix_row_3_};
	PWM jaspar_cne_pwm_80_ = {
/* accession        */ "CN0160",
/* name             */ "LM160",
/* label            */ " CN0160	23.9941793382506	LM160	unknown	; consensus \"RCTTTCAGAAATSMMWWWW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCTTTCAGAAATGCAATTT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_80_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -106.538460,
/* max_score        */ 21.402836,
/* threshold        */ 0.843,
/* info content     */ 23.916929,
/* base_counts      */ {297, 175, 127, 351},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_81_matrix_row_0_[18] = {  0.768572,  0.234733, -7.204238, -1.900934, -2.589118,  1.229790,  1.272341, -7.204238, -7.204238, -7.204238, -0.987632,  1.162364,  1.251292, -1.210277, -7.204238,  1.251292,  0.291859,  0.397164};
	float jaspar_cne_pwm_81_matrix_row_1_[18] = { -1.659772, -6.963077,  0.040898, -2.347956, -6.963077, -6.963077, -6.963077, -2.347956, -6.963077, -6.963077, -0.564481, -1.659772, -6.963077, -6.963077,  1.492454, -2.347956, -0.159571, -1.255966};
	float jaspar_cne_pwm_81_matrix_row_2_[18] = {  0.127833,  0.415307,  1.253282, -6.963077, -6.963077, -1.659772, -6.963077, -6.963077, -6.963077,  1.331223,  1.253282, -2.347956, -6.963077,  0.127833, -2.347956, -6.963077,  0.040898, -0.277216};
	float jaspar_cne_pwm_81_matrix_row_3_[18] = { -0.987632,  0.109648, -7.204238,  1.207816,  1.251292, -7.204238, -7.204238,  1.251292,  1.272341, -0.518378, -7.204238, -1.900934, -2.589118,  0.866980, -7.204238, -7.204238, -0.295484,  0.234733};
	float *jaspar_cne_pwm_81_matrix[4] = { jaspar_cne_pwm_81_matrix_row_0_, jaspar_cne_pwm_81_matrix_row_1_, jaspar_cne_pwm_81_matrix_row_2_, jaspar_cne_pwm_81_matrix_row_3_};
	PWM jaspar_cne_pwm_81_ = {
/* accession        */ "CN0088",
/* name             */ "LM88",
/* label            */ " CN0088	23.6319870327433	LM88	unknown	; consensus \"WRGTTAATTGGAATCANW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ANGTTAATTGGAATCANW",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_81_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -105.212708,
/* max_score        */ 19.219942,
/* threshold        */ 0.830,
/* info content     */ 23.545624,
/* base_counts      */ {327, 83, 177, 277},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_82_matrix_row_0_[15] = {  1.143855, -1.686227, -7.393337, -1.686227, -1.176731, -7.393337, -7.393337, -2.778216, -7.393337, -7.393337,  1.237363,  1.272449,  1.237363, -1.176731, -2.090032};
	float jaspar_cne_pwm_82_matrix_row_1_[15] = { -1.158213, -2.537054, -7.152175, -7.152175,  1.385017, -0.935569, -7.152175, -1.158213,  1.478526,  1.442164, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175};
	float jaspar_cne_pwm_82_matrix_row_2_[15] = { -1.848870, -2.537054,  0.631466,  1.442164, -2.537054, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -7.152175, -1.848870, -2.537054,  1.460510};
	float jaspar_cne_pwm_82_matrix_row_3_[15] = { -2.778216,  1.182314,  0.738488, -2.778216, -2.778216,  1.182314,  1.272449,  1.182314, -2.090032, -1.399375, -2.090032, -7.393337, -7.393337,  1.163269, -2.778216};
	float *jaspar_cne_pwm_82_matrix[4] = { jaspar_cne_pwm_82_matrix_row_0_, jaspar_cne_pwm_82_matrix_row_1_, jaspar_cne_pwm_82_matrix_row_2_, jaspar_cne_pwm_82_matrix_row_3_};
	PWM jaspar_cne_pwm_82_ = {
/* accession        */ "CN0212",
/* name             */ "LM212",
/* label            */ " CN0212	23.8848573552489	LM212	unknown	; consensus \"ATGGCTTTCCAAATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTGCTTTCCAAATG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_82_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.607719,
/* max_score        */ 18.820559,
/* threshold        */ 0.803,
/* info content     */ 23.817080,
/* base_counts      */ {240, 175, 140, 315},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 58
};

	float jaspar_cne_pwm_83_matrix_row_0_[17] = { -1.858410, -7.161715,  1.181363,  1.272314, -2.546595, -7.161715, -7.161715,  0.701936, -1.167754, -1.454605, -2.546595, -0.609207,  1.181363, -1.858410,  1.157271, -2.546595, -1.858410};
	float jaspar_cne_pwm_83_matrix_row_1_[17] = { -0.234692, -6.920553, -2.305433, -6.920553, -1.213443, -2.305433, -6.920553,  0.324389, -1.213443, -1.213443,  1.295805,  1.348435, -1.617248, -2.305433, -2.305433, -6.920553, -6.920553};
	float jaspar_cne_pwm_83_matrix_row_2_[17] = {  1.150665, -1.617248, -1.213443, -6.920553, -2.305433, -1.617248,  1.513476, -0.521958, -2.305433, -6.920553, -2.305433, -6.920553, -6.920553,  1.348435, -1.617248, -6.920553,  1.422525};
	float jaspar_cne_pwm_83_matrix_row_3_[17] = { -1.167754,  1.227872, -7.161715, -7.161715,  1.157271,  1.204888, -7.161715, -7.161715,  1.081304,  1.132584, -0.609207, -7.161715, -1.858410, -1.167754, -1.858410,  1.250340, -1.858410};
	float *jaspar_cne_pwm_83_matrix[4] = { jaspar_cne_pwm_83_matrix_row_0_, jaspar_cne_pwm_83_matrix_row_1_, jaspar_cne_pwm_83_matrix_row_2_, jaspar_cne_pwm_83_matrix_row_3_};
	PWM jaspar_cne_pwm_83_ = {
/* accession        */ "CN0089",
/* name             */ "LM89",
/* label            */ " CN0089	23.7300516279747	LM89	unknown	; consensus \"GTAATTGATTCCAGATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GTAATTGATTCCAGATG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_83_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.682121,
/* max_score        */ 20.627846,
/* threshold        */ 0.821,
/* info content     */ 23.653879,
/* base_counts      */ {220, 113, 177, 272},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_84_matrix_row_0_[19] = { -3.600189, -1.816714, -3.600189, -1.662802, -2.508199,  1.117337, -1.662802, -2.912004, -0.565141,  1.134880, -0.776338, -0.474210, -3.600189, -3.600189,  1.226222,  1.099481,  0.484372, -1.411804, -0.277578};
	float jaspar_cne_pwm_84_matrix_row_1_[19] = {  1.256094, -1.288286,  1.410230,  1.358499,  1.426896, -2.670842,  0.214820, -2.670842, -2.267037, -7.974147,  1.174424, -3.359027, -2.267037,  1.275510, -2.670842, -2.267037, -7.974147, -2.670842, -3.359027};
	float jaspar_cne_pwm_84_matrix_row_2_[19] = { -7.974147, -7.974147, -2.267037, -3.359027, -3.359027, -1.980186, -2.670842, -1.575552,  1.303945, -0.660260, -1.757541, -0.660260, -2.670842, -2.670842, -2.670842, -0.478050, -2.267037,  1.340643,  1.216092};
	float jaspar_cne_pwm_84_matrix_row_3_[19] = { -0.242499,  1.160630, -1.411804, -1.211335, -1.662802, -1.044421,  0.855884,  1.193964, -3.600189, -2.912004, -0.836926,  0.922568,  1.226222, -0.390863, -2.912004, -8.215309,  0.624112, -1.306555, -1.998703};
	float *jaspar_cne_pwm_84_matrix[4] = { jaspar_cne_pwm_84_matrix_row_0_, jaspar_cne_pwm_84_matrix_row_1_, jaspar_cne_pwm_84_matrix_row_2_, jaspar_cne_pwm_84_matrix_row_3_};
	PWM jaspar_cne_pwm_84_ = {
/* accession        */ "CN0105",
/* name             */ "LM105",
/* label            */ " CN0105	23.0217440403128	LM105	unknown	; consensus \"CTCCCATTGACTTCAATGG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTCCCATTGACTTCAATGG",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_84_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.542839,
/* max_score        */ 22.323637,
/* threshold        */ 0.831,
/* info content     */ 23.001083,
/* base_counts      */ {652, 716, 395, 745},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 132
};

	float jaspar_cne_pwm_85_matrix_row_0_[21] = { -0.859820, -2.567232, -2.668980,  1.231921, -2.567232, -1.547078,  1.208545, -2.823071, -2.743061,  0.653912, -0.504332, -1.923835, -2.567232,  1.043372, -3.167740,  1.243407, -1.890504, -0.713224, -0.268168, -0.698187, -1.004262};
	float jaspar_cne_pwm_85_matrix_row_1_[21] = { -0.389695, -0.408474,  1.444520, -3.120608, -2.501899,  1.159314, -2.541104,  1.446005,  1.423499, -0.452062,  0.187682, -2.715381, -2.581909, -0.529217,  1.288884, -3.966955, -2.501899,  1.004671, -1.699764,  0.958646,  1.188527};
	float jaspar_cne_pwm_85_matrix_row_2_[21] = { -0.049919, -0.998120, -2.715381, -3.456795,  1.460733, -0.267277, -2.427818, -1.998947, -2.668883, -0.534579, -0.561827,  1.434065,  1.466564, -1.699764, -0.422793, -2.926578,  1.440053, -0.518580,  0.658002, -0.049919, -1.953495};
	float jaspar_cne_pwm_85_matrix_row_3_[21] = {  0.625189,  0.986881, -2.130132, -2.474887, -3.110615, -1.376471, -2.390353, -2.668980, -1.657606, -0.373884,  0.452099, -2.567232, -3.515802, -2.069520, -1.811735, -3.056576, -3.228328, -0.759742,  0.136207, -1.282954, -0.663956};
	float *jaspar_cne_pwm_85_matrix[4] = { jaspar_cne_pwm_85_matrix_row_0_, jaspar_cne_pwm_85_matrix_row_1_, jaspar_cne_pwm_85_matrix_row_2_, jaspar_cne_pwm_85_matrix_row_3_};
	PWM jaspar_cne_pwm_85_ = {
/* accession        */ "CN0009",
/* name             */ "LM9",
/* label            */ " CN0009	22.0665388934836	LM9	unknown	; consensus \"TTCAGCACCWYGGACAGMKMC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTCAGCACCANGGACAGCNCC",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_85_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -47.820244,
/* max_score        */ 23.818804,
/* threshold        */ 0.832,
/* info content     */ 22.065060,
/* base_counts      */ {7728, 10311, 8001, 4263},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 1443
};

	float jaspar_cne_pwm_86_matrix_row_0_[21] = {  1.109254, -1.916102, -2.319907, -7.623212, -1.916102, -1.916102, -1.629250, -1.224617, -0.532302, -0.819706, -0.184240,  1.186800, -7.623212, -2.319907,  1.092996,  1.156500, -7.623212, -1.406606,  1.025185, -1.406606, -0.819706};
	float jaspar_cne_pwm_86_matrix_row_1_[21] = { -1.388088, -0.378076, -7.382050, -1.388088,  1.382160,  1.350416, -7.382050, -7.382050, -1.388088, -1.165444, -2.766929, -2.078745, -1.674940, -2.078745, -1.388088, -7.382050, -2.766929, -2.078745, -2.078745,  1.230635,  0.834309};
	float jaspar_cne_pwm_86_matrix_row_2_[21] = { -2.078745, -7.382050, -2.766929, -2.078745, -2.078745, -7.382050, -7.382050,  1.397662,  1.115145,  1.248651,  1.193601, -2.078745, -2.766929, -2.766929, -1.674940, -2.078745, -2.766929,  1.382160, -0.137108, -0.829542, -2.766929};
	float jaspar_cne_pwm_86_matrix_row_3_[21] = { -1.406606,  1.059665,  1.230597,  1.186800, -1.629250, -0.937351,  1.216210, -2.319907, -0.937351, -1.916102, -2.319907, -2.319907,  1.216210,  1.201613, -1.406606, -1.224617,  1.244779, -2.319907, -7.623212, -1.224617,  0.240440};
	float *jaspar_cne_pwm_86_matrix[4] = { jaspar_cne_pwm_86_matrix_row_0_, jaspar_cne_pwm_86_matrix_row_1_, jaspar_cne_pwm_86_matrix_row_2_, jaspar_cne_pwm_86_matrix_row_3_};
	PWM jaspar_cne_pwm_86_ = {
/* accession        */ "CN0110",
/* name             */ "LM110",
/* label            */ " CN0110	26.2951091725639	LM110	unknown	; consensus \"ATTTCCTGGGGATTAATGACY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTTCCTGGGGATTAATGACC",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_86_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.268990,
/* max_score        */ 25.061346,
/* threshold        */ 0.901,
/* info content     */ 26.249813,
/* base_counts      */ {392, 263, 326, 552},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 73
};

	float jaspar_cne_pwm_87_matrix_row_0_[23] = { -0.756577,  0.731840,  1.084737,  1.167147, -0.676546, -1.631852, -0.056580, -3.421947, -1.814107, -2.835047, -2.324666, -2.467664,  1.144582, -0.590614,  0.137158, -2.260169, -2.393611, -1.988384, -0.890089, -1.319567,  0.048084, -0.369849,  0.987665};
	float jaspar_cne_pwm_87_matrix_row_1_[23] = { -0.217406, -1.128403, -1.181032, -2.998796, -2.083504, -0.326197, -1.181032,  1.416390, -1.128403, -0.515415, -3.180784, -2.306481, -2.844883, -2.152449,  0.856789, -1.656292,  1.317049,  1.214157, -0.543582, -0.167150, -1.795988, -1.747222, -1.459659};
	float jaspar_cne_pwm_87_matrix_row_2_[23] = { -0.572566,  0.075391, -0.985336, -1.208424,  1.271588, -1.656292,  0.941153, -2.593885, -9.397390, -2.393417, -1.236587,  1.424406, -0.920811,  1.276228, -1.103091, -2.306481, -2.226502, -2.998796, -1.901293, -0.784706,  0.976132,  1.206741, -0.422646};
	float jaspar_cne_pwm_87_matrix_row_3_[23] = {  0.706443, -0.938871, -2.547643, -2.467664, -1.814107,  0.977909, -0.990156, -1.422194,  1.148723,  1.089132,  1.167147, -1.941886, -2.634579, -2.199581, -1.161973,  1.175228, -0.770562, -0.288363,  0.950579,  0.824579, -1.183022, -2.142455, -1.665742};
	float *jaspar_cne_pwm_87_matrix[4] = { jaspar_cne_pwm_87_matrix_row_0_, jaspar_cne_pwm_87_matrix_row_1_, jaspar_cne_pwm_87_matrix_row_2_, jaspar_cne_pwm_87_matrix_row_3_};
	PWM jaspar_cne_pwm_87_ = {
/* accession        */ "CN0093",
/* name             */ "LM93",
/* label            */ " CN0093	22.163820924347	LM93	unknown	; consensus \"YAAAGTGCTTTGAGCTCCTTGGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TAAAGTGCTTTGAGCTCCTTGGA",
/* library_name     */ "jaspar_cne",
/* length           */ 23,
/* matrixname       */ jaspar_cne_pwm_87_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -56.385452,
/* max_score        */ 25.056343,
/* threshold        */ 0.846,
/* info content     */ 22.159897,
/* base_counts      */ {3235, 2368, 3023, 3978},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 548
};

	float jaspar_cne_pwm_88_matrix_row_0_[17] = { -8.413857,  1.253971, -3.798737, -0.717190, -2.015262, -2.706747, -3.110552, -1.242969,  0.365854,  0.944989,  0.786534, -3.110552,  1.221816, -3.798737, -2.706747, -0.589411, -2.197251};
	float jaspar_cne_pwm_88_matrix_row_1_[17] = {  1.488784, -8.172695, -2.465585,  1.315353, -2.465585, -8.172695, -1.956089, -2.465585, -0.309044, -0.522526, -0.011891,  1.488784, -8.172695, -3.557575,  1.409278, -1.486834, -1.263940};
	float jaspar_cne_pwm_88_matrix_row_2_[17] = { -2.869390, -8.172695,  1.409278, -2.869390, -3.557575,  1.495133, -0.622560, -0.476028, -0.348249, -0.676598, -0.389055, -8.172695, -8.172695,  1.402358, -3.557575, -3.557575,  1.330389};
	float jaspar_cne_pwm_88_matrix_row_3_[17] = { -3.110552, -2.706747, -1.322947, -2.197251,  1.208659, -8.413857,  1.096662,  1.003579, -0.001802, -2.015262, -2.706747, -3.110552, -1.727996, -1.099970, -1.322947,  1.035579, -1.322947};
	float *jaspar_cne_pwm_88_matrix[4] = { jaspar_cne_pwm_88_matrix_row_0_, jaspar_cne_pwm_88_matrix_row_1_, jaspar_cne_pwm_88_matrix_row_2_, jaspar_cne_pwm_88_matrix_row_3_};
	PWM jaspar_cne_pwm_88_ = {
/* accession        */ "CN0022",
/* name             */ "LM22",
/* label            */ " CN0022	21.7771243115557	LM22	unknown	; consensus \"CAGCTGTTWAACAGCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAGCTGTTNAACAGCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_88_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -75.329025,
/* max_score        */ 20.256998,
/* threshold        */ 0.804,
/* info content     */ 21.759331,
/* base_counts      */ {674, 706, 696, 661},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 161
};

	float jaspar_cne_pwm_89_matrix_row_0_[14] = { -1.251066, -1.941722, -2.629907,  1.189002, -2.629907, -1.941722,  1.252167,  1.231552, -7.245027, -7.245027, -1.537917, -2.629907, -7.245027, -1.941722};
	float jaspar_cne_pwm_89_matrix_row_1_[14] = { -7.003865, -2.388745,  1.408190, -7.003865, -0.787259, -1.296755, -7.003865, -7.003865, -2.388745, -7.003865, -7.003865, -1.700560,  1.513528,  1.451665};
	float jaspar_cne_pwm_89_matrix_row_2_[14] = {  1.315121,  1.430164, -1.009904, -7.003865,  1.385722, -1.009904, -2.388745, -7.003865, -7.003865, -1.700560, -7.003865, -2.388745, -7.003865, -7.003865};
	float jaspar_cne_pwm_89_matrix_row_3_[14] = { -1.028421, -2.629907, -7.245027, -1.251066, -7.245027,  1.073959, -7.245027, -1.941722,  1.252167,  1.231552,  1.210503,  1.189002, -7.245027, -2.629907};
	float *jaspar_cne_pwm_89_matrix[4] = { jaspar_cne_pwm_89_matrix_row_0_, jaspar_cne_pwm_89_matrix_row_1_, jaspar_cne_pwm_89_matrix_row_2_, jaspar_cne_pwm_89_matrix_row_3_};
	PWM jaspar_cne_pwm_89_ = {
/* accession        */ "CN0186",
/* name             */ "LM186",
/* label            */ " CN0186	22.1134208863855	LM186	unknown	; consensus \"GGCAGTAATTTTCC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GGCAGTAATTTTCC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_89_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.691017,
/* max_score        */ 18.134296,
/* threshold        */ 0.788,
/* info content     */ 22.044100,
/* base_counts      */ {159, 154, 143, 244},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_90_matrix_row_0_[17] = {  0.083226, -1.858410, -2.546595, -1.858410, -1.454605, -7.161715, -7.161715, -7.161715,  1.272314,  1.250340,  0.776017, -1.858410,  0.534952,  1.272314, -7.161715,  1.181363,  0.662731};
	float jaspar_cne_pwm_90_matrix_row_1_[17] = { -0.926592, -6.920553, -0.521958,  1.348435, -0.926592, -6.920553, -6.920553, -2.305433, -6.920553, -2.305433, -6.920553, -6.920553,  0.729616, -6.920553, -6.920553, -6.920553, -2.305433};
	float jaspar_cne_pwm_90_matrix_row_2_[17] = { -6.920553,  0.170357,  1.211272, -2.305433, -6.920553, -6.920553,  1.348435, -6.920553, -6.920553, -6.920553, -6.920553, -6.920553, -1.213443, -6.920553,  1.513476, -0.926592,  0.518419};
	float jaspar_cne_pwm_90_matrix_row_3_[17] = {  0.776017,  0.909503, -0.945109, -1.167754,  1.107273,  1.272314, -0.609207,  1.250340, -7.161715, -7.161715,  0.334382,  1.227872, -7.161715, -7.161715, -7.161715, -7.161715, -1.454605};
	float *jaspar_cne_pwm_90_matrix[4] = { jaspar_cne_pwm_90_matrix_row_0_, jaspar_cne_pwm_90_matrix_row_1_, jaspar_cne_pwm_90_matrix_row_2_, jaspar_cne_pwm_90_matrix_row_3_};
	PWM jaspar_cne_pwm_90_ = {
/* accession        */ "CN0170",
/* name             */ "LM170",
/* label            */ " CN0170	23.6694106453484	LM170	unknown	; consensus \"WNNCTTGTAAWTMAGAR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGCTTGTAAATMAGAA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_90_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -106.215660,
/* max_score        */ 19.109629,
/* threshold        */ 0.821,
/* info content     */ 23.576319,
/* base_counts      */ {278, 77, 156, 271},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_91_matrix_row_0_[18] = { -2.485217, -2.198365, -2.485217,  1.200418,  1.183613,  1.233206, -2.889022, -2.889022, -1.506466, -1.639819, -8.192327, -3.577206, -2.485217, -2.198365, -2.889022, -0.947385,  1.208717,  1.216946};
	float jaspar_cne_pwm_91_matrix_row_1_[18] = {  1.308061, -1.552570,  0.971627, -2.647860, -3.336044, -7.951165, -1.734559,  1.424775,  1.317539, -1.957203, -7.951165, -1.147660,  0.748516, -2.244055, -7.951165, -2.647860, -2.244055, -2.647860};
	float jaspar_cne_pwm_91_matrix_row_2_[18] = { -0.947191, -7.951165, -1.265304, -2.244055, -1.042410, -2.244055, -1.265304, -3.336044, -7.951165, -7.951165, -3.336044, -2.647860, -0.300996, -1.734559,  1.482399,  1.363626, -1.734559, -2.244055};
	float jaspar_cne_pwm_91_matrix_row_3_[18] = { -1.283572,  1.192051,  0.174276, -2.198365, -8.192327, -2.889022,  1.149130, -1.506466, -0.878440,  1.183613,  1.264952,  1.175103,  0.219728,  1.175103, -2.889022, -2.889022, -8.192327, -2.889022};
	float *jaspar_cne_pwm_91_matrix[4] = { jaspar_cne_pwm_91_matrix_row_0_, jaspar_cne_pwm_91_matrix_row_1_, jaspar_cne_pwm_91_matrix_row_2_, jaspar_cne_pwm_91_matrix_row_3_};
	PWM jaspar_cne_pwm_91_ = {
/* accession        */ "CN0204",
/* name             */ "LM204",
/* label            */ " CN0204	24.943067891469	LM204	unknown	; consensus \"CTYAAATCCTTTYTGGAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTCAAATCCTTTYTGGAA",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_91_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.260689,
/* max_score        */ 21.799393,
/* threshold        */ 0.844,
/* info content     */ 24.918365,
/* base_counts      */ {658, 501, 317, 846},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 129
};

	float jaspar_cne_pwm_92_matrix_row_0_[14] = { -7.224841,  1.272354,  1.118237, -7.224841,  1.164746, -7.224841, -0.826246,  1.044147,  1.272354, -2.609720, -2.609720, -7.224841, -7.224841, -2.609720};
	float jaspar_cne_pwm_92_matrix_row_1_[14] = {  1.492901, -6.983679, -6.983679, -6.983679, -6.983679, -2.368558, -1.276569, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.471852, -6.983679};
	float jaspar_cne_pwm_92_matrix_row_2_[14] = { -2.368558, -6.983679, -0.767073,  1.513516, -0.767073, -6.983679, -1.680374, -0.297818, -6.983679, -6.983679,  0.989132,  1.513516, -6.983679, -6.983679};
	float jaspar_cne_pwm_92_matrix_row_3_[14] = { -7.224841, -7.224841, -1.921536, -7.224841, -7.224841,  1.251739,  1.018179, -1.921536, -7.224841,  1.251739,  0.325295, -7.224841, -1.921536,  1.251739};
	float *jaspar_cne_pwm_92_matrix[4] = { jaspar_cne_pwm_92_matrix_row_0_, jaspar_cne_pwm_92_matrix_row_1_, jaspar_cne_pwm_92_matrix_row_2_, jaspar_cne_pwm_92_matrix_row_3_};
	PWM jaspar_cne_pwm_92_ = {
/* accession        */ "CN0115",
/* name             */ "LM115",
/* label            */ " CN0115	22.9283861763997	LM115	unknown	; consensus \"CAAGATTAATKGCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAAGATTAATGGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_92_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.397491,
/* max_score        */ 17.626150,
/* threshold        */ 0.790,
/* info content     */ 22.843599,
/* base_counts      */ {232, 99, 148, 207},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_93_matrix_row_0_[18] = {  0.969743, -2.368942, -8.362904, -1.454149, -1.810396, -3.059599,  1.212149, -3.059599,  0.951887, -1.358930,  1.219069, -3.747783, -3.747783, -2.368942, -1.677043, -3.059599,  1.205181, -1.358930};
	float jaspar_cne_pwm_93_matrix_row_1_[18] = { -0.876800, -3.506621, -0.876800, -8.121742, -1.905136,  1.467103, -1.905136, -2.414631, -2.414631,  0.983349, -3.506621, -2.818437, -1.212987, -8.121742, -2.818437,  1.453311, -1.905136, -1.030832};
	float jaspar_cne_pwm_93_matrix_row_2_[18] = { -1.905136, -2.818437, -3.506621, -3.506621,  1.417974, -3.506621, -3.506621, -2.818437, -0.950853, -0.338101, -3.506621, -8.121742, -8.121742, -2.414631,  1.396157, -3.506621, -8.121742, -2.414631};
	float jaspar_cne_pwm_93_matrix_row_3_[18] = { -0.712735,  1.225941,  1.169592,  1.198164, -3.059599, -2.368942, -2.655793,  1.225941, -0.499252, -0.425172, -1.964309,  1.252968,  1.198164,  1.225941, -1.810396, -1.964309, -2.146298,  1.086532};
	float *jaspar_cne_pwm_93_matrix[4] = { jaspar_cne_pwm_93_matrix_row_0_, jaspar_cne_pwm_93_matrix_row_1_, jaspar_cne_pwm_93_matrix_row_2_, jaspar_cne_pwm_93_matrix_row_3_};
	PWM jaspar_cne_pwm_93_ = {
/* accession        */ "CN0101",
/* name             */ "LM101",
/* label            */ " CN0101	25.5644512024035	LM101	unknown	; consensus \"ATTTGCATACATTTGCAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTTGCATACATTTGCAT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_93_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.535683,
/* max_score        */ 21.859169,
/* threshold        */ 0.850,
/* info content     */ 25.544353,
/* base_counts      */ {719, 457, 333, 1245},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 153
};

	float jaspar_cne_pwm_94_matrix_row_0_[20] = { -1.921536, -7.224841, -1.517731,  1.209188,  1.018179, -0.421336, -0.672333, -7.224841, -1.517731, -1.921536,  0.676537, -7.224841,  1.251739,  1.251739, -7.224841, -7.224841,  0.325295, -2.609720,  1.187214, -7.224841};
	float jaspar_cne_pwm_94_matrix_row_1_[20] = { -2.368558,  1.359399,  1.428376, -2.368558, -1.680374,  0.455293, -1.680374, -2.368558, -6.983679, -0.431171, -2.368558, -1.680374, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.428376, -1.680374, -1.680374};
	float jaspar_cne_pwm_94_matrix_row_2_[20] = { -1.680374, -0.989717, -6.983679, -6.983679, -0.180173, -1.276569, -0.989717, -0.989717,  1.428376,  1.310621,  0.455293, -6.983679, -2.368558, -6.983679, -6.983679, -6.983679,  0.954053, -2.368558, -6.983679, -1.680374};
	float jaspar_cne_pwm_94_matrix_row_3_[20] = {  1.164746, -1.517731, -2.609720, -1.921536, -7.224841,  0.376562,  0.964126,  1.164746, -2.609720, -7.224841, -1.230879,  1.230690, -7.224841, -2.609720,  1.272354,  1.272354, -1.921536, -1.921536, -1.921536,  1.187214};
	float *jaspar_cne_pwm_94_matrix[4] = { jaspar_cne_pwm_94_matrix_row_0_, jaspar_cne_pwm_94_matrix_row_1_, jaspar_cne_pwm_94_matrix_row_2_, jaspar_cne_pwm_94_matrix_row_3_};
	PWM jaspar_cne_pwm_94_ = {
/* accession        */ "CN0131",
/* name             */ "LM131",
/* label            */ " CN0131	27.4002809055401	LM131	unknown	; consensus \"TCCAAYTTGGATAATTGCAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCCAAYTTGGATAATTGCAT",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_94_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -117.229408,
/* max_score        */ 23.215319,
/* threshold        */ 0.903,
/* info content     */ 27.315044,
/* base_counts      */ {298, 170, 160, 352},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_95_matrix_row_0_[17] = { -1.215765, -0.299774,  0.986555, -4.202047, -1.503281, -2.823206,  1.226126, -4.202047, -1.813193,  1.035079, -2.013662, -2.600561, -3.513863, -2.823206, -4.202047,  1.055912, -0.522868};
	float jaspar_cne_pwm_95_matrix_row_1_[17] = {  0.398739,  1.053111, -0.751559, -2.868895, -0.712354,  1.436381, -3.960885, -3.272701,  1.427373, -2.868895,  1.165022, -8.576005, -3.272701, -1.572031,  1.458554, -1.197622,  0.106872};
	float jaspar_cne_pwm_95_matrix_row_2_[17] = { -0.332986, -1.772500, -1.572031, -2.177410, -3.960885, -2.868895, -3.272701, -2.359399, -8.576005, -3.960885, -1.485096, -8.576005,  1.493039, -2.359399, -2.177410, -8.576005,  0.912043};
	float jaspar_cne_pwm_95_matrix_row_3_[17] = {  0.432490, -0.810467, -1.033527,  1.230464,  1.081358, -1.813193, -2.131306,  1.239084, -2.013662, -0.361637, -0.299774,  1.251877, -4.202047,  1.186211, -2.418572, -0.777687, -1.908413};
	float *jaspar_cne_pwm_95_matrix[4] = { jaspar_cne_pwm_95_matrix_row_0_, jaspar_cne_pwm_95_matrix_row_1_, jaspar_cne_pwm_95_matrix_row_2_, jaspar_cne_pwm_95_matrix_row_3_};
	PWM jaspar_cne_pwm_95_ = {
/* accession        */ "CN0054",
/* name             */ "LM54",
/* label            */ " CN0054	21.1215454846814	LM54	unknown	; consensus \"YMATTCATCACTGTCAR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "YCATTCATCACTGTCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_95_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -68.593323,
/* max_score        */ 19.670681,
/* threshold        */ 0.798,
/* info content     */ 21.111042,
/* base_counts      */ {958, 1221, 467, 1451},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 241
};

	float jaspar_cne_pwm_96_matrix_row_0_[14] = { -7.245027,  1.144560, -0.846432, -0.241053, -7.245027, -7.245027, -7.245027, -7.245027,  0.886797,  1.272366,  1.231552, -7.245027, -7.245027, -7.245027};
	float jaspar_cne_pwm_96_matrix_row_1_[14] = {  1.472714, -7.003865, -7.003865,  1.265123, -7.003865, -2.388745, -7.003865, -7.003865, -1.296755, -7.003865, -1.700560, -7.003865, -7.003865, -7.003865};
	float jaspar_cne_pwm_96_matrix_row_2_[14] = { -1.700560, -7.003865,  1.385722, -7.003865, -7.003865,  1.493329, -1.700560,  1.513528, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865,  1.513528};
	float jaspar_cne_pwm_96_matrix_row_3_[14] = { -7.245027, -0.846432, -7.245027, -7.245027,  1.272366, -7.245027,  1.231552, -7.245027, -0.074139, -7.245027, -7.245027,  1.272366,  1.272366, -7.245027};
	float *jaspar_cne_pwm_96_matrix[4] = { jaspar_cne_pwm_96_matrix_row_0_, jaspar_cne_pwm_96_matrix_row_1_, jaspar_cne_pwm_96_matrix_row_2_, jaspar_cne_pwm_96_matrix_row_3_};
	PWM jaspar_cne_pwm_96_ = {
/* accession        */ "CN0091",
/* name             */ "LM91",
/* label            */ " CN0091	24.1856242757097	LM91	unknown	; consensus \"CARCTGTGWAATTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAGCTGTGAAATTG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_96_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.948044,
/* max_score        */ 18.227871,
/* threshold        */ 0.794,
/* info content     */ 24.091055,
/* base_counts      */ {193, 93, 197, 217},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_97_matrix_row_0_[24] = {  1.065808, -0.927891, -0.557624, -0.445171,  1.253583,  1.241842,  1.075153, -1.250533, -0.445171, -3.597397, -4.285581, -1.461730,  1.229961, -1.586815,  1.217938, -1.404604,  1.042055,  0.099041, -1.729813, -0.798721, -1.204035, -1.729813,  0.967210,  0.914009};
	float jaspar_cne_pwm_97_matrix_row_1_[24] = { -1.220568, -1.568630, -8.659539, -4.044419, -4.044419, -8.659539, -2.107032, -0.470573, -2.665578,  1.375020, -2.107032, -2.260945, -2.952429,  1.396712, -2.952429,  0.236227, -1.009371,  0.510083,  0.995551,  1.121837, -1.009371, -0.835094, -1.009371, -1.009371};
	float jaspar_cne_pwm_97_matrix_row_2_[24] = { -1.414598, -1.973679,  1.297536,  1.273555, -2.952429, -2.665578, -0.498736, -1.568630,  1.238986, -2.260945, -3.356235,  1.400994, -2.107032, -2.442934, -2.260945,  0.982648, -0.758162, -0.795888, -1.414598, -2.442934, -1.973679, -1.058137, -1.281156, -0.557559};
	float jaspar_cne_pwm_97_matrix_row_3_[24] = { -1.404604,  1.065808, -2.097196, -2.214841, -4.285581, -2.906740, -2.684096,  0.967210, -1.809792, -1.037050,  1.233937, -2.684096, -4.285581, -2.097196, -2.684096, -1.461730, -2.502107, -0.217825,  0.074043, -0.445171,  1.056374,  1.022637, -0.829483, -1.076256};
	float *jaspar_cne_pwm_97_matrix[4] = { jaspar_cne_pwm_97_matrix_row_0_, jaspar_cne_pwm_97_matrix_row_1_, jaspar_cne_pwm_97_matrix_row_2_, jaspar_cne_pwm_97_matrix_row_3_};
	PWM jaspar_cne_pwm_97_ = {
/* accession        */ "CN0011",
/* name             */ "LM11",
/* label            */ " CN0011	26.0708555289977	LM11	unknown	; consensus \"ATGGAAATGCTGACAGAMCCTTAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATGGAAATGCTGACAGANCCTTAA",
/* library_name     */ "jaspar_cne",
/* length           */ 24,
/* matrixname       */ jaspar_cne_pwm_97_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -71.197052,
/* max_score        */ 26.946449,
/* threshold        */ 0.940,
/* info content     */ 26.060490,
/* base_counts      */ {2423, 1169, 1255, 1441},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 262
};

	float jaspar_cne_pwm_98_matrix_row_0_[20] = {  1.177279,  1.118447,  1.162892, -2.373224,  0.840864, -2.373224, -0.505641, -1.682568,  1.232841,  0.362951, -1.682568,  1.087680, -1.969419, -7.676529, -1.459923, -1.459923, -3.061409, -7.676529, -3.061409, -1.459923};
	float jaspar_cne_pwm_98_matrix_row_1_[20] = { -7.435367, -7.435367, -2.820247, -2.132062, -0.056983, -2.132062, -2.820247,  1.374645, -2.820247, -0.749506, -7.435367, -2.132062, -2.820247,  1.344344,  1.389458,  1.195333, -0.882859, -0.882859, -1.218761, -0.749506};
	float jaspar_cne_pwm_98_matrix_row_2_[20] = { -1.218761, -1.036772, -1.036772,  1.418441, -1.218761, -1.441406,  1.297099, -2.132062, -2.820247,  0.753600, -1.441406, -1.441406, -1.728257, -2.820247, -2.132062, -7.435367, -2.820247, -1.728257, -1.728257, -1.441406};
	float jaspar_cne_pwm_98_matrix_row_3_[20] = { -2.373224, -1.459923, -3.061409, -1.969419, -1.277934,  1.162892, -3.061409, -1.682568, -3.061409, -2.373224,  1.162892, -1.124021,  1.177279, -0.672555, -2.373224, -0.298146,  1.148296,  1.133483,  1.148296,  1.023152};
	float *jaspar_cne_pwm_98_matrix[4] = { jaspar_cne_pwm_98_matrix_row_0_, jaspar_cne_pwm_98_matrix_row_1_, jaspar_cne_pwm_98_matrix_row_2_, jaspar_cne_pwm_98_matrix_row_3_};
	PWM jaspar_cne_pwm_98_ = {
/* accession        */ "CN0197",
/* name             */ "LM197",
/* label            */ " CN0197	24.5286416167029	LM197	unknown	; consensus \"AAAGATGCARTATCCCTTTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAAGATGCARTATCCCTTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_98_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -79.716698,
/* max_score        */ 23.349213,
/* threshold        */ 0.861,
/* info content     */ 24.492624,
/* base_counts      */ {469, 317, 222, 532},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 77
};

	float jaspar_cne_pwm_99_matrix_row_0_[14] = { -7.161715, -1.858410, -7.161715,  1.272314,  1.272314, -7.161715, -2.546595,  1.272314, -7.161715,  1.272314, -7.161715, -7.161715, -7.161715,  0.621925};
	float jaspar_cne_pwm_99_matrix_row_1_[14] = {  1.398433, -1.213443, -6.920553, -6.920553, -6.920553, -6.920553, -2.305433, -6.920553,  1.446050, -6.920553, -6.920553, -6.920553,  1.446050, -1.213443};
	float jaspar_cne_pwm_99_matrix_row_2_[14] = { -0.703947, -6.920553, -2.305433, -6.920553, -6.920553, -6.920553, -1.213443, -6.920553, -1.213443, -6.920553,  1.422525,  1.322466, -1.213443, -6.920553};
	float jaspar_cne_pwm_99_matrix_row_3_[14] = { -7.161715,  1.157271,  1.250340, -7.161715, -7.161715,  1.272314,  1.157271, -7.161715, -7.161715, -7.161715, -1.167754, -0.475854, -7.161715,  0.388420};
	float *jaspar_cne_pwm_99_matrix[4] = { jaspar_cne_pwm_99_matrix_row_0_, jaspar_cne_pwm_99_matrix_row_1_, jaspar_cne_pwm_99_matrix_row_2_, jaspar_cne_pwm_99_matrix_row_3_};
	PWM jaspar_cne_pwm_99_ = {
/* accession        */ "CN0120",
/* name             */ "LM120",
/* label            */ " CN0120	23.0445387614172	LM120	unknown	; consensus \"CTTAATTACAGGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTAATTACAGGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_99_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.166565,
/* max_score        */ 17.583900,
/* threshold        */ 0.791,
/* info content     */ 22.951523,
/* base_counts      */ {211, 134, 95, 204},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_100_matrix_row_0_[19] = {  1.069002,  0.160982, -0.113329, -7.663464,  1.085000,  1.272571,  1.232303,  0.376016, -7.663464,  0.655522, -7.663464,  0.679614, -7.663464, -7.663464, -3.048344, -2.360159, -0.977603,  0.770565, -0.167367};
	float jaspar_cne_pwm_100_matrix_row_1_[19] = { -1.428341, -2.807182, -7.422302, -1.715192, -0.251413, -7.422302, -7.422302, -7.422302,  1.417120, -7.422302, -7.422302, -1.205696, -7.422302, -2.807182, -2.807182,  1.208398,  1.226095, -0.869794, -1.023707};
	float jaspar_cne_pwm_100_matrix_row_2_[19] = { -2.118997, -0.418328,  1.095091,  1.473465, -7.422302, -7.422302, -2.807182, -7.422302, -1.205696, -7.422302,  1.487068, -1.715192, -7.422302, -2.118997, -1.428341, -1.715192, -2.807182, -1.205696,  0.127833};
	float jaspar_cne_pwm_100_matrix_row_3_[19] = { -0.977603,  0.605524, -1.110956, -7.663464, -7.663464, -7.663464, -2.360159,  0.748591, -2.360159,  0.497340, -2.360159,  0.200187,  1.272571,  1.232303,  1.190344, -0.349577, -0.754709, -0.167367,  0.438517};
	float *jaspar_cne_pwm_100_matrix[4] = { jaspar_cne_pwm_100_matrix_row_0_, jaspar_cne_pwm_100_matrix_row_1_, jaspar_cne_pwm_100_matrix_row_2_, jaspar_cne_pwm_100_matrix_row_3_};
	PWM jaspar_cne_pwm_100_ = {
/* accession        */ "CN0158",
/* name             */ "LM158",
/* label            */ " CN0158	22.7326934596902	LM158	unknown	; consensus \"AWGGAAAWCWGWTTTCCWW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATGGAAATCAGATTTCCAN",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_100_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.419922,
/* max_score        */ 20.159664,
/* threshold        */ 0.827,
/* info content     */ 22.682978,
/* base_counts      */ {507, 223, 253, 461},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 76
};

	float jaspar_cne_pwm_101_matrix_row_0_[17] = {  1.134105,  1.207665,  1.207665, -1.868559, -2.035474, -2.130693, -2.130693,  1.189776, -0.391051, -3.736143,  1.207665, -2.353587, -1.138070, -2.130693, -2.486940, -1.066637, -0.027436};
	float jaspar_cne_pwm_101_matrix_row_1_[17] = { -3.494981, -2.581679, -4.183165, -8.798285, -2.112425, -0.301091,  1.390419, -3.091175,  0.942742,  1.500650, -1.889531, -2.581679,  1.344258, -4.183165, -1.553344, -0.364257, -1.101619};
	float jaspar_cne_pwm_101_matrix_row_2_[17] = { -0.727067, -1.889531, -1.794312, -1.794312,  1.427322, -0.696305, -1.889531, -3.494981, -0.049822, -8.798285, -8.798285,  0.412155, -2.112425, -8.798285,  1.390419,  0.391954, -0.321706};
	float jaspar_cne_pwm_101_matrix_row_3_[17] = { -2.822841, -3.045486, -2.486940,  1.189776, -2.640853,  0.908105, -1.725561, -1.489312, -2.035474, -3.736143, -2.235942,  0.802218, -1.948538,  1.235638, -1.794506,  0.417831,  0.569736};
	float *jaspar_cne_pwm_101_matrix[4] = { jaspar_cne_pwm_101_matrix_row_0_, jaspar_cne_pwm_101_matrix_row_1_, jaspar_cne_pwm_101_matrix_row_2_, jaspar_cne_pwm_101_matrix_row_3_};
	PWM jaspar_cne_pwm_101_ = {
/* accession        */ "CN0103",
/* name             */ "LM103",
/* label            */ " CN0103	20.4275000237908	LM103	unknown	; consensus \"AAATGTCACCATCTGKW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAATGTCACCATCTGKW",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_101_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -67.698769,
/* max_score        */ 19.065990,
/* threshold        */ 0.792,
/* info content     */ 20.419538,
/* base_counts      */ {1651, 1153, 968, 1345},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 301
};

	float jaspar_cne_pwm_102_matrix_row_0_[20] = { -0.712306, -0.712306,  0.895990,  1.101789, -7.264814, -7.264814, -2.649694, -1.270853, -7.264814,  1.190716, -7.264814, -1.048208, -7.264814, -2.649694,  1.252579, -7.264814, -7.264814, -1.270853, -1.270853, -1.557704};
	float jaspar_cne_pwm_102_matrix_row_1_[20] = {  0.800794,  0.949159,  0.067258, -2.408532, -7.023652, -7.023652, -7.023652, -1.316542,  1.513540, -2.408532, -1.316542, -0.471144, -7.023652,  1.342951, -7.023652, -2.408532, -1.720347, -1.720347, -0.471144,  1.342951};
	float jaspar_cne_pwm_102_matrix_row_2_[20] = { -2.408532, -1.720347, -2.408532, -2.408532, -7.023652, -2.408532, -7.023652,  1.365935, -7.023652, -2.408532, -2.408532, -0.114897, -2.408532, -1.316542, -7.023652, -2.408532, -7.023652, -7.023652,  1.078329, -1.316542};
	float jaspar_cne_pwm_102_matrix_row_3_[20] = {  0.231283, -0.093926, -1.557704, -0.866219,  1.272378,  1.252579,  1.252579, -7.264814, -7.264814, -1.961509,  1.190716,  0.707997,  1.252579, -1.270853, -2.649694,  1.232380,  1.232380,  1.147241, -0.712306, -1.961509};
	float *jaspar_cne_pwm_102_matrix[4] = { jaspar_cne_pwm_102_matrix_row_0_, jaspar_cne_pwm_102_matrix_row_1_, jaspar_cne_pwm_102_matrix_row_2_, jaspar_cne_pwm_102_matrix_row_3_};
	PWM jaspar_cne_pwm_102_ = {
/* accession        */ "CN0076",
/* name             */ "LM76",
/* label            */ " CN0076	26.6735842900843	LM76	unknown	; consensus \"CCAATTTGCATTTCATTTGC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "YCAATTTGCATTTCATTTGC",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_102_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.474213,
/* max_score        */ 23.375557,
/* threshold        */ 0.892,
/* info content     */ 26.595631,
/* base_counts      */ {211, 230, 103, 476},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 51
};

	float jaspar_cne_pwm_103_matrix_row_0_[14] = { -1.941722, -1.537917, -7.245027,  1.144560,  1.210503, -7.245027,  1.272366, -2.629907,  1.231552, -2.629907, -2.629907, -2.629907, -1.251066, -1.941722};
	float jaspar_cne_pwm_103_matrix_row_1_[14] = {  1.339213, -7.003865, -7.003865, -2.388745, -1.700560,  1.493329, -7.003865, -2.388745, -2.388745, -7.003865, -7.003865, -7.003865,  1.265123,  1.451665};
	float jaspar_cne_pwm_103_matrix_row_2_[14] = { -2.388745, -2.388745,  1.493329, -1.009904, -2.388745, -2.388745, -7.003865,  1.451665, -7.003865, -7.003865,  1.493329,  1.493329, -0.787259, -7.003865};
	float jaspar_cne_pwm_103_matrix_row_3_[14] = { -1.028421,  1.189002, -2.629907, -2.629907, -7.245027, -7.245027, -7.245027, -2.629907, -2.629907,  1.252167, -7.245027, -7.245027, -1.941722, -2.629907};
	float *jaspar_cne_pwm_103_matrix[4] = { jaspar_cne_pwm_103_matrix_row_0_, jaspar_cne_pwm_103_matrix_row_1_, jaspar_cne_pwm_103_matrix_row_2_, jaspar_cne_pwm_103_matrix_row_3_};
	PWM jaspar_cne_pwm_103_ = {
/* accession        */ "CN0070",
/* name             */ "LM70",
/* label            */ " CN0070	22.7450728903571	LM70	unknown	; consensus \"CTGAACAGATGGCC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGAACAGATGGCC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_103_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.075897,
/* max_score        */ 18.781136,
/* threshold        */ 0.790,
/* info content     */ 22.677019,
/* base_counts      */ {204, 182, 207, 107},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_104_matrix_row_0_[17] = { -1.407330, -8.316085,  1.272760,  1.272760,  1.272760,  0.695926, -8.316085, -3.700964,  0.853537,  0.943141, -0.073066, -8.316085, -8.316085, -8.316085, -8.316085, -0.309384, -1.763577};
	float jaspar_cne_pwm_104_matrix_row_1_[17] = { -8.074923, -8.074923, -8.074923, -8.074923, -8.074923, -8.074923, -8.074923,  0.537762,  0.141435,  0.244063, -8.074923, -8.074923, -8.074923, -8.074923,  1.513922,  1.018996, -3.459802};
	float jaspar_cne_pwm_104_matrix_row_2_[17] = {  1.301017,  1.513922, -8.074923, -8.074923, -8.074923, -8.074923,  0.764499,  0.085881, -1.166168, -8.074923, -8.074923, -8.074923, -8.074923, -0.761036, -8.074923, -8.074923, -8.074923};
	float jaspar_cne_pwm_104_matrix_row_3_[17] = { -0.819988, -8.316085, -8.316085, -8.316085, -8.316085,  0.448124,  0.633021,  0.296600, -2.608975, -8.316085,  0.971309,  1.272760,  1.272760,  1.164359, -8.316085, -0.414708,  1.216411};
	float *jaspar_cne_pwm_104_matrix[4] = { jaspar_cne_pwm_104_matrix_row_0_, jaspar_cne_pwm_104_matrix_row_1_, jaspar_cne_pwm_104_matrix_row_2_, jaspar_cne_pwm_104_matrix_row_3_};
	PWM jaspar_cne_pwm_104_ = {
/* accession        */ "CN0036",
/* name             */ "LM36",
/* label            */ " CN0036	24.3913279602104	LM36	unknown	; consensus \"KGAAAWKNNAWTTYCHW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GGAAAATYAATTTTCCT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_104_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -129.845413,
/* max_score        */ 18.858603,
/* threshold        */ 0.827,
/* info content     */ 24.354250,
/* base_counts      */ {807, 369, 393, 913},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 146
};

	float jaspar_cne_pwm_105_matrix_row_0_[18] = { -0.672333,  1.251739,  1.069459,  1.209188,  1.164746, -2.609720,  1.141762, -0.672333, -2.609720, -7.224841, -7.224841, -2.609720, -7.224841, -1.517731, -7.224841, -7.224841, -7.224841, -1.921536};
	float jaspar_cne_pwm_105_matrix_row_1_[18] = { -1.680374, -6.983679, -1.276569, -2.368558, -2.368558,  1.310621, -0.989717, -1.276569, -1.276569, -6.983679, -6.983679, -6.983679, -1.680374, -1.680374,  1.310621, -0.989717,  1.359399, -0.989717};
	float jaspar_cne_pwm_105_matrix_row_2_[18] = {  1.285309, -6.983679, -0.585084, -2.368558, -0.989717, -0.585084, -6.983679,  1.177125, -1.680374, -2.368558, -2.368558,  1.471852, -6.983679, -6.983679, -6.983679, -6.983679, -0.767073, -6.983679};
	float jaspar_cne_pwm_105_matrix_row_3_[18] = { -2.609720, -2.609720, -7.224841, -2.609720, -7.224841, -1.921536, -1.921536, -1.230879,  1.141762,  1.251739,  1.251739, -2.609720,  1.230690,  1.164746, -0.421336,  1.187214, -1.921536,  1.141762};
	float *jaspar_cne_pwm_105_matrix[4] = { jaspar_cne_pwm_105_matrix_row_0_, jaspar_cne_pwm_105_matrix_row_1_, jaspar_cne_pwm_105_matrix_row_2_, jaspar_cne_pwm_105_matrix_row_3_};
	PWM jaspar_cne_pwm_105_ = {
/* accession        */ "CN0206",
/* name             */ "LM206",
/* label            */ " CN0206	25.6341341723973	LM206	unknown	; consensus \"GAAAACAGTTTGTTCTCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GAAAACAGTTTGTTCTCT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_105_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.432571,
/* max_score        */ 22.121471,
/* threshold        */ 0.851,
/* info content     */ 25.557079,
/* base_counts      */ {243, 151, 147, 341},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_106_matrix_row_0_[18] = { -0.439760, -0.352824, -1.449772,  1.222052,  1.272474, -7.443734, -7.443734,  1.073659, -1.449772,  1.150605, -7.443734, -7.443734, -7.443734, -1.449772, -7.443734, -2.828613, -2.140429, -0.640229};
	float jaspar_cne_pwm_106_matrix_row_1_[18] = { -7.202572, -1.495462, -0.650064, -7.202572, -7.202572, -7.202572, -2.587451, -7.202572,  1.410113, -2.587451, -2.587451, -1.899267,  1.445825, -7.202572, -2.587451,  1.391768,  0.986395, -1.495462};
	float jaspar_cne_pwm_106_matrix_row_2_[18] = {  1.231457,  1.231457, -7.202572, -1.495462, -7.202572, -7.202572, -7.202572, -0.399067, -7.202572, -7.202572, -2.587451, -7.202572, -7.202572, -2.587451,  1.463214, -7.202572, -2.587451, -1.899267};
	float jaspar_cne_pwm_106_matrix_row_3_[18] = { -1.449772, -7.443734,  1.073659, -7.443734, -7.443734,  1.272474,  1.255947, -2.140429, -2.140429, -1.045139,  1.239143,  1.239143, -1.449772,  1.186966, -2.140429, -1.045139,  0.252933,  1.011797};
	float *jaspar_cne_pwm_106_matrix[4] = { jaspar_cne_pwm_106_matrix_row_0_, jaspar_cne_pwm_106_matrix_row_1_, jaspar_cne_pwm_106_matrix_row_2_, jaspar_cne_pwm_106_matrix_row_3_};
	PWM jaspar_cne_pwm_106_ = {
/* accession        */ "CN0081",
/* name             */ "LM81",
/* label            */ " CN0081	26.3244614080236	LM81	unknown	; consensus \"GGTAATTACATTCTGCYT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GGTAATTACATTCTGCCT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_106_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -121.898308,
/* max_score        */ 22.158148,
/* threshold        */ 0.858,
/* info content     */ 26.253605,
/* base_counts      */ {270, 221, 167, 440},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 61
};

	float jaspar_cne_pwm_107_matrix_row_0_[14] = { -7.183203, -2.568082, -7.183203, -2.568082, -7.183203, -7.183203, -2.568082, -1.476093, -7.183203,  1.111097, -1.189241, -7.183203, -1.879898, -0.784608};
	float jaspar_cne_pwm_107_matrix_row_1_[14] = {  1.424562, -6.942041, -6.942041,  1.491988,  1.491988,  1.470014, -1.234931, -6.942041, -6.942041, -6.942041, -6.942041, -0.948079, -6.942041, -6.942041};
	float jaspar_cne_pwm_107_matrix_row_2_[14] = { -6.942041, -6.942041,  1.513490, -6.942041, -2.326920, -6.942041, -2.326920, -1.638736,  1.513490, -6.942041, -2.326920, -6.942041, -1.638736,  1.352259};
	float jaspar_cne_pwm_107_matrix_row_3_[14] = { -1.189241,  1.250826, -7.183203, -7.183203, -7.183203, -1.879898,  1.159875,  1.159875, -7.183203, -0.630695,  1.159875,  1.183400,  1.183400, -2.568082};
	float *jaspar_cne_pwm_107_matrix[4] = { jaspar_cne_pwm_107_matrix_row_0_, jaspar_cne_pwm_107_matrix_row_1_, jaspar_cne_pwm_107_matrix_row_2_, jaspar_cne_pwm_107_matrix_row_3_};
	PWM jaspar_cne_pwm_107_ = {
/* accession        */ "CN0184",
/* name             */ "LM184",
/* label            */ " CN0184	22.8604735660693	LM184	unknown	; consensus \"CTGCCCTTGATTTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGCCCTTGATTTG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_107_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.502739,
/* max_score        */ 18.466137,
/* threshold        */ 0.790,
/* info content     */ 22.777626,
/* base_counts      */ {58, 187, 141, 272},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_108_matrix_row_0_[16] = { -0.316086, -0.826246, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841,  1.044147};
	float jaspar_cne_pwm_108_matrix_row_1_[16] = { -1.680374,  0.954053, -6.983679, -6.983679, -1.680374, -6.983679, -6.983679, -6.983679,  0.512419,  1.513516, -6.983679, -6.983679, -6.983679, -6.983679,  1.513516, -2.368558};
	float jaspar_cne_pwm_108_matrix_row_2_[16] = {  0.020295, -0.585084, -6.983679,  1.513516, -6.983679, -6.983679, -6.983679, -1.276569, -0.074924, -6.983679, -6.983679, -6.983679,  1.513516,  1.513516, -6.983679, -6.983679};
	float jaspar_cne_pwm_108_matrix_row_3_[16] = {  0.638810, -0.421336,  1.272354, -7.224841,  1.230690,  1.272354,  1.272354,  1.209188,  0.425328, -7.224841,  1.272354,  1.272354, -7.224841, -7.224841, -7.224841, -0.421336};
	float *jaspar_cne_pwm_108_matrix[4] = { jaspar_cne_pwm_108_matrix_row_0_, jaspar_cne_pwm_108_matrix_row_1_, jaspar_cne_pwm_108_matrix_row_2_, jaspar_cne_pwm_108_matrix_row_3_};
	PWM jaspar_cne_pwm_108_ = {
/* accession        */ "CN0113",
/* name             */ "LM113",
/* label            */ " CN0113	25.7958286123747	LM113	unknown	; consensus \"WYTGTTTTKCTTGGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCTGTTTTYCTTGGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_108_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.413223,
/* max_score        */ 19.518654,
/* threshold        */ 0.826,
/* info content     */ 25.691856,
/* base_counts      */ {55, 149, 177, 403},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_109_matrix_row_0_[15] = {  0.617160, -7.284217, -1.067611, -2.669097,  1.171313,  1.272389, -2.669097, -1.290256,  1.212977, -2.669097, -7.284217,  1.233176, -2.669097, -0.731709, -2.669097};
	float jaspar_cne_pwm_109_matrix_row_1_[15] = { -7.043055, -7.043055,  1.300023, -2.427935, -1.739750, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055,  1.494137, -7.043055,  1.412475, -2.427935, -1.049094};
	float jaspar_cne_pwm_109_matrix_row_2_[15] = { -1.049094,  1.513551, -1.739750, -7.043055, -2.427935, -7.043055, -7.043055, -7.043055, -2.427935,  1.474338, -7.043055, -7.043055, -2.427935, -2.427935, -2.427935};
	float jaspar_cne_pwm_109_matrix_row_3_[15] = {  0.365952, -7.284217, -1.577107,  1.233176, -1.980912, -7.284217,  1.252975,  1.192362, -1.980912, -2.669097, -2.669097, -1.980912, -1.577107,  1.082386,  1.149812};
	float *jaspar_cne_pwm_109_matrix[4] = { jaspar_cne_pwm_109_matrix_row_0_, jaspar_cne_pwm_109_matrix_row_1_, jaspar_cne_pwm_109_matrix_row_2_, jaspar_cne_pwm_109_matrix_row_3_};
	PWM jaspar_cne_pwm_109_ = {
/* accession        */ "CN0055",
/* name             */ "LM55",
/* label            */ " CN0055	23.1977114018334	LM55	unknown	; consensus \"WGCTAATTAGCACTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCTAATTAGCACTT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_109_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.087852,
/* max_score        */ 18.612251,
/* threshold        */ 0.800,
/* info content     */ 23.129784,
/* base_counts      */ {246, 148, 113, 273},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_110_matrix_row_0_[14] = {  1.132584, -7.161715, -7.161715, -7.161715, -7.161715, -1.167754, -7.161715,  1.272314,  1.181363,  1.250340,  1.181363,  1.181363, -7.161715, -1.454605};
	float jaspar_cne_pwm_110_matrix_row_1_[14] = { -1.617248, -6.920553,  1.422525, -6.920553, -6.920553,  1.422525,  1.513476, -6.920553, -2.305433, -6.920553, -0.926592, -1.617248, -6.920553,  1.240251};
	float jaspar_cne_pwm_110_matrix_row_2_[14] = { -1.617248,  1.513476, -0.926592, -6.920553,  1.513476, -6.920553, -6.920553, -6.920553, -2.305433, -6.920553, -6.920553, -2.305433, -1.617248, -0.703947};
	float jaspar_cne_pwm_110_matrix_row_3_[14] = { -1.858410, -7.161715, -7.161715,  1.272314, -7.161715, -7.161715, -7.161715, -7.161715, -1.858410, -2.546595, -7.161715, -2.546595,  1.227872, -1.454605};
	float *jaspar_cne_pwm_110_matrix[4] = { jaspar_cne_pwm_110_matrix_row_0_, jaspar_cne_pwm_110_matrix_row_1_, jaspar_cne_pwm_110_matrix_row_2_, jaspar_cne_pwm_110_matrix_row_3_};
	PWM jaspar_cne_pwm_110_ = {
/* accession        */ "CN0174",
/* name             */ "LM174",
/* label            */ " CN0174	23.2717790086553	LM174	unknown	; consensus \"AGCTGCCAAAAATC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCTGCCAAAAATC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_110_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -79.541023,
/* max_score        */ 18.325243,
/* threshold        */ 0.791,
/* info content     */ 23.185102,
/* base_counts      */ {264, 174, 107, 99},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_111_matrix_row_0_[23] = { -1.680486, -1.910933, -2.316213, -2.071211, -1.146525,  1.145507, -0.048027,  0.642213, -2.162142, -2.262175, -1.710330, -1.217971, -1.595952,  0.730422, -1.294917,  1.133237, -0.642688, -0.601870,  1.201405,  1.229026,  1.217745, -0.462121, -0.077182};
	float jaspar_cne_pwm_111_matrix_row_1_[23] = {  1.203654, -0.511515, -0.559137, -2.400260, -1.531668, -2.567174, -2.021013,  0.270517, -1.920980,  1.100153,  1.069527, -0.391164,  0.613789, -3.354542, -1.787508, -1.707497, -2.567174, -2.480238, -2.480238, -2.767643, -4.267843, -3.354542, -0.776172};
	float jaspar_cne_pwm_111_matrix_row_2_[23] = { -1.969746, -3.018641, -2.885287, -0.995498,  1.340315, -1.181561,  1.074300, -0.806939, -1.439324, -1.276849, -2.567174, -1.276849, -0.838683,  0.459016,  1.384925, -1.252162, -0.761136,  1.284016, -1.707497, -2.132177, -2.021013,  1.272366,  0.968943};
	float jaspar_cne_pwm_111_matrix_row_3_[23] = { -0.419565,  1.069522,  1.091827,  1.126157, -2.498424, -2.071211, -1.543322, -1.236660,  1.148985, -0.119482,  0.039936,  0.926280,  0.455855, -1.493324, -3.595704, -2.316213,  0.962491, -2.115643, -3.259803, -4.509006, -2.567369, -2.210908, -1.543322};
	float *jaspar_cne_pwm_111_matrix[4] = { jaspar_cne_pwm_111_matrix_row_0_, jaspar_cne_pwm_111_matrix_row_1_, jaspar_cne_pwm_111_matrix_row_2_, jaspar_cne_pwm_111_matrix_row_3_};
	PWM jaspar_cne_pwm_111_ = {
/* accession        */ "CN0016",
/* name             */ "LM16",
/* label            */ " CN0016	23.3132593748501	LM16	unknown	; consensus \"CTTTGARATCCTYAGATGAAAGR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTGAGATCCTYAGATGAAAGG",
/* library_name     */ "jaspar_cne",
/* length           */ 23,
/* matrixname       */ jaspar_cne_pwm_111_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -59.709885,
/* max_score        */ 24.936808,
/* threshold        */ 0.870,
/* info content     */ 23.309822,
/* base_counts      */ {4743, 2312, 3729, 4212},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 652
};

	float jaspar_cne_pwm_112_matrix_row_0_[17] = { -2.892087,  1.209001, -1.800097, -2.892087, -0.821346, -2.892087, -1.513245,  1.123493, -1.290601, -2.892087,  1.225259,  1.029985,  1.158579,  1.123493,  1.158579, -2.892087, -1.108612};
	float jaspar_cne_pwm_112_matrix_row_1_[17] = {  1.399741, -7.266045, -1.962740,  1.450163, -7.266045, -7.266045, -1.049439, -0.867450, -7.266045, -1.962740, -2.650924, -1.272083, -2.650924, -1.962740, -1.962740, -2.650924, -1.558935};
	float jaspar_cne_pwm_112_matrix_row_2_[17] = { -7.266045, -7.266045,  0.335357, -1.962740, -7.266045,  1.450163,  1.346640, -1.962740, -1.272083,  1.466421, -2.650924, -1.962740, -2.650924, -1.962740, -1.049439, -2.650924,  1.364655};
	float jaspar_cne_pwm_112_matrix_row_3_[17] = { -1.108612, -1.513245,  0.787093, -2.892087,  1.141190, -1.800097, -2.892087, -2.892087,  1.123493, -7.507207, -2.892087, -0.821346, -1.290601, -1.290601, -7.507207,  1.225259, -7.507207};
	float *jaspar_cne_pwm_112_matrix[4] = { jaspar_cne_pwm_112_matrix_row_0_, jaspar_cne_pwm_112_matrix_row_1_, jaspar_cne_pwm_112_matrix_row_2_, jaspar_cne_pwm_112_matrix_row_3_};
	PWM jaspar_cne_pwm_112_ = {
/* accession        */ "CN0154",
/* name             */ "LM154",
/* label            */ " CN0154	23.4173275673741	LM154	unknown	; consensus \"CAKCTGGATGAAAAATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATCTGGATGAAAAATG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_112_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -81.851433,
/* max_score        */ 20.783207,
/* threshold        */ 0.818,
/* info content     */ 23.371445,
/* base_counts      */ {433, 148, 274, 250},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 65
};

	float jaspar_cne_pwm_113_matrix_row_0_[18] = {  1.108360, -2.977142, -2.977142, -1.370365, -1.370365, -2.690291,  1.214273, -1.593342, -0.677551, -3.380947, -0.900612,  1.219285, -1.680278, -2.977142,  0.064211, -0.987585, -2.467646,  1.113930};
	float jaspar_cne_pwm_113_matrix_row_1_[18] = { -1.272202, -3.827970, -2.044495, -3.827970, -1.757229,  1.455435, -3.139785, -2.449129,  0.466280, -2.044495,  1.082134, -2.449129, -3.139785, -1.890582, -3.139785, -1.352180,  1.450398, -1.890582};
	float jaspar_cne_pwm_113_matrix_row_2_[18] = { -1.890582, -2.735980, -2.735980, -3.827970,  1.377070, -8.443090, -3.827970, -2.449129,  0.256591, -1.890582, -0.076487, -2.735980, -3.139785, -8.443090, -2.449129,  1.315429, -3.827970, -3.827970};
	float jaspar_cne_pwm_113_matrix_row_3_[18] = { -1.593342,  1.239087,  1.214273,  1.188828, -2.690291, -1.998391, -1.880747,  1.173244, -0.228722,  1.199084, -2.131744, -2.690291,  1.199084,  1.224273,  0.876815, -2.690291, -2.131744, -0.943153};
	float *jaspar_cne_pwm_113_matrix[4] = { jaspar_cne_pwm_113_matrix_row_0_, jaspar_cne_pwm_113_matrix_row_1_, jaspar_cne_pwm_113_matrix_row_2_, jaspar_cne_pwm_113_matrix_row_3_};
	PWM jaspar_cne_pwm_113_ = {
/* accession        */ "CN0027",
/* name             */ "LM27",
/* label            */ " CN0027	23.8959198763857	LM27	unknown	; consensus \"ATTTGCATSTCATTWGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTTGCATNTCATTTGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_113_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -63.929260,
/* max_score        */ 21.117279,
/* threshold        */ 0.832,
/* info content     */ 23.883686,
/* base_counts      */ {970, 683, 497, 1648},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 211
};

	float jaspar_cne_pwm_114_matrix_row_0_[14] = { -0.896017, -1.894320, -9.832051,  1.231472, -3.615446,  1.239279, -3.838090, -3.615446, -4.124941, -1.314659, -0.621611,  1.222024,  1.168397,  1.023113};
	float jaspar_cne_pwm_114_matrix_row_1_[14] = {  1.156340, -9.590890, -3.374284, -3.883779,  1.498931, -2.094792, -3.596928, -3.596928, -3.374284,  0.465362,  1.242811, -1.766444, -2.682135, -2.151918};
	float jaspar_cne_pwm_114_matrix_row_2_[14] = { -1.135359, -4.975769,  1.501980, -3.038382, -3.883779, -4.287585, -4.287585, -3.596928, -3.596928, -0.206512, -1.296590, -4.975769, -1.430086, -0.223460};
	float jaspar_cne_pwm_114_matrix_row_3_[14] = { -0.882946,  1.228333, -4.124941, -2.393080, -4.528747, -4.528747,  1.257769,  1.253178,  1.254711,  0.345311, -2.335954, -3.279544, -2.181883, -2.661163};
	float *jaspar_cne_pwm_114_matrix[4] = { jaspar_cne_pwm_114_matrix_row_0_, jaspar_cne_pwm_114_matrix_row_1_, jaspar_cne_pwm_114_matrix_row_2_, jaspar_cne_pwm_114_matrix_row_3_};
	PWM jaspar_cne_pwm_114_ = {
/* accession        */ "CN0032",
/* name             */ "LM32",
/* label            */ " CN0032	20.3585125615029	LM32	unknown	; consensus \"CTGACATTTYCAAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGACATTTNCAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_114_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -59.497227,
/* max_score        */ 16.743700,
/* threshold        */ 0.782,
/* info content     */ 20.354790,
/* base_counts      */ {3301, 1951, 1039, 3019},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 665
};

	float jaspar_cne_pwm_115_matrix_row_0_[16] = { -0.826246, -2.609720,  1.251739,  1.230690, -7.224841, -2.609720,  1.187214, -2.609720, -1.230879, -7.224841, -1.921536,  1.251739, -7.224841, -1.921536,  1.118237,  0.558800};
	float jaspar_cne_pwm_115_matrix_row_1_[16] = {  0.261263, -1.680374, -2.368558, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.310621, -6.983679,  1.450350, -6.983679, -6.983679, -2.368558, -1.680374, -0.585084};
	float jaspar_cne_pwm_115_matrix_row_2_[16] = { -0.767073, -1.680374, -6.983679, -1.680374, -6.983679,  1.405908, -1.680374,  1.471852, -1.276569, -6.983679, -6.983679, -2.368558, -6.983679, -1.680374, -6.983679,  0.187210};
	float jaspar_cne_pwm_115_matrix_row_3_[16] = {  0.558800,  1.164746, -7.224841, -7.224841,  1.272354, -1.230879, -1.921536, -2.609720, -1.921536,  1.272354, -2.609720, -7.224841,  1.272354,  1.164746, -1.008235, -0.826246};
	float *jaspar_cne_pwm_115_matrix[4] = { jaspar_cne_pwm_115_matrix_row_0_, jaspar_cne_pwm_115_matrix_row_1_, jaspar_cne_pwm_115_matrix_row_2_, jaspar_cne_pwm_115_matrix_row_3_};
	PWM jaspar_cne_pwm_115_ = {
/* accession        */ "CN0048",
/* name             */ "LM48",
/* label            */ " CN0048	23.3249924223388	LM48	unknown	; consensus \"YTAATGAGCTCATTAR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "YTAATGAGCTCATTAR",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_115_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.819748,
/* max_score        */ 18.942499,
/* threshold        */ 0.809,
/* info content     */ 23.250683,
/* base_counts      */ {271, 112, 121, 280},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_116_matrix_row_0_[18] = { -0.220867, -7.224841, -1.517731,  1.141762,  1.187214, -7.224841, -7.224841, -7.224841, -2.609720,  0.020101, -1.921536, -2.609720, -7.224841, -7.224841,  1.251739,  1.141762, -1.517731, -1.517731};
	float jaspar_cne_pwm_116_matrix_row_1_[18] = {  0.617724, -2.368558, -6.983679, -0.585084, -0.989717,  1.450350, -1.276569,  0.261263, -0.767073,  0.107231, -1.680374, -0.989717, -6.983679, -6.983679, -2.368558, -0.989717,  1.405908, -0.585084};
	float jaspar_cne_pwm_116_matrix_row_2_[18] = { -0.585084, -6.983679,  1.450350, -6.983679, -6.983679, -2.368558, -6.983679, -2.368558, -2.368558,  0.666490, -1.276569, -0.989717, -6.983679,  1.513516, -6.983679, -6.983679, -1.680374, -2.368558};
	float jaspar_cne_pwm_116_matrix_row_3_[18] = { -0.133931,  1.251739, -7.224841, -7.224841, -7.224841, -1.921536,  1.209188,  0.906984,  1.118237, -1.921536,  1.118237,  1.069459,  1.272354, -7.224841, -7.224841, -1.921536, -7.224841,  1.044147};
	float *jaspar_cne_pwm_116_matrix[4] = { jaspar_cne_pwm_116_matrix_row_0_, jaspar_cne_pwm_116_matrix_row_1_, jaspar_cne_pwm_116_matrix_row_2_, jaspar_cne_pwm_116_matrix_row_3_};
	PWM jaspar_cne_pwm_116_ = {
/* accession        */ "CN0198",
/* name             */ "LM198",
/* label            */ " CN0198	24.3818001742724	LM198	unknown	; consensus \"MTGAACTTTGTTTGAACT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NTGAACTTTNTTTGAACT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_116_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.473076,
/* max_score        */ 20.817160,
/* threshold        */ 0.838,
/* info content     */ 24.302393,
/* base_counts      */ {217, 172, 135, 358},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_117_matrix_row_0_[14] = { -1.189241, -7.183203, -1.476093,  1.228852,  1.272328, -1.879898, -7.183203, -7.183203, -7.183203,  1.206384, -7.183203, -7.183203, -2.568082, -1.476093};
	float jaspar_cne_pwm_117_matrix_row_1_[14] = { -1.638736,  1.491988, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -0.948079, -2.326920, -6.942041, -6.942041, -2.326920, -6.942041};
	float jaspar_cne_pwm_117_matrix_row_2_[14] = {  1.376945, -6.942041, -6.942041, -1.638736, -6.942041, -6.942041, -6.942041,  1.376945,  1.376945, -6.942041, -6.942041, -2.326920, -1.638736,  1.401037};
	float jaspar_cne_pwm_117_matrix_row_3_[14] = { -7.183203, -2.568082,  1.206384, -7.183203, -7.183203,  1.228852,  1.272328, -0.784608, -1.879898, -1.879898,  1.272328,  1.250826,  1.183400, -1.879898};
	float *jaspar_cne_pwm_117_matrix[4] = { jaspar_cne_pwm_117_matrix_row_0_, jaspar_cne_pwm_117_matrix_row_1_, jaspar_cne_pwm_117_matrix_row_2_, jaspar_cne_pwm_117_matrix_row_3_};
	PWM jaspar_cne_pwm_117_ = {
/* accession        */ "CN0045",
/* name             */ "LM45",
/* label            */ " CN0045	23.4244082454306	LM45	unknown	; consensus \"GCTAATTGGATTTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCTAATTGGATTTG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_117_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.985077,
/* max_score        */ 18.145542,
/* threshold        */ 0.792,
/* info content     */ 23.339031,
/* base_counts      */ {149, 54, 170, 285},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_118_matrix_row_0_[17] = { -1.176731, -1.686227, -0.994742, -2.778216, -7.393337, -7.393337, -2.778216,  1.182314,  1.163269, -7.393337,  1.124056, -2.778216, -2.778216,  1.182314,  1.255060, -7.393337,  1.219348};
	float jaspar_cne_pwm_118_matrix_row_1_[17] = { -0.599667, -0.243420,  0.092767, -7.152175, -7.152175,  1.513611,  1.116813, -1.445065, -7.152175, -1.158213, -7.152175, -7.152175,  1.385017, -1.158213, -7.152175,  1.496222, -1.848870};
	float jaspar_cne_pwm_118_matrix_row_2_[17] = {  0.161712,  0.919044, -2.537054, -7.152175,  1.513611, -7.152175, -7.152175, -7.152175, -1.445065,  1.324405, -0.599667,  1.460510, -1.158213, -7.152175, -2.537054, -7.152175, -2.537054};
	float jaspar_cne_pwm_118_matrix_row_3_[17] = {  0.646143, -0.222448,  0.823021,  1.255060, -7.393337, -7.393337,  0.102760, -2.090032, -1.686227, -0.994742, -2.778216, -2.090032, -2.090032, -2.778216, -7.393337, -2.778216, -7.393337};
	float *jaspar_cne_pwm_118_matrix[4] = { jaspar_cne_pwm_118_matrix_row_0_, jaspar_cne_pwm_118_matrix_row_1_, jaspar_cne_pwm_118_matrix_row_2_, jaspar_cne_pwm_118_matrix_row_3_};
	PWM jaspar_cne_pwm_118_ = {
/* accession        */ "CN0136",
/* name             */ "LM136",
/* label            */ " CN0136	23.5479252048254	LM136	unknown	; consensus \"NKYTGCCAAGAGCAACA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGTTGCCAAGAGCAACA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_118_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.603462,
/* max_score        */ 20.579817,
/* threshold        */ 0.820,
/* info content     */ 23.483393,
/* base_counts      */ {338, 249, 225, 174},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 58
};

	float jaspar_cne_pwm_119_matrix_row_0_[21] = { -1.584046, -0.129853,  1.150841,  1.204903, -3.521433, -1.919947,  1.073887, -0.640456, -2.833248, -1.737958,  1.132150, -1.333048,  1.083836, -1.737958,  1.160056,  1.264490, -3.521433, -2.833248, -1.919947, -0.697582, -0.697582};
	float jaspar_cne_pwm_119_matrix_row_1_[21] = { -0.724503, -0.154292, -2.188281, -3.280271, -1.678785, -2.188281, -2.592086, -1.496796,  1.128740,  1.284593, -7.895391, -2.188281, -2.188281, -1.678785, -3.280271, -7.895391, -1.496796, -2.592086, -7.895391,  1.187229,  0.661215};
	float jaspar_cne_pwm_119_matrix_row_2_[21] = { -0.070945,  0.868818, -1.496796, -1.901430, -3.280271, -1.678785, -1.342883, -1.209530, -1.901430, -7.895391, -0.650450,  1.401219, -0.724503, -2.188281, -0.804481, -7.895391, -7.895391, -2.592086,  1.428367, -1.496796, -1.678785};
	float jaspar_cne_pwm_119_matrix_row_3_[21] = {  0.812552, -1.919947, -1.919947, -2.429443,  1.213636,  1.160056, -0.965665,  0.968538, -0.034573, -0.586418, -2.833248, -3.521433, -1.919947,  1.150841, -8.136554, -3.521433,  1.213636,  1.222293, -1.919947, -1.132579,  0.340026};
	float *jaspar_cne_pwm_119_matrix[4] = { jaspar_cne_pwm_119_matrix_row_0_, jaspar_cne_pwm_119_matrix_row_1_, jaspar_cne_pwm_119_matrix_row_2_, jaspar_cne_pwm_119_matrix_row_3_};
	PWM jaspar_cne_pwm_119_ = {
/* accession        */ "CN0108",
/* name             */ "LM108",
/* label            */ " CN0108	25.0499589793009	LM108	unknown	; consensus \"YSAATTATCCAGATAATTGCY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGAATTATCCAGATAATTGCY",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_119_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -83.124733,
/* max_score        */ 23.771898,
/* threshold        */ 0.881,
/* info content     */ 25.025480,
/* base_counts      */ {885, 396, 400, 881},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 122
};

	float jaspar_cne_pwm_120_matrix_row_0_[21] = {  0.603666, -1.522805, -1.522805, -7.739411, -1.522805, -2.032301,  1.114397, -3.124291,  1.222596, -2.032301, -2.436106,  1.235333,  1.222596,  1.247910, -0.648502,  0.001688, -1.340816, -0.735437, -2.032301, -1.053550,  0.854928};
	float jaspar_cne_pwm_120_matrix_row_1_[21] = {  0.051886, -0.945741,  0.603731,  1.114436, -0.694744,  1.311763, -7.498250, -2.883129, -2.194944, -2.883129, -2.883129, -2.883129, -7.498250, -7.498250, -7.498250, -1.504288, -0.494275, -7.498250,  1.281462,  0.151919, -1.791139};
	float jaspar_cne_pwm_120_matrix_row_2_[21] = { -0.589495, -1.281643, -1.099654, -1.504288, -7.498250, -1.504288, -1.099654,  1.489072, -2.194944, -1.504288,  1.476495, -2.194944, -1.504288, -7.498250,  1.296727,  1.114436, -2.883129,  1.311763, -1.791139, -7.498250, -0.694744};
	float jaspar_cne_pwm_120_matrix_row_3_[21] = { -0.735437,  1.040300,  0.503608,  0.001688,  1.085413, -1.053550, -1.340816, -7.739411, -7.739411,  1.169959, -7.739411, -7.739411, -7.739411, -2.436106, -1.745450, -7.739411,  1.024798, -1.745450, -0.735437,  0.836239, -0.361028};
	float *jaspar_cne_pwm_120_matrix[4] = { jaspar_cne_pwm_120_matrix_row_0_, jaspar_cne_pwm_120_matrix_row_1_, jaspar_cne_pwm_120_matrix_row_2_, jaspar_cne_pwm_120_matrix_row_3_};
	PWM jaspar_cne_pwm_120_ = {
/* accession        */ "CN0145",
/* name             */ "LM145",
/* label            */ " CN0145	24.4465940941895	LM145	unknown	; consensus \"MTWCTCAGATGAAAGRTGCTW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATYCTCAGATGAAAGGTGCTA",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_120_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -114.568428,
/* max_score        */ 23.658018,
/* threshold        */ 0.871,
/* info content     */ 24.403967,
/* base_counts      */ {568, 300, 407, 447},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 82
};

	float jaspar_cne_pwm_121_matrix_row_0_[18] = { -1.558339, -0.643546, -0.866439,  1.227411,  1.196163, -7.552300, -1.558339, -1.153705,  1.242676,  1.004306,  1.257712,  1.023350, -7.552300,  1.147381, -2.937180,  1.113485, -1.335694, -1.845190};
	float jaspar_cne_pwm_121_matrix_row_1_[18] = { -7.311138, -7.311138, -0.912543, -2.007833, -2.007833, -2.007833, -7.311138,  1.354647, -7.311138, -2.696018, -7.311138, -2.007833,  1.405070, -0.912543,  1.319562, -2.696018, -1.317177, -7.311138};
	float jaspar_cne_pwm_121_matrix_row_2_[18] = { -1.094532,  1.245468,  1.206255, -7.311138, -2.007833, -2.007833, -0.758630, -1.604028, -7.311138,  0.002749, -7.311138, -1.317177, -2.007833, -2.696018, -2.696018, -7.311138, -1.317177, -2.696018};
	float jaspar_cne_pwm_121_matrix_row_3_[18] = {  1.130577, -1.153705, -1.558339, -2.937180, -2.937180,  1.211909,  1.096097, -2.937180, -2.248995, -7.552300, -2.937180, -0.748795, -1.335694, -2.937180, -0.643546, -0.748795,  1.060385,  1.211909};
	float *jaspar_cne_pwm_121_matrix[4] = { jaspar_cne_pwm_121_matrix_row_0_, jaspar_cne_pwm_121_matrix_row_1_, jaspar_cne_pwm_121_matrix_row_2_, jaspar_cne_pwm_121_matrix_row_3_};
	PWM jaspar_cne_pwm_121_ = {
/* accession        */ "CN0230",
/* name             */ "LM230",
/* label            */ " CN0230	23.993025417076	LM230	unknown	; consensus \"TGGAATTCAAAACACATT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGGAATTCAAAACACATT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_121_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.796600,
/* max_score        */ 21.454365,
/* threshold        */ 0.833,
/* info content     */ 23.945055,
/* base_counts      */ {525, 201, 149, 349},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 68
};

	float jaspar_cne_pwm_122_matrix_row_0_[12] = { -8.144714, -8.144714,  1.272722, -8.144714,  1.272722, -8.144714, -8.144714, -8.144714, -8.144714, -8.144714, -8.144714, -8.144714};
	float jaspar_cne_pwm_122_matrix_row_1_[12] = { -7.903552, -7.903552, -7.903552,  1.513884, -7.903552, -7.903552,  1.513884, -7.903552, -7.903552, -7.903552, -7.903552, -7.903552};
	float jaspar_cne_pwm_122_matrix_row_2_[12] = { -7.903552,  1.513884, -7.903552, -7.903552, -7.903552,  1.513884, -7.903552, -7.903552,  1.513884, -7.903552, -7.903552, -7.903552};
	float jaspar_cne_pwm_122_matrix_row_3_[12] = {  1.272722, -8.144714, -8.144714, -8.144714, -8.144714, -8.144714, -8.144714,  1.272722, -8.144714,  1.272722,  1.272722,  1.272722};
	float *jaspar_cne_pwm_122_matrix[4] = { jaspar_cne_pwm_122_matrix_row_0_, jaspar_cne_pwm_122_matrix_row_1_, jaspar_cne_pwm_122_matrix_row_2_, jaspar_cne_pwm_122_matrix_row_3_};
	PWM jaspar_cne_pwm_122_ = {
/* accession        */ "CN0064",
/* name             */ "LM64",
/* label            */ " CN0064	24	LM64	unknown	; consensus \"TGACAGCTGTTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGACAGCTGTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 12,
/* matrixname       */ jaspar_cne_pwm_122_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.736572,
/* max_score        */ 16.478472,
/* threshold        */ 0.774,
/* info content     */ 23.956030,
/* base_counts      */ {246, 246, 369, 615},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 123
};

	float jaspar_cne_pwm_123_matrix_row_0_[17] = {  1.162364, -1.900934, -7.204238, -2.589118, -7.204238, -0.805644, -1.497128,  1.064750, -7.204238, -7.204238, -1.210277, -7.204238, -7.204238, -1.497128, -0.987632,  0.984728,  1.229790};
	float jaspar_cne_pwm_123_matrix_row_1_[17] = { -2.347956, -1.255966,  1.470952, -6.963077, -1.659772, -1.255966,  1.355910, -2.347956, -6.963077, -2.347956,  1.331223, -0.564481, -1.255966, -2.347956, -6.963077, -0.746470, -2.347956};
	float jaspar_cne_pwm_123_matrix_row_2_[17] = { -6.963077,  1.168748, -2.347956, -6.963077,  1.331223, -0.746470, -6.963077, -6.963077, -2.347956, -6.963077, -2.347956, -6.963077, -0.564481,  1.380001,  1.403526, -0.969115, -6.963077};
	float jaspar_cne_pwm_123_matrix_row_3_[17] = { -1.210277, -0.400733, -2.589118,  1.251292, -0.805644,  0.927586, -1.210277, -0.518378,  1.251292,  1.251292, -1.497128,  1.138839,  1.064750, -1.900934, -7.204238, -1.497128, -2.589118};
	float *jaspar_cne_pwm_123_matrix[4] = { jaspar_cne_pwm_123_matrix_row_0_, jaspar_cne_pwm_123_matrix_row_1_, jaspar_cne_pwm_123_matrix_row_2_, jaspar_cne_pwm_123_matrix_row_3_};
	PWM jaspar_cne_pwm_123_ = {
/* accession        */ "CN0183",
/* name             */ "LM183",
/* label            */ " CN0183	22.8347988531333	LM183	unknown	; consensus \"AGCTGTCATTCTTGGAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCTGTCATTCTTGGAA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_123_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.594994,
/* max_score        */ 20.768269,
/* threshold        */ 0.813,
/* info content     */ 22.764927,
/* base_counts      */ {188, 154, 177, 297},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_124_matrix_row_0_[15] = {  0.579434, -1.577107, -7.284217, -1.577107,  1.149812, -7.284217, -7.284217, -7.284217,  1.272389, -1.980912, -7.284217, -7.284217, -7.284217, -7.284217, -2.669097};
	float jaspar_cne_pwm_124_matrix_row_1_[15] = {  0.453042,  1.275931, -7.043055, -7.043055, -1.335945,  1.494137,  1.513551, -0.826449, -7.043055,  1.474338, -7.043055, -7.043055, -0.644460,  1.433524,  1.300023};
	float jaspar_cne_pwm_124_matrix_row_2_[15] = { -7.043055, -1.049094, -7.043055,  1.454139, -1.739750, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055, -1.049094, -7.043055};
	float jaspar_cne_pwm_124_matrix_row_3_[15] = { -0.598356, -1.290256,  1.272389, -7.284217, -2.669097, -2.669097, -7.284217,  1.171313, -7.284217, -7.284217,  1.272389,  1.272389,  1.149812, -7.284217, -0.480712};
	float *jaspar_cne_pwm_124_matrix[4] = { jaspar_cne_pwm_124_matrix_row_0_, jaspar_cne_pwm_124_matrix_row_1_, jaspar_cne_pwm_124_matrix_row_2_, jaspar_cne_pwm_124_matrix_row_3_};
	PWM jaspar_cne_pwm_124_ = {
/* accession        */ "CN0139",
/* name             */ "LM139",
/* label            */ " CN0139	23.9425126253882	LM139	unknown	; consensus \"ACTGACCTACTTTCC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "MCTGACCTACTTTCC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_124_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.458702,
/* max_score        */ 19.085571,
/* threshold        */ 0.803,
/* info content     */ 23.857727,
/* base_counts      */ {133, 316, 59, 272},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_125_matrix_row_0_[21] = {  0.121195, -0.359665,  0.909554, -2.497528, -2.784379, -3.876369,  1.255403, -2.497528,  1.144184, -3.876369, -2.274884, -8.491489, -8.491489, -8.491489, -1.246548, -1.177603, -1.113106,  0.934043, -1.320601, -1.687984,  0.624650};
	float jaspar_cne_pwm_125_matrix_row_1_[21] = { -0.148347,  0.398069, -0.936441, -0.509228,  1.391860,  1.502395, -8.250328, -3.635207, -1.159418, -8.250328, -3.635207, -2.033721, -1.446822, -2.033721, -8.250328, -1.697820,  1.267571, -1.341573, -0.348950,  1.008898, -0.811356};
	float jaspar_cne_pwm_125_matrix_row_2_[21] = { -1.005386, -0.600159, -0.648925, -0.425882, -1.246353, -8.250328, -2.947023,  1.466891, -1.446822, -8.250328,  1.454770, -2.033721, -2.256366, -2.947023,  1.404763,  1.331645, -2.033721, -1.079439, -0.243627, -1.564467, -0.386676};
	float jaspar_cne_pwm_125_matrix_row_3_[21] = {  0.390486,  0.240976, -0.995392,  0.917784, -2.092895, -3.876369, -3.876369, -2.784379, -8.491489,  1.267030, -2.497528,  1.213608,  1.195147,  1.231734, -2.497528, -1.938982, -1.052518, -0.590112,  0.758168,  0.065117, -0.197190};
	float *jaspar_cne_pwm_125_matrix[4] = { jaspar_cne_pwm_125_matrix_row_0_, jaspar_cne_pwm_125_matrix_row_1_, jaspar_cne_pwm_125_matrix_row_2_, jaspar_cne_pwm_125_matrix_row_3_};
	PWM jaspar_cne_pwm_125_ = {
/* accession        */ "CN0014",
/* name             */ "LM14",
/* label            */ " CN0014	23.7373405491339	LM14	unknown	; consensus \"WNATCCAGATGTTTGGCAYYW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NNATCCAGATGTTTGGCATCA",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_125_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -91.254738,
/* max_score        */ 23.068649,
/* threshold        */ 0.859,
/* info content     */ 23.719522,
/* base_counts      */ {834, 789, 826, 1205},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 174
};

	float jaspar_cne_pwm_126_matrix_row_0_[21] = {  0.653515, -0.113329, -1.067611,  0.499423,  1.272389,  1.272389,  1.272389, -7.284217, -7.284217, -7.284217,  1.149812,  1.192362, -7.284217, -7.284217, -2.669097, -7.284217, -1.067611, -1.067611,  0.579434,  0.094167,  0.688594};
	float jaspar_cne_pwm_126_matrix_row_1_[21] = { -1.739750, -1.739750, -2.427935,  0.781391, -7.043055, -7.043055, -7.043055, -7.043055, -1.739750,  1.028163, -1.335945, -1.739750, -7.043055, -7.043055, -7.043055, -1.049094,  1.088770,  1.275931, -0.239550, -0.134300, -2.427935};
	float jaspar_cne_pwm_126_matrix_row_2_[21] = { -0.239550,  0.858322,  1.251244, -2.427935, -7.043055, -7.043055, -7.043055, -7.043055, -1.335945,  0.507080, -1.739750, -2.427935, -7.043055, -7.043055, -7.043055,  0.201886, -0.490547, -1.335945, -2.427935, -1.335945, -0.239550};
	float jaspar_cne_pwm_126_matrix_row_3_[21] = { -0.113329, -0.375462, -0.885622, -1.980912, -7.284217, -7.284217, -7.284217,  1.272389,  1.171313, -2.669097, -2.669097, -2.669097,  1.272389,  1.272389,  1.252975,  0.847608, -0.885622, -1.577107,  0.094167,  0.456882, -0.113329};
	float *jaspar_cne_pwm_126_matrix[4] = { jaspar_cne_pwm_126_matrix_row_0_, jaspar_cne_pwm_126_matrix_row_1_, jaspar_cne_pwm_126_matrix_row_2_, jaspar_cne_pwm_126_matrix_row_3_};
	PWM jaspar_cne_pwm_126_ = {
/* accession        */ "CN0094",
/* name             */ "LM94",
/* label            */ " CN0094	24.8567798112583	LM94	unknown	; consensus \"WRGMAAATTSAATTTKCMWWW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGGMAAATTCAATTTTCCWWA",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_126_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.111107,
/* max_score        */ 21.910646,
/* threshold        */ 0.878,
/* info content     */ 24.778624,
/* base_counts      */ {402, 168, 139, 383},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_127_matrix_row_0_[19] = {  0.906984, -0.316086, -2.609720, -1.921536,  1.164746, -1.921536, -1.517731, -1.517731, -7.224841, -7.224841, -2.609720, -1.008235, -7.224841, -2.609720,  1.164746, -2.609720, -1.517731, -1.230879, -0.538980};
	float jaspar_cne_pwm_127_matrix_row_1_[19] = { -0.989717, -2.368558,  1.148146,  1.259341, -0.989717, -2.368558,  0.187210, -2.368558, -6.983679, -0.767073, -2.368558, -2.368558,  1.492901,  1.382924, -6.983679, -6.983679, -6.983679, -2.368558, -1.276569};
	float jaspar_cne_pwm_127_matrix_row_2_[19] = { -0.431171, -0.989717,  0.020295, -2.368558, -2.368558, -0.767073, -1.276569, -0.767073,  1.513516, -6.983679, -6.983679,  1.205288, -6.983679, -6.983679, -1.680374, -6.983679,  1.428376,  1.382924, -6.983679};
	float jaspar_cne_pwm_127_matrix_row_3_[19] = { -1.230879,  0.906984, -1.517731, -0.538980, -7.224841,  1.094145,  0.781860,  1.069459, -7.224841,  1.164746,  1.230690, -0.672333, -2.609720, -1.008235, -1.517731,  1.251739, -2.609720, -2.609720,  1.018179};
	float *jaspar_cne_pwm_127_matrix[4] = { jaspar_cne_pwm_127_matrix_row_0_, jaspar_cne_pwm_127_matrix_row_1_, jaspar_cne_pwm_127_matrix_row_2_, jaspar_cne_pwm_127_matrix_row_3_};
	PWM jaspar_cne_pwm_127_ = {
/* accession        */ "CN0191",
/* name             */ "LM191",
/* label            */ " CN0191	23.859667940978	LM191	unknown	; consensus \"ATCCATTTGTTGCCATGGT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATCCATTTGTTGCCATGGT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_127_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.612289,
/* max_score        */ 22.567692,
/* threshold        */ 0.842,
/* info content     */ 23.791975,
/* base_counts      */ {166, 198, 212, 355},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_128_matrix_row_0_[15] = { -0.039248, -2.673675,  0.029721,  1.213260,  1.263016, -0.732038, -2.673675,  1.010342, -0.886070, -7.976980,  1.253261, -7.976980, -7.976980, -2.673675, -3.361859};
	float jaspar_cne_pwm_128_matrix_row_1_[15] = { -2.028708,  0.236993, -1.337223, -2.028708, -7.735818, -3.120697, -1.741856, -7.735818, -2.432513,  1.504178, -7.735818, -7.735818,  1.464573, -7.735818, -3.120697};
	float jaspar_cne_pwm_128_matrix_row_2_[15] = {  0.396007,  1.132173, -1.183310, -3.120697, -3.120697, -3.120697, -1.741856, -2.432513,  1.358101, -3.120697, -7.735818,  1.513839, -1.519212, -3.120697,  1.484572};
	float jaspar_cne_pwm_128_matrix_row_3_[15] = {  0.292008, -2.673675,  0.739228, -2.673675, -7.976980,  1.105641,  1.171591, -0.280313, -3.361859, -7.976980, -2.673675, -7.976980, -7.976980,  1.243410, -3.361859};
	float *jaspar_cne_pwm_128_matrix[4] = { jaspar_cne_pwm_128_matrix_row_0_, jaspar_cne_pwm_128_matrix_row_1_, jaspar_cne_pwm_128_matrix_row_2_, jaspar_cne_pwm_128_matrix_row_3_};
	PWM jaspar_cne_pwm_128_ = {
/* accession        */ "CN0049",
/* name             */ "LM49",
/* label            */ " CN0049	21.3581245304141	LM49	unknown	; consensus \"KGTAATTAGCAGCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NGTAATTAGCAGCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_128_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -76.793762,
/* max_score        */ 17.853191,
/* threshold        */ 0.790,
/* info content     */ 21.327213,
/* base_counts      */ {474, 251, 422, 413},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 104
};

	float jaspar_cne_pwm_129_matrix_row_0_[20] = { -2.355722, -6.970843, -0.062088, -6.970843, -6.970843, -6.970843, -2.355722, -2.355722, -0.572248,  0.679326, -0.572248,  0.343044,  0.274099, -6.970843,  1.272177,  1.245515,  0.525255,  0.679326, -0.976881,  1.272177};
	float jaspar_cne_pwm_129_matrix_row_1_[20] = { -6.729681, -1.022570, -0.735719, -6.729681, -2.114560, -6.729681, -2.114560,  0.871722,  1.094765, -2.114560, -2.114560, -6.729681, -6.729681,  1.341538, -6.729681, -6.729681,  0.871722, -6.729681,  1.243130, -6.729681};
	float jaspar_cne_pwm_129_matrix_row_2_[20] = { -6.729681,  1.372300,  0.361229,  0.820455, -6.729681, -6.729681,  1.459286, -2.114560, -6.729681, -2.114560, -6.729681,  1.011418,  1.053960, -0.331086, -6.729681, -2.114560, -6.729681,  0.441208, -1.426376, -6.729681};
	float jaspar_cne_pwm_129_matrix_row_3_[20] = {  1.245515, -1.667538,  0.120067,  0.579293,  1.245515,  1.272177, -6.970843,  0.407541, -0.418335,  0.343044,  1.068637, -6.970843, -6.970843, -6.970843, -6.970843, -6.970843, -6.970843, -0.976881, -1.263733, -6.970843};
	float *jaspar_cne_pwm_129_matrix[4] = { jaspar_cne_pwm_129_matrix_row_0_, jaspar_cne_pwm_129_matrix_row_1_, jaspar_cne_pwm_129_matrix_row_2_, jaspar_cne_pwm_129_matrix_row_3_};
	PWM jaspar_cne_pwm_129_ = {
/* accession        */ "CN0219",
/* name             */ "LM219",
/* label            */ " CN0219	24.8607405200657	LM219	unknown	; consensus \"TGYKTTGYYWWRGCAAMRCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGNKTTGCCATGGCAACACA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_129_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -117.201195,
/* max_score        */ 21.481888,
/* threshold        */ 0.866,
/* info content     */ 24.742432,
/* base_counts      */ {231, 137, 171, 221},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 38
};

	float jaspar_cne_pwm_130_matrix_row_0_[19] = { -1.105323, -2.706809,  1.133601, -2.706809, -2.706809, -0.923334,  0.838875, -1.614819,  1.234677, -0.769421, -0.413174, -2.018624, -2.018624,  1.175265,  1.253722,  1.154650, -1.105323, -0.518424, -1.614819};
	float jaspar_cne_pwm_130_matrix_row_1_[19] = { -7.080767, -7.080767, -2.465647,  1.416427,  1.395813, -0.864161, -1.373657, -0.864161, -7.080767, -7.080767, -2.465647, -7.080767, -1.777462, -7.080767, -7.080767, -2.465647, -7.080767, -7.080767,  1.353262};
	float jaspar_cne_pwm_130_matrix_row_2_[19] = { -1.777462,  1.494884, -1.373657, -7.080767, -7.080767, -2.465647, -0.528259,  1.331288, -1.777462,  1.353262,  1.285836, -0.277262,  1.353262, -7.080767, -2.465647, -1.086805,  1.331288,  1.285836, -1.777462};
	float jaspar_cne_pwm_130_matrix_row_3_[19] = {  1.133601, -7.321929, -1.614819, -1.327968, -1.105323,  1.021149, -0.518424, -2.706809, -7.321929, -2.706809, -7.321929,  1.044674, -1.327968, -1.105323, -7.321929, -2.706809, -1.327968, -2.018624, -1.614819};
	float *jaspar_cne_pwm_130_matrix[4] = { jaspar_cne_pwm_130_matrix_row_0_, jaspar_cne_pwm_130_matrix_row_1_, jaspar_cne_pwm_130_matrix_row_2_, jaspar_cne_pwm_130_matrix_row_3_};
	PWM jaspar_cne_pwm_130_ = {
/* accession        */ "CN0211",
/* name             */ "LM211",
/* label            */ " CN0211	24.8132048190737	LM211	unknown	; consensus \"TGACCTAGAGGTGAAAGGC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGACCTAGAGGTGAAAGGC",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_130_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.448509,
/* max_score        */ 23.591372,
/* threshold        */ 0.854,
/* info content     */ 24.748543,
/* base_counts      */ {339, 161, 352, 174},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 54
};

	float jaspar_cne_pwm_131_matrix_row_0_[19] = { -7.224841,  1.164746, -7.224841, -1.008235,  1.044147, -7.224841, -2.609720, -7.224841, -2.609720,  0.935963, -2.609720, -2.609720,  1.272354, -1.921536, -7.224841, -1.921536,  1.018179, -1.921536, -0.538980};
	float jaspar_cne_pwm_131_matrix_row_1_[19] = {  1.513516, -1.276569,  1.450350,  0.394705, -6.983679, -2.368558, -6.983679, -2.368558, -0.074924, -2.368558,  1.382924,  1.335307, -6.983679, -2.368558, -2.368558, -1.680374, -6.983679, -2.368558, -1.276569};
	float jaspar_cne_pwm_131_matrix_row_2_[19] = { -6.983679, -6.983679, -1.680374, -2.368558, -0.767073,  1.492901,  1.471852,  1.492901, -0.074924,  0.020295, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -2.368558, -0.074924,  1.450350, -1.276569};
	float jaspar_cne_pwm_131_matrix_row_3_[19] = { -7.224841, -1.921536, -2.609720,  0.676537, -1.008235, -7.224841, -2.609720, -7.224841,  0.712891, -1.921536, -1.008235, -0.672333, -7.224841,  1.209188,  1.251739,  1.164746, -2.609720, -7.224841,  0.935963};
	float *jaspar_cne_pwm_131_matrix[4] = { jaspar_cne_pwm_131_matrix_row_0_, jaspar_cne_pwm_131_matrix_row_1_, jaspar_cne_pwm_131_matrix_row_2_, jaspar_cne_pwm_131_matrix_row_3_};
	PWM jaspar_cne_pwm_131_ = {
/* accession        */ "CN0155",
/* name             */ "LM155",
/* label            */ " CN0155	26.3994585330845	LM155	unknown	; consensus \"CACTAGGGKACCATTTAGT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CACTAGGGTACCATTTAGT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_131_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -110.451599,
/* max_score        */ 22.976553,
/* threshold        */ 0.874,
/* info content     */ 26.317160,
/* base_counts      */ {228, 219, 232, 252},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_132_matrix_row_0_[18] = { -0.444485, -0.518538,  1.206341, -7.689426, -7.689426, -7.689426, -7.689426,  1.272581,  1.246609,  1.259679, -1.982316, -1.003565,  0.135020,  1.192549, -1.472820, -1.982316, -1.472820, -1.695464};
	float jaspar_cne_pwm_132_matrix_row_1_[18] = { -0.539509, -0.762403, -2.833143,  1.433711, -2.833143, -7.448264,  1.487771, -7.448264, -7.448264, -7.448264, -7.448264, -7.448264, -1.049669, -1.741154, -7.448264,  1.217522, -0.895756,  0.712540};
	float jaspar_cne_pwm_132_matrix_row_2_[18] = { -0.762403,  1.069129, -1.454302, -1.741154, -7.448264, -7.448264, -2.833143, -7.448264, -2.833143, -7.448264,  1.461106,  1.405544,  1.007267, -2.833143,  1.391158, -1.231658, -7.448264,  0.101871};
	float jaspar_cne_pwm_132_matrix_row_3_[18] = {  0.744603, -1.136918, -7.689426, -1.982316,  1.259679,  1.272581, -3.074306, -7.689426, -3.074306, -3.074306, -3.074306, -7.689426, -7.689426, -2.386121, -1.695464, -0.598516,  1.105550, -0.088024};
	float *jaspar_cne_pwm_132_matrix[4] = { jaspar_cne_pwm_132_matrix_row_0_, jaspar_cne_pwm_132_matrix_row_1_, jaspar_cne_pwm_132_matrix_row_2_, jaspar_cne_pwm_132_matrix_row_3_};
	PWM jaspar_cne_pwm_132_ = {
/* accession        */ "CN0035",
/* name             */ "LM35",
/* label            */ " CN0035	24.5746338470254	LM35	unknown	; consensus \"WRACTTCAAAGGGAGCTB\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGACTTCAAAGGGAGCTN",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_132_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -107.166969,
/* max_score        */ 21.745920,
/* threshold        */ 0.840,
/* info content     */ 24.524220,
/* base_counts      */ {456, 277, 352, 319},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 78
};

	float jaspar_cne_pwm_133_matrix_row_0_[16] = {  0.846378, -1.921536, -2.609720, -2.609720,  1.272354,  1.251739,  1.094145,  0.153543, -2.609720,  1.230690,  1.209188,  1.272354, -7.224841,  1.230690, -2.609720, -0.421336};
	float jaspar_cne_pwm_133_matrix_row_1_[16] = { -6.983679, -6.983679,  1.492901,  1.492901, -6.983679, -6.983679, -6.983679,  0.666490,  1.492901, -1.680374, -1.276569, -6.983679,  1.513516, -6.983679, -1.276569, -0.431171};
	float jaspar_cne_pwm_133_matrix_row_2_[16] = { -0.767073,  1.405908, -6.983679, -6.983679, -6.983679, -2.368558, -0.297818,  0.020295, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.428376, -0.585084};
	float jaspar_cne_pwm_133_matrix_row_3_[16] = { -0.133931, -1.517731, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -2.609720, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841, -1.921536, -7.224841,  0.676537};
	float *jaspar_cne_pwm_133_matrix[4] = { jaspar_cne_pwm_133_matrix_row_0_, jaspar_cne_pwm_133_matrix_row_1_, jaspar_cne_pwm_133_matrix_row_2_, jaspar_cne_pwm_133_matrix_row_3_};
	PWM jaspar_cne_pwm_133_ = {
/* accession        */ "CN0141",
/* name             */ "LM141",
/* label            */ " CN0141	24.331115467769	LM141	unknown	; consensus \"WGCCAAAMCAAACAGN\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCCAAAMCAAACAGT",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_133_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.619087,
/* max_score        */ 19.577065,
/* threshold        */ 0.816,
/* info content     */ 24.242186,
/* base_counts      */ {390, 229, 120, 45},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_134_matrix_row_0_[16] = { -0.641337, -2.309870, -2.794897, -1.739659, -2.572004,  1.215744,  1.118398,  1.215744, -9.480759, -3.264152, -2.677253,  0.248435,  1.224753,  1.257832, -2.309870, -0.797881};
	float jaspar_cne_pwm_134_matrix_row_1_[16] = {  0.902948, -2.553735, -1.861212, -4.624475, -0.996577, -2.553735, -1.925709, -3.245635, -9.239596, -9.239596,  1.287179, -2.436091, -4.624475, -9.239596, -2.841001, -0.784066};
	float jaspar_cne_pwm_134_matrix_row_2_[16] = { -1.023238, -3.936291, -2.687088, -2.436091,  1.371952, -2.068708, -3.022990, -3.245635, -3.532486,  1.429382, -1.689461, -4.624475, -1.925709, -3.936291, -2.235622,  0.648828};
	float jaspar_cne_pwm_134_matrix_row_3_[16] = { -0.193364,  1.222509,  1.204368,  1.199781, -2.235817, -3.264152, -1.025228, -1.984661,  1.266471, -1.378778, -0.670746,  0.794327, -3.082163, -3.264152,  1.206654,  0.230418};
	float *jaspar_cne_pwm_134_matrix[4] = { jaspar_cne_pwm_134_matrix_row_0_, jaspar_cne_pwm_134_matrix_row_1_, jaspar_cne_pwm_134_matrix_row_2_, jaspar_cne_pwm_134_matrix_row_3_};
	PWM jaspar_cne_pwm_134_ = {
/* accession        */ "CN0041",
/* name             */ "LM41",
/* label            */ " CN0041	21.6262629089616	LM41	unknown	; consensus \"CTTTGAAATGCTAATK\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTGAAATGCTAATK",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_134_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -68.008720,
/* max_score        */ 18.566872,
/* threshold        */ 0.797,
/* info content     */ 21.620604,
/* base_counts      */ {2569, 780, 1161, 2978},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 468
};

	float jaspar_cne_pwm_135_matrix_row_0_[17] = { -0.421336,  0.425328, -1.008235, -2.609720, -2.609720, -1.921536,  1.272354, -7.224841, -2.609720, -2.609720, -1.517731,  0.991517,  1.141762, -1.230879, -2.609720, -2.609720, -0.421336};
	float jaspar_cne_pwm_135_matrix_row_1_[17] = { -0.585084, -0.074924, -1.680374, -2.368558, -6.983679,  1.428376, -6.983679, -6.983679, -6.983679, -6.983679, -2.368558, -1.276569, -6.983679, -6.983679, -6.983679,  1.450350, -6.983679};
	float jaspar_cne_pwm_135_matrix_row_2_[17] = {  0.840767, -0.989717, -6.983679, -2.368558, -6.983679, -2.368558, -6.983679, -6.983679, -6.983679, -6.983679,  1.405908, -0.767073, -2.368558, -2.368558,  1.428376, -6.983679, -6.983679};
	float jaspar_cne_pwm_135_matrix_row_3_[17] = { -0.421336,  0.020101,  1.118237,  1.209188,  1.251739, -2.609720, -7.224841,  1.272354,  1.251739,  1.251739, -2.609720, -1.230879, -1.008235,  1.164746, -1.517731, -1.921536,  1.069459};
	float *jaspar_cne_pwm_135_matrix[4] = { jaspar_cne_pwm_135_matrix_row_0_, jaspar_cne_pwm_135_matrix_row_1_, jaspar_cne_pwm_135_matrix_row_2_, jaspar_cne_pwm_135_matrix_row_3_};
	PWM jaspar_cne_pwm_135_ = {
/* accession        */ "CN0180",
/* name             */ "LM180",
/* label            */ " CN0180	23.8116243822919	LM180	unknown	; consensus \"KWTTTCATTTGAATGCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GNTTTCATTTGAATGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_135_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.983330,
/* max_score        */ 19.973936,
/* threshold        */ 0.822,
/* info content     */ 23.737534,
/* base_counts      */ {188, 114, 127, 404},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_136_matrix_row_0_[20] = {  0.768572, -0.651731, -2.589118, -0.651731, -1.210277,  1.272341, -1.210277, -7.204238, -7.204238, -1.210277, -0.400733, -0.200264,  0.835241,  1.064750,  1.229790,  1.207816, -7.204238, -2.589118, -0.295484,  0.927586};
	float jaspar_cne_pwm_136_matrix_row_1_[20] = {  0.040898, -6.963077, -6.963077, -6.963077,  1.426511, -6.963077,  0.900575, -6.963077, -0.969115, -2.347956, -6.963077, -2.347956, -0.054322, -6.963077, -6.963077, -6.963077, -2.347956, -6.963077, -0.969115, -0.277216};
	float jaspar_cne_pwm_136_matrix_row_2_[20] = { -0.746470, -1.659772,  1.470952,  1.279943, -6.963077, -6.963077, -6.963077, -2.347956, -2.347956, -2.347956,  1.305912,  1.197727, -1.255966, -0.410569, -1.659772, -2.347956, -1.659772,  1.492454,  0.900575, -6.963077};
	float jaspar_cne_pwm_136_matrix_row_3_[20] = { -1.497128,  1.064750, -2.589118, -1.497128, -7.204238, -7.204238,  0.291859,  1.251292,  1.162364,  1.138839, -7.204238, -2.589118, -1.210277, -1.900934, -7.204238, -1.900934,  1.207816, -7.204238, -0.518378, -0.805644};
	float *jaspar_cne_pwm_136_matrix[4] = { jaspar_cne_pwm_136_matrix_row_0_, jaspar_cne_pwm_136_matrix_row_1_, jaspar_cne_pwm_136_matrix_row_2_, jaspar_cne_pwm_136_matrix_row_3_};
	PWM jaspar_cne_pwm_136_ = {
/* accession        */ "CN0220",
/* name             */ "LM220",
/* label            */ " CN0220	25.2660551768414	LM220	unknown	; consensus \"ATGGCACTTTGGAAAATGGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATGGCACTTTGGAAAATGGA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_136_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -115.034721,
/* max_score        */ 23.105808,
/* threshold        */ 0.872,
/* info content     */ 25.183170,
/* base_counts      */ {330, 110, 256, 264},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_137_matrix_row_0_[15] = { -7.245027, -7.245027, -2.629907, -7.245027, -7.245027, -7.245027, -1.941722, -2.629907, -7.245027, -7.245027, -7.245027, -7.245027, -1.537917,  1.272366, -2.629907};
	float jaspar_cne_pwm_137_matrix_row_1_[15] = { -7.003865, -7.003865, -2.388745,  1.513528, -7.003865, -7.003865,  0.492232,  1.493329, -0.451357, -1.700560,  1.513528,  1.513528,  1.451665, -7.003865, -7.003865};
	float jaspar_cne_pwm_137_matrix_row_2_[15] = { -7.003865,  1.513528,  1.430164, -7.003865, -7.003865,  1.385722,  0.820581, -7.003865, -2.388745, -2.388745, -7.003865, -7.003865, -7.003865, -7.003865,  1.493329};
	float jaspar_cne_pwm_137_matrix_row_3_[15] = {  1.272366, -7.245027, -1.941722, -7.245027,  1.272366, -0.846432, -1.028421, -7.245027,  1.098051,  1.210503, -7.245027, -7.245027, -7.245027, -7.245027, -7.245027};
	float *jaspar_cne_pwm_137_matrix[4] = { jaspar_cne_pwm_137_matrix_row_0_, jaspar_cne_pwm_137_matrix_row_1_, jaspar_cne_pwm_137_matrix_row_2_, jaspar_cne_pwm_137_matrix_row_3_};
	PWM jaspar_cne_pwm_137_ = {
/* accession        */ "CN0221",
/* name             */ "LM221",
/* label            */ " CN0221	25.6857491206426	LM221	unknown	; consensus \"TGGCTGKCTTCCCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGGCTGSCTTCCCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_137_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.756973,
/* max_score        */ 20.254555,
/* threshold        */ 0.812,
/* info content     */ 25.591286,
/* base_counts      */ {58, 274, 216, 202},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_138_matrix_row_0_[22] = { -0.257402, -0.950349, -1.603907, -0.218189, -1.866040, -2.088934, -2.088934, -0.950349,  1.133729,  1.187008,  1.153434, -1.460908,  1.133729, -1.033696, -3.471490, -2.088934,  1.210318,  1.210318, -1.866040, -0.801984, -1.529854,  0.886684};
	float jaspar_cne_pwm_138_matrix_row_1_[22] = { -1.288692, -1.981125, -3.230328, -0.883465,  1.236380,  1.270089, -1.981125, -1.155250, -2.826523, -2.826523, -8.533633, -2.826523, -2.826523, -2.317027, -1.442723, -1.981125, -2.135038, -1.624878, -1.362745, -1.847772,  0.899931, -1.288692};
	float jaspar_cne_pwm_138_matrix_row_2_[22] = {  1.020368, -0.709187,  1.374892,  1.048340, -2.317027, -8.533633, -2.317027,  1.281078, -0.709187, -1.362745, -0.983498,  1.369904, -1.037536,  1.354791, -8.533633, -2.826523, -2.826523, -3.230328, -1.288692,  1.189591, -1.442723, -0.401809};
	float jaspar_cne_pwm_138_matrix_row_3_[22] = { -0.911144,  0.989488, -1.460908, -1.603907, -0.455809, -0.431717,  1.182280, -2.222287, -3.471490, -3.067685, -2.222287, -1.603907, -1.971290, -2.376200,  1.210318,  1.191714, -2.558189, -3.471490,  1.098284, -0.873418,  0.212527, -0.911144};
	float *jaspar_cne_pwm_138_matrix[4] = { jaspar_cne_pwm_138_matrix_row_0_, jaspar_cne_pwm_138_matrix_row_1_, jaspar_cne_pwm_138_matrix_row_2_, jaspar_cne_pwm_138_matrix_row_3_};
	PWM jaspar_cne_pwm_138_ = {
/* accession        */ "CN0012",
/* name             */ "LM12",
/* label            */ " CN0012	24.883970111514	LM12	unknown	; consensus \"RTGGCCTGAAAGAGTTAATGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GTGGCCTGAAAGAGTTAATGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 22,
/* matrixname       */ jaspar_cne_pwm_138_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -70.786613,
/* max_score        */ 25.632666,
/* threshold        */ 0.890,
/* info content     */ 24.872629,
/* base_counts      */ {1702, 635, 1412, 1333},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 231
};

	float jaspar_cne_pwm_139_matrix_row_0_[20] = { -1.537917, -7.245027, -0.441522, -7.245027, -1.941722,  0.997992,  1.210503,  0.997992,  1.049272, -0.692519, -1.028421,  1.049272, -0.846432,  1.167028, -7.245027, -7.245027, -2.629907, -1.028421,  0.692704, -1.537917};
	float jaspar_cne_pwm_139_matrix_row_1_[20] = { -1.700560,  1.472714, -7.003865, -7.003865, -1.009904, -1.700560, -1.296755, -0.787259, -7.003865, -1.700560,  1.385722, -1.700560,  1.339213, -2.388745, -1.700560,  0.167023, -7.003865, -1.700560, -0.787259, -7.003865};
	float jaspar_cne_pwm_139_matrix_row_2_[20] = { -2.388745, -1.700560, -7.003865,  1.362738,  1.212493, -0.318004, -7.003865, -1.009904, -1.009904,  1.035615, -7.003865, -7.003865, -2.388745, -2.388745, -0.451357, -2.388745, -1.009904,  1.290434, -0.200360, -7.003865};
	float jaspar_cne_pwm_139_matrix_row_3_[20] = {  1.144560, -7.245027,  1.073959, -0.692519, -0.692519, -1.941722, -7.245027, -1.537917, -0.846432, -0.336272, -2.629907, -0.559166, -2.629907, -1.537917,  1.073959,  0.943940,  1.167028, -1.537917, -0.559166,  1.210503};
	float *jaspar_cne_pwm_139_matrix[4] = { jaspar_cne_pwm_139_matrix_row_0_, jaspar_cne_pwm_139_matrix_row_1_, jaspar_cne_pwm_139_matrix_row_2_, jaspar_cne_pwm_139_matrix_row_3_};
	PWM jaspar_cne_pwm_139_ = {
/* accession        */ "CN0214",
/* name             */ "LM214",
/* label            */ " CN0214	23.3936017298117	LM214	unknown	; consensus \"TCTGGAAAAGCACATTTGAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCTGGAAAAGCACATTTGAT",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_139_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.265472,
/* max_score        */ 22.877642,
/* threshold        */ 0.845,
/* info content     */ 23.327036,
/* base_counts      */ {317, 177, 193, 313},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_140_matrix_row_0_[14] = { -7.245027, -7.245027, -7.245027, -7.245027, -7.245027,  1.098051, -2.629907,  1.272366, -7.245027, -2.629907, -7.245027, -7.245027,  1.189002, -1.537917};
	float jaspar_cne_pwm_140_matrix_row_1_[14] = { -1.296755, -2.388745, -7.003865, -7.003865, -2.388745, -0.318004,  1.493329, -7.003865, -2.388745,  1.362738, -2.388745,  1.362738, -7.003865, -2.388745};
	float jaspar_cne_pwm_140_matrix_row_2_[14] = { -1.009904, -7.003865, -7.003865, -7.003865,  1.493329, -7.003865, -7.003865, -7.003865,  1.430164, -1.009904, -7.003865, -1.009904, -7.003865,  1.430164};
	float jaspar_cne_pwm_140_matrix_row_3_[14] = {  1.121576,  1.252167,  1.272366,  1.272366, -7.245027, -7.245027, -7.245027, -7.245027, -1.537917, -1.941722,  1.252167, -1.537917, -1.251066, -7.245027};
	float *jaspar_cne_pwm_140_matrix[4] = { jaspar_cne_pwm_140_matrix_row_0_, jaspar_cne_pwm_140_matrix_row_1_, jaspar_cne_pwm_140_matrix_row_2_, jaspar_cne_pwm_140_matrix_row_3_};
	PWM jaspar_cne_pwm_140_ = {
/* accession        */ "CN0127",
/* name             */ "LM127",
/* label            */ " CN0127	23.2419762025656	LM127	unknown	; consensus \"TTTTGACAGCTCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTTGACAGCTCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_140_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.574089,
/* max_score        */ 18.302523,
/* threshold        */ 0.791,
/* info content     */ 23.161404,
/* base_counts      */ {143, 151, 153, 253},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_141_matrix_row_0_[20] = { -0.434622, -0.690461, -0.257744,  0.101549, -2.766992,  1.237074, -8.474102, -0.978005, -3.858981, -3.170797,  0.082504, -8.474102, -8.474102, -8.474102, -3.170797, -2.075507,  1.261026, -3.170797, -0.372121, -0.467401};
	float jaspar_cne_pwm_141_matrix_row_1_[20] = { -0.536273,  0.086046, -1.324185, -0.736842,  1.484278, -3.617819, -8.232940,  1.143000, -8.232940, -0.736842, -0.919053, -2.525830, -2.929635, -3.617819,  0.243640,  1.389576, -3.617819, -2.525830, -0.919053, -0.682805};
	float jaspar_cne_pwm_141_matrix_row_2_[20] = { -0.536273, -0.536273, -0.331563,  0.779072, -3.617819, -8.232940, -2.525830, -0.536273, -8.232940,  1.362731,  0.741805, -3.617819, -8.232940, -2.525830,  0.977500, -1.680432, -3.617819, -3.617819, -0.449299, -0.582771};
	float jaspar_cne_pwm_141_matrix_row_3_[20] = {  0.695521,  0.562004,  0.705882, -0.978005, -3.858981, -2.257496,  1.255092, -1.303213,  1.266925, -2.480140, -0.649656,  1.249122,  1.261026,  1.249122, -0.823933, -1.921594, -8.474102,  1.237074,  0.726289,  0.746288};
	float *jaspar_cne_pwm_141_matrix[4] = { jaspar_cne_pwm_141_matrix_row_0_, jaspar_cne_pwm_141_matrix_row_1_, jaspar_cne_pwm_141_matrix_row_2_, jaspar_cne_pwm_141_matrix_row_3_};
	PWM jaspar_cne_pwm_141_ = {
/* accession        */ "CN0017",
/* name             */ "LM17",
/* label            */ " CN0017	22.5463270106005	LM17	unknown	; consensus \"WNWRCATCTGRTTTGCATTW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TNTRCATCTGRTTTGCATTT",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_141_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.083344,
/* max_score        */ 21.330410,
/* threshold        */ 0.832,
/* info content     */ 22.527821,
/* base_counts      */ {628, 652, 563, 1577},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 171
};

	float jaspar_cne_pwm_142_matrix_row_0_[20] = { -0.849035,  1.117647, -8.450438, -8.450438, -2.743327, -2.743327, -2.233832,  1.198222, -8.450438, -0.443737, -2.743327, -1.897930,  1.178679, -1.897930, -3.835317, -1.541683, -1.541683, -2.743327,  1.131535, -1.011466};
	float jaspar_cne_pwm_142_matrix_row_1_[20] = {  0.059712, -1.810681, -2.905971, -3.594155, -3.594155, -0.770304,  1.426398, -2.502165, -2.502165, -0.425635, -1.992670,  1.020965, -1.656768, -2.905971, -1.300521, -8.209275, -0.964334,  1.365777, -2.215314, -0.271544};
	float jaspar_cne_pwm_142_matrix_row_2_[20] = {  0.672700, -1.656768, -3.594155, -8.209275, -2.905971,  1.351792, -2.502165, -3.594155, -2.502165,  0.109710, -1.810681,  0.267304, -2.502165, -8.209275, -8.209275, -1.992670,  1.278772, -1.992670, -3.594155, -1.810681};
	float jaspar_cne_pwm_142_matrix_row_3_[20] = { -0.261471, -1.446464,  1.254660,  1.266780,  1.236199, -2.233832, -2.051843, -1.764577,  1.236199,  0.431537,  1.185236, -1.541683, -2.233832,  1.217391,  1.204652,  1.178679, -1.446464, -1.136551, -1.011466,  0.908409};
	float *jaspar_cne_pwm_142_matrix[4] = { jaspar_cne_pwm_142_matrix_row_0_, jaspar_cne_pwm_142_matrix_row_1_, jaspar_cne_pwm_142_matrix_row_2_, jaspar_cne_pwm_142_matrix_row_3_};
	PWM jaspar_cne_pwm_142_ = {
/* accession        */ "CN0098",
/* name             */ "LM98",
/* label            */ " CN0098	25.3783034419564	LM98	unknown	; consensus \"GATTTGCATTTCATTTGCAY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NATTTGCATNTCATTTGCAT",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_142_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.349663,
/* max_score        */ 22.862230,
/* threshold        */ 0.874,
/* info content     */ 25.359808,
/* base_counts      */ {714, 565, 478, 1583},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 167
};

	float jaspar_cne_pwm_143_matrix_row_0_[18] = {  0.234540, -0.144869, -1.610352,  1.174988, -2.197251, -3.798737, -8.413857,  1.260280, -3.798737,  1.181813, -3.110552, -2.706747, -8.413857, -8.413857, -2.706747, -1.242969, -0.863722, -0.476125};
	float jaspar_cne_pwm_143_matrix_row_1_[18] = { -0.622560, -0.858808,  1.123915, -1.956089, -0.733724,  1.488784,  1.507711, -8.172695, -2.869390, -1.956089, -8.172695, -3.557575, -2.178734, -1.774100, -1.168721, -1.263940, -0.476028,  0.943444};
	float jaspar_cne_pwm_143_matrix_row_2_[18] = { -1.263940, -1.620187, -1.620187, -3.557575, -1.486834, -3.557575, -8.172695, -3.557575,  1.488784, -1.620187, -8.172695,  1.488784, -1.486834, -8.172695, -1.001807,  1.159951,  1.168761, -0.794311};
	float jaspar_cne_pwm_143_matrix_row_3_[18] = {  0.508934,  0.796583, -0.224890, -1.610352,  1.066587, -3.110552, -3.798737, -3.798737, -3.798737, -3.110552,  1.260280, -8.413857,  1.195326,  1.234802,  1.089227, -0.589411, -2.015262, -0.550206};
	float *jaspar_cne_pwm_143_matrix[4] = { jaspar_cne_pwm_143_matrix_row_0_, jaspar_cne_pwm_143_matrix_row_1_, jaspar_cne_pwm_143_matrix_row_2_, jaspar_cne_pwm_143_matrix_row_3_};
	PWM jaspar_cne_pwm_143_ = {
/* accession        */ "CN0068",
/* name             */ "LM68",
/* label            */ " CN0068	22.8570135047616	LM68	unknown	; consensus \"WTCATCCAGATGTTTGGY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "WTCATCCAGATGTTTGGC",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_143_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -77.748245,
/* max_score        */ 21.138958,
/* threshold        */ 0.821,
/* info content     */ 22.837996,
/* base_counts      */ {632, 634, 620, 1012},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 161
};

	float jaspar_cne_pwm_144_matrix_row_0_[15] = {  1.021149, -7.321929, -7.321929, -7.321929, -7.321929, -7.321929, -2.018624,  1.272410,  0.809896, -7.321929, -7.321929,  1.272410,  0.056455,  1.272410, -1.614819};
	float jaspar_cne_pwm_144_matrix_row_1_[15] = { -2.465647, -7.080767,  1.513572, -7.080767, -7.080767, -7.080767,  0.743679, -7.080767, -7.080767, -7.080767,  1.436626, -7.080767,  1.021214, -7.080767, -7.080767};
	float jaspar_cne_pwm_144_matrix_row_2_[15] = { -1.086805,  1.475839, -7.080767, -7.080767,  1.513572, -7.080767,  0.820610, -7.080767, -0.682172, -7.080767, -1.086805, -7.080767, -0.864161, -7.080767,  1.456425};
	float jaspar_cne_pwm_144_matrix_row_3_[15] = { -0.769421, -2.018624, -7.321929,  1.272410, -7.321929,  1.272410, -7.321929, -7.321929, -0.076988,  1.272410, -7.321929, -7.321929, -7.321929, -7.321929, -7.321929};
	float *jaspar_cne_pwm_144_matrix[4] = { jaspar_cne_pwm_144_matrix_row_0_, jaspar_cne_pwm_144_matrix_row_1_, jaspar_cne_pwm_144_matrix_row_2_, jaspar_cne_pwm_144_matrix_row_3_};
	PWM jaspar_cne_pwm_144_ = {
/* accession        */ "CN0109",
/* name             */ "LM109",
/* label            */ " CN0109	24.2923164703666	LM109	unknown	; consensus \"WKCTGTSAWTCAMAR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCTGTSAATCACAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_144_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.731499,
/* max_score        */ 18.703363,
/* threshold        */ 0.805,
/* info content     */ 24.203817,
/* base_counts      */ {259, 163, 203, 185},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 54
};

	float jaspar_cne_pwm_145_matrix_row_0_[16] = {  1.046805,  1.161310,  1.212438,  0.211735, -3.860899,  0.714017,  1.266996,  1.269941, -3.170243, -4.549084, -1.423105, -2.073294, -0.324783, -3.860899, -4.549084, -1.993316};
	float jaspar_cne_pwm_145_matrix_row_1_[16] = { -1.139402, -1.919068, -8.923042, -4.307922, -0.467512,  0.526394, -8.923042, -8.923042, -8.923042, -8.923042, -8.923042,  1.254320,  0.580042, -3.619737, -1.226375, -0.950231};
	float jaspar_cne_pwm_145_matrix_row_2_[16] = { -0.734075, -1.098596, -2.014287,  0.846971,  1.352043, -3.215932, -8.923042, -4.307922, -8.923042, -8.923042,  0.757364, -0.223361, -8.923042, -8.923042, -2.119537, -1.372907};
	float jaspar_cne_pwm_145_matrix_row_3_[16] = { -2.360699, -9.164205, -2.255450, -0.708674, -3.860899, -1.785821, -3.860899, -9.164205,  1.261078,  1.269941,  0.503624, -2.765609,  0.368292,  1.261078,  1.174340,  1.075791};
	float *jaspar_cne_pwm_145_matrix[4] = { jaspar_cne_pwm_145_matrix_row_0_, jaspar_cne_pwm_145_matrix_row_1_, jaspar_cne_pwm_145_matrix_row_2_, jaspar_cne_pwm_145_matrix_row_3_};
	PWM jaspar_cne_pwm_145_ = {
/* accession        */ "CN0025",
/* name             */ "LM25",
/* label            */ " CN0025	21.0317487734382	LM25	unknown	; consensus \"AAARGMAATTKCYTTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAAGGAAATTKCYTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_145_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.843170,
/* max_score        */ 17.504477,
/* threshold        */ 0.793,
/* info content     */ 21.021540,
/* base_counts      */ {2017, 660, 788, 1991},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 341
};

	float jaspar_cne_pwm_146_matrix_row_0_[20] = { -0.499746,  0.521195,  0.480390,  0.767968,  0.991049,  0.010636, -1.999946, -7.303251,  1.253355,  1.193944, -2.688130, -7.303251,  1.233941,  1.272400, -7.303251, -1.999946,  1.253355, -0.904656, -1.999946,  0.246884};
	float jaspar_cne_pwm_146_matrix_row_1_[20] = { -1.354979, -0.509581, -0.663494, -0.845483, -1.068127, -1.068127, -7.062089, -7.062089, -7.062089, -7.062089, -2.446968,  1.494517, -1.758784, -7.062089, -7.062089, -7.062089, -7.062089, -0.509581,  1.327498, -0.509581};
	float jaspar_cne_pwm_146_matrix_row_2_[20] = {  0.910722,  0.316295,  0.251798, -0.845483, -1.758784, -2.446968, -7.062089,  1.513562, -2.446968, -1.354979,  1.475103, -2.446968, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089,  0.801562, -1.758784, -2.446968};
	float jaspar_cne_pwm_146_matrix_row_3_[20] = { -0.212341, -1.086645, -0.617390, -0.299277, -0.750743,  0.798730,  1.233941, -7.303251, -7.303251, -2.688130, -7.303251, -7.303251, -7.303251, -7.303251,  1.272400,  1.233941, -2.688130, -0.058309, -1.086645,  0.560400};
	float *jaspar_cne_pwm_146_matrix[4] = { jaspar_cne_pwm_146_matrix_row_0_, jaspar_cne_pwm_146_matrix_row_1_, jaspar_cne_pwm_146_matrix_row_2_, jaspar_cne_pwm_146_matrix_row_3_};
	PWM jaspar_cne_pwm_146_ = {
/* accession        */ "CN0087",
/* name             */ "LM87",
/* label            */ " CN0087	24.7943682188293	LM87	unknown	; consensus \"RNNWWTTGAAGCAATTAGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GRNAATTGAAGCAATTAKCW",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_146_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.879028,
/* max_score        */ 21.589972,
/* threshold        */ 0.865,
/* info content     */ 24.718105,
/* base_counts      */ {434, 142, 206, 278},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 53
};

	float jaspar_cne_pwm_147_matrix_row_0_[20] = {  0.856953, -0.154117, -1.537917, -1.251066,  1.121576, -1.941722, -1.941722, -1.941722, -1.941722, -1.537917, -1.537917, -7.245027, -2.629907, -7.245027, -1.537917, -7.245027,  1.210503,  1.167028,  1.189002, -7.245027};
	float jaspar_cne_pwm_147_matrix_row_1_[20] = { -7.003865, -2.388745, -7.003865,  1.385722, -1.296755, -0.451357, -7.003865, -1.296755, -2.388745, -7.003865, -1.296755,  1.339213, -1.296755, -1.700560, -7.003865, -0.787259, -7.003865, -7.003865, -7.003865, -1.009904};
	float jaspar_cne_pwm_147_matrix_row_2_[20] = { -1.296755, -0.605270, -0.787259, -2.388745, -2.388745, -1.700560, -1.296755, -0.787259, -0.787259,  1.339213,  1.239154, -2.388745, -7.003865, -0.605270, -2.388745,  1.408190, -1.700560, -1.296755, -1.009904,  1.185102};
	float jaspar_cne_pwm_147_matrix_row_3_[20] = { -0.000086,  0.794453,  1.098051, -2.629907, -1.537917,  1.023961,  1.167028,  1.049272,  1.098051, -1.028421, -0.846432, -0.692519,  1.189002,  1.098051,  1.189002, -7.245027, -2.629907, -1.941722, -7.245027, -0.336272};
	float *jaspar_cne_pwm_147_matrix[4] = { jaspar_cne_pwm_147_matrix_row_0_, jaspar_cne_pwm_147_matrix_row_1_, jaspar_cne_pwm_147_matrix_row_2_, jaspar_cne_pwm_147_matrix_row_3_};
	PWM jaspar_cne_pwm_147_ = {
/* accession        */ "CN0196",
/* name             */ "LM196",
/* label            */ " CN0196	24.4372926458252	LM196	unknown	; consensus \"ATTCATTTTGGCTTTGAAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTCATTTTGGCTTTGAAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_147_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -107.473564,
/* max_score        */ 23.148523,
/* threshold        */ 0.860,
/* info content     */ 24.370491,
/* base_counts      */ {251, 118, 209, 422},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_148_matrix_row_0_[17] = { -0.704470,  0.260456,  1.178593,  1.212239, -0.896789, -2.600175, -0.809803, -0.896789,  1.155516,  1.219565,  1.107700, -2.090015,  1.230454, -3.695465, -2.600175, -1.907860, -4.383649};
	float jaspar_cne_pwm_148_matrix_row_1_[17] = { -0.541249, -1.261510, -2.359013, -3.050497, -1.156205, -8.757607, -8.757607, -0.856230, -1.848853, -2.763646, -1.156205,  1.457071, -4.142487, -2.205100,  1.404429, -4.142487, -8.757607};
	float jaspar_cne_pwm_148_matrix_row_2_[17] = {  1.046115, -0.541249, -1.666698, -2.205100, -1.107439, -0.488619,  1.361032,  1.222887, -1.512666, -2.359013, -1.156205, -3.454303, -8.757607,  1.018103, -1.753633, -8.757607,  1.478810};
	float jaspar_cne_pwm_148_matrix_row_3_[17] = { -0.959290,  0.466290, -2.312909, -2.446262,  0.977085,  1.103610, -2.782163, -1.827881, -2.312909, -2.782163, -3.004808, -3.004808, -1.994795,  0.250888, -1.827881,  1.226838, -2.195264};
	float *jaspar_cne_pwm_148_matrix[4] = { jaspar_cne_pwm_148_matrix_row_0_, jaspar_cne_pwm_148_matrix_row_1_, jaspar_cne_pwm_148_matrix_row_2_, jaspar_cne_pwm_148_matrix_row_3_};
	PWM jaspar_cne_pwm_148_ = {
/* accession        */ "CN0033",
/* name             */ "LM33",
/* label            */ " CN0033	21.1908979508842	LM33	unknown	; consensus \"GWAATTGGAAACAKCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GWAATTGGAAACAGCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_148_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.252251,
/* max_score        */ 19.866337,
/* threshold        */ 0.799,
/* info content     */ 21.182041,
/* base_counts      */ {1872, 686, 1269, 1086},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 289
};

	float jaspar_cne_pwm_149_matrix_row_0_[22] = { -1.140175, -1.521429, -2.655656, -2.655656, -1.634715, -2.288273,  1.101872, -2.550406, -2.655656, -2.773300, -2.655656, -2.145274,  1.207489,  1.174312,  1.228251, -1.521429, -0.793375,  0.553226, -0.883510, -1.270194, -1.298357, -1.595510};
	float jaspar_cne_pwm_149_matrix_row_1_[22] = {  0.912664, -0.158365, -1.616597, -1.476900, -0.923699,  1.319443, -1.393553, -1.779027,  1.236525, -1.667864, -2.047111,  1.393549, -1.667864, -2.127089, -4.602879, -2.214025,  0.719938, -0.569602,  1.018419, -0.741419, -0.899013, -0.205988};
	float jaspar_cne_pwm_149_matrix_row_2_[22] = { -0.741419, -1.667864, -3.224038, -2.127089, -1.839615, -1.839615, -1.721902, -1.904112, -1.779027, -3.914694, -1.779027, -1.973057, -3.001393, -1.476900, -2.214025,  1.371133, -0.949011,  0.254783, -0.828412, -2.047111, -1.616597, -0.720804};
	float jaspar_cne_pwm_149_matrix_row_3_[22] = { -0.083221,  0.929865,  1.198122,  1.171899,  1.078281, -0.902555, -1.486350,  1.176718, -0.411222,  1.207489,  1.183904, -1.718062, -3.242555, -2.773300, -2.773300, -1.762494,  0.183027, -0.982582, -0.447150,  1.034915,  1.037681,  0.853153};
	float *jaspar_cne_pwm_149_matrix[4] = { jaspar_cne_pwm_149_matrix_row_0_, jaspar_cne_pwm_149_matrix_row_1_, jaspar_cne_pwm_149_matrix_row_2_, jaspar_cne_pwm_149_matrix_row_3_};
	PWM jaspar_cne_pwm_149_ = {
/* accession        */ "CN0031",
/* name             */ "LM31",
/* label            */ " CN0031	23.4301510765849	LM31	unknown	; consensus \"CTTTTCATCTTCAAAGYRCTTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTTCATCTTCAAAGYRCTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 22,
/* matrixname       */ jaspar_cne_pwm_149_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -49.366287,
/* max_score        */ 24.108849,
/* threshold        */ 0.863,
/* info content     */ 23.425392,
/* base_counts      */ {2303, 2381, 929, 4463},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 458
};

	float jaspar_cne_pwm_150_matrix_row_0_[17] = { -1.845190, -0.548326, -7.552300, -1.153705, -2.937180, -1.335694, -2.248995, -2.937180, -0.999792,  1.060385,  1.227411, -1.335694, -1.335694,  1.004306, -2.248995, -1.558339, -2.248995};
	float jaspar_cne_pwm_150_matrix_row_1_[17] = { -0.066197, -2.696018, -0.912543,  1.388543, -1.604028, -0.912543, -7.311138, -7.311138, -7.311138, -0.307164, -7.311138, -2.696018,  1.421328, -7.311138, -7.311138,  1.301547, -2.696018};
	float jaspar_cne_pwm_150_matrix_row_2_[17] = { -7.311138, -2.696018,  1.354647, -2.696018, -7.311138, -2.007833, -7.311138,  1.468573,  1.371739, -2.007833, -2.007833,  1.337259, -2.696018, -7.311138,  1.421328, -7.311138, -2.696018};
	float jaspar_cne_pwm_150_matrix_row_3_[17] = {  0.984892,  1.060385, -1.558339, -2.937180,  1.211909,  1.060385,  1.242676, -2.248995, -2.248995, -7.552300, -2.937180, -1.335694, -7.552300, -0.173917, -1.558339, -0.748795,  1.211909};
	float *jaspar_cne_pwm_150_matrix[4] = { jaspar_cne_pwm_150_matrix_row_0_, jaspar_cne_pwm_150_matrix_row_1_, jaspar_cne_pwm_150_matrix_row_2_, jaspar_cne_pwm_150_matrix_row_3_};
	PWM jaspar_cne_pwm_150_ = {
/* accession        */ "CN0159",
/* name             */ "LM159",
/* label            */ " CN0159	23.2126876530612	LM159	unknown	; consensus \"TTGCTTTGGAAGCAGCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGCTTTGGAAGCAGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_150_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -101.490204,
/* max_score        */ 21.129223,
/* threshold        */ 0.817,
/* info content     */ 23.164330,
/* base_counts      */ {226, 220, 311, 399},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 68
};

	float jaspar_cne_pwm_151_matrix_row_0_[15] = { -2.892087, -7.507207, -2.203902, -7.507207,  1.123493,  1.209001, -0.011110, -7.507207, -7.507207,  1.257002,  1.257002,  1.123493,  1.141190, -2.892087,  1.272504};
	float jaspar_cne_pwm_151_matrix_row_1_[15] = { -1.962740, -7.266045, -7.266045,  1.364655, -0.713537, -7.266045, -7.266045, -7.266045,  1.309606, -2.650924, -7.266045, -0.580184, -7.266045,  1.416832, -7.266045};
	float jaspar_cne_pwm_151_matrix_row_2_[15] = { -7.266045, -7.266045, -7.266045, -1.272083, -7.266045, -7.266045,  1.146010, -1.962740, -0.175135, -7.266045, -2.650924, -7.266045, -7.266045, -7.266045, -7.266045};
	float jaspar_cne_pwm_151_matrix_row_3_[15] = {  1.225259,  1.272504,  1.241257, -1.290601, -2.203902, -1.513245, -2.203902,  1.241257, -7.507207, -7.507207, -7.507207, -2.892087, -0.821346, -1.290601, -7.507207};
	float *jaspar_cne_pwm_151_matrix[4] = { jaspar_cne_pwm_151_matrix_row_0_, jaspar_cne_pwm_151_matrix_row_1_, jaspar_cne_pwm_151_matrix_row_2_, jaspar_cne_pwm_151_matrix_row_3_};
	PWM jaspar_cne_pwm_151_ = {
/* accession        */ "CN0205",
/* name             */ "LM205",
/* label            */ " CN0205	23.908000825804	LM205	unknown	; consensus \"TTTCAAGTCAAAACA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTCAAGTCAAAACA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_151_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -110.678818,
/* max_score        */ 18.601068,
/* threshold        */ 0.803,
/* info content     */ 23.842079,
/* base_counts      */ {445, 186, 64, 280},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 65
};

	float jaspar_cne_pwm_152_matrix_row_0_[14] = {  1.272400, -7.303251, -7.303251,  1.272400,  1.272400, -0.299277, -7.303251,  1.193944,  1.272400, -7.303251, -1.596141, -7.303251, -7.303251, -2.688130};
	float jaspar_cne_pwm_152_matrix_row_1_[14] = { -7.062089, -7.062089,  1.513562, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089,  1.494517,  1.327498, -7.062089, -7.062089, -0.258584};
	float jaspar_cne_pwm_152_matrix_row_2_[14] = { -7.062089,  1.513562, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -7.062089, -0.258584};
	float jaspar_cne_pwm_152_matrix_row_3_[14] = { -7.303251, -7.303251, -7.303251, -7.303251, -7.303251,  1.039827,  1.272400, -1.309289, -7.303251, -2.688130, -0.904656,  1.272400,  1.272400,  0.828574};
	float *jaspar_cne_pwm_152_matrix[4] = { jaspar_cne_pwm_152_matrix_row_0_, jaspar_cne_pwm_152_matrix_row_1_, jaspar_cne_pwm_152_matrix_row_2_, jaspar_cne_pwm_152_matrix_row_3_};
	PWM jaspar_cne_pwm_152_ = {
/* accession        */ "CN0080",
/* name             */ "LM80",
/* label            */ " CN0080	24.5412801351407	LM80	unknown	; consensus \"AGCAATTAACCTTY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCAATTAACCTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_152_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.906914,
/* max_score        */ 17.818283,
/* threshold        */ 0.795,
/* info content     */ 24.449442,
/* base_counts      */ {276, 158, 62, 246},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 53
};

	float jaspar_cne_pwm_153_matrix_row_0_[16] = { -0.509456, -1.807225, -3.396301, -2.184500, -0.954136, -4.012316, -4.363558,  1.188469, -1.430760, -0.298203, -2.664037,  0.597659,  0.978000,  1.227493, -2.913951,  0.274729};
	float jaspar_cne_pwm_153_matrix_row_1_[16] = { -1.012899, -4.022363, -1.461523, -2.452142, -1.439704,  1.379626,  1.413585, -2.263258, -2.004703, -5.678570, -3.540707,  0.126332, -2.492547, -3.078192,  1.425541,  0.455044};
	float jaspar_cne_pwm_153_matrix_row_2_[16] = {  0.556285,  1.429427, -3.059846, -3.195952,  1.188210, -2.736496, -5.678570, -2.305102, -1.724979,  1.276670,  1.482881, -1.461523, -0.108904, -2.231000, -3.041831, -1.220894};
	float jaspar_cne_pwm_153_matrix_row_3_[16] = {  0.273711, -2.190470,  1.199631,  1.211667, -0.862788, -0.966384, -1.120239, -2.045782,  1.127028, -4.599807, -4.050043, -0.385870, -1.947184, -3.265297, -1.555919, -0.242048};
	float *jaspar_cne_pwm_153_matrix[4] = { jaspar_cne_pwm_153_matrix_row_0_, jaspar_cne_pwm_153_matrix_row_1_, jaspar_cne_pwm_153_matrix_row_2_, jaspar_cne_pwm_153_matrix_row_3_};
	PWM jaspar_cne_pwm_153_ = {
/* accession        */ "CN0001",
/* name             */ "LM1",
/* label            */ " CN0001	18.7548948672918	LM1	unknown	; consensus \"YKGTTKCCATGGMAACMR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "KGTTGCCATGGAAACN",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_153_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -48.278610,
/* max_score        */ 18.137217,
/* threshold        */ 0.777,
/* info content     */ 18.754524,
/* base_counts      */ {22275, 19110, 22528, 21399},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 5332
};

	float jaspar_cne_pwm_154_matrix_row_0_[19] = { -0.692519, -1.941722, -2.629907, -2.629907, -2.629907, -1.941722, -0.846432, -7.245027, -1.028421, -2.629907,  1.210503,  0.133357,  0.997992, -0.692519, -7.245027,  1.272366,  1.210503,  1.231552, -1.028421};
	float jaspar_cne_pwm_154_matrix_row_1_[19] = {  1.098116, -2.388745, -0.605270, -7.003865, -7.003865, -0.787259,  1.265123, -0.451357, -0.787259,  1.385722, -2.388745, -7.003865, -0.318004,  1.035615,  1.493329, -7.003865, -1.700560, -7.003865, -1.700560};
	float jaspar_cne_pwm_154_matrix_row_2_[19] = { -2.388745, -2.388745, -1.700560, -1.700560,  1.472714,  1.290434, -1.296755, -2.388745, -0.451357, -1.296755, -7.003865,  1.002836, -1.296755, -1.296755, -2.388745, -7.003865, -2.388745, -1.700560, -1.009904};
	float jaspar_cne_pwm_154_matrix_row_3_[19] = { -0.441522,  1.189002,  1.073959,  1.210503, -2.629907, -1.537917, -1.941722,  1.098051,  0.856953, -1.941722, -1.941722, -1.251066, -2.629907, -0.441522, -7.245027, -7.245027, -7.245027, -7.245027,  1.023961};
	float *jaspar_cne_pwm_154_matrix[4] = { jaspar_cne_pwm_154_matrix_row_0_, jaspar_cne_pwm_154_matrix_row_1_, jaspar_cne_pwm_154_matrix_row_2_, jaspar_cne_pwm_154_matrix_row_3_};
	PWM jaspar_cne_pwm_154_ = {
/* accession        */ "CN0177",
/* name             */ "LM177",
/* label            */ " CN0177	23.4922627974238	LM177	unknown	; consensus \"CTTTGGCTTCAGACCAAAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTGGCTTCAGACCAAAT",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_154_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.816978,
/* max_score        */ 22.419235,
/* threshold        */ 0.837,
/* info content     */ 23.429705,
/* base_counts      */ {284, 233, 152, 281},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_155_matrix_row_0_[20] = { -2.188407, -2.188407, -2.876592, -7.491713,  1.191165,  1.138988, -0.688207, -2.188407, -2.188407, -7.491713, -1.784602,  1.064894, -1.275106, -7.491713, -1.497751, -2.188407, -0.487738,  0.058423,  1.174073, -0.805851};
	float jaspar_cne_pwm_155_matrix_row_1_[20] = { -7.250550, -7.250550,  1.325100,  1.449131, -1.256589, -7.250550, -1.543440, -0.851955, -1.543440, -1.947245,  1.397846, -7.250550, -2.635430,  1.449131,  1.266843, -0.564689, -2.635430, -0.698042, -2.635430, -2.635430};
	float jaspar_cne_pwm_155_matrix_row_2_[20] = { -1.256589,  1.465658, -1.256589, -2.635430, -2.635430, -1.033944,  1.266843, -7.250550, -1.256589, -1.543440, -1.947245, -7.250550,  1.397846, -2.635430, -7.250550, -1.256589,  1.226029,  0.992469, -1.256589,  1.343789};
	float jaspar_cne_pwm_155_matrix_row_3_[20] = {  1.174073, -2.876592, -1.093117, -1.784602, -7.491713, -1.784602, -2.188407,  1.138988,  1.120973,  1.191165, -2.188407, -0.400803, -2.876592, -1.784602, -0.582958,  1.025681, -1.497751, -7.491713, -2.876592, -2.876592};
	float *jaspar_cne_pwm_155_matrix[4] = { jaspar_cne_pwm_155_matrix_row_0_, jaspar_cne_pwm_155_matrix_row_1_, jaspar_cne_pwm_155_matrix_row_2_, jaspar_cne_pwm_155_matrix_row_3_};
	PWM jaspar_cne_pwm_155_ = {
/* accession        */ "CN0128",
/* name             */ "LM128",
/* label            */ " CN0128	25.6746722574165	LM128	unknown	; consensus \"TGCCAAGTTTCAGCCTGGAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCCAAGTTTCAGCCTGGAG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_155_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.857300,
/* max_score        */ 24.800682,
/* threshold        */ 0.878,
/* info content     */ 25.622849,
/* base_counts      */ {295, 317, 341, 327},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 64
};

	float jaspar_cne_pwm_156_matrix_row_0_[14] = { -2.629907, -1.941722, -7.245027, -1.941722, -1.941722,  1.167028,  1.210503, -7.245027, -7.245027,  1.189002, -7.245027,  1.189002, -2.629907,  1.210503};
	float jaspar_cne_pwm_156_matrix_row_1_[14] = { -7.003865, -2.388745,  1.513528,  1.408190, -7.003865, -1.296755, -2.388745, -7.003865, -1.700560, -2.388745,  1.493329, -1.700560,  1.408190, -7.003865};
	float jaspar_cne_pwm_156_matrix_row_2_[14] = { -2.388745,  1.385722, -7.003865, -7.003865, -7.003865, -1.700560, -7.003865, -2.388745, -1.296755, -1.296755, -2.388745, -7.003865, -1.700560, -2.388745};
	float jaspar_cne_pwm_156_matrix_row_3_[14] = {  1.231552, -1.537917, -7.245027, -1.537917,  1.231552, -7.245027, -1.941722,  1.252167,  1.167028, -7.245027, -7.245027, -1.941722, -1.941722, -1.941722};
	float *jaspar_cne_pwm_156_matrix[4] = { jaspar_cne_pwm_156_matrix_row_0_, jaspar_cne_pwm_156_matrix_row_1_, jaspar_cne_pwm_156_matrix_row_2_, jaspar_cne_pwm_156_matrix_row_3_};
	PWM jaspar_cne_pwm_156_ = {
/* accession        */ "CN0157",
/* name             */ "LM157",
/* label            */ " CN0157	22.4542695904819	LM157	unknown	; consensus \"TGCCTAATTACACA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCCTAATTACACA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_156_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.512001,
/* max_score        */ 18.057295,
/* threshold        */ 0.789,
/* info content     */ 22.387381,
/* base_counts      */ {239, 199, 58, 204},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_157_matrix_row_0_[14] = { -2.277966, -1.874161,  1.228741,  1.134937, -7.581271, -7.581271,  1.049429, -2.277966, -7.581271, -7.581271, -7.581271, -7.581271, -1.364665, -2.277966};
	float jaspar_cne_pwm_157_matrix_row_1_[14] = {  1.177284, -1.123503, -2.724988, -2.724988, -7.340109, -7.340109, -7.340109, -7.340109, -7.340109, -7.340109,  1.469903,  1.484716, -7.340109, -0.787601};
	float jaspar_cne_pwm_157_matrix_row_2_[14] = { -0.787601, -2.724988, -2.724988, -2.724988, -7.340109,  1.499313, -0.169220, -7.340109, -2.724988, -7.340109, -7.340109, -7.340109, -7.340109,  1.342768};
	float jaspar_cne_pwm_157_matrix_row_3_[14] = { -0.577297,  1.134937, -2.966151, -1.028763,  1.272537, -2.966151, -2.966151,  1.243554,  1.258151,  1.272537, -1.874161, -2.277966,  1.198440, -2.277966};
	float *jaspar_cne_pwm_157_matrix[4] = { jaspar_cne_pwm_157_matrix_row_0_, jaspar_cne_pwm_157_matrix_row_1_, jaspar_cne_pwm_157_matrix_row_2_, jaspar_cne_pwm_157_matrix_row_3_};
	PWM jaspar_cne_pwm_157_ = {
/* accession        */ "CN0161",
/* name             */ "LM161",
/* label            */ " CN0161	22.1779571465156	LM161	unknown	; consensus \"CTAATGATTTCCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTAATGATTTCCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_157_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.480026,
/* max_score        */ 17.767246,
/* threshold        */ 0.788,
/* info content     */ 22.126396,
/* base_counts      */ {198, 199, 152, 431},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 70
};

	float jaspar_cne_pwm_158_matrix_row_0_[15] = {  0.090152, -7.459984, -7.459984, -2.844863,  1.205802,  1.205802, -1.752874, -7.459984, -1.061389, -1.061389,  1.239697,  1.205802,  1.256224, -7.459984, -7.459984};
	float jaspar_cne_pwm_158_matrix_row_1_[15] = { -7.218822, -7.218822,  1.497386, -2.603701, -7.218822, -2.603701, -7.218822, -7.218822, -2.603701,  1.375518, -7.218822, -2.603701, -2.603701, -7.218822, -1.224860};
	float jaspar_cne_pwm_158_matrix_row_2_[15] = { -0.820227,  1.393863, -2.603701, -7.218822, -1.915517, -7.218822, -7.218822, -2.603701,  1.298571, -1.915517, -7.218822, -1.915517, -7.218822, -1.511711,  1.429575};
	float jaspar_cne_pwm_158_matrix_row_3_[15] = {  0.756374, -0.907476, -7.459984,  1.239697, -2.156679, -1.752874,  1.222893,  1.256224, -1.243378, -7.459984, -2.156679, -2.844863, -7.459984,  1.222893, -2.844863};
	float *jaspar_cne_pwm_158_matrix[4] = { jaspar_cne_pwm_158_matrix_row_0_, jaspar_cne_pwm_158_matrix_row_1_, jaspar_cne_pwm_158_matrix_row_2_, jaspar_cne_pwm_158_matrix_row_3_};
	PWM jaspar_cne_pwm_158_ = {
/* accession        */ "CN0029",
/* name             */ "LM29",
/* label            */ " CN0029	23.5017994831121	LM29	unknown	; consensus \"WGCTAATTGCAAATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCTAATTGCAAATG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_158_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.981384,
/* max_score        */ 18.806322,
/* threshold        */ 0.801,
/* info content     */ 23.441053,
/* base_counts      */ {330, 124, 179, 297},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 62
};

	float jaspar_cne_pwm_159_matrix_row_0_[15] = { -0.325193, -7.821290, -2.517985, -3.206170, -2.114180, -7.821290,  1.214816, -1.422695, -1.827329,  1.214816,  1.140717,  1.261330, -1.827329, -1.827329,  0.696103};
	float jaspar_cne_pwm_159_matrix_row_1_[15] = { -1.181533,  1.407194, -7.580128, -1.586167, -7.580128, -7.580128, -7.580128, -2.276823, -2.965008, -2.276823, -1.181533, -7.580128, -2.965008,  1.419615, -0.409240};
	float jaspar_cne_pwm_159_matrix_row_2_[15] = {  0.116539, -1.181533, -7.580128,  1.368977, -2.276823,  1.513791, -7.580128, -2.276823, -7.580128, -2.965008, -1.873018, -7.580128,  1.381879, -2.276823, -7.580128};
	float jaspar_cne_pwm_159_matrix_row_3_[15] = {  0.545313, -2.114180,  1.249903, -1.268782,  1.214816, -7.821290, -1.604684,  1.153454,  1.214816, -2.517985, -2.517985, -3.206170, -1.422695, -2.517985,  0.042361};
	float *jaspar_cne_pwm_159_matrix[4] = { jaspar_cne_pwm_159_matrix_row_0_, jaspar_cne_pwm_159_matrix_row_1_, jaspar_cne_pwm_159_matrix_row_2_, jaspar_cne_pwm_159_matrix_row_3_};
	PWM jaspar_cne_pwm_159_ = {
/* accession        */ "CN0153",
/* name             */ "LM153",
/* label            */ " CN0153	21.2409786898481	LM153	unknown	; consensus \"NCTGTGATTAAAGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NCTGTGATTAAAGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_159_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -78.753860,
/* max_score        */ 17.997540,
/* threshold        */ 0.790,
/* info content     */ 21.206400,
/* base_counts      */ {426, 196, 282, 431},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 89
};

	float jaspar_cne_pwm_160_matrix_row_0_[20] = {  0.311351, -2.248995, -7.552300, -1.153705, -0.056203,  0.272146, -7.552300, -2.937180, -7.552300, -7.552300,  0.311351,  1.242676,  1.130577,  1.272525, -7.552300, -7.552300,  0.097868,  0.272146,  1.272525,  0.837287};
	float jaspar_cne_pwm_160_matrix_row_1_[20] = { -7.311138, -7.311138, -7.311138, -0.912543,  1.186056,  1.055465, -7.311138, -0.912543, -7.311138, -1.317177,  0.513308, -2.696018, -2.696018, -7.311138, -7.311138, -7.311138, -7.311138,  1.055465, -7.311138, -1.094532};
	float jaspar_cne_pwm_160_matrix_row_2_[20] = { -0.912543, -7.311138, -0.140250, -7.311138, -7.311138, -7.311138, -7.311138, -7.311138, -7.311138,  1.301547,  0.127833, -7.311138, -0.625277, -7.311138,  0.339031,  0.790842,  0.067245, -7.311138, -7.311138, -1.317177};
	float jaspar_cne_pwm_160_matrix_row_3_[20] = {  0.636667,  1.242676,  1.060385,  1.078400, -2.937180, -7.552300,  1.272525,  1.163908,  1.272525, -0.748795, -7.552300, -2.937180, -7.552300, -7.552300,  0.903230,  0.608504,  0.487180, -7.552300, -7.552300, -0.238413};
	float *jaspar_cne_pwm_160_matrix[4] = { jaspar_cne_pwm_160_matrix_row_0_, jaspar_cne_pwm_160_matrix_row_1_, jaspar_cne_pwm_160_matrix_row_2_, jaspar_cne_pwm_160_matrix_row_3_};
	PWM jaspar_cne_pwm_160_ = {
/* accession        */ "CN0192",
/* name             */ "LM192",
/* label            */ " CN0192	25.4423882390788	LM192	unknown	; consensus \"WTKWMMTTTKVAAAKKWMAW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTTCCTTTGMAAATTWCAA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_160_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -143.122772,
/* max_score        */ 20.775763,
/* threshold        */ 0.875,
/* info content     */ 25.367962,
/* base_counts      */ {455, 183, 173, 549},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 68
};

	float jaspar_cne_pwm_161_matrix_row_0_[14] = { -7.284217,  1.233176, -1.067611,  1.058861, -1.980912, -7.284217, -7.284217,  1.212977, -1.980912,  1.272389, -7.284217, -7.284217, -7.284217, -2.669097};
	float jaspar_cne_pwm_161_matrix_row_1_[14] = {  1.513551, -7.043055, -1.335945, -2.427935, -1.739750, -7.043055, -0.239550, -7.043055,  1.474338, -7.043055, -2.427935, -7.043055, -7.043055,  1.454139};
	float jaspar_cne_pwm_161_matrix_row_2_[14] = { -7.043055, -7.043055,  1.199964, -1.335945, -1.049094, -7.043055,  1.323548, -1.335945, -7.043055, -7.043055, -7.043055, -7.043055, -7.043055, -1.739750};
	float jaspar_cne_pwm_161_matrix_row_3_[14] = { -7.284217, -1.980912, -0.885622, -0.885622,  1.105370,  1.272389, -7.284217, -7.284217, -7.284217, -7.284217,  1.252975,  1.272389,  1.272389, -7.284217};
	float *jaspar_cne_pwm_161_matrix[4] = { jaspar_cne_pwm_161_matrix_row_0_, jaspar_cne_pwm_161_matrix_row_1_, jaspar_cne_pwm_161_matrix_row_2_, jaspar_cne_pwm_161_matrix_row_3_};
	PWM jaspar_cne_pwm_161_ = {
/* accession        */ "CN0148",
/* name             */ "LM148",
/* label            */ " CN0148	22.9804080575699	LM148	unknown	; consensus \"CAGATTGACATTTC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAGATTGACATTTC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_161_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.630020,
/* max_score        */ 17.918455,
/* threshold        */ 0.790,
/* info content     */ 22.901863,
/* base_counts      */ {203, 167, 93, 265},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_162_matrix_row_0_[18] = { -0.169376, -1.346303, -0.169376,  1.049322,  1.177128, -2.725144,  0.309904, -1.346303, -7.340265,  1.235386,  1.272420,  1.254075,  1.272420, -7.340265, -2.036960, -2.725144, -2.036960, -1.633154};
	float jaspar_cne_pwm_162_matrix_row_1_[18] = { -1.391992, -1.391992,  0.279281, -1.105141, -7.099102, -2.483982,  0.551066, -7.099102, -1.105141, -1.795798, -7.099102, -7.099102, -7.099102, -1.391992, -7.099102, -7.099102,  1.457504, -1.105141};
	float jaspar_cne_pwm_162_matrix_row_2_[18] = { -1.105141, -0.295597,  0.339869, -0.700508, -7.099102,  1.312952, -2.483982, -2.483982,  1.438089, -7.099102, -7.099102, -7.099102, -7.099102, -0.095129, -7.099102,  1.495237, -7.099102, -7.099102};
	float jaspar_cne_pwm_162_matrix_row_3_[18] = {  0.820539,  0.928723, -0.536759, -2.725144, -1.123659, -0.654404, -0.249355,  1.177128, -7.340265, -7.340265, -7.340265, -2.725144, -7.340265,  0.978721,  1.235386, -7.340265, -2.725144,  1.136315};
	float *jaspar_cne_pwm_162_matrix[4] = { jaspar_cne_pwm_162_matrix_row_0_, jaspar_cne_pwm_162_matrix_row_1_, jaspar_cne_pwm_162_matrix_row_2_, jaspar_cne_pwm_162_matrix_row_3_};
	PWM jaspar_cne_pwm_162_ = {
/* accession        */ "CN0116",
/* name             */ "LM116",
/* label            */ " CN0116	23.7887526952949	LM116	unknown	; consensus \"WWNAAGHTGAAAATTGCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTNAAGMTGAAAATTGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_162_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.891220,
/* max_score        */ 20.132282,
/* threshold        */ 0.831,
/* info content     */ 23.718103,
/* base_counts      */ {375, 113, 199, 303},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 55
};

	float jaspar_cne_pwm_163_matrix_row_0_[18] = { -0.299783, -0.592092, -1.451088, -3.094497, -3.107919, -0.936671, -3.627702, -3.386589,  0.965585, -0.470919, -1.042201, -1.969658,  0.599571,  1.061543,  1.238907, -2.506770,  0.698311,  0.487409};
	float jaspar_cne_pwm_163_matrix_row_1_[18] = {  0.263307, -0.784061, -2.716493, -1.044066, -1.238685, -0.598660,  1.429292,  1.362899, -0.701691, -1.437583, -4.772153, -2.295683,  0.393287, -3.181787, -2.740021,  1.428924,  0.262125, -0.704796};
	float jaspar_cne_pwm_163_matrix_row_2_[18] = { -0.300893,  0.740737,  1.388531, -3.043661, -1.934462,  0.478264, -3.912476, -4.867372, -5.223619, -1.434352,  1.395028,  1.437905, -3.874750, -0.611327, -3.110341, -2.671036, -2.951302,  0.461449};
	float jaspar_cne_pwm_163_matrix_row_3_[18] = {  0.204170,  0.011456, -2.000428,  1.166924,  1.158617,  0.392137, -1.393501, -0.773538, -0.595372,  0.945143, -3.177867, -3.135314, -0.563042, -1.506730, -3.386589, -1.859120, -0.695512, -1.175730};
	float *jaspar_cne_pwm_163_matrix[4] = { jaspar_cne_pwm_163_matrix_row_0_, jaspar_cne_pwm_163_matrix_row_1_, jaspar_cne_pwm_163_matrix_row_2_, jaspar_cne_pwm_163_matrix_row_3_};
	PWM jaspar_cne_pwm_163_ = {
/* accession        */ "CN0010",
/* name             */ "LM10",
/* label            */ " CN0010	17.756621239845	LM10	unknown	; consensus \"YKGTTTCCATGGAAACMR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NNGTTKCCATGGAAACAR",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_163_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -51.530243,
/* max_score        */ 18.246897,
/* threshold        */ 0.766,
/* info content     */ 17.756308,
/* base_counts      */ {29306, 25367, 25353, 26426},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 5914
};

	float jaspar_cne_pwm_164_matrix_row_0_[23] = { -0.457217,  1.272586,  1.272586,  1.207212, -2.398854, -3.087038, -3.087038, -2.398854, -7.702158, -7.702158, -3.087038, -7.702158, -3.087038, -0.457217,  1.272586, -3.087038,  1.046305, -2.398854, -7.702158, -3.087038,  1.122666, -1.708197, -7.702158};
	float jaspar_cne_pwm_164_matrix_row_1_[23] = {  1.221881, -7.460997, -7.460997, -1.244390, -7.460997, -7.460997,  1.475039, -0.216055,  1.513748, -7.460997, -7.460997, -1.753886, -7.460997,  1.238685, -7.460997, -2.845876, -7.460997, -7.460997,  1.513748,  1.501011, -1.467035,  1.378425,  1.133343};
	float jaspar_cne_pwm_164_matrix_row_2_[23] = { -7.460997, -7.460997, -7.460997, -7.460997,  1.475039,  1.475039, -2.845876, -7.460997, -7.460997, -7.460997, -1.753886, -7.460997, -2.845876, -7.460997, -7.460997,  1.488109, -0.082613,  1.488109, -7.460997, -7.460997, -0.908489, -7.460997, -1.753886};
	float jaspar_cne_pwm_164_matrix_row_3_[23] = { -1.303564, -7.702158, -7.702158, -7.702158, -3.087038, -2.398854, -3.087038,  1.046305, -7.702158,  1.272586,  1.220633,  1.233876,  1.246947, -1.485552, -7.702158, -7.702158, -7.702158, -7.702158, -7.702158, -7.702158, -7.702158, -1.303564, -0.005491};
	float *jaspar_cne_pwm_164_matrix[4] = { jaspar_cne_pwm_164_matrix_row_0_, jaspar_cne_pwm_164_matrix_row_1_, jaspar_cne_pwm_164_matrix_row_2_, jaspar_cne_pwm_164_matrix_row_3_};
	PWM jaspar_cne_pwm_164_ = {
/* accession        */ "CN0006",
/* name             */ "LM6",
/* label            */ " CN0006	37.556203116516	LM6	unknown	; consensus \"CAAAGGCTCTTTTMAGAGCCACY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAAAGGCTCTTTTCAGAGCCACC",
/* library_name     */ "jaspar_cne",
/* length           */ 23,
/* matrixname       */ jaspar_cne_pwm_164_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -170.605255,
/* max_score        */ 30.116467,
/* threshold        */ 1.159,
/* info content     */ 37.470909,
/* base_counts      */ {486, 581, 337, 413},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 79
};

	float jaspar_cne_pwm_165_matrix_row_0_[14] = { -6.944203, -6.944203, -6.944203, -6.944203, -6.944203,  1.272155,  0.839438,  1.272155,  1.127015, -6.944203, -6.944203,  1.272155,  1.272155, -0.545608};
	float jaspar_cne_pwm_165_matrix_row_1_[14] = {  1.513317, -6.703041, -6.703041, -6.703041,  1.513317, -6.703041, -6.703041, -6.703041, -6.703041, -6.703041,  1.303660, -6.703041, -6.703041, -6.703041};
	float jaspar_cne_pwm_165_matrix_row_2_[14] = { -6.703041, -6.703041,  1.485926, -6.703041, -6.703041, -6.703041,  0.467848, -6.703041, -0.486435, -6.703041, -0.150533, -6.703041, -6.703041, -2.087920};
	float jaspar_cne_pwm_165_matrix_row_3_[14] = { -6.944203,  1.272155, -2.329082,  1.272155, -6.944203, -6.944203, -6.944203, -6.944203, -6.944203,  1.272155, -6.944203, -6.944203, -6.944203,  1.062498};
	float *jaspar_cne_pwm_165_matrix[4] = { jaspar_cne_pwm_165_matrix_row_0_, jaspar_cne_pwm_165_matrix_row_1_, jaspar_cne_pwm_165_matrix_row_2_, jaspar_cne_pwm_165_matrix_row_3_};
	PWM jaspar_cne_pwm_165_ = {
/* accession        */ "CN0083",
/* name             */ "LM83",
/* label            */ " CN0083	24.8026372819834	LM83	unknown	; consensus \"CTGTCAAAATCAAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGTCAAAATCAAT",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_165_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.977692,
/* max_score        */ 17.750259,
/* threshold        */ 0.796,
/* info content     */ 24.671099,
/* base_counts      */ {210, 104, 62, 142},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 37
};

	float jaspar_cne_pwm_166_matrix_row_0_[20] = { -0.826246, -7.224841, -0.826246, -7.224841,  1.141762, -7.224841, -1.921536,  0.814639, -0.133931,  1.164746,  1.094145, -0.826246,  1.069459, -2.609720, -0.826246,  1.069459,  1.187214, -1.921536, -0.826246,  1.187214};
	float jaspar_cne_pwm_166_matrix_row_1_[20] = { -2.368558, -6.983679, -1.680374,  1.285309, -0.989717, -6.983679, -0.767073, -1.680374, -2.368558, -6.983679, -1.680374, -1.276569, -6.983679, -6.983679, -6.983679, -1.276569, -6.983679, -6.983679,  1.310621, -6.983679};
	float jaspar_cne_pwm_166_matrix_row_2_[20] = { -6.983679, -1.680374, -6.983679, -0.297818, -6.983679, -0.989717, -1.276569, -0.431171,  1.177125, -1.680374, -0.767073,  1.023022, -0.180173, -1.680374,  1.055801, -2.368558, -6.983679,  1.118302, -2.368558, -6.983679};
	float jaspar_cne_pwm_166_matrix_row_3_[20] = {  1.118237,  1.230690,  1.094145, -1.921536, -1.921536,  1.187214,  1.044147, -0.421336, -2.609720, -1.517731, -2.609720, -0.316086, -7.224841,  1.209188, -0.133931, -1.008235, -1.230879,  0.020101, -1.921536, -1.230879};
	float *jaspar_cne_pwm_166_matrix[4] = { jaspar_cne_pwm_166_matrix_row_0_, jaspar_cne_pwm_166_matrix_row_1_, jaspar_cne_pwm_166_matrix_row_2_, jaspar_cne_pwm_166_matrix_row_3_};
	PWM jaspar_cne_pwm_166_ = {
/* accession        */ "CN0217",
/* name             */ "LM217",
/* label            */ " CN0217	23.7427223366843	LM217	unknown	; consensus \"TTTCATTAGAAGATGAAGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTCATTAGAAGATGAAGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_166_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -106.587524,
/* max_score        */ 22.582438,
/* threshold        */ 0.850,
/* info content     */ 23.670967,
/* base_counts      */ {376, 102, 173, 329},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_167_matrix_row_0_[19] = {  1.170285, -2.994306,  0.984913, -7.609426, -2.994306, -7.609426, -2.994306,  1.258565, -2.306121, -7.609426,  1.258565,  0.215020,  1.215399, -1.210831, -1.210831, -0.700671, -0.364484, -2.994306,  1.139037};
	float jaspar_cne_pwm_167_matrix_row_1_[19] = { -7.368264, -7.368264, -7.368264, -7.368264, -7.368264, -7.368264,  1.485544, -7.368264, -2.753144,  1.456561, -2.753144, -1.151658, -1.661154, -1.374303, -0.054377,  0.792540,  1.108315,  1.426713, -7.368264};
	float jaspar_cne_pwm_167_matrix_row_2_[19] = { -1.374303,  1.499727,  0.127833,  1.411447,  1.499727,  1.513711, -7.368264, -2.753144,  1.471158, -2.064959, -7.368264,  0.950722, -7.368264,  1.297522,  0.638437, -0.123322, -2.753144, -7.368264, -0.969669};
	float jaspar_cne_pwm_167_matrix_row_3_[19] = { -1.902316, -7.609426, -7.609426, -1.056918, -7.609426, -7.609426, -2.994306, -7.609426, -7.609426, -2.306121, -7.609426, -2.994306, -2.994306, -1.615465,  0.040743, -0.438538, -0.805921, -1.392820, -1.902316};
	float *jaspar_cne_pwm_167_matrix[4] = { jaspar_cne_pwm_167_matrix_row_0_, jaspar_cne_pwm_167_matrix_row_1_, jaspar_cne_pwm_167_matrix_row_2_, jaspar_cne_pwm_167_matrix_row_3_};
	PWM jaspar_cne_pwm_167_ = {
/* accession        */ "CN0194",
/* name             */ "LM194",
/* label            */ " CN0194	26.1147660982807	LM194	unknown	; consensus \"AGAGGGCAGCARAGYCCCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGAGGGCAGCAGAGNNCCA",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_167_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -114.600571,
/* max_score        */ 23.578888,
/* threshold        */ 0.870,
/* info content     */ 26.055983,
/* base_counts      */ {459, 316, 523, 70},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 72
};

	float jaspar_cne_pwm_168_matrix_row_0_[21] = {  0.922470, -3.194875,  0.666584, -2.102885,  1.164749,  1.261197, -2.506691, -7.809996,  1.202016, -1.411401, -0.259860,  1.226110,  1.214136, -0.806022, -0.496109, -1.411401,  1.189747,  1.272625,  1.164749, -3.194875, -1.593390};
	float jaspar_cne_pwm_168_matrix_row_1_[21] = { -0.882973,  1.163632,  0.081335, -2.953713, -1.170239, -7.568834, -7.568834, -7.568834, -7.568834, -2.953713, -2.265529, -2.953713, -2.953713, -2.953713, -1.574872, -0.564860, -1.861723, -7.568834, -2.265529, -1.016326,  1.079563};
	float jaspar_cne_pwm_168_matrix_row_2_[21] = { -0.190450, -2.265529, -0.397945, -7.568834, -7.568834, -7.568834, -7.568834, -7.568834, -1.861723,  1.418488,  1.195376, -1.861723, -1.574872,  1.353958,  1.241178, -1.170239, -1.861723, -7.568834, -1.352228, -2.265529, -0.882973};
	float jaspar_cne_pwm_168_matrix_row_3_[21] = { -2.506691, -0.068897, -1.411401,  1.226110, -2.102885, -3.194875,  1.249638,  1.272625, -2.102885, -3.194875, -2.102885, -7.809996, -7.809996, -3.194875, -2.506691,  0.969715, -3.194875, -7.809996, -2.506691,  1.152012, -0.313898};
	float *jaspar_cne_pwm_168_matrix[4] = { jaspar_cne_pwm_168_matrix_row_0_, jaspar_cne_pwm_168_matrix_row_1_, jaspar_cne_pwm_168_matrix_row_2_, jaspar_cne_pwm_168_matrix_row_3_};
	PWM jaspar_cne_pwm_168_ = {
/* accession        */ "CN0052",
/* name             */ "LM52",
/* label            */ " CN0052	28.0018706101454	LM52	unknown	; consensus \"RYVTAATTAGGAAGGTAAATM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACATAATTAGGAAGGTAAATC",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_168_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -99.260330,
/* max_score        */ 24.606678,
/* threshold        */ 0.930,
/* info content     */ 27.957531,
/* base_counts      */ {842, 188, 351, 467},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 88
};

	float jaspar_cne_pwm_169_matrix_row_0_[21] = { -1.204999,  0.388322, -0.244063,  1.134631, -2.159281,  1.239985,  1.212958, -1.823380, -1.371913,  0.058141,  1.226562, -1.284978,  1.178113, -0.438156, -3.760767, -2.381926, -1.371913,  0.920722, -2.159281, -0.997504, -0.106899};
	float jaspar_cne_pwm_169_matrix_row_1_[21] = {  0.254861, -8.134726, -2.831421, -2.427615, -2.140764, -3.519605, -3.519605, -2.140764, -0.196994, -1.918119, -8.134726, -1.918119, -2.427615, -1.736131, -0.533323, -2.427615, -1.918119, -3.519605,  1.360869, -1.331220, -1.448865};
	float jaspar_cne_pwm_169_matrix_row_2_[21] = { -1.448865,  0.913214,  1.224121, -1.130751, -2.140764, -2.831421, -2.427615, -2.427615, -1.582218,  1.095515, -1.918119,  1.375793, -1.736131,  1.266318, -2.140764, -2.427615,  1.390499,  0.208352, -2.427615, -2.427615,  1.085664};
	float jaspar_cne_pwm_169_matrix_row_3_[21] = {  0.729203, -1.977293, -2.668777, -1.977293,  1.185180, -3.072583, -2.159281,  1.178113,  0.920722, -3.072583, -3.072583, -2.668777, -2.159281, -8.375888,  1.096894,  1.206085, -3.072583, -2.668777, -1.130946,  1.073548, -1.823380};
	float *jaspar_cne_pwm_169_matrix[4] = { jaspar_cne_pwm_169_matrix_row_0_, jaspar_cne_pwm_169_matrix_row_1_, jaspar_cne_pwm_169_matrix_row_2_, jaspar_cne_pwm_169_matrix_row_3_};
	PWM jaspar_cne_pwm_169_ = {
/* accession        */ "CN0077",
/* name             */ "LM77",
/* label            */ " CN0077	24.9424396113144	LM77	unknown	; consensus \"YRGATAATTGAGAGTTGACTR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGGATAATTGAGAGTTGACTG",
/* library_name     */ "jaspar_cne",
/* length           */ 21,
/* matrixname       */ jaspar_cne_pwm_169_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.189720,
/* max_score        */ 24.014711,
/* threshold        */ 0.879,
/* info content     */ 24.925095,
/* base_counts      */ {1125, 285, 901, 944},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 155
};

	float jaspar_cne_pwm_170_matrix_row_0_[15] = { -3.008091, -7.623212, -3.008091,  1.258763,  1.156500, -7.623212, -3.008091,  0.695774, -0.714457,  1.258763,  1.244779,  1.230597, -7.623212, -2.319907,  1.258763};
	float jaspar_cne_pwm_170_matrix_row_1_[15] = { -7.382050,  1.485941, -2.766929, -7.382050, -7.382050, -2.766929, -7.382050, -2.766929,  1.317632, -7.382050, -2.766929, -2.766929, -2.766929,  0.689169, -2.766929};
	float jaspar_cne_pwm_170_matrix_row_2_[15] = {  1.427962, -2.078745, -7.382050, -2.766929, -7.382050, -2.766929, -7.382050,  0.624651, -2.078745, -7.382050, -7.382050, -2.766929, -7.382050,  0.806917, -7.382050};
	float jaspar_cne_pwm_170_matrix_row_3_[15] = { -1.406606, -7.623212,  1.244779, -7.623212, -0.937351,  1.244779,  1.258763, -3.008091, -3.008091, -3.008091, -3.008091, -3.008091,  1.258763, -1.916102, -7.623212};
	float *jaspar_cne_pwm_170_matrix[4] = { jaspar_cne_pwm_170_matrix_row_0_, jaspar_cne_pwm_170_matrix_row_1_, jaspar_cne_pwm_170_matrix_row_2_, jaspar_cne_pwm_170_matrix_row_3_};
	PWM jaspar_cne_pwm_170_ = {
/* accession        */ "CN0125",
/* name             */ "LM125",
/* label            */ " CN0125	24.0240531513715	LM125	unknown	; consensus \"GCTAATTRCAAATSA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCTAATTACAAATSA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_170_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.752548,
/* max_score        */ 18.149479,
/* threshold        */ 0.804,
/* info content     */ 23.972069,
/* base_counts      */ {478, 170, 140, 307},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 73
};

	float jaspar_cne_pwm_171_matrix_row_0_[14] = { -7.522465, -7.522465,  1.272511, -7.522465, -7.522465,  1.272511,  1.225998, -0.718960, -2.219160, -7.522465, -7.522465,  1.053186,  1.108235,  1.210001};
	float jaspar_cne_pwm_171_matrix_row_1_[14] = { -7.281303, -7.281303, -7.281303, -7.281303, -7.281303, -7.281303, -1.574193, -1.287342, -7.281303, -1.064697,  1.513673, -0.110415, -1.287342, -7.281303};
	float jaspar_cne_pwm_171_matrix_row_2_[14] = { -1.977998,  1.513673, -7.281303, -7.281303, -0.882708, -7.281303, -7.281303, -7.281303, -2.666183,  1.434905, -7.281303, -7.281303, -0.882708, -1.287342};
	float jaspar_cne_pwm_171_matrix_row_3_[14] = {  1.241744, -7.522465, -7.522465,  1.272511,  1.177216, -7.522465, -7.522465,  1.053186,  1.225998, -7.522465, -7.522465, -7.522465, -7.522465, -7.522465};
	float *jaspar_cne_pwm_171_matrix[4] = { jaspar_cne_pwm_171_matrix_row_0_, jaspar_cne_pwm_171_matrix_row_1_, jaspar_cne_pwm_171_matrix_row_2_, jaspar_cne_pwm_171_matrix_row_3_};
	PWM jaspar_cne_pwm_171_ = {
/* accession        */ "CN0071",
/* name             */ "LM71",
/* label            */ " CN0071	23.7046410785893	LM71	unknown	; consensus \"TGATTAATTGCAAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGATTAATTGCAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_171_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.832207,
/* max_score        */ 17.575350,
/* threshold        */ 0.793,
/* info content     */ 23.635483,
/* base_counts      */ {377, 95, 146, 306},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 66
};

	float jaspar_cne_pwm_172_matrix_row_0_[17] = { -2.629907, -1.941722, -1.941722, -2.629907, -7.245027, -1.537917, -1.941722, -7.245027, -7.245027, -0.336272,  1.189002, -1.941722, -7.245027, -1.028421, -7.245027,  1.252167, -1.537917};
	float jaspar_cne_pwm_172_matrix_row_1_[17] = { -1.700560,  1.339213, -1.296755, -7.003865, -7.003865, -2.388745,  1.315121, -7.003865, -1.009904,  1.185102, -2.388745, -7.003865, -2.388745, -1.009904,  1.493329, -2.388745, -1.009904};
	float jaspar_cne_pwm_172_matrix_row_2_[17] = {  1.185102, -7.003865, -7.003865, -0.095110, -1.700560,  1.385722, -2.388745, -7.003865, -2.388745, -2.388745, -7.003865, -1.296755, -7.003865, -2.388745, -7.003865, -7.003865,  1.290434};
	float jaspar_cne_pwm_172_matrix_row_3_[17] = { -0.241053, -0.846432,  1.167028,  1.023961,  1.231552, -1.941722, -0.846432,  1.272366,  1.167028, -1.537917, -1.537917,  1.167028,  1.252167,  1.049272, -2.629907, -7.245027, -1.537917};
	float *jaspar_cne_pwm_172_matrix[4] = { jaspar_cne_pwm_172_matrix_row_0_, jaspar_cne_pwm_172_matrix_row_1_, jaspar_cne_pwm_172_matrix_row_2_, jaspar_cne_pwm_172_matrix_row_3_};
	PWM jaspar_cne_pwm_172_ = {
/* accession        */ "CN0185",
/* name             */ "LM185",
/* label            */ " CN0185	23.6543363076089	LM185	unknown	; consensus \"GCTTTGCTTCATTTCAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCTTTGCTTCATTTCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_172_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.212288,
/* max_score        */ 20.965591,
/* threshold        */ 0.820,
/* info content     */ 23.585285,
/* base_counts      */ {126, 189, 139, 396},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_173_matrix_row_0_[14] = { -0.033350, -0.200264, -7.204238, -7.204238, -7.204238, -7.204238, -7.204238, -7.204238,  1.272341, -7.204238, -7.204238,  1.272341,  1.272341, -2.589118};
	float jaspar_cne_pwm_173_matrix_row_1_[14] = { -2.347956, -6.963077, -6.963077, -6.963077, -6.963077,  1.448978, -6.963077,  1.513503, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077,  0.207812};
	float jaspar_cne_pwm_173_matrix_row_2_[14] = { -0.969115,  0.587059,  1.426511, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077, -6.963077,  1.168748};
	float jaspar_cne_pwm_173_matrix_row_3_[14] = {  0.802462,  0.291859, -1.210277,  1.272341,  1.272341, -1.497128,  1.272341, -7.204238, -7.204238,  1.272341,  1.272341, -7.204238, -7.204238, -7.204238};
	float *jaspar_cne_pwm_173_matrix[4] = { jaspar_cne_pwm_173_matrix_row_0_, jaspar_cne_pwm_173_matrix_row_1_, jaspar_cne_pwm_173_matrix_row_2_, jaspar_cne_pwm_173_matrix_row_3_};
	PWM jaspar_cne_pwm_173_ = {
/* accession        */ "CN0132",
/* name             */ "LM132",
/* label            */ " CN0132	23.3734828252666	LM132	unknown	; consensus \"TRGTTCTCATTAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TKGTTCTCATTAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_173_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.761894,
/* max_score        */ 17.125988,
/* threshold        */ 0.792,
/* info content     */ 23.276680,
/* base_counts      */ {169, 107, 101, 295},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_174_matrix_row_0_[17] = { -0.246771, -2.188407,  1.256751, -2.876592, -7.491713, -2.876592,  0.610268, -7.491713,  0.446019, -7.491713, -0.939205,  1.272497,  1.272497,  1.207969, -7.491713, -1.093117,  0.669091};
	float jaspar_cne_pwm_174_matrix_row_1_[17] = { -0.698042,  1.068436, -7.250550, -7.250550, -7.250550, -2.635430,  0.127833,  1.513659, -7.250550, -7.250550, -1.256589, -7.250550, -7.250550, -7.250550, -7.250550,  0.490549, -0.851955};
	float jaspar_cne_pwm_174_matrix_row_2_[17] = { -1.947245,  0.350852, -7.250550, -1.947245, -7.250550, -7.250550, -1.543440, -7.250550, -7.250550,  1.513659,  0.063336, -7.250550, -7.250550, -7.250550, -7.250550,  0.910253, -1.947245};
	float jaspar_cne_pwm_174_matrix_row_3_[17] = {  0.827274, -2.876592, -2.876592,  1.224496,  1.272497,  1.240753, -0.400803, -7.491713,  0.697254, -7.491713,  0.751307, -7.491713, -7.491713, -1.497751,  1.272497, -7.491713,  0.158456};
	float *jaspar_cne_pwm_174_matrix[4] = { jaspar_cne_pwm_174_matrix_row_0_, jaspar_cne_pwm_174_matrix_row_1_, jaspar_cne_pwm_174_matrix_row_2_, jaspar_cne_pwm_174_matrix_row_3_};
	PWM jaspar_cne_pwm_174_ = {
/* accession        */ "CN0118",
/* name             */ "LM118",
/* label            */ " CN0118	23.4315347136407	LM118	unknown	; consensus \"WSATTTWCWGKAAATSW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCATTTACTGTAAATGA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_174_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.265854,
/* max_score        */ 18.581161,
/* threshold        */ 0.818,
/* info content     */ 23.363747,
/* base_counts      */ {378, 162, 143, 405},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 64
};

	float jaspar_cne_pwm_175_matrix_row_0_[16] = {  0.926563,  0.703437, -2.661448, -1.005241, -1.851904, -4.040289, -8.655410, -4.040289, -4.040289,  1.263016,  1.267930, -0.615930, -2.438803, -2.438803, -0.830964,  0.631984};
	float jaspar_cne_pwm_175_matrix_row_1_[16] = { -0.673148, -8.414248, -3.110943,  1.160805,  1.427418, -1.861740, -8.414248, -8.414248, -1.169306, -8.414248, -8.414248, -1.035864, -1.861740, -0.589802,  1.088837, -0.374768};
	float jaspar_cne_pwm_175_matrix_row_2_[16] = { -0.918150, -1.610742,  1.484278, -0.630607, -8.414248, -8.414248, -8.414248, -8.414248,  1.437999, -3.799127, -8.414248,  1.253581, -1.505493, -0.047645, -0.550596, -1.728387};
	float jaspar_cne_pwm_175_matrix_row_3_[16] = { -1.105274,  0.331912, -8.655410, -1.277026, -1.969549,  1.233015,  1.272819,  1.267930, -8.655410, -4.040289, -4.040289, -8.655410,  1.159301,  0.832638, -1.054007,  0.010376};
	float *jaspar_cne_pwm_175_matrix[4] = { jaspar_cne_pwm_175_matrix_row_0_, jaspar_cne_pwm_175_matrix_row_1_, jaspar_cne_pwm_175_matrix_row_2_, jaspar_cne_pwm_175_matrix_row_3_};
	PWM jaspar_cne_pwm_175_ = {
/* accession        */ "CN0074",
/* name             */ "LM74",
/* label            */ " CN0074	20.4756921397386	LM74	unknown	; consensus \"AWGCCTTTGAAGTTMW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAGCCTTTGAAGTTCA",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_175_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.149422,
/* max_score        */ 18.411552,
/* threshold        */ 0.789,
/* info content     */ 20.459497,
/* base_counts      */ {879, 591, 686, 1124},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 205
};

	float jaspar_cne_pwm_176_matrix_row_0_[15] = {  0.200311,  1.077353, -4.439140, -4.439140,  1.190680,  1.269622, -1.526087,  0.411840,  1.230929, -2.246348, -2.192310, -4.439140,  1.258003, -1.225052,  0.195492};
	float jaspar_cne_pwm_176_matrix_row_1_[15] = {  0.932862, -1.067254, -9.501283, -9.501283, -1.461803, -9.501283, -4.886163, -2.122899, -3.794173, -9.501283, -2.948775,  1.478929, -3.507322, -2.122899, -0.100240};
	float jaspar_cne_pwm_176_matrix_row_2_[15] = { -4.197978, -3.794173, -4.886163,  1.507478, -4.886163, -9.501283, -4.886163, -3.507322, -9.501283, -4.197978, -0.983890, -2.330395, -9.501283, -0.125344,  0.567761};
	float jaspar_cne_pwm_176_matrix_row_3_[15] = { -1.076660, -1.059568,  1.267970, -4.439140, -2.364061, -4.439140,  1.206623,  0.664874, -2.045778,  1.239469,  1.139387, -3.343850, -3.525839,  0.912482, -1.352858};
	float *jaspar_cne_pwm_176_matrix[4] = { jaspar_cne_pwm_176_matrix_row_0_, jaspar_cne_pwm_176_matrix_row_1_, jaspar_cne_pwm_176_matrix_row_2_, jaspar_cne_pwm_176_matrix_row_3_};
	PWM jaspar_cne_pwm_176_ = {
/* accession        */ "CN0008",
/* name             */ "LM8",
/* label            */ " CN0008	20.9642817346103	LM8	unknown	; consensus \"MATGAATWATTCATR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATGAATTATTCATN",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_176_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.143166,
/* max_score        */ 16.944422,
/* threshold        */ 0.788,
/* info content     */ 20.959194,
/* base_counts      */ {3650, 1172, 1035, 3263},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 608
};

	float jaspar_cne_pwm_177_matrix_row_0_[20] = { -0.993768, -1.397574, -2.085758, -6.700879, -1.397574,  0.738093,  0.738093,  1.200499, -6.700879, -6.700879, -0.706917,  1.236853, -6.700879, -6.700879, -0.148371, -0.993768, -0.706917, -1.397574,  1.082762, -0.484273};
	float jaspar_cne_pwm_177_matrix_row_1_[20] = {  1.364729, -1.156412, -6.459717,  1.236950,  0.785225, -6.459717, -6.459717, -1.156412, -6.459717, -6.459717,  1.364729, -1.844596, -6.459717,  0.711172, -1.156412, -1.844596, -1.844596,  1.281382, -6.459717, -6.459717};
	float jaspar_cne_pwm_177_matrix_row_2_[20] = { -1.844596, -6.459717,  1.281382, -0.465755, -6.459717, -0.465755,  0.631193, -6.459717, -6.459717,  0.544258, -6.459717, -6.459717, -6.459717, -6.459717, -6.459717,  1.036381,  1.190452, -0.465755, -6.459717,  1.190452};
	float jaspar_cne_pwm_177_matrix_row_3_[20] = { -6.700879,  1.123567, -0.484273, -0.993768,  0.470010, -0.015018, -6.700879, -6.700879,  1.271932,  0.795219, -6.700879, -6.700879,  1.271932,  0.677505,  0.900524, -0.148371, -0.993768, -6.700879, -0.484273, -0.993768};
	float *jaspar_cne_pwm_177_matrix[4] = { jaspar_cne_pwm_177_matrix_row_0_, jaspar_cne_pwm_177_matrix_row_1_, jaspar_cne_pwm_177_matrix_row_2_, jaspar_cne_pwm_177_matrix_row_3_};
	PWM jaspar_cne_pwm_177_ = {
/* accession        */ "CN0179",
/* name             */ "LM179",
/* label            */ " CN0179	23.4531875277397	LM179	unknown	; consensus \"MTKNYWRATKMATYWRKMAR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGCYAAATTCATTTGGCAG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_177_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -122.616890,
/* max_score        */ 21.802328,
/* threshold        */ 0.846,
/* info content     */ 23.310535,
/* base_counts      */ {146, 131, 119, 184},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 29
};

	float jaspar_cne_pwm_178_matrix_row_0_[15] = {  1.251739,  1.187214,  0.991517, -1.517731, -2.609720, -7.224841, -7.224841, -1.921536, -2.609720, -7.224841, -2.609720,  1.094145,  1.209188, -0.538980, -2.609720};
	float jaspar_cne_pwm_178_matrix_row_1_[15] = { -6.983679, -1.680374, -6.983679, -2.368558,  1.492901, -2.368558, -6.983679, -0.767073,  1.405908, -2.368558, -6.983679, -6.983679, -2.368558, -6.983679, -2.368558};
	float jaspar_cne_pwm_178_matrix_row_2_[15] = { -2.368558, -1.680374, -6.983679,  1.359399, -6.983679, -6.983679, -6.983679, -1.276569, -0.989717, -6.983679, -6.983679, -1.276569, -6.983679, -6.983679,  1.405908};
	float jaspar_cne_pwm_178_matrix_row_3_[15] = { -7.224841, -7.224841, -0.133931, -1.517731, -7.224841,  1.251739,  1.272354,  1.044147, -7.224841,  1.251739,  1.251739, -1.008235, -1.921536,  1.094145, -1.517731};
	float *jaspar_cne_pwm_178_matrix[4] = { jaspar_cne_pwm_178_matrix_row_0_, jaspar_cne_pwm_178_matrix_row_1_, jaspar_cne_pwm_178_matrix_row_2_, jaspar_cne_pwm_178_matrix_row_3_};
	PWM jaspar_cne_pwm_178_ = {
/* accession        */ "CN0226",
/* name             */ "LM226",
/* label            */ " CN0226	23.1640325414534	LM226	unknown	; consensus \"AAAGCTTTCTTAATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAAGCTTTCTTAATG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_178_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -92.392105,
/* max_score        */ 18.563782,
/* threshold        */ 0.799,
/* info content     */ 23.088163,
/* base_counts      */ {234, 104, 99, 298},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_179_matrix_row_0_[16] = { -1.941722,  1.252167, -7.245027,  1.144560,  1.231552,  1.210503, -1.941722, -7.245027,  1.144560, -1.028421,  1.272366, -7.245027, -2.629907, -1.941722, -1.941722,  0.193944};
	float jaspar_cne_pwm_179_matrix_row_1_[16] = {  1.472714, -2.388745, -2.388745, -1.700560, -1.700560, -7.003865, -1.700560,  1.513528, -1.700560,  1.265123, -7.003865, -2.388745,  1.451665, -7.003865, -0.787259,  0.374519};
	float jaspar_cne_pwm_179_matrix_row_2_[16] = { -7.003865, -7.003865, -1.700560, -1.700560, -7.003865, -1.700560, -2.388745, -7.003865, -7.003865, -0.787259, -7.003865,  1.493329, -1.700560, -2.388745,  1.067353, -1.700560};
	float jaspar_cne_pwm_179_matrix_row_3_[16] = { -7.245027, -7.245027,  1.210503, -1.941722, -7.245027, -2.629907,  1.167028, -7.245027, -1.251066, -2.629907, -7.245027, -7.245027, -7.245027,  1.210503, -0.241053,  0.068860};
	float *jaspar_cne_pwm_179_matrix[4] = { jaspar_cne_pwm_179_matrix_row_0_, jaspar_cne_pwm_179_matrix_row_1_, jaspar_cne_pwm_179_matrix_row_2_, jaspar_cne_pwm_179_matrix_row_3_};
	PWM jaspar_cne_pwm_179_ = {
/* accession        */ "CN0058",
/* name             */ "LM58",
/* label            */ " CN0058	23.4925658167972	LM58	unknown	; consensus \"CATAAATCACAGCTKH\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATAAATCACAGCTGN",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_179_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.574463,
/* max_score        */ 19.481976,
/* threshold        */ 0.810,
/* info content     */ 23.420679,
/* base_counts      */ {313, 216, 98, 173},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_180_matrix_row_0_[17] = { -0.441522, -2.629907, -1.537917,  1.189002,  1.144560,  1.231552, -1.028421, -2.629907,  1.231552,  1.272366,  1.121576, -7.245027, -2.629907, -1.941722, -1.537917,  1.231552, -0.846432};
	float jaspar_cne_pwm_180_matrix_row_1_[17] = { -2.388745, -2.388745, -0.318004, -2.388745, -1.700560, -2.388745, -7.003865, -1.296755, -7.003865, -7.003865, -0.451357, -7.003865, -7.003865, -7.003865, -1.009904, -7.003865,  1.035615};
	float jaspar_cne_pwm_180_matrix_row_2_[17] = { -2.388745,  0.000109,  1.265123, -7.003865, -1.296755, -2.388745, -1.700560,  1.408190, -1.700560, -7.003865, -7.003865, -1.700560,  1.472714, -1.700560,  1.339213, -2.388745, -1.700560};
	float jaspar_cne_pwm_180_matrix_row_3_[17] = {  1.023961,  0.971331, -7.245027, -1.537917, -2.629907, -7.245027,  1.121576, -2.629907, -7.245027, -7.245027, -7.245027,  1.231552, -2.629907,  1.189002, -2.629907, -2.629907, -0.241053};
	float *jaspar_cne_pwm_180_matrix[4] = { jaspar_cne_pwm_180_matrix_row_0_, jaspar_cne_pwm_180_matrix_row_1_, jaspar_cne_pwm_180_matrix_row_2_, jaspar_cne_pwm_180_matrix_row_3_};
	PWM jaspar_cne_pwm_180_ = {
/* accession        */ "CN0164",
/* name             */ "LM164",
/* label            */ " CN0164	23.8746324163003	LM164	unknown	; consensus \"TTGAAATGAAATGTGAC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGAAATGAAATGTGAC",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_180_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.098427,
/* max_score        */ 20.480434,
/* threshold        */ 0.822,
/* info content     */ 23.806232,
/* base_counts      */ {358, 59, 201, 232},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_181_matrix_row_0_[14] = { -1.201055, -3.276134,  0.468500, -2.362833,  1.267531, -8.579439, -3.964319, -8.579439, -3.964319, -2.026931,  1.161588, -1.408551,  1.246141, -1.334497};
	float jaspar_cne_pwm_181_matrix_row_1_[14] = { -8.338277, -2.631167, -1.024390,  1.481883, -3.723156, -3.034972, -2.631167, -2.631167, -3.723156, -2.631167, -2.344316,  1.396851, -8.338277, -1.429522};
	float jaspar_cne_pwm_181_matrix_row_2_[14] = { -3.034972,  1.414446, -0.267058, -3.723156, -8.338277, -1.534772, -2.344316, -3.723156, -3.034972,  1.426006, -1.247367, -2.631167, -8.338277,  1.329551};
	float jaspar_cne_pwm_181_matrix_row_3_[14] = {  1.173284, -1.408551,  0.086347, -8.579439, -8.579439,  1.213173,  1.229793,  1.251531,  1.251531, -2.180844, -2.585478, -2.362833, -2.362833, -1.893578};
	float *jaspar_cne_pwm_181_matrix[4] = { jaspar_cne_pwm_181_matrix_row_0_, jaspar_cne_pwm_181_matrix_row_1_, jaspar_cne_pwm_181_matrix_row_2_, jaspar_cne_pwm_181_matrix_row_3_};
	PWM jaspar_cne_pwm_181_ = {
/* accession        */ "CN0059",
/* name             */ "LM59",
/* label            */ " CN0059	20.9838891823972	LM59	unknown	; consensus \"TGWCATTTTGACAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGWCATTTTGACAG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_181_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -72.964859,
/* max_score        */ 17.311811,
/* threshold        */ 0.784,
/* info content     */ 20.968966,
/* base_counts      */ {688, 398, 570, 1004},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 190
};

	float jaspar_cne_pwm_182_matrix_row_0_[18] = { -2.203902, -2.892087,  0.989987,  1.241257, -1.800097, -1.800097, -2.203902, -1.290601,  1.010186, -2.892087, -1.513245, -0.821346, -2.892087, -0.703702, -7.507207,  1.158579, -1.290601,  1.141190};
	float jaspar_cne_pwm_182_matrix_row_1_[18] = {  1.346640, -7.266045, -1.272083, -7.266045, -2.650924, -1.049439, -7.266045, -7.266045, -0.867450, -1.558935, -1.272083, -7.266045, -2.650924, -0.867450,  1.466421, -7.266045, -1.962740, -0.713537};
	float jaspar_cne_pwm_182_matrix_row_2_[18] = { -2.650924, -7.266045, -0.462540, -2.650924, -2.650924, -1.049439, -1.049439,  1.416832, -7.266045, -7.266045, -1.272083, -2.650924,  1.382352,  1.002943, -1.558935, -7.266045,  1.309606, -7.266045};
	float jaspar_cne_pwm_182_matrix_row_3_[18] = { -0.954699,  1.257002, -1.800097, -2.892087,  1.192474,  1.049399,  1.158579, -2.892087, -0.703702,  1.209001,  1.068444,  1.123493, -1.108612, -0.503233, -7.507207, -0.954699, -1.290601, -2.892087};
	float *jaspar_cne_pwm_182_matrix[4] = { jaspar_cne_pwm_182_matrix_row_0_, jaspar_cne_pwm_182_matrix_row_1_, jaspar_cne_pwm_182_matrix_row_2_, jaspar_cne_pwm_182_matrix_row_3_};
	PWM jaspar_cne_pwm_182_ = {
/* accession        */ "CN0092",
/* name             */ "LM92",
/* label            */ " CN0092	23.4779881360103	LM92	unknown	; consensus \"CTAATTTGATTTGGCAGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTAATTTGATTTGGCAGA",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_182_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -89.039169,
/* max_score        */ 21.524387,
/* threshold        */ 0.828,
/* info content     */ 23.428907,
/* base_counts      */ {321, 156, 238, 455},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 65
};

	float jaspar_cne_pwm_183_matrix_row_0_[14] = { -7.117303,  1.249300, -7.117303, -7.117303,  1.249300,  1.249300, -2.502182, -0.718708, -7.117303, -7.117303, -7.117303,  1.099055,  1.272284, -7.117303};
	float jaspar_cne_pwm_183_matrix_row_1_[14] = {  1.466937, -6.876141, -6.876141, -6.876141, -2.261020, -6.876141,  1.366879,  1.312826, -2.261020, -1.169031, -2.261020, -1.169031, -6.876141,  1.340217};
	float jaspar_cne_pwm_183_matrix_row_2_[14] = { -6.876141, -6.876141, -2.261020, -6.876141, -6.876141, -6.876141, -6.876141, -6.876141, -6.876141, -1.572836, -2.261020, -2.261020, -6.876141, -0.477546};
	float jaspar_cne_pwm_183_matrix_row_3_[14] = { -1.813998, -2.502182,  1.249300,  1.272284, -7.117303, -2.502182, -0.900697, -1.813998,  1.249300,  1.151685,  1.225775, -1.410193, -7.117303, -2.502182};
	float *jaspar_cne_pwm_183_matrix[4] = { jaspar_cne_pwm_183_matrix_row_0_, jaspar_cne_pwm_183_matrix_row_1_, jaspar_cne_pwm_183_matrix_row_2_, jaspar_cne_pwm_183_matrix_row_3_};
	PWM jaspar_cne_pwm_183_ = {
/* accession        */ "CN0150",
/* name             */ "LM150",
/* label            */ " CN0150	22.9337346654183	LM150	unknown	; consensus \"CATTAACCTTTAAC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATTAACCTTTAAC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_183_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.821304,
/* max_score        */ 17.754444,
/* threshold        */ 0.790,
/* info content     */ 22.847181,
/* base_counts      */ {217, 162, 11, 226},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 44
};

	float jaspar_cne_pwm_184_matrix_row_0_[12] = { -8.033049, -8.033049, -8.033049,  1.272693, -8.033049, -8.033049, -8.033049,  1.272693, -8.033049, -8.033049, -8.033049, -8.033049};
	float jaspar_cne_pwm_184_matrix_row_1_[12] = {  1.513855, -7.791886, -7.791886, -7.791886,  1.513855,  1.513855, -7.791886, -7.791886,  1.513855, -7.791886, -7.791886, -7.791886};
	float jaspar_cne_pwm_184_matrix_row_2_[12] = { -7.791886, -7.791886,  1.513855, -7.791886, -7.791886, -7.791886, -7.791886, -7.791886, -7.791886, -7.791886, -7.791886, -7.791886};
	float jaspar_cne_pwm_184_matrix_row_3_[12] = { -8.033049,  1.272693, -8.033049, -8.033049, -8.033049, -8.033049,  1.272693, -8.033049, -8.033049,  1.272693,  1.272693,  1.272693};
	float *jaspar_cne_pwm_184_matrix[4] = { jaspar_cne_pwm_184_matrix_row_0_, jaspar_cne_pwm_184_matrix_row_1_, jaspar_cne_pwm_184_matrix_row_2_, jaspar_cne_pwm_184_matrix_row_3_};
	PWM jaspar_cne_pwm_184_ = {
/* accession        */ "CN0111",
/* name             */ "LM111",
/* label            */ " CN0111	24	LM111	unknown	; consensus \"CTGACCTACTTT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGACCTACTTT",
/* library_name     */ "jaspar_cne",
/* length           */ 12,
/* matrixname       */ jaspar_cne_pwm_184_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.396599,
/* max_score        */ 16.478125,
/* threshold        */ 0.774,
/* info content     */ 23.951361,
/* base_counts      */ {220, 440, 110, 550},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 110
};

	float jaspar_cne_pwm_185_matrix_row_0_[14] = { -2.629907, -2.629907, -2.629907, -7.245027, -2.629907, -1.251066, -1.941722,  1.231552, -1.537917,  1.210503,  1.189002,  1.252167, -1.941722,  1.210503};
	float jaspar_cne_pwm_185_matrix_row_1_[14] = { -7.003865,  1.451665, -1.700560, -7.003865, -7.003865, -1.700560,  1.451665, -7.003865, -1.700560, -2.388745, -1.296755, -7.003865,  1.362738, -1.296755};
	float jaspar_cne_pwm_185_matrix_row_2_[14] = {  1.451665, -1.700560, -2.388745, -7.003865,  1.472714, -7.003865, -7.003865, -7.003865,  1.408190, -1.700560, -2.388745, -2.388745, -1.296755, -7.003865};
	float jaspar_cne_pwm_185_matrix_row_3_[14] = { -1.941722, -7.245027,  1.189002,  1.272366, -2.629907,  1.144560, -2.629907, -1.941722, -7.245027, -7.245027, -7.245027, -7.245027, -1.941722, -7.245027};
	float *jaspar_cne_pwm_185_matrix[4] = { jaspar_cne_pwm_185_matrix_row_0_, jaspar_cne_pwm_185_matrix_row_1_, jaspar_cne_pwm_185_matrix_row_2_, jaspar_cne_pwm_185_matrix_row_3_};
	PWM jaspar_cne_pwm_185_ = {
/* accession        */ "CN0168",
/* name             */ "LM168",
/* label            */ " CN0168	22.4792810838903	LM168	unknown	; consensus \"GCTTGTCAGAAACA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GCTTGTCAGAAACA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_185_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.306137,
/* max_score        */ 18.298294,
/* threshold        */ 0.789,
/* info content     */ 22.412258,
/* base_counts      */ {252, 150, 150, 148},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_186_matrix_row_0_[14] = {  1.135783,  1.183400, -1.879898, -1.879898, -7.183203, -7.183203,  1.183400,  1.272328, -7.183203, -7.183203,  1.250826, -7.183203, -1.189241,  1.272328};
	float jaspar_cne_pwm_186_matrix_row_1_[14] = { -0.948079, -6.942041, -6.942041,  1.376945,  1.376945, -6.942041, -1.638736, -6.942041, -6.942041, -2.326920, -6.942041, -6.942041,  1.218763, -6.942041};
	float jaspar_cne_pwm_186_matrix_row_2_[14] = { -1.638736, -1.234931,  1.447546, -2.326920, -6.942041, -6.942041, -1.638736, -6.942041, -6.942041, -2.326920, -2.326920,  1.491988, -0.389533, -6.942041};
	float jaspar_cne_pwm_186_matrix_row_3_[14] = { -7.183203, -2.568082, -2.568082, -1.476093, -0.784608,  1.272328, -7.183203, -7.183203,  1.272328,  1.228852, -7.183203, -2.568082, -2.568082, -7.183203};
	float *jaspar_cne_pwm_186_matrix[4] = { jaspar_cne_pwm_186_matrix_row_0_, jaspar_cne_pwm_186_matrix_row_1_, jaspar_cne_pwm_186_matrix_row_2_, jaspar_cne_pwm_186_matrix_row_3_};
	PWM jaspar_cne_pwm_186_ = {
/* accession        */ "CN0042",
/* name             */ "LM42",
/* label            */ " CN0042	22.9083601223866	LM42	unknown	; consensus \"AAGCCTAATTAGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAGCCTAATTAGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_186_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.611130,
/* max_score        */ 17.983759,
/* threshold        */ 0.790,
/* info content     */ 22.826986,
/* base_counts      */ {275, 124, 107, 152},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_187_matrix_row_0_[15] = { -1.718779, -7.022083, -7.022083, -7.022083,  1.220936,  1.246905, -7.022083, -7.022083,  1.246905, -7.022083, -7.022083,  1.138720, -1.314973, -1.314973,  0.068826};
	float jaspar_cne_pwm_187_matrix_row_1_[15] = { -6.780921, -1.477617,  1.513378, -6.780921, -1.477617, -6.780921, -6.780921, -2.165801, -6.780921, -6.780921,  1.488067, -2.165801, -2.165801,  1.350903,  0.389967};
	float jaspar_cne_pwm_187_matrix_row_2_[15] = { -1.477617,  1.462098, -6.780921, -6.780921, -6.780921, -2.165801, -6.780921, -6.780921, -6.780921,  1.513378, -2.165801, -6.780921,  1.290297, -1.477617, -6.780921};
	float jaspar_cne_pwm_187_matrix_row_3_[15] = {  1.166883, -7.022083, -7.022083,  1.272216, -7.022083, -7.022083,  1.272216,  1.246905, -2.406963, -7.022083, -7.022083, -1.028122, -1.028122, -2.406963,  0.291803};
	float *jaspar_cne_pwm_187_matrix[4] = { jaspar_cne_pwm_187_matrix_row_0_, jaspar_cne_pwm_187_matrix_row_1_, jaspar_cne_pwm_187_matrix_row_2_, jaspar_cne_pwm_187_matrix_row_3_};
	PWM jaspar_cne_pwm_187_ = {
/* accession        */ "CN0019",
/* name             */ "LM19",
/* label            */ " CN0019	24.1392804486059	LM19	unknown	; consensus \"TGCTAATTAGCAGCH\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCTAATTAGCAGCN",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_187_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.895195,
/* max_score        */ 18.819773,
/* threshold        */ 0.804,
/* info content     */ 24.035009,
/* base_counts      */ {171, 133, 116, 180},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 40
};

	float jaspar_cne_pwm_188_matrix_row_0_[14] = {  0.936208, -7.843503, -0.752594, -7.843503, -7.843503,  1.239117, -7.843503,  1.272636, -7.843503, -2.540199, -7.843503, -0.347406, -1.157643,  1.227690};
	float jaspar_cne_pwm_188_matrix_row_1_[14] = { -0.598367,  1.468852, -7.602342, -7.602342, -7.602342, -2.987221,  1.513798, -7.602342, -7.602342, -7.602342, -7.602342, -0.916480,  1.333694, -2.987221};
	float jaspar_cne_pwm_188_matrix_row_2_[14] = { -0.288455, -1.608380, -1.895231, -7.602342,  1.513798, -2.299037, -7.602342, -7.602342, -7.602342, -7.602342, -7.602342,  1.097340, -1.049834, -7.602342};
	float jaspar_cne_pwm_188_matrix_row_3_[14] = { -7.843503, -7.843503,  1.092532,  1.272636, -7.843503, -7.843503, -7.843503, -7.843503,  1.272636,  1.250415,  1.272636, -1.626897, -7.843503, -2.136393};
	float *jaspar_cne_pwm_188_matrix[4] = { jaspar_cne_pwm_188_matrix_row_0_, jaspar_cne_pwm_188_matrix_row_1_, jaspar_cne_pwm_188_matrix_row_2_, jaspar_cne_pwm_188_matrix_row_3_};
	PWM jaspar_cne_pwm_188_ = {
/* accession        */ "CN0082",
/* name             */ "LM82",
/* label            */ " CN0082	22.9365545092618	LM82	unknown	; consensus \"ACTTGACATTTGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ACTTGACATTTGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_188_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.868958,
/* max_score        */ 17.763990,
/* threshold        */ 0.790,
/* info content     */ 22.888248,
/* base_counts      */ {371, 275, 182, 446},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 91
};

	float jaspar_cne_pwm_189_matrix_row_0_[14] = { -7.321929, -7.321929, -2.018624, -2.018624, -2.706809, -1.614819,  1.195464, -2.706809,  1.234677, -2.706809, -0.317955,  1.253722,  1.215263,  1.253722};
	float jaspar_cne_pwm_189_matrix_row_1_[14] = {  1.513572, -7.080767, -2.465647, -1.086805, -7.080767, -7.080767, -2.465647,  1.456425, -7.080767, -1.777462, -7.080767, -7.080767, -1.777462, -7.080767};
	float jaspar_cne_pwm_189_matrix_row_2_[14] = { -7.080767, -7.080767,  1.436626, -1.373657, -7.080767,  1.395813, -1.373657, -7.080767, -7.080767,  1.416427, -2.465647, -2.465647, -7.080767, -2.465647};
	float jaspar_cne_pwm_189_matrix_row_3_[14] = { -7.321929,  1.272410, -2.706809,  1.090126,  1.253722, -1.614819, -7.321929, -2.018624, -2.018624, -2.018624,  1.021149, -7.321929, -2.706809, -7.321929};
	float *jaspar_cne_pwm_189_matrix[4] = { jaspar_cne_pwm_189_matrix_row_0_, jaspar_cne_pwm_189_matrix_row_1_, jaspar_cne_pwm_189_matrix_row_2_, jaspar_cne_pwm_189_matrix_row_3_};
	PWM jaspar_cne_pwm_189_ = {
/* accession        */ "CN0175",
/* name             */ "LM175",
/* label            */ " CN0175	22.7565022477221	LM175	unknown	; consensus \"CTGTTGACAGTAAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGTTGACAGTAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_189_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -86.526489,
/* max_score        */ 18.009117,
/* threshold        */ 0.790,
/* info content     */ 22.690445,
/* base_counts      */ {280, 115, 156, 205},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 54
};

	float jaspar_cne_pwm_190_matrix_row_0_[17] = { -1.517731, -7.224841, -7.224841, -1.517731, -0.826246,  1.230690,  1.230690,  1.209188, -0.538980, -0.220867, -1.921536, -2.609720, -1.921536,  1.018179, -7.224841, -2.609720,  1.094145};
	float jaspar_cne_pwm_190_matrix_row_1_[17] = {  1.335307, -2.368558, -1.680374, -6.983679,  1.335307, -1.680374, -2.368558, -6.983679, -6.983679, -0.297818,  1.310621, -0.767073,  1.428376, -6.983679, -6.983679, -6.983679, -1.680374};
	float jaspar_cne_pwm_190_matrix_row_2_[17] = { -2.368558, -6.983679, -0.767073, -6.983679, -1.680374, -6.983679, -2.368558, -2.368558,  1.087540,  0.840767, -0.585084, -6.983679, -6.983679, -1.680374, -6.983679, -6.983679, -6.983679};
	float jaspar_cne_pwm_190_matrix_row_3_[17] = { -1.230879,  1.251739,  1.118237,  1.209188, -7.224841, -7.224841, -7.224841, -1.921536, -0.421336, -1.008235, -2.609720,  1.141762, -1.921536, -0.421336,  1.272354,  1.251739, -0.826246};
	float *jaspar_cne_pwm_190_matrix[4] = { jaspar_cne_pwm_190_matrix_row_0_, jaspar_cne_pwm_190_matrix_row_1_, jaspar_cne_pwm_190_matrix_row_2_, jaspar_cne_pwm_190_matrix_row_3_};
	PWM jaspar_cne_pwm_190_ = {
/* accession        */ "CN0201",
/* name             */ "LM201",
/* label            */ " CN0201	23.3636148924881	LM201	unknown	; consensus \"CTTTCAAAGGCTCATTA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTCAAAGGCTCATTA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_190_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -105.205002,
/* max_score        */ 20.365828,
/* threshold        */ 0.818,
/* info content     */ 23.289249,
/* base_counts      */ {256, 188, 75, 314},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_191_matrix_row_0_[20] = { -0.007090, -0.668337,  0.733226,  0.480834, -0.915110, -0.801824, -3.032445,  1.212770, -2.187048, -2.745594, -2.340961, -3.436251, -1.830801, -1.568667,  1.203201,  1.123162,  1.217520, -1.300584, -1.425669,  0.882961};
	float jaspar_cne_pwm_191_matrix_row_1_[20] = { -0.525583,  0.019000, -0.366569, -0.366569, -0.801726, -1.184507,  1.444363, -2.504432, -2.281787, -2.504432, -2.281787, -1.327505,  1.288616,  1.343272, -2.099799, -1.494419, -2.504432, -1.945886,  1.260126, -0.673948};
	float jaspar_cne_pwm_191_matrix_row_2_[20] = { -0.714753,  0.639484, -0.597016, -0.948258, -0.673948, -0.948258, -1.694888, -8.498394, -1.694888, -2.791283, -8.498394, -2.504432, -0.896991, -2.791283, -3.195089, -1.407484, -1.945886,  1.277317, -0.896991, -3.195089};
	float jaspar_cne_pwm_191_matrix_row_3_[20] = {  0.547838, -0.262976, -0.668337,  0.099866,  0.882961,  0.947081, -3.032445, -1.936050,  1.173932,  1.222248,  1.222248,  1.183784, -1.425669, -1.189420, -2.187048, -2.053695, -4.124435, -0.998456, -1.425669, -0.327501};
	float *jaspar_cne_pwm_191_matrix[4] = { jaspar_cne_pwm_191_matrix_row_0_, jaspar_cne_pwm_191_matrix_row_1_, jaspar_cne_pwm_191_matrix_row_2_, jaspar_cne_pwm_191_matrix_row_3_};
	PWM jaspar_cne_pwm_191_ = {
/* accession        */ "CN0178",
/* name             */ "LM178",
/* label            */ " CN0178	20.7181026094443	LM178	unknown	; consensus \"NNMWTTCATTTTCCAAAGCA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "WNAWTTCATTTTCCAAAGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_191_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -54.199799,
/* max_score        */ 21.286943,
/* threshold        */ 0.806,
/* info content     */ 20.708574,
/* base_counts      */ {1427, 1015, 474, 1544},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 223
};

	float jaspar_cne_pwm_192_matrix_row_0_[15] = { -7.204238, -0.518378, -7.204238, -1.900934, -7.204238, -7.204238, -1.900934, -2.589118, -7.204238,  1.251292,  1.272341, -2.589118, -2.589118,  1.251292, -7.204238};
	float jaspar_cne_pwm_192_matrix_row_1_[15] = {  1.043624, -6.963077, -6.963077,  1.470952, -1.255966,  1.470952, -2.347956,  0.638326, -6.963077, -2.347956, -6.963077, -6.963077, -2.347956, -6.963077,  0.127833};
	float jaspar_cne_pwm_192_matrix_row_2_[15] = { -0.410569, -6.963077,  1.492454, -6.963077, -6.963077, -6.963077, -6.963077,  0.861369, -6.963077, -6.963077, -6.963077, -6.963077, -1.659772, -6.963077,  1.225890};
	float jaspar_cne_pwm_192_matrix_row_3_[15] = { -0.200264,  1.090061, -2.589118, -7.204238,  1.207816, -1.900934,  1.207816, -1.900934,  1.272341, -7.204238, -7.204238,  1.251292,  1.185349, -2.589118, -7.204238};
	float *jaspar_cne_pwm_192_matrix[4] = { jaspar_cne_pwm_192_matrix_row_0_, jaspar_cne_pwm_192_matrix_row_1_, jaspar_cne_pwm_192_matrix_row_2_, jaspar_cne_pwm_192_matrix_row_3_};
	PWM jaspar_cne_pwm_192_ = {
/* accession        */ "CN0095",
/* name             */ "LM95",
/* label            */ " CN0095	23.5439864925077	LM95	unknown	; consensus \"YTGCTCTSTAATTAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTGCTCTGTAATTAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_192_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.868683,
/* max_score        */ 18.554842,
/* threshold        */ 0.801,
/* info content     */ 23.457644,
/* base_counts      */ {157, 160, 117, 286},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_193_matrix_row_0_[15] = {  0.168939, -2.346921,  1.123110,  0.742215,  1.267444,  1.091563, -8.563527,  1.272805, -8.563527,  1.256633, -8.563527, -8.563527, -8.563527, -1.760022,  0.820851};
	float jaspar_cne_pwm_193_matrix_row_1_[15] = { -3.707244, -0.625698, -8.322365, -3.707244, -8.322365, -2.328403,  1.513967, -8.322365, -8.322365, -2.615255, -8.322365, -8.322365, -8.322365,  1.382733, -0.315664};
	float jaspar_cne_pwm_193_matrix_row_2_[15] = {  0.517057,  1.358042, -0.458714,  0.613670, -3.707244, -0.420988, -8.322365, -8.322365,  1.513967, -8.322365, -8.322365,  1.513967,  1.481358, -1.077423, -8.322365};
	float jaspar_cne_pwm_193_matrix_row_3_[15] = {  0.049158, -8.563527, -8.563527, -8.563527, -8.563527, -8.563527, -8.563527, -8.563527, -8.563527, -8.563527,  1.272805, -8.563527, -2.164932, -8.563527, -0.320508};
	float *jaspar_cne_pwm_193_matrix[4] = { jaspar_cne_pwm_193_matrix_row_0_, jaspar_cne_pwm_193_matrix_row_1_, jaspar_cne_pwm_193_matrix_row_2_, jaspar_cne_pwm_193_matrix_row_3_};
	PWM jaspar_cne_pwm_193_ = {
/* accession        */ "CN0024",
/* name             */ "LM24",
/* label            */ " CN0024	23.0121912261049	LM24	unknown	; consensus \"DGARAACAGATGGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NGAAAACAGATGGCA",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_193_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -123.355469,
/* max_score        */ 18.128517,
/* threshold        */ 0.799,
/* info content     */ 22.986296,
/* base_counts      */ {1179, 412, 928, 286},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 187
};

	float jaspar_cne_pwm_194_matrix_row_0_[15] = { -1.476093, -7.183203, -7.183203,  1.228852, -2.568082, -2.568082, -7.183203, -7.183203, -1.476093,  1.135783,  1.272328,  1.228852, -1.476093,  0.061739, -0.379698};
	float jaspar_cne_pwm_194_matrix_row_1_[15] = { -0.725435, -6.942041, -6.942041, -1.638736,  1.491988,  1.447546, -6.942041, -6.942041, -6.942041, -0.725435, -6.942041, -6.942041, -6.942041,  1.064660,  0.061933};
	float jaspar_cne_pwm_194_matrix_row_2_[15] = { -1.234931, -6.942041,  1.513490, -6.942041, -6.942041, -6.942041, -6.942041, -2.326920, -6.942041, -2.326920, -6.942041, -1.638736,  1.376945, -6.942041, -1.638736};
	float jaspar_cne_pwm_194_matrix_row_3_[15] = {  1.005764,  1.272328, -7.183203, -7.183203, -7.183203, -1.879898,  1.272328,  1.250826,  1.206384, -7.183203, -7.183203, -7.183203, -1.476093, -1.476093,  0.641243};
	float *jaspar_cne_pwm_194_matrix[4] = { jaspar_cne_pwm_194_matrix_row_0_, jaspar_cne_pwm_194_matrix_row_1_, jaspar_cne_pwm_194_matrix_row_2_, jaspar_cne_pwm_194_matrix_row_3_};
	PWM jaspar_cne_pwm_194_ = {
/* accession        */ "CN0104",
/* name             */ "LM104",
/* label            */ " CN0104	23.1811991379051	LM104	unknown	; consensus \"TTGACCTTTAAAGCW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGACCTTTAAAGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_194_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.531815,
/* max_score        */ 18.409317,
/* threshold        */ 0.799,
/* info content     */ 23.094673,
/* base_counts      */ {212, 143, 97, 253},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_195_matrix_row_0_[15] = { -2.361644, -8.068754, -8.068754, -8.068754,  1.263893, -3.453633, -8.068754, -3.453633, -8.068754, -8.068754, -8.068754, -3.453633, -1.064780, -0.572656,  0.386777};
	float jaspar_cne_pwm_195_matrix_row_1_[15] = { -0.918837,  1.513865, -2.524287, -7.827592, -7.827592, -1.275084, -7.827592, -7.827592, -7.827592,  1.496167, -7.827592, -7.827592, -0.331494,  0.936618, -0.918837};
	float jaspar_cne_pwm_195_matrix_row_2_[15] = {  0.243627, -7.827592, -3.212471,  1.513865, -7.827592, -3.212471, -3.212471,  1.505055,  1.513865, -3.212471, -7.827592,  1.505055,  0.967385, -0.513705,  0.441396};
	float jaspar_cne_pwm_195_matrix_row_3_[15] = {  0.770668, -8.068754,  1.246037, -8.068754, -3.453633,  1.190472,  1.263893, -8.068754, -8.068754, -3.453633,  1.272703, -8.068754, -0.518618, -0.629782, -0.572656};
	float *jaspar_cne_pwm_195_matrix[4] = { jaspar_cne_pwm_195_matrix_row_0_, jaspar_cne_pwm_195_matrix_row_1_, jaspar_cne_pwm_195_matrix_row_2_, jaspar_cne_pwm_195_matrix_row_3_};
	PWM jaspar_cne_pwm_195_ = {
/* accession        */ "CN0130",
/* name             */ "LM130",
/* label            */ " CN0130	22.3891802519928	LM130	unknown	; consensus \"TCTGATTGGCTGKCR\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCTGATTGGCTGGCR",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_195_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.875069,
/* max_score        */ 18.400934,
/* threshold        */ 0.795,
/* info content     */ 22.353386,
/* base_counts      */ {195, 337, 610, 568},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 114
};

	float jaspar_cne_pwm_196_matrix_row_0_[15] = { -1.049017, -8.362904, -3.747783, -2.368942, -8.362904,  1.272770,  1.272770,  1.272770, -8.362904, -8.362904, -8.362904, -0.984520,  1.272770,  0.905800,  0.369562};
	float jaspar_cne_pwm_196_matrix_row_1_[15] = {  1.279302, -1.212987, -2.818437, -8.121742, -8.121742, -8.121742, -8.121742, -8.121742, -8.121742, -8.121742, -1.030832,  1.343318, -8.121742, -0.148931, -1.435881};
	float jaspar_cne_pwm_196_matrix_row_2_[15] = { -8.121742, -8.121742, -1.117768, -1.905136,  1.513932, -8.121742, -8.121742, -8.121742, -8.121742,  1.480708, -8.121742, -8.121742, -8.121742, -1.212987, -0.520339};
	float jaspar_cne_pwm_196_matrix_row_3_[15] = { -0.923932,  1.205181,  1.176812,  1.212149, -8.362904, -8.362904, -8.362904, -8.362904,  1.272770, -2.146298,  1.191097, -1.677043, -8.362904, -1.677043,  0.385560};
	float *jaspar_cne_pwm_196_matrix[4] = { jaspar_cne_pwm_196_matrix_row_0_, jaspar_cne_pwm_196_matrix_row_1_, jaspar_cne_pwm_196_matrix_row_2_, jaspar_cne_pwm_196_matrix_row_3_};
	PWM jaspar_cne_pwm_196_ = {
/* accession        */ "CN0030",
/* name             */ "LM30",
/* label            */ " CN0030	23.4560582405832	LM30	unknown	; consensus \"CTTTGAAATGTCAAW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTGAAATGTCAAW",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_196_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -106.492058,
/* max_score        */ 18.057707,
/* threshold        */ 0.801,
/* info content     */ 23.425728,
/* base_counts      */ {816, 311, 347, 821},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 153
};

	float jaspar_cne_pwm_197_matrix_row_0_[15] = { -0.677659, -4.076443, -10.070404, -3.671809, -4.076443, -10.070404, -4.363294,  0.991551,  0.103149, -4.076443, -0.488432,  1.250161,  1.209340, -3.853798,  0.296849};
	float jaspar_cne_pwm_197_matrix_row_1_[15] = { -1.612884, -5.214122, -2.658354, -3.612636, -4.525938,  1.514092, -0.148836, -9.829243, -5.214122, -9.829243,  1.128575, -4.122132, -3.835281,  1.503372,  0.692157};
	float jaspar_cne_pwm_197_matrix_row_2_[15] = {  0.568972,  1.505764, -4.525938, -5.214122,  1.488896, -9.829243, -5.214122, -1.668438,  1.100305,  1.509342, -1.668438, -3.276734, -1.462640, -9.829243, -2.450859};
	float jaspar_cne_pwm_197_matrix_row_3_[15] = {  0.418116, -4.767099,  1.254998,  1.258610, -2.756517, -10.070404,  1.056873, -0.317682, -2.329305, -10.070404, -0.965313, -3.266899, -3.853798, -4.076443, -0.530688};
	float *jaspar_cne_pwm_197_matrix[4] = { jaspar_cne_pwm_197_matrix_row_0_, jaspar_cne_pwm_197_matrix_row_1_, jaspar_cne_pwm_197_matrix_row_2_, jaspar_cne_pwm_197_matrix_row_3_};
	PWM jaspar_cne_pwm_197_ = {
/* accession        */ "CN0003",
/* name             */ "LM3",
/* label            */ " CN0003	21.3718399470909	LM3	unknown	; consensus \"KGTTGCTWRGCAACM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "KGTTGCTAGGCAACM",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_197_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -88.960243,
/* max_score        */ 18.033009,
/* threshold        */ 0.790,
/* info content     */ 21.367857,
/* base_counts      */ {3125, 2850, 3526, 3159},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 844
};

	float jaspar_cne_pwm_198_matrix_row_0_[16] = { -2.087258, -0.594038, -3.097271, -0.701645, -1.920344,  1.253763, -4.476112, -1.777346,  1.221081,  1.247311,  1.234282, -1.020014, -2.287727,  1.066936,  0.930083,  0.812305};
	float jaspar_cne_pwm_198_matrix_row_1_[16] = {  0.298501, -4.234950, -3.546766, -2.046565,  1.438714, -4.234950, -4.234950, -1.248668, -8.850070, -2.856109, -2.633464, -8.850070, -0.877260, -1.846096, -1.153403, -1.199902};
	float jaspar_cne_pwm_198_matrix_row_2_[16] = { -1.679182, -1.679182,  1.284569,  1.063417, -4.234950, -8.850070,  1.504525,  1.346124, -1.471687, -2.856109, -3.142960, -3.546766,  1.371907, -1.248668, -0.438016, -0.483468};
	float jaspar_cne_pwm_198_matrix_row_3_[16] = {  0.807293,  1.051311, -0.408355, -0.358767, -2.287727, -2.874626, -4.476112, -1.846291, -9.091232, -9.091232, -3.097271,  1.159420, -3.097271, -1.153501, -1.266787, -0.515582};
	float *jaspar_cne_pwm_198_matrix[4] = { jaspar_cne_pwm_198_matrix_row_0_, jaspar_cne_pwm_198_matrix_row_1_, jaspar_cne_pwm_198_matrix_row_2_, jaspar_cne_pwm_198_matrix_row_3_};
	PWM jaspar_cne_pwm_198_ = {
/* accession        */ "CN0043",
/* name             */ "LM43",
/* label            */ " CN0043	20.1931221351829	LM43	unknown	; consensus \"TTGKCAGRAAATGAWW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGGCAGGAAATGAAA",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_198_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -68.908524,
/* max_score        */ 18.793039,
/* threshold        */ 0.787,
/* info content     */ 20.185053,
/* base_counts      */ {2087, 514, 1471, 1000},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 317
};

	float jaspar_cne_pwm_199_matrix_row_0_[18] = { -2.038790, -3.335654,  1.239194,  1.239194,  1.221923, -2.644169, -2.239259,  0.946947, -1.301665, -2.490256, -2.356903, -3.335654,  0.599424, -1.105032,  1.211416,  1.145940,  0.825148, -1.003284};
	float jaspar_cne_pwm_199_matrix_row_1_[18] = { -1.998097,  1.420376, -2.249094, -3.094492, -4.186481, -2.807640,  0.991010, -1.200200,  1.344871, -2.584996, -1.630713, -1.710692, -1.251467,  1.329061, -4.186481, -2.115741, -1.892847,  0.985408};
	float jaspar_cne_pwm_199_matrix_row_2_[18] = { -1.423218, -1.487715, -8.801602, -2.584996, -1.892847,  1.456093, -2.584996, -0.207263, -2.249094, -8.801602, -2.249094, -2.584996, -0.225951, -1.998097, -2.249094, -1.305505,  0.066389, -1.305505};
	float jaspar_cne_pwm_199_matrix_row_3_[18] = {  1.145940, -2.239259, -3.335654, -3.739459, -3.048802, -2.490256,  0.253846, -2.134009, -1.603792,  1.232322,  1.175571,  1.204349, -0.106729, -1.797822, -2.134009, -2.134009, -1.105032, -0.119972};
	float *jaspar_cne_pwm_199_matrix[4] = { jaspar_cne_pwm_199_matrix_row_0_, jaspar_cne_pwm_199_matrix_row_1_, jaspar_cne_pwm_199_matrix_row_2_, jaspar_cne_pwm_199_matrix_row_3_};
	PWM jaspar_cne_pwm_199_ = {
/* accession        */ "CN0102",
/* name             */ "LM102",
/* label            */ " CN0102	22.3017907010853	LM102	unknown	; consensus \"TCAAAGYACTTTWCAARM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCAAAGCACTTTACAAAC",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_199_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -59.140293,
/* max_score        */ 20.714188,
/* threshold        */ 0.815,
/* info content     */ 22.293953,
/* base_counts      */ {2115, 1250, 585, 1486},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 302
};

	float jaspar_cne_pwm_200_matrix_row_0_[15] = { -7.204238, -1.900934, -2.589118, -1.900934, -7.204238,  1.207816, -2.589118, -7.204238,  1.272341, -7.204238,  1.162364,  1.251292,  1.185349, -7.204238, -2.589118};
	float jaspar_cne_pwm_200_matrix_row_1_[15] = { -6.963077, -2.347956, -2.347956,  1.426511,  1.513503, -1.255966, -0.564481,  1.225890, -6.963077, -0.746470, -6.963077, -6.963077, -1.659772, -2.347956,  1.492454};
	float jaspar_cne_pwm_200_matrix_row_2_[15] = { -6.963077, -6.963077, -0.969115, -2.347956, -6.963077, -6.963077, -6.963077, -0.410569, -6.963077, -0.746470, -1.659772, -6.963077, -6.963077, -6.963077, -6.963077};
	float jaspar_cne_pwm_200_matrix_row_3_[15] = {  1.272341,  1.207816,  1.138839, -2.589118, -7.204238, -7.204238,  1.114748, -0.987632, -7.204238,  1.038781, -1.497128, -2.589118, -1.900934,  1.251292, -7.204238};
	float *jaspar_cne_pwm_200_matrix[4] = { jaspar_cne_pwm_200_matrix_row_0_, jaspar_cne_pwm_200_matrix_row_1_, jaspar_cne_pwm_200_matrix_row_2_, jaspar_cne_pwm_200_matrix_row_3_};
	PWM jaspar_cne_pwm_200_ = {
/* accession        */ "CN0229",
/* name             */ "LM229",
/* label            */ " CN0229	23.8220883047563	LM229	unknown	; consensus \"TTTCCATCATAAATC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTCCATCATAAATC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_200_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -97.627525,
/* max_score        */ 18.761335,
/* threshold        */ 0.803,
/* info content     */ 23.740515,
/* base_counts      */ {234, 194, 19, 273},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_201_matrix_row_0_[17] = { -0.559166, -7.245027,  1.252167, -0.692519, -1.028421, -1.941722, -7.245027, -2.629907,  1.144560,  1.210503,  1.210503, -7.245027, -1.941722,  0.971331, -0.441522,  0.971331, -0.846432};
	float jaspar_cne_pwm_201_matrix_row_1_[17] = { -7.003865, -1.296755, -7.003865,  1.339213, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -1.700560, -2.388745, -7.003865, -1.700560, -0.605270,  1.098116, -1.296755, -1.296755};
	float jaspar_cne_pwm_201_matrix_row_2_[17] = { -1.700560,  1.408190, -2.388745, -2.388745, -7.003865,  1.408190, -7.003865,  1.493329, -7.003865, -2.388745, -7.003865, -7.003865, -0.200360, -2.388745, -0.787259, -1.296755,  1.212493};
	float jaspar_cne_pwm_201_matrix_row_3_[17] = {  1.049272, -1.941722, -7.245027, -7.245027,  1.167028, -1.537917,  1.272366, -7.245027, -0.846432, -7.245027, -1.941722,  1.272366,  0.971331, -0.846432, -1.537917, -0.692519, -1.251066};
	float *jaspar_cne_pwm_201_matrix[4] = { jaspar_cne_pwm_201_matrix_row_0_, jaspar_cne_pwm_201_matrix_row_1_, jaspar_cne_pwm_201_matrix_row_2_, jaspar_cne_pwm_201_matrix_row_3_};
	PWM jaspar_cne_pwm_201_ = {
/* accession        */ "CN0203",
/* name             */ "LM203",
/* label            */ " CN0203	23.0812763638508	LM203	unknown	; consensus \"TGACTGTGAAATTACAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGACTGTGAAATTACAG",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_201_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.196404,
/* max_score        */ 20.452288,
/* threshold        */ 0.815,
/* info content     */ 23.009096,
/* base_counts      */ {301, 95, 199, 255},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_202_matrix_row_0_[15] = { -0.846432,  1.231552,  1.121576, -2.629907, -7.245027,  1.189002, -1.941722, -1.251066,  1.231552,  1.272366, -7.245027, -7.245027,  0.886797, -1.028421, -0.074139};
	float jaspar_cne_pwm_202_matrix_row_1_[15] = { -1.700560, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -2.388745, -7.003865,  1.385722,  0.737234};
	float jaspar_cne_pwm_202_matrix_row_2_[15] = {  1.265123, -1.700560, -0.451357, -7.003865,  1.513528, -1.296755, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865,  1.493329,  0.241076, -7.003865, -2.388745};
	float jaspar_cne_pwm_202_matrix_row_3_[15] = { -1.537917, -7.245027, -7.245027,  1.252167, -7.245027, -2.629907,  1.231552,  1.189002, -1.941722, -7.245027,  1.272366, -7.245027, -1.941722, -2.629907, -0.074139};
	float *jaspar_cne_pwm_202_matrix[4] = { jaspar_cne_pwm_202_matrix_row_0_, jaspar_cne_pwm_202_matrix_row_1_, jaspar_cne_pwm_202_matrix_row_2_, jaspar_cne_pwm_202_matrix_row_3_};
	PWM jaspar_cne_pwm_202_ = {
/* accession        */ "CN0066",
/* name             */ "LM66",
/* label            */ " CN0066	23.136319024744	LM66	unknown	; consensus \"GAATGATTAATGACY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GAATGATTAATGACN",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_202_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.586533,
/* max_score        */ 18.272867,
/* threshold        */ 0.799,
/* info content     */ 23.054272,
/* base_counts      */ {300, 70, 165, 215},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_203_matrix_row_0_[19] = { -7.537494, -2.234189,  1.110903, -2.922374, -7.537494, -0.851633, -7.537494, -1.830384, -1.138899, -1.320888, -2.234189, -2.922374,  1.038157,  1.110903, -1.320888, -2.234189, -1.138899, -0.098522, -0.533520};
	float jaspar_cne_pwm_203_matrix_row_1_[19] = { -1.589222,  1.467878, -2.681211, -7.296332, -7.296332, -7.296332,  1.279319,  1.403349, -7.296332,  1.137697,  1.279319, -1.589222, -0.292358, -1.079726, -1.993027, -1.993027, -1.589222, -0.743824,  0.017555};
	float jaspar_cne_pwm_203_matrix_row_2_[19] = { -2.681211, -2.681211, -7.296332, -2.681211, -7.296332, -1.993027, -1.993027, -7.296332, -2.681211, -0.205422, -7.296332, -7.296332, -1.993027, -2.681211, -2.681211, -1.589222,  1.316353,  0.676479, -0.492827};
	float jaspar_cne_pwm_203_matrix_row_3_[19] = {  1.210970, -7.537494, -0.733989,  1.242217,  1.272518,  1.110903, -0.446584, -1.543532,  1.162187, -1.543532, -0.446584,  1.210970, -2.922374, -1.543532,  1.145383,  1.162187, -1.830384, -0.292552,  0.533724};
	float *jaspar_cne_pwm_203_matrix[4] = { jaspar_cne_pwm_203_matrix_row_0_, jaspar_cne_pwm_203_matrix_row_1_, jaspar_cne_pwm_203_matrix_row_2_, jaspar_cne_pwm_203_matrix_row_3_};
	PWM jaspar_cne_pwm_203_ = {
/* accession        */ "CN0163",
/* name             */ "LM163",
/* label            */ " CN0163	23.4620113749556	LM163	unknown	; consensus \"TCATTTCCTCCTAATTGNY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCATTTCCTCCTAATTGNN",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_203_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.394562,
/* max_score        */ 21.871418,
/* threshold        */ 0.837,
/* info content     */ 23.414112,
/* base_counts      */ {236, 328, 120, 589},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 67
};

	float jaspar_cne_pwm_204_matrix_row_0_[17] = {  0.034415,  1.143882, -2.172970, -1.767838, -1.340626, -1.990815, -2.395864,  1.243790, -1.703341, -0.669670, -0.271713,  1.099432,  1.196803,  1.253578, -2.683130, -0.738647,  0.961568};
	float jaspar_cne_pwm_204_matrix_row_1_[17] = {  0.616716, -1.931808, -2.846601, -2.154702, -4.225442, -0.976912,  1.465084, -2.623957, -4.225442, -2.441968,  0.935148, -3.133453, -3.133453, -3.537258, -3.133453, -1.595621, -1.290428};
	float jaspar_cne_pwm_204_matrix_row_2_[17] = { -0.939185, -1.401591, -3.133453, -2.846601, -2.441968,  1.344375, -2.623957, -4.225442, -1.931808,  1.282100, -1.462179, -3.537258, -1.836589, -4.225442, -4.225442,  1.269979, -0.738582};
	float jaspar_cne_pwm_204_matrix_row_3_[17] = { -0.256900, -2.077751,  1.217211,  1.182962,  1.172455, -2.077751, -3.778420, -3.374615,  1.182962, -1.836783, -0.469040, -0.669670, -2.278220, -3.374615,  1.240506, -1.990815, -1.010506};
	float *jaspar_cne_pwm_204_matrix[4] = { jaspar_cne_pwm_204_matrix_row_0_, jaspar_cne_pwm_204_matrix_row_1_, jaspar_cne_pwm_204_matrix_row_2_, jaspar_cne_pwm_204_matrix_row_3_};
	PWM jaspar_cne_pwm_204_ = {
/* accession        */ "CN0015",
/* name             */ "LM15",
/* label            */ " CN0015	21.5544935711601	LM15	unknown	; consensus \"MATTTGCATGCAAATGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "NATTTGCATGCAAATGA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_204_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -49.836472,
/* max_score        */ 19.808552,
/* threshold        */ 0.802,
/* info content     */ 21.547499,
/* base_counts      */ {2009, 708, 897, 1724},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 314
};

	float jaspar_cne_pwm_205_matrix_row_0_[15] = { -7.204238, -7.204238, -7.204238, -7.204238, -0.987632,  0.768572, -1.497128, -2.589118, -2.589118, -7.204238, -2.589118, -7.204238,  1.272341, -7.204238, -7.204238};
	float jaspar_cne_pwm_205_matrix_row_1_[15] = {  1.492454, -2.347956, -6.963077, -6.963077, -1.659772, -2.347956, -2.347956,  1.470952, -6.963077, -2.347956, -0.746470,  1.355910, -6.963077, -6.963077, -2.347956};
	float jaspar_cne_pwm_205_matrix_row_2_[15] = { -2.347956, -6.963077, -1.659772, -6.963077,  1.355910, -6.963077, -6.963077, -2.347956, -6.963077, -6.963077, -1.659772, -1.255966, -6.963077, -6.963077, -6.963077};
	float jaspar_cne_pwm_205_matrix_row_3_[15] = { -7.204238,  1.251292,  1.229790,  1.272341, -7.204238,  0.291859,  1.185349, -7.204238,  1.251292,  1.251292,  1.090061, -1.210277, -7.204238,  1.272341,  1.251292};
	float *jaspar_cne_pwm_205_matrix[4] = { jaspar_cne_pwm_205_matrix_row_0_, jaspar_cne_pwm_205_matrix_row_1_, jaspar_cne_pwm_205_matrix_row_2_, jaspar_cne_pwm_205_matrix_row_3_};
	PWM jaspar_cne_pwm_205_ = {
/* accession        */ "CN0187",
/* name             */ "LM187",
/* label            */ " CN0187	24.8257961232955	LM187	unknown	; consensus \"CTTTGATCTTTCATT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTGATCTTTCATT",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_205_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.724976,
/* max_score        */ 18.771189,
/* threshold        */ 0.808,
/* info content     */ 24.737186,
/* base_counts      */ {88, 146, 50, 436},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 48
};

	float jaspar_cne_pwm_206_matrix_row_0_[14] = { -1.008235, -7.224841, -7.224841, -7.224841, -7.224841,  1.164746,  1.272354, -7.224841, -1.517731,  1.230690,  1.187214, -1.008235, -7.224841, -7.224841};
	float jaspar_cne_pwm_206_matrix_row_1_[14] = { -0.585084, -6.983679, -6.983679,  1.382924,  1.513516, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.513516, -6.983679};
	float jaspar_cne_pwm_206_matrix_row_2_[14] = { -0.989717, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -0.989717,  1.087540, -6.983679, -6.983679};
	float jaspar_cne_pwm_206_matrix_row_3_[14] = {  0.906984,  1.272354,  1.272354, -0.826246, -7.224841, -1.008235, -7.224841,  1.272354,  1.209188, -1.921536, -7.224841, -0.133931, -7.224841,  1.272354};
	float *jaspar_cne_pwm_206_matrix[4] = { jaspar_cne_pwm_206_matrix_row_0_, jaspar_cne_pwm_206_matrix_row_1_, jaspar_cne_pwm_206_matrix_row_2_, jaspar_cne_pwm_206_matrix_row_3_};
	PWM jaspar_cne_pwm_206_ = {
/* accession        */ "CN0114",
/* name             */ "LM114",
/* label            */ " CN0114	23.3995604087411	LM114	unknown	; consensus \"TTTCCAATTAAGCT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTTCCAATTAAGCT",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_206_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.966515,
/* max_score        */ 17.558086,
/* threshold        */ 0.792,
/* info content     */ 23.306644,
/* base_counts      */ {198, 147, 40, 301},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_207_matrix_row_0_[14] = { -7.224841, -7.224841, -7.224841,  1.272354, -7.224841,  0.271257,  1.272354,  1.272354, -7.224841, -7.224841,  1.272354,  0.747970,  1.272354, -7.224841};
	float jaspar_cne_pwm_207_matrix_row_1_[14] = {  1.205288, -6.983679,  1.513516, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679,  1.513516, -6.983679,  0.330208, -6.983679, -6.983679};
	float jaspar_cne_pwm_207_matrix_row_2_[14] = { -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -6.983679, -0.767073, -6.983679,  1.513516};
	float jaspar_cne_pwm_207_matrix_row_3_[14] = { -0.053952,  1.272354, -7.224841, -7.224841,  1.272354,  0.814639, -7.224841, -7.224841,  1.272354, -7.224841, -7.224841, -7.224841, -7.224841, -7.224841};
	float *jaspar_cne_pwm_207_matrix[4] = { jaspar_cne_pwm_207_matrix_row_0_, jaspar_cne_pwm_207_matrix_row_1_, jaspar_cne_pwm_207_matrix_row_2_, jaspar_cne_pwm_207_matrix_row_3_};
	PWM jaspar_cne_pwm_207_ = {
/* accession        */ "CN0056",
/* name             */ "LM56",
/* label            */ " CN0056	24.9100786371029	LM56	unknown	; consensus \"CTCATTAATCAAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTCATTAATCAAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_207_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.906593,
/* max_score        */ 17.487272,
/* threshold        */ 0.796,
/* info content     */ 24.804337,
/* base_counts      */ {292, 149, 54, 191},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_208_matrix_row_0_[15] = { -7.183203, -1.189241, -2.568082, -7.183203, -7.183203, -7.183203,  1.206384, -7.183203,  1.250826,  1.272328,  1.159875, -1.189241, -7.183203, -2.568082, -7.183203};
	float jaspar_cne_pwm_208_matrix_row_1_[15] = { -6.942041, -6.942041, -1.234931, -6.942041, -6.942041,  1.470014, -6.942041, -1.638736, -2.326920, -6.942041, -6.942041, -1.234931,  1.513490, -6.942041, -2.326920};
	float jaspar_cne_pwm_208_matrix_row_2_[15] = { -1.234931,  1.424562, -1.638736, -6.942041, -6.942041, -6.942041, -6.942041,  0.659362, -6.942041, -6.942041, -6.942041,  1.159940, -6.942041, -6.942041,  1.424562};
	float jaspar_cne_pwm_208_matrix_row_3_[15] = {  1.206384, -7.183203,  1.135783,  1.272328,  1.272328, -1.879898, -1.476093,  0.641243, -7.183203, -7.183203, -0.966597, -0.630695, -7.183203,  1.250826, -1.476093};
	float *jaspar_cne_pwm_208_matrix[4] = { jaspar_cne_pwm_208_matrix_row_0_, jaspar_cne_pwm_208_matrix_row_1_, jaspar_cne_pwm_208_matrix_row_2_, jaspar_cne_pwm_208_matrix_row_3_};
	PWM jaspar_cne_pwm_208_ = {
/* accession        */ "CN0228",
/* name             */ "LM228",
/* label            */ " CN0228	24.1029766656565	LM228	unknown	; consensus \"TGTTTCATAAAGCTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGTTTCATAAAGCTG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_208_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.461166,
/* max_score        */ 18.678989,
/* threshold        */ 0.804,
/* info content     */ 24.011406,
/* base_counts      */ {189, 102, 144, 270},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_209_matrix_row_0_[14] = { -7.224841,  1.164746, -7.224841, -1.008235,  1.230690,  1.141762, -7.224841, -7.224841,  1.272354, -7.224841, -1.008235,  1.209188, -7.224841, -7.224841};
	float jaspar_cne_pwm_209_matrix_row_1_[14] = {  1.513516, -6.983679, -6.983679, -2.368558, -1.680374, -1.680374, -6.983679,  1.513516, -6.983679, -2.368558,  1.359399, -6.983679, -6.983679, -6.983679};
	float jaspar_cne_pwm_209_matrix_row_2_[14] = { -6.983679, -6.983679, -2.368558, -1.680374, -6.983679, -6.983679, -0.989717, -6.983679, -6.983679,  1.382924, -1.680374, -2.368558, -6.983679, -6.983679};
	float jaspar_cne_pwm_209_matrix_row_3_[14] = { -7.224841, -1.008235,  1.251739,  1.094145, -7.224841, -1.230879,  1.187214, -7.224841, -7.224841, -1.008235, -7.224841, -1.921536,  1.272354,  1.272354};
	float *jaspar_cne_pwm_209_matrix[4] = { jaspar_cne_pwm_209_matrix_row_0_, jaspar_cne_pwm_209_matrix_row_1_, jaspar_cne_pwm_209_matrix_row_2_, jaspar_cne_pwm_209_matrix_row_3_};
	PWM jaspar_cne_pwm_209_ = {
/* accession        */ "CN0143",
/* name             */ "LM143",
/* label            */ " CN0143	23.5045583691905	LM143	unknown	; consensus \"CATTAATCAGMATT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATTAATCAGCATT",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_209_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -95.568001,
/* max_score        */ 17.865900,
/* threshold        */ 0.792,
/* info content     */ 23.419081,
/* base_counts      */ {239, 146, 53, 248},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_210_matrix_row_0_[17] = { -0.945109, -2.546595,  0.009173, -7.161715,  1.272314, -7.161715,  1.272314, -7.161715, -7.161715,  0.083226, -1.858410, -1.167754, -7.161715, -7.161715, -7.161715, -0.945109,  0.999089};
	float jaspar_cne_pwm_210_matrix_row_1_[17] = { -6.920553, -6.920553,  1.181428,  1.513476, -6.920553, -6.920553, -6.920553, -1.213443, -1.213443, -6.920553, -2.305433,  1.348435, -6.920553, -2.305433, -1.617248,  0.729616, -1.213443};
	float jaspar_cne_pwm_210_matrix_row_2_[17] = { -1.213443,  1.491502, -6.920553, -6.920553, -6.920553,  1.513476, -6.920553, -6.920553,  1.446050,  1.150665, -6.920553, -6.920553, -6.920553,  0.729616,  0.457831, -0.234692, -6.920553};
	float jaspar_cne_pwm_210_matrix_row_3_[17] = {  1.081304, -7.161715, -7.161715, -7.161715, -7.161715, -7.161715, -7.161715,  1.204888, -7.161715, -7.161715,  1.204888, -1.454605,  1.272314,  0.621925,  0.776017, -0.070805, -0.475854};
	float *jaspar_cne_pwm_210_matrix[4] = { jaspar_cne_pwm_210_matrix_row_0_, jaspar_cne_pwm_210_matrix_row_1_, jaspar_cne_pwm_210_matrix_row_2_, jaspar_cne_pwm_210_matrix_row_3_};
	PWM jaspar_cne_pwm_210_ = {
/* accession        */ "CN0129",
/* name             */ "LM129",
/* label            */ " CN0129	24.3087284745856	LM129	unknown	; consensus \"WGMCAGATGRTMTKKNW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGCCAGATGGTCTTTNA",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_210_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -114.567902,
/* max_score        */ 20.187389,
/* threshold        */ 0.826,
/* info content     */ 24.209558,
/* base_counts      */ {171, 152, 214, 245},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_211_matrix_row_0_[18] = { -1.168296,  1.243122, -7.566891, -1.572929, -2.951770, -2.263586, -1.014383, -1.168296,  1.212821, -2.951770,  1.115986,  1.257934,  1.228086, -1.350284, -0.881030,  0.702098, -2.263586, -0.475981};
	float jaspar_cne_pwm_211_matrix_row_1_[18] = {  1.191665, -2.710608, -0.773221, -7.325728, -2.022424,  1.322668,  0.170369,  1.286957, -7.325728, -2.710608, -2.022424, -7.325728, -2.710608, -7.325728, -0.773221, -0.773221,  1.390479, -1.331767};
	float jaspar_cne_pwm_211_matrix_row_2_[18] = { -0.154840, -7.325728, -2.022424, -7.325728, -2.022424, -1.618618, -7.325728, -0.927134, -7.325728,  1.484284, -1.109122, -2.710608, -2.710608, -7.325728,  1.211463, -0.522223, -2.710608, -2.710608};
	float jaspar_cne_pwm_211_matrix_row_3_[18] = { -7.566891, -2.951770,  1.132791,  1.212821,  1.197319, -1.014383,  0.822697, -2.263586, -1.572929, -7.566891, -1.859780, -7.566891, -2.951770,  1.197319, -1.859780, -0.321949, -1.350284,  0.989716};
	float *jaspar_cne_pwm_211_matrix[4] = { jaspar_cne_pwm_211_matrix_row_0_, jaspar_cne_pwm_211_matrix_row_1_, jaspar_cne_pwm_211_matrix_row_2_, jaspar_cne_pwm_211_matrix_row_3_};
	PWM jaspar_cne_pwm_211_ = {
/* accession        */ "CN0215",
/* name             */ "LM215",
/* label            */ " CN0215	23.2712946638551	LM215	unknown	; consensus \"CATTTCTCAGAAATGACT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATTTCTCAGAAATGACT",
/* library_name     */ "jaspar_cne",
/* length           */ 18,
/* matrixname       */ jaspar_cne_pwm_211_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.403557,
/* max_score        */ 21.200226,
/* threshold        */ 0.826,
/* info content     */ 23.224861,
/* base_counts      */ {418, 273, 162, 389},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 69
};

	float jaspar_cne_pwm_212_matrix_row_0_[16] = { -0.000086,  1.073959, -1.941722,  1.272366,  1.272366,  1.272366, -1.941722, -1.941722, -7.245027,  1.252167, -1.941722, -2.629907,  0.826191,  1.210503, -7.245027,  0.193944};
	float jaspar_cne_pwm_212_matrix_row_1_[16] = {  0.859786, -7.003865, -7.003865, -7.003865, -7.003865, -7.003865, -2.388745,  1.430164,  1.408190, -7.003865, -7.003865, -7.003865, -1.700560, -2.388745,  1.385722,  0.241076};
	float jaspar_cne_pwm_212_matrix_row_2_[16] = { -0.318004, -7.003865,  1.472714, -7.003865, -7.003865, -7.003865, -1.009904, -2.388745, -7.003865, -7.003865, -7.003865, -7.003865, -2.388745, -2.388745, -0.787259, -2.388745};
	float jaspar_cne_pwm_212_matrix_row_3_[16] = { -1.941722, -0.441522, -7.245027, -7.245027, -7.245027, -7.245027,  1.121576, -2.629907, -1.028421, -2.629907,  1.231552,  1.252167,  0.068860, -2.629907, -2.629907,  0.251070};
	float *jaspar_cne_pwm_212_matrix[4] = { jaspar_cne_pwm_212_matrix_row_0_, jaspar_cne_pwm_212_matrix_row_1_, jaspar_cne_pwm_212_matrix_row_2_, jaspar_cne_pwm_212_matrix_row_3_};
	PWM jaspar_cne_pwm_212_ = {
/* accession        */ "CN0147",
/* name             */ "LM147",
/* label            */ " CN0147	23.2218643671062	LM147	unknown	; consensus \"MAGAAATCCATTAACW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAGAAATCCATTAACN",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_212_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -85.853401,
/* max_score        */ 18.592859,
/* threshold        */ 0.808,
/* info content     */ 23.145229,
/* base_counts      */ {359, 179, 69, 193},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_213_matrix_row_0_[20] = { -2.969598, -1.872650, -2.969598, -2.564965,  1.194609, -0.861579, -1.959586,  1.105485, -2.411052, -3.256449, -4.348439, -2.746953, -2.411052,  1.217597, -2.277699, -1.872650,  0.979197, -0.551505,  0.914661,  1.118116};
	float jaspar_cne_pwm_213_matrix_row_1_[20] = { -1.918892, -1.477456, -1.631488,  1.408265, -2.505791, -2.728436,  1.258097, -2.169890, -0.453409, -2.323803, -2.169890,  1.359278,  1.346647, -2.728436, -1.631488, -1.226300, -0.938757, -0.651179, -1.344014, -1.477456};
	float jaspar_cne_pwm_213_matrix_row_2_[20] = { -2.169890,  1.363453, -2.169890, -2.505791, -2.505791,  1.350875, -1.551509, -0.981299, -2.728436, -4.107277, -4.107277, -3.419093, -4.107277, -3.015287, -1.718423, -1.477456, -1.551509, -1.120995, -0.039521, -1.172262};
	float jaspar_cne_pwm_213_matrix_row_3_[20] = {  1.198477, -1.792671,  1.186827, -1.524588, -1.959586, -2.746953, -0.694571, -1.792671,  1.075376,  1.236359,  1.240070, -0.861579, -0.802756, -2.277699,  1.155080,  1.101239, -0.831735,  0.845672, -2.160054, -2.411052};
	float *jaspar_cne_pwm_213_matrix[4] = { jaspar_cne_pwm_213_matrix_row_0_, jaspar_cne_pwm_213_matrix_row_1_, jaspar_cne_pwm_213_matrix_row_2_, jaspar_cne_pwm_213_matrix_row_3_};
	PWM jaspar_cne_pwm_213_ = {
/* accession        */ "CN0061",
/* name             */ "LM61",
/* label            */ " CN0061	24.9768450611339	LM61	unknown	; consensus \"TGTCAGCATTTCCATTATRA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGTCAGCATTTCCATTATAA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_213_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -52.878799,
/* max_score        */ 23.655380,
/* threshold        */ 0.868,
/* info content     */ 24.968212,
/* base_counts      */ {1558, 1165, 685, 2172},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 279
};

	float jaspar_cne_pwm_214_matrix_row_0_[14] = { -0.179229, -1.476093, -1.476093, -1.879898, -2.568082,  1.183400,  1.272328, -7.183203, -7.183203,  1.250826, -1.476093, -2.568082, -7.183203, -2.568082};
	float jaspar_cne_pwm_214_matrix_row_1_[14] = { -2.326920, -2.326920, -6.942041, -1.638736, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041,  1.447546, -2.326920, -6.942041,  1.424562};
	float jaspar_cne_pwm_214_matrix_row_2_[14] = {  1.064660, -6.942041,  1.424562, -6.942041, -6.942041, -1.638736, -6.942041, -6.942041,  1.424562, -2.326920, -6.942041, -6.942041, -6.942041, -6.942041};
	float jaspar_cne_pwm_214_matrix_row_3_[14] = { -0.966597,  1.183400, -2.568082,  1.183400,  1.250826, -1.879898, -7.183203,  1.272328, -1.189241, -7.183203, -7.183203,  1.228852,  1.272328, -1.476093};
	float *jaspar_cne_pwm_214_matrix[4] = { jaspar_cne_pwm_214_matrix_row_0_, jaspar_cne_pwm_214_matrix_row_1_, jaspar_cne_pwm_214_matrix_row_2_, jaspar_cne_pwm_214_matrix_row_3_};
	PWM jaspar_cne_pwm_214_ = {
/* accession        */ "CN0171",
/* name             */ "LM171",
/* label            */ " CN0171	22.8012682354102	LM171	unknown	; consensus \"RTGTTAATGACTTC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GTGTTAATGACTTC",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_214_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -94.020432,
/* max_score        */ 17.883579,
/* threshold        */ 0.790,
/* info content     */ 22.720070,
/* base_counts      */ {161, 92, 119, 286},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_215_matrix_row_0_[16] = { -1.983019,  1.171591,  1.233460, -7.976980, -7.976980, -0.663093, -3.361859, -0.732038,  1.233460,  0.817997, -2.269870, -7.976980, -7.976980,  1.253261,  0.342006,  0.520215};
	float jaspar_cne_pwm_215_matrix_row_1_[16] = {  1.358101, -1.049957, -3.120697, -7.735818, -1.183310, -7.735818, -2.028708,  1.358101, -2.028708, -3.120697, -3.120697, -7.735818,  1.513839, -7.735818, -1.741856, -1.049957};
	float jaspar_cne_pwm_215_matrix_row_2_[16] = { -2.432513, -7.735818, -7.735818, -7.735818, -2.028708, -2.028708,  1.474622, -3.120697, -7.735818, -3.120697, -7.735818, -2.028708, -7.735818, -7.735818,  0.236993, -3.120697};
	float jaspar_cne_pwm_215_matrix_row_3_[16] = { -1.173475, -2.673675, -2.269870,  1.272677,  1.171591,  1.082654, -7.976980, -7.976980, -3.361859,  0.211987,  1.233460,  1.243410, -7.976980, -2.673675,  0.029721,  0.457049};
	float *jaspar_cne_pwm_215_matrix[4] = { jaspar_cne_pwm_215_matrix_row_0_, jaspar_cne_pwm_215_matrix_row_1_, jaspar_cne_pwm_215_matrix_row_2_, jaspar_cne_pwm_215_matrix_row_3_};
	PWM jaspar_cne_pwm_215_ = {
/* accession        */ "CN0075",
/* name             */ "LM75",
/* label            */ " CN0075	23.1321356507585	LM75	unknown	; consensus \"CAATTTGCAATTCADW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CAATTTGCAATTCANW",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_215_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -104.692551,
/* max_score        */ 18.280447,
/* threshold        */ 0.808,
/* info content     */ 23.095823,
/* base_counts      */ {589, 318, 143, 614},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 104
};

	float jaspar_cne_pwm_216_matrix_row_0_[15] = {  1.141762,  1.230690, -7.224841, -2.609720,  1.272354, -7.224841, -0.672333, -7.224841, -7.224841, -7.224841, -7.224841,  1.209188,  1.164746,  1.209188, -2.609720};
	float jaspar_cne_pwm_216_matrix_row_1_[15] = { -2.368558, -1.680374, -6.983679,  1.428376, -6.983679, -6.983679, -6.983679, -6.983679, -0.431171, -6.983679, -6.983679, -6.983679, -0.767073, -6.983679, -6.983679};
	float jaspar_cne_pwm_216_matrix_row_2_[15] = { -0.767073, -6.983679, -6.983679, -2.368558, -6.983679, -0.431171,  1.148146, -6.983679, -0.767073, -6.983679,  1.405908, -1.276569, -6.983679, -6.983679,  1.335307};
	float jaspar_cne_pwm_216_matrix_row_3_[15] = { -7.224841, -7.224841,  1.272354, -1.921536, -7.224841,  1.118237, -0.538980,  1.272354,  0.991517,  1.272354, -1.008235, -7.224841, -7.224841, -1.517731, -0.672333};
	float *jaspar_cne_pwm_216_matrix[4] = { jaspar_cne_pwm_216_matrix_row_0_, jaspar_cne_pwm_216_matrix_row_1_, jaspar_cne_pwm_216_matrix_row_2_, jaspar_cne_pwm_216_matrix_row_3_};
	PWM jaspar_cne_pwm_216_ = {
/* accession        */ "CN0224",
/* name             */ "LM224",
/* label            */ " CN0224	23.4329035924821	LM224	unknown	; consensus \"AATCATGTTTGAAAG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AATCATGTTTGAAAG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_216_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -103.034004,
/* max_score        */ 18.472481,
/* threshold        */ 0.801,
/* info content     */ 23.345957,
/* base_counts      */ {284, 60, 140, 251},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_217_matrix_row_0_[14] = { -1.921536,  1.118237, -1.921536, -1.921536, -7.224841, -7.224841, -7.224841,  1.251739, -2.609720,  1.094145, -1.921536, -1.517731,  1.272354,  1.230690};
	float jaspar_cne_pwm_217_matrix_row_1_[14] = {  1.450350, -0.767073, -6.983679, -2.368558,  1.513516, -6.983679, -6.983679, -6.983679, -6.983679, -0.767073, -1.680374,  1.310621, -6.983679, -6.983679};
	float jaspar_cne_pwm_217_matrix_row_2_[14] = { -6.983679, -6.983679, -2.368558,  1.428376, -6.983679, -6.983679,  1.513516, -2.368558,  1.492901, -6.983679, -1.680374, -0.585084, -6.983679, -6.983679};
	float jaspar_cne_pwm_217_matrix_row_3_[14] = { -2.609720, -1.921536,  1.209188, -2.609720, -7.224841,  1.272354, -7.224841, -7.224841, -7.224841, -1.517731,  1.141762, -7.224841, -7.224841, -1.921536};
	float *jaspar_cne_pwm_217_matrix[4] = { jaspar_cne_pwm_217_matrix_row_0_, jaspar_cne_pwm_217_matrix_row_1_, jaspar_cne_pwm_217_matrix_row_2_, jaspar_cne_pwm_217_matrix_row_3_};
	PWM jaspar_cne_pwm_217_ = {
/* accession        */ "CN0166",
/* name             */ "LM166",
/* label            */ " CN0166	23.0591356107137	LM166	unknown	; consensus \"CATGCTGAGATCAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATGCTGAGATCAA",
/* library_name     */ "jaspar_cne",
/* length           */ 14,
/* matrixname       */ jaspar_cne_pwm_217_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.023537,
/* max_score        */ 18.299749,
/* threshold        */ 0.791,
/* info content     */ 22.980631,
/* base_counts      */ {239, 148, 152, 147},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_218_matrix_row_0_[20] = { -0.763120, -0.763120, -0.157741,  0.970110,  1.272314, -2.546595, -1.167754,  0.970110, -0.763120, -1.167754,  0.152172,  1.227872,  1.181363,  1.054643, -1.454605,  1.132584,  1.132584,  1.181363, -7.161715, -1.454605};
	float jaspar_cne_pwm_218_matrix_row_1_[20] = { -6.920553, -6.920553, -2.305433, -0.926592, -6.920553, -6.920553,  1.373747, -2.305433, -0.926592, -2.305433, -6.920553, -6.920553, -6.920553, -2.305433,  1.295805, -0.926592, -2.305433, -6.920553, -6.920553, -6.920553};
	float jaspar_cne_pwm_218_matrix_row_2_[20] = { -0.117048,  1.268414,  1.118927, -0.521958, -6.920553,  1.491502, -6.920553, -2.305433,  1.052258, -1.213443,  1.118927, -2.305433, -0.926592, -0.521958, -1.213443, -6.920553, -0.926592, -1.213443, -0.926592,  1.398433};
	float jaspar_cne_pwm_218_matrix_row_3_[20] = {  0.877765, -1.167754, -1.454605, -1.858410, -7.161715, -7.161715, -1.858410, -0.252960, -0.609207,  1.081304, -7.161715, -2.546595, -7.161715, -1.858410, -1.454605, -1.858410, -2.546595, -2.546595,  1.181363, -1.858410};
	float *jaspar_cne_pwm_218_matrix[4] = { jaspar_cne_pwm_218_matrix_row_0_, jaspar_cne_pwm_218_matrix_row_1_, jaspar_cne_pwm_218_matrix_row_2_, jaspar_cne_pwm_218_matrix_row_3_};
	PWM jaspar_cne_pwm_218_ = {
/* accession        */ "CN0232",
/* name             */ "LM232",
/* label            */ " CN0232	24.4994736532216	LM232	unknown	; consensus \"TGGAAGCAGTGAAACAAATG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGGAAGCAGTGAAACAAATG",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_218_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.260376,
/* max_score        */ 23.381386,
/* threshold        */ 0.861,
/* info content     */ 24.421942,
/* base_counts      */ {418, 94, 257, 151},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 46
};

	float jaspar_cne_pwm_219_matrix_row_0_[20] = {  0.959426, -1.936050, -3.436251, -2.053695, -0.766745, -2.745594,  1.222248, -2.522949, -0.163905, -2.340961, -1.138153,  1.198382, -3.032445, -3.436251, -1.494614, -1.089387, -2.187048,  1.217520, -1.648646,  0.557054};
	float jaspar_cne_pwm_219_matrix_row_1_[20] = { -0.948258, -2.504432, -2.281787, -3.195089, -1.407484,  1.444363, -3.195089, -1.812533,  0.355415, -2.281787,  0.894352, -2.791283, -3.883273, -1.812533, -3.883273, -1.407484,  1.434701, -3.195089, -1.184507, -1.002296};
	float jaspar_cne_pwm_219_matrix_row_2_[20] = { -1.184507, -2.281787, -1.945886, -2.791283,  1.288616, -8.498394, -8.498394, -8.498394,  0.038798, -1.945886,  0.311618, -1.694888, -3.883273, -8.498394, -3.883273,  1.332577, -8.498394, -3.883273, -3.195089,  0.058213};
	float jaspar_cne_pwm_219_matrix_row_3_[20] = { -0.875904,  1.188673,  1.207997,  1.212770, -2.745594, -1.735582, -1.936050,  1.212770, -0.242361,  1.188673, -1.361172, -2.745594,  1.250155,  1.226954,  1.198382, -2.745594, -1.830801, -1.936050,  1.133524, -0.349969};
	float *jaspar_cne_pwm_219_matrix[4] = { jaspar_cne_pwm_219_matrix_row_0_, jaspar_cne_pwm_219_matrix_row_1_, jaspar_cne_pwm_219_matrix_row_2_, jaspar_cne_pwm_219_matrix_row_3_};
	PWM jaspar_cne_pwm_219_ = {
/* accession        */ "CN0021",
/* name             */ "LM21",
/* label            */ " CN0021	25.6661940201692	LM21	unknown	; consensus \"ATTTGCATNTCATTTGCATW\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "ATTTGCATNTCATTTGCATN",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_219_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -80.886429,
/* max_score        */ 22.724550,
/* threshold        */ 0.878,
/* info content     */ 25.652733,
/* base_counts      */ {1097, 721, 585, 2057},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 223
};

	float jaspar_cne_pwm_220_matrix_row_0_[15] = { -0.518538,  1.219944, -3.074306, -7.689426, -3.074306,  1.272581,  1.272581,  1.272581, -7.689426, -3.074306,  1.259679, -1.695464, -7.689426, -1.003565, -0.598516};
	float jaspar_cne_pwm_220_matrix_row_1_[15] = {  0.941323, -2.144959, -7.448264, -7.448264,  1.433711, -7.448264, -7.448264, -7.448264, -7.448264, -2.144959, -7.448264, -1.454302,  1.200133,  1.315946,  0.894814};
	float jaspar_cne_pwm_220_matrix_row_2_[15] = { -2.144959, -7.448264, -2.833143,  1.513743, -2.833143, -7.448264, -7.448264, -7.448264, -7.448264,  1.474528, -7.448264,  1.405544, -0.203322, -1.741154, -7.448264};
	float jaspar_cne_pwm_220_matrix_row_3_[15] = { -0.139291, -2.386121,  1.246609, -7.689426, -1.695464, -7.689426, -7.689426, -7.689426,  1.272581, -7.689426, -3.074306, -7.689426, -1.136918, -1.982316,  0.094215};
	float *jaspar_cne_pwm_220_matrix[4] = { jaspar_cne_pwm_220_matrix_row_0_, jaspar_cne_pwm_220_matrix_row_1_, jaspar_cne_pwm_220_matrix_row_2_, jaspar_cne_pwm_220_matrix_row_3_};
	PWM jaspar_cne_pwm_220_ = {
/* accession        */ "CN0037",
/* name             */ "LM37",
/* label            */ " CN0037	23.0541404787858	LM37	unknown	; consensus \"YATGCAAATGAGCCM\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CATGCAAATGAGCCC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_220_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -98.510040,
/* max_score        */ 18.996298,
/* threshold        */ 0.799,
/* info content     */ 23.002243,
/* base_counts      */ {425, 287, 244, 214},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 78
};

	float jaspar_cne_pwm_221_matrix_row_0_[23] = { -1.578442, -1.534010, -1.143284, -1.235629,  1.030538, -1.624940, -1.578442,  0.150424,  1.104644, -1.673707, -0.287787, -2.876514, -2.876514,  1.224492,  1.138234,  0.623416, -1.302298, -0.626712, -0.313102,  1.173635, -0.006405, -1.114305, -1.032089};
	float jaspar_cne_pwm_221_matrix_row_1_[23] = { -0.477341,  1.202471,  1.108597,  1.195277, -1.292848,  0.271795, -1.292848, -1.789005, -2.481439,  1.307828, -1.594975, -1.209501,  1.188031, -2.817341, -2.230442, -3.326837, -3.326837, -2.817341, -1.170295, -2.635352, -1.483811,  0.665035, -0.403246};
	float jaspar_cne_pwm_221_matrix_row_2_[23] = { -1.250306, -1.537850, -3.326837, -2.348086, -1.594975, -1.061136, -1.383778,  1.030851, -0.621892, -2.029973, -0.790927, -2.348086, -3.039985, -2.481439, -0.931966, -3.730642,  1.400198,  1.298104,  1.147210, -1.483811,  1.035097, -1.209501, -1.789005};
	float jaspar_cne_pwm_221_matrix_row_3_[23] = {  0.975544, -0.558901, -0.170018, -0.495398, -0.932031,  0.728269,  1.082666, -2.589248, -2.722601, -0.980809,  0.835433,  1.164901, -0.105486, -2.876514, -2.876514,  0.506267, -2.471604, -2.184199, -2.104220, -2.271135, -1.673707,  0.392720,  0.939570};
	float *jaspar_cne_pwm_221_matrix[4] = { jaspar_cne_pwm_221_matrix_row_0_, jaspar_cne_pwm_221_matrix_row_1_, jaspar_cne_pwm_221_matrix_row_2_, jaspar_cne_pwm_221_matrix_row_3_};
	PWM jaspar_cne_pwm_221_ = {
/* accession        */ "CN0040",
/* name             */ "LM40",
/* label            */ " CN0040	21.8880124085836	LM40	unknown	; consensus \"TCCCATTGACTTCAAWGGGAGYT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TCCCATTGACTTCAAAGGGAGYT",
/* library_name     */ "jaspar_cne",
/* length           */ 23,
/* matrixname       */ jaspar_cne_pwm_221_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -53.482498,
/* max_score        */ 24.600044,
/* threshold        */ 0.841,
/* info content     */ 21.882536,
/* base_counts      */ {2593, 1961, 1708, 2501},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 381
};

	float jaspar_cne_pwm_222_matrix_row_0_[20] = {  0.200046, -6.970843,  0.343044,  0.274099, -2.355722, -6.970843, -6.970843, -6.970843, -1.263733, -6.970843, -6.970843, -0.976881,  1.218124,  1.245515,  1.245515,  1.001968, -1.667538, -2.355722,  1.068637,  1.218124};
	float jaspar_cne_pwm_222_matrix_row_1_[20] = { -6.729681, -6.729681, -2.114560,  1.011418,  0.871722, -6.729681, -2.114560, -6.729681, -6.729681, -6.729681,  1.459286,  1.372300, -6.729681, -6.729681, -2.114560, -6.729681, -2.114560,  1.243130, -0.177173, -6.729681};
	float jaspar_cne_pwm_222_matrix_row_2_[20] = { -0.331086,  1.277020,  0.920488, -6.729681, -2.114560, -2.114560, -6.729681, -2.114560,  1.011418,  1.486678, -6.729681, -6.729681, -2.114560, -2.114560, -6.729681, -0.177173,  1.171697, -6.729681, -6.729681, -2.114560};
	float jaspar_cne_pwm_222_matrix_row_3_[20] = {  0.579293, -0.284982, -2.355722, -2.355722,  0.407541,  1.245515,  1.245515,  1.245515,  0.120067, -2.355722, -1.667538, -2.355722, -2.355722, -6.970843, -6.970843, -1.667538, -0.284982, -0.284982, -6.970843, -2.355722};
	float *jaspar_cne_pwm_222_matrix[4] = { jaspar_cne_pwm_222_matrix_row_0_, jaspar_cne_pwm_222_matrix_row_1_, jaspar_cne_pwm_222_matrix_row_2_, jaspar_cne_pwm_222_matrix_row_3_};
	PWM jaspar_cne_pwm_222_ = {
/* accession        */ "CN0207",
/* name             */ "LM207",
/* label            */ " CN0207	26.4182429717377	LM207	unknown	; consensus \"WKRMYTTTKGCMAAAAKYMA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "WGGCCTTTGGCCAAAAGCAA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_222_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -123.401047,
/* max_score        */ 23.138882,
/* threshold        */ 0.889,
/* info content     */ 26.303961,
/* base_counts      */ {259, 152, 157, 192},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 38
};

	float jaspar_cne_pwm_223_matrix_row_0_[19] = { -1.577107, -1.980912, -1.290256, -1.980912, -7.284217, -1.577107, -2.669097, -2.669097,  1.252975,  1.192362,  1.212977, -1.577107, -2.669097, -1.577107, -1.067611,  1.171313, -0.885622, -0.885622,  1.010082};
	float jaspar_cne_pwm_223_matrix_row_1_[19] = { -2.427935, -7.043055, -0.134300,  1.346532, -1.049094, -1.739750, -1.739750,  1.346532, -2.427935, -2.427935, -2.427935, -1.739750, -0.644460,  1.275931,  1.412475, -7.043055, -7.043055, -0.826449, -1.049094};
	float jaspar_cne_pwm_223_matrix_row_2_[19] = { -1.335945, -1.049094,  1.088770, -7.043055, -7.043055, -0.826449, -1.739750, -1.335945, -7.043055, -1.739750, -1.739750, -2.427935,  1.199964, -1.335945, -7.043055, -2.427935,  1.251244,  1.251244, -0.644460};
	float jaspar_cne_pwm_223_matrix_row_3_[19] = {  1.127838,  1.149812, -1.290256, -0.885622,  1.192362,  1.058861,  1.171313, -1.290256, -7.284217, -2.669097, -7.284217,  1.149812, -0.731709, -1.067611, -7.284217, -1.290256, -0.885622, -2.669097, -1.980912};
	float *jaspar_cne_pwm_223_matrix[4] = { jaspar_cne_pwm_223_matrix_row_0_, jaspar_cne_pwm_223_matrix_row_1_, jaspar_cne_pwm_223_matrix_row_2_, jaspar_cne_pwm_223_matrix_row_3_};
	PWM jaspar_cne_pwm_223_ = {
/* accession        */ "CN0149",
/* name             */ "LM149",
/* label            */ " CN0149	23.4669238548993	LM149	unknown	; consensus \"TTGCTTTCAAATGCCAGGA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TTGCTTTCAAATGCCAGGA",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_223_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -82.098473,
/* max_score        */ 22.862396,
/* threshold        */ 0.837,
/* info content     */ 23.410181,
/* base_counts      */ {275, 215, 184, 314},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 52
};

	float jaspar_cne_pwm_224_matrix_row_0_[19] = { -0.231032,  0.655851, -7.475974,  1.223707, -2.172669, -1.768864, -2.172669, -2.860853, -1.768864, -1.482012, -1.482012, -1.768864, -1.768864,  0.958055,  1.154726, -1.077379, -7.475974, -0.672469, -1.259368};
	float jaspar_cne_pwm_224_matrix_row_1_[19] = { -0.682304,  0.366590,  1.497654, -1.527702, -1.018206,  1.377873, -7.234812, -7.234812, -0.431307, -1.931507, -7.234812, -1.527702,  1.359527,  0.143572, -1.527702, -2.619691, -7.234812, -7.234812, -1.018206};
	float jaspar_cne_pwm_224_matrix_row_2_[19] = {  0.925992, -1.018206, -7.234812, -7.234812, -0.682304, -1.931507, -7.234812,  1.497654,  1.154775, -1.931507,  0.666565,  1.177243, -1.018206, -7.234812, -2.619691, -7.234812, -1.931507, -7.234812,  1.199217};
	float jaspar_cne_pwm_224_matrix_row_3_[19] = { -0.923466, -1.482012, -2.860853, -7.475974,  1.021221, -1.768864,  1.240234, -7.475974, -0.923466,  1.136711,  0.595245, -0.385064, -2.860853, -2.860853, -1.768864,  1.154726,  1.240234,  1.118365, -0.923466};
	float *jaspar_cne_pwm_224_matrix[4] = { jaspar_cne_pwm_224_matrix_row_0_, jaspar_cne_pwm_224_matrix_row_1_, jaspar_cne_pwm_224_matrix_row_2_, jaspar_cne_pwm_224_matrix_row_3_};
	PWM jaspar_cne_pwm_224_ = {
/* accession        */ "CN0084",
/* name             */ "LM84",
/* label            */ " CN0084	22.6877368350239	LM84	unknown	; consensus \"GACATCTGGTKGCAATTTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "GACATCTGGTTGCAATTTG",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_224_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -84.796753,
/* max_score        */ 21.760334,
/* threshold        */ 0.827,
/* info content     */ 22.636061,
/* base_counts      */ {255, 245, 283, 414},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 63
};

	float jaspar_cne_pwm_225_matrix_row_0_[19] = {  1.220524, -2.657019,  1.147690, -1.869651, -2.187764, -3.570320, -3.570320, -8.873625,  1.203858, -3.570320, -8.873625, -8.873625,  0.775035, -2.070120, -1.049179, -8.873625,  0.591435,  0.768563, -0.019817};
	float jaspar_cne_pwm_225_matrix_row_1_[19] = { -2.925353,  0.033323, -3.329158,  0.192362, -0.659652, -8.632463,  1.490200,  1.510081, -1.541553,  1.060365,  1.514010, -8.632463, -3.329158,  1.250873, -1.946602, -8.632463, -1.723708, -0.001762, -0.075857};
	float jaspar_cne_pwm_225_matrix_row_2_[19] = { -2.638501,  1.029017, -0.935796,  1.009725, -3.329158,  1.474006, -3.329158, -8.632463, -4.017342, -8.632463, -8.632463, -8.632463,  0.276908, -0.338163, -8.632463,  1.514010,  0.654931, -1.136365, -1.318576};
	float jaspar_cne_pwm_225_matrix_row_3_[19] = { -2.475030, -0.712821, -2.475030, -1.176958,  1.106870, -2.187764, -3.570320, -4.258504, -2.879663,  0.242515, -8.873625,  1.272848, -1.089984, -1.964870,  1.134268, -8.873625, -2.187764, -0.972248,  0.502315};
	float *jaspar_cne_pwm_225_matrix[4] = { jaspar_cne_pwm_225_matrix_row_0_, jaspar_cne_pwm_225_matrix_row_1_, jaspar_cne_pwm_225_matrix_row_2_, jaspar_cne_pwm_225_matrix_row_3_};
	PWM jaspar_cne_pwm_225_ = {
/* accession        */ "CN0047",
/* name             */ "LM47",
/* label            */ " CN0047	23.731873920837	LM47	unknown	; consensus \"AGAGTGCCACCTACTGAAT\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGAGTGCCACCTACTGAAN",
/* library_name     */ "jaspar_cne",
/* length           */ 19,
/* matrixname       */ jaspar_cne_pwm_225_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -93.131874,
/* max_score        */ 21.639193,
/* threshold        */ 0.840,
/* info content     */ 23.717236,
/* base_counts      */ {1277, 1416, 1097, 1055},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 255
};

	float jaspar_cne_pwm_226_matrix_row_0_[17] = {  0.405142, -7.245027, -1.251066, -1.941722, -7.245027, -7.245027, -7.245027,  1.272366, -1.941722, -1.251066, -7.245027, -7.245027, -7.245027, -0.846432,  1.252167, -2.629907, -2.629907};
	float jaspar_cne_pwm_226_matrix_row_1_[17] = { -7.003865, -7.003865,  1.430164, -7.003865, -7.003865, -2.388745,  1.493329, -7.003865, -1.700560, -2.388745, -2.388745, -1.700560, -7.003865,  1.265123, -2.388745, -2.388745, -2.388745};
	float jaspar_cne_pwm_226_matrix_row_2_[17] = {  0.000109,  1.493329, -7.003865, -7.003865,  1.451665, -7.003865, -2.388745, -7.003865,  0.374519, -7.003865, -7.003865,  0.737234, -0.318004, -0.787259, -7.003865, -2.388745, -0.787259};
	float jaspar_cne_pwm_226_matrix_row_3_[17] = {  0.251070, -2.629907, -7.245027,  1.231552, -1.537917,  1.252167, -7.245027, -7.245027,  0.761674,  1.167028,  1.252167,  0.579419,  1.098051, -7.245027, -7.245027,  1.210503,  1.121576};
	float *jaspar_cne_pwm_226_matrix[4] = { jaspar_cne_pwm_226_matrix_row_0_, jaspar_cne_pwm_226_matrix_row_1_, jaspar_cne_pwm_226_matrix_row_2_, jaspar_cne_pwm_226_matrix_row_3_};
	PWM jaspar_cne_pwm_226_ = {
/* accession        */ "CN0123",
/* name             */ "LM123",
/* label            */ " CN0123	24.9212641781074	LM123	unknown	; consensus \"WGCTGTCAKTTKTMATK\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "WGCTGTCATTTKTCATT",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_226_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -107.908417,
/* max_score        */ 19.895237,
/* threshold        */ 0.832,
/* info content     */ 24.836906,
/* base_counts      */ {140, 144, 166, 400},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 50
};

	float jaspar_cne_pwm_227_matrix_row_0_[17] = {  1.164746, -0.826246, -1.517731, -7.224841, -7.224841, -0.672333,  1.141762,  1.251739,  1.230690, -1.230879, -7.224841, -7.224841, -1.517731,  0.906984, -1.230879, -7.224841, -1.921536};
	float jaspar_cne_pwm_227_matrix_row_1_[17] = { -2.368558, -6.983679,  1.310621, -6.983679, -2.368558,  1.359399, -6.983679, -6.983679, -6.983679, -0.431171, -6.983679, -6.983679,  1.382924, -0.297818, -0.989717, -0.989717,  1.148146};
	float jaspar_cne_pwm_227_matrix_row_2_[17] = { -1.680374,  1.382924, -0.767073, -6.983679,  1.492901, -6.983679, -6.983679, -6.983679, -1.680374, -0.767073, -6.983679,  1.450350, -2.368558, -6.983679, -0.989717, -2.368558, -0.585084};
	float jaspar_cne_pwm_227_matrix_row_3_[17] = { -1.921536, -7.224841, -2.609720,  1.272354, -7.224841, -7.224841, -0.826246, -2.609720, -7.224841,  0.877140,  1.272354, -1.517731, -1.921536, -0.672333,  0.991517,  1.164746, -0.672333};
	float *jaspar_cne_pwm_227_matrix[4] = { jaspar_cne_pwm_227_matrix_row_0_, jaspar_cne_pwm_227_matrix_row_1_, jaspar_cne_pwm_227_matrix_row_2_, jaspar_cne_pwm_227_matrix_row_3_};
	PWM jaspar_cne_pwm_227_ = {
/* accession        */ "CN0121",
/* name             */ "LM121",
/* label            */ " CN0121	23.5111886114074	LM121	unknown	; consensus \"AGCTGCAAATTGCATTC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGCTGCAAATTGCATTC",
/* library_name     */ "jaspar_cne",
/* length           */ 17,
/* matrixname       */ jaspar_cne_pwm_227_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -90.479904,
/* max_score        */ 20.801294,
/* threshold        */ 0.819,
/* info content     */ 23.433804,
/* base_counts      */ {245, 184, 163, 241},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 49
};

	float jaspar_cne_pwm_228_matrix_row_0_[15] = { -2.568082, -1.189241, -7.183203,  1.250826, -7.183203, -2.568082, -2.568082, -7.183203, -7.183203,  1.272328, -1.879898,  1.111097, -1.879898, -2.568082, -0.966597};
	float jaspar_cne_pwm_228_matrix_row_1_[15] = {  1.352259, -6.942041,  1.470014, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041,  1.513490, -6.942041, -6.942041, -2.326920, -6.942041, -0.543446,  1.352259};
	float jaspar_cne_pwm_228_matrix_row_2_[15] = { -6.942041, -6.942041, -2.326920, -6.942041, -6.942041, -6.942041, -6.942041,  1.424562, -6.942041, -6.942041, -6.942041, -6.942041, -6.942041, -0.543446, -2.326920};
	float jaspar_cne_pwm_228_matrix_row_3_[15] = { -0.784608,  1.183400, -2.568082, -2.568082,  1.272328,  1.250826,  1.250826, -1.189241, -7.183203, -7.183203,  1.228852, -0.784608,  1.228852,  0.948622, -2.568082};
	float *jaspar_cne_pwm_228_matrix[4] = { jaspar_cne_pwm_228_matrix_row_0_, jaspar_cne_pwm_228_matrix_row_1_, jaspar_cne_pwm_228_matrix_row_2_, jaspar_cne_pwm_228_matrix_row_3_};
	PWM jaspar_cne_pwm_228_ = {
/* accession        */ "CN0112",
/* name             */ "LM112",
/* label            */ " CN0112	24.5271536804108	LM112	unknown	; consensus \"CTCATTTGCATATTC\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTCATTTGCATATTC",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_228_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -96.588509,
/* max_score        */ 19.110538,
/* threshold        */ 0.806,
/* info content     */ 24.437588,
/* base_counts      */ {150, 179, 51, 325},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 47
};

	float jaspar_cne_pwm_229_matrix_row_0_[15] = { -0.131022, -0.823812, -0.244308, -1.852148,  1.263893,  1.272703,  1.255005, -2.765449, -3.453633,  1.218640, -1.516246, -2.765449, -8.068754, -8.068754, -0.897865};
	float jaspar_cne_pwm_229_matrix_row_1_[15] = { -7.827592, -2.524287, -7.827592, -3.212471, -3.212471, -7.827592, -7.827592, -7.827592, -1.610986, -7.827592, -1.610986, -0.513705, -7.827592, -7.827592, -2.120481};
	float jaspar_cne_pwm_229_matrix_row_2_[15] = { -2.120481,  1.266327,  0.110140,  1.450501, -7.827592, -7.827592, -2.524287, -7.827592,  1.450501, -1.428997,  1.299476, -1.610986, -1.833630,  1.422065,  1.320980};
	float jaspar_cne_pwm_229_matrix_row_3_[15] = {  0.955378, -1.265249,  0.647454, -3.453633, -8.068754, -8.068754, -8.068754,  1.255005, -3.453633, -8.068754, -1.159999,  1.058314,  1.236988, -1.159999, -2.074792};
	float *jaspar_cne_pwm_229_matrix[4] = { jaspar_cne_pwm_229_matrix_row_0_, jaspar_cne_pwm_229_matrix_row_1_, jaspar_cne_pwm_229_matrix_row_2_, jaspar_cne_pwm_229_matrix_row_3_};
	PWM jaspar_cne_pwm_229_ = {
/* accession        */ "CN0208",
/* name             */ "LM208",
/* label            */ " CN0208	21.6170213397895	LM208	unknown	; consensus \"TGTGAAATGAGTTGG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "TGTGAAATGAGTTGG",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_229_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -87.823769,
/* max_score        */ 18.373228,
/* threshold        */ 0.792,
/* info content     */ 21.586054,
/* base_counts      */ {544, 32, 641, 493},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 114
};

	float jaspar_cne_pwm_230_matrix_row_0_[15] = {  0.841375, -0.627437,  1.124565,  1.162376, -2.082360,  1.257853, -3.333933, -2.561640, -3.333933, -3.333933, -3.738567, -4.025418, -2.641618, -3.180021, -0.214630};
	float jaspar_cne_pwm_230_matrix_row_1_[15] = { -3.092771, -0.542261, -0.860666, -1.553635,  1.473395, -3.497405, -2.052395,  1.471663, -4.876246, -4.188061, -1.101779, -1.941231, -2.177480,  1.184565,  0.564885};
	float jaspar_cne_pwm_230_matrix_row_2_[15] = { -0.342795,  1.154082, -2.246425, -1.553635, -4.876246, -9.491366,  0.272917, -3.497405, -9.491366,  1.497327, -2.805505, -1.794699, -1.222378, -0.623375, -2.687861};
	float jaspar_cne_pwm_230_matrix_row_3_[15] = { -0.417738, -2.418642, -2.561640, -3.180021, -4.429224, -3.515922,  0.876554, -3.046667,  1.261220, -4.429224,  1.175279,  1.197019,  1.156795, -0.616389,  0.284332};
	float *jaspar_cne_pwm_230_matrix[4] = { jaspar_cne_pwm_230_matrix_row_0_, jaspar_cne_pwm_230_matrix_row_1_, jaspar_cne_pwm_230_matrix_row_2_, jaspar_cne_pwm_230_matrix_row_3_};
	PWM jaspar_cne_pwm_230_ = {
/* accession        */ "CN0028",
/* name             */ "LM28",
/* label            */ " CN0028	19.4530351759717	LM28	unknown	; consensus \"AGAACATCTGTTTCY\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AGAACATCTGTTTCY",
/* library_name     */ "jaspar_cne",
/* length           */ 15,
/* matrixname       */ jaspar_cne_pwm_230_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -62.646095,
/* max_score        */ 17.398954,
/* threshold        */ 0.781,
/* info content     */ 19.449234,
/* base_counts      */ {2346, 2090, 1476, 3118},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 602
};

	float jaspar_cne_pwm_231_matrix_row_0_[20] = { -1.750716, -2.664017, -7.967322, -3.352201, -1.163816,  1.115299, -0.653435, -0.471224, -3.352201, -2.260211, -2.664017, -7.967322, -7.967322, -0.963348, -1.568727,  1.212662,  0.732360, -0.963348,  1.068784,  1.222918};
	float jaspar_cne_pwm_231_matrix_row_1_[20] = {  1.367759, -1.327565, -2.019049, -1.327565, -1.732198, -3.111039, -2.019049,  0.211572, -1.040299,  1.356461,  1.389980, -1.173652, -1.509554, -7.726160, -7.726160, -2.422855, -3.111039, -3.111039, -2.422855, -7.726160};
	float jaspar_cne_pwm_231_matrix_row_2_[20] = { -3.111039, -7.726160, -3.111039, -7.726160,  1.285852, -1.040299,  1.273583, -0.481218, -3.111039, -3.111039, -7.726160, -2.422855, -0.230062,  1.389980,  1.443463, -2.019049, -0.287188,  1.321779, -0.347776, -1.509554};
	float jaspar_cne_pwm_231_matrix_row_3_[20] = { -1.281461,  1.191831,  1.233069,  1.202301, -1.281461, -1.568727, -1.973360,  0.399281,  1.170555, -0.963348, -1.058567,  1.181250,  1.020000, -3.352201, -3.352201, -3.352201, -0.142876, -1.568727, -3.352201, -7.967322};
	float *jaspar_cne_pwm_231_matrix[4] = { jaspar_cne_pwm_231_matrix_row_0_, jaspar_cne_pwm_231_matrix_row_1_, jaspar_cne_pwm_231_matrix_row_2_, jaspar_cne_pwm_231_matrix_row_3_};
	PWM jaspar_cne_pwm_231_ = {
/* accession        */ "CN0213",
/* name             */ "LM213",
/* label            */ " CN0213	25.3025047912525	LM213	unknown	; consensus \"CTTTGAGTTCCTTGGAAGAA\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "CTTTGAGNTCCTTGGAAGAA",
/* library_name     */ "jaspar_cne",
/* length           */ 20,
/* matrixname       */ jaspar_cne_pwm_231_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -100.344353,
/* max_score        */ 23.579165,
/* threshold        */ 0.872,
/* info content     */ 25.270662,
/* base_counts      */ {511, 345, 522, 682},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 103
};

	float jaspar_cne_pwm_232_matrix_row_0_[16] = {  0.984678,  1.249300,  1.071664, -2.502182, -7.117303,  1.176997,  1.272284,  0.922177, -7.117303, -7.117303, -7.117303,  1.225775, -7.117303, -7.117303, -7.117303, -1.813998};
	float jaspar_cne_pwm_232_matrix_row_1_[16] = { -1.572836, -6.876141, -0.477546, -6.876141, -6.876141, -1.572836, -6.876141, -2.261020, -6.876141, -0.882179,  1.513446, -2.261020, -6.876141, -2.261020, -6.876141, -0.477546};
	float jaspar_cne_pwm_232_matrix_row_2_[16] = { -1.572836, -2.261020, -6.876141, -2.261020,  1.513446, -1.572836, -6.876141, -6.876141, -6.876141, -1.572836, -6.876141, -6.876141, -2.261020, -6.876141, -6.876141,  1.225840};
	float jaspar_cne_pwm_232_matrix_row_3_[16] = { -0.564795, -7.117303, -1.813998,  1.225775, -7.117303, -7.117303, -7.117303, -0.026393,  1.272284,  1.125717, -7.117303, -2.502182,  1.249300,  1.249300,  1.272284, -1.410193};
	float *jaspar_cne_pwm_232_matrix[4] = { jaspar_cne_pwm_232_matrix_row_0_, jaspar_cne_pwm_232_matrix_row_1_, jaspar_cne_pwm_232_matrix_row_2_, jaspar_cne_pwm_232_matrix_row_3_};
	PWM jaspar_cne_pwm_232_ = {
/* accession        */ "CN0172",
/* name             */ "LM172",
/* label            */ " CN0172	25.5442989154849	LM172	unknown	; consensus \"AAATGAAATTCATTTG\" ; medline \"17442748\" ; species \"Homo sapiens\" ",
/* motif            */ "AAATGAAATTCATTTG",
/* library_name     */ "jaspar_cne",
/* length           */ 16,
/* matrixname       */ jaspar_cne_pwm_232_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -102.064415,
/* max_score        */ 19.550266,
/* threshold        */ 0.824,
/* info content     */ 25.445625,
/* base_counts      */ {272, 67, 86, 279},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 44
};

PWM *jaspar_cne[JASPAR_CNE_N] = { &jaspar_cne_pwm_0_, &jaspar_cne_pwm_1_, &jaspar_cne_pwm_2_, &jaspar_cne_pwm_3_, &jaspar_cne_pwm_4_, &jaspar_cne_pwm_5_, &jaspar_cne_pwm_6_, &jaspar_cne_pwm_7_, &jaspar_cne_pwm_8_, &jaspar_cne_pwm_9_, &jaspar_cne_pwm_10_, &jaspar_cne_pwm_11_, &jaspar_cne_pwm_12_, &jaspar_cne_pwm_13_, &jaspar_cne_pwm_14_, &jaspar_cne_pwm_15_, &jaspar_cne_pwm_16_, &jaspar_cne_pwm_17_, &jaspar_cne_pwm_18_, &jaspar_cne_pwm_19_, &jaspar_cne_pwm_20_, &jaspar_cne_pwm_21_, &jaspar_cne_pwm_22_, &jaspar_cne_pwm_23_, &jaspar_cne_pwm_24_, &jaspar_cne_pwm_25_, &jaspar_cne_pwm_26_, &jaspar_cne_pwm_27_, &jaspar_cne_pwm_28_, &jaspar_cne_pwm_29_, &jaspar_cne_pwm_30_, &jaspar_cne_pwm_31_, &jaspar_cne_pwm_32_, &jaspar_cne_pwm_33_, &jaspar_cne_pwm_34_, &jaspar_cne_pwm_35_, &jaspar_cne_pwm_36_, &jaspar_cne_pwm_37_, &jaspar_cne_pwm_38_, &jaspar_cne_pwm_39_, &jaspar_cne_pwm_40_, &jaspar_cne_pwm_41_, &jaspar_cne_pwm_42_, &jaspar_cne_pwm_43_, &jaspar_cne_pwm_44_, &jaspar_cne_pwm_45_, &jaspar_cne_pwm_46_, &jaspar_cne_pwm_47_, &jaspar_cne_pwm_48_, &jaspar_cne_pwm_49_, &jaspar_cne_pwm_50_, &jaspar_cne_pwm_51_, &jaspar_cne_pwm_52_, &jaspar_cne_pwm_53_, &jaspar_cne_pwm_54_, &jaspar_cne_pwm_55_, &jaspar_cne_pwm_56_, &jaspar_cne_pwm_57_, &jaspar_cne_pwm_58_, &jaspar_cne_pwm_59_, &jaspar_cne_pwm_60_, &jaspar_cne_pwm_61_, &jaspar_cne_pwm_62_, &jaspar_cne_pwm_63_, &jaspar_cne_pwm_64_, &jaspar_cne_pwm_65_, &jaspar_cne_pwm_66_, &jaspar_cne_pwm_67_, &jaspar_cne_pwm_68_, &jaspar_cne_pwm_69_, &jaspar_cne_pwm_70_, &jaspar_cne_pwm_71_, &jaspar_cne_pwm_72_, &jaspar_cne_pwm_73_, &jaspar_cne_pwm_74_, &jaspar_cne_pwm_75_, &jaspar_cne_pwm_76_, &jaspar_cne_pwm_77_, &jaspar_cne_pwm_78_, &jaspar_cne_pwm_79_, &jaspar_cne_pwm_80_, &jaspar_cne_pwm_81_, &jaspar_cne_pwm_82_, &jaspar_cne_pwm_83_, &jaspar_cne_pwm_84_, &jaspar_cne_pwm_85_, &jaspar_cne_pwm_86_, &jaspar_cne_pwm_87_, &jaspar_cne_pwm_88_, &jaspar_cne_pwm_89_, &jaspar_cne_pwm_90_, &jaspar_cne_pwm_91_, &jaspar_cne_pwm_92_, &jaspar_cne_pwm_93_, &jaspar_cne_pwm_94_, &jaspar_cne_pwm_95_, &jaspar_cne_pwm_96_, &jaspar_cne_pwm_97_, &jaspar_cne_pwm_98_, &jaspar_cne_pwm_99_, &jaspar_cne_pwm_100_, &jaspar_cne_pwm_101_, &jaspar_cne_pwm_102_, &jaspar_cne_pwm_103_, &jaspar_cne_pwm_104_, &jaspar_cne_pwm_105_, &jaspar_cne_pwm_106_, &jaspar_cne_pwm_107_, &jaspar_cne_pwm_108_, &jaspar_cne_pwm_109_, &jaspar_cne_pwm_110_, &jaspar_cne_pwm_111_, &jaspar_cne_pwm_112_, &jaspar_cne_pwm_113_, &jaspar_cne_pwm_114_, &jaspar_cne_pwm_115_, &jaspar_cne_pwm_116_, &jaspar_cne_pwm_117_, &jaspar_cne_pwm_118_, &jaspar_cne_pwm_119_, &jaspar_cne_pwm_120_, &jaspar_cne_pwm_121_, &jaspar_cne_pwm_122_, &jaspar_cne_pwm_123_, &jaspar_cne_pwm_124_, &jaspar_cne_pwm_125_, &jaspar_cne_pwm_126_, &jaspar_cne_pwm_127_, &jaspar_cne_pwm_128_, &jaspar_cne_pwm_129_, &jaspar_cne_pwm_130_, &jaspar_cne_pwm_131_, &jaspar_cne_pwm_132_, &jaspar_cne_pwm_133_, &jaspar_cne_pwm_134_, &jaspar_cne_pwm_135_, &jaspar_cne_pwm_136_, &jaspar_cne_pwm_137_, &jaspar_cne_pwm_138_, &jaspar_cne_pwm_139_, &jaspar_cne_pwm_140_, &jaspar_cne_pwm_141_, &jaspar_cne_pwm_142_, &jaspar_cne_pwm_143_, &jaspar_cne_pwm_144_, &jaspar_cne_pwm_145_, &jaspar_cne_pwm_146_, &jaspar_cne_pwm_147_, &jaspar_cne_pwm_148_, &jaspar_cne_pwm_149_, &jaspar_cne_pwm_150_, &jaspar_cne_pwm_151_, &jaspar_cne_pwm_152_, &jaspar_cne_pwm_153_, &jaspar_cne_pwm_154_, &jaspar_cne_pwm_155_, &jaspar_cne_pwm_156_, &jaspar_cne_pwm_157_, &jaspar_cne_pwm_158_, &jaspar_cne_pwm_159_, &jaspar_cne_pwm_160_, &jaspar_cne_pwm_161_, &jaspar_cne_pwm_162_, &jaspar_cne_pwm_163_, &jaspar_cne_pwm_164_, &jaspar_cne_pwm_165_, &jaspar_cne_pwm_166_, &jaspar_cne_pwm_167_, &jaspar_cne_pwm_168_, &jaspar_cne_pwm_169_, &jaspar_cne_pwm_170_, &jaspar_cne_pwm_171_, &jaspar_cne_pwm_172_, &jaspar_cne_pwm_173_, &jaspar_cne_pwm_174_, &jaspar_cne_pwm_175_, &jaspar_cne_pwm_176_, &jaspar_cne_pwm_177_, &jaspar_cne_pwm_178_, &jaspar_cne_pwm_179_, &jaspar_cne_pwm_180_, &jaspar_cne_pwm_181_, &jaspar_cne_pwm_182_, &jaspar_cne_pwm_183_, &jaspar_cne_pwm_184_, &jaspar_cne_pwm_185_, &jaspar_cne_pwm_186_, &jaspar_cne_pwm_187_, &jaspar_cne_pwm_188_, &jaspar_cne_pwm_189_, &jaspar_cne_pwm_190_, &jaspar_cne_pwm_191_, &jaspar_cne_pwm_192_, &jaspar_cne_pwm_193_, &jaspar_cne_pwm_194_, &jaspar_cne_pwm_195_, &jaspar_cne_pwm_196_, &jaspar_cne_pwm_197_, &jaspar_cne_pwm_198_, &jaspar_cne_pwm_199_, &jaspar_cne_pwm_200_, &jaspar_cne_pwm_201_, &jaspar_cne_pwm_202_, &jaspar_cne_pwm_203_, &jaspar_cne_pwm_204_, &jaspar_cne_pwm_205_, &jaspar_cne_pwm_206_, &jaspar_cne_pwm_207_, &jaspar_cne_pwm_208_, &jaspar_cne_pwm_209_, &jaspar_cne_pwm_210_, &jaspar_cne_pwm_211_, &jaspar_cne_pwm_212_, &jaspar_cne_pwm_213_, &jaspar_cne_pwm_214_, &jaspar_cne_pwm_215_, &jaspar_cne_pwm_216_, &jaspar_cne_pwm_217_, &jaspar_cne_pwm_218_, &jaspar_cne_pwm_219_, &jaspar_cne_pwm_220_, &jaspar_cne_pwm_221_, &jaspar_cne_pwm_222_, &jaspar_cne_pwm_223_, &jaspar_cne_pwm_224_, &jaspar_cne_pwm_225_, &jaspar_cne_pwm_226_, &jaspar_cne_pwm_227_, &jaspar_cne_pwm_228_, &jaspar_cne_pwm_229_, &jaspar_cne_pwm_230_, &jaspar_cne_pwm_231_, &jaspar_cne_pwm_232_};
extern PWM *jaspar_cne[JASPAR_CNE_N];
