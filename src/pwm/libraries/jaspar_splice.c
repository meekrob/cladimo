#include "pwm.h"
#include "jaspar_splice.h"
/* created by ./build_library_code using JASPAR_SPLICE_2008.wmx on Tue Mar  3 16:19:03 2009
*/
	float jaspar_splice_pwm_0_matrix_row_0_[20] = { -0.629640, -0.715714, -0.792031, -0.887196, -0.975913, -1.079341, -1.159359, -1.149278, -1.014256, -0.927441, -0.873932, -1.158234, -1.120714, -0.150893, -1.543424,  1.272965, -14.499735, -0.071094, -0.128100, -0.050879};
	float jaspar_splice_pwm_0_matrix_row_1_[20] = {  0.196474,  0.193886,  0.230597,  0.222046,  0.211657,  0.214510,  0.152363,  0.225793,  0.271700,  0.370044,  0.402249,  0.424669,  0.271994,  0.200107,  1.056628, -14.258573, -14.258573, -0.440764, -0.155029,  0.041646};
	float jaspar_splice_pwm_0_matrix_row_2_[20] = { -0.465613, -0.488896, -0.540012, -0.584049, -0.629993, -0.736895, -0.760927, -0.708460, -0.658360, -0.746737, -0.881720, -1.230518, -1.249600, -0.093602, -4.683519, -14.258573,  1.514127,  0.789469, -0.138227,  0.048765};
	float jaspar_splice_pwm_0_matrix_row_3_[20] = {  0.463179,  0.498883,  0.513510,  0.554122,  0.590664,  0.632265,  0.680873,  0.632828,  0.571535,  0.510395,  0.503193,  0.602361,  0.687566,  0.035566,  0.086440, -14.499735, -14.499735, -0.906117,  0.288561, -0.023341};
	float *jaspar_splice_pwm_0_matrix[4] = { jaspar_splice_pwm_0_matrix_row_0_, jaspar_splice_pwm_0_matrix_row_1_, jaspar_splice_pwm_0_matrix_row_2_, jaspar_splice_pwm_0_matrix_row_3_};
	PWM jaspar_splice_pwm_0_ = {
/* accession        */ "SA0001",
/* name             */ "at_AC_acceptor",
/* label            */ " SA0001	8.98345403774424	at_AC_acceptor	Unknown	; description \"Non-canonical splice acceptor (3\') site (at_AC)\" ; medline \"15475254\" ",
/* motif            */ "NNNYTTTTYYYTTNCAGNNN",
/* library_name     */ "jaspar_splice",
/* length           */ 20,
/* matrixname       */ jaspar_splice_pwm_0_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -47.637913,
/* max_score        */ 12.611997,
/* threshold        */ 0.670,
/* info content     */ 8.983428,
/* base_counts      */ {246902, 367086, 248029, 553863},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 70794
};

	float jaspar_splice_pwm_1_matrix_row_0_[11] = {  0.373834,  1.100037, -2.324666, -9.638553, -9.638553,  1.189214,  1.073663, -2.835047, -1.369565,  0.197779, -0.351159};
	float jaspar_splice_pwm_1_matrix_row_1_[11] = {  0.491034, -1.958419, -4.782270, -9.397390,  1.514073, -2.393417, -2.019007, -2.593885, -1.078405, -0.488020,  0.127833};
	float jaspar_splice_pwm_1_matrix_row_2_[11] = { -0.238238, -1.795988,  1.469290,  1.514073, -9.397390, -1.656292, -0.920811,  1.471197, -1.030788,  0.485945, -0.013013};
	float jaspar_splice_pwm_1_matrix_row_3_[11] = { -1.536572, -1.121160, -2.952692, -9.638553, -9.638553, -2.729798, -1.477749, -3.421947,  1.018730, -0.522413,  0.181608};
	float *jaspar_splice_pwm_1_matrix[4] = { jaspar_splice_pwm_1_matrix_row_0_, jaspar_splice_pwm_1_matrix_row_1_, jaspar_splice_pwm_1_matrix_row_2_, jaspar_splice_pwm_1_matrix_row_3_};
	PWM jaspar_splice_pwm_1_ = {
/* accession        */ "SD0002",
/* name             */ "at_AC_acceptor",
/* label            */ " SD0002	12.3393140803366	at_AC_acceptor	Unknown	; description \"Non-canonical splice acceptor (3\') site (at_AC)\" ; medline \"15475254\" ",
/* motif            */ "MAGGCAAGTNN",
/* library_name     */ "jaspar_splice",
/* length           */ 11,
/* matrixname       */ jaspar_splice_pwm_1_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -37.968254,
/* max_score        */ 11.508863,
/* threshold        */ 0.795,
/* info content     */ 12.335963,
/* base_counts      */ {1995, 1051, 2141, 841},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 548
};

	float jaspar_splice_pwm_2_matrix_row_0_[11] = {  0.190983,  0.453232, -0.096533,  1.272457, -7.410420,  1.220281, -0.857912, -7.410420, -2.795299, -0.857912, -2.795299};
	float jaspar_splice_pwm_2_matrix_row_1_[11] = {  0.001631, -0.165283,  0.075684, -7.169258, -7.169258, -7.169258, -2.554137,  1.327937,  1.327937, -1.865953, -1.175296};
	float jaspar_splice_pwm_2_matrix_row_2_[11] = { -0.078348, -0.616750,  0.571841, -7.169258, -7.169258, -1.462147, -2.554137, -0.365752, -7.169258, -2.554137, -1.865953};
	float jaspar_splice_pwm_2_matrix_row_3_[11] = { -0.165478, -0.096533, -0.857912, -7.410420,  1.272457, -7.410420,  1.106973, -2.795299, -0.606914,  1.086775,  1.146186};
	float *jaspar_splice_pwm_2_matrix[4] = { jaspar_splice_pwm_2_matrix_row_0_, jaspar_splice_pwm_2_matrix_row_1_, jaspar_splice_pwm_2_matrix_row_2_, jaspar_splice_pwm_2_matrix_row_3_};
	PWM jaspar_splice_pwm_2_ = {
/* accession        */ "SD0003",
/* name             */ "at_AC_acceptor",
/* label            */ " SD0003	12.2306695437445	at_AC_acceptor	Unknown	; description \"Non-canonical splice acceptor (3\') site (at_AC)\" ; medline \"15475254\" ",
/* motif            */ "NNNATATCCTT",
/* library_name     */ "jaspar_splice",
/* length           */ 11,
/* matrixname       */ jaspar_splice_pwm_2_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -46.354652,
/* max_score        */ 10.977060,
/* threshold        */ 0.795,
/* info content     */ 12.198158,
/* base_counts      */ {192, 143, 58, 256},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 59
};

	float jaspar_splice_pwm_3_matrix_row_0_[20] = { -0.663808, -0.638810, -0.813728, -0.663808, -0.756577, -1.101361, -1.248966, -1.081947, -1.161973, -0.715761, -0.828541, -1.081947, -1.062902, -0.158109, -2.037150,  1.272911, -9.638553,  0.035584, -0.029369, -0.022681};
	float jaspar_splice_pwm_3_matrix_row_1_[20] = {  0.120508,  0.149493,  0.135106,  0.244797,  0.142325,  0.205059,  0.059888,  0.184582,  0.231726,  0.417320,  0.422770,  0.433580,  0.184582,  0.090657,  1.074276, -9.397390, -9.397390, -0.681183, -0.177001, -0.021451};
	float jaspar_splice_pwm_3_matrix_row_2_[20] = { -0.515415, -0.361285, -0.572566, -0.543582, -0.633181, -0.748994, -0.748994, -0.697709, -0.602414, -0.633181, -1.007804, -1.181032, -1.326172, -0.055934, -3.690280, -9.397390,  1.514073,  0.853262, -0.281251,  0.036173};
	float jaspar_splice_pwm_3_matrix_row_3_[20] = {  0.531176,  0.459720,  0.579782,  0.467916,  0.576126,  0.643406,  0.731840,  0.639975,  0.608560,  0.387197,  0.500046,  0.576126,  0.728700,  0.108339,  0.114170, -9.638553, -9.638553, -1.226498,  0.299384,  0.010107};
	float *jaspar_splice_pwm_3_matrix[4] = { jaspar_splice_pwm_3_matrix_row_0_, jaspar_splice_pwm_3_matrix_row_1_, jaspar_splice_pwm_3_matrix_row_2_, jaspar_splice_pwm_3_matrix_row_3_};
	PWM jaspar_splice_pwm_3_ = {
/* accession        */ "SA0002",
/* name             */ "at_AC_acceptor",
/* label            */ " SA0002	9.10739671864104	at_AC_acceptor	Unknown	; description \"Non-canonical splice acceptor (3\') site (at_AC)\" ; medline \"15475254\" ",
/* motif            */ "NNYNYTTTTNYYTNCAGGNN",
/* library_name     */ "jaspar_splice",
/* length           */ 20,
/* matrixname       */ jaspar_splice_pwm_3_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -37.017673,
/* max_score        */ 12.619113,
/* threshold        */ 0.670,
/* info content     */ 9.104689,
/* base_counts      */ {1978, 2743, 1939, 4300},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 548
};

	float jaspar_splice_pwm_4_matrix_row_0_[20] = { -1.416458, -2.107115, -0.606914, -0.406446,  0.085678,  0.490958, -0.165478, -0.096533, -0.406446, -0.319510, -0.165478, -0.165478, -1.416458, -0.606914, -1.011825,  1.272457, -7.410420, -0.239531, -1.703310,  0.028552};
	float jaspar_splice_pwm_4_matrix_row_1_[20] = {  0.732120,  0.803553,  0.209126, -0.260503, -0.260503, -0.078348,  0.614383,  0.432145,  0.326840,  0.326840, -0.078348,  0.614383,  0.269714,  0.075684,  0.837443, -7.169258,  1.513619,  0.380878, -0.165283, -0.770663};
	float jaspar_splice_pwm_4_matrix_row_2_[20] = { -1.175296, -1.462147, -2.554137, -1.462147, -0.483397, -1.462147, -1.865953, -0.770663, -0.078348, -0.952652, -0.260503, -0.770663, -0.483397, -0.260503, -7.169258, -7.169258, -7.169258, -0.078348, -0.365752, -0.260503};
	float jaspar_splice_pwm_4_matrix_row_3_[20] = {  0.373221,  0.414026,  0.691561,  0.750384,  0.330679,  0.028552,  0.139716,  0.085678,  0.085678,  0.373221,  0.330679, -0.096533,  0.596281,  0.453232,  0.330679, -7.410420, -7.410420, -0.096533,  0.778547,  0.453232};
	float *jaspar_splice_pwm_4_matrix[4] = { jaspar_splice_pwm_4_matrix_row_0_, jaspar_splice_pwm_4_matrix_row_1_, jaspar_splice_pwm_4_matrix_row_2_, jaspar_splice_pwm_4_matrix_row_3_};
	PWM jaspar_splice_pwm_4_ = {
/* accession        */ "SA0003",
/* name             */ "at_AC_acceptor",
/* label            */ " SA0003	9.01616174501847	at_AC_acceptor	Unknown	; description \"Non-canonical splice acceptor (3\') site (at_AC)\" ; medline \"15475254\" ",
/* motif            */ "YYTTNNNNNNNNTNCACNTN",
/* library_name     */ "jaspar_splice",
/* length           */ 20,
/* matrixname       */ jaspar_splice_pwm_4_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -41.239254,
/* max_score        */ 12.776595,
/* threshold        */ 0.670,
/* info content     */ 8.992785,
/* base_counts      */ {262, 376, 112, 430},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 59
};

	float jaspar_splice_pwm_5_matrix_row_0_[11] = {  0.218212,  0.834764, -0.981417, -14.499735, -14.499735,  0.806183,  0.909897, -1.116083, -0.421859,  0.099906, -0.182176};
	float jaspar_splice_pwm_5_matrix_row_1_[11] = {  0.461885, -0.757497, -2.051995, -14.258573, -14.258573, -2.173532, -1.112881, -1.389294, -0.437576, -0.168768,  0.080898};
	float jaspar_splice_pwm_5_matrix_row_2_[11] = { -0.187102, -0.710940,  1.284657,  1.514127, -14.258573,  0.369423, -0.686279,  1.254277, -0.189350,  0.232533,  0.023248};
	float jaspar_splice_pwm_5_matrix_row_3_[11] = { -0.846389, -0.667759, -1.360316, -14.499735,  1.272965, -2.245820, -0.835627, -1.224648,  0.564273, -0.209170,  0.078683};
	float *jaspar_splice_pwm_5_matrix[4] = { jaspar_splice_pwm_5_matrix_row_0_, jaspar_splice_pwm_5_matrix_row_1_, jaspar_splice_pwm_5_matrix_row_2_, jaspar_splice_pwm_5_matrix_row_3_};
	PWM jaspar_splice_pwm_5_ = {
/* accession        */ "SD0001",
/* name             */ "at_AC_acceptor",
/* label            */ " SD0001	8.12219678199792	at_AC_acceptor	Unknown	; description \"Non-canonical splice acceptor (3\') site (at_AC)\" ; medline \"15475254\" ",
/* motif            */ "NAGGTAAGNNN",
/* library_name     */ "jaspar_splice",
/* length           */ 11,
/* matrixname       */ jaspar_splice_pwm_5_matrix,
/* background       */ { 0.280, 0.220, 0.220, 0.280},
/* min_score        */ -38.232265,
/* max_score        */ 9.216458,
/* threshold        */ 0.806,
/* info content     */ 8.122171,
/* base_counts      */ {229309, 84891, 281090, 183444},
/* base_pseudocounts*/ { 0.010, 0.010, 0.010, 0.010},
/* index            */ NULL,
/* Nsites           */ 70794
};

PWM *jaspar_splice[JASPAR_SPLICE_N] = { &jaspar_splice_pwm_0_, &jaspar_splice_pwm_1_, &jaspar_splice_pwm_2_, &jaspar_splice_pwm_3_, &jaspar_splice_pwm_4_, &jaspar_splice_pwm_5_};
extern PWM *jaspar_splice[JASPAR_SPLICE_N];
