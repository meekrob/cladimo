#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pwm.h"
#include "library.h"
#include "pwm_readers.h"
#include "combo_lib.h"

#ifndef MAX_WMX
#define MAX_WMX 512
#endif
void set_background(struct combo_lib *runlib, float A, float C, float G, float T)
{
    int i;
    for(i=0; i< runlib->total_n; i++)
    {
        PWM *pwm = runlib->pwm_library[i];
        pwm_retransform(pwm, A, C, G, T);
        if (pwm->index != NULL)
            pwm->index = build_index(pwm);
    }
}
float get_cmd_threshold(char *arg)
{
    double threshold = DEFAULT_PWM_THRESHOLD;
    char *cptr = rindex(arg, ':');
    cptr[0] = 0;
    char* threshold_string = cptr + 1;
    char* endptr;
    threshold = strtod(threshold_string, &endptr);
    if (endptr == threshold_string) 
        return -1;
    else
        return (float)threshold;
}

void free_combo_lib(struct combo_lib** clp)
{
    struct combo_lib *cl = *clp;
    int i;
    for (i=0; i<cl->total_n; i++) pwm_free_index(cl->pwm_library[i]);
    for (i=0; i< cl->userlibn; i++) free_pwm(&(cl->user_library[i]));
    free(cl->user_library);
    free(cl->static_library);
    free(cl->pwm_library);
    free(cl);
    cl = NULL;
}

struct combo_lib * parse_arg_list(char **argv, int argc)
{
    int i,j,k;
    int file_n = 0;
    int volume_n = 0;
    int total_n = 0;
    int statlibn = 0;
    int userlibn = 0;
    PWM **static_library = build_static_library(&statlibn); // PWMs are defined by literals in libraries/library.c, should not be freed
    PWM **user_library = calloc(MAX_WMX,  sizeof(PWM*)); // PWMs created in this session, must be freed
    PWM **pwm_library = calloc(MAX_WMX, sizeof(PWM*)); // master list
    PWM * res=NULL;
    PWM **pwm_arr = NULL;

    if (argc == 0)// default run
    {
        pwm_arr = search_volume_name("fam", &volume_n);
        memcpy(&(pwm_library[total_n]), pwm_arr, sizeof(PWM*) * volume_n);
        pwm_arr= NULL;
        total_n += volume_n;
    }
    // else... try to match the command line to an accession, library name, file name, or motif
    for(i=0; i< argc; i++)
    {
        float threshold = -1;
        if (rindex(argv[i], ':') != NULL) 
        {
            threshold = get_cmd_threshold(argv[i]);
        }
        res = retrieve_library_accession(static_library, statlibn, argv[i]);
        if (res != NULL)
        {
            pwm_library[total_n++] = res;
            if (threshold > -1) res->threshold = threshold;
            if (total_n == MAX_WMX) break;
        }
        else if (valid_motif(argv[i])) 
        {
            user_library[userlibn++] = new_pwm_motif(argv[i]);
            pwm_library[total_n++] = user_library[userlibn-1];
            if (threshold > -1) user_library[userlibn-1]->threshold = threshold;
            if (total_n == MAX_WMX) break;
        }
        else if ((pwm_arr = search_volume_name(argv[i], &volume_n)) != NULL)
        {
            if (volume_n < (MAX_WMX - total_n))
            {
                if (threshold > -1) {for (j=0; j< volume_n; j++) { pwm_arr[j]->threshold = threshold; }}
                memcpy(&(pwm_library[total_n]), pwm_arr, sizeof(PWM*) * volume_n);
                pwm_arr= NULL;
                total_n += volume_n;
            }
        }
        else if ((pwm_arr = read_pwm_file(argv[i], &file_n)) != NULL)
        {
            if (file_n < (MAX_WMX - total_n))
            {
                if (threshold > -1) {for (j=0; j< file_n; j++) pwm_arr[j]->threshold = threshold;}
                memcpy(&(user_library[userlibn]), pwm_arr, sizeof(PWM*) * file_n);
                memcpy(&(pwm_library[total_n]), pwm_arr, sizeof(PWM*) * file_n);
                free(pwm_arr);
                userlibn += file_n;
                total_n += file_n;
                if (total_n == MAX_WMX) break;
            }
            else
            {
                fprintf(stderr, "%s has too many records (%d),", argv[i], file_n);
                fprintf(stderr, "I can only read in %d more\n.", MAX_WMX - total_n);
                for (k=0; k< file_n; k++) free_pwm(&(pwm_arr[k]));
                free(pwm_arr); pwm_arr = NULL; file_n = 0;
            }
        }
        else
        {
            fprintf(stderr, "can't recognize the format of %s\n", argv[i]);
        }
    }

    // initialize the PWMs/motifs to run
    int max_pwm_length = 0;
    total_n = condense_pointer_array(pwm_library, total_n);
    for (i=0; i<total_n; i++) 
    {
        if (pwm_library[i]->length <= 6) pwm_library[i]->index = build_index(pwm_library[i]);
        max_pwm_length = MAX(max_pwm_length, pwm_library[i]->length);
    }

    struct combo_lib *rval = malloc(sizeof(struct combo_lib));
    rval->file_n = file_n;
    rval->volume_n = volume_n;
    rval->total_n = total_n;
    rval->statlibn = statlibn;
    rval->userlibn = userlibn;
    rval->static_library = static_library;
    rval->user_library = user_library;
    rval->pwm_library = pwm_library;
    rval->max_pwm_length = max_pwm_length;
    return rval;
}
