#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "linesplit.h"
#include "pwm.h"
#include "matrix.h"
#undef bool 
#define true 1
#define false 0
// -INF
#define DOUBLE_MIN -(1.0/0.0)
// INF
#define DOUBLE_MAX  (1.0/0.0) 
// NAN
#define DOUBLE_NAN (0.0/0.0)


char NT[] = {'A','C','G','T'};
float BKGD_basic[] = {GLOBAL_A, GLOBAL_C, GLOBAL_G, GLOBAL_T};
char degen(char X, char Y);
float Ufunc(float P)
{
    if (P==0) return 0;
    return -P * log2f(P);
}
PWM *new_pwm_motif(const char *motif)
{
    if (strlen(motif) > MAXWORDSIZE) 
    {
        fprintf(stderr, "motif %s too long (> %ld)\n", motif, MAXWORDSIZE);
        return NULL;
    }
    PWM *pwm = new_pwm();
    strcpy(pwm->motif, motif);
    pwm->length = strlen(pwm->motif);
    pwm->threshold = 1;
    return pwm;
}

PWM *new_pwm()
{
    PWM *p = malloc(sizeof(PWM));
    bzero(p, sizeof(PWM));
    memcpy(p->background, BKGD_basic, 4*sizeof(float));
    return p;
}

PWM *cpy_pwm(PWM *dest, PWM *src)
{
    if (dest==NULL) dest = malloc(sizeof(PWM));
    memcpy(dest,src,sizeof(PWM));
    dest->matrix = copy_matrix(src->matrix, 4, src->length);
    if (src->index)
    {
        dest->index = calloc(index_len(src), sizeof(float));
        memcpy(dest->index, src->index, index_len(src) * sizeof(float));
    }
    return dest;
}

void free_pwm(PWM **pwm)
{
    if(*pwm==NULL) return;
    if ((*pwm)->index) free((*pwm)->index);
    free_matrix(&((*pwm)->matrix));
    free(*pwm);
    *pwm=NULL;
}

float pwm_max(PWM *pwm_1)
{
    _pwm_loop_
    char nt[] = {'A','C','G','T'};
    float max_score=0;
    for_each_position( pwm_1->length )
    {
        float position_max = DOUBLE_MIN;
        for_each_base
        {
            char c=nt[base];
            position_max = MAX(position_max,pwm_match(c,position,pwm_1));
        }
        max_score += position_max;
    }
    return max_score;
}
float pwm_min(PWM *pwm_1)
{
    _pwm_loop_ 
    float min_score=0;
    char nt[] = {'A','C','G','T'};
    for_each_position( pwm_1->length )
    {
        float position_min = DOUBLE_MAX;
        for_each_base position_min = MIN(position_min, pwm_match(nt[base], position,pwm_1));
        min_score += position_min;
    }
    return min_score;
}

float pseudocount(int count)
{
    //return sqrtf(count+1);
    return 0.01;
}
void pwm_retransform(PWM *pwm, float A, float C, float G, float T)
{
    _pwm_loop_
    int i;
    int pwmlen = pwm->length;
    // reset matrix to counts
    float **counts = pwm_counts(pwm);
    for(i=0; i< pwmlen * 4; i++)
    for_each_base_position(pwmlen)
        pwm->matrix[base][position] = counts[base][position];

    pwm_transform_with_background(pwm, A, C, G, T);
    free_matrix(&counts);
}
void pwm_transform_with_background(PWM *pwm, float A, float C, float G, float T)
{
    pwm->background[0] = A;
    pwm->background[1] = C;
    pwm->background[2] = G;
    pwm->background[3] = T;
    pwm_transform(pwm);
}
void pwm_transform(PWM *pwm)
{
    /* modify count matrix with pseudocounts */
    /* convert modified count matrix into log-odds matrix*/
    /* set pwm min and max scores */
    _pwm_loop_
    int pwmlen = pwm->length;
    float *column_sum = calloc(pwmlen, sizeof(float));

    // pseudocounts
    bzero(pwm->base_counts, sizeof(float)*4);
    bzero(pwm->base_pseudocounts, sizeof(float)*4);
    for_each_base
    {
        for_each_position(pwmlen)
        {
            column_sum[position] += pwm->matrix[base][position];
            pwm->base_counts[base] += pwm->matrix[base][position];
        }
        pwm->base_pseudocounts[base] = pseudocount(pwm->base_counts[base]);
    }

    float correction = array_sum(pwm->base_pseudocounts,4);
    int Nsites = column_sum[0]; 
    // convert frequencies to log P after applying the pseudocount adjustment
    // replace matrix with weight matrix
    float entropy = 0;
    float background_uncertainty = 2;
    pwm->information_content = 0;
    for_each_position(pwmlen)
    {
        entropy = 0;
        for_each_base
        {
            float count = pwm->matrix[base][position];
            float pseudocount = pwm->base_pseudocounts[base];
            float P = (count + pseudocount) / (Nsites + correction);
            // information content
            entropy += Ufunc(P);
            // matrix val is now a weight matrix val
            pwm->matrix[base][position] = log(P) - log(pwm->background[base]); 
        }
        pwm->information_content += background_uncertainty - entropy;
    }
    pwm->max_score = pwm_max(pwm);
    pwm->min_score = pwm_min(pwm);
    pwm->Nsites = Nsites;
    free(column_sum);
}
float **pwm_pmatrix(PWM *pwm)
{
    _pwm_loop_
    int pwmlen = pwm->length;
    float **p_matrix = zeroes(4, pwmlen);

    // subtract the background from the score matrix
    for_each_base_position(pwmlen) p_matrix[base][position] = exp(pwm->matrix[base][position]) * pwm->background[base];
    return p_matrix;
}

float **pwm_counts(PWM *pwm)
{
    _pwm_loop_
    int pwmlen = pwm->length;
    float **count_matrix = zeroes(4, pwm->length);

    // reverse all the transform operations
    float correction = array_sum(pwm->base_pseudocounts,4);
    for_each_base
    {
        for_each_position(pwmlen)
        {
            float adjusted_probability = exp(pwm->matrix[base][position]) * pwm->background[base];
            float count = (adjusted_probability * (pwm->Nsites + correction)) - pwm->base_pseudocounts[base];
            count_matrix[base][position] = fabs(round(count));
        }
    }
    return count_matrix;
}
void pwm_basecount(PWM *pwm, int *arr)
{
    _pwm_loop_
    int pwmlen = pwm->length;
    float **count_matrix = pwm_counts(pwm);
    bzero(arr, sizeof(int)*4);
    for_each_base_position( pwmlen ) arr[base] += (int)count_matrix[base][position];
}

/*float** ICfunc_wc(PWM *pwm, int use_pseudocounts)
{
    _pwm_loop_
    float H_background = 2;
    float **H_matrix = zeroes(5, pwm->length);
    float **count_matrix = pwm_counts(pwm);
    float Nsites = pwm->Nsites;
    if (use_pseudocounts)
        Nsites += array_sum(pwm->base_pseudocounts,4);

    for_each_position( pwm->length )
    {
        float H = 0.;
        for_each_base
        {
            if (use_pseudocounts) 
                count_matrix[base][position] += pwm->base_pseudocounts[base];
            float P = count_matrix[base][position] / Nsites;
            H_matrix[base][position] = fabs((H_background-Ufunc(P)) * P);
            H += Ufunc(P);
        }
        H_matrix[4][position] = H_background - H;
    }
    free_matrix(&count_matrix);
    return H_matrix;
}*/

float** motif_ICfunc(char *motif)
{
    _pwm_loop_
    _pwm_indexing_ // for mscore
    int wordlen = strlen(motif);
    float **H_matrix = zeroes(5, wordlen);
    float H_background = 2;
    char nt[] = "ACGT";

    for_each_position( wordlen )
    {
        int sym = (int)motif[position];
        // Nsites = fold degeneracy for the column
        float Nsites = 0;
        for_each_base Nsites += mscore(letterval(nt[base]), sym);

        // columnar IC
        float H = 0.;
        for_each_base H += Ufunc((float)mscore(letterval(nt[base]), sym) / Nsites);
        H_matrix[4][position] = H_background - H;

        // base-specific IC
        for_each_base H_matrix[base][position] = ((float)mscore(letterval(nt[base]), sym) / Nsites) * H_matrix[4][position];
    }
    return H_matrix;
}
float** ICfunc(PWM *pwm)
{
    if (pwm->matrix == NULL) return motif_ICfunc(pwm->motif);
    _pwm_loop_
    float H_background = 2;
    float **H_matrix = zeroes(5, pwm->length);
    float **count_matrix = pwm_counts(pwm);
    float correction = 0;
    float Nsites = pwm->Nsites;
    Nsites += array_sum(pwm->base_pseudocounts,4);
    for_each_position( pwm->length )
    {
        float H = 0.;
        for_each_base
        {
            count_matrix[base][position] += pwm->base_pseudocounts[base];
            float P = count_matrix[base][position] / (pwm->Nsites+correction);
            H += Ufunc(P);
        }
        H_matrix[4][position] = H_background - H;
        for_each_base
        {
            float P = count_matrix[base][position] / (pwm->Nsites+correction);
            H_matrix[base][position] = P * H_matrix[4][position];
        }
    }
    free_matrix(&count_matrix);
    return H_matrix;
}
float totalICfunc(PWM *pwm)
// corresponds to the sum of the fifth row in the output of ICfunc
{
    _pwm_loop_
    float H_background = 2;
    float **count_matrix = pwm_counts(pwm);
    float correction = 0;
    float totalIC = 0;
    for_each_position( pwm->length )
    {
        float H = 0.;
        for_each_base
        {
            float P = count_matrix[base][position] / (pwm->Nsites+correction);
            H += Ufunc(P);
        }
        totalIC += H_background - H;
    }
    return totalIC;
}
float thresholdFunc(PWM *pwm)
{
    return estimated_threshold_3_per_kb(pwm);
}
float estimated_threshold_3_per_kb(PWM *pwm)
// the estimated threshold at which the pwm will average 3 matches / 1kb in background.
{
    //float ic = totalICfunc(pwm);
    float ic = pwm->information_content;
    float length = (float)pwm->length;

    /*
       Based on regressing the threshold at which the 
       PWM produces an expected ~3 hits per 1kb using the background frequencies of A,C,G,T
       against
        X1: the length of the PWM, and X2: its information content.

       The interaction term added slightly to the Adjusted R-squared (0.6489 to 0.6698)

       lm(formula = t.3 ~ length * ic, data = x)
       Coefficients:
       Estimate Std. Error t value Pr(>|t|)    
       (Intercept)  1.1956358  0.0632022  18.918  < 2e-16 ***
       length      -0.0361048  0.0073257  -4.929 4.05e-06 ***
       ic          -0.0238384  0.0068941  -3.458 0.000853 ***
       length:ic    0.0019205  0.0007569   2.537 0.012991 *
       Residual standard error: 0.03203 on 85 degrees of freedom
       Multiple R-squared: 0.681,      Adjusted R-squared: 0.6698 
       F-statistic: 60.49 on 3 and 85 DF,  p-value: < 2.2e-16 
    */
    float intercept = 1.195636 + 0.03;
    float ic_coef = -0.023838;
    float length_coef = -0.036105;
    float interaction = 0.001921;
    return MAX( .67, intercept + (ic*ic_coef) + (length*length_coef) + (length*ic*interaction));
}

void print_pwm(FILE* out, PWM *pwm)
{
    _pwm_loop_
    char nt[] = "ACGT";
    if (pwm->motif[0] != 0)
    {
        fprintf(out,"%4c ", ' ');
        for_each_position(pwm->length)
            fprintf(out,"%5c ",pwm->motif[position]);
        fputc('\n',out);
    }
    for_each_base
    {
        fprintf(out,"%4c ",nt[base]);
        for_each_position( pwm->length )
        {
            float floatval = pwm->matrix[base][position];
            int intval = (int)pwm->matrix[base][position];
            if (floatval == intval)
                fprintf(out, "%5d ", intval);
            else
                fprintf(out, "% 4.2f ", floatval);
        }
        fputc('\n',out);
    }
}
struct pair 
{
    int count;
    char sym;
};
int pairSort(struct pair *p1,struct pair *p2);
int pairSort(struct pair *p1,struct pair *p2)
{
    // sort pairs by count, reverse order
    int a=p1->count;
    int b=p2->count;
    if (a==b) return 0;
    return (a<b ? 1 : -1);
}

char consensus_char(float* v)
{
    struct pair pairs[4];
    int total=0;
    int i; for(i=0;i<4;i++) 
    {
        int val;
        if (v[i] < 1) val = v[i] * 100;
        else val = v[i];
        total += val;
        pairs[i].count = val;
        pairs[i].sym = NT[i];
    }
    qsort(pairs,4,sizeof(struct pair),(void*)pairSort);
    // unique symbol for majority
    if ((float)pairs[0].count/total > 0.5) return pairs[0].sym;
    // 2-fold degenerate
    else if ( (float)(pairs[0].count+pairs[1].count)/total >=0.75)
        return degen(pairs[0].sym,pairs[1].sym);
    
    return 'N';
}
char consensus_char_v(float A, float C, float G, float T)
{
    float vec[] = {A,C,G,T};
    return consensus_char(vec);
}
char degen(char X, char Y)
{
    switch(X+Y)
    {
        case 'A' + 'C': return 'M';
        case 'A' + 'G': return 'R';
        case 'A' + 'T': return 'W';
        case 'C' + 'G': return 'S';
        case 'C' + 'T': return 'Y';
        case 'G' + 'T': return 'K';
    }
    return 'N';
}
int symbol_degeneracy(char symbol)
{
    switch(toupper(symbol))
    {
        case 'N': return 4;
        case 'R':
        case 'Y':
        case 'M':
        case 'K':
        case 'S':
        case 'W': return 2;
        case 'A':
        case 'C':
        case 'G':
        case 'T': return 1;
    }
    return 0;
}

int motif_degeneracy(char *motif)
{
    int i;
    int r=1;
    for (i=0; i<strlen(motif); i++) r*= symbol_degeneracy(motif[i]);
    return r;
}

float* pwm_compare(PWM *pwm_1, PWM *pwm_2, int* comparisons)
{
    float **pwm1 = pwm_1->matrix;
    float **pwm2 = pwm_2->matrix;
    int len1 = pwm_1->length;
    int len2 = pwm_2->length;

    int ri=0; 
    float* rval = calloc(len1*len2-1,sizeof(float));
    float **tpwm1 = transpose_matrix(pwm1, 4, len1);
    float **tpwm2 = transpose_matrix(pwm2, 4, len2);
    // vectorize
    float *vecx = tpwm1[0]; 
    float *vecy = tpwm2[0]; 
    // find covariances of all overlapping subvectors
    float *xstart,*ystart;
    int i;
    for (i=len1-1; i > -len2; i--)
    {
        int overlap = 0;
        if (i>=0) 
        {
            xstart = vecx + (i*4);
            ystart = vecy;
            overlap = 4 * MIN(len1 - i,len2);
        }
        else 
        {
            xstart = vecx;
            ystart = vecy + (-4*i);
            overlap = 4 * MIN(len1,len2+i);
        }
        rval[ri++] = array_cov(xstart,ystart,overlap);
    }
    free_matrix(&tpwm1);
    free_matrix(&tpwm2);
    *comparisons=ri;
    return rval;
}

float GC_content(unsigned char* wordstart, int windowsize)
{
    int count=0;
    unsigned char* wp = wordstart;
    while(wp-wordstart < windowsize) count += patmatch(*(wp++), 'S');

    return (float)count / windowsize;
}
