#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "pwm.h"
#include "library.h"
struct dat {
    char* seq;
    PWM *pwm;
    float score;
};

int cmp(const void *a, const void *b)
{
    float x = ((struct dat *)(a))->score;
    float y = ((struct dat *)(b))->score;
    if (x==y) return 0;
    return x < y ? 1: -1;
}

int main(int argc, char **argv)
{
    int total_n=0;
    int i,pwmi;
    PWM **pwm_library = build_static_library(&total_n);
    char *seq = argv[1];
    int seqlen = strlen(seq);
    struct dat * records = calloc(total_n, sizeof(struct dat));

    // scan sequence
    bitsequence dna_word_index = 0;
    for(i=0; i < seqlen; i++)
    {
        if (! validchar(seq[i])) continue;
        dna_word_index = dnashiftone((unsigned char)seq[i], dna_word_index, MAXWORDSIZE);
        for (pwmi=0; pwmi < total_n; pwmi++)
        {
            int setposition = i - (pwm_library[pwmi]->length - 1);
            if ((pwm_library[pwmi]->length == seqlen) && (setposition >= 0))
            {
                float score = fabs(numeric_wordscore(dna_word_index, pwm_library[pwmi]));
                struct dat *rec = &(records[pwmi]);
                if (score > rec->score)
                {
                    rec->score = score;
                    rec->pwm = pwm_library[pwmi];
                    rec->seq = &(seq[setposition]);
                }
            }
        }
    }
    qsort(records, total_n, sizeof(struct dat), cmp);
    for(i=0; i < 5; i++)
    {
        if (records[i].pwm != NULL) printf("%s %s %.3f %.*s\n", records[i].pwm->name, records[i].pwm->accession, records[i].score, records[i].pwm->length, records[i].seq);
    }
    free(records);
    free(pwm_library);
    return 0;
}
