#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
void print_leafs(struct btree *tree)
{
    if (tree==NULL) 
        return;
    else if (tree->left != NULL)
    {
        print_leafs(tree->left);
        print_leafs(tree->right);
    }
    else
    {
        printf("'%s', ", tree->name);
    }
    return;
}
int main(int argc, char **argv)
{
    char treestring[0x100000];
    FILE *infile = fopen(argv[1],"r");
    fscanf(infile, "%s", treestring);
    fprintf(stdout, "%s\n\n", treestring);
    fclose(infile);

    struct btree *tree = parse_nh(treestring);
    print_leafs(tree);
    printf("\n");
    depth_first_free(&tree);
    return 0;
}

