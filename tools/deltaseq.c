#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "pwm.h"
#include "matrix.h"
#include "combo_lib.h"
#define letterval(a) dna_encoding[(int)a]
extern const signed char dna_encoding[256];
const char letters[] = "ACGT";
void score(PWM *pwm, char *sequence1, char *sequence2)
{
    _pwm_loop_
    float **ICmx = ICfunc(pwm);
    float total=0;
    int subs = 0;
    float IC = 0;
    for_each_position( pwm->length )
    {
        IC += ICmx[4][position];

        int w = letterval(sequence1[position]);;
        int z = letterval(sequence2[position]);;
        if (w!=z) subs++;
        float delta = fabs(ICmx[w][position] - ICmx[z][position]);
        fprintf(stderr, "%c,%c %f\n", letters[w], letters[z], delta);
        total += delta;
    }
    free_matrix(&ICmx);
    fprintf(stderr, "seq1 %s %.3f\n", sequence1,pwm_stringscore(sequence1, 0, pwm)); 
    fprintf(stderr, "seq1 %s - %.3f\n", sequence1,pwm_stringscore(sequence1, -1, pwm)); 
    fprintf(stderr, "seq1 %s + %.3f\n", sequence1,pwm_stringscore(sequence1, 1, pwm)); 
    fprintf(stderr, "seq2 %s %.3f\n", sequence2,pwm_stringscore(sequence2, 0, pwm)); 
    fprintf(stderr, "pattern delta: %f, sequence distance: %d\n", total, subs);
    float psim = 1 - (total/IC);
    float ssim = 1 - ((float)subs/pwm->length);
    fprintf(stderr, "pattern similarity: %f, sequence similarity: %f\n", psim, ssim);
    fprintf(stderr, "log ratio psim/ssim = %f\n", log(psim) - log(ssim));
}
int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "%s seq1 seq2 pwm\n", argv[0]);
        return 1;
    }
    char *seq1 = argv[1];
    char *seq2 = argv[2];
    struct combo_lib *runlib = parse_arg_list(&(argv[3]), 1);
    PWM *pwm = runlib->pwm_library[0];
    score(pwm, seq1, seq2);
    free_combo_lib(&runlib);
    return 0;
}
