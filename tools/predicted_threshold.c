#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "pwm.h"
#include "combo_lib.h"
float model(float length, float ic)
{
    float intercept = 1.195636;
    float ic_coef = -0.023838;
    float length_coef = -0.036105;
    float interaction = 0.001921;
    return intercept + (ic*ic_coef) + (length*length_coef) + (length*ic*interaction);
}
float ICfunc(PWM *pwm);
int main(int argc, char **argv)
{
    struct combo_lib *runlib = parse_arg_list(&(argv[1]), argc-1);
    int pwmi=0;
    PWM *pwm;
    for (pwmi=0; pwmi < runlib->total_n; pwmi++) 
    {
        pwm = runlib->pwm_library[pwmi];
        float IC = ICfunc(pwm);
        printf("%s_%s %d %.3f %.3f\n", pwm->name, pwm->accession, pwm->length, IC, model((float)pwm->length, IC));
    }
    free_combo_lib(&runlib);
    return 0;
}
float Ufunc(float P)
{
    if (P==0) return 0;
    return -P * log2f(P);
}
float ICfunc(PWM *pwm)
{
    int i,column;
    float H_background = 2;
    float **count_matrix = pwm_untransform(pwm);
    float correction = 0;
    float totalIC = 0;
    for (column=0; column < pwm->length; column++)
    {
        float H = 0.;
        for (i=0; i<4; i++)
        {
            float P = count_matrix[i][column] / (pwm->Nsites+correction);
            H += Ufunc(P);
        }
        totalIC += H_background - H;
    }
    return totalIC;
}
