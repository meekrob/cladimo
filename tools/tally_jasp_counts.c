#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pwm.h"
#include "library.h"
#include "pwm_readers.h"

#include "maf.h"
#include "seq.h"
#include "mafchop.h"

#include "cladistic_motifs.h"
#include "cladistic_motif_printing.h"
#define MAXWORDSIZE 32

struct record
{
    PWM *pwm;
    int count;
};

static int cmp_accession(const void *a, const void *b)
{
    struct record *r1 = (struct record *)a;
    struct record *r2 = (struct record *)b;
    return strcmp(r1->pwm->accession, r2->pwm->accession);
}
static int cmp_count_desc(const void *a, const void *b)
{
    struct record *r1 = (struct record *)a;
    struct record *r2 = (struct record *)b;
    return r1->count < r2->count ? 1 : -1;
}


int main(int argc, char **argv)
{

    int i;
    int statlibn = 0;
    PWM **static_library = build_static_library(&statlibn);
    int max_pwm_length = 0;
    struct record *counts = calloc(statlibn, sizeof(struct record));
    for (i=0; i<statlibn; i++) 
    {
        counts[i].count = 0;
        counts[i].pwm = static_library[i];
        static_library[i]->threshold = .9;
        max_pwm_length = MAX(max_pwm_length, static_library[i]->length);
    }
    qsort(counts, statlibn, sizeof(struct record), cmp_accession);
    int threshold = strtol(argv[2], NULL, 10);

    struct mafFile *mf = NULL;
    mf = mafOpen(argv[1], 0);
    if (mf) // score the alignment
    {
        longline ref_species;
        ref_species[0] = 0;
        int chop_point=0;
        struct mafAli* scoredblock = NULL; // maintains overlaps between blocks
        struct cladistic_motif *cm = NULL; // output matches to the scored block
        while ((scoredblock = ascoremaf(mf, &cm, static_library, statlibn, max_pwm_length, ref_species, &chop_point, scoredblock)) != NULL)
        {
            struct cladistic_motif *cptr = cm;
            while (cptr!=NULL)
            {
                if (cptr->nmatches >= threshold)
                {
                    struct record key, *rec;
                    key.pwm = cptr->pwm;
                    rec = bsearch(&key, counts, statlibn, sizeof(struct record), cmp_accession);
                    if (rec) rec->count++;
                }
                cptr = cptr->next;
            }
            free_cm_list(&cm);
        }
        mafFileFree(&mf);
    }

    qsort(counts, statlibn, sizeof(struct record), cmp_count_desc);
    for (i=0; i<statlibn; i++)
    {
        if (counts[i].count > 0)
        {
            char *name = pwm_name(counts[i].pwm);
            printf("%s %s %d\n", counts[i].pwm->accession, name, counts[i].count);
            free(name);
        }
    }
    free(counts);
    free(static_library);
    return 0;
}
