#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include "pwm.h"
#include "matrix.h"
#include "math.h"
#include "pwm_readers.h"
#include "linesplit.h"
void print_count_matrix(float **count_matrix, int pwmlen, int as_ints)
{
    _pwm_loop_
    char nt[] = "ACGT";
    for_each_base
    {
        printf("%4c ", nt[base]);
        for_each_position( pwmlen )
        {
            if (as_ints)
                printf("% 5d ", (int)count_matrix[base][position]);
            else
                printf("%5.2f ", count_matrix[base][position]);
        }
        printf("\n");
    }
}
void print_counts(PWM *pwm)
{
    float **cmx = pwm_counts(pwm);
    print_count_matrix(cmx, pwm->length, 1);
    free_matrix(&cmx);
}

void print_matrices(PWM *pwm)
{
    _pwm_loop_
    printf("original:\n");
    print_counts(pwm);

    float **freq = pwm_pmatrix(pwm);
    printf("frequencies:\n");
    print_count_matrix(freq, pwm->length, 0);
    free_matrix(&freq);

    printf("scores:\n");
    print_pwm(stdout, pwm);

    float **ICmx = ICfunc(pwm);
    printf("Information content:\n");
    print_count_matrix(ICmx, pwm->length, 0);

    float sum = 0;
    printf("IC   ");
    for_each_position( pwm->length )
    {
        printf("%5.2f ", ICmx[4][position]);
        sum += ICmx[4][position];
    }
    printf("IC total = %.3f\n", sum);
    free_matrix(&ICmx);
}

typedef enum { readpwm, readpwmx } flavor ;
flavor whatami(char *progpath)
{
    char *progname = rindex(progpath, '/');
    if (progname != NULL) progname = progname + 1;
    else progname = progpath;
    if (strcmp(progname, "readpwmx")==0) return readpwmx;
    else return readpwm;
}
const char xml_format_str[] = "<%1$s>%2$s</%1$s>"; // tag name, str value
const char xml_format_int[] = "<%1$s>%2$d</%1$s>"; // tag name, int value
const char xml_format_float[] = "<%1$s>%2$.*3$f</%1$s>"; // tag name, float value, precision

int main(int argc, char **argv)
{
    int i,filevar;

    int xmloutformat = (whatami(argv[0]) == readpwmx);

    if (argc < 2)
    {
        fprintf(stderr, "%s filename\n", argv[0]);
        return 1;
    }
    filevar=1;
    for (; filevar < argc; filevar++)
    {
        int npwm=0;
        PWM **pwmlist = read_pwm_file(argv[filevar], &npwm);
        if (xmloutformat)
            fprintf(stdout, "<pwmfile name=\"%s\" npwm=\"%d\">\n", argv[filevar], npwm);
        else
            fprintf(stdout, "npwm %d\n", npwm);
        for(i=0; i<npwm; i++)
        {
            PWM *pwm = pwmlist[i];
            if (xmloutformat)
            {
                fprintf(stdout, xml_format_str, "accession", pwm->accession);
                fprintf(stdout, xml_format_float, "threshold", pwm->threshold, 4);
                fprintf(stdout, xml_format_str, "name", pwm->name);
                fprintf(stdout, xml_format_int, "length", pwm->length);
                fprintf(stdout, xml_format_str, "label", pwm->label);
                fprintf(stdout, xml_format_str, "consensus_motif", pwm->motif);
            }
            else
            {
                fprintf(stdout, "threshold %.4f\n", pwm->threshold);
                fprintf(stdout, "accession %s\n", pwm->accession);
                fprintf(stdout, "name %s\n", pwm->name);
                fprintf(stdout, "length %d\n", pwm->length);
                fprintf(stdout, "label %s\n", pwm->label);
                fprintf(stdout, "consensus motif %s\n", pwm->motif);
                fprintf(stdout, "Nsites %d\n", pwm->Nsites);
            }
            print_matrices(pwm);
            free_pwm(&pwm);
        }
        free(pwmlist);
        if (xmloutformat)
            fprintf(stdout, "</pwmfile>\n");
    }
    return 0;
}
