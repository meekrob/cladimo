#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "pwm.h"
#include "combo_lib.h"
struct datum
{
    int seqindex;
    //float binding_p;
    struct { float clm; float evens; } background_p;
    struct { float clm; float evens; } background_e;
    float adjusted_score;
};

int datum_cmp(const void *a, const void*b)
{
    float y = ((struct datum*)a)->adjusted_score;
    float x = ((struct datum*)b)->adjusted_score;
    return (-1)*(x<y) + (+1)*(x>y);
}
int main(int argc, char **argv)
{
    struct combo_lib *runlib = parse_arg_list(&(argv[1]), argc-1);
    int pwmi=0;
    PWM *pwm;

#ifdef PRINT_ALL_SCORES
    FILE *print_all_scores = fopen("score_data.out","w");
    if (print_all_scores) fprintf(print_all_scores, "name length ic threshold ebackground\n");
#endif

    char header[] = "quantile name threshold expected length ic"; printf("%s\n", header);
    for (pwmi=0; pwmi < runlib->total_n; pwmi++) 
    {
        pwm = runlib->pwm_library[pwmi];
        if (pwm->length > 15) continue;
        int npossible = 1 << 2* pwm->length;
        struct datum* data = calloc(npossible, sizeof(struct datum));

        float IC = totalICfunc(pwm);
        printf("#%s_%s %d %.3f ", pwm->name, pwm->accession, pwm->length, IC);

        // score all possible sequences (4 to the pwm->length power)
        int seqindex;
        for (seqindex = 0; seqindex < npossible; seqindex++)
        {
            int pos;
            int word = seqindex;
            float forward_score = 0;
            struct { float clm; float evens; } background_p = {1,1};

            for (pos=0; pos < pwm->length; pos++)
            {
                int i = pwm->length - pos - 1;
                int c = word & 3;
                background_p.clm *= pwm->background[c];
                background_p.evens *= .25;

                forward_score += pwm->matrix[c][i];
                word >>= 2;
            }
            // background probability
            data[seqindex].background_p.clm = background_p.clm;
            data[seqindex].background_p.evens = background_p.evens;

            // (foreground) score
            float adjusted = (forward_score - pwm->min_score) / (pwm->max_score - pwm->min_score);
            data[seqindex].adjusted_score = adjusted;

            // corresponding sequence 
            data[seqindex].seqindex = seqindex;
        }

        // sort by highest-scoring sequence to lowest
        qsort(data, npossible, sizeof(struct datum), datum_cmp);
        float last_distance_to_target = -100;
        struct { float clm; float evens; } background_e = {0,0};

        int ix;
        for (ix = 0; ix < npossible; ix++)
        {
            if (data[ix].adjusted_score < .701) break;
            background_e.clm += 1000 * data[ix].background_p.clm;
            background_e.evens += 1000 * data[ix].background_p.evens;

            data[ix].background_e.clm = background_e.clm;
            data[ix].background_e.evens = background_e.evens;

#ifdef PRINT_ALL_SCORES
            if (print_all_scores) fprintf(print_all_scores, "%s_%s %d %.3f %.3f %.3f\n", pwm->name, pwm->accession, pwm->length, IC, data[ix].adjusted_score, data[ix].background_e.clm);
#endif

            float distance_to_target = 5 - background_e.clm;
            if ((last_distance_to_target > 0) && (distance_to_target <= 0))
            {
                if (ix == 0)
                {
                    //printf("%.4f to %.4f\n",distance_to_target, last_distance_to_target);
                    printf("%.3f %.3f\n", data[ix].adjusted_score, data[ix].background_e.clm);
                    //printf("%.3f %.3f", data[ix+1].adjusted_score, data[ix+1].background_e.clm);
                }
                else if(fabs(last_distance_to_target) < fabs(distance_to_target))
                {
                    //printf("%.4f to %.4f\n",last_distance_to_target, distance_to_target);
                    printf("%.3f %.3f\n", data[ix-1].adjusted_score, data[ix-1].background_e.clm);
                    //printf("%.3f %.3f", data[ix].adjusted_score, data[ix].background_e.clm);
                }
                else
                {
                    //printf("%.4f to %.4f\n",last_distance_to_target, distance_to_target);
                    //printf("%.3f %.3f\n", data[ix-1].adjusted_score, data[ix-1].background_e.clm);
                    printf("%.3f %.3f\n", data[ix].adjusted_score, data[ix].background_e.clm);
                }
                //break;
            }
            last_distance_to_target = distance_to_target;
        }
        //if (ix == npossible) printf("%.3f %.3f-%.3f", data[ix-1].adjusted_score, data[0].background_e.clm, data[ix-1].background_e.clm);
        //printf("\n");
        struct datum qtiles[50]; 
        int nqtiles = sizeof(qtiles) / sizeof(qtiles[0]);
        int i;
        for (i=0; i<nqtiles; i++)
        {
            float prop = (float)(i+1)/nqtiles;
            qtiles[i] = data[ix - (int)(ix*prop)];
        }

        for (i=0; i<nqtiles; i++)
        {
            float prop = (float)(i+1)/nqtiles;
            printf("%.2f ", prop);
            printf("%s_%s %.3f %.3f %d %.3f\n", pwm->name, pwm->accession, qtiles[i].adjusted_score, qtiles[i].background_e.clm, pwm->length, IC);
        }
        free(data);
    }
    free_combo_lib(&runlib);

#ifdef PRINT_ALL_SCORES
    if (print_all_scores) fclose(print_all_scores);
#endif
    return 0;
}
