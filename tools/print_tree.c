#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
int main(int argc, char **argv)
{
    char treestring[0x100000];
    FILE *infile = fopen(argv[1],"r");
    fscanf(infile, "%s", treestring);
    fprintf(stdout, "%s\n\n", treestring);
    fclose(infile);

    struct btree *tree = parse_nh(treestring);
    print_nh(stdout, tree, 0,3);
    printf("\n");
    depth_first_free(&tree);
    return 0;
}

