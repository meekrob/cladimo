#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "genomeFeature.h"
#include "strsep_all.h"
#include "append_to_array.h"

#define MAGIC 0xFAB11612

#define NBINS (1<<18)
#define MAXLEN 247250000
#define BINSIZE (MAXLEN/NBINS)
#define getbin(coordinate) ((coordinate)/(BINSIZE))

void cleartable(long *table, int len)
{
    long value = -1;
    int i=0;
    while (i < len) table[i++] = value;
}

int main(int argc, char **argv)
{
    switch (argc)
    {
        case 1:
        case 3:
            fprintf(stdout, "table size will be %ld\n", NBINS*sizeof(long));
            fprintf(stdout, "%s file.gff -- creates file.index\n", argv[0]);
            //fprintf(stdout, "%s file.gff file.index start-end ...  -- uses file.index to locate the interval start-end in file.gff\n", argv[0]);
            fprintf(stdout, "%s file.gff file.index fileext < intervals:  uses file.index to locate the intervals (as start-end) in file.gff\n", argv[0]);
            return 1;
    }

    // gff vars
    FILE *gff = fopen(argv[1], "r");
    if (gff==NULL)
    {
        fprintf(stderr, "error: input file not found `%s'\n", argv[1]);
        return 1;
    }
    char *line = NULL;
    char *linebuf = NULL;
    size_t line_limit = 0;

    // table, index vars
    char *indexfilename = NULL;
    FILE *indexfile = NULL;
    long *table = malloc(sizeof(long)*NBINS);
    cleartable(table, NBINS);

    if (argc > 3)
    {
        indexfilename = argv[2];
        indexfile = fopen(indexfilename, "r");
        if (indexfile == NULL)
        {
            fprintf(stderr, "error: index file not found `%s'\n", indexfilename);
            return 1;
        }
        fread(table, sizeof(long), NBINS, indexfile);
        fclose(indexfile);

        // for the basename for output files
        char *basename = argv[1];
        char *fileext = argv[3];
        int baselen;
        if (strrchr(argv[1], '/') != NULL)
            basename = strrchr(argv[1], '/') + 1;

        char *extstart = strstr(basename, fileext);
        if (extstart == NULL)
            baselen = strlen(basename);
        else
            baselen = extstart - basename;

        int nregions = 0;
        while(!feof(stdin))
        {
            long left = 0;
            long right = 0;
            if (fscanf(stdin, "%ld-%ld\n", &left, &right) == 2)
            {
                char *outfilename = NULL;
                asprintf(&outfilename, "%.*s.%d.%s", baselen-1, basename, nregions++, fileext);
                FILE *outfile = fopen(outfilename, "w");

                int leftbin = getbin(left);
                int rightbin = getbin(right);
                int bin;
                long filepos = -1;
                for(bin = leftbin; bin <= rightbin; bin++)
                {
                    if (table[bin] > -1)
                    {
                        filepos = table[bin];
                        break;
                    }
                }
                if (filepos > -1)
                {
                    fseek(gff, filepos, SEEK_SET);
                    if (errno > 0)
                    {
                        perror("An error occured");
                        if (errno == EINVAL)
                        {
                            fprintf(stderr, "EINVAL: fseek(gff, filepos = %ld, SEEK_SET)\n", filepos);
                        }
                        else
                        {
                            fprintf(stderr, "not EINVAL: fseek(gff, filepos = %ld, SEEK_SET)\n", filepos);
                        }
                        abort();
                    }
                    int inoutputblock = 0;
                    while(next_line_buf(gff, &linebuf, &line_limit) >= 0)
                    {
                        line = linebuf;
                        long startpt = 0;
                        long endpt = 0;
                        struct genomeFeature *gfeat = NULL;
                        if ( (gfeat = genomeFeatureFromGFFStr(line)) != NULL)
                        {
                            startpt = gfeat->start;
                            endpt = gfeat->end;
                            //fprintf(stdout, "%s\n", line);
                            //fprintf(stdout, "       %8s  %8s\n", "user", "gff");
                            //fprintf(stdout, "start: % 7ld  % 7ld\n", left, startpt);
                            //fprintf(stdout, "end:   % 7ld  % 7ld\n", right, endpt);
                            if ((endpt >= left)&(startpt <= right))
                            {
                                //fprintf(outfile, "%s\n", line);
                                writeGFFgenomeFeature(outfile, gfeat);
                                inoutputblock = 1;
                            }
                            else if (startpt > right) 
                            {
                                inoutputblock = 0;
                                break;
                            }
                        }
                        else if (inoutputblock == 1)
                        {
                            // relay any intervening lines
                            fprintf(outfile, "%s\n", line);
                        }

                        genomeFeatureFree(&gfeat);
                    }
                }
                fclose(outfile);
                fprintf(stdout, "%s written.\n", outfilename);
                free(outfilename);
            }
            else
            {
                //fprintf(stderr, "Unrecognizable argument %s, skipping\n", argv[argi]);
            }
        }
        free(table);
    }

    /* creation of the index */
    if (argc == 2)
    {
        asprintf(&indexfilename, "%s.index", argv[1]);
        fprintf(stderr, "creating %s\n", indexfilename);
        indexfile = fopen(indexfilename, "w");
        long range_left = -1;
        long range_right = -1;
        while(next_line_buf(gff, &linebuf, &line_limit) >= 0)
        {
            line = linebuf;
            long startpt = 0;
            long endpt = 0;
            struct genomeFeature *gfeat = NULL;
            if ( (gfeat = genomeFeatureFromGFFStr(line)) != NULL)
            {
                /* GFF-defined chromosome ranges ****/
                startpt = gfeat->start;
                endpt = gfeat->end;
                if (startpt == 0)
                {
                    fprintf(stderr, "encountered weird value start,end ~ %ld,%ld:\n", startpt, endpt);
                    writeGFFgenomeFeature(stderr, gfeat);
                    return 1;
                }
                if ((range_left==-1) && startpt > 0) range_left = startpt;
                range_right = endpt;
                //printf("% 7ld - % 7ld\r", range_left, range_right);
                if (startpt < range_left)
                {
                    fprintf(stderr, "position out of order, %ld(current start) < %ld(start point)\n", startpt, range_left);
                    return 1;
                }
                if (endpt < range_left)
                {
                    fprintf(stderr, "position out of order, %ld(current end) < %ld(start point)\n", endpt, range_left);
                    return 1;
                }
                //writeGFFgenomeFeature(stderr, gfeat);

                genomeFeatureFree(&gfeat);
                long filepos = ftell(gff) - (strlen(line)+1);
                int leftbin = getbin(startpt);
                int rightbin = getbin(endpt);
                //fprintf(stderr, "filepos %ld for {startpt %ld[%d], endptr %ld[%d]}\n", filepos, startpt, leftbin, endpt, rightbin);
                if ((leftbin >= NBINS) || (rightbin >= NBINS))
                {
                    //fprintf(stderr, "input coordinate %ld-%ld out of range\n", startpt, endpt);
                    //fprintf(stderr, "%d or %d >= %d\n", leftbin, rightbin, NBINS);
                    break;
                }
                int bin;
                //fprintf(stderr, "setting bins %d-%d:\n", leftbin, rightbin);
                for (bin=leftbin; bin <= rightbin; bin++)
                {
                    if (table[bin] == -1)
                    {
                        table[bin] = filepos;
                        //fprintf(stderr, "\ttable[bin:%d] = filepos %ld;\n", bin, filepos);
                    }
                    else if (filepos < table[bin])
                    {
                        table[bin] = filepos;
                        //fprintf(stderr, "\ttable[bin:%d] = filepos %ld;\n", bin, filepos);
                    }

                } 
            }
            else
            {
                if (strlen(line)>0) 
                {
                    perror(NULL);
                    fprintf(stderr, "skipped %s\n", line);
                }
            }
        }
        free(linebuf);
        fclose(gff);

        fprintf(stderr, "\nwriting %s ... ", indexfilename);
        fwrite(table, sizeof(long), NBINS, indexfile);
        fclose(indexfile);
        fprintf(stderr, "\n");
        free(indexfilename);
        free(table);
    }
    return 0;
}
