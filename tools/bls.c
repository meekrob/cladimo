const char description[] = "\
 BLS - Resolve the Branch Length Sum covered by members of a phylogenetic tree.\n\
 Specify the subtree as a list of species in the `attributes' fields of a GFF (General Feature Format) \
\
"; 
const char *terms[] = {"species_matched", "matchset","alignedset"};
                            
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "btree.h"
#include "common_names.h"
#include "strsep_all.h"
#include "genomeFeature.h"
#include "append_to_array.h" // for next_line_buf

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stdout, "Usage: bls file.nh < input.gff\n");
        fprintf(stdout, "%s\n", description);
        return 1;
    }
    struct btree *tree = readTreeFile(argv[1]);
    //float maxsum = treesum(tree);
    FILE *pipe_comments = stdout;

    // iterate over the input and attach _bls terms to applicable attributes
    {
        FILE *in = stdin;
        struct genomeFeature *gfeat = NULL;

        while( (gfeat = readNextGFFLine(in, pipe_comments)) != NULL )
        {
            int nterms = 3;
            for(int termkey_i = 0; termkey_i < nterms; termkey_i++)
            {
                const char *termkey = terms[termkey_i];
                struct keyedAttribute *species_matched_attr = getAttr(termkey, gfeat->attributes);
                if (species_matched_attr != NULL)
                {
                    // add a BLS attribute corresponding to the list of species
                    struct keyedAttribute *bls_attr = createBLSattr(termkey, species_matched_attr, tree);
                    gfeat->attributes = addAttr(bls_attr, gfeat->attributes);
                }
            }
            writeGFFgenomeFeature(stdout, gfeat);
            genomeFeatureFree(&gfeat);
        }
        fclose(in);
    }

    depth_first_free(&tree);
    return 0;
}
