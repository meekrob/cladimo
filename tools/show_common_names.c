#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common_names.h"

int main(int argc, char **argv)
{
    int n_names = init_assembly_names();
    extern struct assembly_name common_names[];
    int i;
    for (i=0; i < n_names; i++)
    {
        printf("%d %s %s\n", common_names[i].plen, common_names[i].assembly, common_names[i].commonname);
    }
    for (i=0; i < n_names; i++) printf("'%s', ", common_names[i].commonname);
    fputc('\n', stdout);
    
    return 0;
}
