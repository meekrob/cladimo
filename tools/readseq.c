#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linesplit.h"

struct seqMap
{
    struct { int base; int max; int strand; } genome_coordinate;
    char *info;
    // seqlen
    unsigned char* seq;
    int *mapindex;
    int seqlen;
    // maplen, -1 for gaps
    int *seqindex; 
    int maplen;
    struct { size_t seq; size_t seqix; size_t mapix; } array_size_t;
};
void seqMap_free(struct seqMap **ptr)
{
    if (*ptr == NULL) return;
    free((*ptr)->seq);
    free((*ptr)->seqindex);
    free((*ptr)->info);
    free((*ptr)->mapindex);
    free(*ptr);
    *ptr = NULL;
}
inline int deprojectMapIndex(int mapPos, struct seqMap *map)
// return -1 for gap positions, otherwise, return the corresponding offset in sequence
{
    return map->seqindex[mapPos];
}
inline int projectSeqIndex(int seqIndex, struct seqMap *map)
// return the offset in the aligned text
{
    return map->mapindex[seqIndex];
}
inline unsigned char projectedSequence(int mapPos, struct seqMap *map)
{
    if (map->seqindex[mapPos] >= 0) return map->seq[map->seqindex[mapPos]];
    else
        return '-';
}

inline void update_map(int ch, struct seqMap *map, int chunk)
{
    int seqlen = map->seqlen;
    int gap = -1;

    if (ch == '-')
        // update the projection with a gap mapping
        map->maplen = append_to_array(&gap, &(map->seqindex), map->maplen, sizeof(int), &(map->array_size_t.mapix), chunk);
    else
    {
        // add to sequence
        append_to_array(&ch, &(map->seq), seqlen, sizeof(unsigned char), &(map->array_size_t.seq), chunk);
        // add the map index to the projection
        append_to_array(&(map->maplen), &(map->mapindex), seqlen, sizeof(int), &(map->array_size_t.seqix), chunk);
        // add the sequence index to the map
        map->maplen = append_to_array(&(seqlen), &(map->seqindex), map->maplen, sizeof(int), &(map->array_size_t.mapix), chunk);

        map->seqlen = seqlen+1;
    }
}

struct scored_seqMap_feature
{
    int len;
    struct seqMap *source;
    int position_in_source;
    float model_score;
    void *model;
};

int cmp_features_in_projection(void *a, void *b)
{
    struct scored_seqMap_feature *x = (struct scored_seqMap_feature *)a;
    struct scored_seqMap_feature *y = (struct scored_seqMap_feature *)b;
    int p1 = projectSeqIndex(x->position_in_source, x->source);
    int p2 = projectSeqIndex(y->position_in_source, y->source);
    if (p1 == p2)
    {
        int ref1 = *(char *)(x->model);
        int ref2 = *(char *)(y->model);
        return (-1)*(ref1<ref2) + (+1)*(ref1>ref2);
    }
    return (-1)*(p1<p2) + (+1)*(p1>p2);
}

void readSeqLine(FILE *in, struct seqMap *seq)
{
    int ch;
    int chunk = 4;
    while ((ch = fgetc(in)) != -1)
    {
        if (ch == '\n') break;
        update_map(ch, seq, chunk);
    }
}

struct seqMap *readMapSeq(FILE *in)
{
    int chunk = 4;
    struct seqMap * seq = malloc(sizeof(struct seqMap));
    bzero(seq, sizeof(struct seqMap));

    int linestart;
    while ((linestart = fgetc(in)) != -1)
    {
        if (index("ACGTacgt", linestart) != 0)
        {
            update_map(linestart, seq, chunk);
            readSeqLine(in, seq);
        }
        else if (linestart == '\n')
            continue;
        else if (seq->seqlen > 0) 
            break;
        else if (seq->info == NULL)
        {
            int nfields=0;
            char **fields = linesplit(in, &nfields);
            seq->info = join_fields(" ", fields, nfields);
            fieldsfree(&fields);
        }
    }
    int seqlen = seq->seqlen;
    seq->seq= realloc(seq->seq, sizeof(unsigned char) * (seqlen+1));
    seq->seq[seqlen] = 0;
    seq->seqlen = seqlen;
    return seq;
}

void print_simple(struct seqMap *seq)
{
    printf("info: %s\n", seq->info);
    printf("gap-free sequence:\n");
    printf("%s\n", seq->seq);

    printf("projected sequence:\n");
    int i;
    for (i=0; i< seq->maplen; i++)
    {
        printf("%c", projectedSequence(i, seq));
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    FILE *infile = fopen("test.fa", "r");
    while(! feof(infile))
    {
        struct seqMap *seq = readMapSeq(infile);
        seqMap_free(&seq);
    }
    fclose(infile);

    FILE *infile = fopen("sample_data/ghp010.maf", "r");
    int nseq = 0;
    struct seqMap **block = readMaf

    return 0;
}

void print_maps(struct seqMap *seq)
{
    printf("projectSeqIndex:\n");
    int i;
    for (i=0; i< seq->seqlen; i++)
    {
        printf("%d ", projectSeqIndex(i, seq));
    }
    printf("\n");
    printf("deprojectMapIndex:\n");
    for (i=0; i< seq->maplen; i++)
    {
        printf("%d ", deprojectMapIndex(i, seq));
    }
    printf("\n");
}
