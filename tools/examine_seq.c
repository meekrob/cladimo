#define _GNU_SOURCE // for asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pwm.h"
#include "library.h"
#include "pwm_readers.h"

#include "maf.h"
#include "seq.h"
#include "mafchop.h"

#include "cladistic_motifs.h"
#include "cladistic_motif_printing.h"
#include "combo_lib.h"

int main(int argc, char **argv)
{
    struct combo_lib *runlib = parse_arg_list(&(argv[1]), 1);
    int i;
    float range = runlib->pwm_library[0]->max_score - runlib->pwm_library[0]->min_score;
    fprintf(stdout, "%.3f - %.3f.\n", runlib->pwm_library[0]->min_score, runlib->pwm_library[0]->max_score);
    print_pwm(stdout, runlib->pwm_library[0]);
    float forscore = 0.;
    for(i=0; i< strlen(argv[2]); i++)
    {
        fprintf(stdout, "%.3f ", (runlib->pwm_library[0]->matrix[base_offset(argv[2][i])][i]) / range);
        forscore +=  (runlib->pwm_library[0]->matrix[base_offset(argv[2][i])][i]) / range;
    }
    putchar('\n');
    float msum = 0.;
    for(i=0; i< runlib->pwm_library[0]->length; i++)
    {
        int j;
        float colsum = 0.;
        for(j=0;j<4;j++) colsum += fabs(runlib->pwm_library[0]->matrix[j][i]);
        fprintf(stdout, "%.3f ", colsum);
        msum += colsum;
    }
    putchar('\n');
    fprintf(stdout, "msum %.3f\n", msum);
    //fprintf(stdout, "%.3f - %.3f = %.3f\n", forscore, runlib->pwm_library[0]->min_score/range,  forscore - ( runlib->pwm_library[0]->min_score/range));
    free_combo_lib(&runlib);
    return 0;
}
