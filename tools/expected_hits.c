#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "pwm.h"
#include "combo_lib.h"
#include "matrix.h"

struct sequence_score
{
    bitsequence bseq;
    double forward_logp; // so that the effective score can be recomputed after the fact with different background distributions
    int strand;
    float score;
};

int cmp(const void *a, const void *b)
{
    float y = ((struct sequence_score *)a)->score;
    float x = ((struct sequence_score *)b)->score;
    return (-1)*(x<y) + (+1)*(x>y);
}

double forescore(bitsequence bseq, float **pmatrix, int pwmlen, int strand)
{
    _pwm_loop_
    double score = 0;
    bitsequence word = bseq;
    for_each_position(pwmlen)
    {
        double p = 0;
        if (strand==1)
            p = pmatrix[word & 3][pwmlen - position - 1];
        else
            p = pmatrix[3 -(word & 3)][position];

        word >>= 2;
        score += log(p);
    }
    return score;
}

int main(int argc, char **argv)
{
    const char STRANDS[] = "-+";
    unsigned char seqbuf[MAXWORDSIZE +1];

    struct combo_lib *runlib = parse_arg_list(&(argv[1]), argc-1);
    int pwmi=0;
    PWM *pwm;

    struct sequence_score top_scores[10];
    size_t nscores = sizeof(top_scores) / sizeof(top_scores[0]);

    for (pwmi=0; pwmi < runlib->total_n; pwmi++) 
    {
        pwm = runlib->pwm_library[pwmi];
        fprintf(stderr, "pwm length %d MAXWORDSIZE %ld\n", pwm->length, MAXWORDSIZE);
        print_pwm(stderr, pwm);
        float **pmatrix = pwm_pmatrix(pwm);
        bzero(top_scores, nscores*sizeof(struct sequence_score));

        // scan sequence space
        bitsequence max_bseq = 1;
        fprintf(stderr, "max_bseq %lld\n", max_bseq);
        max_bseq <<= (2*pwm->length);
        fprintf(stderr, "max_bseq %lld\n", max_bseq);
        max_bseq -= 1;
        fprintf(stderr, "max_bseq %lld\n", max_bseq);
        deparse(seqbuf, max_bseq, pwm->length);
        fprintf(stderr, "%s\n", seqbuf);

        bitsequence bseq = 0;
        while (bseq < max_bseq)
        {
            float score = numeric_wordscore(bseq, pwm);

            // insert if needed
            if (score > top_scores[nscores-1].score)
            {
                int strand = score > 0;
                top_scores[nscores-1].strand = strand;
                top_scores[nscores-1].score = score;
                top_scores[nscores-1].bseq = bseq;
                top_scores[nscores-1].forward_logp = forescore(bseq, pmatrix, pwm->length, strand);
                qsort(top_scores,nscores, sizeof(struct sequence_score), cmp);
            }
            bseq++;
        }
        free_matrix(&pmatrix);
        long long int iterations = (long long int)bseq;
        fprintf(stderr, "reached %lld\n", iterations);
        deparse(seqbuf, bseq-1, pwm->length);
        fprintf(stderr, "reached %s\n", seqbuf);

        int i;
        for (i=0; i<nscores; i++)
        {
            struct sequence_score *tp = &(top_scores[i]);
            deparse(seqbuf, tp->bseq, pwm->length);
            printf("%03d %c %.3f %.3e %.*s\n", i, STRANDS[tp->strand], tp->score, tp->forward_logp, pwm->length, seqbuf);
        }
    }
    free_combo_lib(&runlib);
    return 0;
}
