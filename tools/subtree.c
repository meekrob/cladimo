#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "common_names.h"

int main(int argc, char **argv)
{
    char treestring[0x100000];
    if (argc < 2)
    {
        fprintf(stdout, "%s file.nh leaf1 leaf2 ...\n", argv[0]);
        return 1;
    }
    FILE *infile = fopen(argv[1],"r");
    fscanf(infile, "%s", treestring);
    struct btree *tree = parse_nh(treestring);

    char **limit = &(argv[2]);
    float len = 0;
    struct btree* subtree = get_subtree(tree, limit, argc - 2, &len);
    print_nh(stdout, subtree, len, 4);
    printf("\n");
    depth_first_free(&tree);
    depth_first_free(&subtree);
    return 0;
}
