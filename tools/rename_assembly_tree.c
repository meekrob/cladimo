#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "common_names.h"
void rename_tree(struct btree *node)
{
    if (node == NULL) return;
    char *renamed = NULL;
    if (strlen(node->name) > 0)
    {
        renamed = translate_assembly_name(node->name, -1);
        strcpy(node->name, renamed);
    }
    rename_tree(node->left);
    rename_tree(node->right);
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "%s tree.nh\n", argv[0]);
        return 1;
    }
    char treestring[0x100000];
    FILE *infile = fopen(argv[1],"r");
    fscanf(infile, "%s", treestring);
    init_assembly_names();

    struct btree *tree = parse_nh(treestring);
    rename_tree(tree);
    print_nh(stdout, tree, 0,6);
    printf("\n");
    depth_first_free(&tree);
    return 0;
}
