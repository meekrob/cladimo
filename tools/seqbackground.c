#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "seq.h"

int main(int argc, char **argv)
{
    size_t i;
    struct { int a; int c; int g; int t; } background = {0,0,0,0};
    SEQ *seqq = NULL;
    seqq = seq_open(argv[1]);
    seq_read(seqq);
    uchar* seq = SEQ_CHARS(seqq);
    size_t seqlen = strlen((char*)seq);
    for (i=0; i<seqlen; i++)
    {
        switch(seq[i])
        {
            case 'a':
            case 'A':
                background.a++;
                break;
            case 'c':
            case 'C':
                background.c++;
                break;
            case 'g':
            case 'G':
                background.g++;
                break;
            case 't':
            case 'T':
                background.t++;
                break;
            //default:
                //fprintf(stderr, "encountered '%c'\n", seq[i]);
        }
    }
    seq_close(seqq);
    int n = background.a + background.c + background.g + background.t;
    printf("%c: %.3f\n", 'A', (float)(background.a)/n);
    printf("%c: %.3f\n", 'C', (float)(background.c)/n);
    printf("%c: %.3f\n", 'G', (float)(background.g)/n);
    printf("%c: %.3f\n", 'T', (float)(background.t)/n);

    return 0;
}
