#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>

#include "pwm.h"
#include "library.h"
#include "matrix.h"

enum { motif, accession, name, label };
regex_t preg;
int match_regex(char *pattern, char *word);
int preg_initted = 0;
int match_suffix(char *suffix);

char template[] = "\
<position_weight_matrix>\n\
    <library_name>%s</library_name>\n\
    <accession>%s</accession>\n\
    <name>%s</name>\n\
    <label><![CDATA[%s]]></label>\n\
    <motif>%s</motif>\n\
    <length>%d</length>\n";

void print_pwm_obj(PWM *pwm)
{
    fprintf(stdout, template, pwm->library_name, pwm->accession, pwm->name, pwm->label, pwm->motif, pwm->length);
    int i;
    fprintf(stdout, "    <matrix>\n");
    float **count_matrix = pwm_counts(pwm);
    for (i=0; i<pwm->length; i++)
    {
        //fprintf(stdout, "        <row pos=\"%d\"><a>%.3f</a><c>%.3f</c><g>%.3f</g><t>%.3f</t></row>\n", i, pwm->matrix[0][i], pwm->matrix[1][i], pwm->matrix[2][i], pwm->matrix[3][i]);
        fprintf(stdout, "        <row pos=\"%d\"><a>%d</a><c>%d</c><g>%d</g><t>%d</t></row>\n", i, (int)count_matrix[0][i], (int)count_matrix[1][i], (int)count_matrix[2][i], (int)count_matrix[3][i]);
    }
    free_matrix(&count_matrix);
    fprintf(stdout, "    </matrix>\n");
    fprintf(stdout, "</position_weight_matrix>\n");
}

float score_motif(char *kmer, PWM* pwm, int k, int start)
{
    float score = 0;
    int i;
    for (i=0; i < k; i++)
    {
        if (pwm->motif[start+i] == 'N') continue;
        score += patmatch((unsigned char)kmer[i], pwm->motif[start+i]); // 1 if match, else 0
    }
    return score / k;
}
float search_motif(char *pattern, PWM *pwm)
{
    return (float)(match_regex(pattern, pwm->motif));
}
/*float search_motif(char *kmer, PWM *pwm)
{
    int len = MIN(pwm->length, strlen(kmer));
    int width = MAX(pwm->length, strlen(kmer));
    int start=0;
    float max_score = 0;
    for(start=0; start+len < width; start++)
    {
        float score = score_motif(kmer, pwm, len, start) * ((float)len/width);
        max_score = MAX(score, max_score);
    }
    return max_score;
}*/

float search_accession(char *pattern, PWM *pwm)
{
    return (float)(match_regex(pattern, pwm->accession));
}
float search_name(char *pattern, PWM *pwm)
{
    return (float)(match_regex(pattern, pwm->name));
}
float search_label(char *pattern, PWM *pwm)
{
    return (float)(match_regex(pattern, pwm->label));
}

int main(int argc, char **argv)
{
    char *search_term;
    if (argc < 2)
    {
        fprintf(stdout, "%s search terms\n", argv[0]);
        return 1;
    }
    int total_n=0;
    int pwmi;
    PWM **pwm_library = build_static_library(&total_n);
    char *suffix = rindex(argv[0], '_') + 1;
    float (*scorefunc)(char*, PWM*) = NULL;

    switch(match_suffix(suffix))
    {
        case motif:
            scorefunc = search_motif;
            break;
        case accession:
            scorefunc = search_accession;
            break;
        case name:
            scorefunc = search_name;
            break;
        case label:
            scorefunc = search_label;
            break;
    }

    sort_library(pwm_library, total_n, suffix);

    printf("<?xml version=\"1.0\"?>\n");
    printf("<search_results>");

    int i;
    for (i=1; i < argc; i++)
    {
        search_term = argv[i];
        preg_initted = 0;
        int max_index = -1;
        float max_score = 0;
        printf("<search_result term=\"%s\" field=\"%s\">\n", search_term, suffix);
        for (pwmi=0; pwmi<total_n; pwmi++)
        {
            float score = scorefunc(search_term, pwm_library[pwmi]);
            if (score > max_score)
            {
                max_index = pwmi;
                max_score = score;
            }
            if (score > 0.5)
            {
                //printf("%s matched with %s at %.3f\n", search_term, pwm_library[pwmi]->name, score);
                //printf("%s\n", pwm_library[pwmi]->label);
                //printf("score %.2f\n",score);
                print_pwm_obj(pwm_library[pwmi]);
            }
        }
        printf("</search_result>\n");

        //if (max_index == -1) printf("no matches :(\n");
        regfree(&preg);
    }
    printf("</search_results>");
    free(pwm_library);
    return 0;
}
int match_suffix(char *suffix)
{
    if (0==strcasecmp(suffix, "motif")) return motif;
    else if (0==strcasecmp(suffix, "accession")) return accession;
    else if (0==strcasecmp(suffix, "name")) return name;
    else if (0==strcasecmp(suffix, "label")) return label;
    else
        return -1;
}
void compile_pattern(char * pattern)
{
    int cflags = REG_ICASE | REG_EXTENDED;
    int errcode=regcomp(&preg, pattern, cflags); 
    // do something if error
    char errbuf[64];
    size_t rg = regerror(errcode, &preg, errbuf, 64);
    if (errcode) fprintf(stderr, "%d regerror %.*s\n", errcode, (int)rg, errbuf);

}
int match_regex(char *pattern, char *word)
{
    if (! preg_initted)
    {
        compile_pattern(pattern);
        preg_initted = 1;
    }
    int eflags = 0;
    regmatch_t pmatch[1];
    int success = regexec(&preg, word, 1, pmatch, eflags);
    return (success != REG_NOMATCH);
}
