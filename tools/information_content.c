#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "pwm.h"
#include "combo_lib.h"
#include "matrix.h"
float *partials(PWM *pwm);
char nt[] = {'A','C','G','T'};
float add_pseudocounts(float **mx, PWM *pwm)
{
    int i,j;
    for (i=0;i<4;i++)
    {
        for (j=0;j<pwm->length;j++)
            //mx[i][j] += pwm->base_pseudocounts[i] / pwm->length;
            mx[i][j] += 1;
    }
    return array_sum(pwm->base_pseudocounts, 4);
}
float Ufunc(float P)
{
    if (P==0) return 0;
    return -P * log2f(P);
}
float uncertainty(float *P, int len)
{
    int i = 0;
    float H = 0;
    while (i<len) H += Ufunc(P[i++]);
    return H;
}
void print_mat(FILE *out, PWM *pwm, float **mx, char mode)
{
    int i,j;
    int nrows = 4;
    int symbol_spacing = 5;
    //if (mode == 'f') symbol_spacing = 5;
    if (pwm->motif[0] != 0)
    {
        fprintf(out,"%*c ", 4, ' ');
        for (i=0; i<pwm->length; i++) 
            fprintf(out,"%*c ", symbol_spacing, pwm->motif[i]);
        fputc('\n',out);
    }
    for (i=0; i<nrows; i++) 
    {
        fprintf(out,"%4c ",nt[i]);
        for(j=0; j< pwm->length; j++)
            if ((mode == 'i') || (mx[i][j] == (int)mx[i][j]))
                fprintf(out, "%*d ", symbol_spacing, (int)mx[i][j]);
            else
                fprintf(out, "%*.2f ", symbol_spacing, mx[i][j] );
        fputc('\n',out);
    }
}
float *Hfunc_counts(PWM *pwm, int do_pseudocounts)
{
    int i,column;
    //float H_background = uncertainty(pwm->background, 4);
    float H_background = 0;
    float *H_result = calloc(pwm->length, sizeof(float));
    float **count_matrix = pwm_untransform(pwm);
    float correction = 0;
    if (do_pseudocounts)
    {
        correction = add_pseudocounts(count_matrix, pwm);
        print_mat(stdout, pwm, count_matrix, 'f');
    }
    else
        print_mat(stdout, pwm, count_matrix, 'i');
    for (column=0; column < pwm->length; column++)
    {
        float H = 0.;
        for (i=0; i<4; i++)
        {
            float P = count_matrix[i][column] / (pwm->Nsites+correction);
            H += Ufunc(P);
        }
        H_result[column] = H_background - H;
    }
    free_matrix(&count_matrix);
    return H_result;
}
float *Hfunc(PWM *pwm)
{
    int i;
    //float H_background = uncertainty(pwm->background, 4);
    float H_background = 0;
    float *H_result = calloc(pwm->length, sizeof(float));
    
    int column;
    for (column=0; column < pwm->length; column++)
    {
        float H = 0.;
        for (i=0; i<4; i++)
        {
            float P = expf(pwm->matrix[i][column]) * pwm->background[i];
            H += Ufunc(P);
        }
        H_result[column] = H_background - H;
    }
    return H_result;
}

#ifndef INFORMATION_CONTENT_MAIN
#define INFORMATION_CONTENT_MAIN test_main
#endif

int INFORMATION_CONTENT_MAIN(int argc, char **argv)
{
    int i;
    char *run[] = {"MA0037"};
    int nrun = sizeof(run)/sizeof(run[0]);
    struct combo_lib *runlib;
    if (argc < 2) 
        runlib = parse_arg_list(run, nrun);
    else
        runlib = parse_arg_list(&(argv[1]), argc-1);

    for(i=0; i< runlib->total_n; i++)
    {
        float *H;
        int j;
        PWM* pwm = runlib->pwm_library[i];
        fprintf(stdout, "original:\n");
        // before processing
        H = Hfunc_counts(pwm, 0);
        fprintf(stdout, "%4s ", "IC");
        for (j=0; j < pwm->length; j++) printf("%5.2f ", H[j]);
        fprintf(stdout, ": %.3f\n", array_sum(H, pwm->length));
        free(H);

        fprintf(stdout, "base pseudocounts:\n");
        float *base_pseudoprob = calloc(4, sizeof(float));
        float correction = array_sum(pwm->base_pseudocounts, 4);
        for(j=0; j<4; j++)
        {
            fprintf(stdout,"%4c %.3f\n",nt[j], pwm->base_pseudocounts[j]);
            base_pseudoprob[j] = pwm->base_pseudocounts[j] / correction;
        }
        fprintf(stdout, "base pseudocount IC: %.3f\n", 2 - uncertainty(base_pseudoprob, 4));
        free(base_pseudoprob);
        fprintf(stdout, "original + base pseudocounts:\n");
        // after processing
        H = Hfunc_counts(pwm, 1);
        fprintf(stdout, "%4s ", "IC");
        for (j=0; j < pwm->length; j++) printf("%5.2f ", H[j]);
        fprintf(stdout, ": %.3f\n", array_sum(H, pwm->length));
        free(H);

        fprintf(stdout, "weight matrix:\n");
        print_pwm(stdout, pwm);
        // after processing
        H = Hfunc(pwm);
        fprintf(stdout, "%4s ", "IC");
        for (j=0; j < pwm->length; j++) printf(" %4.2f ", H[j]);
        fprintf(stdout, ": %.3f\n", array_sum(H, pwm->length));
        free(H);

        int nfields = 0;
        int field_i = 0;
        char **info;
        float range = pwm->max_score - pwm->min_score;
        fprintf(stdout, "type some sequences:");
        while( (info = linesplit(stdin, &nfields)) != NULL)
        {
            fprintf(stdout, "info %p nfields %d\n", info, nfields);
            while(field_i < nfields)
            {
                float total_score = 0;
                // print user sequence
                fprintf(stdout,"%*c ", 4, ' ');
                for (i=0; i<pwm->length; i++) 
                    fprintf(stdout,"%*c ", 5, info[field_i][i]);
                fputc('\n',stdout);

                // print the score of the user sequence 
                fprintf(stdout,"%4c ", ' ');
                for (j=0; j < MIN(strlen(info[field_i]), pwm->length); j++) 
                {
                    int base = base_offset(info[field_i][j]);
                    float val = pwm->matrix[base][j]; 
                    fprintf(stdout, "% 4.2f ", val);
                    total_score += val;
                }
                //int slen = MIN(strlen(info[field_i]), pwm->length);
                fprintf(stdout, "\t:%.3f - %.3f = %.3f\n", total_score, pwm->min_score/range, (total_score - pwm->min_score)/range);
                field_i++;
            }
            fieldsfree(&info);
            field_i = 0;
            info = NULL;
            fprintf(stdout, "type some sequences:");
        }
    }
    free_combo_lib(&runlib);
    return 0;
}
