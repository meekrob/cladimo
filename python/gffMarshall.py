#!/usr/bin/env python
import sys
import shlex

def strformat(val, places=3):
    if int(val) == val: return "%d" % val
    else: 
        return "%.*f"  % (places, val)

def next_group( gffs ):
    last = None
    group = []
    gffs.sort()
    for gff in gffs:
            current = (gff.seqname, gff.start, gff.end)
            if current == last:
                group.append( gff )
            else:
                if group: yield group
                group = [gff]
            last = current
    if group: yield group


def next_mafblock( lines ):
    block = []
    for line in lines:
        if line.startswith('#'):
            continue
        if not line.isspace():
            try: gff = GFF(line, commasplit="True")
            except:
                print >>sys.stderr, "failed to gff %s" % line
                raise
            if gff.feature == 'maf_block':
                if block: yield block
                block = [gff]
            else:
                block.append( gff )
    if block: 
        yield block
        

def next_clm( gffs ):
    for group in next_group( gffs ):
        # line types
        pwm_matches = []
        clm = None
        mafblock = None
        # find the spanning range
        minleft = None    
        maxright = 0
        
        for gff in group:
            if gff.feature == 'pwm_match':
                pwm_matches.append( gff )
                left = gff.attributes[ 'column' ]
                right = left + gff.attributes[ 'width' ]
                maxright = max( right, maxright )
                if minleft == None: 
                    minleft = left
                else:
                    minleft = min( left, minleft )
            elif gff.feature == 'maf_block':
                mafblock = gff
            elif gff.feature == 'cladistic_motif':
                clm = gff

        if clm: yield { 'mafblock': mafblock, 'clm': clm, 'pwm_matches': pwm_matches, 'position': (minleft,maxright) }
        
def flatten(l):
  if not isinstance(l, (list, tuple)): return l
  out = []
  for item in l:
    if isinstance(item, (list, tuple)):
      out.extend(flatten(item))
    else:
      out.append(item)
  return out

def convert_type( val ):
    if isinstance( val, list ): 
        for i in range( len( val ) ):
            val[i] = convert_type( val[i] )
    elif isinstance( val, tuple ):
        val = list(val)
        for i in range( len( val ) ):
            val[i] = convert_type( val[i] )
        return tuple(val)
    elif isinstance( val, str ) and val.find(',') >= 0:
        val = convert_type( val.split(',') )
    else:
        try: 
            fvalue = float( val )
            try:
                ivalue = int( val )
                if fvalue == ivalue: 
                    return ivalue
            except:
                return fvalue
        except:
            return val
    return val

class GFF:
    def __init__(self, src, **options):
        self.attributes = {}
        self.options = options
        if isinstance(src, str):
            text = src
            fields = text.split('\t')
        elif isinstance( src, (list, tuple) ):
            fields = src
        else:
            raise TypeError

        try:
            seqname, source, feature, start, end, score, strand, frame = fields[:8]
        except:
            raise
        self.seqname = seqname
        self.source = source
        self.feature = feature
        self.start = int( start )
        self.end = int( end )
        self.score = float( score )
        self.strand = strand
        self.frame = frame
        if len( fields ) > 8: 
            attr_str = fields[8]
            for attr  in attr_str.split( ';' ):
                attr_strp = attr.strip()
                # only run shlex if needed, it is a massive hog
                if attr_strp.find('"') > 0 or attr_strp.find("'") > 0:
                    terms = shlex.split( attr_strp )
                else:
                    terms = attr_strp.split()
                
                if not terms: 
                    continue

                key = terms.pop( 0 )
                #self.attributes[ key ] = self.termfunc( terms )
                self.attributes[ key ] = terms # termfunc->convert_type takes too long

    def write(self, fp, newline=True):                    
        fp.write( ("%s %s %s %d %d %s %s %s " % (self.seqname, self.source, self.feature, self.start, self.end, strformat(self.score), self.strand, self.frame) ).replace(' ','\t') )
        self.write_attributes( fp )
        if newline: fp.write( "\n" )
    def write_attributes(self, fp):
        hashlist = [ [i,j] for i,j in self.attributes.items() ]
        h = map( flatten, hashlist )
        h2 = [ map( str, item ) for item in h ]
        h3 = [' '.join(item) for item in h2 ] # god damnit
        attr_str = '; '.join( h3 )
        fp.write( attr_str )

    def overlaps_range(self, chrom, start, end):
        if self.seqname != chrom:
            return False
        if (self.start == start) or (self.end == end): # any overlapping start or end point
            #print >>sys.stderr, "(self.start{%d} == start{%d}) or (self.end{%d} == end{%d}) %d"  % (self,start, start, self.end, end, (self.start == start) or (self.end == end))
            return True
        if (self.start == end) or (start == self.end): # contiguous
            return True
        if (self.start <= end) and (self.end >= start): # staggered
            #print >>sys.stderr, "(self.start{%d} <= end{%d}) and (self.end{%d} <= start{%d}) %d " % (self.start, end, self.end, start, (self.start <= end) and (self.end >= start))
            return True

        return False
        

    def overlaps(self, other):
        if other is None: 
            return False
        if self.seqname != other.seqname:
            return False
        if (self.start == other.start) or (self.end == other.end): # any overlapping start or end point
            return True
        if (self.start == other.end) or (other.start == self.end): # contiguous
            return True
        if (self.start <= other.end) and (self.end >= other.start): # staggered
            return True

        return False

    def __repr__(self):
        return "<GFF %s %d %d %s %s %.3f %s>" % (self.seqname, self.start, self.end, self.feature, self.strand, self.score, str(self.attributes))

    def __eq__(self, other):
        if other is None: 
            return False
        else:
            return self.__cmp__(other) == 0
    def __cmp__(self, other):
        sortorder = [cmp(self.seqname, other.seqname), cmp(self.start, other.start), cmp(self.end, other.end), cmp(self.feature, other.feature), cmp(self.source, other.source)]
        for comp in sortorder:
            if comp != 0: return comp

        return 0

    def termfunc(self, terms):
        if len( terms ) == 0: return None
        
        if len( terms ) > 1: 
            return tuple( convert_type( terms ) )
        else:
            try:
                datum = terms[0]
            except:
                print >>sys.stderr, "what is a ", terms
                raise
            if 'commasplit' in self.options and self.options['commasplit'] == True:
                if datum.find(',') >= 0: 
                    return tuple( convert_type( datum.split(',') ) )
            return convert_type( datum )

class BED:
    def __init__(self, src, delim=None):
        if isinstance(src, str):
            text = src
            fields = text.split(delim)
        elif isinstance( src, (list, tuple) ):
            fields = src
        else:
            raise TypeError
        try:
            chrom, start, end = fields[:3]
        except:
            print >>sys.stderr, "failed to parse %s:" % str(src), fields
            raise
    
        self.chrom = chrom
        self.start = int( start )
        self.end = int( end )
        if len(fields) > 3:
            vec = fields[3:]
            self.vec = flatten( convert_type( vec ) )
        else:
            self.vec = []

    def write(self, fp, delim=' '):                    
        vec = delim.join(map(str, flatten( self.vec )))
        fp.write( ('%s %d %d %s\n' % (self.chrom, self.start, self.end, vec) ) )

    def __repr__(self):
        return 'BED(%s)' % str( flatten( [self.chrom, self.start, self.end] + self.vec ) )
    def __eq__(self, other):
        if other is None: 
            return False
        else:
            return self.__cmp__(other) == 0
    def __cmp__(self, other):
        for comp in [cmp(self.chrom, other.chrom), cmp(self.start, other.start), cmp(self.end, other.end)]:
            if comp != 0: return comp

        order = [ cmp(a, b) for a,b in zip(self.vec, other.vec) ]

        for o in order:
            if o != 0: return o

        return 0


if __name__ == "__main__":
    for line in file( sys.argv[1] ):
        if line.isspace() or line.startswith('#'):
            continue
        print GFF(line, commasplit=True)
                
                
                



