#!/usr/bin/env python
from __future__ import division
import sys
from gffMarshall import *

nclm = 0
tracks = {
'maf_block': [], 'cladistic_motif': [], 'match': {} }
for line in file( sys.argv[1] ):
    if line.startswith('#'): continue
    if not line.isspace():
        try:
            gff = GFF(line, commasplit="True")
        except:
            print line,
            continue
        if gff.feature == 'maf_block':
            gff.attributes = {'maf_block':''}
            tracks['maf_block'].append( gff )

        elif gff.feature == 'cladistic_motif':
            gff.attributes = { gff.attributes['pwm_motif']: ''}
            tracks['cladistic_motif'].append( gff )

        elif gff.feature == 'pwm_match':
            motif = gff.attributes['pwm_motif']
            chrom,start,end = gff.attributes['position']
            species = chrom.split('.')[0]
            gff.attributes = { species: '' }
            gff.score *= 1000
            gff.strand = '+'
            if motif not in tracks['match']:
                tracks['match'][motif] = [gff]
            else:
                tracks['match'][motif].append(gff)

ranges = {}
for gff in tracks['cladistic_motif']:
    if gff.seqname not in ranges: 
        ranges[gff.seqname] = [gff.start, gff.end]
    else:
        ranges[gff.seqname][0] = min( ranges[gff.seqname][0], gff.start )
        ranges[gff.seqname][1] = max( ranges[gff.seqname][1], gff.start )

if len(ranges) == 1:
    chrom, position = ranges.items().pop()
    print 'browser position %s:%d-%d' % tuple([ chrom ] + list( position ))

print "browser hide all"
print "browser full cladistic_motif" 
print 'browser pack', ' '.join( tracks['match'].keys() )
print "browser dense maf_block"

print "track name=cladistic_motif"
for gff in tracks['cladistic_motif']:
    gff.score = 0
    gff.write( sys.stdout )

for motif,matches in tracks['match'].items():
    print "track name=%s" % motif
    for gff in matches:
        gff.write( sys.stdout )

print "track name=maf_block"
scores = [block.score for block in tracks['maf_block']]
minscore = min(scores)
maxscore = max(scores)
span = maxscore - minscore
binwidth = int(maxscore/10)

for gff in tracks['maf_block']:
    binleft = int(gff.score / binwidth) * binwidth
    binright = binleft + binwidth
    gff.attributes['bin'] = "%d-%d" % (binleft,binright)
    gff.score = int(((gff.score - minscore)/span) * 1000)
    gff.write( sys.stdout )

