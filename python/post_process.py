#!/usr/bin/env python
# post_process.py - an example processing script for cladimo output (MAF input required)
# 
import sys, os
from gffMarshall import *

if len( sys.argv ) > 1:
    infile = file( sys.argv[1] )
    lines = list( infile )
    
    # parse file GFFs into structures
    for mafblock in next_mafblock( lines ):
        # yields mafblock - []
        # list of gff objects
        for clm in next_clm( mafblock ):
            # yields clm - {}
            # dict of gff objects: mafblock, clm, pwm_matches

            maxscore = 0
            for match in clm['pwm_matches']:
                maxscore = max( maxscore, match.score )

            # add attribute
            clm['clm'].attributes['"max score"'] = maxscore
            clm['clm'].write( sys.stdout ) # output GFF format
