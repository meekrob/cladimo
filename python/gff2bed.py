#!/usr/bin/env python
import sys
from gffMarshall import *

feature = None
attributes = []

if len( sys.argv ) > 1:
    infile = sys.argv[1]

if len( sys.argv ) > 2:
    feature = sys.argv[2]

if len( sys.argv ) > 3:
    attributes = sys.argv[3].split(',')

print '#', 'chrom', 'start', 'end', 'feature',
for at in attributes: print at,
print

for line in file( infile ):
    if line.startswith('#'): continue
    if not line.isspace():
        try: gff = GFF(line, commasplit="True")
        except:
            continue
        # sub-select
        if feature and gff.feature != feature: 
            continue

        fields = [gff.seqname, gff.start - 1, gff.end - 1]
        for attr in attributes:
            if attr in gff.attributes:
                val = gff.attributes[ attr ]
                if isinstance( val, (tuple, list)):
                    s = ','.join( map( str, flatten( val ) ) )
                fields.append( val )
            else:
                fields.append( 'NA' )
        
        bed = BED( fields )
        bed.write( sys.stdout, delim=',' )

