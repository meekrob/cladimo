#!/usr/bin/env python
import sys, os
from gffMarshall import *

def print_cluster( cluster ):
    chromStart = min( [ clm['clm'].start for clm in cluster ] )
    chromEnd = max( [ clm['clm'].end for clm in cluster ] )
    temp = GFF( ['0'] * 8 )
    temp.seqname = cluster[0]['clm'].seqname
    temp.strand = cluster[0]['clm'].strand
    temp.frame = cluster[0]['clm'].frame
    temp.start = chromStart
    temp.end = chromEnd
    temp.feature = 'cluster'
    temp.source = os.path.basename( sys.argv[0] )
    temp.score = 0
    temp.attributes = { 'nclms': len( cluster )}
    sp_counts = {}
    for clmi, clm in enumerate( cluster ): 
        splist = clm['clm'].attributes['species_matched']
        if not isinstance(splist, (list, tuple)): 
            if splist.find(',') >= 0: 
                splist = splist.split(',')
            else:
                splist = [splist]
        for sp in splist:
            if sp in sp_counts: sp_counts[sp] += 1
            else: sp_counts[sp] = 1

    for sp, count in sp_counts.items():
        temp.attributes[sp] = count
        
    temp.write( sys.stdout )
    for clmi, clm in enumerate( cluster ): 
        clm['clm'].attributes['cluster'] = str( temp.start ) + '.' + str( clmi )
        #print '# clm'
        clm['clm'].write( sys.stdout )
        for match in clm['pwm_matches']: 
            if 'pwm_motif' in match.attributes: del match.attributes['pwm_motif']
            if 'width' in match.attributes: del match.attributes['width']
            if 'column' in match.attributes: del match.attributes['column']
            #match.write( sys.stdout )
    

if len( sys.argv ) > 2:
    breakdist = int(sys.argv[2] )
else:
    breakdist = 1

if len( sys.argv ) > 1:
    infile = sys.argv[1]
    for mafblock in next_mafblock( list( file( infile ) ) ):
        #print "mafblock"
        clms = []
        diffs = [0]
        for clm in next_clm( mafblock ):
            if clms:
                last = clms[-1]
                diff = clm['position'][0] - last['position'][1]
                diffs.append( diff )
                clms.append( clm )
            else:
                clms.append( clm )
            
        if len( clms ) == 1:
            print '## cluster'
            cluster = clms
            print_cluster( cluster )

        # report clusters within a break distance
        startix = 0
        for i in range(1, len( diffs ) ):
            curr = diffs[i]
            if curr >= breakdist:
                print '## cluster'
                if i == len( clms ) - 1: # end case is a singleton cluster
                    cluster = [ clms[-1] ]
                    print_cluster( cluster )
                else:
                    cluster = clms[startix:i]
                    print_cluster( cluster )
                    startix = i

            if i == len( clms ) - 1: # end case is in the final cluster
                print '## cluster' 
                cluster = clms[startix:]
                print_cluster( cluster )



